<?php

class ControllerCheckoutMember extends Controller {

    public function index() {

        $this->load->model('affiliate/affiliate');

        $this->load->language('checkout/checkout');

        $affiliates = $this->model_affiliate_affiliate->getAllAffiliates();
        if (isset($this->request->cookie['tracking'])) {
            $member_id = $this->model_affiliate_affiliate->getAffiliateByCode($this->request->cookie['tracking']);
            if ($member_id) {
                $getChildaffiliates = $this->model_affiliate_affiliate->getChildAffiliatesDetail($member_id['affiliate_id']);
                if ($getChildaffiliates) {
                    $affiliates = $getChildaffiliates;
                }
            }
        }

        $data['text_loading'] = $this->language->get('text_loading');
        $data['button_continue'] = $this->language->get('button_continue');

        $data['member'] = $this->language->get('text_member');
        $data['default_option'] = $this->language->get('text_member_default');
        $data['affiliates'] = $affiliates;

        $this->response->setOutput($this->load->view('checkout/member', $data));
    }

    public function save() {
        $this->load->language('checkout/checkout');
        $this->response->addHeader('Content-Type: application/json');

        $json = array();

        /* if (isset($this->request->post['member'])) {
          setcookie('tracking',$this->request->post['member'], time() + 3600 * 24 * 1000, '/');
          } */

        if (isset($this->request->post['member']) && $this->request->post['member'] != '') {
            setcookie('tracking', $this->request->post['member'], time() + 3600 * 24 * 1000, '/');
        } else {
            $json['error']['warning'] = $this->language->get('error_member');
        }
        $this->response->setOutput(json_encode($json));
    }

}
