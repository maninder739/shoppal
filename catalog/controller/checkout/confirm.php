<?php

class ControllerCheckoutConfirm extends Controller {

    public function index() {

        //echo "<pre>"; print_r($this->session->data); die('maninder');
        $redirect = '';

        if ($this->cart->hasShipping()) {
            // Validate if shipping address has been set.
            if (!isset($this->session->data['shipping_address'])) {

                $redirect = $this->url->link('checkout/checkout', '', true);
            }

            // Validate if shipping method has been set.
            if (!isset($this->session->data['shipping_method'])) {

                $redirect = $this->url->link('checkout/checkout', '', true);
            }
        } else {
            unset($this->session->data['shipping_address']);
            unset($this->session->data['shipping_method']);
            unset($this->session->data['shipping_methods']);
        }

        // Validate if payment address has been set.
        if (!isset($this->session->data['payment_address'])) {
            $redirect = $this->url->link('checkout/checkout', '', true);
        }

        // Validate if payment method has been set.
        if (!isset($this->session->data['payment_method'])) {
            $redirect = $this->url->link('checkout/checkout', '', true);
        }

        // Validate cart has products and has stock.
        if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
            $redirect = $this->url->link('checkout/cart');
        }

        // Validate minimum quantity requirements.
        $products = $this->cart->getProducts();

        foreach ($products as $product) {
            $product_total = 0;

            foreach ($products as $product_2) {
                if ($product_2['product_id'] == $product['product_id']) {
                    $product_total += $product_2['quantity'];
                }
            }

            if ($product['minimum'] > $product_total) {
                $redirect = $this->url->link('checkout/cart');

                break;
            }
        }

        if (!$redirect) {


            $order_data = array();

            $totals = array();
            $taxes = $this->cart->getTaxes();
            $total = 0;

            // Because __call can not keep var references so we put them into an array.
            $total_data = array(
                'totals' => &$totals,
                'taxes' => &$taxes,
                'total' => &$total
            );

            $this->load->model('extension/extension');

            $sort_order = array();

            $results = $this->model_extension_extension->getExtensions('total');

            foreach ($results as $key => $value) {
                $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
            }

            array_multisort($sort_order, SORT_ASC, $results);

            foreach ($results as $result) {
                if ($this->config->get($result['code'] . '_status')) {
                    $this->load->model('extension/total/' . $result['code']);

                    // We have to put the totals in an array so that they pass by reference.
                    $this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
                }
            }

            $sort_order = array();

            foreach ($totals as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $totals);

            $order_data['totals'] = $totals;

            $this->load->language('checkout/checkout');

            $order_data['invoice_prefix'] = date('Ym') . '-';
            $order_data['store_id'] = $this->config->get('config_store_id');
            $order_data['store_name'] = $this->config->get('config_name');

            if ($order_data['store_id']) {
                $order_data['store_url'] = $this->config->get('config_url');
            } else {
                if ($this->request->server['HTTPS']) {
                    $order_data['store_url'] = HTTPS_SERVER;
                } else {
                    $order_data['store_url'] = HTTP_SERVER;
                }
            }


            if ($this->customer->isLogged()) {
                $this->load->model('account/customer');

                $customer_info = $this->model_account_customer->getCustomer($this->customer->getId());

                $order_data['customer_id'] = $this->customer->getId();
                $order_data['customer_group_id'] = $customer_info['customer_group_id'];
                $order_data['firstname'] = $customer_info['firstname'];
                $order_data['lastname'] = $customer_info['lastname'];
                $order_data['email'] = $customer_info['email'];
                $order_data['telephone'] = $customer_info['telephone'];
                $order_data['fax'] = $customer_info['fax'];
                $order_data['custom_field'] = json_decode($customer_info['custom_field'], true);
                $order_data['affiliate_id'] = $customer_info['affiliate_id'];
            } elseif (isset($this->session->data['guest'])) {
                $order_data['customer_id'] = 0;
                $order_data['customer_group_id'] = $this->session->data['guest']['customer_group_id'];
                $order_data['firstname'] = $this->session->data['guest']['firstname'];
                $order_data['lastname'] = $this->session->data['guest']['lastname'];
                $order_data['email'] = $this->session->data['guest']['email'];
                $order_data['telephone'] = $this->session->data['guest']['telephone'];
                $order_data['fax'] = $this->session->data['guest']['fax'];
                $order_data['custom_field'] = $this->session->data['guest']['custom_field'];
                $order_data['affiliate_id'] = $this->session->data['guest']['affiliate_id'];
            }

            $order_data['payment_firstname'] = $this->session->data['payment_address']['firstname'];
            $order_data['payment_lastname'] = $this->session->data['payment_address']['lastname'];
            $order_data['payment_company'] = $this->session->data['payment_address']['company'];
            $order_data['payment_address_1'] = $this->session->data['payment_address']['address_1'];
            $order_data['payment_address_2'] = $this->session->data['payment_address']['address_2'];
            $order_data['payment_city'] = $this->session->data['payment_address']['city'];
            $order_data['payment_postcode'] = $this->session->data['payment_address']['postcode'];
            $order_data['payment_zone'] = $this->session->data['payment_address']['zone'];
            $order_data['payment_zone_id'] = $this->session->data['payment_address']['zone_id'];
            $order_data['payment_country'] = $this->session->data['payment_address']['country'];
            $order_data['payment_country_id'] = $this->session->data['payment_address']['country_id'];
            $order_data['payment_address_format'] = $this->session->data['payment_address']['address_format'];
            $order_data['payment_custom_field'] = (isset($this->session->data['payment_address']['custom_field']) ? $this->session->data['payment_address']['custom_field'] : array());


            if (isset($this->session->data['payment_method']['title'])) {
                $order_data['payment_method'] = $this->session->data['payment_method']['title'];
            } else {
                $order_data['payment_method'] = '';
            }

            if (isset($this->session->data['payment_method']['code'])) {
                $order_data['payment_code'] = $this->session->data['payment_method']['code'];
            } else {
                $order_data['payment_code'] = '';
            }

            if ($this->cart->hasShipping()) {
                $order_data['shipping_firstname'] = $this->session->data['shipping_address']['firstname'];
                $order_data['shipping_lastname'] = $this->session->data['shipping_address']['lastname'];
                $order_data['shipping_company'] = $this->session->data['shipping_address']['company'];
                $order_data['shipping_address_1'] = $this->session->data['shipping_address']['address_1'];
                $order_data['shipping_address_2'] = $this->session->data['shipping_address']['address_2'];
                $order_data['shipping_city'] = $this->session->data['shipping_address']['city'];
                $order_data['shipping_postcode'] = $this->session->data['shipping_address']['postcode'];
                $order_data['shipping_zone'] = $this->session->data['shipping_address']['zone'];
                $order_data['shipping_zone_id'] = $this->session->data['shipping_address']['zone_id'];
                $order_data['shipping_country'] = $this->session->data['shipping_address']['country'];
                $order_data['shipping_country_id'] = $this->session->data['shipping_address']['country_id'];
                $order_data['shipping_address_format'] = $this->session->data['shipping_address']['address_format'];
                $order_data['shipping_custom_field'] = (isset($this->session->data['shipping_address']['custom_field']) ? $this->session->data['shipping_address']['custom_field'] : array());
                $order_data['remain_anonymous'] = $this->session->data['shipping_address']['remain_anonymous'];
                $order_data['behalf_of'] = $this->session->data['shipping_address']['behalf_of'];
                $order_data['name'] = $this->session->data['shipping_address']['name'];


                if (isset($this->session->data['shipping_method']['title'])) {
                    $order_data['shipping_method'] = $this->session->data['shipping_method']['title'];
                } else {
                    $order_data['shipping_method'] = '';
                }

                if (isset($this->session->data['shipping_method']['code'])) {
                    $order_data['shipping_code'] = $this->session->data['shipping_method']['code'];
                } else {
                    $order_data['shipping_code'] = '';
                }
            } else {
                $order_data['shipping_firstname'] = '';
                $order_data['shipping_lastname'] = '';
                $order_data['shipping_company'] = '';
                $order_data['shipping_address_1'] = '';
                $order_data['shipping_address_2'] = '';
                $order_data['shipping_city'] = '';
                $order_data['shipping_postcode'] = '';
                $order_data['shipping_zone'] = '';
                $order_data['shipping_zone_id'] = '';
                $order_data['shipping_country'] = '';
                $order_data['shipping_country_id'] = '';
                $order_data['shipping_address_format'] = '';
                $order_data['shipping_custom_field'] = array();
                $order_data['shipping_method'] = '';
                $order_data['shipping_code'] = '';
                $order_data['remain_anonymous'] = '';
                $order_data['behalf_of'] = '';
                $order_data['name'] = '';
            }

            $order_data['products'] = array();

            foreach ($this->cart->getProducts() as $product) {
                $option_data = array();

                foreach ($product['option'] as $option) {
                    $option_data[] = array(
                        'product_option_id' => $option['product_option_id'],
                        'product_option_value_id' => $option['product_option_value_id'],
                        'option_id' => $option['option_id'],
                        'option_value_id' => $option['option_value_id'],
                        'name' => $option['name'],
                        'value' => $option['value'],
                        'type' => $option['type']
                    );
                }

                $order_data['products'][] = array(
                    'product_id' => $product['product_id'],
                    'name' => $product['name'],
                    'model' => $product['model'],
                    'option' => $option_data,
                    'download' => $product['download'],
                    'quantity' => $product['quantity'],
                    'subtract' => $product['subtract'],
                    'price' => $product['price'],
                    'total' => $product['total'],
                    'tax' => $this->tax->getTax($product['price'], $product['tax_class_id']),
                    'reward' => $product['reward']
                );
            }

            // Gift Voucher
            $order_data['vouchers'] = array();

            if (!empty($this->session->data['vouchers'])) {
                foreach ($this->session->data['vouchers'] as $voucher) {
                    $order_data['vouchers'][] = array(
                        'description' => $voucher['description'],
                        'code' => token(10),
                        'to_name' => $voucher['to_name'],
                        'to_email' => $voucher['to_email'],
                        'from_name' => $voucher['from_name'],
                        'from_email' => $voucher['from_email'],
                        'voucher_theme_id' => $voucher['voucher_theme_id'],
                        'message' => $voucher['message'],
                        'amount' => $voucher['amount']
                    );
                }
            }

            $order_data['comment'] = $this->session->data['comment'];
            $order_data['total'] = $total_data['total'];

            if (isset($order_data['affiliate_id'])) {

                $subtotal = $this->cart->getSubTotal();

                // Affiliate
                $this->load->model('affiliate/affiliate');

                //$affiliate_info = $this->model_affiliate_affiliate->getAffiliateByCode($this->request->cookie['tracking']);
                $affiliate_info = $this->model_affiliate_affiliate->getAffiliate($order_data['affiliate_id']);

                $order_data['tracking'] = $affiliate_info['code'];

                if ($affiliate_info) {

                    $order_data['affiliate_id'] = $affiliate_info['affiliate_id'];
                    $order_data['commission'] = ($subtotal / 100) * $affiliate_info['commission'];
                } else {
                    $order_data['affiliate_id'] = 0;
                    $order_data['commission'] = 0;
                }

                // Marketing
                $this->load->model('checkout/marketing');

                $marketing_info = $this->model_checkout_marketing->getMarketingByCode($affiliate_info['code']);

                if ($marketing_info) {
                    $order_data['marketing_id'] = $marketing_info['marketing_id'];
                } else {
                    $order_data['marketing_id'] = 0;
                }
            } else {
                $order_data['affiliate_id'] = 0;
                $order_data['commission'] = 0;
                $order_data['marketing_id'] = 0;
                $order_data['tracking'] = '';
            }

            $order_data['language_id'] = $this->config->get('config_language_id');
            $order_data['currency_id'] = $this->currency->getId($this->session->data['currency']);
            $order_data['currency_code'] = $this->session->data['currency'];
            $order_data['currency_value'] = $this->currency->getValue($this->session->data['currency']);
            $order_data['ip'] = $this->request->server['REMOTE_ADDR'];

            if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
                $order_data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
            } elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
                $order_data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
            } else {
                $order_data['forwarded_ip'] = '';
            }

            if (isset($this->request->server['HTTP_USER_AGENT'])) {
                $order_data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
            } else {
                $order_data['user_agent'] = '';
            }

            if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
                $order_data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
            } else {
                $order_data['accept_language'] = '';
            }

            $this->load->model('checkout/order');

            $order_data['order_type'] = $this->session->data['order_type'];

            if (!empty($this->request->get['old_order_id'])) {
                $order_data['old_order_id'] = $this->request->get['old_order_id'];
                $this->session->data['order_id'] = $this->model_checkout_order->addOrder($order_data);

                $this->autopayment($order_data['customer_id'], $this->session->data['order_id'], $order_data['total'], $order_data['currency_code']);
            } else {
                $this->session->data['order_id'] = $this->model_checkout_order->addOrder($order_data);
            }

            $data['text_recurring_item'] = $this->language->get('text_recurring_item');
            $data['text_payment_recurring'] = $this->language->get('text_payment_recurring');

            $data['column_name'] = $this->language->get('column_name');
            $data['column_model'] = $this->language->get('column_model');
            $data['column_quantity'] = $this->language->get('column_quantity');
            $data['column_price'] = $this->language->get('column_price');
            $data['column_total'] = $this->language->get('column_total');

            $this->load->model('tool/upload');

            $data['products'] = array();

            foreach ($this->cart->getProducts() as $product) {
                $option_data = array();

                foreach ($product['option'] as $option) {
                    if ($option['type'] != 'file') {
                        $value = $option['value'];
                    } else {
                        $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                        if ($upload_info) {
                            $value = $upload_info['name'];
                        } else {
                            $value = '';
                        }
                    }

                    $option_data[] = array(
                        'name' => $option['name'],
                        'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
                    );
                }

                $recurring = '';

                if ($product['recurring']) {
                    $frequencies = array(
                        'day' => $this->language->get('text_day'),
                        'week' => $this->language->get('text_week'),
                        'semi_month' => $this->language->get('text_semi_month'),
                        'month' => $this->language->get('text_month'),
                        'year' => $this->language->get('text_year'),
                    );

                    if ($product['recurring']['trial']) {
                        $recurring = sprintf($this->language->get('text_trial_description'), $this->currency->format($this->tax->calculate($product['recurring']['trial_price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['trial_cycle'], $frequencies[$product['recurring']['trial_frequency']], $product['recurring']['trial_duration']) . ' ';
                    }

                    if ($product['recurring']['duration']) {
                        $recurring .= sprintf($this->language->get('text_payment_description'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
                    } else {
                        $recurring .= sprintf($this->language->get('text_payment_cancel'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
                    }
                }

                $data['products'][] = array(
                    'cart_id' => $product['cart_id'],
                    'product_id' => $product['product_id'],
                    'name' => $product['name'],
                    'model' => $product['model'],
                    'option' => $option_data,
                    'recurring' => $recurring,
                    'quantity' => $product['quantity'],
                    'subtract' => $product['subtract'],
                    'price' => $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']),
                    'total' => $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'], $this->session->data['currency']),
                    'href' => $this->url->link('product/product', 'product_id=' . $product['product_id'])
                );
            }

            // Gift Voucher
            $data['vouchers'] = array();

            if (!empty($this->session->data['vouchers'])) {
                foreach ($this->session->data['vouchers'] as $voucher) {
                    $data['vouchers'][] = array(
                        'description' => $voucher['description'],
                        'amount' => $this->currency->format($voucher['amount'], $this->session->data['currency'])
                    );
                }
            }

            $data['totals'] = array();

            foreach ($order_data['totals'] as $total) {
                $data['totals'][] = array(
                    'title' => $total['title'],
                    'text' => $this->currency->format($total['value'], $this->session->data['currency'])
                );
            }

            $data['payment'] = $this->load->controller('extension/payment/' . $this->session->data['payment_method']['code']);
        } else {
            $data['redirect'] = $redirect;
        }


        $this->response->setOutput($this->load->view('checkout/confirm', $data));
    }

    public function autopayment($customer_id, $order_id, $total, $currency_code) {

        include_once(DIR_APPLICATION . '/controller/gateway_tps_xml.php');
        $this->load->model('checkout/order');
        $this->load->model('account/order');
        $this->load->model('setting/setting');
        $order_info = array();
        ///'CAD'
        //echo $this->config->get('globalonepay_secure_status_success'); die;

        switch ($currency_code) {
            case $this->config->get('globalonepay_secure_currency1') :
                $currency = $this->config->get('globalonepay_secure_currency1');
                $terminalId = $this->config->get('globalonepay_secure_terminal1');
                $secret = $this->config->get('globalonepay_secure_secret1');
                break;
            case $this->config->get('globalonepay_secure_currency2') :
                $currency = $this->config->get('globalonepay_secure_currency2');
                $terminalId = $this->config->get('globalonepay_secure_terminal2');
                $secret = $this->config->get('globalonepay_secure_secret2');
                break;
            case $this->config->get('globalonepay_secure_currency3') :
                $currency = $this->config->get('globalonepay_secure_currency3');
                $terminalId = $this->config->get('globalonepay_secure_terminal3');
                $secret = $this->config->get('globalonepay_secure_secret3');
        }

        $testAccount = false;
        if ($this->config->get('globalonepay_secure_test')) {
            $testAccount = true;
        }

        $gateway = 'globalone';

        // Order details //
        $customer_id = $customer_id; //1;
        $order_id = $order_id; //120;
        $amount = $total; //10;
        //get stored cardref details  //
        $refDetails = $this->model_account_order->getPaymentrefDetails($customer_id);
        //echo "<pre>"; print_r($refDetails); die;

        if (!empty($refDetails)) {
            $cardNumber = $refDetails['responseref'];
            $cardType = 'SECURECARD';
            //$email = ($this->config->get('globalonepayhpp_send_receipt') == '1' ? $order_info['email'] : '');
            # These values are specific to the transaction.
            $orderId = 'SHOPPAL' . $order_id;
            # These fields are for AVS (Address Verification Check). This is only appropriate in the UK and the US.
            $cvv = '';
            $email = '';
            $mobileNumber = '';
            $issueNo = '';
            $address1 = '';         # (optional) This is the first line of the cardholders address.
            $address2 = '';         # (optional) This is the second line of the cardholders address.
            $postcode = '';         # (optional) This is the cardholders post code.
            $country = '';          # (optional) This is the cardholders country name.
            $phone = '';            # (optional) This is the cardholders home phone number.
            # eDCC fields. Populate these if you have retreived a rate for the transaction, offered it to the cardholder and they have accepted that rate.
            $cardCurrency = '';     # (optional) This is the three character ISO currency code returned in the rate request.
            $cardAmount = '';       # (optional) This is the foreign currency transaction amount returned in the rate request.
            $conversionRate = '';       # (optional) This is the currency conversion rate returned in the rate request.
            # 3D Secure reference. Only include if you have verified 3D Secure throuugh the GlobalOne MPI and received an MPIREF back.
            $mpiref = '';           # This should be blank unless instructed otherwise by GlobalOne.
            $deviceId = '';         # This should be blank unless instructed otherwise by GlobalOne.

            $autoready = '';        # (optional) (Y/N) Whether or not this transaction should be marked with a status of "ready" as apposed to "pending".
            $multicur = false;      # This should be false unless instructed otherwise by GlobalOne.

            $description = '';      # (optional) This can is a description for the transaction that will be available in the merchant notification e-mail and in the SelfCare  system.
            $autoReady = '';        # (optional) Y or N. Automatically set the transaction to a status of Ready in the batch. If not present the terminal default will be used.
            $isMailOrder = false;   #If true the transaction will be processed as a Mail Order transaction. This is only for use with Mail Order enabled Terminal IDs.
            # Set up the authorisation object

            $auth = new XmlAuthRequest($terminalId, $orderId, $currency, $amount, $cardNumber, $cardType);
            if ($cardType != "SECURECARD")
                $auth->SetNonSecureCardCardInfo($cardExpiry, $cardHolderName);
            if ($cvv != "")
                $auth->SetCvv($cvv);
            if ($cardCurrency != "" && $cardAmount != "" && $conversionRate != "")
                $auth->SetForeignCurrencyInformation($cardCurrency, $cardAmount, $conversionRate);
            if ($email != "")
                $auth->SetEmail($email);
            if ($mobileNumber != "")
                $auth->SetMobileNumber($mobileNumber);
            if ($description != "")
                $auth->SetDescription($description);

            if ($issueNo != "")
                $auth->SetIssueNo($issueNo);
            if ($address1 != "" && $address2 != "" && $postcode != "")
                $auth->SetAvs($address1, $address2, $postcode);
            if ($country != "")
                $auth->SetCountry($country);
            if ($phone != "")
                $auth->SetPhone($phone);

            if ($mpiref != "")
                $auth->SetMpiRef($mpiref);
            if ($deviceId != "")
                $auth->SetDeviceId($deviceId);

            if ($multicur)
                $auth->SetMultiCur();
            if ($autoready)
                $auth->SetAutoReady($autoready);
            if ($isMailOrder)
                $auth->SetMotoTrans();

            # Perform the online authorisation and read in the result
            $response = $auth->ProcessRequestToGateway($secret, $testAccount, $gateway);


            $expectedResponseHash = md5($terminalId . $response->UniqueRef() . ($multicur == true ? $currency : '') . $amount . $response->DateTime() . $response->ResponseCode() . $response->ResponseText() . $secret);

            if ($response->IsError()) {
                //echo  'AN ERROR OCCURED! You transaction was not processed. Error details: ' . $response->ErrorString();
                return false;
            } else {
                switch ($response->ResponseCode()) {
                    case "A" :  # -- If using local database, update order as Authorised.
                        //echo 'Payment Processed successfully. Thanks you for your order.';
                        $this->model_checkout_order->addOrderHistory($order_id, $this->config->get('globalonepay_secure_status_success'), $response->ResponseText(), FALSE);
                        return true;

                        break;
                    case "R" :
                    case "D" :
                    case "C" :
                    case "S" :

                    default :  # -- If using local database, update order as declined/failed --
                        // echo  'PAYMENT DECLINED! Please try again with another card. Bank response: ' . $response->ResponseText();
                        $this->model_checkout_order->addOrderHistory($order_id, $this->config->get('globalonepay_secure_status_declined'), $response->ResponseText(), FALSE);

                        return false;
                }

                $request_data = $auth->getRequestInfo();
                $request_data['customer_id'] = $customer_id;
                $request_data['order_id'] = $order_id;
                $request_data['order_type'] = 'monthly';
                $this->saveGlobaloneXmldata($request_data, $response);
            }
        } else {

            return false;
            //die('no Reference details for payment');
        }

        exit;
    }

    public function saveGlobaloneXmldata($request_data, $response) {

        $globalone_request_id = $this->model_checkout_order->saveGlobaloneXmlRequest($request_data);
        if ($globalone_request_id) {
            $response_data = array(
                'globalone_request_id' => $globalone_request_id,
                'isError' => $response->IsError(),
                'errorString' => $response->ErrorString(),
                'responseCode' => $response->ResponseCode(),
                'responseText' => $response->ResponseText(),
                'approvalCode' => $response->ApprovalCode(),
                'authorizedAmount' => $response->AuthorizedAmount(),
                'dateTime' => $response->DateTime(),
                'avsResponse' => $response->AvsResponse(),
                'cvvResponse' => $response->CvvResponse(),
                'uniqueRef' => $response->UniqueRef(),
                'hash' => $response->Hash(),
            );
            $this->model_checkout_order->saveGlobaloneXmlResponse($response_data);
        }
    }

}
