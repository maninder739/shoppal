<?php
class ControllerCheckoutOrderType extends Controller {

	public function index() {
		
		$this->load->language('checkout/checkout');

		$data['monthly_autoship'] = $this->language->get('text_monthly_autoship');
		$data['one_time_order'] = $this->language->get('text_one_time_order');
		
		$data['text_loading'] = $this->language->get('text_loading');
		$data['button_continue'] = $this->language->get('button_continue');

		$this->response->setOutput($this->load->view('checkout/order_type', $data));

	}

	public function save() {

		$this->load->language('checkout/checkout');
		$this->response->addHeader('Content-Type: application/json');
		
		$json = array();

		if (isset($this->request->post['order_type']) && $this->request->post['order_type'] != '' ) {
			$this->session->data['order_type'] = $this->request->post['order_type'];
		}
		else {
			$json['error']['warning'] = '';
		} 
		 
		$this->response->setOutput(json_encode($json));

	}
}
