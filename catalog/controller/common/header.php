<?php

class ControllerCommonHeader extends Controller {

    public function index() {

        $data['text_logout'] = '';
        $data['firstname'] = '';
        $this->load->language('affiliate/common');
        $this->load->model('affiliate/affiliate');
        if ($this->affiliate->isLogged()) {

            $data['text_logout'] = $this->language->get('text_logout');
            $data['firstname'] = $this->affiliate->getFirstName();
            $data['logged_in'] = 'logged';
        }

        //echo $_SERVER['REMOTE_ADDR']; die;
        // By default cookie

        $data['custom_logo'] = '';
        $data['mission'] = '';


        if (isset($_COOKIE['affiliate_info'])) {
            $affiliate = unserialize($_COOKIE['affiliate_info']);
            $data['custom_logo'] = $affiliate['logo'];
            $data['mission'] = $affiliate['mission'];
        }



        if (isset($this->request->get['route']) && $this->request->get['route'] == 'common/home' && isset($this->request->get['_route_']) || isset($this->request->cookie['tracking'])) {

            $data['custom_logo'] = '';
            $data['mission'] = '';

            //$app =  'not-approved';
            //cb added this sept 29

            if (isset($this->request->get['_route_'])) {
                $tracking = $this->request->get['_route_'];
                if ($tracking == 'maxley') {
                    $check_tracking = $this->model_affiliate_affiliate->getAffiliateByCode('ScottFerguson');
                } else if ($tracking == 'brandonboxingclub') {
                    $check_tracking = $this->model_affiliate_affiliate->getAffiliateByCode('Brandonbc');
                } elseif (strstr($tracking, 'initiatives')) {
                    $check_tracking = $this->model_affiliate_affiliate->getAffiliateByCode('initiatives');
                } else {
                    $check_tracking = $this->model_affiliate_affiliate->getAffiliateByCode($tracking);
                }


                if ($check_tracking) {
                    $tracking = $check_tracking['code'];
                } else {
                    $tracking = $this->request->cookie['tracking'];
                }
            } else {
                $tracking = $this->request->cookie['tracking'];
            }


            $affiliate_info = $this->model_affiliate_affiliate->getAffiliateByCode($tracking);
            $custom_logo = '';
            $mission = '';

            if ($affiliate_info && $affiliate_info['approved'] && in_array($affiliate_info['member_type'], array(1, 2, 3))) {

                //$app =  'approved';

                if ($affiliate_info['member_type'] == '2') {
                    //setcookie("tracking", "", time() - 3600);

                    if (isset($affiliate_info['code'])) {

                        setcookie('tracking', $affiliate_info['code'], time() + 3600 * 24 * 1000, '/');
                        $custom_logo = $affiliate_info['custom_logo'];

                        $language = $this->language->get('code');

                        if ($language == 'en') {
                            $mission = $this->language->get('text_mission') . ' Contact ' . $affiliate_info['firstname'] . ' at ' . $affiliate_info['telephone'] . '.';
                        } else {
                            $mission = $this->language->get('text_mission') . ' Contact ' . $affiliate_info['firstname'] . ' à ' . $affiliate_info['telephone'] . '.';
                        }

                        $data['custom_logo'] = $custom_logo;
                        $data['mission'] = $mission;
                    }
                } else {

                    $custom_logo = $affiliate_info['custom_logo'];
                    $language = $this->language->get('code');
                    if ($language == 'en') {
                        $mission = $affiliate_info['mission'];
                    } else {
                        $mission = $affiliate_info['mission_french'];
                    }

                    if (isset($affiliate_info['code'])) {
                        setcookie('tracking', $affiliate_info['code'], time() + 3600 * 24 * 1000, '/');

                        setcookie('affiliate_info', serialize(array(
                            'mission' => $mission,
                            'logo' => $custom_logo
                                )), time() + 3600 * 24 * 1000, '/');


                        $custom_logo = $affiliate_info['custom_logo'];
                        $language = $this->language->get('code');
                        if ($language == 'en') {
                            $mission = $affiliate_info['mission'];
                        } else {
                            $mission = $affiliate_info['mission_french'];
                        }

                        $data['custom_logo'] = $custom_logo;
                        $data['mission'] = $mission;
                    }
                }
            }
        }
        //echo 'tracking--'.$this->request->cookie['tracking'];

        /* Affilate data by Tracking Code */

        // Analytics
        $this->load->model('extension/extension');

        $data['analytics'] = array();

        $analytics = $this->model_extension_extension->getExtensions('analytics');

        foreach ($analytics as $analytic) {
            if ($this->config->get($analytic['code'] . '_status')) {
                $data['analytics'][] = $this->load->controller('extension/analytics/' . $analytic['code'], $this->config->get($analytic['code'] . '_status'));
            }
        }

        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }

        // CB replace 'image/' with 'pics/'
        if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
            $this->document->addLink($server . 'pics/' . $this->config->get('config_icon'), 'icon');
        }

        $corporate = 'Corporate';
        if (!empty($this->request->cookie['tracking'])) {
            $data['member_name'] = $this->request->cookie['tracking'];
        } else {
            $data['member_name'] = $corporate;
        }

        //$data['title'] = $this->document->getTitle();
        $data['title'] = $this->config->get('config_name');
        $data['member_type'] = $affiliate_info['member_type'];
        $data['base'] = $server;
        // $data['description'] = $this->document->getDescription();
        // $data['keywords']    = $this->document->getKeywords();
        $data['description'] = $this->config->get('config_meta_description');
        ;
        $data['keywords'] = $this->config->get('config_meta_keyword');
        $data['links'] = $this->document->getLinks();
        $data['styles'] = $this->document->getStyles();
        $data['scripts'] = $this->document->getScripts();
        $data['lang'] = $this->language->get('code');
        $data['direction'] = $this->language->get('direction');

        $data['name'] = $this->config->get('config_name');
        // CB replace 'image/' with 'pics/'
        if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
            $data['logo'] = $server . 'pics/' . $this->config->get('config_logo');
        } else {
            $data['logo'] = $server . 'pics/';
        }

        $this->load->language('common/header');

        $data['text_home'] = $this->language->get('text_home');

        // new field
        $data['text_mission'] = $this->language->get('text_mission');

        if ($data['mission']) {
            $data['text_mission'] = $data['mission'];
        }

        // new field
        // Wishlist
        if ($this->customer->isLogged()) {
            $this->load->model('account/wishlist');

            $data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
        } else {
            $data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
        }

        $data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
        $data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', true), $this->customer->getFirstName(), $this->url->link('account/logout', '', true));

        $data['text_account'] = $this->language->get('text_account');
        $data['text_register'] = $this->language->get('text_register');
        $data['text_login'] = $this->language->get('text_login');
        $data['text_order'] = $this->language->get('text_order');
        $data['text_transaction'] = $this->language->get('text_transaction');
        $data['text_download'] = $this->language->get('text_download');
        $data['text_logout'] = $this->language->get('text_logout');
        $data['text_checkout'] = $this->language->get('text_checkout');
        $data['text_category'] = $this->language->get('text_category');
        $data['text_all'] = $this->language->get('text_all');
        $data['share_cart'] = $this->language->get('share_cart');
        $data['get_link'] = $this->language->get('get_link');

        $data['home'] = $this->url->link('common/home');
        $data['wishlist'] = $this->url->link('account/wishlist', '', true);
        $data['logged'] = $this->customer->isLogged();
        $data['user_email'] = $this->customer->getEmail();
        $data['account'] = $this->url->link('account/account', '', true);
        $data['register'] = $this->url->link('account/register', '', true);
        $data['login'] = $this->url->link('account/login', '', true);
        $data['order'] = $this->url->link('account/order', '', true);
        $data['transaction'] = $this->url->link('account/transaction', '', true);
        $data['download'] = $this->url->link('account/download', '', true);
        $data['logout'] = $this->url->link('account/logout', '', true);
        $data['shopping_cart'] = $this->url->link('checkout/cart');
        $data['checkout'] = $this->url->link('checkout/checkout', '', true);
        $data['contact'] = $this->url->link('information/contact');
        $data['telephone'] = $this->config->get('config_telephone');

        // Menu
        $this->load->model('catalog/category');

        $this->load->model('catalog/product');

        $data['categories'] = array();

        $categories = $this->model_catalog_category->getCategories(0);

        foreach ($categories as $category) {
            if ($category['top']) {
                // Level 2
                $children_data = array();

                $children = $this->model_catalog_category->getCategories($category['category_id']);

                foreach ($children as $child) {
                    $filter_data = array(
                        'filter_category_id' => $child['category_id'],
                        'filter_sub_category' => true
                    );

                    $children_data[] = array(
                        'name' => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
                        'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
                    );
                }

                // Level 1
                $data['categories'][] = array(
                    'name' => $category['name'],
                    'children' => $children_data,
                    'column' => $category['column'] ? $category['column'] : 1,
                    'href' => $this->url->link('product/category', 'path=' . $category['category_id'])
                );
            }
        }

        $data['language'] = $this->load->controller('common/language');
        $data['currency'] = $this->load->controller('common/currency');
        $data['search'] = $this->load->controller('common/search');
        $data['cart'] = $this->load->controller('common/cart');

        // For page specific css
        if (isset($this->request->get['route'])) {
            if (isset($this->request->get['product_id'])) {
                $class = '-' . $this->request->get['product_id'];
            } elseif (isset($this->request->get['path'])) {
                $class = '-' . $this->request->get['path'];
            } elseif (isset($this->request->get['manufacturer_id'])) {
                $class = '-' . $this->request->get['manufacturer_id'];
            } elseif (isset($this->request->get['information_id'])) {
                $class = '-' . $this->request->get['information_id'];
            } else {
                $class = '';
            }

            $data['class'] = str_replace('/', '-', $this->request->get['route']) . $class;
        } else {
            $data['class'] = 'common-home';
        }

        return $this->load->view('common/header', $data);
    }

}
