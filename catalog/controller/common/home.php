<?php

class ControllerCommonHome extends Controller {

    public function index() {
        $this->document->setTitle($this->config->get('config_meta_title'));
        $this->document->setDescription($this->config->get('config_meta_description'));
        $this->document->setKeywords($this->config->get('config_meta_keyword'));
        $this->load->language('common/home');

        if (isset($this->request->get['route'])) {
            $this->document->addLink($this->config->get('config_url') . $_COOKIE['tracking'], 'canonical');
        }

     
        $corporate = 'Corporate';
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->load->language('affiliate/common');
        $this->load->model('affiliate/affiliate');

        /* Affilate data by Tracking Code */
        if (isset($this->request->get['route']) && $this->request->get['route'] == 'common/home' && isset($this->request->get['_route_']) || isset($this->request->cookie['tracking'])) {
            $data['custom_logo'] = '';
            $data['mission'] = '';

            if (isset($this->request->cookie['tracking'])) {
                $tracking = $this->request->cookie['tracking'];
                if ($tracking == 'maxley') {
                    $check_tracking = $this->model_affiliate_affiliate->getAffiliateByCode('ScottFerguson');
                } else if($tracking == 'brandonboxingclub') {
                    $check_tracking = $this->model_affiliate_affiliate->getAffiliateByCode('Brandonbc');
                } else {
                    $check_tracking = $this->model_affiliate_affiliate->getAffiliateByCode($tracking);
                }

                if ($check_tracking) {
                    $tracking = $check_tracking['code'];
                } else {
                    $tracking = $this->request->cookie['tracking'];
                }
            } else {
                $tracking = $this->request->cookie['tracking'];
            }

 

            $affiliate_info = $this->model_affiliate_affiliate->getAffiliateByCode($tracking);
            $custom_title = $affiliate_info['company'];
            $organization = $affiliate_info['company'];
            $custom_logo = $affiliate_info['custom_logo'];
            $data['member_type'] = $affiliate_info['member_type'];

            if ($language == 'en') {
                $short_mission = $affiliate_info['mission'];
            } else {
                $short_mission = $affiliate_info['mission_french'];
            }

            $language = $this->language->get('code');
            $affiliate_fundraisers = $this->model_affiliate_affiliate->getAffiliatefundraisers($affiliate_info['affiliate_id']);

            if ($language == 'en') {
                $mission = $affiliate_fundraisers['organization_detailed'];
            } else {
                $mission = $affiliate_fundraisers['organization_detailed_french'];
            }

            if (!empty($affiliate_fundraisers['member_video'])) {
                $image_video = $affiliate_fundraisers['member_video'];
            } else {
                if (!empty($affiliate_fundraisers['member_image2'])) {
                    $image_video = $affiliate_fundraisers['member_image2'];
                }
            }

            if(!empty($this->request->cookie['tracking'])) {
              $data['member_name'] = $this->request->cookie['tracking'];
            } else{
              $data['member_name'] = $corporate;
            }

            $data['custom_title'] = $custom_title;
            $data['custom_logo'] = $custom_logo;
            $data['organization'] = $organization;
            $data['mission'] = $mission;
            $data['short_mission'] = $short_mission;
            $data['member_image1'] = isset($affiliate_fundraisers['member_image1']) ? $affiliate_fundraisers['member_image1'] : '';
            $data['member_image2'] = isset($affiliate_fundraisers['member_image2']) ? $affiliate_fundraisers['member_image2'] : '';
            $data['member_video'] = isset($affiliate_fundraisers['member_video']) ? ($affiliate_fundraisers['member_video']) : '';
            // $data['action_tracking'] = '/shop-for-a-cause&tracking='.$tracking; 

            $data['REQUEST_URI'] = $this->request->server['REQUEST_URI'];
            if (strtolower($tracking) == 'cheerstrong') {
                $data['action_tracking'] = '/shop-for-a-cause&tracking=' . $tracking . '#/rain-cosmetics-m16';
            } else {
                $data['action_tracking'] = '/shop-for-a-cause&tracking=' . $tracking;
            }

            $data['lang'] = $this->language->get('code');
            $data['text_how_it_works'] = $this->language->get('text_how_it_works');
            $data['text_how_it_works1'] = $this->language->get('text_how_it_works1');
            $data['text_how_it_works2'] = $this->language->get('text_how_it_works2');
            $data['text_how_it_works3'] = $this->language->get('text_how_it_works3');
            $data['text_show_support'] = $this->language->get('text_show_support');
            $data['text_show_support1'] = $this->language->get('text_show_support1');
            $data['text_want_to_support'] = $this->language->get('text_want_to_support');
            $data['text_want_to_support_text'] = $this->language->get('text_want_to_support_text');
            $data['cancel_order_text'] = $this->language->get('cancel_order_text');

            $browser_lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
            $acceptLang = ['fr', 'en'];
            $data['browser_lang'] = in_array($lang, $acceptLang) ? $lang : 'en';
        }

        if ($check_tracking['approved'] == 1 || $check_tracking['member_type'] != '1') {
            $this->response->setOutput($this->load->view('common/home', $data));
        } else {
            $data = array();
            $this->load->language('error/not_found');

            $this->document->setTitle($this->language->get('heading_title'));

            $data['heading_title'] = $this->language->get('heading_title');

            $data['text_not_found'] = $this->language->get('text_not_found');

            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('error/not_found', 'token=' . $this->session->data['token'], true)
            );

            $data['header'] = $this->load->controller('common/header');
            $data['footer'] = $this->load->controller('common/footer');
            $data['link1'] = $this->language->get('link1');
            $data['text_error'] = $this->language->get('text_error');
            $data['link2'] = $this->language->get('link2');
            $this->response->setOutput($this->load->view('error/not_found', $data));
        }
    }

}
