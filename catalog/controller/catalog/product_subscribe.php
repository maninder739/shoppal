<?php
class ControllerCatalogProductSubscribe extends Controller {

	public function index() {
		$this->load->language('catalog/product_subscribe');
		
		$this->document->setTitle($this->language->get('heading_title_change')); 
		
		$this->load->model('catalog/product_subscribe');

		$data['breadcrumbs'] = array();
		
		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home')
		);
		
		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title_change'),
			'href'      => $this->url->link('catalog/product_subscribe', '', true)
		);
		
		$data['heading_title'] = $this->language->get('heading_title_change');
		
		$data['column_name'] = $this->language->get('column_name');
		$data['column_model'] = $this->language->get('column_model');
		$data['column_price'] = $this->language->get('column_price');
		$data['column_description'] = $this->language->get('column_description');
		$data['column_effective_date'] = $this->language->get('column_effective_date');
		
		$data['button_checkout'] = $this->language->get('button_checkout');

		$data['checkout'] = $this->url->link('checkout/checkout', '', true);

		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		
		$data['header'] = $this->load->controller('common/header');
		$data['footer'] = $this->load->controller('common/footer');
		
		if (!$this->cart->hasStock() && (!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning'))) {
			$data['error_warning'] = $this->language->get('error_stock');
		} elseif (isset($this->session->data['error'])) {
			$data['error_warning'] = $this->session->data['error'];
			unset($this->session->data['error']);
		} else {
			$data['error_warning'] = '';
		}

		if ($this->config->get('config_customer_price') && !$this->customer->isLogged()) {
			$data['attention'] = sprintf($this->language->get('text_login'), $this->url->link('account/login'), $this->url->link('account/register'));
		} else {
			$data['attention'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$order_subscription_info = $this->model_catalog_product_subscribe->getOrderSubscriptionInfo($this->session->data['order_subscription_id']);

		foreach ($this->cart->getProducts() as $product) {
			$data['order_subscription_id'] = $this->session->data['order_subscription_id'];
			
			$data['product_id'] = $product['product_id'];
			
			$data['name'] = $product['name'];
			
			$data['model'] = $product['model'];
			
			$data['subscription_id'] = $product['subscription_id'];
			
			$data['effective_date'] = date($this->language->get('date_format_short'), $order_subscription_info['next_due']);
			
			if ($product['subscription_price'] > 0) {
				if ($product['include_taxes']) {
					$prod_price = $this->currency->format($this->tax->calculate($product['subscription_price'], $product['tax_class_id'], 1) * $product['quantity'], $this->session->data['currency'], false, false, false);
				} else {
					$prod_price = $this->currency->format($product['subscription_price'] * $product['quantity'], $this->session->data['currency'], false, false, false);
				}
			} else {
				if ($product['include_taxes']) {
					$prod_price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], 1) * $product['quantity'], $this->session->data['currency'], false, false, false);
				} else {
					$prod_price = $this->currency->format($product['price'] * $product['quantity'], $this->session->data['currency'], false, false, false);
				}
			}
			
			if ($product['include_shipping'] && isset($this->session->data['shipping_method'])) {
				if ($product['include_taxes'] && (isset($this->session->data['shipping_method']['tax_class_id']) && $this->session->data['shipping_method']['tax_class_id'])) {
					$shipping_amt = $this->currency->format($this->tax->calculate($this->session->data['shipping_method']['cost'], $this->session->data['shipping_method']['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], false, false, false);
				} else {
					$shipping_amt = $this->currency->format($this->session->data['shipping_method']['cost'], $this->session->data['currency'], false, false, false);
				}
				$prod_price += $shipping_amt;
			}
			
			$data['price'] = $this->currency->format(($prod_price), $this->session->data['currency']);
			
			if ($product['subscription_duration']) {
				if ($product['subscription_specific_day']) {
					$data['description'] = sprintf($this->language->get('text_subscription_duration_sd'), $product['subscription_cycle'], $this->language->get('text_' . $product['subscription_frequency']), $this->language->get('text_' . $product['subscription_specific_day']), $prod_price, $product['subscription_duration']);
				} else {
					$data['description'] = sprintf($this->language->get('text_subscription_duration'), $product['subscription_cycle'], $this->language->get('text_' . $product['subscription_frequency']), $prod_price, $product['subscription_duration']);
				}
			} else {
				if ($product['subscription_specific_day']) {
					$data['description'] = sprintf($this->language->get('text_subscription_cancel_sd'), $product['subscription_cycle'], $this->language->get('text_' . $product['subscription_frequency']), $this->language->get('text_' . $product['subscription_specific_day']), $prod_price);
				} else {
					$data['description'] = sprintf($this->language->get('text_subscription_cancel'), $product['subscription_cycle'], $this->language->get('text_' . $product['subscription_frequency']), $prod_price);
				}
			}
		}

		// Totals
		$this->load->model('extension/extension');

		$totals = array();
		$taxes = $this->cart->getTaxes();
		$total = 0;
		
		// Because __call can not keep var references so we put them into an array. 			
		$total_data = array(
			'totals' => &$totals,
			'taxes'  => &$taxes,
			'total'  => &$total
		);
		
		// Display prices
		if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
		$sort_order = array();

		$results = $this->model_extension_extension->getExtensions('total');

			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
			}

			array_multisort($sort_order, SORT_ASC, $results);

			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('extension/total/' . $result['code']);
					
					// We have to put the totals in an array so that they pass by reference.
					$this->{'model_extension_total_' . $result['code']}->getTotal($total_data, $total, $totals, $taxes);
				}
			}

			$sort_order = array();

			foreach ($totals as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $total_data);
		}

		$data['totals'] = array();

		foreach ($totals as $total) {
			$data['totals'][] = array(
				'title' => $total['title'],
				'text'  => $this->currency->format($total['value'], $this->session->data['currency'])
			);
		}
		
		$this->response->setOutput($this->load->view($this->config->get('config_template') . '/catalog/product_subscribe', $data));
		
  	}

}
