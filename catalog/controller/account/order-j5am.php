<?php

class ControllerAccountOrder extends Controller {

    public function index() {
        if (!$this->customer->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('account/order', '', true);

            $this->response->redirect($this->url->link('account/login', '', true));
        }

        $this->load->language('account/order');
 
        $this->document->setTitle($this->language->get('heading_title'));

        $url = '';

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_account'),
            'href' => $this->url->link('account/account', '', true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('account/order', $url, true)
        );

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_empty'] = $this->language->get('text_empty');

        $data['column_order_id'] = $this->language->get('column_order_id');
        $data['order_type'] = $this->language->get('order_type');

        $data['column_customer'] = $this->language->get('column_customer');
        $data['column_product'] = $this->language->get('column_product');
        $data['column_total'] = $this->language->get('column_total');
        $data['column_status'] = $this->language->get('column_status');
        $data['column_date_added'] = $this->language->get('column_date_added');

        $data['button_view'] = $this->language->get('button_view');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_continue'] = $this->language->get('button_continue');

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $data['orders'] = array();

        $this->load->model('account/order');

        $order_total = $this->model_account_order->getTotalOrders();

        $results = $this->model_account_order->getOrders(($page - 1) * 10, 10);

        foreach ($results as $result) {
            $product_total = $this->model_account_order->getTotalOrderProductsByOrderId($result['order_id']);
            $voucher_total = $this->model_account_order->getTotalOrderVouchersByOrderId($result['order_id']);

            $data['orders'][] = array(
                'order_id' => $result['order_id'],
                'order_type' => $result['order_type'],
                'order_added' => $result['date_added'],
                'name' => $result['firstname'] . ' ' . $result['lastname'],
                'status' => $result['status'],
                'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
                'products' => ($product_total + $voucher_total),
                'total' => $this->currency->format($result['total'], $result['currency_code'], $result['currency_value']),
                'view' => $this->url->link('account/order/info', 'order_id=' . $result['order_id'], true),
                'edit' => $this->url->link('account/order/edit', 'order_id=' . $result['order_id'], true),
            );
        }

        $pagination = new Pagination();
        $pagination->total = $order_total;
        $pagination->page = $page;
        $pagination->limit = 10;
        $pagination->url = $this->url->link('account/order', 'page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($order_total - 10)) ? $order_total : ((($page - 1) * 10) + 10), $order_total, ceil($order_total / 10));

        $data['continue'] = $this->url->link('account/account', '', true);

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');


        $details = $this->autoship();  
      //  echo "<pre>"; print_r($details); die;

        $this->response->setOutput($this->load->view('account/order_list', $data));
    }

    public function edit() {

        $this->load->language('account/order');
        $data['get_order_type'] = '';
        if (isset($this->request->get['order_id'])) {
            $order_id = $this->request->get['order_id'];
        } else {
            $order_id = 0;
        }

        if (!$this->customer->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('account/order/edit', 'order_id=' . $order_id, true);

            $this->response->redirect($this->url->link('account/login', '', true));
        }

        $this->load->model('account/order');

        $order_info = $this->model_account_order->getOrder($order_id);

        if ($order_info) {
            $this->document->setTitle($this->language->get('text_order_edit'));

            $url = '';

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            $order_type = $order_info['order_type'];
            if ($order_type == 'monthly') {
                $order_type = 'Monthly Order';
            } else if ($order_type == 'one time') {
                $order_type = 'One Time Order';
            }
            $data['get_order_type'] = $order_type;
            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/home')
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_account'),
                'href' => $this->url->link('account/account', '', true)
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('account/order', $url, true)
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_order_edit'),
                'href' => $this->url->link('account/order/edit', 'order_id=' . $this->request->get['order_id'] . $url, true)
            );

            $data['heading_title'] = $this->language->get('text_order');
            $data['text_order_detail'] = $this->language->get('text_order_detail');
            $data['text_invoice_no'] = $this->language->get('text_invoice_no');
            $data['text_order_id'] = $this->language->get('text_order_id');
            $data['text_date_added'] = $this->language->get('text_date_added');
            $data['text_shipping_method'] = $this->language->get('text_shipping_method');
            $data['text_shipping_address'] = $this->language->get('text_shipping_address');
            $data['text_payment_method'] = $this->language->get('text_payment_method');
            $data['text_payment_address'] = $this->language->get('text_payment_address');
            $data['org_name'] = $this->language->get('org_name');
            $data['text_history'] = $this->language->get('text_history');
            $data['text_comment'] = $this->language->get('text_comment');
            $data['text_no_results'] = $this->language->get('text_no_results');

            $data['text_product'] = $this->language->get('text_product');
            $data['entry_quantity'] = $this->language->get('entry_quantity');
            $data['entry_product'] = $this->language->get('entry_product');

            $data['text_default'] = $this->language->get('text_default');
            $data['text_select'] = $this->language->get('text_select');

            $data['add_edit_product'] = $this->language->get('add_edit_product');
            $data['monthly_order'] = $this->language->get('monthly_order');
            $data['close'] = $this->language->get('close');
            $data['save'] = $this->language->get('save');

            $data['discard_product_list'] = $this->language->get('discard_product_list');
            $data['discard_product_msg'] = $this->language->get('discard_product_msg');


            $data['text_none'] = $this->language->get('text_none');

            $data['column_name'] = $this->language->get('column_name');
            $data['column_model'] = $this->language->get('column_model');
            $data['column_quantity'] = $this->language->get('column_quantity');
            $data['column_price'] = $this->language->get('column_price');
            $data['column_total'] = $this->language->get('column_total');
            $data['column_action'] = $this->language->get('column_action');
            $data['column_date_added'] = $this->language->get('column_date_added');
            $data['column_status'] = $this->language->get('column_status');
            $data['column_comment'] = $this->language->get('column_comment');
            $data['button_return'] = $this->language->get('button_return');
            $data['button_continue'] = $this->language->get('button_continue');
            $data['button_reorder'] = $this->language->get('button_reorder');
            $data['button_edit'] = $this->language->get('button_edit');
            $data['button_delete'] = $this->language->get('button_delete');

            if (isset($this->session->data['error'])) {
                $data['error_warning'] = $this->session->data['error'];

                unset($this->session->data['error']);
            } else {
                $data['error_warning'] = '';
            }

            if (isset($this->session->data['success'])) {
                $data['success'] = $this->session->data['success'];

                unset($this->session->data['success']);
            } else {
                $data['success'] = '';
            }

            if ($order_info['order_id']) {
                $data['invoice_no'] = $order_info['invoice_prefix'] . $order_info['order_id'];
            } else {
                $data['invoice_no'] = '';
            }

            $data['order_id'] = $this->request->get['order_id'];
            $data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));

            if ($order_info['payment_address_format']) {
                $format = $order_info['payment_address_format'];
            } else {
                $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
            }

            $data['company_name'] = $order_info['company_name'];

            $find = array(
                '{firstname}',
                '{lastname}',
                '{company}',
                '{address_1}',
                '{address_2}',
                '{city}',
                '{postcode}',
                '{zone}',
                '{zone_code}',
                '{country}'
            );

            $replace = array(
                'firstname' => $order_info['payment_firstname'],
                'lastname' => $order_info['payment_lastname'],
                'company' => $order_info['payment_company'],
                'address_1' => $order_info['payment_address_1'],
                'address_2' => $order_info['payment_address_2'],
                'city' => $order_info['payment_city'],
                'postcode' => $order_info['payment_postcode'],
                'zone' => $order_info['payment_zone'],
                'zone_code' => $order_info['payment_zone_code'],
                'country' => $order_info['payment_country']
            );

            $data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

            $data['payment_method'] = $order_info['payment_method'];

            if ($order_info['shipping_address_format']) {
                $format = $order_info['shipping_address_format'];
            } else {
                $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
            }

            $find = array(
                '{firstname}',
                '{lastname}',
                '{company}',
                '{address_1}',
                '{address_2}',
                '{city}',
                '{postcode}',
                '{zone}',
                '{zone_code}',
                '{country}'
            );

            $replace = array(
                'firstname' => $order_info['shipping_firstname'],
                'lastname' => $order_info['shipping_lastname'],
                'company' => $order_info['shipping_company'],
                'address_1' => $order_info['shipping_address_1'],
                'address_2' => $order_info['shipping_address_2'],
                'city' => $order_info['shipping_city'],
                'postcode' => $order_info['shipping_postcode'],
                'zone' => $order_info['shipping_zone'],
                'zone_code' => $order_info['shipping_zone_code'],
                'country' => $order_info['shipping_country']
            );

            $data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

            $data['shipping_method'] = $order_info['shipping_method'];

            $this->load->model('catalog/product');
            $this->load->model('tool/upload');

            // Products
            $data['products'] = array();

            $products = $this->model_account_order->getOrderProducts($this->request->get['order_id']);

            foreach ($products as $product) {
                $option_data = array();

                $options = $this->model_account_order->getOrderOptions($this->request->get['order_id'], $product['order_product_id']);
               

                foreach ($options as $option) {
                    if ($option['type'] != 'file') {
                        $value = $option['value'];
                    } else {
                        $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                        if ($upload_info) {
                            $value = $upload_info['name'];
                        } else {
                            $value = '';
                        }
                    }

                    $option_data[] = array(
                        'name' => $option['name'],
                        'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value),
                        'product_option_id' =>  $option['product_option_id'],
                        'product_option_value_id' =>  $option['product_option_value_id']
                    );
                }

                $product_info = $this->model_catalog_product->getProduct($product['product_id']);

                if ($product_info) {
                    $reorder = $this->url->link('account/order/edit_reorder', 'order_id=' . $order_id . '&order_product_id=' . $product['order_product_id'], true);
                } else {
                    $reorder = '';
                }

                $data['products'][] = array(
                    'order_product_id' => $product['order_product_id'],
                    'product_id' => $product['product_id'],
                    'name' => $product['name'],
                    'model' => $product['model'],
                    'option' => $option_data,
                    'quantity' => $product['quantity'],
                    'price' => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
                    'total' => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
                    'reorder' => $reorder,
                    'return' => $this->url->link('account/return/add', 'order_id=' . $order_info['order_id'] . '&product_id=' . $product['product_id'], true)
                );
            }

            // Voucher
            $data['vouchers'] = array();

            $vouchers = $this->model_account_order->getOrderVouchers($this->request->get['order_id']);

            foreach ($vouchers as $voucher) {
                $data['vouchers'][] = array(
                    'description' => $voucher['description'],
                    'amount' => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
                );
            }

            // Totals
            $data['totals'] = array();

            $totals = $this->model_account_order->getOrderTotals($this->request->get['order_id']);

            foreach ($totals as $total) {
                $data['totals'][] = array(
                    'title' => $total['title'],
                    'text' => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
                );
            }

            $data['comment'] = nl2br($order_info['comment']);

            // History
            $data['histories'] = array();

            $results = $this->model_account_order->getOrderHistories($this->request->get['order_id']);

            foreach ($results as $result) {
                $data['histories'][] = array(
                    'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
                    'status' => $result['status'],
                    'comment' => $result['notify'] ? nl2br($result['comment']) : ''
                );
            }

            $data['continue'] = $this->url->link('account/order', '', true);

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            $this->response->setOutput($this->load->view('account/order_edit', $data));
        } else {
            $this->document->setTitle($this->language->get('text_order'));

            $data['heading_title'] = $this->language->get('text_order');

            $data['text_error'] = $this->language->get('text_error');

            $data['button_continue'] = $this->language->get('button_continue');

            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/home')
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_account'),
                'href' => $this->url->link('account/account', '', true) 
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('account/order', '', true)
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_order'),
                'href' => $this->url->link('account/order/edit', 'order_id=' . $order_id, true)
            );

            $data['continue'] = $this->url->link('account/order', '', true);

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            $this->response->setOutput($this->load->view('error/not_found', $data));
        }
    }

    public function info() {
        $this->load->language('account/order');
        $data['get_order_type'] = '';
        if (isset($this->request->get['order_id'])) {
            $order_id = $this->request->get['order_id'];
        } else {
            $order_id = 0;
        }

        if (!$this->customer->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('account/order/info', 'order_id=' . $order_id, true);

            $this->response->redirect($this->url->link('account/login', '', true));
        }

        $this->load->model('account/order');

        $order_info = $this->model_account_order->getOrder($order_id);

        if ($order_info) {
            $this->document->setTitle($this->language->get('text_order'));

            $url = '';

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            $order_type = $order_info['order_type'];
            if ($order_type == 'monthly') {
                $order_type = 'Monthly Order';
            } else if ($order_type == 'one time') {
                $order_type = 'One Time Order';
            }
            $data['get_order_type'] = $order_type;

            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/home')
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_account'),
                'href' => $this->url->link('account/account', '', true)
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('account/order', $url, true)
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_order'),
                'href' => $this->url->link('account/order/info', 'order_id=' . $this->request->get['order_id'] . $url, true)
            );

            $data['heading_title'] = $this->language->get('text_order');
            $data['org_name'] = $this->language->get('org_name');
            $data['text_order_detail'] = $this->language->get('text_order_detail');
            $data['text_invoice_no'] = $this->language->get('text_invoice_no');
            $data['text_order_id'] = $this->language->get('text_order_id');
            $data['text_date_added'] = $this->language->get('text_date_added');
            $data['text_shipping_method'] = $this->language->get('text_shipping_method');
            $data['text_shipping_address'] = $this->language->get('text_shipping_address');
            $data['text_payment_method'] = $this->language->get('text_payment_method');
            $data['text_payment_address'] = $this->language->get('text_payment_address');
            $data['text_history'] = $this->language->get('text_history');
            $data['text_comment'] = $this->language->get('text_comment');
            $data['text_no_results'] = $this->language->get('text_no_results');

            $data['column_name'] = $this->language->get('column_name');
            $data['column_model'] = $this->language->get('column_model');
            $data['column_quantity'] = $this->language->get('column_quantity');
            $data['column_price'] = $this->language->get('column_price');
            $data['column_total'] = $this->language->get('column_total');
            $data['column_action'] = $this->language->get('column_action');
            $data['column_date_added'] = $this->language->get('column_date_added');
            $data['column_status'] = $this->language->get('column_status');
            $data['column_comment'] = $this->language->get('column_comment');
            $data['monthly_order'] = $this->language->get('monthly_order');

            $data['button_reorder'] = $this->language->get('button_reorder');
            $data['button_return'] = $this->language->get('button_return');
            $data['button_continue'] = $this->language->get('button_continue');

            if (isset($this->session->data['error'])) {
                $data['error_warning'] = $this->session->data['error'];

                unset($this->session->data['error']);
            } else {
                $data['error_warning'] = '';
            }

            if (isset($this->session->data['success'])) {
                $data['success'] = $this->session->data['success'];

                unset($this->session->data['success']);
            } else {
                $data['success'] = '';
            }

            if ($order_info['order_id']) {
                $data['invoice_no'] = $order_info['invoice_prefix'] . $order_info['order_id'];
            } else {
                $data['invoice_no'] = '';
            }

            $data['order_id'] = $this->request->get['order_id'];
            $data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));

            if ($order_info['payment_address_format']) {
                $format = $order_info['payment_address_format'];
            } else {
                $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
            }

            $data['company_name'] = $order_info['company_name'];

            $find = array(
                '{firstname}',
                '{lastname}',
                '{company}',
                '{address_1}',
                '{address_2}',
                '{city}',
                '{postcode}',
                '{zone}',
                '{zone_code}',
                '{country}'
            );

            $replace = array(
                'firstname' => $order_info['payment_firstname'],
                'lastname' => $order_info['payment_lastname'],
                'company' => $order_info['payment_company'],
                'address_1' => $order_info['payment_address_1'],
                'address_2' => $order_info['payment_address_2'],
                'city' => $order_info['payment_city'],
                'postcode' => $order_info['payment_postcode'],
                'zone' => $order_info['payment_zone'],
                'zone_code' => $order_info['payment_zone_code'],
                'country' => $order_info['payment_country']
            );

            $data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

            $data['payment_method'] = $order_info['payment_method'];

            if ($order_info['shipping_address_format']) {
                $format = $order_info['shipping_address_format'];
            } else {
                $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
            }

            $find = array(
                '{firstname}',
                '{lastname}',
                '{company}',
                '{address_1}',
                '{address_2}',
                '{city}',
                '{postcode}',
                '{zone}',
                '{zone_code}',
                '{country}'
            );

            $replace = array(
                'firstname' => $order_info['shipping_firstname'],
                'lastname' => $order_info['shipping_lastname'],
                'company' => $order_info['shipping_company'],
                'address_1' => $order_info['shipping_address_1'],
                'address_2' => $order_info['shipping_address_2'],
                'city' => $order_info['shipping_city'],
                'postcode' => $order_info['shipping_postcode'],
                'zone' => $order_info['shipping_zone'],
                'zone_code' => $order_info['shipping_zone_code'],
                'country' => $order_info['shipping_country']
            );

            $data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

            $data['shipping_method'] = $order_info['shipping_method'];

            $this->load->model('catalog/product');
            $this->load->model('tool/upload');

            // Products
            $data['products'] = array();

            $products = $this->model_account_order->getOrderProducts($this->request->get['order_id']);

            foreach ($products as $product) {
                $option_data = array();

                $options = $this->model_account_order->getOrderOptions($this->request->get['order_id'], $product['order_product_id']);

                foreach ($options as $option) {
                    if ($option['type'] != 'file') {
                        $value = $option['value'];
                    } else {
                        $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                        if ($upload_info) {
                            $value = $upload_info['name'];
                        } else {
                            $value = '';
                        }
                    }

                    $option_data[] = array(
                        'name' => $option['name'],
                        'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
                    );
                }

                $product_info = $this->model_catalog_product->getProduct($product['product_id']);

                if ($product_info) {
                    $reorder = $this->url->link('account/order/reorder', 'order_id=' . $order_id . '&order_product_id=' . $product['order_product_id'], true);
                } else {
                    $reorder = '';
                }

                $data['products'][] = array(
                    'name' => $product['name'],
                    'model' => $product['model'],
                    'option' => $option_data,
                    'quantity' => $product['quantity'],
                    'price' => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
                    'total' => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
                    'reorder' => $reorder,
                    'return' => $this->url->link('account/return/add', 'order_id=' . $order_info['order_id'] . '&product_id=' . $product['product_id'], true)
                );
            }

            // Voucher
            $data['vouchers'] = array();

            $vouchers = $this->model_account_order->getOrderVouchers($this->request->get['order_id']);

            foreach ($vouchers as $voucher) {
                $data['vouchers'][] = array(
                    'description' => $voucher['description'],
                    'amount' => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
                );
            }

            // Totals
            $data['totals'] = array();

            $totals = $this->model_account_order->getOrderTotals($this->request->get['order_id']);

            foreach ($totals as $total) {
                $data['totals'][] = array(
                    'title' => $total['title'],
                    'text' => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
                );
            }

            $data['comment'] = nl2br($order_info['comment']);

            // History
            $data['histories'] = array();

            $results = $this->model_account_order->getOrderHistories($this->request->get['order_id']);

            foreach ($results as $result) {
                $data['histories'][] = array(
                    'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
                    'status' => $result['status'],
                    'comment' => $result['notify'] ? nl2br($result['comment']) : ''
                );
            }

            $data['continue'] = $this->url->link('account/order', '', true);

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            $this->response->setOutput($this->load->view('account/order_info', $data));
        } else {
            $this->document->setTitle($this->language->get('text_order'));

            $data['heading_title'] = $this->language->get('text_order');

            $data['text_error'] = $this->language->get('text_error');

            $data['button_continue'] = $this->language->get('button_continue');

            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/home')
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_account'),
                'href' => $this->url->link('account/account', '', true)
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('account/order', '', true)
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_order'),
                'href' => $this->url->link('account/order/info', 'order_id=' . $order_id, true)
            );

            $data['continue'] = $this->url->link('account/order', '', true);

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            $this->response->setOutput($this->load->view('error/not_found', $data)); 
        }
    }

    public function reorder() {
        $this->load->language('account/order');

        if (isset($this->request->get['order_id'])) {
            $order_id = $this->request->get['order_id'];
        } else {
            $order_id = 0;
        }

        $this->load->model('account/order');

        $order_info = $this->model_account_order->getOrder($order_id);

        if ($order_info) {
            if (isset($this->request->get['order_product_id'])) {
                $order_product_id = $this->request->get['order_product_id'];
            } else {
                $order_product_id = 0;
            }

            $order_product_info = $this->model_account_order->getOrderProduct($order_id, $order_product_id);

            if ($order_product_info) {
                $this->load->model('catalog/product');

                $product_info = $this->model_catalog_product->getProduct($order_product_info['product_id']);

                if ($product_info) {
                    $option_data = array();

                    $order_options = $this->model_account_order->getOrderOptions($order_product_info['order_id'], $order_product_id);

                    foreach ($order_options as $order_option) {
                        if ($order_option['type'] == 'select' || $order_option['type'] == 'radio' || $order_option['type'] == 'image') {
                            $option_data[$order_option['product_option_id']] = $order_option['product_option_value_id'];
                        } elseif ($order_option['type'] == 'checkbox') {
                            $option_data[$order_option['product_option_id']][] = $order_option['product_option_value_id'];
                        } elseif ($order_option['type'] == 'text' || $order_option['type'] == 'textarea' || $order_option['type'] == 'date' || $order_option['type'] == 'datetime' || $order_option['type'] == 'time') {
                            $option_data[$order_option['product_option_id']] = $order_option['value'];
                        } elseif ($order_option['type'] == 'file') {
                            $option_data[$order_option['product_option_id']] = $this->encryption->encrypt($order_option['value']);
                        }
                    }

                    $this->cart->add($order_product_info['product_id'], $order_product_info['quantity'], $option_data);

                    $this->session->data['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $product_info['product_id']), $product_info['name'], $this->url->link('checkout/cart'));

                    unset($this->session->data['shipping_method']);
                    unset($this->session->data['shipping_methods']);
                    unset($this->session->data['payment_method']);
                    unset($this->session->data['payment_methods']);
                } else {
                    $this->session->data['error'] = sprintf($this->language->get('error_reorder'), $order_product_info['name']);
                }
            }
        }

        $this->response->redirect($this->url->link('account/order/info', 'order_id=' . $order_id));
    }

    public function edit_reorder() {
        $this->load->language('account/order');

        if (isset($this->request->get['order_id'])) {
            $order_id = $this->request->get['order_id'];
        } else {
            $order_id = 0;
        }

        $this->load->model('account/order');

        $order_info = $this->model_account_order->getOrder($order_id);

        if ($order_info) {
            if (isset($this->request->get['order_product_id'])) {
                $order_product_id = $this->request->get['order_product_id'];
            } else {
                $order_product_id = 0;
            }

            $order_product_info = $this->model_account_order->getOrderProduct($order_id, $order_product_id);

            if ($order_product_info) {
                $this->load->model('catalog/product');

                $product_info = $this->model_catalog_product->getProduct($order_product_info['product_id']);

                if ($product_info) {
                    $option_data = array();

                    $order_options = $this->model_account_order->getOrderOptions($order_product_info['order_id'], $order_product_id);

                    foreach ($order_options as $order_option) {
                        if ($order_option['type'] == 'select' || $order_option['type'] == 'radio' || $order_option['type'] == 'image') {
                            $option_data[$order_option['product_option_id']] = $order_option['product_option_value_id'];
                        } elseif ($order_option['type'] == 'checkbox') {
                            $option_data[$order_option['product_option_id']][] = $order_option['product_option_value_id'];
                        } elseif ($order_option['type'] == 'text' || $order_option['type'] == 'textarea' || $order_option['type'] == 'date' || $order_option['type'] == 'datetime' || $order_option['type'] == 'time') {
                            $option_data[$order_option['product_option_id']] = $order_option['value'];
                        } elseif ($order_option['type'] == 'file') {
                            $option_data[$order_option['product_option_id']] = $this->encryption->encrypt($order_option['value']);
                        }
                    }

                    $this->cart->add($order_product_info['product_id'], $order_product_info['quantity'], $option_data);

                    $this->session->data['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $product_info['product_id']), $product_info['name'], $this->url->link('checkout/cart'));

                    unset($this->session->data['shipping_method']);
                    unset($this->session->data['shipping_methods']);
                    unset($this->session->data['payment_method']);
                    unset($this->session->data['payment_methods']);
                } else {
                    $this->session->data['error'] = sprintf($this->language->get('error_reorder'), $order_product_info['name']);
                }
            }
        }

        $this->response->redirect($this->url->link('account/order/edit', 'order_id=' . $order_id));
    }

    public function autoComplete() {
        $this->load->language('account/order');
        $affiliate_data = array();
        if (isset($this->request->get['filter_name']) || isset($this->request->get['id'])) {

            if (isset($this->request->get['filter_name'])) {
                $filter_name = $this->request->get['filter_name'];
            } else {
                $filter_name = '';
            }

            if (isset($this->request->get['id'])) {
                $filter_id = $this->request->get['id'];
            } else {
                $filter_id = '';
            }

            if (isset($this->request->get['limit'])) {
                $limit = $this->request->get['limit'];
            } else {
                $limit = 5;
            }

            $filter_data = array(
                'filter_name' => $filter_name,
                'filter_id' => $filter_id,
                'start' => 0,
                    //'limit'        => 100
            );

            $this->load->model('account/order');
            $results = $this->model_account_order->getproductsbyid($filter_data);
            //echo "<pre>"; print_r($results); die;

            foreach ($results as $result) {
                $option_data = array();
                $json[] = array(
                    'product_id' => $result['product_id'],
                    'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
                    'model' => $result['model'],
                    'price' => $result['price']
                );
            }


            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }
    }

    public function productlist() {

        $this->load->model('account/order');
        $cat = 195;
        $results = $this->model_account_order->getProductlists($cat);

        foreach ($results as $result) {
            $json[] = array(
                'name' => $result['name'],
                'category_id' => $result['category_id'],
            );
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getProduct() {

        $this->load->model('account/order');
        if (isset($this->request->get['product_id'])) {
            $product_id = $this->request->get['product_id'];
        } else {
            $product_id = '';
        }
        $product_info = $this->model_account_order->getProduct($product_id);

        if ($product_info) {

            $data['options'] = array();
            foreach ($this->model_catalog_product->getProductOptions($this->request->get['product_id']) as $option) {
                $product_option_value_data = array();


                foreach ($option['product_option_value'] as $option_value) {

                    if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {

                        if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float) $option_value['price']) {

                            $price = $this->currency->format($this->tax->calculate($option_value['price'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                        } else {
                            $price = false;
                        }

                        $product_option_value_data[] = array(
                            'product_option_value_id' => $option_value['product_option_value_id'],
                            'option_value_id' => $option_value['option_value_id'],
                            'name' => $option_value['name'],
                            'price' => $price,
                            'price_prefix' => $option_value['price_prefix']
                        );
                    }
                }


                $data['options'][] = array(
                    'product_option_id' => $option['product_option_id'],
                    'product_option_value' => $product_option_value_data,
                    'option_id' => $option['option_id'],
                    'name' => $option['name'],
                    'type' => $option['type'],
                    'value' => $option['value'],
                    'required' => $option['required']
                );
            }

            $data['products'] = array();
            $results = $this->model_account_order->getProductRelated($this->request->get['product_id']);
            foreach ($results as $result) {

                $data['products'][] = array(
                    'product_id' => $result['product_id'],
                    'name' => $result['name'],
                    'model' => $result['model'],
                    'price' => $result['price'],
                );
            }


            //echo "<pre>";  print_r($data); die;
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($data));
        }
    }

    Public function autoship() {

        $this->load->model('account/order');
        $results = array();
        $results = $this->model_account_order->getLastMonthlyOrder();
         
        if(!empty($results)) {
            foreach ($results as $key => $value) {

                 
                    $this->db->query("INSERT INTO `" . DB_PREFIX . "order` SET invoice_prefix = '" . $this->db->escape($value['invoice_prefix']) . "', store_id = '" . (int) $value['store_id'] . "', store_name = '" . $this->db->escape($value['store_name']) . "', store_url = '" . $this->db->escape($value['store_url']) . "', customer_id = '" . (int) $value['customer_id'] . "', customer_group_id = '" . (int) $value['customer_group_id'] . "', firstname = '" . $this->db->escape($value['firstname']) . "', lastname = '" . $this->db->escape($value['lastname']) . "', email = '" . $this->db->escape($value['email']) . "', telephone = '" . $this->db->escape($value['telephone']) . "', fax = '" . $this->db->escape($value['fax']) . "', custom_field = '" . $this->db->escape(isset($value['custom_field']) ? json_encode($value['custom_field']) : '') . "', payment_firstname = '" . $this->db->escape($value['payment_firstname']) . "', payment_lastname = '" . $this->db->escape($value['payment_lastname']) . "', payment_company = '" . $this->db->escape($value['payment_company']) . "', payment_address_1 = '" . $this->db->escape($value['payment_address_1']) . "', payment_address_2 = '" . $this->db->escape($value['payment_address_2']) . "', payment_city = '" . $this->db->escape($value['payment_city']) . "', payment_postcode = '" . $this->db->escape($value['payment_postcode']) . "', payment_country = '" . $this->db->escape($value['payment_country']) . "', payment_country_id = '" . (int) $value['payment_country_id'] . "', payment_zone = '" . $this->db->escape($value['payment_zone']) . "', payment_zone_id = '" . (int) $value['payment_zone_id'] . "', payment_address_format = '" . $this->db->escape($value['payment_address_format']) . "', payment_custom_field = '" . $this->db->escape(isset($value['payment_custom_field']) ? json_encode($value['payment_custom_field']) : '') . "', payment_method = '" . $this->db->escape($value['payment_method']) . "', payment_code = '" . $this->db->escape($value['payment_code']) . "', shipping_firstname = '" . $this->db->escape($value['shipping_firstname']) . "', shipping_lastname = '" . $this->db->escape($value['shipping_lastname']) . "', shipping_company = '" . $this->db->escape($value['shipping_company']) . "', shipping_address_1 = '" . $this->db->escape($value['shipping_address_1']) . "', shipping_address_2 = '" . $this->db->escape($value['shipping_address_2']) . "', shipping_city = '" . $this->db->escape($value['shipping_city']) . "', shipping_postcode = '" . $this->db->escape($value['shipping_postcode']) . "', shipping_country = '" . $this->db->escape($value['shipping_country']) . "', shipping_country_id = '" . (int) $value['shipping_country_id'] . "', shipping_zone = '" . $this->db->escape($value['shipping_zone']) . "', shipping_zone_id = '" . (int) $value['shipping_zone_id'] . "', shipping_address_format = '" . $this->db->escape($value['shipping_address_format']) . "', shipping_custom_field = '" . $this->db->escape(isset($value['shipping_custom_field']) ? json_encode($value['shipping_custom_field']) : '') . "', shipping_method = '" . $this->db->escape($value['shipping_method']) . "', shipping_code = '" . $this->db->escape($value['shipping_code']) . "', comment = '" . $this->db->escape($value['comment']) . "', total = '" . (float) $value['total'] . "', order_status_id  =  '" . (int) 0 . "', affiliate_id = '" . (int) $value['affiliate_id'] . "', commission = '" . (float) $value['commission'] . "', marketing_id = '" . (int) $value['marketing_id'] . "', tracking = '" . $this->db->escape($value['tracking']) . "', language_id = '" . (int) $value['language_id'] . "', currency_id = '" . (int) $value['currency_id'] . "', currency_code = '" . $this->db->escape($value['currency_code']) . "', currency_value = '" . (float) $value['currency_value'] . "', ip = '" . $this->db->escape($value['ip']) . "', forwarded_ip = '" . $this->db->escape($value['forwarded_ip']) . "', user_agent = '" . $this->db->escape($value['user_agent']) . "', accept_language = '" . $this->db->escape($value['accept_language']) . "', date_added = NOW(), date_modified = NOW(), order_type = '" . $this->db->escape($value['order_type']) . "'");
                    $order_id = $this->db->getLastId();

                    $products = $this->model_account_order->getOrderProducts($value['order_id']);
                    foreach ($products as $key => $product) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "order_product SET order_id = '" . (int) $order_id . "', product_id = '" . (int) $product['product_id'] . "', name = '" . $this->db->escape($product['name']) . "', model = '" . $this->db->escape($product['model']) . "', quantity = '" . (int) $product['quantity'] . "', price = '" . (float) $product['price'] . "', total = '" . (float) $product['total'] . "', tax = '" . (float) $product['tax'] . "', reward = '" . (int) $product['reward'] . "'");
                        $order_product_id = $this->db->getLastId();

                        $order_options = $this->model_account_order->getOrderOptions($value['order_id'], $product['order_product_id']);
                        foreach ($order_options as $option) {
                            $this->db->query("INSERT INTO " . DB_PREFIX . "order_option SET order_id = '" . (int) $order_id . "', order_product_id = '" . (int) $order_product_id . "', product_option_id = '" . (int) $option['product_option_id'] . "', product_option_value_id = '" . (int) $option['product_option_value_id'] . "', name = '" . $this->db->escape($option['name']) . "', `value` = '" . $this->db->escape($option['value']) . "', `type` = '" . $this->db->escape($option['type']) . "'");
                        }
                    }

                    $order_vouchers = $this->model_account_order->getOrderVouchers($value['order_id']);
                    foreach ($order_vouchers as $voucher) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "order_voucher SET order_id = '" . (int) $order_id . "', description = '" . $this->db->escape($voucher['description']) . "', code = '" . $this->db->escape($voucher['code']) . "', from_name = '" . $this->db->escape($voucher['from_name']) . "', from_email = '" . $this->db->escape($voucher['from_email']) . "', to_name = '" . $this->db->escape($voucher['to_name']) . "', to_email = '" . $this->db->escape($voucher['to_email']) . "', voucher_theme_id = '" . (int) $voucher['voucher_theme_id'] . "', message = '" . $this->db->escape($voucher['message']) . "', amount = '" . (float) $voucher['amount'] . "'");

                        $order_voucher_id = $this->db->getLastId();

                        $voucher_id = $this->model_extension_total_voucher->addVoucher($value['order_id'], $voucher);

                        $this->db->query("UPDATE " . DB_PREFIX . "order_voucher SET voucher_id = '" . (int) $voucher_id . "' WHERE order_voucher_id = '" . (int) $order_voucher_id . "'");
                    }

                    $order_totals = $this->model_account_order->getOrderTotals($value['order_id']);
                    foreach ($order_totals as $key => $total) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET order_id = '" . (int) $order_id . "', code = '" . $this->db->escape($total['code']) . "', title = '" . $this->db->escape($total['title']) . "', `value` = '" . (float) $total['value'] . "', sort_order = '" . (int) $total['sort_order'] . "'");
                    }

                    $autopaymentdetails = $this->autopayment($value['customer_id'], $order_id, $value['total'], $value['currency_code']);


                    if($autopaymentdetails) {
                        mail('maninder@nascenture.com', 'subject', 'success');
                        echo "success";
                    } else {
                        mail('maninder@nascenture.com', 'subject', 'failed');
                         echo "failed";
                    }
                 
            }
        } else {
           //  mail('maninder@nascenture.com', 'subject', 'foreach empty');
        }
    }


    Public function runCron() {

        $this->load->model('account/order');
        $results = array();
        $results = $this->model_account_order->getWhichisNotRunOrder();
         
        if(!empty($results)) {
            foreach ($results as $key => $value) {

                 
                    $this->db->query("INSERT INTO `" . DB_PREFIX . "order` SET invoice_prefix = '" . $this->db->escape($value['invoice_prefix']) . "', store_id = '" . (int) $value['store_id'] . "', store_name = '" . $this->db->escape($value['store_name']) . "', store_url = '" . $this->db->escape($value['store_url']) . "', customer_id = '" . (int) $value['customer_id'] . "', customer_group_id = '" . (int) $value['customer_group_id'] . "', firstname = '" . $this->db->escape($value['firstname']) . "', lastname = '" . $this->db->escape($value['lastname']) . "', email = '" . $this->db->escape($value['email']) . "', telephone = '" . $this->db->escape($value['telephone']) . "', fax = '" . $this->db->escape($value['fax']) . "', custom_field = '" . $this->db->escape(isset($value['custom_field']) ? json_encode($value['custom_field']) : '') . "', payment_firstname = '" . $this->db->escape($value['payment_firstname']) . "', payment_lastname = '" . $this->db->escape($value['payment_lastname']) . "', payment_company = '" . $this->db->escape($value['payment_company']) . "', payment_address_1 = '" . $this->db->escape($value['payment_address_1']) . "', payment_address_2 = '" . $this->db->escape($value['payment_address_2']) . "', payment_city = '" . $this->db->escape($value['payment_city']) . "', payment_postcode = '" . $this->db->escape($value['payment_postcode']) . "', payment_country = '" . $this->db->escape($value['payment_country']) . "', payment_country_id = '" . (int) $value['payment_country_id'] . "', payment_zone = '" . $this->db->escape($value['payment_zone']) . "', payment_zone_id = '" . (int) $value['payment_zone_id'] . "', payment_address_format = '" . $this->db->escape($value['payment_address_format']) . "', payment_custom_field = '" . $this->db->escape(isset($value['payment_custom_field']) ? json_encode($value['payment_custom_field']) : '') . "', payment_method = '" . $this->db->escape($value['payment_method']) . "', payment_code = '" . $this->db->escape($value['payment_code']) . "', shipping_firstname = '" . $this->db->escape($value['shipping_firstname']) . "', shipping_lastname = '" . $this->db->escape($value['shipping_lastname']) . "', shipping_company = '" . $this->db->escape($value['shipping_company']) . "', shipping_address_1 = '" . $this->db->escape($value['shipping_address_1']) . "', shipping_address_2 = '" . $this->db->escape($value['shipping_address_2']) . "', shipping_city = '" . $this->db->escape($value['shipping_city']) . "', shipping_postcode = '" . $this->db->escape($value['shipping_postcode']) . "', shipping_country = '" . $this->db->escape($value['shipping_country']) . "', shipping_country_id = '" . (int) $value['shipping_country_id'] . "', shipping_zone = '" . $this->db->escape($value['shipping_zone']) . "', shipping_zone_id = '" . (int) $value['shipping_zone_id'] . "', shipping_address_format = '" . $this->db->escape($value['shipping_address_format']) . "', shipping_custom_field = '" . $this->db->escape(isset($value['shipping_custom_field']) ? json_encode($value['shipping_custom_field']) : '') . "', shipping_method = '" . $this->db->escape($value['shipping_method']) . "', shipping_code = '" . $this->db->escape($value['shipping_code']) . "', comment = '" . $this->db->escape($value['comment']) . "', total = '" . (float) $value['total'] . "', order_status_id  =  '" . (int) 0 . "', affiliate_id = '" . (int) $value['affiliate_id'] . "', commission = '" . (float) $value['commission'] . "', marketing_id = '" . (int) $value['marketing_id'] . "', tracking = '" . $this->db->escape($value['tracking']) . "', language_id = '" . (int) $value['language_id'] . "', currency_id = '" . (int) $value['currency_id'] . "', currency_code = '" . $this->db->escape($value['currency_code']) . "', currency_value = '" . (float) $value['currency_value'] . "', ip = '" . $this->db->escape($value['ip']) . "', forwarded_ip = '" . $this->db->escape($value['forwarded_ip']) . "', user_agent = '" . $this->db->escape($value['user_agent']) . "', accept_language = '" . $this->db->escape($value['accept_language']) . "', date_added = NOW(), date_modified = NOW(), order_type = '" . $this->db->escape($value['order_type']) . "'");
                    $order_id = $this->db->getLastId();

                    $products = $this->model_account_order->getOrderProducts($value['order_id']);
                    foreach ($products as $key => $product) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "order_product SET order_id = '" . (int) $order_id . "', product_id = '" . (int) $product['product_id'] . "', name = '" . $this->db->escape($product['name']) . "', model = '" . $this->db->escape($product['model']) . "', quantity = '" . (int) $product['quantity'] . "', price = '" . (float) $product['price'] . "', total = '" . (float) $product['total'] . "', tax = '" . (float) $product['tax'] . "', reward = '" . (int) $product['reward'] . "'");
                        $order_product_id = $this->db->getLastId();

                        $order_options = $this->model_account_order->getOrderOptions($value['order_id'], $product['order_product_id']);
                        foreach ($order_options as $option) {
                            $this->db->query("INSERT INTO " . DB_PREFIX . "order_option SET order_id = '" . (int) $order_id . "', order_product_id = '" . (int) $order_product_id . "', product_option_id = '" . (int) $option['product_option_id'] . "', product_option_value_id = '" . (int) $option['product_option_value_id'] . "', name = '" . $this->db->escape($option['name']) . "', `value` = '" . $this->db->escape($option['value']) . "', `type` = '" . $this->db->escape($option['type']) . "'");
                        }
                    }

                    $order_vouchers = $this->model_account_order->getOrderVouchers($value['order_id']);
                    foreach ($order_vouchers as $voucher) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "order_voucher SET order_id = '" . (int) $order_id . "', description = '" . $this->db->escape($voucher['description']) . "', code = '" . $this->db->escape($voucher['code']) . "', from_name = '" . $this->db->escape($voucher['from_name']) . "', from_email = '" . $this->db->escape($voucher['from_email']) . "', to_name = '" . $this->db->escape($voucher['to_name']) . "', to_email = '" . $this->db->escape($voucher['to_email']) . "', voucher_theme_id = '" . (int) $voucher['voucher_theme_id'] . "', message = '" . $this->db->escape($voucher['message']) . "', amount = '" . (float) $voucher['amount'] . "'");

                        $order_voucher_id = $this->db->getLastId();

                        $voucher_id = $this->model_extension_total_voucher->addVoucher($value['order_id'], $voucher);

                        $this->db->query("UPDATE " . DB_PREFIX . "order_voucher SET voucher_id = '" . (int) $voucher_id . "' WHERE order_voucher_id = '" . (int) $order_voucher_id . "'");
                    }

                    $order_totals = $this->model_account_order->getOrderTotals($value['order_id']);
                    foreach ($order_totals as $key => $total) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET order_id = '" . (int) $order_id . "', code = '" . $this->db->escape($total['code']) . "', title = '" . $this->db->escape($total['title']) . "', `value` = '" . (float) $total['value'] . "', sort_order = '" . (int) $total['sort_order'] . "'");
                    }

                    $autopaymentdetails = $this->autopayment($value['customer_id'], $order_id, $value['total'], $value['currency_code']);


                    if($autopaymentdetails) {
                        mail('maninder@nascenture.com', 'subject', 'success');
                        echo "success";
                    } else {
                        mail('maninder@nascenture.com', 'subject', 'failed');
                         echo "failed";
                    }
                 
            }
        } else {
           //  mail('maninder@nascenture.com', 'subject', 'foreach empty');
        }
    }

    public function autopayment($customer_id, $order_id, $total, $currency_code) {
     
        include_once(DIR_APPLICATION . '/controller/gateway_tps_xml.php');
        $this->load->model('checkout/order');
        $this->load->model('account/order');
        $this->load->model('setting/setting');
        $order_info = array();
        ///'CAD'
        switch ($currency_code) {
            case $this->config->get('globalonepay_secure_currency1') :
                $currency = $this->config->get('globalonepay_secure_currency1');
                $terminalId = $this->config->get('globalonepay_secure_terminal1');
                $secret = $this->config->get('globalonepay_secure_secret1');
                break;
            case $this->config->get('globalonepay_secure_currency2') :
                $currency = $this->config->get('globalonepay_secure_currency2');
                $terminalId = $this->config->get('globalonepay_secure_terminal2');
                $secret = $this->config->get('globalonepay_secure_secret2');
                break;
            case $this->config->get('globalonepay_secure_currency3') :
                $currency = $this->config->get('globalonepay_secure_currency3');
                $terminalId = $this->config->get('globalonepay_secure_terminal3');
                $secret = $this->config->get('globalonepay_secure_secret3');
        }

        $testAccount = false;
        if ($this->config->get('globalonepay_secure_test')) {
            $testAccount = true;
        }

        $gateway = 'globalone';

        // Order details //
        $customer_id = $customer_id; //1;
        $order_id = $order_id; //120;
        $amount = number_format($total,2); //10;
        //get stored cardref details  //
        $refDetails = $this->model_account_order->getPaymentrefDetails($customer_id);
         
        
        if (!empty($refDetails)) 
        {

            $cardNumber = $refDetails['responseref'];
            $cardType = 'SECURECARD';
            //$email = ($this->config->get('globalonepayhpp_send_receipt') == '1' ? $order_info['email'] : '');
            # These values are specific to the transaction.
            $orderId = 'SHOPPAL' . $order_id;
            # These fields are for AVS (Address Verification Check). This is only appropriate in the UK and the US.
            $cvv = '';
            $email = '';
            $mobileNumber = '';
            $issueNo = '';
            $address1 = '';         # (optional) This is the first line of the cardholders address.
            $address2 = '';         # (optional) This is the second line of the cardholders address.
            $postcode = '';         # (optional) This is the cardholders post code.
            $country = '';          # (optional) This is the cardholders country name.
            $phone = '';            # (optional) This is the cardholders home phone number.
            # eDCC fields. Populate these if you have retreived a rate for the transaction, offered it to the cardholder and they have accepted that rate.
            $cardCurrency = '';     # (optional) This is the three character ISO currency code returned in the rate request.
            $cardAmount = '';       # (optional) This is the foreign currency transaction amount returned in the rate request.
            $conversionRate = '';       # (optional) This is the currency conversion rate returned in the rate request.
            # 3D Secure reference. Only include if you have verified 3D Secure throuugh the GlobalOne MPI and received an MPIREF back.
            $mpiref = '';           # This should be blank unless instructed otherwise by GlobalOne.
            $deviceId = '';         # This should be blank unless instructed otherwise by GlobalOne.

            $autoready = '';        # (optional) (Y/N) Whether or not this transaction should be marked with a status of "ready" as apposed to "pending".
            $multicur = false;      # This should be false unless instructed otherwise by GlobalOne.

            $description = '';      # (optional) This can is a description for the transaction that will be available in the merchant notification e-mail and in the SelfCare  system.
            $autoReady = '';        # (optional) Y or N. Automatically set the transaction to a status of Ready in the batch. If not present the terminal default will be used.
            $isMailOrder = false;   #If true the transaction will be processed as a Mail Order transaction. This is only for use with Mail Order enabled Terminal IDs.
            # Set up the authorisation object
            //echo "string2" ; die;

            $auth = new XmlAuthRequest($terminalId, $orderId, $currency, $amount, $cardNumber, $cardType);
             
            if ($cardType != "SECURECARD")
                $auth->SetNonSecureCardCardInfo($cardExpiry, $cardHolderName);
            if ($cvv != "")
                $auth->SetCvv($cvv);
            if ($cardCurrency != "" && $cardAmount != "" && $conversionRate != "")
                $auth->SetForeignCurrencyInformation($cardCurrency, $cardAmount, $conversionRate);
            if ($email != "")
                $auth->SetEmail($email);
            if ($mobileNumber != "")
                $auth->SetMobileNumber($mobileNumber);
            if ($description != "")
                $auth->SetDescription($description);

            if ($issueNo != "")
                $auth->SetIssueNo($issueNo);
            if ($address1 != "" && $address2 != "" && $postcode != "")
                $auth->SetAvs($address1, $address2, $postcode);
            if ($country != "")
                $auth->SetCountry($country);
            if ($phone != "")
                $auth->SetPhone($phone);

            if ($mpiref != "")
                $auth->SetMpiRef($mpiref);
            if ($deviceId != "")
                $auth->SetDeviceId($deviceId);

            if ($multicur)
                $auth->SetMultiCur();
            if ($autoready)
                $auth->SetAutoReady($autoready);
            if ($isMailOrder)
                $auth->SetMotoTrans();

            # Perform the online authorisation and read in the result
            $response = $auth->ProcessRequestToGateway($secret, $testAccount, $gateway);


              $expectedResponseHash = md5($terminalId . $response->UniqueRef() . ($multicur == true ? $currency : '') . $amount . $response->DateTime() . $response->ResponseCode() . $response->ResponseText() .  $secret);

                //$response->BankResponseCode() .
              //if($expectedResponseHash == $response->Hash())
            if ($response->IsError()) {
                echo  'AN ERROR OCCURED! You transaction was not processed. Error details: ' . $response->ErrorString();
                
                return false;
            } else {
                  
                switch ($response->ResponseCode()) {
                    case "A" :  # -- If using local database, update order as Authorised.
                        //echo 'Payment Processed successfully. Thanks you for your order.';
                        $this->model_checkout_order->addOrderHistory($order_id, $this->config->get('globalonepay_secure_status_pendingAutoship'), $response->ResponseText(), FALSE);
                        return true;

                        break;
                    case "R" :
                    case "D" :
                    case "C" :
                    case "S" :

                    default :  # -- If using local database, update order as declined/failed --
                        // echo  'PAYMENT DECLINED! Please try again with another card. Bank response: ' . $response->ResponseText();
                         $this->model_checkout_order->addOrderHistory($order_id, $this->config->get('globalonepay_secure_status_declined'), $response->ResponseText(), FALSE);
                             
                        return false;
                }

                $request_data = $auth->getRequestInfo();
                $request_data['customer_id'] = $customer_id;
                $request_data['order_id'] = $order_id;
                $request_data['order_type'] = 'monthly';
                $this->saveGlobaloneXmldata($request_data, $response);

             } 
             // else {               
                  
             //     $uniqueReference = $response->UniqueRef();              
             //       // $this->session->data['error'] = 'PAYMENT FAILED: INVALID RESPONSE HASH. Please contact to clarify if you will get charged for this order.';             
             //        $this->session->data['error'] = $this->language->get('invalid_hash_error');      
             //        $this->model_checkout_order->addOrderHistory( $order_id , 
             //        $this->config->get('globalonepay_secure_status_failed'), 
             //        $response->ResponseText(), FALSE );             
             //        $json['redirect'] = $this->url->link('checkout/checkout');

             // }
        } else {
               
            return false;
            //die('no Reference details for payment');
        }

        exit;
    }

    public function saveGlobaloneXmldata($request_data, $response){

        $globalone_request_id = $this->model_checkout_order->saveGlobaloneXmlRequest($request_data);
        if($globalone_request_id){
            $response_data =  array(
                'globalone_request_id' => $globalone_request_id,
                'isError' => $response->IsError(),
                'errorString' => $response->ErrorString(),
                'responseCode' => $response->ResponseCode(),
                'responseText' => $response->ResponseText(),
                'approvalCode' => $response->ApprovalCode(),
                'authorizedAmount' => $response->AuthorizedAmount(),
                'dateTime' => $response->DateTime(),
                'avsResponse' => $response->AvsResponse(),
                'cvvResponse' => $response->CvvResponse(),
                'uniqueRef' => $response->UniqueRef(),
                'hash' => $response->Hash(),
            );
            $this->model_checkout_order->saveGlobaloneXmlResponse($response_data);
        }
    }

}
