<?php
class ControllerAccountRegister extends Controller {
	private $error = array();

	public function index() {
		$data['tracking'] = $_COOKIE['tracking'];
		if ($this->customer->isLogged()) {
			$this->response->redirect($this->url->link('account/account', '', true));
		}
	  
		$data['member_type'] = 'customer';

 		if(isset($_GET['type']) && $_GET['type'] == 'fundraising'){
 			$data['member_type'] = 'fundraising';
 		}


		$this->load->language('account/register');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

		$this->load->model('account/customer');

		if ($this->request->server['REQUEST_METHOD'] == 'POST'){


			if($data['member_type'] == 'customer' && $this->validate()){

				$customer_id = $this->model_account_customer->addCustomer($this->request->post);
				$this->model_account_customer->updateAffiliateid($customer_id);
				//echo "string"; die;

				// Clear any previous login attempts for unregistered accounts.
				$this->model_account_customer->deleteLoginAttempts($this->request->post['email']);

				$this->customer->login($this->request->post['email'], $this->request->post['password']);
				unset($this->session->data['guest']);

				//Add to activity log
				if ($this->config->get('config_customer_activity')) {
					$this->load->model('account/activity');

					$activity_data = array(
						'customer_id' => $customer_id,
						'name'        => $this->request->post['firstname'] . ' ' . $this->request->post['lastname']
					);

					$this->model_account_activity->addActivity('register', $activity_data);
				}

				$this->response->redirect($this->url->link('account/success'));
				//echo "string"; die;

			}
			else if($data['member_type'] == 'fundraising' && $this->validateOrganization()){
 				
 				$uploads_dir = 'image/data/';
 				$postdata = $this->request->post;
 				$postdata['logo'] = '';
				if (is_uploaded_file($this->request->files['logo']['tmp_name'])) {
					$logo_name = $this->uploadLogo($this->request->files['logo']);
					if($logo_name){
						$postdata['logo'] = $logo_name;	
					}
					
				}

				$customer_id = $this->model_account_customer->addFundraising($postdata);
				// Clear any previous login attempts for unregistered accounts.
				$this->model_account_customer->deleteLoginAttempts($this->request->post['email']);

				$this->customer->login($this->request->post['email'], $this->request->post['password']);
				unset($this->session->data['guest']);

				// Add to activity log
				if ($this->config->get('config_customer_activity')) {
					$this->load->model('account/activity');

					$activity_data = array(
						'customer_id' => $customer_id,
						'name'        => $this->request->post['firstname'] . ' ' . $this->request->post['lastname']
					);

					$this->model_account_activity->addActivity('register', $activity_data);
				}

				$this->response->redirect($this->url->link('account/success'));

			}

		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_register'),
			'href' => $this->url->link('account/register', '', true)
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_account_already'] = sprintf($this->language->get('text_account_already'), $this->url->link('account/login', '', true));
		$data['text_your_details'] = $this->language->get('text_your_details');
		$data['text_your_address'] = $this->language->get('text_your_address');
		$data['text_your_password'] = $this->language->get('text_your_password');
		$data['text_newsletter'] = $this->language->get('text_newsletter');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$data['entry_firstname'] = $this->language->get('entry_firstname');
		$data['entry_lastname'] = $this->language->get('entry_lastname');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_telephone'] = $this->language->get('entry_telephone');
		$data['entry_fax'] = $this->language->get('entry_fax');
		$data['entry_company'] = $this->language->get('entry_company');
		$data['entry_address_1'] = $this->language->get('entry_address_1');
		$data['entry_address_2'] = $this->language->get('entry_address_2');
		$data['entry_postcode'] = $this->language->get('entry_postcode');
		$data['entry_city'] = $this->language->get('entry_city');
		$data['entry_country'] = $this->language->get('entry_country');
		$data['entry_zone'] = $this->language->get('entry_zone');
		$data['entry_newsletter'] = $this->language->get('entry_newsletter');
		$data['entry_password'] = $this->language->get('entry_password');
		$data['entry_confirm'] = $this->language->get('entry_confirm');
		$data['entry_hear_about_us'] = $this->language->get('entry_hear_about_us');
		$data['entry_facebook'] = $this->language->get('entry_facebook'); 
		$data['entry_linkedin'] = $this->language->get('entry_linkedin'); 
		$data['entry_local_event'] = $this->language->get('entry_local_event'); 
		$data['entry_shoppal_website'] = $this->language->get('entry_shoppal_website');
		$data['entry_local_flyer'] = $this->language->get('entry_local_flyer'); 
		$data['entry_other_fundraising_organization'] = $this->language->get('entry_other_fundraising_organization');
		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_upload'] = $this->language->get('button_upload');

		$data['entry_other_basic_information'] = $this->language->get('entry_other_basic_information');
		$data['entry_organization_name'] = $this->language->get('entry_organization_name');
		$data['entry_telephone_format'] = $this->language->get('entry_telephone_format');

		$data['entry_position_organization'] = $this->language->get('entry_position_organization');
		$data['entry_mobile_number'] = $this->language->get('entry_mobile_number');
		$data['entry_organization_phone_number'] = $this->language->get('entry_organization_phone_number');
		$data['entry_street_address'] = $this->language->get('entry_street_address');
		$data['entry_street_address2'] = $this->language->get('entry_street_address2');
		$data['entry_zip_code'] = $this->language->get('entry_zip_code');
		$data['entry_region'] = $this->language->get('entry_region');
		$data['entry_branded_website'] = $this->language->get('entry_branded_website');
		$data['entry_organization_website'] = $this->language->get('entry_organization_website');
		$data['entry_organization_hashtags'] = $this->language->get('entry_organization_hashtags');
		$data['entry_organization_facebook_link'] = $this->language->get('entry_organization_facebook_link');
		$data['entry_organization_instagram'] = $this->language->get('entry_organization_instagram');
		$data['entry_organization_twitter'] = $this->language->get('entry_organization_twitter');
		$data['entry_organization_linkedIn_link'] = $this->language->get('entry_organization_linkedIn_link');
		$data['entry_agreement_heading'] = $this->language->get('entry_agreement_heading');
		$data['entry_agreement_checkbox1'] = $this->language->get('entry_agreement_checkbox1');
		$data['entry_agreement_checkbox2'] = $this->language->get('entry_agreement_checkbox2');
		$data['entry_agreement_checkbox3'] = $this->language->get('entry_agreement_checkbox3');
		$data['entry_logo'] = $this->language->get('entry_logo');
		$data['entry_organization_mission_statement'] = $this->language->get('entry_organization_mission_statement');

		$data['entry_organization_details'] = $this->language->get('entry_organization_details');
		$data['entry_type_question'] = $this->language->get('entry_type_question'); 
		$data['entry_local_store'] = $this->language->get('entry_local_store'); 
		$data['entry_local_flyer'] = $this->language->get('entry_local_flyer'); 
		$data['entry_other_fundraising_organization'] = $this->language->get('entry_other_fundraising_organization');
		$data['entry_other_hear_about_us'] = $this->language->get('entry_other_hear_about_us');
 		$data['entry_shoppal_website'] = $this->language->get('entry_shoppal_website');
 		$data['entry_organization_detailed'] = $this->language->get('entry_organization_detailed');
 		$data['entry_hear_about_us'] = $this->language->get('entry_hear_about_us');
		
		
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['firstname'])) {
			$data['error_firstname'] = $this->error['firstname'];
		} else {
			$data['error_firstname'] = '';
		}

		if (isset($this->error['lastname'])) {
			$data['error_lastname'] = $this->error['lastname'];
		} else {
			$data['error_lastname'] = '';
		}

		if (isset($this->error['email'])) {
			$data['error_email'] = $this->error['email'];
		} else {
			$data['error_email'] = '';
		}

		if (isset($this->error['telephone'])) {
			$data['error_telephone'] = $this->error['telephone'];
		} else {
			$data['error_telephone'] = '';
		}

		if (isset($this->error['address_1'])) {
			$data['error_address_1'] = $this->error['address_1'];
		} else {
			$data['error_address_1'] = '';
		}

		if (isset($this->error['city'])) {
			$data['error_city'] = $this->error['city'];
		} else {
			$data['error_city'] = '';
		}

		if (isset($this->error['postcode'])) {
			$data['error_postcode'] = $this->error['postcode'];
		} else {
			$data['error_postcode'] = '';
		}

		if (isset($this->error['country'])) {
			$data['error_country'] = $this->error['country'];
		} else {
			$data['error_country'] = '';
		}

		if (isset($this->error['zone'])) {
			$data['error_zone'] = $this->error['zone'];
		} else {
			$data['error_zone'] = '';
		}

		if (isset($this->error['custom_field'])) {
			$data['error_custom_field'] = $this->error['custom_field'];
		} else {
			$data['error_custom_field'] = array();
		}

		if (isset($this->error['password'])) {
			$data['error_password'] = $this->error['password'];
		} else {
			$data['error_password'] = '';
		}

		if (isset($this->error['confirm'])) {
			$data['error_confirm'] = $this->error['confirm'];
		} else {
			$data['error_confirm'] = '';
		}

		//organization errors
		if (isset($this->error['organization_name'])) {
			$data['error_organization_name'] = $this->error['organization_name'];
		} else {
			$data['error_organization_name'] = '';
		}

		if (isset($this->error['organization_phone_number'])) {
			$data['error_organization_phone_number'] = $this->error['organization_phone_number'];
		} else {
			$data['error_organization_phone_number'] = '';
		}

		if (isset($this->error['position_in_organization'])) {
			$data['error_position_in_organization'] = $this->error['position_in_organization'];
		} else {
			$data['error_position_in_organization'] = '';
		}
		

		if (isset($this->error['street_address'])) {
			$data['error_street_address'] = $this->error['street_address'];
		} else {
			$data['error_street_address'] = '';
		}

		/* if (isset($this->error['street_address_line2'])) {
			$data['error_street_address_line2'] = $this->error['street_address_line2'];
		} else {
			$data['error_street_address_line2'] = '';
		} */

		if (isset($this->error['city'])) {
			$data['error_city'] = $this->error['city'];
		} else {
			$data['error_city'] = '';
		}
		if (isset($this->request->post['referred_by'])) {
			$data['referred_by'] = $this->request->post['referred_by'];
		} else {
			$data['referred_by'] = '';
		}
		
		if (isset($this->error['organization_mission_statement'])) {
			$data['error_organization_mission_statement'] = $this->error['organization_mission_statement'];
		} else {
			$data['error_organization_mission_statement'] = '';
		}
		
		

		if (isset($this->error['zone_id'])) {
			$data['error_state'] = $this->error['zone_id'];
		} else {
			$data['error_state'] = '';
		}
		if (isset($this->error['zip_code'])) {
			$data['error_zip_code'] = $this->error['zip_code'];
		} else {
			$data['error_zip_code'] = '';
		}
		if (isset($this->error['country_id'])) {
			$data['error_country'] = $this->error['country_id'];
		} else {
			$data['error_country'] = '';
		}
		
		if (isset($this->error['logo'])) {
			$data['error_logo'] = $this->error['logo'];
		} else {
			$data['error_logo'] = '';
		}

		$data['action'] = $this->url->link('account/register', '', true);

		$data['customer_groups'] = array();

		if (is_array($this->config->get('config_customer_group_display'))) {
			$this->load->model('account/customer_group');

			$customer_groups = $this->model_account_customer_group->getCustomerGroups();

			foreach ($customer_groups as $customer_group) {
				if (in_array($customer_group['customer_group_id'], $this->config->get('config_customer_group_display'))) {
					$data['customer_groups'][] = $customer_group;
				}
			}
		}

		if (isset($this->request->post['customer_group_id'])) {
			$data['customer_group_id'] = $this->request->post['customer_group_id'];
		} else {
			$data['customer_group_id'] = $this->config->get('config_customer_group_id');
		}

		if (isset($this->request->post['firstname'])) {
			$data['firstname'] = $this->request->post['firstname'];
		} else {
			$data['firstname'] = '';
		}

		if (isset($this->request->post['lastname'])) {
			$data['lastname'] = $this->request->post['lastname'];
		} else {
			$data['lastname'] = '';
		}

		if (isset($this->request->post['email'])) {
			$data['email'] = $this->request->post['email'];
		} else {
			$data['email'] = '';
		}

		if (isset($this->request->post['telephone'])) {
			$data['telephone'] = $this->request->post['telephone'];
		} else {
			$data['telephone'] = '';
		}

		if (isset($this->request->post['fax'])) {
			$data['fax'] = $this->request->post['fax'];
		} else {
			$data['fax'] = '';
		}

		if (isset($this->request->post['company'])) {
			$data['company'] = $this->request->post['company'];
		} else {
			$data['company'] = '';
		}

		if (isset($this->request->post['address_1'])) {
			$data['address_1'] = $this->request->post['address_1'];
		} else {
			$data['address_1'] = '';
		}

		if (isset($this->request->post['address_2'])) {
			$data['address_2'] = $this->request->post['address_2'];
		} else {
			$data['address_2'] = '';
		}

		if (isset($this->request->post['postcode'])) {
			$data['postcode'] = $this->request->post['postcode'];
		} elseif (isset($this->session->data['shipping_address']['postcode'])) {
			$data['postcode'] = $this->session->data['shipping_address']['postcode'];
		} else {
			$data['postcode'] = '';
		}

		if (isset($this->request->post['city'])) {
			$data['city'] = $this->request->post['city'];
		} else {
			$data['city'] = '';
		}
		
		if (isset($this->request->post['referred_by'])) {
			$data['referred_by'] = $this->request->post['referred_by'];
		} else {
			$data['referred_by'] = '';
		}
		
		if (isset($this->request->post['country_id'])) {
			$data['country_id'] = (int)$this->request->post['country_id'];
		} elseif (isset($this->session->data['shipping_address']['country_id'])) {
			$data['country_id'] = $this->session->data['shipping_address']['country_id'];
		} else {
			
			$data['country_id'] = $this->config->get('config_country_id');
			
		}

		if (isset($this->request->post['zone_id'])) {
			$data['zone_id'] = (int)$this->request->post['zone_id'];
		} elseif (isset($this->session->data['shipping_address']['zone_id'])) {
			$data['zone_id'] = $this->session->data['shipping_address']['zone_id'];
		} else {
			$data['zone_id'] = '';
		}
		
		
		

		$this->load->model('localisation/country');

		$data['countries'] = $this->model_localisation_country->getCountries();

		// Custom Fields
		$this->load->model('account/custom_field');

		$data['custom_fields'] = $this->model_account_custom_field->getCustomFields();

		if (isset($this->request->post['custom_field'])) {
			if (isset($this->request->post['custom_field']['account'])) {
				$account_custom_field = $this->request->post['custom_field']['account'];
			} else {
				$account_custom_field = array();
			}

			if (isset($this->request->post['custom_field']['address'])) {
				$address_custom_field = $this->request->post['custom_field']['address'];
			} else {
				$address_custom_field = array();
			}

			$data['register_custom_field'] = $account_custom_field + $address_custom_field;
		} else {
			$data['register_custom_field'] = array();
		}

		if (isset($this->request->post['password'])) {
			$data['password'] = $this->request->post['password'];
		} else {
			$data['password'] = '';
		}

		if (isset($this->request->post['confirm'])) {
			$data['confirm'] = $this->request->post['confirm'];
		} else {
			$data['confirm'] = '';
		}

		if (isset($this->request->post['newsletter'])) {
			$data['newsletter'] = $this->request->post['newsletter'];
		} else {
			$data['newsletter'] = '';
		}


		//organization post data
		if (isset($this->request->post['organization_name'])) {
			$data['organization_name'] = $this->request->post['organization_name'];
		} else {
			$data['organization_name'] = '';
		}
		if (isset($this->request->post['position_in_organization'])) {
			$data['position_in_organization'] = $this->request->post['position_in_organization'];
		} else {
			$data['position_in_organization'] = '';
		}
		if (isset($this->request->post['mobile_number'])) {
			$data['mobile_number'] = $this->request->post['mobile_number'];
		} else {
			$data['mobile_number'] = '';
		}
		if (isset($this->request->post['organization_phone_number'])) {
			$data['organization_phone_number'] = $this->request->post['organization_phone_number'];
		} else {
			$data['organization_phone_number'] = '';
		}
		if (isset($this->request->post['street_address'])) {
			$data['street_address'] = $this->request->post['street_address'];
		} else {
			$data['street_address'] = '';
		}
		if (isset($this->request->post['street_address_line2'])) {
			$data['street_address_line2'] = $this->request->post['street_address_line2'];
		} else {
			$data['street_address_line2'] = '';
		}
		if (isset($this->request->post['city'])) {
			$data['city'] = $this->request->post['city'];
		} else {
			$data['city'] = '';
		}
		if (isset($this->request->post['zone_id'])) {
			$data['zone_id'] = $this->request->post['zone_id'];
		} else {
			$data['zone_id'] = '';
		}
		if (isset($this->request->post['referred_by'])) {
			$data['referred_by'] = $this->request->post['referred_by'];
		} else {
			$data['referred_by'] = '';
		}
		
		if (isset($this->request->post['zip_code'])) {
			$data['zip_code'] = $this->request->post['zip_code'];
		} else {
			$data['zip_code'] = '';
		}
		
		if (isset($this->request->post['branded_website'])) {
			$data['branded_website'] = $this->request->post['branded_website'];
		} else {
			$data['branded_website'] = '';
		}
		if (isset($this->request->post['organization_website'])) {
			$data['organization_website'] = $this->request->post['organization_website'];
		} else {
			$data['organization_website'] = '';
		}
		if (isset($this->request->post['organization_hashtags'])) {
			$data['organization_hashtags'] = $this->request->post['organization_hashtags'];
		} else {
			$data['organization_hashtags'] = '';
		}
		if (isset($this->request->post['organization_facebook_link'])) {
			$data['organization_facebook_link'] = $this->request->post['organization_facebook_link'];
		} else {
			$data['organization_facebook_link'] = '';
		}
		if (isset($this->request->post['organization_instagram'])) {
			$data['organization_instagram'] = $this->request->post['organization_instagram'];
		} else {
			$data['organization_instagram'] = '';
		}
		if (isset($this->request->post['organization_twitter'])) {
			$data['organization_twitter'] = $this->request->post['organization_twitter'];
		} else {
			$data['organization_twitter'] = '';
		}
		if (isset($this->request->post['organization_linkedIn_link'])) {
			$data['organization_linkedIn_link'] = $this->request->post['organization_linkedIn_link'];
		} else {
			$data['organization_linkedIn_link'] = '';
		}
		if (isset($this->request->post['organization_mission_statement'])) {
			$data['organization_mission_statement'] = $this->request->post['organization_mission_statement'];
		} else {
			$data['organization_mission_statement'] = '';
		}
		if (isset($this->request->post['organization_detailed'])) {
			$data['organization_detailed'] = $this->request->post['organization_detailed'];
		} else {
			$data['organization_detailed'] = '';
		}

		// Captcha
		if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('register', (array)$this->config->get('config_captcha_page'))) {
			$data['captcha'] = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha'), $this->error);
		} else {
			$data['captcha'] = '';
		}

		if ($this->config->get('config_account_id')) {
			$this->load->model('catalog/information');

			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));

			if ($information_info) {
				$data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/agree', 'information_id=' . $this->config->get('config_account_id'), true), $information_info['title'], $information_info['title']);
			} else {
				$data['text_agree'] = '';
			}
		} else {
			$data['text_agree'] = '';
		}

		if (isset($this->request->post['agree'])) {
			$data['agree'] = $this->request->post['agree'];
		} else {
			$data['agree'] = false;
		}


		/* For checkboxes*/
		
		if (isset($this->request->post['zone_id'])) {
			$data['zone_id'] = $this->request->post['zone_id'];
		} else {
			$data['zone_id'] = '';
		}
		
		if (isset($this->request->post['agreement'])) {
			$data['checkBox1'] = '';
			$data['checkBox2'] = '';
			$data['checkBox3'] = '';
			$data['agreement'] = $this->request->post['agreement'];
			$checkBox1 = 'on the main, or other pages of the ShopPal website that display Memberinformation';
			$checkBox2 = 'on Social Media and other Internet Marketing platforms, and/or';
			$checkBox3 = 'on Print Media and or Radio/Television Media.';
			foreach($data['agreement'] as $getAggrement)
			{
				$getAggrement = strtolower(trim($getAggrement));
				if($getAggrement == strtolower(trim($checkBox1)))
				{
					$data['checkBox1'] = 'checked';
				}
				if($getAggrement == strtolower(trim($checkBox2)))
				{
					$data['checkBox2'] = 'checked';
				}
				if($getAggrement == strtolower(trim($checkBox3)))
				{
					$data['checkBox3'] = 'checked';
				}
			}
			
		} else {
			$data['agreement'] = '';
			$data['checkBox1'] = '';
			$data['checkBox2'] = '';
			$data['checkBox3'] = '';
		}
		
		/* Social Radio Buttons */

		
		//$data['checked_other'] = '';
		//$data['referred_by'] = '';
		//$data['disabled'] = '';
		
		if (isset($this->request->post['hear_about_us'])) {
			$data['checked_facebook'] = '';
			$data['checked_linkedIn'] = '';
			$data['checked_shoppal'] = '';
			$data['checked_local_store'] = '';
			$data['checked_loal_event'] = '';
			$data['checked_flyer'] = '';
			$data['checked_other_fundraising_organization'] = '';
			$data['hear_about_us'] = $this->request->post['hear_about_us'];
			$checked_facebook = 'Facebook';
			$checked_linkedIn = 'LinkedIn';
			$checked_shoppal = 'ShopPal Website';
			$checked_local_store = 'Local Store';
			$checked_loal_event = 'Local Event';
			$checked_flyer = 'Flyer';
			$checked_other_fundraising_organization = 'Other Fundraising Organization';
			
			foreach($data['hear_about_us'] as $gethearaboutus)
			{
				if($gethearaboutus == $checked_facebook)
				{
					$data['checked_facebook'] = 'checked';
				}
				if($gethearaboutus == $checked_linkedIn)
				{
					$data['checked_linkedIn'] = 'checked';
				}
				if($gethearaboutus == $checked_shoppal)
				{
					$data['checked_shoppal'] = 'checked';
				}
				if($gethearaboutus == $checked_local_store)
				{
					$data['checked_local_store'] = 'checked';
				}
				if($gethearaboutus == $checked_loal_event)
				{
					$data['checked_loal_event'] = 'checked';
				}
				if($gethearaboutus == $checked_flyer)
				{
					$data['checked_flyer'] = 'checked';
				}
				if($gethearaboutus == $checked_other_fundraising_organization)
				{
					$data['checked_other_fundraising_organization'] = 'checked';
				}
			}

		}
		else {
			$data['checked_facebook'] = '';
			$data['checked_linkedIn'] = '';
			$data['checked_shoppal'] = '';
			$data['checked_local_store'] = '';
			$data['checked_loal_event'] = '';
			$data['checked_flyer'] = '';
			$data['checked_other_fundraising_organization'] = '';
			$data['hear_about_us'] = '';
		}
		
		

		$this->load->model('affiliate/affiliate');
		if(isset($_COOKIE['tracking'])) {
			$getAffiliate = $this->model_affiliate_affiliate->getAffiliateByCode(trim($_COOKIE['tracking']));
			if($getAffiliate)
			{
				//$data['disabled'] = 'disabled';
				//$data['referred_by'] = $getAffiliate['firstname'].' '.$getAffiliate['lastname'];
				//$data['referred_by'] = $getAffiliate['company'];
				//$data['checked_other'] = 'checked';
				
				
			}
		}
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/register', $data));
	}

	private function uploadLogo($fileData){

		$uploads_dir = 'image/data/';
		if (is_uploaded_file($fileData['tmp_name'])) {
			$path = $fileData['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
			$image = substr(md5(uniqid(rand(), true)), 0, 12) . ".$ext";
			
			if(move_uploaded_file($fileData['tmp_name'],$uploads_dir.$image)){
				return $image;
			}
			 
		}

	}

	private function validateOrganization() {
		
		if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen(trim($this->request->post['organization_name'])) < 1) || (utf8_strlen(trim($this->request->post['organization_name'])) > 50)) {
			$this->error['organization_name'] = $this->language->get('error_organization_name');
		}

		if ((utf8_strlen(trim($this->request->post['position_in_organization'])) < 1) || (utf8_strlen(trim($this->request->post['position_in_organization'])) > 30)) {
			$this->error['position_in_organization'] = $this->language->get('error_position_in_organization');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_exists');
		}

		if ((utf8_strlen($this->request->post['organization_phone_number']) < 3) || (utf8_strlen($this->request->post['organization_phone_number']) > 32)) {
			$this->error['organization_phone_number'] = $this->language->get('error_organization_phone_number');
		}

		if ((utf8_strlen($this->request->post['street_address']) < 1) || (utf8_strlen($this->request->post['street_address']) > 255)) {
			$this->error['street_address'] = $this->language->get('error_street_address');
		}

		/*if ((utf8_strlen($this->request->post['street_address_line2']) < 1) || (utf8_strlen($this->request->post['street_address_line2']) > 255)) {
			$this->error['street_address_line2'] = 'Street Address Line 2 must be between 1 and 255 characters!';
		}*/

		if ((utf8_strlen($this->request->post['city']) < 1) || (utf8_strlen($this->request->post['city']) > 50)) {
			$this->error['city'] = $this->language->get('error_city');
		}
		if ((utf8_strlen($this->request->post['organization_mission_statement']) < 1)) {
			$this->error['organization_mission_statement'] = $this->language->get('error_organization_mission_statement');
		}
		

		if ((utf8_strlen($this->request->post['zone_id']) < 1) || (utf8_strlen($this->request->post['zone_id']) > 100)) {
			$this->error['zone_id'] = $this->language->get('error_zone_id');
		}
		if ((utf8_strlen($this->request->post['zip_code']) < 1) || (utf8_strlen($this->request->post['zip_code']) > 20)) {
			$this->error['zip_code'] = $this->language->get('error_zip_code');
		}
		if ((utf8_strlen($this->request->post['country_id']) < 1) || (utf8_strlen($this->request->post['country_id']) > 50)) {
			$this->error['country_id'] = $this->language->get('error_country_id');
		}

		if ((utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) < 4) || (utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) > 40)) {
			$this->error['password'] = $this->language->get('error_password');
		}

		if ($this->request->post['confirm'] != $this->request->post['password']) {
			$this->error['confirm'] = $this->language->get('error_confirm');
		}

	 
		if (file_exists($_FILES['logo']['tmp_name'])){

			$allowedExts = array("gif", "jpeg", "jpg", "png");

			$name = explode(".", $_FILES["logo"]["name"]);
			$extension = end($name);
 
			if ((($_FILES["logo"]["type"] == "image/gif")
			|| ($_FILES["logo"]["type"] == "image/jpeg")
			|| ($_FILES["logo"]["type"] == "image/jpg")
			|| ($_FILES["logo"]["type"] == "image/png"))
			&& in_array($extension, $allowedExts)){

			}
			else{
				$this->error['logo'] = $this->language->get('error_logo_type');
			}

		}
		else{
			$this->error['logo'] = $this->language->get('error_logo');
		}
		
		return !$this->error;

	}


	private function validate() {
		if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_exists');
		}

		// if (!preg_match("/^\([0-9]{3}\) [0-9]{3}-[0-9]{4}$/i", $this->request->post['telephone'])) {
		// 	$this->error['telephone'] = $this->language->get('error_telephone');
		// }

		if (!preg_match("/^\([0-9]{3}\) [0-9]{3}-[0-9]{4}$/i", $this->request->post['telephone']) && !preg_match("/^[0-9]{10}$/i", $this->request->post['telephone'])) {
			$this->error['telephone'] = $this->language->get('error_telephone');
		}

		if ((utf8_strlen(trim($this->request->post['address_1'])) < 3) || (utf8_strlen(trim($this->request->post['address_1'])) > 128)) {
			$this->error['address_1'] = $this->language->get('error_address_1');
		}

		if ((utf8_strlen(trim($this->request->post['city'])) < 2) || (utf8_strlen(trim($this->request->post['city'])) > 128)) {
			$this->error['city'] = $this->language->get('error_city');
		}

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($this->request->post['country_id']);

		if (utf8_strlen(trim($this->request->post['postcode'])) < 6 || utf8_strlen(trim($this->request->post['postcode'])) > 6) {
			$this->error['postcode'] = $this->language->get('error_postcode');
		}

		if ($this->request->post['country_id'] == '') {
			$this->error['country'] = $this->language->get('error_country');
		}

		if (!isset($this->request->post['zone_id']) || $this->request->post['zone_id'] == '' || !is_numeric($this->request->post['zone_id'])) {
			$this->error['zone'] = $this->language->get('error_zone');
		}

		// Customer Group
		if (isset($this->request->post['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->post['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $this->request->post['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		// Custom field validation
		$this->load->model('account/custom_field');

		$custom_fields = $this->model_account_custom_field->getCustomFields($customer_group_id);

		foreach ($custom_fields as $custom_field) {
            if ($custom_field['required'] && empty($this->request->post['custom_field'][$custom_field['location']][$custom_field['custom_field_id']])) {
				$this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
			} elseif (($custom_field['type'] == 'text') && !empty($custom_field['validation']) && !filter_var($this->request->post['custom_field'][$custom_field['location']][$custom_field['custom_field_id']], FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => $custom_field['validation'])))) {
            	$this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
            }
		}

		if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
			$this->error['password'] = $this->language->get('error_password');
		}

		if ($this->request->post['confirm'] != $this->request->post['password']) {
			$this->error['confirm'] = $this->language->get('error_confirm');
		}

		// Captcha
		if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('register', (array)$this->config->get('config_captcha_page'))) {
			$captcha = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha') . '/validate');

			if ($captcha) {
				$this->error['captcha'] = $captcha;
			}
		}

		// Agree to terms
		if ($this->config->get('config_account_id')) {
			$this->load->model('catalog/information');

			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));

			if ($information_info && !isset($this->request->post['agree'])) {
				$this->error['warning'] = sprintf($this->language->get('error_agree'), $information_info['title']);
			}
		}

		return !$this->error;
	}

	public function customfield() {
		$json = array();

		$this->load->model('account/custom_field');

		// Customer Group
		if (isset($this->request->get['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->get['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $this->request->get['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		$custom_fields = $this->model_account_custom_field->getCustomFields($customer_group_id);

		foreach ($custom_fields as $custom_field) {
			$json[] = array(
				'custom_field_id' => $custom_field['custom_field_id'],
				'required'        => $custom_field['required']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}