<?php

class ControllerAccountCreditCard extends Controller {

    public function index() {

        if (!$this->customer->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('account/creditcard', '', true);
            $this->response->redirect($this->url->link('account/login', '', true));
        }

        $this->load->language('account/creditcard');
        $this->load->model('account/creditcard');

        $this->document->setTitle($this->language->get('heading_title'));

        if (isset($this->session->data['warning'])) {
            $data['warning'] = $this->session->data['warning'];

            unset($this->session->data['warning']);
        }


        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_account'),
            'href' => $this->url->link('account/account', '', true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('account/cardit card')
        );

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_empty'] = $this->language->get('text_empty');

        $data['entry_cardNumber'] = $this->language->get('entry_cardNumber');
        $data['entry_cardType'] = $this->language->get('entry_cardType');
        $data['entry_cardExpiry'] = $this->language->get('entry_cardExpiry');
        $data['entry_cardHolderName'] = $this->language->get('entry_cardHolderName');
        $data['last_update_on'] = $this->language->get('last_update_on');


        $data['button_continue'] = $this->language->get('button_continue');
        $data['button_back'] = $this->language->get('button_back');
        $data['button_autoship'] = $this->language->get('button_autoship');
        $data['button_remove'] = $this->language->get('button_remove');
        $data['action'] = $this->url->link('account/creditcard', '', true);
        $card_types = array(
            'VISA' => 'Visa Credit',
            'MASTERCARD' => 'MasterCard',
            'DEBIT MASTERCARD' => 'Debit MasterCard',
            'ELECTRON' => 'Visa Electron',
            'AMEX' => 'American Express',
            'DINERS' => 'Diners',
            'DISCOVER' => 'Discover',
            'MasterCard', 'Debit MasterCard', 'Visa Electron'
        );
        $data['card_types'] = $card_types;


        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            $data = $this->update();

            if ($data == 'successful') {
                $this->session->data['success'] = $this->language->get('text_success');
                $this->response->redirect($this->url->link('account/account', '', true));
            } else {
                $this->session->data['warning'] = $data;
                $this->response->redirect($this->url->link('account/creditcard', '', true));
            }
        }

        $data['creditCardStatus'] = $this->model_account_creditcard->creditCardStatus($this->customer->getId());

        $ordersDeniedStatus = $this->model_account_creditcard->getOrder($this->customer->getId());
        $data['total_denied_status'] = count($ordersDeniedStatus);

        $data['continue'] = $this->url->link('account/account', '', true);
        $data['back'] = $this->url->link('account/account', '', true);
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('account/creditcard', $data));
    }

    public function update() {

        $this->load->language('account/creditcard');
        $this->load->model('account/creditcard');
        $this->load->model('account/order');

        $card_info = $this->model_account_creditcard->getCreditCard($this->customer->getId());


        $cardnumber = trim($this->request->post['cardnumber']);
        $cardType = trim($this->request->post['cardType']);
        $expiry_month = trim($this->request->post['expiry_month']);
        $expiry_year = trim($this->request->post['expiry-year']);
        $cardHolderName = trim($this->request->post['cardHolderName']);

        include_once(DIR_APPLICATION . '/controller/gateway_tps_xml.php');

        $testAccount = true;
        if ($this->config->get('globalonepay_secure_test')) {
            $testAccount = true;
        }

        
        if($this->config->get('globalonepay_secure_currency1')){
            $terminalId = $this->config->get('globalonepay_secure_terminal1');
            $secret = $this->config->get('globalonepay_secure_secret1');
        } elseif($this->config->get('globalonepay_secure_currency2')) {
            $terminalId = $this->config->get('globalonepay_secure_terminal2');
            $secret = $this->config->get('globalonepay_secure_secret2');
        } elseif ($this->config->get('globalonepay_secure_currency3')) {
            $terminalId = $this->config->get('globalonepay_secure_terminal3');
            $secret = $this->config->get('globalonepay_secure_secret3');
        }

        $terminalId = $terminalId;
        # These values are specific to the cardholder.
        $secureCardMerchantRef = $card_info['merchantref']; # Unique Merchant Reference. Length is limited to 48 chars.
        $cardNumber = $cardnumber;  # This is the full PAN (card number) of the credit card. It must be digits only (i.e. no spaces or other characters).
        $cardType = strtoupper($cardType);  # See our Integrator Guide for a list of valid Card Type parameters
        $year = str_replace('20', '', $expiry_year);
        $cardExpiry = $expiry_month . $year;  //1228';		# The 4 digit expiry date (MMYY)
        $cardHolderName = $cardHolderName;  # The full cardholders name, as it is displayed on the credit card
        $recurringAmount = '';
        $dontCheckSecurity = '';  # (optional) "Y" if you do not want the CVV to be validated online.
        $cvv = '';   # (optional) 3 digit (4 for AMEX cards) security digit on the back of the card.
        $issueNo = '';   # (optional) Issue number for Switch and Solo cards.
        $secret = $secret;
        $gateway = 'globalone';
        # Set up the SecureCard update object
        $secureupd = new XmlSecureCardUpdRequest($secureCardMerchantRef, $terminalId, $cardNumber, $cardExpiry, $cardType, $cardHolderName);

        if ($dontCheckSecurity != "")
            $secureupd->SetDontCheckSecurity($dontCheckSecurity);
        if ($cvv != "")
            $secureupd->SetCvv($cvv);
        if ($issueNo != "")
            $secureupd->SetIssueNo($issueNo);

        $response = $secureupd->ProcessRequestToGateway($secret, $testAccount, $gateway);
        //echo "<pre>"; print_r($response); die;
        if ($response->IsError()) {
            //echo 'AN ERROR OCCURED, Card details not updated. Error details: ' . $response->ErrorString();
            return $response->ErrorString();
        } else {
            $expectedResponseHash = md5($terminalId . $response->MerchantReference() . $response->CardReference() . $response->DateTime() . $secret);
            if ($expectedResponseHash != $response->Hash()) {

                $msg = 'SECURECARD UPDATE FAILED: INVALID RESPONSE HASH. Please contact <a href="mailto:' . $adminEmail . '">' . $adminEmail . '</a> or call ' . $adminPhone . ' to clarify if your card details were updated.';

                $merchantRef = $response->MerchantReference();
                if (isset($merchantRef)) {

                    $msg = 'Please quote %Gateway Terminal ID: ' . $terminalId . ', and SecureCard Merchant Reference: ' . $merchantRef . ' when mailling or calling.';
                }
                return $msg;
            } else {

                $data = array(
                    'customer_id' => $this->customer->getId(),
                    'cardReference' => $response->CardReference(),
                    'dateTime' => $response->DateTime(),
                    'MerchantReference' => $response->MerchantReference(),
                    'cardType' => $cardType,
                    'cardExpiry' => $cardExpiry
                );

                $card_info = $this->model_account_creditcard->updateCreditCard($data);
                return 'successful';
            }
        }
    }

    public function CheckDeniedOrder() {

        $this->customer->getId();
        $this->load->model('account/creditcard');
        $this->load->model('account/order');

        $get_orders = $this->model_account_creditcard->getOrder($this->customer->getId());
        //echo '<pre>'; print_r($get_orders); die;

        if (!empty($get_orders)) {
            foreach ($get_orders as $value) {

                $this->db->query("INSERT INTO `" . DB_PREFIX . "order` SET invoice_prefix = '" . $this->db->escape($value['invoice_prefix']) . "', store_id = '" . (int) $value['store_id'] . "', store_name = '" . $this->db->escape($value['store_name']) . "', store_url = '" . $this->db->escape($value['store_url']) . "', customer_id = '" . (int) $value['customer_id'] . "', customer_group_id = '" . (int) $value['customer_group_id'] . "', firstname = '" . $this->db->escape($value['firstname']) . "', lastname = '" . $this->db->escape($value['lastname']) . "', email = '" . $this->db->escape($value['email']) . "', telephone = '" . $this->db->escape($value['telephone']) . "', fax = '" . $this->db->escape($value['fax']) . "', custom_field = '" . $this->db->escape(isset($value['custom_field']) ? json_encode($value['custom_field']) : '') . "', payment_firstname = '" . $this->db->escape($value['payment_firstname']) . "', payment_lastname = '" . $this->db->escape($value['payment_lastname']) . "', payment_company = '" . $this->db->escape($value['payment_company']) . "', payment_address_1 = '" . $this->db->escape($value['payment_address_1']) . "', payment_address_2 = '" . $this->db->escape($value['payment_address_2']) . "', payment_city = '" . $this->db->escape($value['payment_city']) . "', payment_postcode = '" . $this->db->escape($value['payment_postcode']) . "', payment_country = '" . $this->db->escape($value['payment_country']) . "', payment_country_id = '" . (int) $value['payment_country_id'] . "', payment_zone = '" . $this->db->escape($value['payment_zone']) . "', payment_zone_id = '" . (int) $value['payment_zone_id'] . "', payment_address_format = '" . $this->db->escape($value['payment_address_format']) . "', payment_custom_field = '" . $this->db->escape(isset($value['payment_custom_field']) ? json_encode($value['payment_custom_field']) : '') . "', payment_method = '" . $this->db->escape($value['payment_method']) . "', payment_code = '" . $this->db->escape($value['payment_code']) . "', shipping_firstname = '" . $this->db->escape($value['shipping_firstname']) . "', shipping_lastname = '" . $this->db->escape($value['shipping_lastname']) . "', shipping_company = '" . $this->db->escape($value['shipping_company']) . "', shipping_address_1 = '" . $this->db->escape($value['shipping_address_1']) . "', shipping_address_2 = '" . $this->db->escape($value['shipping_address_2']) . "', shipping_city = '" . $this->db->escape($value['shipping_city']) . "', shipping_postcode = '" . $this->db->escape($value['shipping_postcode']) . "', shipping_country = '" . $this->db->escape($value['shipping_country']) . "', shipping_country_id = '" . (int) $value['shipping_country_id'] . "', shipping_zone = '" . $this->db->escape($value['shipping_zone']) . "', shipping_zone_id = '" . (int) $value['shipping_zone_id'] . "', shipping_address_format = '" . $this->db->escape($value['shipping_address_format']) . "', shipping_custom_field = '" . $this->db->escape(isset($value['shipping_custom_field']) ? json_encode($value['shipping_custom_field']) : '') . "', shipping_method = '" . $this->db->escape($value['shipping_method']) . "', shipping_code = '" . $this->db->escape($value['shipping_code']) . "', comment = '" . $this->db->escape($value['comment']) . "', total = '" . (float) $value['total'] . "', order_status_id  =  '" . (int) 0 . "', affiliate_id = '" . (int) $value['affiliate_id'] . "', commission = '" . (float) $value['commission'] . "', marketing_id = '" . (int) $value['marketing_id'] . "', tracking = '" . $this->db->escape($value['tracking']) . "', language_id = '" . (int) $value['language_id'] . "', currency_id = '" . (int) $value['currency_id'] . "', currency_code = '" . $this->db->escape($value['currency_code']) . "', currency_value = '" . (float) $value['currency_value'] . "', ip = '" . $this->db->escape($value['ip']) . "', forwarded_ip = '" . $this->db->escape($value['forwarded_ip']) . "', user_agent = '" . $this->db->escape($value['user_agent']) . "', accept_language = '" . $this->db->escape($value['accept_language']) . "', date_added = NOW(), date_modified = NOW(), order_type = '" . $this->db->escape($value['order_type']) . "'");
                    $order_id = $this->db->getLastId();

                    $products = $this->model_account_order->getOrderProducts($value['order_id']);
                    foreach ($products as $key => $product) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "order_product SET order_id = '" . (int) $order_id . "', product_id = '" . (int) $product['product_id'] . "', name = '" . $this->db->escape($product['name']) . "', model = '" . $this->db->escape($product['model']) . "', quantity = '" . (int) $product['quantity'] . "', price = '" . (float) $product['price'] . "', total = '" . (float) $product['total'] . "', tax = '" . (float) $product['tax'] . "', reward = '" . (int) $product['reward'] . "'");
                        $order_product_id = $this->db->getLastId();

                        $order_options = $this->model_account_order->getOrderOptions($value['order_id'], $product['order_product_id']);
                        foreach ($order_options as $option) {
                            $this->db->query("INSERT INTO " . DB_PREFIX . "order_option SET order_id = '" . (int) $order_id . "', order_product_id = '" . (int) $order_product_id . "', product_option_id = '" . (int) $option['product_option_id'] . "', product_option_value_id = '" . (int) $option['product_option_value_id'] . "', name = '" . $this->db->escape($option['name']) . "', `value` = '" . $this->db->escape($option['value']) . "', `type` = '" . $this->db->escape($option['type']) . "'");
                        }
                    }

                    $order_vouchers = $this->model_account_order->getOrderVouchers($value['order_id']);
                    foreach ($order_vouchers as $voucher) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "order_voucher SET order_id = '" . (int) $order_id . "', description = '" . $this->db->escape($voucher['description']) . "', code = '" . $this->db->escape($voucher['code']) . "', from_name = '" . $this->db->escape($voucher['from_name']) . "', from_email = '" . $this->db->escape($voucher['from_email']) . "', to_name = '" . $this->db->escape($voucher['to_name']) . "', to_email = '" . $this->db->escape($voucher['to_email']) . "', voucher_theme_id = '" . (int) $voucher['voucher_theme_id'] . "', message = '" . $this->db->escape($voucher['message']) . "', amount = '" . (float) $voucher['amount'] . "'");

                        $order_voucher_id = $this->db->getLastId();

                        $voucher_id = $this->model_extension_total_voucher->addVoucher($value['order_id'], $voucher);

                        $this->db->query("UPDATE " . DB_PREFIX . "order_voucher SET voucher_id = '" . (int) $voucher_id . "' WHERE order_voucher_id = '" . (int) $order_voucher_id . "'");
                    }

                    $order_totals = $this->model_account_order->getOrderTotals($value['order_id']);
                    foreach ($order_totals as $key => $total) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET order_id = '" . (int) $order_id . "', code = '" . $this->db->escape($total['code']) . "', title = '" . $this->db->escape($total['title']) . "', `value` = '" . (float) $total['value'] . "', sort_order = '" . (int) $total['sort_order'] . "'");
                    }



                $deniedOrderAutoshipStatus = $this->model_account_creditcard->deniedOrderAutoshipStatus($value['order_id']);

               $autopaymentdetails = $this->autopayment($value['customer_id'], $order_id, $value['total'], $value['currency_code']);
                    
                    sleep(2);

                    if($autopaymentdetails){
                        mail('maninder@nascenture.com', 'Monthly Order Successfull', 'Order ID:'.$order_id);
                        mail('christine.bieri@itgs.ca', 'Monthly Order Successfull', 'Order ID:'.$order_id);
                        
                    }else{
                        mail('maninder@nascenture.com', 'Monthly Order Failed', 'Order ID:'.$order_id);
                        mail('christine.bieri@itgs.ca', 'Monthly Order Failed', 'Order ID:'.$order_id);
                      
                    }
                    sleep(2);

                $json = array(
                    'msg' => 'Auto-ship is successfully Start For denied order.',
                    'denied_status' => $deniedOrderAutoshipStatus
                );


                $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($json));
            }
        }
    }

    public function autopayment($customer_id, $order_id, $total, $currency_code) {

        include_once(DIR_APPLICATION . '/controller/gateway_tps_xml.php');
        $this->load->model('checkout/order');
        $this->load->model('account/order');
        $this->load->model('setting/setting');
        $order_info = array();
        ///'CAD'
        switch ($currency_code) {
            case $this->config->get('globalonepay_secure_currency1') :
                $currency = $this->config->get('globalonepay_secure_currency1');
                $terminalId = $this->config->get('globalonepay_secure_terminal1');
                $secret = $this->config->get('globalonepay_secure_secret1');
                break;
            case $this->config->get('globalonepay_secure_currency2') :
                $currency = $this->config->get('globalonepay_secure_currency2');
                $terminalId = $this->config->get('globalonepay_secure_terminal2');
                $secret = $this->config->get('globalonepay_secure_secret2');
                break;
            case $this->config->get('globalonepay_secure_currency3') :
                $currency = $this->config->get('globalonepay_secure_currency3');
                $terminalId = $this->config->get('globalonepay_secure_terminal3');
                $secret = $this->config->get('globalonepay_secure_secret3');
        }

        $testAccount = false;
        if ($this->config->get('globalonepay_secure_test')) {
            $testAccount = true;
        }

        $gateway = 'globalone';

        // Order details //
        $customer_id = $customer_id; //1;
        $order_id = $order_id; //120;
        $amount = number_format($total,2); //10;
        //get stored cardref details  //
        $refDetails = $this->model_account_order->getPaymentrefDetails($customer_id);
        //echo "<pre>"; print_r($refDetails); die;

        if (!empty($refDetails)) {
            $cardNumber = $refDetails['responseref'];
            $cardType = 'SECURECARD';
            //$email = ($this->config->get('globalonepayhpp_send_receipt') == '1' ? $order_info['email'] : '');
            # These values are specific to the transaction.
            $orderId = $order_id;
            # These fields are for AVS (Address Verification Check). This is only appropriate in the UK and the US.
            $cvv = '';
            $email = '';
            $mobileNumber = '';
            $issueNo = '';
            $address1 = '';         # (optional) This is the first line of the cardholders address.
            $address2 = '';         # (optional) This is the second line of the cardholders address.
            $postcode = '';         # (optional) This is the cardholders post code.
            $country = '';          # (optional) This is the cardholders country name.
            $phone = '';            # (optional) This is the cardholders home phone number.
            # eDCC fields. Populate these if you have retreived a rate for the transaction, offered it to the cardholder and they have accepted that rate.
            $cardCurrency = '';     # (optional) This is the three character ISO currency code returned in the rate request.
            $cardAmount = '';       # (optional) This is the foreign currency transaction amount returned in the rate request.
            $conversionRate = '';       # (optional) This is the currency conversion rate returned in the rate request.
            # 3D Secure reference. Only include if you have verified 3D Secure throuugh the GlobalOne MPI and received an MPIREF back.
            $mpiref = '';           # This should be blank unless instructed otherwise by GlobalOne.
            $deviceId = '';         # This should be blank unless instructed otherwise by GlobalOne.

            $autoready = '';        # (optional) (Y/N) Whether or not this transaction should be marked with a status of "ready" as apposed to "pending".
            $multicur = true;      # This should be false unless instructed otherwise by GlobalOne.

            $description = '';      # (optional) This can is a description for the transaction that will be available in the merchant notification e-mail and in the SelfCare  system.
            $autoReady = '';        # (optional) Y or N. Automatically set the transaction to a status of Ready in the batch. If not present the terminal default will be used.
            $isMailOrder = false;   #If true the transaction will be processed as a Mail Order transaction. This is only for use with Mail Order enabled Terminal IDs.
            # Set up the authorisation object

            $auth = new XmlAuthRequest($terminalId, $orderId, $currency, $amount, $cardNumber, $cardType);
            if ($cardType != "SECURECARD")
                $auth->SetNonSecureCardCardInfo($cardExpiry, $cardHolderName);
            if ($cvv != "")
                $auth->SetCvv($cvv);
            if ($cardCurrency != "" && $cardAmount != "" && $conversionRate != "")
                $auth->SetForeignCurrencyInformation($cardCurrency, $cardAmount, $conversionRate);
            if ($email != "")
                $auth->SetEmail($email);
            if ($mobileNumber != "")
                $auth->SetMobileNumber($mobileNumber);
            if ($description != "")
                $auth->SetDescription($description);

            if ($issueNo != "")
                $auth->SetIssueNo($issueNo);
            if ($address1 != "" && $address2 != "" && $postcode != "")
                $auth->SetAvs($address1, $address2, $postcode);
            if ($country != "")
                $auth->SetCountry($country);
            if ($phone != "")
                $auth->SetPhone($phone);

            if ($mpiref != "")
                $auth->SetMpiRef($mpiref);
            if ($deviceId != "")
                $auth->SetDeviceId($deviceId);

            if ($multicur)
                $auth->SetMultiCur();
            if ($autoready)
                $auth->SetAutoReady($autoready);
            if ($isMailOrder)
                $auth->SetMotoTrans();

            # Perform the online authorisation and read in the result
            $response = $auth->ProcessRequestToGateway($secret, $testAccount, $gateway);
          

            $expectedResponseHash = md5($terminalId . $response->UniqueRef() . ($multicur == true ? $currency : '') . $amount . $response->DateTime() . $response->ResponseCode() . $response->ResponseText() . $response->BankResponseCode() . $secret);

            if ($response->IsError()) {
                //echo  'AN ERROR OCCURED! You transaction was not processed. Error details: ' . $response->ErrorString();
                return false;
             } elseif($expectedResponseHash == $response->Hash()) {
                switch ($response->ResponseCode()) {
                    case "A" :  # -- If using local database, update order as Authorised.
                        //echo 'Payment Processed successfully. Thanks you for your order.';
                        $this->model_checkout_order->addOrderHistory($orderId, $this->config->get('globalonepay_secure_status_pendingAutoship'), $response->ResponseText(), FALSE);
                        return true;

                        break;
                    case "R" :
                    case "D" :
                    case "C" :
                    case "S" :

                    default :  # -- If using local database, update order as declined/failed --
                        // echo  'PAYMENT DECLINED! Please try again with another card. Bank response: ' . $response->ResponseText();
                         $this->model_checkout_order->addOrderHistory($orderId, $this->config->get('globalonepay_secure_status_declined'), $response->ResponseText(), FALSE);
                        return false;
                }

                $request_data = $auth->getRequestInfo();
                $request_data['customer_id'] = $customer_id;
                $request_data['order_id'] = $order_id;
                $request_data['order_type'] = 'monthly';
                $this->saveGlobaloneXmldata($request_data, $response);
            } else {               
                  
                 $uniqueReference = $response->UniqueRef();              
                   // $this->session->data['error'] = 'PAYMENT FAILED: INVALID RESPONSE HASH. Please contact to clarify if you will get charged for this order.';             
                    $this->session->data['error'] = $this->language->get('invalid_hash_error');      
                    $this->model_checkout_order->addOrderHistory( $orderId , 
                    $this->config->get('globalonepay_secure_status_failed'), 
                    $response->ResponseText(), FALSE );             
                    $json['redirect'] = $this->url->link('checkout/checkout');

             } 

        } else {

            return false;
            //die('no Reference details for payment');
        }

        exit;
    }

    public function saveGlobaloneXmldata($request_data, $response) {

        $globalone_request_id = $this->model_checkout_order->saveGlobaloneXmlRequest($request_data);
        if ($globalone_request_id) {
            $response_data = array(
                'globalone_request_id' => $globalone_request_id,
                'isError' => $response->IsError(),
                'errorString' => $response->ErrorString(),
                'responseCode' => $response->ResponseCode(),
                'responseText' => $response->ResponseText(),
                'approvalCode' => $response->ApprovalCode(),
                'authorizedAmount' => $response->AuthorizedAmount(),
                'dateTime' => $response->DateTime(),
                'avsResponse' => $response->AvsResponse(),
                'cvvResponse' => $response->CvvResponse(),
                'uniqueRef' => $response->UniqueRef(),
                'hash' => $response->Hash(),
            );
            $this->model_checkout_order->saveGlobaloneXmlResponse($response_data);
        }
    }

}
