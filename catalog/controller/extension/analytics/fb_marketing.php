<?php
class ControllerExtensionAnalyticsFbMarketing extends Controller { 
    
    public function index() {
		$fb_data = $this->getSettingData();

		foreach ($fb_data as $key => $value) {
			$data_key = substr($key, strlen('fb_marketing_'));
			$$data_key = $value;
		}
		
		if (!empty($status) && !empty($pixel_id) ) {
			$fb_pixel_code = "<script>!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js'); " . (!empty($manual_only_mode) ? "fbq('set', 'autoConfig', 'false', '%s');" : "" ) . " fbq('init', '%s'";
			
			if (!empty($advanced_matching) && !empty($advanced_matching_data)) {
				$fb_pixel_code .= $this->getAdvancedMatchingData($advanced_matching_data);
			}
			
			$fb_pixel_code .= ");";
			
			$fb_pixel_code_postfix = '</script>';
			
			$facebook_pixel = str_replace('%s', $pixel_id , $fb_pixel_code);
			
			$dynamic_event = !empty($this->request->post['dynamic_event']) ? $this->request->post['dynamic_event'] : '' ;
			
			// Events
			$event = $this->getEvent($events, $product_catalog_id, $dynamic_event);
			
			// Bring all parts together	
			$pixel = $facebook_pixel . $event . $fb_pixel_code_postfix;	
			
			return html_entity_decode($pixel, ENT_QUOTES, 'UTF-8');
			
		}
		
	}
	
	protected function getEvent($event_data, $product_catalog_id = '', $dynamic_event = '') {
		
		$event = '';
		
		if(!empty($dynamic_event)) {
			switch ($dynamic_event) {
				case ('CompleteRegistration') : 
					$route = 'account/success';	
					break;
			} 
		} else if(isset($this->request->get['route'])) {
			$route = $this->request->get['route'];
		} else {
			$route = '';
		}
		
		switch ($route) {
			case 'checkout/success' : 
				if (!empty($event_data['purchase']['status']) && !empty($this->session->data['last_order_id'])) {
					$options = $this->getEventOptions($event_data, 'purchase', $product_catalog_id);
					$event = 'fbq(\'track\', \'Purchase\', {' . $options . '});';
				}
				break;
			case 'product/product':
				if (!empty($event_data['viewcontent']['status'])) {				
					$options = $this->getEventOptions($event_data, 'viewcontent', $product_catalog_id);
					$event = "fbq('track', 'ViewContent', {" . $options . "});";
				}
				break;
			case 'checkout/checkout';	
			case 'quickcheckout/checkout';
				if (!empty($event_data['initiatecheckout']['status'])) {				
					$options = $this->getEventOptions($event_data, 'initiatecheckout', $product_catalog_id);
					$event = "fbq('track', 'InitiateCheckout', {" . $options . "});";
				}
				break;
			case 'account/success';
			case 'affiliate/success';
				if (!empty($event_data['completeregistration']['status'])) {				
					$options = $this->getEventOptions($event_data, 'completeregistration', $product_catalog_id);
					$event = "fbq('track', 'CompleteRegistration', {" . $options . "});";
				}
				break;
			case 'product/search';
				if (!empty($event_data['search']['status'])) {				
					$options = $this->getEventOptions($event_data, 'search', $product_catalog_id);
					$event = 'fbq(\'track\', \'Search\', {' . $options . '});';
				}
				break;
			default :
				if (!empty($event_data['pageview']['status'])) {				
					$options = $this->getEventOptions($event_data, 'pageview', $product_catalog_id);
					$event = 'fbq(\'track\', \'PageView\', {'. $options . '});';	
				}
		}
		
		return $event;
	}
	
	protected function getEventOptions($event_data, $event, $product_catalog_id) {
	
		$vat_incl = $this->config->get('fb_marketing_values_vat_inc');
		$fb_currency = $this->config->get('config_currency');
		$pid = $this->config->get('fb_marketing_pid');
		
		$products = array();
		$product_count = 0;
		$options = '';
		
		$contents = array();
		
		switch ($event) {
			case 'purchase':
				
				if(!empty($this->session->data['last_order_id'])) {
					$order_id = $this->session->data['last_order_id'];
				}
				
				// var_dump($order_id);
				
				if (!empty($order_id)) {
					// Complete Checkout
					$query = $this->db->query("SELECT op.*, p.sku, p.upc, p.ean, p.jan, p.mpn, p.isbn FROM `" . DB_PREFIX . "order_product` op LEFT JOIN `" . DB_PREFIX . "product` p ON op.product_id = p.product_id WHERE `order_id` = '" . (int)$order_id . "'");
					foreach($query->rows as $product) {
						$content = array();
						if(!empty($product[$pid])) {
							$products[] = '\'' . $product[$pid] . '\'';
							$content['id'] = $product[$pid];
						} else {
							$products[] = '\'' . $product['product_id'] . '\'';
							$content['id'] = $product['product_id'];
						}	
						
						$product_count += $product['quantity'];
						
						$content['quantity'] = $product['quantity'];
						
						if(!empty($vat_incl)) {
							$content['item_price'] = $this->formatValue($product['price'] + $product['tax']);
						} else {
							$content['item_price'] = $this->formatValue($product['price']);
						}
							
						$contents[] = $content;
					}
					
					$cart_products = implode(',' , $products);
					$order_value = 0;
					
					if(!empty($vat_incl)) {
						$query = $this->db->query("SELECT total FROM `" . DB_PREFIX . "order` WHERE `order_id` = '" . (int)$order_id . "'");
						if ($query->num_rows) {
							$order_value = $query->row['total'];
						}
					} else {
						$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE `order_id` = '" . (int)$order_id . "'");
						if ($query->num_rows) {
							$totals = $query->rows;
						
							$sort_order = array();

							foreach ($totals as $key => $value) {
								$sort_order[$key] = $value['sort_order'];
							}
							
							array_multisort($sort_order, SORT_ASC, $totals);
							
							array_pop($totals);
							
							foreach ($totals as $total) {
								if ($total['code'] != 'tax') {
									$order_value = $order_value +  $total['value'];
								}
							}
						}
					}

					$options .= 'value: \'' . $this->formatValue($order_value) . '\'';
					$options .= ', currency: \'' . $fb_currency . '\'';
					$options .= ', num_items: \'' . $product_count . '\'';
					$options .= ', content_ids: [' . $cart_products . ']';
					$options .= ', content_type: \'product\'';
					
					if(!empty($contents)) {
						$contents_json = array();
						foreach ($contents as $v) {
							$contents_json[] = json_encode($v) ;
						}
						$options .= ', contents: [' . implode(',' , $contents_json) . ']';
					}
									
					$options .= !empty($product_catalog_id) ? ', product_catalog_id: \''. $product_catalog_id . '\'' : '';
					
					unset($order_id);
					
				}
				break;
				
			case 'viewcontent':
				// View Content
				$this->load->model('catalog/product');
				$product = $this->model_catalog_product->getProduct($this->request->get['product_id']);
				
				$value = 0;
				$content = array();
				
				$price = empty($product['special']) ? $product['price'] : $product['special'] ;
				if(!empty($vat_incl)) {
					$value  = $this->tax->calculate($price, $product['tax_class_id']);
				} else {
					$value = $price;
				}
				
				if(!empty($product[$pid])) {
					$content['id'] = $product[$pid];
				} else {
					$content['id'] = $product['product_id'];
				}
				
				$content['quantity'] = $product['minimum'];
				$content['item_price'] = $value;
			
				$options .= 'value: \'' . $this->formatValue($value)  . '\'';
				$options .= ', currency: \'' . $fb_currency . '\'';
				
				if(!empty($product[$pid])) {
					$options .= ', content_ids: [\'' . $product[$pid] . '\']';
				} else {
					$options .= ', content_ids: [\'' . $product['product_id'] . '\']';
				}
				
				$options .= ', content_name: \'' . $this->db->escape($product['name']) . '\'';
				$options .= ', content_type: \'product\'';
				
				if(!empty($content)) {
					$options .= ', contents: [' . json_encode($content) . ']';
				}
				
				$options .= !empty($product_catalog_id) ? ', product_catalog_id:\''. $product_catalog_id . '\'' : '';

				break;
			case 'initiatecheckout';
				// Initiate Checkout
				$cart = $this->cart->getProducts();
				foreach($cart as $cart_product) {
					$this->load->model('catalog/product');
					$product = $this->model_catalog_product->getProduct($cart_product['product_id']);
					
					$content = array();
					
					if(!empty($product[$pid])) {
						$products[] = '\'' . $product[$pid] . '\'';
						$content['id'] = $product[$pid];
					} else {
						$products[] = '\'' . $product['product_id'] . '\'';
						$content['id'] = $product['product_id'];
					}
					
					$content['quantity'] = $cart_product['quantity'];
						
					if(!empty($vat_incl)) {
						$content['item_price'] = $this->formatValue($this->tax->calculate($cart_product['price'],$cart_product['tax_class_id']));
					} else {
						$content['item_price'] = $this->formatValue($cart_product['price']);
					}
						
					$contents[] = $content;				
				}
				
				$cart_products = implode(',', $products);
				
				$value = $this->getCartTotal($vat_incl);
				
				$options .= 'value: \'' . $this->formatValue($value) . '\'';
				$options .= ', currency: \'' . $fb_currency . '\'';
				$options .= ', content_ids: [' . $cart_products  . ']';
				$options .= ', num_items: \'' . $this->cart->countProducts() . '\'';
				$options .= ', content_type: \'product\'';
				
				if(!empty($contents)) {
					$contents_json = array();
					foreach ($contents as $v) {
						$contents_json[] = json_encode($v) ;
					}
					$options .= ', contents: [' . implode(',' , $contents_json) . ']';
				}
				
				$options .= !empty($product_catalog_id) ? ', product_catalog_id:\''. $product_catalog_id . '\'' : '';
			
				break;
			case 'completeregistration';
				// Complete Registration
				$value = $event_data['completeregistration']['value'];
				
				$options .= 'value: \'' . $this->formatValue($value) . '\'';
				$options .= ', currency: \'' . $fb_currency . '\'';

				break;
			case 'search';
				// Search
				$search_string = (!empty($this->request->get['search']) ? $this->request->get['search'] : '' );	
				if (!empty($search_string)){			
					$this->load->model('catalog/product');
					$search_results = $this->model_catalog_product->getProducts(array('filter_name' => $search_string));
					
					$content_ids = '';
					$content = array();
									
					foreach($search_results as $key => $product) {
						if(!empty($product[$pid])) {
							$products[] = '\'' . $product[$pid] . '\'';
							$content['id'] = $product[$pid];
						} else {
							$products[] = '\'' . $product['product_id'] . '\'';
							$content['id'] = $product['product_id'];
						}
						
						$content['quantity'] = $product['minimum'];
						
						if(!empty($vat_incl)) {
							$content['item_price'] = $this->formatValue($this->tax->calculate($product['price'],$product['tax_class_id']));
						} else {
							$content['item_price'] = $this->formatValue($product['price']);
						}
						
						$contents[] = $content;			
					}
					
					$content_ids = implode(',',  $products);
					$value = $event_data['search']['value'];
					
					$options .= 'value: \'' . $this->formatValue($value) . '\'';
					$options .= ', currency: \'' . $fb_currency . '\'';
					$options .= ', search_string: \'' . $this->db->escape($search_string) . '\'';
					$options .= ', content_ids: [' . $content_ids  . ']';
					$options .= ', content_type: \'product\'';
					
					if(!empty($contents)) {
						$contents_json = array();
						foreach ($contents as $v) {
							$contents_json[] = json_encode($v) ;
						}
						$options .= ', contents: [' . implode(',' , $contents_json) . ']';
					}
					
					$options .= !empty($product_catalog_id) ? ', product_catalog_id:\''. $product_catalog_id . '\'' : '';
				}
				break;
			case 'pageview':
			default :
				// Default PageView, displayed on all pages
				$value = $event_data['pageview']['value'];
					
				$options .= 'value: \'' . $this->formatValue($value) . '\'';
				$options .= ', currency: \'' . $fb_currency . '\'';
		}
		
		return $options;
	}
	
	protected function getAdvancedMatchingData($data_types) {
		
		if ($this->customer->isLogged() && !empty($data_types)){
			$advanced_matching_data = ", {";
			
			foreach ($data_types as $key => $status) {
				$advanced_matching_data .= "" . $key . ": '" . $this->getTypeData($key) . "',";
				
			}
			
			$advanced_matching_data = substr($advanced_matching_data,0,-1);
			$advanced_matching_data .= "}";
			
			return $advanced_matching_data;
		} else if (!empty($this->session->data['last_order_id']) && !empty($data_types)){
			$advanced_matching_data = ", {";
			
			foreach ($data_types as $key => $status) {
				$advanced_matching_data .= "" . $key . ": '" . $this->getTypeDataOrder($key) . "',";
				
			}
			
			$advanced_matching_data = substr($advanced_matching_data,0,-1);
			$advanced_matching_data .= "}";
			
			return $advanced_matching_data;
		}
	}
	
	protected function getTypeData($type) { 
		switch ($type) {
		
			case 'em':
				return strtolower(trim($this->customer->getEmail()));
				break;
			case 'fn': 
				return strtolower(trim($this->customer->getFirstName()));
				break;
			case 'ln': 
				return strtolower(trim($this->customer->getLastName()));
				break;
			case 'ph': 
				return $this->cleanPhoneNumber($this->customer->getTelephone());
				break;
			case 'ct':
				$address_id = $this->customer->getAddressID();
				if(!empty($address_id )) {
					$this->load->model('account/address');
					$address = $this->model_account_address->getAddress($this->customer->getAddressID());
			
					if(!empty($address['city'])) {
						return strtolower(trim($address['city']));
					}
				}
				break;
			case 'st':
				$address_id = $this->customer->getAddressID();
				if(!empty($address_id )) {
					$this->load->model('account/address');
					$address = $this->model_account_address->getAddress($this->customer->getAddressID());
			
					if(!empty($address['zone_id'])) {
						return strtolower(trim($this->getState($address['zone_id'])));
					}
				}
				break;		
			case 'zp':
				$address_id = $this->customer->getAddressID();
				if(!empty($address_id )) {
					$this->load->model('account/address');
					$address = $this->model_account_address->getAddress($this->customer->getAddressID());
			
					if(!empty($address['postcode'])) {
						return (int)(trim($address['postcode']));
					}
				}
				break;		
		}
	}
	
	protected function getTypeDataOrder($type) { 
		if (!empty($this->session->data['last_order_id'])) {
			$this->load->model('checkout/order');
		
			$order = $this->model_checkout_order->getOrder($this->session->data['last_order_id']);
		
			switch ($type) {
				case 'em':
					return strtolower(trim($order['email']));
					break;
				case 'fn': 
					return strtolower(trim($order['firstname']));
					break;
				case 'ln': 
					return strtolower(trim($order['lastname']));
					break;
				case 'ph': 
					return $this->cleanPhoneNumber($order['telephone']);
					break;
				case 'ct':
					if(!empty($order['payment_city'])) {
						return strtolower(trim($order['payment_city']));
					}
					break;
				case 'st':
					if(!empty($order['payment_zone_id'])) {
						return strtolower(trim($this->getState($order['payment_zone_id'])));
					}
					break;		
				case 'zp':
					if(!empty($order['payment_postcode'])) {
						return (int)(trim($order['payment_postcode']));
					}
					break;		
			}
		}
	}
	
	protected function getSettingData () {
		$store_id = $this->config->get('config_store_id');
		$this->load->model('setting/setting');
		return $this->model_setting_setting->getSetting('fb_marketing',$store_id);
	}
	
	protected function cleanPhoneNumber($number) {
		$number = str_replace('+', '00', $number);
		$number = str_replace(' ', '', $number);
		return $number;
	}
	
	protected function getState($zone_id) {
		$this->load->model('localisation/zone');
		$state = $this->model_localisation_zone->getZone($zone_id);
		return $state['code'];
	}
	
	protected function formatValue ($value) {
		
		if(empty($value)) {
			$value = 0.00;
		}
		return number_format($value, 2 , '.' , '' );
	}
	
	protected function getCartTotal($vat_incl) {
		// Totals
		$order_data = array();

		$totals = array();
		$taxes = $this->cart->getTaxes();
		$total = 0;

		// Because __call can not keep var references so we put them into an array. 
		$total_data = array(
			'totals' => &$totals,
			'taxes'  => &$taxes,
			'total'  => &$total
		);
	
		$this->load->model('extension/extension');

		$sort_order = array();

		$results = $this->model_extension_extension->getExtensions('total');

		foreach ($results as $key => $value) {
			$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
		}

		array_multisort($sort_order, SORT_ASC, $results);

		foreach ($results as $result) {
			if ($this->config->get($result['code'] . '_status')) {
				
				if (version_compare(VERSION, '2.3.0.0', '>=')) {
					$this->load->model('extension/total/' . $result['code']);
					// We have to put the totals in an array so that they pass by reference.
					$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
				} else {
					$this->load->model('total/' . $result['code']);
					// We have to put the totals in an array so that they pass by reference.
					$this->{'model_total_' . $result['code']}->getTotal($total_data);
				}

			}
		}

		$sort_order = array();

		foreach ($totals as $key => $value) {
			$sort_order[$key] = $value['sort_order'];
		}

		array_multisort($sort_order, SORT_ASC, $totals);
		
		if(!empty($vat_incl)) {	

			$cart_total = end($totals);
			return $cart_total['value'];

		} else {
			
			array_pop($totals);
			$cart_total = 0;
			
			foreach ($totals as $total) {
				if ($total['code'] != 'tax') {
					$cart_total += $total['value'];
				}
			}
			
			return $cart_total;
			
		}	
	}
	
	public function addToCart ($route,$args,&$output) {
		$this->insertPixel('AddToCart');
	}
	
	public function addToWishlist ($route,$args,&$output) {
		$this->insertPixel('AddToWishlist');
	}
	
	public function CompleteRegistration ($route,$args,$output) {
		if($route == 'checkout/register/save') {
			$this->session->data['saveRegistration'] = true;
			$this->insertPixel('CompleteRegistration');
		} else {
			if(!empty($this->session->data['saveRegistration']) && !empty($this->session->data['completeRegistrationPixel'])) {
				$output = $this->response->getOutput();
				$output .= $this->session->data['completeRegistrationPixel'];
				
				unset($this->session->data['completeRegistrationPixel']);
				unset($this->session->data['saveRegistration']);
				
				$this->response->setOutput($output);
			}
		}
	}
	
	public function CompleteRegistration2 ($route,$args,$output) {
		//$this->insertPixel('CompleteRegistration2');
		
		$output = $this->index();
		$this->response->setOutput($output);
		
	}
	
	public function AddPaymentInfo ($route,$args,&$output) {
		$this->insertPixel('AddPaymentInfo');
	}
	
	protected function insertPixel($event) {
		
		$settings = $this->getSettingData();
		$pid = $this->config->get('fb_marketing_pid');
		
		if (!empty($settings['fb_marketing_events'][strtolower($event)]['status'])) {
		
			switch ($event) {
				case 'AddToWishlist':
				case 'AddToCart':
					if (isset($this->request->post['product_id'])) {
						$product_id = (int)$this->request->post['product_id'];
					} else {
						$product_id = 0;
					}
					
					// Hijack response output	
					$response = $this->response->getOutput();
					$string = json_decode($response, true);
		
					if(!empty($product_id ) && !empty($string['success'])) {
						
						$content = array();	
						
						if($event == 'AddToWishlist') {
							$this->load->model('catalog/product');
							$product_info = $this->model_catalog_product->getProduct($product_id);
						} else {
						 	$cart_products = $this->cart->getProducts();
							foreach($cart_products as $product) {
								if($product['product_id'] == $product_id) {
									$product_info = $product;
								}
							}
						}
						
						if(!empty($product_info)) {
									
							$price = $product_info['price'] ;	
							$pixel = '<script><!--';
							
							if (!empty($settings['fb_marketing_values_vat_inc'])) {
								$value = $this->tax->calculate($price,$product_info['tax_class_id']);
							} else {
								$value = $price;
							}
							
							if(!empty($product_info[$pid])) {
								$content_id = $product_info[$pid];
							} else {
								$content_id = $product_info['product_id'];
							}
							
							$content['id'] = $content_id;
							
							if($event == 'AddToWishlist') { 
								$content['quantity'] = 1 ;
							} else {
								$content['quantity'] = $product['quantity'] ;
							}
							
							$content['item_price'] = $value;
							
							$pixel .= 'fbq([\'track\', \'' . $event . '\', { value : \'' . $this->formatValue($value) . '\', currency: \'' . $this->config->get('config_currency'). '\', content_ids: [\'' . $content_id . '\'], content_type: \'product\', content_name: \'' . $this->db->escape($product_info['name']) . '\', num_items: \'' . $product_info['quantity'] . '\'';
							
							if(!empty($content)) {
								$pixel .= ', contents: [' . json_encode($content) . ']';
							}
							
							if(!empty($settings['fb_marketing_product_catalog_id'])) {
								$pixel .= ' , product_catalog_id: \'' . $settings['fb_marketing_product_catalog_id'] . '\'';
							}
				
							$pixel .= '}]);';
							$pixel .= '//--></script>';
				
							$string['success'] .= $pixel;
				
							$this->response->addHeader('Content-Type: application/json');
							$this->response->setOutput(json_encode($string));
				
						}
					}	
					break;
				case 'CompleteRegistration' :			
					if(!empty($this->session->data['saveRegistration'])) {
						$output = json_decode($this->response->getOutput(), true);
						if(empty($output)) {
							$this->request->post['dynamic_event'] = 'CompleteRegistration';
							$this->session->data['completeRegistrationPixel'] = $this->index();			
						}
					}
					break;
				
				case 'AddPaymentInfo' :
					$output = $this->response->getOutput();
					$value = $settings['fb_marketing_events'][strtolower($event)]['value'];
					$pixel = '<script><!--';
					$pixel .= 'fbq([\'track\', \'' . $event . '\', { value : \'' . $this->formatValue($value) . '\', currency: \'' . $this->config->get('config_currency'). '\'';
					
					// Contents 
					$cart = $this->cart->getProducts();
					foreach($cart as $cart_product) {
						$this->load->model('catalog/product');
						$product = $this->model_catalog_product->getProduct($cart_product['product_id']);
					
						$content = array();
					
						if(!empty($product[$pid])) {
							$products[] = '\'' . $product[$pid] . '\'';
							$content['id'] = $product[$pid];
						} else {
							$products[] = '\'' . $product['product_id'] . '\'';
							$content['id'] = $product['product_id'];
						}
					
						$content['quantity'] = $cart_product['quantity'];
						
						if(!empty($vat_incl)) {
							$content['item_price'] = $this->formatValue($this->tax->calculate($cart_product['price'],$cart_product['tax_class_id']));
						} else {
							$content['item_price'] = $this->formatValue($cart_product['price']);
						}
						
						$contents[] = $content;				
					}
				
					$cart_products = implode(',', $products);
					
					$pixel .= ', content_ids: [' . $cart_products  . ']';
					$pixel .= ', content_type: \'product\'';
					
					if(!empty($contents)) {
						$contents_json = array();
						foreach ($contents as $v) {
							$contents_json[] = json_encode($v) ;
						}
						$pixel .= ', contents: [' . implode(',' , $contents_json) . ']';
					}
					
					if(!empty($settings['fb_marketing_product_catalog_id'])) {
						$pixel .= ' , product_catalog_id: \'' . $settings['fb_marketing_product_catalog_id'] . '\'';
					}
					
					$pixel .= '}])';
					$pixel .= '//--></script>';
					
					$output .= $pixel;
				
					$this->session->data['AddPaymentInfoPixel'] = $pixel; 
					$this->response->setOutput($output);
					break;	
			}
		}
	}
	
	public function addOrder ($route,$args) {
		
		//if (!empty($this->session->data['last_order_id'])) {
		//	unset($this->session->data['last_order_id']);	
		//}
		
		if (!empty($this->session->data['order_id'])) {
			$this->session->data['last_order_id'] = $this->session->data['order_id'];
		}
		
		//$this->log->write('AddOrder triggered - this->session->data[\'last_order_id\'] = ' . (!empty($this->session->data['last_order_id']) ? $this->session->data['last_order_id'] : 'empty/unset') );
	}
	
	
	private function path () {
		if (version_compare(VERSION, '2.3.0.0', '>=')) {
			return 'extension/analytics/fb_marketing';
		} else {
			return 'analytics/fb_marketing';
		}
	}
	
	private function folder () {
		if (version_compare(VERSION, '3.0.0.0', '>=')) {
			return 'setting';
		} else {
			return 'extension';
		}	
	}
	
	private function prefix () {
		if (version_compare(VERSION, '3.0.0.0', '>=')) {
			return 'analytics_';
		} else {
			return '';
		}	
	}
}