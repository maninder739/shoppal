<?php
//==============================================================================
// Individual Shipping v302.1
// 
// Author: Clear Thinking, LLC
// E-mail: johnathan@getclearthinking.com
// Website: http://www.getclearthinking.com
// 
// All code within this file is copyright Clear Thinking, LLC.
// You may not copy or reuse code within this file without written permission.
//==============================================================================

class ControllerExtensionShippingIndividualShipping extends Controller {  
	private $type = 'shipping';
	private $name = 'individual_shipping';
	
	public function apply() {
		$code = (version_compare(VERSION, '3.0', '<') ? '' : $this->type . '_') . $this->name;
		
		$titles = array($this->config->get($code . '_total_shipping_text_' . $this->session->data['language']));
		$cost = 0;
		$tax_class_id = $this->session->data['shipping_methods'][$this->name]['quote'][$this->name]['tax_class_id'];
		
		foreach ($this->request->post as $key => $value) {
			if (strpos($value, '::') === false) continue;
			$info = explode('::', $value);
			$title = str_replace(
				array('[name]', '[method]', '[cost]'),
				array($info[0], $info[1], $this->currency->format($info[2], $this->session->data['currency'])),
				$this->config->get($code . '_line_item_text_' . $this->session->data['language'])
			);
			$titles[] = html_entity_decode($title, ENT_QUOTES, 'UTF-8');
			$cost += $info[2];
			if ($info[3]) {
				$tax_class_id = $info[3];
			}
		}
		
		$this->session->data[$this->name] = $this->request->post;
		
		$this->session->data['shipping_methods'][$this->name]['quote'][$this->name]['title'] = implode('<br />', $titles) . '<span style="display: none">';
		$this->session->data['shipping_methods'][$this->name]['quote'][$this->name]['cost'] = $cost;
		$this->session->data['shipping_methods'][$this->name]['quote'][$this->name]['tax_class_id'] = $tax_class_id;
		$this->session->data['shipping_methods'][$this->name]['quote'][$this->name]['text'] = $this->currency->format($this->tax->calculate($cost, $tax_class_id, $this->config->get('config_tax')), $this->session->data['currency']);
		
		echo $this->session->data['shipping_methods'][$this->name]['quote'][$this->name]['text'];
  	}
}
?>