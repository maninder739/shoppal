<?php
class ControllerExtensionPaymentPPExpressMonthly extends Controller {
	public function index() {
		$this->load->language('extension/payment/pp_express_monthly');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['continue'] = $this->url->link('extension/payment/pp_express_monthly/checkout', '', true);

		unset($this->session->data['paypal']);

		return $this->load->view('extension/payment/pp_express_monthly', $data);
	}

	public function checkout() {
		
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			 
			$this->response->redirect($this->url->link('checkout/cart'));
		}

		$this->load->model('extension/payment/pp_express_monthly');
		$this->load->model('tool/image');
		$this->load->model('checkout/order');

		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

		$max_amount = $this->cart->getTotal() * 1.5;
		$max_amount = $this->currency->format($max_amount, $this->session->data['currency'], '', false);

		if ($this->cart->hasShipping()) {
			$shipping = 0;

			// PayPal requires some countries to use zone code (not name) to be sent in SHIPTOSTATE
			$ship_to_state_codes = array(
				'30', // Brazil
				'38', // Canada
				'105', // Italy
				'138', // Mexico
				'223', // USA
			);

			if (in_array($order_info['shipping_country_id'], $ship_to_state_codes)) {
				$ship_to_state = $order_info['shipping_zone_code'];
			} else {
				$ship_to_state = $order_info['shipping_zone'];
			}

			$data_shipping = array(
				'PAYMENTREQUEST_0_SHIPTONAME'        => html_entity_decode($order_info['shipping_firstname'] . ' ' . $order_info['shipping_lastname'], ENT_QUOTES, 'UTF-8'),
				'PAYMENTREQUEST_0_SHIPTOSTREET'      => html_entity_decode($order_info['shipping_address_1'], ENT_QUOTES, 'UTF-8'),
				'PAYMENTREQUEST_0_SHIPTOSTREET2'     => html_entity_decode($order_info['shipping_address_2'], ENT_QUOTES, 'UTF-8'),
				'PAYMENTREQUEST_0_SHIPTOCITY'        => html_entity_decode($order_info['shipping_city'], ENT_QUOTES, 'UTF-8'),
				'PAYMENTREQUEST_0_SHIPTOSTATE'       => html_entity_decode($ship_to_state, ENT_QUOTES, 'UTF-8'),
				'PAYMENTREQUEST_0_SHIPTOZIP'         => html_entity_decode($order_info['shipping_postcode'], ENT_QUOTES, 'UTF-8'),
				'PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE' => $order_info['shipping_iso_code_2']
			);
		} else {
			$shipping = 1;
			$data_shipping = array();
		}

		$data = array(
			'METHOD'             => 'SetExpressCheckout',
			'MAXAMT'             => $max_amount,
			'RETURNURL'          => $this->url->link('extension/payment/pp_express_monthly/checkoutReturn', '', true),
			'CANCELURL'          => $this->url->link('checkout/checkout', '', true),
			'REQCONFIRMSHIPPING' => 0,
			'NOSHIPPING'         => $shipping,
			'LOCALECODE'         => 'EN',
			'LANDINGPAGE'        => 'Login',
			'HDRIMG'             => $this->model_tool_image->resize($this->config->get('pp_express_logo'), 750, 90),
			'PAYFLOWCOLOR'       => $this->config->get('pp_express_monthly_colour'),
			'CHANNELTYPE'        => 'Merchant',
			'ALLOWNOTE'          => $this->config->get('pp_express_monthly_allow_note'),
			'L_BILLINGTYPE0'     => 'MerchantInitiatedBilling'
		);

		$data = array_merge($data, $data_shipping);

		if (isset($this->session->data['pp_login']['seamless']['access_token']) && (isset($this->session->data['pp_login']['seamless']['customer_id']) && $this->session->data['pp_login']['seamless']['customer_id'] == $this->customer->getId()) && $this->config->get('pp_login_seamless')) {
			$data['IDENTITYACCESSTOKEN'] = $this->session->data['pp_login']['seamless']['access_token'];
		}

		$data = array_merge($data, $this->model_extension_payment_pp_express_monthly->paymentRequestInfo());
		//echo 'string'; print_r($data); die;
		$result = $this->model_extension_payment_pp_express_monthly->call($data);
		
		/**
		 * If a failed PayPal setup happens, handle it.
		 */
		if (!isset($result['TOKEN'])) {
			 
			$this->session->data['error'] = $result['L_LONGMESSAGE0'];
			/**
			 * Unable to add error message to user as the session errors/success are not
			 * used on the cart or checkout pages - need to be added?
			 * If PayPal debug log is off then still log error to normal error log.
			 */
			$this->log->write('Unable to create Paypal session' . json_encode($result));

			$this->response->redirect($this->url->link('checkout/checkout', '', true));
		}

		 
		$this->session->data['paypal']['token'] = $result['TOKEN'];

		if ($this->config->get('pp_express_monthly_test') == 1) {
			header('Location: https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=' . $result['TOKEN'] . '&useraction=commit');
		} else {
			header('Location: https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=' . $result['TOKEN'] . '&useraction=commit');
		}
	}

	public function checkoutReturn() {
		$this->load->language('extension/payment/pp_express_monthly');

		$this->load->model('extension/payment/pp_express_monthly');
		$this->load->model('checkout/order');

		$data = array(
			'METHOD' => 'GetExpressCheckoutDetails',
			'TOKEN'  => $this->session->data['paypal']['token']
		);

		$result = $this->model_extension_payment_pp_express_monthly->call($data);
	
		$this->session->data['paypal']['payerid'] = $result['PAYERID'];
		$this->session->data['paypal']['result'] = $result;

		$order_id = $this->session->data['order_id'];

		$paypal_data = array(
			'TOKEN'                      => $this->session->data['paypal']['token'],
			'PAYERID'                    => $this->session->data['paypal']['payerid'],
			'METHOD'                     => 'DoExpressCheckoutPayment',
			'PAYMENTREQUEST_0_NOTIFYURL' => $this->url->link('extension/payment/pp_express_monthly/ipn', '', true),
			'RETURNFMFDETAILS'           => 1
		);

		$paypal_data = array_merge($paypal_data, $this->model_extension_payment_pp_express_monthly->paymentRequestInfo());

		$result = $this->model_extension_payment_pp_express_monthly->call($paypal_data);

		if ($result['ACK'] == 'Success') {
			//handle order status
			switch($result['PAYMENTINFO_0_PAYMENTSTATUS']) {
				case 'Canceled_Reversal':
					$order_status_id = $this->config->get('pp_express_monthly_canceled_reversal_status_id');
					break;
				case 'Completed':
					$order_status_id = $this->config->get('pp_express_monthly_completed_status_id');
					break;
				case 'Denied':
					$order_status_id = $this->config->get('pp_express_monthly_denied_status_id');
					break;
				case 'Expired':
					$order_status_id = $this->config->get('pp_express_monthly_expired_status_id');
					break;
				case 'Failed':
					$order_status_id = $this->config->get('pp_express_monthly_failed_status_id');
					break;
				case 'Pending':
					$order_status_id = $this->config->get('pp_express_monthly_pending_status_id');
					break;
				case 'Processed':
					$order_status_id = $this->config->get('pp_express_monthly_processed_status_id');
					break;
				case 'Refunded':
					$order_status_id = $this->config->get('pp_express_monthly_refunded_status_id');
					break;
				case 'Reversed':
					$order_status_id = $this->config->get('pp_express_monthly_reversed_status_id');
					break;
				case 'Voided':
					$order_status_id = $this->config->get('pp_express_monthly_voided_status_id');
					break;
			}

			$this->model_checkout_order->addOrderHistory($order_id, $order_status_id);
			
			//add order to paypal table
			$paypal_order_data = array(
				'order_id'         => $order_id,
				'customer_id'      => $this->customer->getId(),
				'capture_status'   => ($this->config->get('pp_express_monthly_transaction') == 'Sale' ? 'Complete' : 'NotComplete'),
				'currency_code'    => $result['PAYMENTINFO_0_CURRENCYCODE'],
				'authorization_id' => $result['PAYMENTINFO_0_TRANSACTIONID'],
				'total'            => $result['PAYMENTINFO_0_AMT'],
				'billing_agreement_id' => $result['BILLINGAGREEMENTID']
			);

			$paypal_order_id = $this->model_extension_payment_pp_express_monthly->addOrder($paypal_order_data);

			//add transaction to paypal transaction table
			$paypal_transaction_data = array(
				'paypal_order_id'       => $paypal_order_id,
				'transaction_id'        => $result['PAYMENTINFO_0_TRANSACTIONID'],
				'parent_id' => '',
				'note'                  => '',
				'msgsubid'              => '',
				'receipt_id'            => (isset($result['PAYMENTINFO_0_RECEIPTID']) ? $result['PAYMENTINFO_0_RECEIPTID'] : ''),
				'payment_type'          => $result['PAYMENTINFO_0_PAYMENTTYPE'],
				'payment_status'        => $result['PAYMENTINFO_0_PAYMENTSTATUS'],
				'pending_reason'        => $result['PAYMENTINFO_0_PENDINGREASON'],
				'transaction_entity'    => ($this->config->get('pp_express_monthly_transaction') == 'Sale' ? 'payment' : 'auth'),
				'amount'                => $result['PAYMENTINFO_0_AMT'],
				'debug_data'            => json_encode($result)
			);
			$this->model_extension_payment_pp_express_monthly->addTransaction($paypal_transaction_data);

			$recurring_products = $this->cart->getRecurringProducts();

			//loop through any products that are recurring items
			if ($recurring_products) {
				$this->load->model('checkout/recurring');

				$billing_period = array(
					'day'        => 'Day',
					'week'       => 'Week',
					'semi_month' => 'SemiMonth',
					'month'      => 'Month',
					'year'       => 'Year'
				);

				foreach ($recurring_products as $item) {
					$data = array(
						'METHOD'             => 'CreateRecurringPaymentsProfile',
						'TOKEN'              => $this->session->data['paypal']['token'],
						'PROFILESTARTDATE'   => gmdate("Y-m-d\TH:i:s\Z", gmmktime(gmdate('H'), gmdate('i') + 5, gmdate('s'), gmdate('m'), gmdate('d'), gmdate('y'))),
						'BILLINGPERIOD'      => $billing_period[$item['recurring']['frequency']],
						'BILLINGFREQUENCY'   => $item['recurring']['cycle'],
						'TOTALBILLINGCYCLES' => $item['recurring']['duration'],
						'AMT'                => $this->currency->format($this->tax->calculate($item['recurring']['price'], $item['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], false, false) * $item['quantity'],
						'CURRENCYCODE'       => $this->session->data['currency']
					);

					//trial information
					if ($item['recurring']['trial'] == 1) {
						$data_trial = array(
							'TRIALBILLINGPERIOD'      => $billing_period[$item['recurring']['trial_frequency']],
							'TRIALBILLINGFREQUENCY'   => $item['recurring']['trial_cycle'],
							'TRIALTOTALBILLINGCYCLES' => $item['recurring']['trial_duration'],
							'TRIALAMT'                => $this->currency->format($this->tax->calculate($item['recurring']['trial_price'], $item['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], false, false) * $item['quantity']
						);

						$trial_amt = $this->currency->format($this->tax->calculate($item['recurring']['trial_price'], $item['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], false, false) * $item['quantity'] . ' ' . $this->session->data['currency'];
						$trial_text =  sprintf($this->language->get('text_trial'), $trial_amt, $item['recurring']['trial_cycle'], $item['recurring']['trial_frequency'], $item['recurring']['trial_duration']);

						$data = array_merge($data, $data_trial);
					} else {
						$trial_text = '';
					}

					$recurring_amt = $this->currency->format($this->tax->calculate($item['recurring']['price'], $item['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], false, false)  * $item['quantity'] . ' ' . $this->session->data['currency'];
					$recurring_description = $trial_text . sprintf($this->language->get('text_recurring'), $recurring_amt, $item['recurring']['cycle'], $item['recurring']['frequency']);

					if ($item['recurring']['duration'] > 0) {
						$recurring_description .= sprintf($this->language->get('text_length'), $item['recurring']['duration']);
					}

					//create new recurring and set to pending status as no payment has been made yet.
					$recurring_id = $this->model_checkout_recurring->create($item, $order_id, $recurring_description);

					$data['PROFILEREFERENCE'] = $recurring_id;
					$data['DESC'] = $recurring_description;

					$result = $this->model_extension_payment_pp_express_monthly->call($data);

					if (isset($result['PROFILEID'])) {
						$this->model_checkout_recurring->addReference($recurring_id, $result['PROFILEID']);
					} else {
						// there was an error creating the recurring, need to log and also alert admin / user

					}
				}
			}

			if (isset($result['REDIRECTREQUIRED']) && $result['REDIRECTREQUIRED'] == true) {
				//- handle german redirect here
				$this->response->redirect('https://www.paypal.com/cgi-bin/webscr?cmd=_complete-express-checkout&token=' . $this->session->data['paypal']['token']);
			} else {
				$this->response->redirect($this->url->link('checkout/success'));
			}
		} else {
			if ($result['L_ERRORCODE0'] == '10486') {
				if (isset($this->session->data['paypal_redirect_count'])) {

					if ($this->session->data['paypal_redirect_count'] == 2) {
						$this->session->data['paypal_redirect_count'] = 0;
						$this->session->data['error'] = $this->language->get('error_too_many_failures');

						$this->response->redirect($this->url->link('checkout/checkout', '', true));
					} else {
						$this->session->data['paypal_redirect_count']++;
					}
				} else {
					$this->session->data['paypal_redirect_count'] = 1;
				}

				if ($this->config->get('pp_express_monthly_test') == 1) {
					$this->response->redirect('https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=' . $this->session->data['paypal']['token']);
				} else {
					$this->response->redirect('https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=' . $this->session->data['paypal']['token']);
				}
			}

			$this->load->language('extension/payment/pp_express_monthly');

			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'href' => $this->url->link('common/home'),
				'text' => $this->language->get('text_home')
			);

			$data['breadcrumbs'][] = array(
				'href' => $this->url->link('checkout/cart'),
				'text' => $this->language->get('text_cart')
			);

			$data['heading_title'] = $this->language->get('error_heading_title');

			$data['text_error'] = '<div class="warning">' . $result['L_ERRORCODE0'] . ' : ' . $result['L_LONGMESSAGE0'] . '</div>';

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('checkout/cart');

			unset($this->session->data['success']);

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
	}

	public function ipn() {
		$this->load->model('extension/payment/pp_express_monthly');
		$this->load->model('account/recurring');

		$request = 'cmd=_notify-validate';

		foreach ($_POST as $key => $value) {
			$request .= '&' . $key . '=' . urlencode(html_entity_decode($value, ENT_QUOTES, 'UTF-8'));
		}

		if ($this->config->get('pp_express_monthly_test') == 1) {
			$curl = curl_init('https://www.sandbox.paypal.com/cgi-bin/webscr');
		} else {
			$curl = curl_init('https://www.paypal.com/cgi-bin/webscr');
		}

		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_TIMEOUT, 30);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

		$response = trim(curl_exec($curl));

		if (!$response) {
			$this->model_extension_payment_pp_express_monthly->log(array('error' => curl_error($curl),'error_no' => curl_errno($curl)), 'Curl failed');
		}

		$this->model_extension_payment_pp_express_monthly->log(array('request' => $request,'response' => $response), 'IPN data');

		if ((string)$response == "VERIFIED")  {
			if (isset($this->request->post['transaction_entity'])) {
				$this->log->write($this->request->post['transaction_entity']);
			}

			if (isset($this->request->post['txn_id'])) {
				$transaction = $this->model_extension_payment_pp_express_monthly->getTransactionRow($this->request->post['txn_id']);
			} else {
				$transaction = false;
			}

			if (isset($this->request->post['parent_txn_id'])) {
				$parent_transaction = $this->model_extension_payment_pp_express_monthly->getTransactionRow($this->request->post['parent_txn_id']);
			} else {
				$parent_transaction = false;
			}

			if ($transaction) {
				//transaction exists, check for cleared payment or updates etc
				$this->model_extension_payment_pp_express_monthly->log('Transaction exists', 'IPN data');

				//if the transaction is pending but the new status is completed
				if ($transaction['payment_status'] != $this->request->post['payment_status']) {
					$this->db->query("UPDATE `" . DB_PREFIX . "paypal_order_monthly_transaction` SET `payment_status` = '" . $this->db->escape($this->request->post['payment_status']) . "' WHERE `transaction_id` = '" . $this->db->escape($transaction['transaction_id']) . "' LIMIT 1");
				} elseif ($transaction['payment_status'] == 'Pending' && ($transaction['pending_reason'] != $this->request->post['pending_reason'])) {
					//payment is still pending but the pending reason has changed, update it.
					$this->db->query("UPDATE `" . DB_PREFIX . "paypal_order_monthly_transaction` SET `pending_reason` = '" . $this->db->escape($this->request->post['pending_reason']) . "' WHERE `transaction_id` = '" . $this->db->escape($transaction['transaction_id']) . "' LIMIT 1");
				}
			} else {
				$this->model_extension_payment_pp_express_monthly->log('Transaction does not exist', 'IPN data');

				if ($parent_transaction) {
					//parent transaction exists
					$this->model_extension_payment_pp_express_monthly->log('Parent transaction exists', 'IPN data');

					//add new related transaction
					$transaction = array(
						'paypal_order_id'       => $parent_transaction['paypal_order_id'],
						'transaction_id'        => $this->request->post['txn_id'],
						'parent_id' => $this->request->post['parent_txn_id'],
						'note'                  => '',
						'msgsubid'              => '',
						'receipt_id'            => (isset($this->request->post['receipt_id']) ? $this->request->post['receipt_id'] : ''),
						'payment_type'          => (isset($this->request->post['payment_type']) ? $this->request->post['payment_type'] : ''),
						'payment_status'        => (isset($this->request->post['payment_status']) ? $this->request->post['payment_status'] : ''),
						'pending_reason'        => (isset($this->request->post['pending_reason']) ? $this->request->post['pending_reason'] : ''),
						'amount'                => $this->request->post['mc_gross'],
						'debug_data'            => json_encode($this->request->post),
						'transaction_entity'    => (isset($this->request->post['transaction_entity']) ? $this->request->post['transaction_entity'] : '')
					);

					$this->model_extension_payment_pp_express_monthly->addTransaction($transaction);

					/**
					 * If there has been a refund, log this against the parent transaction.
					 */
					if (isset($this->request->post['payment_status']) && $this->request->post['payment_status'] == 'Refunded') {
						if (($this->request->post['mc_gross'] * -1) == $parent_transaction['amount']) {
							$this->db->query("UPDATE `" . DB_PREFIX . "paypal_order_monthly_transaction` SET `payment_status` = 'Refunded' WHERE `transaction_id` = '" . $this->db->escape($parent_transaction['transaction_id']) . "' LIMIT 1");
						} else {
							$this->db->query("UPDATE `" . DB_PREFIX . "paypal_order_monthly_transaction` SET `payment_status` = 'Partially-Refunded' WHERE `transaction_id` = '" . $this->db->escape($parent_transaction['transaction_id']) . "' LIMIT 1");
						}
					}

					/**
					 * If the capture payment is now complete
					 */
					if (isset($this->request->post['auth_status']) && $this->request->post['auth_status'] == 'Completed' && $parent_transaction['payment_status'] == 'Pending') {
						$captured = $this->currency->format($this->model_extension_payment_pp_express_monthly->getTotalCaptured($parent_transaction['paypal_order_id']), $this->session->data['currency'], false, false);
						$refunded = $this->currency->format($this->model_extension_payment_pp_express_monthly->getRefundedTotal($parent_transaction['paypal_order_id']), $this->session->data['currency'], false, false);
						$remaining = $this->currency->format($parent_transaction['amount'] - $captured + $refunded, $this->session->data['currency'], false, false);

						$this->model_extension_payment_pp_express_monthly->log('Captured: ' . $captured, 'IPN data');
						$this->model_extension_payment_pp_express_monthly->log('Refunded: ' . $refunded, 'IPN data');
						$this->model_extension_payment_pp_express_monthly->log('Remaining: ' . $remaining, 'IPN data');

						if ($remaining > 0.00) {
							$transaction = array(
								'paypal_order_id'       => $parent_transaction['paypal_order_id'],
								'transaction_id'        => '',
								'parent_id'             => $this->request->post['parent_txn_id'],
								'note'                  => '',
								'msgsubid'              => '',
								'receipt_id'            => '',
								'payment_type'          => '',
								'payment_status'        => 'Void',
								'pending_reason'        => '',
								'amount'                => '',
								'debug_data'            => 'Voided after capture',
								'transaction_entity'    => 'auth'
							);

							$this->model_extension_payment_pp_express_monthly->addTransaction($transaction);
						}

						$this->model_extension_payment_pp_express_monthly->updateOrder('Complete', $parent_transaction['order_id']);
					}

				} else {
					//parent transaction doesn't exists, need to investigate?
					$this->model_extension_payment_pp_express_monthly->log('Parent transaction not found', 'IPN data');
				}
			}

			/*
			 * Subscription payments
			 *
			 * recurring ID should always exist if its a recurring payment transaction.
			 *
			 * also the reference will match a recurring payment ID
			 */
			if (isset($this->request->post['txn_type'])) {
				$this->model_extension_payment_pp_express_monthly->log($this->request->post['txn_type'], 'IPN data');

				//payment
				if ($this->request->post['txn_type'] == 'recurring_payment') {
					$recurring = $this->model_account_recurring->getOrderRecurringByReference($this->request->post['recurring_payment_id']);

					$this->model_extension_payment_pp_express_monthly->log($recurring, 'IPN data');

					if ($recurring != false) {
						$this->db->query("INSERT INTO `" . DB_PREFIX . "order_recurring_transaction` SET `order_recurring_id` = '" . (int)$recurring['order_recurring_id'] . "', `date_added` = NOW(), `amount` = '" . (float)$this->request->post['amount'] . "', `type` = '1'");

						//as there was a payment the recurring is active, ensure it is set to active (may be been suspended before)
						if ($recurring['status'] != 1) {
							$this->db->query("UPDATE `" . DB_PREFIX . "order_recurring` SET `status` = 2 WHERE `order_recurring_id` = '" . (int)$recurring['order_recurring_id'] . "'");
						}
					}
				}

				//suspend
				if ($this->request->post['txn_type'] == 'recurring_payment_suspended') {
					$recurring = $this->model_account_recurring->getOrderRecurringByReference($this->request->post['recurring_payment_id']);

					if ($recurring != false) {
						$this->db->query("INSERT INTO `" . DB_PREFIX . "order_recurring_transaction` SET `order_recurring_id` = '" . (int)$recurring['order_recurring_id'] . "', `date_added` = NOW(), `type` = '6'");
						$this->db->query("UPDATE `" . DB_PREFIX . "order_recurring` SET `status` = 3 WHERE `order_recurring_id` = '" . (int)$recurring['order_recurring_id'] . "' LIMIT 1");
					}
				}

				//suspend due to max failed
				if ($this->request->post['txn_type'] == 'recurring_payment_suspended_due_to_max_failed_payment') {
					$recurring = $this->model_account_recurring->getOrderRecurringByReference($this->request->post['recurring_payment_id']);

					if ($recurring != false) {
						$this->db->query("INSERT INTO `" . DB_PREFIX . "order_recurring_transaction` SET `order_recurring_id` = '" . (int)$recurring['order_recurring_id'] . "', `date_added` = NOW(), `type` = '7'");
						$this->db->query("UPDATE `" . DB_PREFIX . "order_recurring` SET `status` = 3 WHERE `order_recurring_id` = '" . (int)$recurring['order_recurring_id'] . "' LIMIT 1");
					}
				}

				//payment failed
				if ($this->request->post['txn_type'] == 'recurring_payment_failed') {
					$recurring = $this->model_account_recurring->getOrderRecurringByReference($this->request->post['recurring_payment_id']);

					if ($recurring != false) {
						$this->db->query("INSERT INTO `" . DB_PREFIX . "order_recurring_transaction` SET `order_recurring_id` = '" . (int)$recurring['order_recurring_id'] . "', `date_added` = NOW(), `type` = '4'");
					}
				}

				//outstanding payment failed
				if ($this->request->post['txn_type'] == 'recurring_payment_outstanding_payment_failed') {
					$recurring = $this->model_account_recurring->getOrderRecurringByReference($this->request->post['recurring_payment_id']);

					if ($recurring != false) {
						$this->db->query("INSERT INTO `" . DB_PREFIX . "order_recurring_transaction` SET `order_recurring_id` = '" . (int)$recurring['order_recurring_id'] . "', `date_added` = NOW(), `type` = '8'");
					}
				}

				//outstanding payment
				if ($this->request->post['txn_type'] == 'recurring_payment_outstanding_payment') {
					$recurring = $this->model_account_recurring->getOrderRecurringByReference($this->request->post['recurring_payment_id']);

					if ($recurring != false) {
						$this->db->query("INSERT INTO `" . DB_PREFIX . "order_recurring_transaction` SET `order_recurring_id` = '" . (int)$recurring['order_recurring_id'] . "', `date_added` = NOW(), `amount` = '" . (float)$this->request->post['amount'] . "', `type` = '2'");

						//as there was a payment the recurring is active, ensure it is set to active (may be been suspended before)
						if ($recurring['status'] != 1) {
							$this->db->query("UPDATE `" . DB_PREFIX . "order_recurring` SET `status` = 2 WHERE `order_recurring_id` = '" . (int)$recurring['order_recurring_id'] . "'");
						}
					}
				}

				//date_added
				if ($this->request->post['txn_type'] == 'recurring_payment_recurring_date_added') {
					$recurring = $this->model_account_recurring->getOrderRecurringByReference($this->request->post['recurring_payment_id']);

					if ($recurring != false) {
						$this->db->query("INSERT INTO `" . DB_PREFIX . "order_recurring_transaction` SET `order_recurring_id` = '" . (int)$recurring['order_recurring_id'] . "', `date_added` = NOW(), `type` = '0'");

						if ($recurring['status'] != 1) {
							$this->db->query("UPDATE `" . DB_PREFIX . "order_recurring` SET `status` = 2 WHERE `order_recurring_id` = '" . (int)$recurring['order_recurring_id'] . "'");
						}
					}
				}

				//cancelled
				if ($this->request->post['txn_type'] == 'recurring_payment_recurring_cancel') {
					$recurring = $this->model_account_recurring->getOrderRecurringByReference($this->request->post['recurring_payment_id']);

					if ($recurring != false && $recurring['status'] != 3) {
						$this->db->query("INSERT INTO `" . DB_PREFIX . "order_recurring_transaction` SET `order_recurring_id` = '" . (int)$recurring['order_recurring_id'] . "', `date_added` = NOW(), `type` = '5'");
						$this->db->query("UPDATE `" . DB_PREFIX . "order_recurring` SET `status` = 4 WHERE `order_recurring_id` = '" . (int)$recurring['order_recurring_id'] . "' LIMIT 1");
					}
				}

				//skipped
				if ($this->request->post['txn_type'] == 'recurring_payment_skipped') {
					$recurring = $this->model_account_recurring->getOrderRecurringByReference($this->request->post['recurring_payment_id']);

					if ($recurring != false) {
						$this->db->query("INSERT INTO `" . DB_PREFIX . "order_recurring_transaction` SET `order_recurring_id` = '" . (int)$recurring['order_recurring_id'] . "', `date_added` = NOW(), `type` = '3'");
					}
				}

				//expired
				if ($this->request->post['txn_type'] == 'recurring_payment_expired') {
					$recurring = $this->model_account_recurring->getOrderRecurringByReference($this->request->post['recurring_payment_id']);

					if ($recurring != false) {
						$this->db->query("INSERT INTO `" . DB_PREFIX . "order_recurring_transaction` SET `order_recurring_id` = '" . (int)$recurring['order_recurring_id'] . "', `date_added` = NOW(), `type` = '9'");
						$this->db->query("UPDATE `" . DB_PREFIX . "order_recurring` SET `status` = 5 WHERE `order_recurring_id` = '" . (int)$recurring['order_recurring_id'] . "' LIMIT 1");
					}
				}
			}
		} elseif ((string)$response == "INVALID") {
			$this->model_extension_payment_pp_express_monthly->log(array('IPN was invalid'), 'IPN fail');
		} else {
			$this->model_extension_payment_pp_express_monthly->log('Response string unknown: ' . (string)$response, 'IPN data');
		}

		header("HTTP/1.1 200 Ok");
	}

}