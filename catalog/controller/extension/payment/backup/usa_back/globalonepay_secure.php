<?php
include_once(DIR_APPLICATION.'/controller/gateway_tps_xml.php');
class ControllerExtensionPaymentGlobalonepaySecure extends Controller {
	private $error = array();

	public function index(){
		$this->load->language('extension/payment/globalonepay_secure');
		$data['button_confirm'] = $this->language->get('button_confirm');
		$data['text_testmode'] = $this->language->get('text_testmode');
		$data['action'] = $this->url->link('extension/payment/globalonepay_secure/onetimePayment', '', 'SSl');
		if( isset($this->session->data['order_type']) && $this->session->data['order_type'] == 'monthly'){
			$data['action'] = $this->url->link('extension/payment/globalonepay_secure/monthlyPayment', '', 'SSL');
		}
		$data['continue'] = $this->url->link('checkout/success', '', 'SSL');
		$data['text_loading'] = $this->language->get('text_loading');
		$data['ext_link'] = $this->language->get('ext_link');
		
		$card_types =  array(
			'VISA' => 'Visa',
			'VISA DEBIT' => 'Visa Debit',
			'MASTERCARD' => 'MasterCard',
		);

		$startdate = 2018;
      	$enddate = date(2035);
      	$years = range ( $startdate, $enddate );
		$data['years'] = $years;
		$data['card_types'] = $card_types;

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/extension/payment/globalonepayhpp.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/extension/payment/globalonepay_secure.tpl', $data);
		} else {
			return $this->load->view('extension/payment/globalonepay_secure.tpl', $data);
		}
	}

	public function monthlyPayment(){
		$json = array();
		$terminalId = '';
		$secret = '';
		$details = true; 
		$this->load->language('extension/payment/globalonepay_secure');
		if ( $this->request->post && $this->session->data['payment_method']['code'] == 'globalonepay_secure' ){

			$card_details = $this->request->post;
			$this->load->model('checkout/order');
			$this->load->model('extension/payment/globalonepay_secure');
			// get current order info //
			$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
		
			$gateway = 'globalone';
			
			switch($order_info['currency_code']) {
				case $this->config->get('globalonepay_secure_currency1') :
					$currency = $this->config->get('globalonepay_secure_currency1');
					$terminalId = $this->config->get('globalonepay_secure_terminal1');
					$secret = $this->config->get('globalonepay_secure_secret1');
					break;
				case $this->config->get('globalonepay_secure_currency2') :
					$currency = $this->config->get('globalonepay_secure_currency2');
					$terminalId = $this->config->get('globalonepay_secure_terminal2');
					$secret = $this->config->get('globalonepay_secure_secret2');
					break;
				case $this->config->get('globalonepay_secure_currency3') :
					$currency = $this->config->get('globalonepay_secure_currency3');
					$terminalId = $this->config->get('globalonepay_secure_terminal3');
					$secret = $this->config->get('globalonepay_secure_secret3');
			}

			if($card_details['cardnumber'] == '' || $card_details['cardtype'] == '' || $card_details['expiry_month'] == '' || $card_details['expiry_year'] == '' || $card_details['cardholdername'] == '' || $card_details['cvv'] == ''){

				$json['error']['warning'] = $this->language->get('invalid_credentials');
				$details = false;
			}

			if( $card_details['expiry_month'] == 'Month' ){
				$json['error']['warning'] = $this->language->get('expiry_month_error');
				$details = false;
			}

			if($card_details['expiry_month'] <= date('m') && $card_details['expiry_year'] == date('y')){
				$json['error']['warning'] = $this->language->get('expiry_date_error');
				$details = false;
			}

			if($details && (!$terminalId || !$secret)){
				$json['error']['warning'] = $this->language->get('unable_to_process');
			}

			$testAccount = false;
			if($this->config->get('globalonepay_secure_test')){
				$testAccount = true;
			}
			# These are used only in the case where the response hash is incorrect, which should
			# never happen in the live environment unless someone is attempting fraud.
			$adminEmail = '';
			$adminPhone = '';
			$secureCardMerchantRef = date('Y').date('M').date('D').$card_details['cardtype'].$order_info['order_id'].$order_info['customer_id'];	
			
			# Unique Merchant Reference. Length is limited to 48 chars.
			$cardNumber = $card_details['cardnumber'];		# This is the full PAN (card number) of the credit card. It must be digits only (i.e. no spaces or other characters).
			$cardType = $card_details['cardtype'];			# See our Integrator Guide for a list of valid Card Type parameters
			$cardExpiry = $card_details['expiry_month'].$card_details['expiry_year'];		# The 4 digit expiry date (MMYY)
			$cardHolderName = $card_details['cardholdername'];		# The full cardholders name, as it is displayed on the credit card
			$cvv = $card_details['cvv']; # (optional) 3 digit (4 for AMEX cards) security digit on the back of the card.
			$dontCheckSecurity = ''; 	# (optional) "Y" if you do not want the CVV to be validated online.
			
			$issueNo = '';# (optional) Issue number for Switch and Solo cards.
			
			# Set up the SecureCard addition object
			if(empty($json)){
				$securereg = new XmlSecureCardRegRequest($secureCardMerchantRef, $terminalId, $cardNumber, $cardExpiry, $cardType, $cardHolderName);
				 
				if($dontCheckSecurity != "") $securereg->SetDontCheckSecurity($dontCheckSecurity);
				if($cvv != "") $securereg->SetCvv($cvv);
				if($issueNo != "") $securereg->SetIssueNo($issueNo);
				 
				$response = $securereg->ProcessRequestToGateway($secret,$testAccount,$gateway);
			
				if($response->IsError()){
					$json['error']['warning'] = $this->language->get('response_error'). $response->ErrorString();
			
				} else {

					$orderId = 'USD_'. $order_info['order_id'];
					$merchantRef = $response->MerchantReference();
					$expectedResponseHash = md5($terminalId.$response->MerchantReference().$response->CardReference().$response->DateTime().$secret);

					if($expectedResponseHash != $response->Hash()) {

						$this->model_checkout_order->addOrderHistory( $orderId, $this->config->get('globalonepay_secure_status_failed'), $response->ResponseText(), FALSE );

						$this->session->data['error'] = $this->language->get('securecard_reg_error');
						$json['redirect'] = $this->url->link('checkout/checkout', '', 'SSL');	
					
					} else {
				       // echo "SecureCard successfully stored.";

						if(	$response->CardReference() ){
							
							$this->model_extension_payment_globalonepay_secure->saveCustomerCardRef($order_info['customer_id'], $order_info['order_id'], $secureCardMerchantRef, $response->CardReference(), $cardType, $cardExpiry);

							$cardNumber = $response->CardReference();
				            $cardType = 'SECURECARD';
				           
				            $cvv = '';
				            $email = '';
				            $mobileNumber = '';
				            $issueNo = '';
				            $address1 = '';         # (optional) This is the first line of the cardholders address.
				            $address2 = '';         # (optional) This is the second line of the cardholders address.
				            $postcode = '';         # (optional) This is the cardholders post code.
				            $country = '';          # (optional) This is the cardholders country name.
				            $phone = '';            # (optional) This is the cardholders home phone number.
				            $cardCurrency = '';     # (optional) This is the three character ISO currency code returned in the rate request.
				            $cardAmount = '';       # (optional) This is the foreign currency transaction amount returned in the rate request.
				            $conversionRate = '';       # (optional) This is the currency conversion rate returned in the rate request
				            $mpiref = '';           # This should be blank unless instructed otherwise by GlobalOne.
				            $deviceId = '';         # This should be blank unless instructed otherwise by GlobalOne.
				            $autoready = '';        # (optional) (Y/N) Whether or not this transaction should be marked with a status of "ready" as apposed to "pending".
				            
							//CB - Dec 12, changed to true.
							$multicur = true;      # This should be false unless instructed otherwise by GlobalOne.
				            $description = '';      # (optional) This can is a description for the transaction that will be available in the merchant notification e-mail and in the SelfCare  system.
				            $autoReady = '';        # (optional) Y or N. Automatically set the transaction to a status of Ready in the batch. If not present the terminal default will be used.
				            $isMailOrder = false;   #If true the transaction will be processed as a Mail Order transaction. This is only for use with Mail Order enabled Terminal IDs.
				            # Set up the authorisation object
				            $cardHolderName = '';
				    		$cardExpiry = '';

							$amount = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], FALSE);

				            $auth = new XmlAuthRequest($terminalId, $orderId, $currency, $amount, $cardNumber, $cardType);

				            if ($cardType != "SECURECARD")
				                $auth->SetNonSecureCardCardInfo($cardExpiry, $cardHolderName);
				            if ($cvv != "")
				                $auth->SetCvv($cvv);
				            if ($cardCurrency != "" && $cardAmount != "" && $conversionRate != "")
				                $auth->SetForeignCurrencyInformation($cardCurrency, $cardAmount, $conversionRate);
				            if ($email != "")
				                $auth->SetEmail($email);
				            if ($mobileNumber != "")
				                $auth->SetMobileNumber($mobileNumber);
				            if ($description != "")
				                $auth->SetDescription($description);

				            if ($issueNo != "")
				                $auth->SetIssueNo($issueNo);
				            if ($address1 != "" && $address2 != "" && $postcode != "")
				                $auth->SetAvs($address1, $address2, $postcode);
				            if ($country != "")
				                $auth->SetCountry($country);
				            if ($phone != "")
				                $auth->SetPhone($phone);

				            if ($mpiref != "")
				                $auth->SetMpiRef($mpiref);
				            if ($deviceId != "")
				                $auth->SetDeviceId($deviceId);

				            if ($multicur)
				                $auth->SetMultiCur();
				            if ($autoready)
				                $auth->SetAutoReady($autoready);
				            if ($isMailOrder)
				                $auth->SetMotoTrans();

				            # Perform the online authorisation and read in the result
				            $response = $auth->ProcessRequestToGateway($secret, $testAccount, $gateway);
                            //echo "<pre>"; print_r($response); die;
				            $expectedResponseHash = md5($terminalId . $response->UniqueRef() . ($multicur == true ? $currency : '') . $amount . $response->DateTime() . $response->ResponseCode() . $response->ResponseText() . $secret);
                           // echo $expectedResponseHash;
 							// echo  "<br>";
        //                     echo $response->Hash(); die;
							//CB - Dec 12
				            if ($response->IsError()) {
				                $json['error']['warning'] = $this->language->get('response_error'). $response->ErrorString() . "  " . $currency;
				               
				            } else  {

				                switch ($response->ResponseCode()) {
				                    case "A" :  # -- If using local database, update order as Authorised.
				                        //echo 'Payment Processed successfully. Thanks you for your order.';
				                        $this->model_checkout_order->addOrderHistory($orderId, $this->config->get('globalonepay_secure_status_success'), $response->ResponseText(), FALSE);
				                       
				                        break;
				                    case "R" :
				                    case "D" :
				                    case "C" :
				                    case "S" :

				                    default :  # -- If using local database, update order as declined/failed --
				                      $this->model_checkout_order->addOrderHistory($orderId, $this->config->get('globalonepay_secure_status_declined'), $response->ResponseText(), FALSE);
				                       
				                        $json['error']['warning'] = $this->language->get('payment_decline_error');
				                }

				                $request_data = $auth->getRequestInfo();
				                $request_data['customer_id'] = $order_info['customer_id'];
				                $request_data['order_id'] = $orderId;
								$request_data['order_type'] = 'monthlyPayment';
						    	$this->saveGlobaloneXmldata($request_data, $response);
					    	}
                            //  else {               
                            //     $uniqueReference = $response->UniqueRef();              
                            //    // $this->session->data['error'] = 'PAYMENT FAILED: INVALID RESPONSE HASH. Please contact to clarify if you will get charged for this order.';             
                            //     $this->session->data['error'] = $this->language->get('invalid_hash_error');      
                            //     $this->model_checkout_order->addOrderHistory( $orderId, 
                            //     $this->config->get('globalonepay_secure_status_failed'), 
                            //     $response->ResponseText(), FALSE );             
                            //     $json['redirect'] = $this->url->link('checkout/checkout');                           
                            // }
				       	} 
					}
				}
			
				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($json));
			}
	 	}
	}
	
	public function onetimePayment(){
		$json = array();
		$terminalId = '';
		$secret = '';
		$details = true; 
		$currency = 'CAD';
		$this->load->language('extension/payment/globalonepay_secure');
		if ( $this->request->post ) {
			$card_details = $this->request->post;

			$this->load->model('checkout/order');
			$this->load->model('extension/payment/globalonepay_secure');
			// get current order info //
			$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
			$gateway = 'globalone';
			switch($order_info['currency_code']) {
				case $this->config->get('globalonepay_secure_currency1') :
					$currency = $this->config->get('globalonepayhpp_currency1');
					$terminalId = $this->config->get('globalonepay_secure_terminal1');
					$secret = $this->config->get('globalonepay_secure_secret1');
					break;
				case $this->config->get('globalonepay_secure_currency2') :
					$currency =$this->config->get('globalonepayhpp_currency2');
					$terminalId = $this->config->get('globalonepay_secure_terminal2');
					$secret = $this->config->get('globalonepay_secure_secret2');
					break;
				case $this->config->get('globalonepay_secure_currency3') :
					$currency =$this->config->get('globalonepayhpp_currency3');
					$terminalId = $this->config->get('globalonepay_secure_terminal3');
					$secret = $this->config->get('globalonepay_secure_secret3');
			}

			if($card_details['cardnumber'] == '' || $card_details['cardtype'] == '' || $card_details['expiry_month'] == '' || $card_details['expiry_year'] == '' || $card_details['cardholdername'] == '' || $card_details['cvv'] == ''){

				$json['error']['warning'] = $this->language->get('invalid_credentials');
				$details = false;
			}

			if( $card_details['expiry_month'] == 'Month' ){
				$json['error']['warning'] = $this->language->get('expiry_month_error');
				$details = false;
			}

			if($details == true && $card_details['expiry_month'] <= date('m') && $card_details['expiry_year'] == date('y')){
				$json['error']['warning'] = $this->language->get('expiry_date_error');
				$details = false;
			}
			
		
			if($details && (!$terminalId || !$secret)){
				$json['error']['warning'] = $this->language->get('unable_to_process');
			}

			$testAccount = false;
			if($this->config->get('globalonepay_secure_test')){
				$testAccount = true;
			}
			# These are used only in the case where the response hash is incorrect, which should
			# never happen in the live environment unless someone is attempting fraud.
			$adminEmail = '';
			$adminPhone = '';
		}
	    # These values are specific to the cardholder.
	    $cardNumber = $card_details['cardnumber'];		# This is the full PAN (card number) of the credit card OR the SecureCard Card Reference if using a SecureCard. It must be digits only (i.e. no spaces or other characters).
	    $cardType = $card_details['cardtype'];			# See our Integrator Guide for a list of valid Card Type parameters
	    $cardExpiry = $card_details['expiry_month'].$card_details['expiry_year'];;		# (if not using SecureCard) The 4 digit expiry date (MMYY).
	    $cardHolderName = $card_details['cardholdername'];		# (if not using SecureCard) The full cardholders name, as it is displayed on the credit card.
	    $cvv = $card_details['cvv'];			# (optional) 3 digit (4 for AMEX cards) security digit on the back of the card.
	    $issueNo = '';			# (optional) Issue number for Switch and Solo cards.
	    $email = ($this->config->get('globalonepayhpp_send_receipt') == '1' ? $order_info['email'] : '');			# (optional) If this is sent then GlobalOne will send a receipt to this e-mail address.
	    $mobileNumber = "";		# (optional) Cardholders mobile phone number for sending of a receipt. Digits only, Include international prefix.

	    # These values are specific to the transaction.
	    $orderId = $order_info['order_id'];			# This should be unique per transaction (12 character max).
	    $amount = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], FALSE);			# This should include the decimal point.
	    $isMailOrder = false;		# If true the transaction will be processed as a Mail Order transaction. This is only for use with Mail Order enabled Terminal IDs.

	    # These fields are for AVS (Address Verification Check). This is only appropriate in the UK and the US.
	    if (!$order_info['payment_address_2']) {
	    	$address1 = $order_info['payment_address_1'];	# (optional) This is the first line of the cardholders address.
	    	$address2 = $order_info['payment_city'] . ', ' . $order_info['payment_zone'];	# (optional) This is the second line of the cardholders address.
	    }else {
			$address1 = $order_info['payment_address_1'] . ', ' . $order_info['payment_address_2'];
			$address2 = $order_info['payment_city'] . ', ' . $order_info['payment_zone'];
		}

	    $postcode = $order_info['payment_postcode'];		# (optional) This is the cardholders post code.
	    $country = $order_info['payment_iso_code_2'];		# (optional) This is the cardholders country name.
	    $phone = $order_info['telephone'];			# (optional) This is the cardholders home phone number.

	    # eDCC fields. Populate these if you have retreived a rate for the transaction, offered it to the cardholder and they have accepted that rate.
	    $cardCurrency = '';		# (optional) This is the three character ISO currency code returned in the rate request.
	    $cardAmount = '';		# (optional) This is the foreign currency transaction amount returned in the rate request.
	    $conversionRate = '';		# (optional) This is the currency conversion rate returned in the rate request.

	    # 3D Secure reference. Only include if you have verified 3D Secure throuugh the GlobalOne MPI and received an MPIREF back.
	    $mpiref = '';			# This should be blank unless instructed otherwise by GlobalOne.
	    $deviceId = '';			# This should be blank unless instructed otherwise by GlobalOne.

	    $autoready = '';		# (optional) (Y/N) Whether or not this transaction should be marked with a status of "ready" as apposed to "pending".
	    //CB - Dec 12, change to true
		$multicur = true;		# This should be false unless instructed otherwise by GlobalOne.

	    $description = $this->config->get('config_name') . ' - #' . $orderId;
		$name = $order_info['payment_firstname'] . ' ' . $order_info['payment_lastname'];	# (optional) This can is a description for the transaction that will be available in the merchant notification e-mail and in the SelfCare  system.
	    $autoReady = '';		# (optional) Y or N. Automatically set the transaction to a status of Ready in the batch. If not present the terminal default will be used.

	    if(empty($json)){
		    # Set up the authorisation object
		    $auth = new XmlAuthRequest($terminalId,$orderId,$currency,$amount,$cardNumber,$cardType);
		    if($cardType != "SECURECARD") $auth->SetNonSecureCardCardInfo($cardExpiry,$cardHolderName);
		    if($cvv != "") $auth->SetCvv($cvv);
		    if($cardCurrency != "" && $cardAmount != "" && $conversionRate != "") $auth->SetForeignCurrencyInformation($cardCurrency,$cardAmount,$conversionRate);
		    if($email != "") $auth->SetEmail($email);
		    if($mobileNumber != "") $auth->SetMobileNumber($mobileNumber);
		    if($description != "") $auth->SetDescription($description);
		     
		    if($issueNo != "") $auth->SetIssueNo($issueNo);
		    if($address1 != "" && $address2 != "" && $postcode != "") $auth->SetAvs($address1,$address2,$postcode);
		    if($country != "") $auth->SetCountry($country);
		    if($phone != "") $auth->SetPhone($phone);
		     
		    if($mpiref != "") $auth->SetMpiRef($mpiref);
		    if($deviceId != "") $auth->SetDeviceId($deviceId);
		     
		    if($multicur) $auth->SetMultiCur();
		    if($autoready) $auth->SetAutoReady($autoready);
		    if($isMailOrder) $auth->SetMotoTrans();
		     
		    # Perform the online authorisation and read in the result
		    $response = $auth->ProcessRequestToGateway($secret,$testAccount, $gateway);
		     
		     
		    $expectedResponseHash = md5($terminalId . $response->UniqueRef() . ($multicur == true ? $currency : '') . $amount . $response->DateTime() . $response->ResponseCode() . $response->ResponseText() . $response->BankResponseCode() . $secret);
		     
		    if($response->IsError()) {
		    	$json['error']['warning'] = $this->language->get('response_error'). $response->ErrorString();
			
			} elseif($expectedResponseHash == $response->Hash()) {

		     	switch($response->ResponseCode()) {
		    		case "A" :	# -- If using local database, update order as Authorised.
		    			#echo 'Payment Processed successfully. Thanks you for your order.';
		    				$this->model_checkout_order->addOrderHistory( $orderId, $this->config->get('globalonepay_secure_status_success'), $response->ResponseText(), FALSE );
		    				
		    				break;
		    		case "R" :
		    		case "D" :
		    		case "C" :
		    		case "S" :
		     
		    		default  :	# -- If using local database, update order as declined/failed --
		    			$this->model_checkout_order->addOrderHistory( $orderId, $this->config->get('globalonepay_secure_status_declined'), $response->ResponseText(), FALSE );
		    			$this->session->data['error'] = $this->language->get('payment_decline_error');
		    			$json['redirect'] = $this->url->link('checkout/checkout','', 'SSL');
		    	}

		    	 $request_data = $auth->getRequestInfo();
	             $request_data['customer_id'] = $order_info['customer_id'];
	             $request_data['order_id'] = $order_info['order_id'];
				 $request_data['order_type'] = 'one time';
		    	 $this->saveGlobaloneXmldata($request_data, $response);
		    } else {
		    	 $uniqueReference = $response->UniqueRef();              
               // $this->session->data['error'] = 'PAYMENT FAILED: INVALID RESPONSE HASH. Please contact to clarify if you will get charged for this order.';             
                $this->session->data['error'] = $this->language->get('invalid_hash_error');      
                $this->model_checkout_order->addOrderHistory( $orderId, 
                $this->config->get('globalonepay_secure_status_failed'), 
                $response->ResponseText(), FALSE );             
                $json['redirect'] = $this->url->link('checkout/checkout');  
		    } 
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function saveGlobaloneXmldata($request_data, $response){

		$globalone_request_id = $this->model_checkout_order->saveGlobaloneXmlRequest($request_data);
	    if($globalone_request_id){
	    	$response_data =  array(
	    		'globalone_request_id' => $globalone_request_id,
	    		'isError' => $response->IsError(),
	    		'errorString' => $response->ErrorString(),
	    		'responseCode' => $response->ResponseCode(),
	    		'responseText' => $response->ResponseText(),
	    		'approvalCode' => $response->ApprovalCode(),
	    		'authorizedAmount' => $response->AuthorizedAmount(),
	    		'dateTime' => $response->DateTime(),
	    		'avsResponse' => $response->AvsResponse(),
	    		'cvvResponse' => $response->CvvResponse(),
	    		'uniqueRef' => $response->UniqueRef(),
	    		'hash' => $response->Hash(),
	    	);
	    	$this->model_checkout_order->saveGlobaloneXmlResponse($response_data);
	    }
	}
}
