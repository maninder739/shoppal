<?php
class ControllerExtensionPaymentGlobalOnePayHPP extends Controller {
	public function index() {

		$this->load->language('extension/payment/globalonepayhpp');
		$data['button_confirm'] = $this->language->get('button_confirm');
		$data['text_testmode'] = $this->language->get('text_testmode');

		$this->load->model('checkout/order');
		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
		$data['test'] = $this->config->get('globalonepayhpp_test');

		$data['action'] = 'https://'.($data['test'] == '1' ? 'test' : '');
		switch($this->config->get('globalonepayhpp_gateway')) {
			case "globalonepay" :
			default :
				$data['action'] .= 'payments.globalone.me';
		}
	
		$data['action'] .= '/merchant/paymentpage';
		
		switch($order_info['currency_code']) {
			case $this->config->get('globalonepayhpp_currency1') :
				$data['terminalid'] = $this->config->get('globalonepayhpp_terminal1');
				$secret = $this->config->get('globalonepayhpp_secret1');
				break;
			case $this->config->get('globalonepayhpp_currency2') :
				$data['terminalid'] = $this->config->get('globalonepayhpp_terminal2');
				$secret = $this->config->get('globalonepayhpp_secret2');
				break;
			case $this->config->get('globalonepayhpp_currency3') :
				$data['terminalid'] = $this->config->get('globalonepayhpp_terminal3');
				$secret = $this->config->get('globalonepayhpp_secret3');
		}
		
		$data['currency'] = $order_info['currency_code'];

		$data['order_id'] = 'SHOPPAL'.$order_info['order_id'];
		$data['amount'] = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], FALSE);
		$data['date_time'] = date('d-m-Y:H:i:s:000');
		$data['description'] = $this->config->get('config_name') . ' - #' . 'SHOPPAL'.$order_info['order_id'];
		$data['name'] = $order_info['payment_firstname'] . ' ' . $order_info['payment_lastname'];
		
		if (!$order_info['payment_address_2']) {
			$data['address1'] = $order_info['payment_address_1'];
			$data['address2'] = $order_info['payment_city'] . ', ' . $order_info['payment_zone'];
		} else {
			$data['address1'] = $order_info['payment_address_1'] . ', ' . $order_info['payment_address_2'];
			$data['address2'] = $order_info['payment_city'] . ', ' . $order_info['payment_zone'];
		}
	

		$data['postcode'] = $order_info['payment_postcode'];
		$data['country'] = $order_info['payment_iso_code_2'];
		$data['telephone'] = $order_info['telephone'];
		$data['email'] = ($this->config->get('globalonepayhpp_send_receipt') == '1' ? $order_info['email'] : '');
		
		$data['receipt_page_URL'] = $this->url->link('extension/payment/globalonepayhpp/callback', '', 'SSL');
		$data['validation_URL'] = $this->url->link('extension/payment/globalonepayhpp/validate', '', 'SSL');

		$data['hash'] = md5($data['terminalid'] . $data['order_id'] . $data['amount'] . $data['date_time'] . $data['receipt_page_URL'] . $data['validation_URL'] . $secret);
		
		if( $this->session->data['order_type'] == 'monthly'){
			$data['secure_card_merchant_ref'] = time().$order_info['customer_id'];
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/extension/payment/globalonepayhpp.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/extension/payment/globalonepayhpp.tpl', $data);
		} else {
			return $this->load->view('extension/payment/globalonepayhpp.tpl', $data);
		}
	}
	
	public function callback() {
	#	$this->load->model('payment/globalonepayhpp');

		if (isset($this->request->get['RESPONSECODE']) && isset($this->request->get['TERMINALID']) && isset($this->request->get['HASH'])) {
			switch($this->request->get['TERMINALID']) {
				case $this->config->get('globalonepayhpp_terminal3') :
					$data['currency'] =$this->config->get('globalonepayhpp_currency3');
					$secret = $this->config->get('globalonepayhpp_secret3');
					break;
				case $this->config->get('globalonepayhpp_terminal2') :
					$data['currency'] =$this->config->get('globalonepayhpp_currency2');
					$secret = $this->config->get('globalonepayhpp_secret2');
					break;
				case $this->config->get('globalonepayhpp_terminal1') :
					$data['currency'] =$this->config->get('globalonepayhpp_currency1');
					$secret = $this->config->get('globalonepayhpp_secret1');
			}
			$data['terminalid'] = $this->request->get['TERMINALID'];
			
			if ($this->request->get['HASH'] == md5($this->request->get['TERMINALID'] . $this->request->get['ORDERID'] . $this->request->get['AMOUNT'] . $this->request->get['DATETIME'] . $this->request->get['RESPONSECODE'] . $this->request->get['RESPONSETEXT'] . $secret)) {
				
				if ($this->request->get['RESPONSECODE'] == 'A') {
					// Transaction Approved
					
					$msg_str = '';
					if( isset($this->request->get['ISSTORED']) && $this->request->get['ISSTORED'] == true ){
						$this->load->model('checkout/order');
						$data =  array(
							'issorted' => $this->request->get['ISSTORED'],
							'merchant_ref' => $this->request->get['MERCHANTREF'],
							'cardrefernce' => $this->request->get['CARDREFERENCE'],
							'cardtype' => $this->request->get['CARDTYPE'],
							'cardexpiry' => $this->request->get['CARDEXPIRY']
						);

						$this->model_checkout_order->addCustomerInfo( $data );

						$msg_str = '_other_' . $this->request->get['ISSTORED'] . $this->request->get['MERCHANTREF'] .$this->request->get['CARDREFERENCE'] .$this->request->get['CARDTYPE'] .$this->request->get['CARDEXPIRY'];
						
					}

					$msg = 'No ERROR:  ' . $this->request->get['TERMINALID'] . $this->request->get['ORDERID'] . $this->request->get['AMOUNT'] . $this->request->get['DATETIME'] . $this->request->get['RESPONSECODE'] . $this->request->get['RESPONSETEXT'] . $secret.$msg_str ;

					mail("christine@.bieri@itgs.ca","Test Site Before Succesful redirect ",$msg);					
		  			$this->response->redirect($this->url->link('checkout/success', '', 'SSL'));
				} else {
					// Transaction Declined
					$this->session->data['error'] = 'Transaction Declined. Bank Response: '.$this->request->get['RESPONSETEXT'];
		  			$this->response->redirect($this->url->link('checkout/checkout', '', 'SSL'));
				}
			} else {
				// Response Hash validation failed
				$this->session->data['error'] = 'ERROR! Response Hash Validation Failed. Please contact us ASAP with Order ID '.$this->request->get['ORDERID'].' to ensure that you will not be charged.  ' . $this->request->get['TERMINALID'] . $this->request->get['ORDERID'] . $this->request->get['AMOUNT'] . $this->request->get['DATETIME'] . $this->request->get['RESPONSECODE'] . $this->request->get['RESPONSETEXT'] . $secret . '_other_' . $this->request->get['ISSTORED']  .$this->request->get['MERCHANTREF'] .$this->request->get['CARDREFERENCE'] .$this->request->get['CARDTYPE'] .$this->request->get['CARDEXPIRY'];
		  		$this->response->redirect($this->url->link('checkout/checkout', '', 'SSL'));
			}
		}
	}

	public function validate() {
		$msg = $this->request->post['RESPONSECODE'] . "  -  " . $this->request->post['TERMINALID'] . "  -  " . $this->request->post['HASH'];
	
		mail("christine.bieri@itgs.ca","Test Site Validate Response ",$msg);
		if (isset($this->request->post['RESPONSECODE']) && isset($this->request->post['TERMINALID']) && isset($this->request->post['HASH'])) {
			
		mail("christine.bieri@itgs.ca","Test Site Validate Response IN IF",$msg);			
			switch($this->request->post['TERMINALID']) {
				case $this->config->get('globalonepayhpp_terminal3') :
					$data['currency'] =$this->config->get('globalonepayhpp_currency3');
					$secret = $this->config->get('globalonepayhpp_secret3');
					break;
				case $this->config->get('globalonepayhpp_terminal2') :
					$data['currency'] =$this->config->get('globalonepayhpp_currency2');
					$secret = $this->config->get('globalonepayhpp_secret2');
					break;
				case $this->config->get('globalonepayhpp_terminal1') :
					$data['currency'] =$this->config->get('globalonepayhpp_currency1');
					$secret = $this->config->get('globalonepayhpp_secret1');
			}
			$data['terminalid'] = $this->request->post['TERMINALID'];
			
			$expectedResponseHash = md5($this->request->post['TERMINALID'] . $this->request->post['ORDERID'] . $this->request->post['AMOUNT'] . $this->request->post['DATETIME'] . $this->request->post['RESPONSECODE'] . $this->request->post['RESPONSETEXT'] . $secret);
			if ($this->request->post['HASH'] == $expectedResponseHash) {
				if ($this->request->post['RESPONSECODE'] == 'A') {
					$this->load->model('checkout/order');
					
					$message = '';
					if (isset($this->request->post['UNIQUEREF'])) $message .= 'Unique Reference: ' . $this->request->post['UNIQUEREF'] . "\n";
					if (isset($this->request->post['RESPONSETEXT'])) $message .= 'Response Message: ' . $this->request->post['RESPONSETEXT'] . "\n";
					if (isset($this->request->post['APPROVALCODE'])) $message .= 'Bank Approval Code: ' . $this->request->post['APPROVALCODE'] . "\n";
					if (isset($this->request->post['AVSRESPONSE'])) $message .= 'AVS Response: ' . $this->request->post['AVSRESPONSE'] . "\n";
					if (isset($this->request->post['CVVRESPONSE'])) $message .= 'CVV Response: ' . $this->request->post['CVVRESPONSE'] . "\n";
					if (isset($this->request->post['DATETIME'])) $message .= 'Server date & Time: ' . $this->request->post['DATETIME'];
					
					$this->model_checkout_order->addOrderHistory($this->request->post['ORDERID'], $this->config->get('globalonepayhpp_status_success'), $message, FALSE);
					
		  			die("OK");
				} else {
					if (isset($this->request->post['RESPONSETEXT'])) $message = 'Response Message: ' . $this->request->post['RESPONSETEXT'];
					$this->model_checkout_order->addOrderHistory($this->request->post['ORDERID'], $this->config->get('globalonepayhpp_status_declined'), $message, FALSE);
					die("OK");
				}
			} else {
				$message = "Response hash validation failed. Expected $expectedResponseHash, received: " . $this->request->post['HASH'] . ".";
				$this->model_checkout_order->addOrderHistory($this->request->post['ORDERID'], $this->config->get('globalonepayhpp_status_failed'), $message, FALSE);
				die("Response hash validation failed.");
			}
		} else {
			$message = "Not all of RESPONSECODE, TERMINALID and HASH exist in validation request.";
			$this->model_checkout_order->addOrderHistory($this->request->post['ORDERID'], $this->config->get('globalonepayhpp_status_failed'), $message, FALSE);
			die("Not all of RESPONSECODE, TERMINALID and HASH exist in validation request.");
		}
	}
}
?>
