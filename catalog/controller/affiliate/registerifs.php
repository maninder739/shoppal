<?php
class ControllerAffiliateRegisterifs extends Controller {
	private $error = array();

	public function index() {
		 
		if ($this->affiliate->isLogged()) {
			$this->response->redirect($this->url->link('affiliate/account', '', true));
		}
	
		$this->load->language('affiliate/register');

		$this->document->setTitle($this->language->get('heading_title'));

		
		//echo $data['other_hear_about_us'] = $getAffiliate['firstname'].' '.$getAffiliate['lastname']; die;
		// echo "<pre>";
		// print_r($this->request->post); die;

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$data['data_post'] = 'data_post';
			

			if($this->validate()){
			
				// Clear any previous login attempts in not registered.
				$this->load->model('account/customer');
				$this->model_account_customer->deleteLoginAttempts($this->request->post['email']);

				$this->affiliate->login($this->request->post['email'], $this->request->post['password']);
				$data = $this->request->post;
				// logo
				if(!empty($this->request->files['custom_logo']['tmp_name'])){
					$uploads_dir = 'image/data/';
					
					if (is_uploaded_file($this->request->files['custom_logo']['tmp_name'])) {
						$path = $this->request->files['custom_logo']['name'];
						$ext = pathinfo($path, PATHINFO_EXTENSION);
						$image = substr(md5(uniqid(rand(), true)), 0, 12) . ".$ext";
						move_uploaded_file($this->request->files['custom_logo']['tmp_name'],$uploads_dir.$image);
						$data['custom_logo'] = $image;
					}
				} else {
					$data['custom_logo'] = '';
				}

				
				/*if(!empty($this->request->files['logo']['tmp_name'])){
					$uploads_dir = 'image/data/';
					if (is_uploaded_file($this->request->files['logo']['tmp_name'])) {
						$path = $this->request->files['logo']['name'];
						$ext = pathinfo($path, PATHINFO_EXTENSION);
						$image = substr(md5(uniqid(rand(), true)), 0, 12) . ".$ext";
						move_uploaded_file($this->request->files['logo']['tmp_name'],$uploads_dir.$image);
						$data['logo'] = $image;
					}
				} else {
					$data['logo'] = '';
				}*/
				
				$data['ifs'] = 2;
				$affiliate_id = $this->model_affiliate_affiliate->addAffiliate($data);
				if($data['ifs']) {
					$this->model_affiliate_affiliate->UpdateAffiliateIfs($affiliate_id);
				}
				$approval = (int)!$this->config->get('config_affiliate_approval');

				$this->model_affiliate_affiliate->setApprovel($affiliate_id, $approval);
				 
				// logo
				
				// Add to activity log
				if ($this->config->get('config_customer_activity')) {
					$this->load->model('affiliate/activity');

					$activity_data = array(
						'affiliate_id' => $affiliate_id,
						'name'         => $this->request->post['firstname'] . ' ' . $this->request->post['lastname']
					);

					$this->model_affiliate_activity->addActivity('register', $activity_data);
				}

				$this->response->redirect($this->url->link('affiliate/success'));
			}
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('affiliate/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_register'),
			'href' => $this->url->link('affiliate/registerifs', '', true)
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_select'] = $this->language->get('text_select');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_account_already'] = sprintf($this->language->get('text_account_already'), $this->url->link('affiliate/login', '', true));
		$data['text_signup'] = $this->language->get('text_signup');
		$data['text_your_details'] = $this->language->get('text_your_details');
		$data['text_your_address'] = $this->language->get('text_your_address');
		$data['text_payment'] = $this->language->get('text_payment');
		$data['text_your_password'] = $this->language->get('text_your_password');
		$data['text_cheque'] = $this->language->get('text_cheque');
		$data['text_paypal'] = $this->language->get('text_paypal');
		$data['text_bank'] = $this->language->get('text_bank');
        
        $data['entry_organization_name'] = $this->language->get('entry_organization_name');
		$data['entry_firstname'] = $this->language->get('entry_firstname');
		$data['entry_lastname'] = $this->language->get('entry_lastname');
		$data['entry_position_organization'] = $this->language->get('entry_position_organization');
		$data['entry_organization_phone_number'] = $this->language->get('entry_organization_phone_number');
		$data['entry_organization_phone_number_format'] = $this->language->get('entry_organization_phone_number_format');
		$data['entry_organization_details'] = $this->language->get('entry_organization_details');
		$data['entry_agreement_heading'] = $this->language->get('entry_agreement_heading');
		$data['entry_agreement_checkbox1'] = $this->language->get('entry_agreement_checkbox1');
		$data['entry_agreement_checkbox2'] = $this->language->get('entry_agreement_checkbox2');
		$data['entry_agreement_checkbox3'] = $this->language->get('entry_agreement_checkbox3');
		$data['entry_branded_website'] = $this->language->get('entry_branded_website');
		$data['entry_organization_website'] = $this->language->get('entry_organization_website');
		$data['entry_organization_hashtags'] = $this->language->get('entry_organization_hashtags');
		$data['entry_organization_facebook_link'] = $this->language->get('entry_organization_facebook_link');
		$data['entry_organization_instagram'] = $this->language->get('entry_organization_instagram');
		$data['entry_organization_twitter'] = $this->language->get('entry_organization_twitter');
		$data['entry_organization_linkedIn_link'] = $this->language->get('entry_organization_linkedIn_link');
		$data['entry_organization_mission_statement'] = $this->language->get('entry_organization_mission_statement');
		$data['entry_organization_detailed'] = $this->language->get('entry_organization_detailed');
		$data['entry_organization_detailed_french'] = $this->language->get('entry_organization_detailed_french');
		$data['entry_hear_about_us'] = $this->language->get('entry_hear_about_us');
		$data['entry_type_question'] = $this->language->get('entry_type_question'); 
		$data['entry_shoppal_website'] = $this->language->get('entry_shoppal_website');
		$data['entry_local_store'] = $this->language->get('entry_local_store'); 
		$data['entry_local_flyer'] = $this->language->get('entry_local_flyer'); 
		$data['entry_other_fundraising_organization'] = $this->language->get('entry_other_fundraising_organization');
		$data['entry_other_hear_about_us'] = $this->language->get('entry_other_hear_about_us');
		$data['entry_logo'] = $this->language->get('entry_logo');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_telephone'] = $this->language->get('entry_telephone');
		$data['entry_telephone_format'] = $this->language->get('entry_telephone_format');
		$data['entry_fax'] = $this->language->get('entry_fax');
		$data['entry_company'] = $this->language->get('entry_company');
		$data['entry_website'] = $this->language->get('entry_website');
		$data['entry_address_1'] = $this->language->get('entry_address_1');
		$data['entry_address_2'] = $this->language->get('entry_address_2');
		$data['entry_postcode'] = $this->language->get('entry_postcode');
		$data['entry_city'] = $this->language->get('entry_city');
		$data['entry_country'] = $this->language->get('entry_country');
		$data['entry_zone'] = $this->language->get('entry_zone');
		$data['entry_tax'] = $this->language->get('entry_tax');
		$data['entry_payment'] = $this->language->get('entry_payment');
		$data['entry_cheque'] = $this->language->get('entry_cheque');
		$data['entry_paypal'] = $this->language->get('entry_paypal');
		$data['entry_bank_name'] = $this->language->get('entry_bank_name');
		$data['entry_bank_branch_number'] = $this->language->get('entry_bank_branch_number');
		$data['entry_bank_swift_code'] = $this->language->get('entry_bank_swift_code');
		$data['entry_bank_account_name'] = $this->language->get('entry_bank_account_name');
		$data['entry_bank_account_number'] = $this->language->get('entry_bank_account_number');
		$data['entry_password'] = $this->language->get('entry_password');
		$data['entry_confirm'] = $this->language->get('entry_confirm');
		$data['button_continue'] = $this->language->get('button_continue');
		
		/*  new fields */
		$data['entry_custom_logo'] = $this->language->get('entry_custom_logo');
		$data['entry_mission'] = $this->language->get('entry_mission');
		$data['entry_mission_short'] = $this->language->get('entry_mission_short');
		$data['entry_mission_max'] = $this->language->get('entry_mission_max');		
		$data['entry_mission_french'] = $this->language->get('entry_mission_french');
		$data['entry_mission_french_short'] = $this->language->get('entry_mission_french_short');		
		$data['entry_slide_info'] = $this->language->get('entry_slide_info');
		$data['entry_slide_background'] = $this->language->get('entry_slide_background');
		$data['error_logo1'] = $this->language->get('error_logo1');
		$data['logo_later'] = $this->language->get('logo_later');
		$data['logo_now'] = $this->language->get('logo_now');
		$data['form_error'] = $this->language->get('form_error');
		$data['look_below'] = $this->language->get('look_below');
		$data['close_modal'] = $this->language->get('close_modal');
		$data['other_details_text'] = $this->language->get('other_details_text');
		$data['other_details_input'] = $this->language->get('other_details_input');
		
		$data['entry_mission_txt_long'] = $this->language->get('entry_mission_txt_long');
		$data['entry_mission_long'] = $this->language->get('entry_mission_long');
		
		$data['entry_mission_french_txt_long'] = $this->language->get('entry_mission_french_txt_long');
		$data['entry_mission_french_long'] = $this->language->get('entry_mission_french_long');
		
		/*  new fields */
		
		$data['get_errors'] = '';

		if (isset($this->error['warning'])) {
			$data['get_errors'] = 'error_exist1';
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['firstname'])) {
			$data['get_errors'] = 'error_exist2';
			$data['error_firstname'] = $this->error['firstname'];
		} else {
			$data['error_firstname'] = '';
		}

		if (isset($this->error['lastname'])) {
			$data['get_errors'] = 'error_exist3';
			$data['error_lastname'] = $this->error['lastname'];
		} else {
			$data['error_lastname'] = '';
		}

		if (isset($this->error['email'])) {
			$data['get_errors'] = 'error_exist4';
			$data['error_email'] = $this->error['email'];
		} else {
			$data['error_email'] = '';
		}

		if (isset($this->error['telephone'])) {
			$data['get_errors'] = 'error_exist5';
			$data['error_telephone'] = $this->error['telephone'];
		} else {
			$data['error_telephone'] = '';
		}

		if (isset($this->error['password'])) {
			$data['get_errors'] = 'error_exist6';
			$data['error_password'] = $this->error['password'];
		} else {
			$data['error_password'] = '';
		}

		if (isset($this->error['confirm'])) {
			$data['get_errors'] = 'error_exist7';
			$data['error_confirm'] = $this->error['confirm'];
		} else {
			$data['error_confirm'] = '';
		}

		if (isset($this->error['address_1'])) {
			$data['get_errors'] = 'error_exist8';
			$data['error_address_1'] = $this->error['address_1'];
		} else {
			$data['error_address_1'] = '';
		}

		if (isset($this->error['city'])) {
			$data['get_errors'] = 'error_exist9';
			$data['error_city'] = $this->error['city'];
		} else {
			$data['error_city'] = '';
		}

		if (isset($this->error['postcode'])) {
			$data['get_errors'] = 'error_exist10';
			$data['error_postcode'] = $this->error['postcode'];
		} else {
			$data['error_postcode'] = '';
		}

		if (isset($this->error['country'])) {
			$data['get_errors'] = 'error_exist11';
			$data['error_country'] = $this->error['country'];
		} else {
			$data['error_country'] = '';
		}

		if (isset($this->error['zone'])) {
			$data['get_errors'] = 'error_exist12';
			$data['error_zone'] = $this->error['zone'];
		} else {
			$data['error_zone'] = '';
		}

		if (isset($this->error['organization_phone_number'])) {
			$data['get_errors'] = 'error_exist13';
			$data['error_organization_phone_number'] = $this->error['organization_phone_number'];
		} else {
			$data['error_organization_phone_number'] = '';
		}
		
		if (isset($this->error['custom_logo_error'])) {
			$data['no_custom_logo'] = 'error_exist14';
			
		} 
		// if (isset($this->error['error_mission_both'])) {
			// $data['get_errors'] = 'error_exist15';
			// $data['error_mission_both'] = $this->error['error_mission_both'];
		// } else {
			// $data['error_mission_both'] = '';
		// }
		
		// if (isset($this->error['error_organization_mission_both'])) {
			// $data['get_errors'] = 'error_exist16';
			// $data['error_organization_mission_both'] = $this->error['error_organization_mission_both'];
		// } else {
			// $data['error_organization_mission_both'] = '';
		// }

		// if (isset($this->error['organization_mission_statement'])) {
			// $data['error_organization_mission_statement'] = $this->error['organization_mission_statement'];
		// } else {
			// $data['error_organization_mission_statement'] = '';
		// }

		$data['action'] = $this->url->link('affiliate/registerifs', '', true);

		if (isset($this->request->post['organization_name'])) {
			$data['organization_name'] = $this->request->post['organization_name'];
		} else {
			$data['organization_name'] = '';
		}
		
		if (isset($this->request->post['position_in_organization'])) {
			$data['position_in_organization'] = $this->request->post['position_in_organization'];
		} else {
			$data['position_in_organization'] = '';
		}

		if (isset($this->request->post['organization_mission_statement'])) {
			$data['organization_mission_statement'] = $this->request->post['organization_mission_statement'];
		} else {
			$data['organization_mission_statement'] = '';
		}


		if (isset($this->request->post['organization_phone_number'])) {
			$data['organization_phone_number'] = $this->request->post['organization_phone_number'];
		} else {
			$data['organization_phone_number'] = '';
		}
		

		if (isset($this->request->post['organization_detailed'])) {
			$data['organization_detailed'] = $this->request->post['organization_detailed'];
		} else {
			$data['organization_detailed'] = '';
		}

		if (isset($this->request->post['organization_detailed_french'])) {
			$data['organization_detailed_french'] = $this->request->post['organization_detailed_french'];
		} else {
			$data['organization_detailed_french'] = '';
		}

		if (isset($this->request->post['branded_website'])) {
			$data['branded_website'] = $this->request->post['branded_website'];
		} else {
			$data['branded_website'] = '';
		}

		if (isset($this->request->post['organization_website'])) {
			$data['organization_website'] = $this->request->post['organization_website'];
		} else {
			$data['organization_website'] = '';
		}
		
		if (isset($this->request->post['organization_hashtags'])) {
			$data['organization_hashtags'] = $this->request->post['organization_hashtags'];
		} else {
			$data['organization_hashtags'] = '';
		}
		
		if (isset($this->request->post['organization_facebook_link'])) {
			$data['organization_facebook_link'] = $this->request->post['organization_facebook_link'];
		} else {
			$data['organization_facebook_link'] = '';
		}
		
		if (isset($this->request->post['organization_instagram'])) {
			$data['organization_instagram'] = $this->request->post['organization_instagram'];
		} else {
			$data['organization_instagram'] = '';
		}
		
		if (isset($this->request->post['organization_twitter'])) {
			$data['organization_twitter'] = $this->request->post['organization_twitter'];
		} else {
			$data['organization_twitter'] = '';
		}
		
		if (isset($this->request->post['organization_linkedIn_link'])) {
			$data['organization_linkedIn_link'] = $this->request->post['organization_linkedIn_link'];
		} else {
			$data['organization_linkedIn_link'] = '';
		}

		
		if (isset($this->request->post['other_details_input'])) {
			$data['other_details_input'] = $this->request->post['other_details_input'];
		} else {
			$data['other_details_input'] = '';
		}

		 

		if (isset($this->request->post['other_details'])) {
			$data['other_details'] =  $this->request->post['other_details'];
		} else {
			$data['other_details'] = '';
		}

		// $data['checked_facebook'] = '';
		// $data['checked_linkedIn'] = '';
		// $data['checked_shoppal'] = '';
		// $data['checked_local_store'] = '';
		// $data['checked_loal_event'] = '';
		// $data['checked_flyer'] = '';
		// $data['checked_other_fundraising_organization'] = '';
		// $data['checked_other'] = '';
		// $data['other_hear_about_us'] = '';
		// $data['disabled'] = '';
		// $data['readonly'] = '';
		// if (isset($this->request->post['hear_about_us'])) {

			// if ($this->request->post['hear_about_us'] == 'Facebook')
			// {
				// $data['checked_facebook'] = 'checked';
			// }
			// if ($this->request->post['hear_about_us'] == 'LinkedIn')
			// {
				// $data['checked_linkedIn'] = 'checked';
			// }
			// if ($this->request->post['hear_about_us'] == 'ShopPal Website')
			// {
				// $data['checked_shoppal'] = 'checked';
			// }
			// if ($this->request->post['hear_about_us'] == 'Local Store')
			// {
				// $data['checked_local_store'] = 'checked';
			// }
			// if ($this->request->post['hear_about_us'] == 'Local Event')
			// {
				// $data['checked_loal_event'] = 'checked';
			// }
			// if ($this->request->post['hear_about_us'] == 'Flyer')
			// {
				// $data['checked_flyer'] = 'checked';
			// }if ($this->request->post['hear_about_us'] == 'Other Fundraising Organization')
			// {
				// $data['checked_other_fundraising_organization'] = 'checked';
			// }
			
			// if ($this->request->post['hear_about_us'] == 'other')
			// {
				// $data['checked_other'] = 'checked';
				// if (isset($this->request->post['other_hear_about_us'])) {
					// $data['other_hear_about_us'] = $this->request->post['other_hear_about_us'];
				// } else {
					// $data['other_hear_about_us'] = '';
				// }
			// }

		// }
		
		if (isset($this->request->post['hear_about_us'])) {
			$data['checked_facebook'] = '';
			$data['checked_linkedIn'] = '';
			$data['checked_shoppal'] = '';
			$data['checked_local_store'] = '';
			$data['checked_loal_event'] = '';
			$data['checked_flyer'] = '';
			$data['checked_other_fundraising_organization'] = '';
			$data['hear_about_us'] = $this->request->post['hear_about_us'];
			$checked_facebook = 'Facebook';
			$checked_linkedIn = 'LinkedIn';
			$checked_shoppal = 'ShopPal Website';
			$checked_local_store = 'Local Store';
			$checked_loal_event = 'Local Event';
			$checked_flyer = 'Flyer';
			$checked_other_fundraising_organization = 'Other Fundraising Organization';
			
			foreach($data['hear_about_us'] as $gethearaboutus)
			{
				if($gethearaboutus == $checked_facebook)
				{
					$data['checked_facebook'] = 'checked';
				}
				if($gethearaboutus == $checked_linkedIn)
				{
					$data['checked_linkedIn'] = 'checked';
				}
				if($gethearaboutus == $checked_shoppal)
				{
					$data['checked_shoppal'] = 'checked';
				}
				if($gethearaboutus == $checked_local_store)
				{
					$data['checked_local_store'] = 'checked';
				}
				if($gethearaboutus == $checked_loal_event)
				{
					$data['checked_loal_event'] = 'checked';
				}
				if($gethearaboutus == $checked_flyer)
				{
					$data['checked_flyer'] = 'checked';
				}
				if($gethearaboutus == $checked_other_fundraising_organization)
				{
					$data['checked_other_fundraising_organization'] = 'checked';
				}
				
			}

		}
		else {
			$data['checked_facebook'] = '';
			$data['checked_linkedIn'] = '';
			$data['checked_shoppal'] = '';
			$data['checked_local_store'] = '';
			$data['checked_loal_event'] = '';
			$data['checked_flyer'] = '';
			$data['checked_other_fundraising_organization'] = '';
			$data['hear_about_us'] = '';
		}
		if (isset($this->request->post['hear_about_us_checked']))
			$data['checked_other'] = '';
			{
				$data['checked_other'] = 'checked';
				if (isset($this->request->post['other_hear_about_us'])) {
					$data['other_hear_about_us'] = $this->request->post['other_hear_about_us'];
				} else {
					$data['other_hear_about_us'] = '';
					$data['checked_other'] = '';
				}
				
			}



		if (isset($this->request->post['firstname'])) {
			$data['firstname'] = $this->request->post['firstname'];
		} else {
			$data['firstname'] = '';
		}

		if (isset($this->request->post['lastname'])) {
			$data['lastname'] = $this->request->post['lastname'];
		} else {
			$data['lastname'] = '';
		}

		if (isset($this->request->post['email'])) {
			$data['email'] = $this->request->post['email'];
		} else {
			$data['email'] = '';
		}
		
		/* new fields  */
		/*if (isset($this->error['organization_name'])) {
			$data['error_organization_name'] = $this->error['organization_name'];
		} else {
			$data['error_organization_name'] = '';
		}*/

		if (isset($this->error['position_in_organization'])) {
			$data['error_position_in_organization'] = $this->error['position_in_organization'];
		} else {
			$data['error_position_in_organization'] = '';
		}


		if (isset($this->error['error_mission'])) {
			$data['error_mission'] = $this->error['error_mission'];
		} else {
			$data['error_mission'] = '';
		}
		
		if (isset($this->error['error_mission_french'])) {
			$data['error_mission_french'] = $this->error['error_mission_french'];
		} else {
			$data['error_mission_french'] = '';
		}


		if (isset($this->error['error_mission_both'])) {
			$data['error_mission_both'] = $this->error['error_mission_both'];
		} else {
			$data['error_mission_both'] = '';
		}

		if (isset($this->error['error_organization_mission_both'])) {
			$data['error_organization_mission_both'] = $this->error['error_organization_mission_both'];
		} else {
			$data['error_organization_mission_both'] = '';
		}
		
		
		if (isset($this->error['cmp_required'])) {
			$data['error_company_required'] = $this->error['cmp_required'];
		} else {
			$data['error_company_required'] = '';
		}
		
		if (isset($this->error['company'])) {
			$data['error_company'] = $this->error['company'];
		} else {
			$data['error_company'] = '';
		}
		
		
		/* new fields  */
		
		/* new fields  */
		if (isset($this->request->post['slider_image'])) {
			$data['slider_image'] = $this->request->post['slider_image'];
		} else {
			$data['slider_image'] = '';
		}
		/* new fields  */
		if (isset($this->request->post['telephone'])) {
			$data['telephone'] = $this->request->post['telephone'];
		} else {
			$data['telephone'] = '';
		}

		if (isset($this->request->post['fax'])) {
			$data['fax'] = $this->request->post['fax'];
		} else {
			$data['fax'] = '';
		}
		if (isset($this->request->post['mission'])) {
			$data['entry_mission_view'] = 'Shoppal';
		} else {
			$data['entry_mission_view'] = 'Shoppal';
		}
		
		if (isset($this->request->post['mission_french'])) {
			$data['entry_mission_french_view'] = 'Shoppal';
		} else {
			$data['entry_mission_french_view'] = 'Shoppal';
		}
		
		if (isset($this->request->post['company'])) {
			$data['company'] = $this->request->post['company'];
		} else {
			$data['company'] = '';
		}

		if (isset($this->request->post['website'])) {
			$data['website'] = $this->request->post['website'];
		} else {
			$data['website'] = '';
		}

		if (isset($this->request->post['address_1'])) {
			$data['address_1'] = $this->request->post['address_1'];
		} else {
			$data['address_1'] = '';
		}

		if (isset($this->request->post['address_2'])) {
			$data['address_2'] = $this->request->post['address_2'];
		} else {
			$data['address_2'] = '';
		}

		if (isset($this->request->post['postcode'])) {
			$data['postcode'] = $this->request->post['postcode'];
		} else {
			$data['postcode'] = '';
		}

		if (isset($this->request->post['city'])) {
			$data['city'] = $this->request->post['city'];
		} else {
			$data['city'] = '';
		}

		if (isset($this->request->post['country_id'])) {
			$data['country_id'] = $this->request->post['country_id'];
		} else {
			$data['country_id'] = $this->config->get('config_country_id');
		}

		if (isset($this->request->post['zone_id'])) {
			$data['zone_id'] = (int)$this->request->post['zone_id'];
		} else {
			$data['zone_id'] = '';
		}

		$this->load->model('localisation/country');

		$data['countries'] = $this->model_localisation_country->getCountries();

		if (isset($this->request->post['tax'])) {
			$data['tax'] = $this->request->post['tax'];
		} else {
			$data['tax'] = '';
		}

		if (isset($this->request->post['payment'])) {
			$data['payment'] = $this->request->post['payment'];
		} else {
			$data['payment'] = 'cheque';
		}

		if (isset($this->request->post['cheque'])) {
			$data['cheque'] = $this->request->post['cheque'];
		} else {
			$data['cheque'] = '';
		}

		if (isset($this->request->post['paypal'])) {
			$data['paypal'] = $this->request->post['paypal'];
		} else {
			$data['paypal'] = '';
		}

		if (isset($this->request->post['bank_name'])) {
			$data['bank_name'] = $this->request->post['bank_name'];
		} else {
			$data['bank_name'] = '';
		}

		if (isset($this->request->post['bank_branch_number'])) {
			$data['bank_branch_number'] = $this->request->post['bank_branch_number'];
		} else {
			$data['bank_branch_number'] = '';
		}

		if (isset($this->request->post['bank_swift_code'])) {
			$data['bank_swift_code'] = $this->request->post['bank_swift_code'];
		} else {
			$data['bank_swift_code'] = '';
		}

		if (isset($this->request->post['bank_account_name'])) {
			$data['bank_account_name'] = $this->request->post['bank_account_name'];
		} else {
			$data['bank_account_name'] = '';
		}

		if (isset($this->request->post['bank_account_number'])) {
			$data['bank_account_number'] = $this->request->post['bank_account_number'];
		} else {
			$data['bank_account_number'] = '';
		}

		if (isset($this->request->post['password'])) {
			$data['password'] = $this->request->post['password'];
		} else {
			$data['password'] = '';
		}

		if (isset($this->request->post['confirm'])) {
			$data['confirm'] = $this->request->post['confirm'];
		} else {
			$data['confirm'] = '';
		}

		// Captcha
		if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('register', (array)$this->config->get('config_captcha_page'))) {
			$data['captcha'] = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha'), $this->error);
		} else {
			$data['captcha'] = '';
		}

		/* For checkboxes*/
		
		if (isset($this->request->post['zone_id'])) {
			$data['zone_id'] = $this->request->post['zone_id'];
		} else {
			$data['zone_id'] = '';
		}
		
		if (isset($this->request->post['agreement'])) {
			$data['checkBox1'] = '';
			$data['checkBox2'] = '';
			$data['checkBox3'] = '';
			$data['agreement'] = $this->request->post['agreement'];
			$checkBox1 = 'on the main, or other pages of the ShopPal website that display Memberinformation';
			$checkBox2 = 'on Social Media and other Internet Marketing platforms, and/or';
			$checkBox3 = 'on Print Media and or Radio/Television Media.';
			foreach($data['agreement'] as $getAggrement)
			{
				$getAggrement = strtolower(trim($getAggrement));
				if($getAggrement == strtolower(trim($checkBox1)))
				{
					$data['checkBox1'] = 'checked';
				}
				if($getAggrement == strtolower(trim($checkBox2)))
				{
					$data['checkBox2'] = 'checked';
				}
				if($getAggrement == strtolower(trim($checkBox3)))
				{
					$data['checkBox3'] = 'checked';
				}
			}
			
		} else {
			$data['agreement'] = '';
			$data['checkBox1'] = '';
			$data['checkBox2'] = '';
			$data['checkBox3'] = '';
		}
		//

		if ($this->config->get('config_affiliate_id')) {
			$this->load->model('catalog/information');

			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_affiliate_id'));

			if ($information_info) {
				$data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/agree', 'information_id=' . $this->config->get('config_affiliate_id'), true), $information_info['title'], $information_info['title']);
			} else {
				$data['text_agree'] = '';
			}
		} else {
			$data['text_agree'] = '';
		}

		if (isset($this->request->post['agree'])) {
			$data['agree'] = $this->request->post['agree'];
		} else {
			$data['agree'] = false;
		}


		$this->load->model('affiliate/affiliate');
		
			$default_code = $this->model_affiliate_affiliate->getAffiliateByEmail('corp@shoppal.ca');
			if($default_code)
			{
				$data['disabled'] = 'disabled';
				$data['readonly'] = 'readonly';
				$data['other_hear_about_us'] = $default_code['code'];
				$data['checked_other'] = 'checked';
			}
		
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		
		$this->response->setOutput($this->load->view('affiliate/registerifs', $data));
		
	}

	protected function validate() {
		
		/*if ((utf8_strlen(trim($this->request->post['organization_name'])) < 1) || (utf8_strlen(trim($this->request->post['organization_name'])) > 50)) {
			$this->error['organization_name'] = $this->language->get('error_organization_name');
		}*/
		if($this->request->post['custom_logo_status']!=1)
		{
			if(empty($this->request->files['custom_logo']['tmp_name']))
			{
				$this->error['custom_logo_error'] = 'logo_error';
				
			}
		}

		if ((utf8_strlen(trim($this->request->post['position_in_organization'])) < 1) || (utf8_strlen(trim($this->request->post['position_in_organization'])) > 30)) {
			$this->error['position_in_organization'] = $this->language->get('error_position_in_organization');
		}
		
		
		if ((utf8_strlen(trim($this->request->post['position_in_organization'])) < 1) || (utf8_strlen(trim($this->request->post['position_in_organization'])) > 30)) {
			$this->error['position_in_organization'] = $this->language->get('error_position_in_organization');
		}

		if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$this->error['email'] = $this->language->get('error_email');
		}

		// if (!preg_match("/^\([0-9]{3}\) [0-9]{3}-[0-9]{4}$/i", $this->request->post['organization_phone_number'])) {
		// 	$this->error['organization_phone_number'] = $this->language->get('error_organization_phone_number');
		// }

		if (!preg_match("/^\([0-9]{3}\) [0-9]{3}-[0-9]{4}$/i", $this->request->post['organization_phone_number']) && !preg_match("/^[0-9]{10}$/i", $this->request->post['organization_phone_number'])) {
			$this->error['organization_phone_number'] = $this->language->get('error_organization_phone_number');
		}
		
		/*if ((utf8_strlen($this->request->post['organization_mission_statement']) < 1)) {
			$this->error['organization_mission_statement'] = $this->language->get('error_organization_mission_statement');
		}*/
		
		$this->load->model('affiliate/affiliate');
		if ($this->model_affiliate_affiliate->getTotalAffiliatesByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_exists');
		}

		// if (!preg_match("/^\([0-9]{3}\) [0-9]{3}-[0-9]{4}$/i", $this->request->post['telephone'])) {
		// 	$this->error['telephone'] = $this->language->get('error_telephone');
		// }

		if (!preg_match("/^\([0-9]{3}\) [0-9]{3}-[0-9]{4}$/i", $this->request->post['telephone']) && !preg_match("/^[0-9]{10}$/i", $this->request->post['telephone'])) {
			$this->error['telephone'] = $this->language->get('error_telephone');
		}

		if ((utf8_strlen(trim($this->request->post['address_1'])) < 3) || (utf8_strlen(trim($this->request->post['address_1'])) > 128)) {
			$this->error['address_1'] = $this->language->get('error_address_1');
		}

		if ((utf8_strlen(trim($this->request->post['city'])) < 2) || (utf8_strlen(trim($this->request->post['city'])) > 128)) {
			$this->error['city'] = $this->language->get('error_city');
		}

		if (utf8_strlen(trim($this->request->post['postcode'])) < 2 || utf8_strlen(trim($this->request->post['postcode'])) > 10) {
			$this->error['postcode'] = $this->language->get('error_postcode');
		}

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($this->request->post['country_id']);

		if ($country_info && $country_info['postcode_required'] && (utf8_strlen(trim($this->request->post['postcode'])) < 2 || utf8_strlen(trim($this->request->post['postcode'])) > 10)) {
			$this->error['postcode'] = $this->language->get('error_postcode');
		}

		if ($this->request->post['country_id'] == '') {
			$this->error['country'] = $this->language->get('error_country');
		}

		if (!isset($this->request->post['zone_id']) || $this->request->post['zone_id'] == '' || !is_numeric($this->request->post['zone_id'])) {
			$this->error['zone'] = $this->language->get('error_zone');
		}

		if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
			$this->error['password'] = $this->language->get('error_password');
		}

		if ($this->request->post['confirm'] != $this->request->post['password']) {
			$this->error['confirm'] = $this->language->get('error_confirm');
		}

		// Captcha
		if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('register', (array)$this->config->get('config_captcha_page'))) {
			$captcha = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha') . '/validate');

			if ($captcha) {
				$this->error['captcha'] = $captcha;
			}
		}

		if ($this->config->get('config_affiliate_id')) {
			$this->load->model('catalog/information');

			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_affiliate_id'));

			if ($information_info && !isset($this->request->post['agree'])) {
				$this->error['warning'] = sprintf($this->language->get('error_agree'), $information_info['title']);
			}
		}
		
		/* new fields */

		
		// if(isset($this->request->post['mission']) && isset($this->request->post['mission_french']) )
		// {
			// if ((utf8_strlen(trim($this->request->post['mission'])) < 1) && (utf8_strlen(trim($this->request->post['mission_french'])) < 1)) {
				// $this->error['error_mission_both'] = $this->language->get('error_mission_both');
				
			// }
		// }
		// if(isset($this->request->post['organization_detailed']) && isset($this->request->post['organization_detailed_french']) )
		// {
			// if ((utf8_strlen(trim($this->request->post['organization_detailed'])) < 1) && (utf8_strlen(trim($this->request->post['organization_detailed_french'])) < 1)) {
				// $this->error['error_organization_mission_both'] = $this->language->get('error_organization_mission_both');
				
			// }
		// }
		
		// if(isset($this->request->post['mission']))
		// {
			// if ((utf8_strlen(trim($this->request->post['mission'])) > 217)) {
				// $this->error['error_mission'] = $this->language->get('error_mission');
			// }
		// }
		// if(isset($this->request->post['mission_french']))
		// {
			// if ((utf8_strlen(trim($this->request->post['mission_french'])) > 217)) {
				// $this->error['error_mission_french'] = $this->language->get('error_mission_french');
			// }
		// }
		
		if ((utf8_strlen(trim($this->request->post['company'])) < 1) || (utf8_strlen(trim($this->request->post['company'])) > 100)) {
			$this->error['cmp_required'] = $this->language->get('cmp_required');
		}
		if ($this->model_affiliate_affiliate->getCompanyName($this->request->post['company'])) {
			$this->error['company'] = $this->language->get('error_company');
		}
		
		/*if (file_exists($_FILES['logo']['tmp_name'])){

			$allowedExts = array("gif", "jpeg", "jpg", "png");

			$name = explode(".", $_FILES["logo"]["name"]);
			$extension = end($name);
 
			if ((($_FILES["logo"]["type"] == "image/gif")
			|| ($_FILES["logo"]["type"] == "image/jpeg")
			|| ($_FILES["logo"]["type"] == "image/jpg")
			|| ($_FILES["logo"]["type"] == "image/png"))
			&& in_array($extension, $allowedExts)){

			}
			else{
				$this->error['logo'] = $this->language->get('error_logo_type');
			}

		}
		else{
			$this->error['logo'] = $this->language->get('error_logo');
		}*/

		/* new fields */
		return !$this->error;
	}

	public function country() {
		$json = array();

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);

		if ($country_info) {
			$this->load->model('localisation/zone');

			$json = array(
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
				'status'            => $country_info['status']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}



	private function uploadLogo($fileData){

		$uploads_dir = 'image/data/';
		if (is_uploaded_file($fileData['tmp_name'])) {
			$path = $fileData['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
			$image = substr(md5(uniqid(rand(), true)), 0, 12) . ".$ext";
			
			if(move_uploaded_file($fileData['tmp_name'],$uploads_dir.$image)){
				return $image;
			}
			 
		}

	}

}
