<?php

class ControllerAffiliateMtaEvent extends Controller {

	public function eventAddCustomerPost($route, $args, $customer_id) {
		$data = $args[0];
		$res = $this->db->query("SELECT affiliate_id FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int) $customer_id . "' AND affiliate_id > 0");
		if($res->num_rows > 0) return;	
		$dt = isset($data['tracking']) ? $data['tracking'] : false;
		$aff_id = 0;
		if (isset($this->request->cookie['tracking']) || $dt || isset($this->request->get['tracking'])) $this->load->model('affiliate/affiliate');
		if (isset($this->request->cookie['tracking'])) {			
			$affiliate_info = $this->model_affiliate_affiliate->getAffiliateByCode($this->request->cookie['tracking']);				
			if ($affiliate_info) $aff_id = $affiliate_info['affiliate_id'];
		}
		if(!$aff_id && $dt) {
			$affiliate_info = $this->model_affiliate_affiliate->getAffiliateByCode($dt);		
			if ($affiliate_info) $aff_id = $affiliate_info['affiliate_id'];
		}		
		if(!$aff_id && isset($this->request->get['tracking'])) {
			$affiliate_info = $this->model_affiliate_affiliate->getAffiliateByCode($this->request->get['tracking']);		
			if ($affiliate_info) $aff_id = $affiliate_info['affiliate_id'];
		}		
		
		if($aff_id) $this->db->query("UPDATE " . DB_PREFIX . "customer SET affiliate_id='" . (int) $aff_id . "' WHERE customer_id='" . (int) $customer_id . "'");		
	}

	public function eventAddAffiliatePost($route, $args, $affiliate_id) {
		$this->load->model('affiliate/mta_affiliate');
		$this->model_affiliate_mta_affiliate->addAffiliate($affiliate_id, $this->model_affiliate_mta_affiliate->find_parent(), (isset($this->request->cookie['mta']) ? $this->request->cookie['mta'] : ''));	
	}

	public function eventAddOrderHistoryPre() {
		$this->config->set('config_affiliate_auto', false);
	}	
	
	public function eventAutoAddCommissions($route, $args) {
		$order_id = $args[0];
		if(!$order_id) return;
		$_autoadd_statuses = $this->config->get('mta_ypx_autoadd_statuses');
		if(!is_array($_autoadd_statuses)) $_autoadd_statuses = $this->config->get('config_complete_status');
		$_res = $this->db->query("SELECT order_id FROM `" . DB_PREFIX . "order` WHERE order_id = '" . (int)$order_id . "' AND order_status_id IN (" . implode(',', $_autoadd_statuses) . ")");
		if($_res->num_rows < 1) return;		
		$this->load->model('affiliate/mta_affiliate');
		$this->model_affiliate_mta_affiliate->autoAddCommissions($order_id);		
	}		

	public function eventAddOrderPost($route, $args, $order_id) {
		$data = $args[0];
		if(!$order_id || !$data) return;		
		$this->load->model('affiliate/mta_affiliate');
		$this->model_affiliate_mta_affiliate->addOrder($order_id, $data);
	}	

	public function eventEditOrderPost($route, $args) {		
		$order_id = $args[0];
		$data = $args[1];
		if(!$order_id || !$data) return;		
		$this->load->model('affiliate/mta_affiliate');
		$this->model_affiliate_mta_affiliate->editOrder($order_id, $data);
	}		

	public function eventAddTransactionPost($route, $args, $affiliate_transaction_id) {
		//$affiliate_transaction_id = $args[0];
		if(!$affiliate_transaction_id) return;
		$descr = $this->config->get('_mta_transaction_description_');
		if(!$descr) return;
		$this->config->set('_mta_transaction_description_', '');
		$this->db->query("UPDATE " . DB_PREFIX . "affiliate_transaction SET description = '" . $this->db->escape($descr) . "' WHERE affiliate_transaction_id = '" . (int)$affiliate_transaction_id . "'");
	}

	public function eventDeleteOrder($route, $args) {
		$order_id = $args[0];
		$this->load->model('affiliate/mta_affiliate');
		$this->model_affiliate_mta_affiliate->deleteOrder($order_id);
	}
	
}