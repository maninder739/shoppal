<?php

// Heading

$_['heading_title']        = 'Créer un compte';


// Text

$_['text_account']         = 'Compte';
$_['text_register']        = 'S`inscrire';
$_['text_account_already'] = 'Si vous avez déjà un compte chez nous, connectez-vous au <a href="%s"> page de connexion </a> .';
$_['text_your_details']    = 'Vos informations personnelles';
$_['text_your_address']    = 'Votre adresse';
$_['text_newsletter']      = 'Bulletin';
$_['text_your_password']   = 'Votre mot de passe';
$_['text_agree']           = 'j`ai lu et accepté les <a href="%s" class="agree">  <b> %s </b>  </a> 
';
$_['text_autoship']           = 'Votre commande de livraison automatique sera envoyée sur votre adresse par défaut.';


// Entry

$_['entry_customer_group'] = 'Groupe de clients';
$_['entry_firstname']      = 'Prénom';
$_['entry_lastname']       = 'Nom de famille';
$_['entry_email']          = 'Email';
$_['entry_telephone']      = 'Téléphone';
$_['entry_telephone_format']  = 'xxxxxxxxxx';
$_['entry_fax']            = 'Fax';
$_['entry_company']        = 'Compagnie';
$_['entry_address_1']      = 'Adresse 1';
$_['entry_address_2']      = 'Adresse 2';
$_['entry_postcode']       = 'Code postal';
$_['entry_city']           = 'Ville';
$_['entry_country']        = 'Pays';
$_['entry_zone']           = 'Province / État / Région';
$_['entry_newsletter']     = 'Souscrire';
$_['entry_password']       = 'Mot de passe';
$_['entry_confirm']        = 'Confirmer le mot de passe';



// new fields
$_['entry_other_basic_information'] = "Informations de base sur l'organisation";
$_['entry_organization_name'] = "nom de l'organisation";
$_['entry_position_organization'] = "Position dans l'organisation";
$_['entry_mobile_number'] = 'Numéro de portable';
$_['entry_organization_phone_number'] = "Numéro de téléphone de l'organisation";
$_['entry_street_address'] = 'Adresse de rue';
$_['entry_street_address2'] = 'Adresse de rue Ligne 2';
$_['entry_city'] = 'Ville';
$_['entry_zip_code'] = 'Code postal';
$_['entry_country'] = 'Pays';
$_['entry_region'] = 'Province / État / Région';
$_['entry_branded_website'] = 'Préférences de nom de site Web de marque ShopPal (Shoppal.ca/ ???)';
$_['entry_organization_details'] = "Détails de l'organisation";
$_['entry_organization_website'] = "Site Web d'organisation";
$_['entry_organization_hashtags'] = 'Organisation Hashtags';
$_['entry_organization_facebook_link'] = 'Organisation Facebook Lien';
$_['entry_organization_instagram'] = 'Organisation Instagram';
$_['entry_organization_twitter'] = 'Organisation Twitter';
$_['entry_organization_linkedIn_link'] = "Lien LinkedIn de l'organisation";
$_['entry_agreement_heading'] = "L'organisation autorise Shoppal à utiliser son logo et son nom, sans lien avec les montants de collecte de fonds, de la manière suivante";
$_['entry_agreement_checkbox1'] = "sur la page principale ou d'autres pages du site Web ShopPal qui affichent des informations sur les membres";
$_['entry_agreement_checkbox2'] = 'sur les médias sociaux et autres plateformes de marketing Internet, et / ou';
$_['entry_agreement_checkbox3'] = 'sur les médias imprimés et / ou les médias radio / télévision.';
$_['entry_logo'] = 'Logo';
$_['entry_organization_mission_statement'] = "Déclaration de mission de l'organisation";
$_['entry_organization_detailed'] = 'Organisation "Ce que nous faisons" / "Détaillé - À propos de nous"';
$_['entry_hear_about_us'] = 'Comment avez-vous entendu parler de nous?';
$_['entry_type_question'] = 'Tapez une question';
$_['entry_shoppal_website'] = 'Site Web de ShopPal';
$_['entry_local_store'] = 'Magasin local';
$_['entry_local_event'] = 'Événement local';
$_['entry_local_flyer'] = 'Prospectus';
$_['entry_other_fundraising_organization'] = 'Autre organisation de collecte de fonds';
$_['entry_other_hear_about_us'] = "Référencé par";
$_['entry_facebook'] = 'Facebook';
$_['entry_linkedin'] = 'LinkedIn';

// Error

$_['error_exists']         = 'Attention: l`adresse e-mail est déjà enregistrée!';
$_['error_firstname']      = 'Le prénom doit avoir entre 1 et 32 ​​caractères!';
$_['error_lastname']       = 'Le nom de famille doit être compris entre 1 et 32 ​​caractères!';
$_['error_email']          = 'L`adresse e-mail ne semble pas être valide!';
$_['error_telephone']      = 'Le téléphone doit comporter 10 caractères!';
$_['error_address_1']      = 'L`adresse 1 doit comporter entre 3 et 128 caractères!';
$_['error_city']           = 'La ville doit avoir entre 2 et 128 caractères!';
$_['error_postcode']       = 'Le code postal doit avoir 6 caractères!';
$_['error_country']        = 'S`il vous plaît sélectionner un pays!';
$_['error_zone']           = 'S`il vous plaît sélectionner une région / état!';
$_['error_custom_field']   = '%s Champs obligatoires!';
$_['error_password']       = 'Le mot de passe doit comporter entre 4 et 20 caractères!';
$_['error_confirm']        = 'La confirmation du mot de passe ne correspond pas au mot de passe!';
$_['error_agree']          = 'Attention: Toi doit se mettre d`accord à la %s!';

// new
$_['error_organization_name']  = "Le nom de l'organisation doit comporter entre 1 et 50 caractères!";
$_['error_position_in_organization']  = "Doit être entre 1 et 32 ​​caractères!";
$_['error_organization_phone_number']  = "Le numéro de téléphone de l'organisation est invalide";
$_['error_street_address']  = "L'adresse doit être comprise entre 1 et 255 caractères!";
$_['error_city']  = 'Le nom de la ville est requis!';
$_['error_organization_mission_statement']  = "L'énoncé de mission est requis!";
$_['error_zone_id']  = 'Province / État / Région est requis!';
$_['error_zip_code']  = 'Le code postal est requis!';
$_['error_country_id']  = 'Le pays est requis!';
$_['error_logo_type']  = "Le type de fichier du logo n'est pas valide!";
$_['error_logo']  = 'Le logo est requis!';



					/* 
					  Copyright (C) 2016-2017 Apptiko.
					  Created by Apptiko - http://extension.apptiko.com.
					  Time-stamp-code:Sat1117071151					  
					*/