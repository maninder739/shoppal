<?php

// Heading

$_['heading_title']                = 'Connexion au compte';


// Text

$_['text_account']                 = 'Compte';
$_['text_login']                   = 'Connexion';
$_['text_new_customer']            = 'Nouveau client';
$_['text_register']                = 'Créer un compte';
$_['text_register_account_customer']        = 'En créant un compte, vous pourrez faire des achats plus rapidement, être à jour sur l`état d`une commande, et garder une trace des commandes que vous avez faites précédemment.';
$_['text_returning_customer']      = 'Connexion Client';
$_['text_i_am_returning_customer'] = 'Je suis déjà client';
$_['text_forgotten']               = 'mot de passe oublié';
$_['create_account']					= 'Ouvrir un compte';

// Entry

$_['entry_email']                  = 'Adresse de courriel';
$_['entry_password']               = 'Mot de passe';


// Error

$_['error_login']                  = 'Attention: Aucune correspondance pour l`adresse e-mail et / ou le mot de passe.';
$_['error_attempts_warning']       = "Attention: Aucune correspondance pour l'adresse e-mail et / ou le mot de passe. Vous avez encore 1 tentative pour entrer une adresse e-mail valide et / ou un mot de passe.";
$_['error_attempts']               = 'Attention: votre compte a dépassé le nombre autorisé de tentatives de connexion. Veuillez réessayer dans 30 minutes.';
$_['error_approved']               = 'Attention: Votre compte nécessite une approbation avant de pouvoir vous connecter.';
$_['fundraise_member_login']       = 'Connexion Membres / Organismes / Charités';
$_['fundraise_speciality_login']   = 'Connexion Spécialiste Indépendant en Levées de Fonds.';
$_['new_to_shoppal']   = 'Nouveau à ShopPal?';



					/* 
					  Copyright (C) 2016-2017 Apptiko.
					  Created by Apptiko - http://extension.apptiko.com.
					  Time-stamp-code:Sat1117071145					  
					*/