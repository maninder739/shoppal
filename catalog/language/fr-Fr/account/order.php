<?php

// Heading

$_['heading_title']         = 'Historique des commandes';


// Text

$_['text_account']          = 'Compte';
$_['text_order']            = 'Informations sur la commande';
$_['text_order_edit']       = 'Modifier la commande';
$_['text_order_detail']     = 'détails de la commande';
$_['text_invoice_no']       = 'Facture No. :. ';
$_['text_order_id']         = 'Numéro de commande:';
$_['text_date_added']       = 'Date ajoutée:';
$_['text_shipping_address'] = 'Adresse de livraison';
$_['text_shipping_method']  = 'Méthode d`envois:';
$_['text_payment_address']  = 'adresse de facturation';
$_['text_payment_method']   = 'Mode de paiement:';
$_['text_comment']          = 'Commentaires';
$_['text_history']          = 'Historique des commandes';
$_['text_success']          = 'Succès: Vous avez ajouté <a href="%s"> %s </a> à votre <a href="%s"> chariot </a> !';
$_['text_empty']            = 'Vous n`avez fait aucune commande précédente!';
$_['text_error']            = 'La commande que vous avez demandée n`a pas pu être trouvée!';

$_['text_product']               = 'Ajouter Produit ( s )';
$_['entry_product']              = 'Choisir un produit';
$_['entry_quantity']             = 'Quantité';

$_['text_none']                     = '--- Aucun ---';
$_['text_select']                   = '--- Veuillez choisir ---';
$_['text_default']                  = ' <b> ( Défaut ) </b> ';  
$_['shop_for_a_Cause'] =    'Magasinez pour une cause';
$_['add_edit_product'] =    'Ajouter/modifier un produit';
$_['close'] 			=    'Fermer';
$_['save'] 				=    'sauvegarder';
$_['monthly_order'] =  'Commande mensuelle';

$_['discard_product_list'] 				=    'Supprimer la liste des produits';
$_['discard_product_msg'] 				=    "Êtes-vous sûr de vouloir annuler les changements?";
 

 

// Column


$_['column_order_id']       = 'numéro de commande';
$_['org_name']       		= "Nom de l'organisation:";
$_['order_type']            = 'Type de commande';
$_['column_customer']       = 'Client';
$_['column_product']        = 'No. des produits. ';
$_['column_name']           = 'Nom du produit';
$_['column_model']          = 'maquette';
$_['column_quantity']       = 'Quantité';
$_['column_price']          = 'Prix';
$_['column_total']          = 'Total';
$_['column_action']         = 'action';
$_['column_date_added']     = 'date ajoutée';
$_['column_status']         = 'Statut';
$_['column_comment']        = 'Commentaire';


$_['text_local_dog_shelter']  = 'Fait un don';
$_['text_on_behalf_of']       = 'De la part de ';
$_['text_remain_anonymous']   = 'anonymement';

// Error

$_['error_reorder']         = '%s est ne pas actuellement disponible à être réordonné.';




					/* 
					  Copyright (C) 2016-2017 Apptiko.
					  Created by Apptiko - http://extension.apptiko.com.
					  Time-stamp-code:Sat1117071145					  
					*/
