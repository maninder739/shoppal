<?php
$_['heading_title'] = 'Mettre à jour les informations de carte';

// Text
$_['text_account']       = 'Compte';
$_['text_edit']          = 'Modifier les informations de carte de crédit';
$_['text_your_details']  = 'Détails de la carte de crédit';
$_['text_success']       = 'Succès: votre compte a été mis à jour avec succès.';
$_['entry_telephone_format']  = '(xxx) xxx-xxxx';
$_['example_text'] = 'Entrez le numéro sans espacement ou tiret';
// Entry
$_['entry_cardNumber']    	  = 'Numéro de carte';
$_['entry_cardType']     	  = 'Type de carte';
$_['entry_cardExpiry']        = 'Expiration de la carte';
$_['entry_cardHolderName']    = 'Nom du titulaire';
 

// Error
$_['error_exists']        = 'Warning: E-Mail address is already registered!';
$_['error_cardHolderName']     = 'Card holder name must be between 1 and 32 characters!';
$_['error_cardNumber']      = 'Card Number is required';
$_['error_cardExpiry']         = 'Card Expiry is required';
$_['error_cardType']     = 'Card type is required';
$_['error_warning']  = 'Veuillez à nouveau modifier les détails de la carte.';
?>