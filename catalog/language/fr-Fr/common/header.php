<?php

// Text

$_['text_home']          = 'Accueil';
$_['text_wishlist']      = 'Liste d`envies (%s)';
$_['text_shopping_cart'] = 'Chariot';
$_['text_category']      = 'Catégories';
$_['text_account']       = 'Mon compte';
$_['text_register']      = 'S`inscrire';
$_['text_login']         = 'S`identifier';
$_['text_order']         = 'Historique des commandes';
$_['text_transaction']   = 'Transactions';
$_['text_download']      = 'Téléchargements';
$_['text_logout']        = 'Déconnexion';
$_['text_checkout']      = 'Check-out';
$_['text_search']        = 'Chercher';
$_['text_all']           = 'AFficher tout';
$_['text_mission']       = "La mission de ShopPal est de fournir des produits uniques qui offrent une valeur inégalée au consommateur en maintenant une source continue de financement pour les OSBL.";
$_['share_cart']         = 'Partagez votre panier via le lien';
$_['get_link']           = 'Obtenir le lien';




					/* 
					  Copyright (C) 2016-2017 Apptiko.
					  Created by Apptiko - http://extension.apptiko.com.
					  Time-stamp-code:Sat1117071210					  
					*/