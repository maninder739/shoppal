<?php
// Text
$_['text_how_it_works']       = 'Comment ça marche';
$_['text_how_it_works1']      = 'Faites un achat ponctuel, ou inscrivez-vous pour un achat mensuel.';
$_['text_how_it_works2']      = '10% de chaque commande vient appuyer';
$_['text_how_it_works3']      = 'Vos items seront livrés directement chez vous!';
$_['text_show_support']      = "C’est SIMPLE et FACILE de les appuyer!";
$_['text_show_support1']     = 'Vous recevez vos items, et une partie des fonds de chaque commande vient appuyer';
$_['text_want_to_support']     = "JE VEUX MAGASINEZ POUR APPUYER ";
$_['text_want_to_support_text'] = "ShopPal’s veut aider tous les organisms à but non lucratif et causes charitable à atteindre leur objectifs.  Lorsque vous magasinez avec nous, 10% de votre volume d’achat revient à l’organisme qui vous a référé.  Merci de votre soutien continue!  Lorsque vous magasinez, magasinez pour une cause!";
 $_['cancel_order_text'] = "* Vous pouvez annuler ou modifier votre commande n’importe quand sur le site web.";
