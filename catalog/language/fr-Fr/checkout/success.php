<?php

// Heading

$_['heading_title']        = 'Votre commande a bien été reçue!';


// Text

$_['text_basket']          = 'Chariot';
$_['text_checkout']        = 'Check-out';
$_['text_success']         = 'Succès';
$_['text_customer']        = ' <p> Votre commande a été traitée avec succès! </p><br><br><p><b>10&#37;</b> de votre commande ira direcement &agrave; la cause que vous avez choisie de soutenir!</p><br>  <p> Vous pouvez voir l`historique de vos commandes en allant à la page <a href="%s"> Mon compte </a> et en cliquant sur <a href="%s"> histoire </a> . </p>  <p> Si votre achat est associé à un téléchargement, vous pouvez accéder à la page de <a href="%s"> téléchargements </a> pour les voir. </p>  <p> Veuillez adresser toutes les questions que vous avez à <a href="%s"> l`équipe de soutien Shoppal </a> . </p>  <p> Merci de MAGASINER POUR UNE CAUSE! </p>';
$_['text_guest']           = ' <p> Votre commande a été traitée avec succès! </p>  <p> Veuillez adresser toutes les questions que vous avez à <a href="%s"> l`équipe de soutien Shoppal </a> . </p>  <p> de MAGASINER POUR UNE CAUSE! </p> ';




					/* 
					  Copyright (C) 2016-2017 Apptiko.
					  Created by Apptiko - http://extension.apptiko.com.
					  Time-stamp-code:Sat1117071209					  
					*/