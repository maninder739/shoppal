<?php
// Text
 
$_['text_new_subject']          = '% s - Commande% s
'; 
$_['text_header'] = "cher ";

$_['text_greeting'] = "Merci d'être un client de la commande mensuelle ShopPal et de soutenir une organisation chaque mois.
";

$_['text_message']  = "Nous avons tenté de facturer vos frais de commande mensuels à la carte de crédit figurant dans le dossier, mais nous n'avons pas pu le faire parce que l'émetteur de la carte a refusé la transaction.";

$_['text_message1']  = 'Pour éviter de manquer votre commande ce mois-ci, veuillez mettre à jour les informations de votre carte de crédit ou entrer une nouvelle carte de crédit.(<a href="https://spdev.shoppalweb.ca/index.php?route=account/creditcard">https://spdev.shoppalweb.ca/index.php?route=account/creditcard</a>)';

$_['text_alert']   = 'Veuillez noter que votre commande mensuelle expirera dans 30 jours si vous ne mettez pas à jour cette information et que vous pourriez être sujet à de nouveaux prix pour vos produits actuels sur la commande mensuelle.';

$_['text_footer']   = "Si vous avez des questions concernant votre compte, veuillez contacter le service clientèle de ShopPal.(hotlink to customerservice@shoppal.ca)";

$_['text_thanks'] = "Merci de magasiner avec nous!"; 

$_['team'] = "L'équipe ShopPal";