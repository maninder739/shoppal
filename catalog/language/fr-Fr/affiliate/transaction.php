<?php

// Heading

$_['heading_title']      = 'Vos transactions';


// Column

 

$_['column_date']  = 'Rendez-vous amoureux';
$_['column_orderid']  = 'numéro de commande';
$_['column_retail']  = 'Volume des ventes';
$_['column_total'] = 'Total';
$_['column_commission']      = 'Votre pourcentage'; 



// Text

$_['text_account']       = 'Compte';
$_['text_transaction']   = 'Vos transactions';
$_['text_balance']       = 'Votre solde actuel est:';
$_['text_empty']         = 'Vous n`avez aucune transaction!';




					/* 
					  Copyright (C) 2016-2017 Apptiko.
					  Created by Apptiko - http://extension.apptiko.com.
					  Time-stamp-code:Sat1117071159					  
					*/