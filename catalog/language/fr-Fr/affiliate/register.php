<?php

// Heading

$_['heading_title']             = 'Programme d`affiliation';


// Text

$_['text_account']              = 'Compte';
$_['text_register']             = 'Registre des affiliés';
$_['text_account_already']      = 'Si vous avez déjà un compte chez nous, connectez-vous au <a href="%s"> page de connexion </a> .';
$_['text_signup']               = 'Pour créer un compte affilié, remplissez le formulaire ci-dessous en vous assurant de remplir tous les champs obligatoires:';
$_['text_your_details']         = 'Vos informations personnelles';
$_['text_your_address']         = 'Vos coordonnées';
$_['text_payment']              = 'Informations de paiement';
$_['text_your_password']        = 'Votre mot de passe';
$_['text_cheque']               = 'Vérifier';
$_['text_paypal']               = 'Pay Pal';
$_['text_bank']                 = 'Virement';
$_['text_agree']                = 'j`ai lu et accepté les <a href="%s" class="agree">  <b> %s </b>  </a> 
';


// Entry
//$_['entry_organization_name'] = "nom de l'organisation";
$_['entry_firstname']           = 'Prénom';
$_['entry_lastname']            = 'Nom de famille';
$_['entry_email']               = 'Email';
$_['entry_telephone']           = 'Téléphone';
$_['entry_telephone_format']    = 'xxxxxxxxxx';
$_['entry_extension']             = 'Extension';
$_['entry_fax']                 = 'Fax';
$_['entry_company']             = 'Compagnie';
$_['entry_website']             = 'Site Internet';
$_['entry_address_1']           = 'Adresse 1';
$_['entry_address_2']           = 'Adresse 2';
$_['entry_postcode']            = 'Code postal';
$_['entry_city']                = 'Ville';
$_['entry_country']             = 'Pays';
$_['entry_zone']                = 'Région / État';
$_['entry_tax']                 = 'Numéro d`identification fiscale';
$_['entry_payment']             = 'Mode de paiement';
$_['entry_cheque']              = 'Vérifier le nom du bénéficiaire';
$_['entry_paypal']              = 'Compte de messagerie PayPal';
$_['entry_bank_name']           = 'Nom de banque';
$_['entry_bank_branch_number']  = 'ABA / BSB nombre ( Branche Nombre )';
$_['entry_bank_swift_code']     = 'Code rapide';
$_['entry_bank_account_name']   = 'Nom du compte';
$_['entry_bank_account_number'] = 'Numéro de compte';
$_['entry_password']            = 'Mot de passe';
$_['entry_confirm']             = 'Confirmer le mot de passe';
/* new fields */
$_['entry_custom_logo']         = 'Logo personnalisé';
$_['entry_mission']             = 'Votre mission en anglais';
$_['entry_mission_short'] 		= "Laissez cet espace vide si vous n'avez pas votre mission en anglais";
$_['entry_mission_max'] = 'Max 211 ​​caractères';
$_['entry_mission_french']      = 'Votre mission';
$_['entry_mission_french_short'] = 'Ceci est une courte mission qui apparaît en haut de votre page répliquée ShopPal.  Si votre mission est plus longue, veuillez svp utiliser la section À Propos de nous ci-bas.';

$_['entry_slide_info']          = 'Informations sur les diapos';
$_['entry_slide_background']    = 'Choisissez votre fond de diapositive';	


$_['entry_mission_txt_long']    = 'Votre longue mission';	
$_['entry_mission_long'] = 'Ceci est la déclaration longue.';

$_['entry_mission_french_txt_long']    = 'Votre longue mission française';	
$_['entry_mission_french_long'] = "Laissez cet espace vide si vous n'avez pas votre mission en anglais";

$_['entry_other_basic_information'] = "Informations de base sur l'organisation";
$_['entry_organization_name'] = "nom de l'organisation";
$_['entry_position_organization'] = "Position dans l'organisation";
$_['entry_mobile_number'] = 'Numéro de portable';
$_['entry_organization_phone_number'] = "Numéro de téléphone de l'organisation";
$_['entry_organization_phone_number_format'] = "xxxxxxxxxx";
$_['entry_street_address'] = 'Adresse de rue';
$_['entry_street_address2'] = 'Adresse de rue Ligne 2';
$_['entry_city'] = 'Ville';
$_['entry_zip_code'] = 'Code postal';
$_['entry_country'] = 'Pays';
$_['entry_region'] = 'Province / État / Région';
$_['entry_branded_website'] = 'Préférences de nom de site Web de marque ShopPal (Shoppal.ca/ ???)';
$_['entry_organization_details'] = "Détails de l'organisation";
$_['entry_organization_website'] = "Site Web d'organisation";
$_['entry_organization_hashtags'] = 'Organisation Hashtags';
$_['entry_organization_facebook_link'] = 'Organisation Facebook Lien';
$_['entry_organization_instagram'] = 'Organisation Instagram';
$_['entry_organization_twitter'] = 'Organisation Twitter';
$_['entry_organization_linkedIn_link'] = "Lien LinkedIn de l'organisation";
$_['entry_agreement_heading'] = "L'organisation autorise Shoppal à utiliser son logo et son nom, sans lien avec les montants de collecte de fonds, de la manière suivante";
$_['entry_agreement_checkbox1'] = "sur la page principale ou d'autres pages du site Web ShopPal qui affichent des informations sur les membres";
$_['entry_agreement_checkbox2'] = 'sur les médias sociaux et autres plateformes de marketing Internet, et / ou';
$_['entry_agreement_checkbox3'] = 'sur les médias imprimés et / ou les médias radio / télévision.';
$_['entry_logo'] = 'Logo';
$_['entry_organization_mission_statement'] = "Laissez vide si vous ne l'avez pas en anglais.";
$_['entry_organization_detailed'] = 'Organisation "Ce que nous faisons" / "Détaillé - À propos de nous"';
$_['entry_organization_detailed_french'] = 'Organisation "Ce que nous faisons" / "Détaillé - À propos de nous Français"';
$_['entry_hear_about_us'] = 'Comment avez-vous entendu parler de nous?';
$_['entry_type_question'] = 'Tapez une question';
$_['entry_shoppal_website'] = 'Site Web de ShopPal';
$_['entry_local_store'] = 'Magasin local';
$_['entry_local_event'] = 'Événement local';
$_['entry_local_flyer'] = 'Prospectus';
$_['entry_other_fundraising_organization'] = 'Autre organisation de collecte de fonds';
$_['entry_other_hear_about_us'] = "Nom de l'acheteur";
	
/* new fields */

// Error
$_['error_organization_name']  = "Le nom de l'organisation doit comporter entre 1 et 50 caractères!";
$_['error_exists']              = 'Attention: l`adresse e-mail est déjà enregistrée!';
$_['error_firstname']           = 'Le prénom doit avoir entre 1 et 32 ​​caractères!';
$_['error_lastname']            = 'Le nom de famille doit être compris entre 1 et 32 ​​caractères!';
$_['error_email']               = 'L`adresse e-mail ne semble pas être valide!';
$_['error_telephone']           = 'Le numéro de téléphone ne doit comporter que 10 chiffres';
$_['error_password']            = 'Le mot de passe doit comporter entre 4 et 20 caractères!';
$_['error_confirm']             = 'La confirmation du mot de passe ne correspond pas au mot de passe!';
$_['error_address_1']           = 'L`adresse 1 doit comporter entre 3 et 128 caractères!';
$_['error_city']                = 'La ville doit avoir entre 2 et 128 caractères!';
$_['error_country']             = 'S`il vous plaît sélectionner un pays!';
$_['error_zone']                = 'S`il vous plaît sélectionner une région / état!';
$_['error_postcode']            = 'Le code postal doit comporter entre 2 et 10 caractères!';
$_['error_agree']               = 'Attention: Toi doit se mettre d`accord à la %s!';

$_['error_organization_name']  = "Le nom de l'organisation doit comporter entre 1 et 50 caractères!";
$_['error_position_in_organization']  = "Doit être entre 1 et 32 ​​caractères!";
$_['error_organization_phone_number']  = "Le numéro de téléphone de l'organisation ne doit comporter que 10 chiffres";
$_['error_organization_mission_statement']  = "L'énoncé de mission est requis!";
// new fields
$_['cmp_required']              = "Le nom de l'entreprise doit comporter entre 1 et 100 ​​caractères!";
$_['error_company']             = 'Attention: la société est déjà enregistrée!';
$_['error_mission']             = 'Votre mission doit être inférieure ou égale à 217 caractères!';
$_['error_mission_french']      = 'Votre mission française devrait être inférieure ou égale à 217 caractères!';
$_['error_mission_both']        = "Vous devez entrer dans l'une des missions suivantes soit 'Votre Mission» soit 'Votre Mission Française'";

$_['error_organization_mission_both']        = "Vous devez entrer l'un des détails suivants soit 'Organisation' Ce que nous faisons '/' Détaillé - À propos de nous 'Ou' Organisation 'Ce que nous faisons' / 'Détaillé - À propos de nous Français'";
$_['error_logo1'] = 'Erreur de logo';
$_['logo_later'] = 'Je téléchargerai le logo plus tard';
$_['logo_now'] = 'Je vais joindre le logo maintenant';
$_['form_error'] = 'Erreur';
$_['look_below'] = "S'il vous plaît jeter un oeil ci-dessous pour les erreurs";
$_['close_modal'] = 'Fermer';
$_['other_details_text'] = 'Autres détails';
$_['other_details_input'] = 'autres détails';


					/* 
					  Copyright (C) 2016-2017 Apptiko.
					  Created by Apptiko - http://extension.apptiko.com.
					  Time-stamp-code:Sat1117071158					  
					*/