<?php

// Heading

$_['heading_title']     = 'Mon compte Informations';


// Text

$_['text_account']      = 'Compte';
$_['text_edit']         = 'Modifier les informations';
$_['text_your_details'] = 'Vos informations personnelles';
$_['text_your_address'] = 'Votre adresse';
$_['text_success']      = 'Succès: votre compte a été mis à jour avec succès.';


// Entry

$_['entry_firstname']   = 'Prénom';
$_['entry_lastname']    = 'Nom de famille';
$_['entry_email']       = 'Email';
$_['entry_telephone']   = 'Téléphone';
$_['entry_telephone_format']   = '(xxx) xxx-xxxx';
$_['entry_fax']         = 'Fax';
$_['entry_company']     = 'Compagnie';
$_['entry_website']     = 'Site Internet';
$_['entry_address_1']   = 'Adresse 1';
$_['entry_address_2']   = 'Adresse 2';
$_['entry_postcode']    = 'Code postal';
$_['entry_city']        = 'Ville';
$_['entry_country']     = 'Pays';
$_['entry_zone']        = 'Région / État';
/* new fields */
$_['entry_custom_logo']         = 'Logo personnalisé';
$_['entry_mission']             = 'Votre mission';
$_['entry_mission_french']      = 'Votre mission française';
$_['entry_slide_info']          = 'Informations sur les diapos';
$_['entry_slide_background']    = 'Choisissez votre fond de diapositive';	

// Error

$_['error_exists']      = 'Attention: l`adresse e-mail est déjà enregistrée!';
$_['error_firstname']   = 'Le prénom doit avoir entre 1 et 32 ​​caractères!';
$_['error_lastname']    = 'Le nom de famille doit être compris entre 1 et 32 ​​caractères!';
$_['error_email']       = 'L`adresse e-mail ne semble pas être valide!';
$_['error_telephone']   = 'Le format du téléphone est (xxx) xxx-xxxx';
$_['error_address_1']   = 'L`adresse 1 doit comporter entre 3 et 128 caractères!';
$_['error_city']        = 'La ville doit avoir entre 2 et 128 caractères!';
$_['error_country']     = 'S`il vous plaît sélectionner un pays!';
$_['error_zone']        = 'S`il vous plaît sélectionner une région / état!';
$_['error_postcode']    = 'Le code postal doit comporter entre 2 et 10 caractères!';
$_['error_modalheader']    = 'Erreur';
$_['error_modalfields']    = 'Vérifiez attentivement le formulaire et TOUS les onglets, car il y en a plusieurs';
$_['error_modalclose']    = 'Fermer';

/* new fields */
$_['error_mission']             = 'Votre mission doit être inférieure ou égale à 217 caractères!';
$_['error_mission_french']      = 'Votre mission française devrait être inférieure ou égale à 217 caractères!';


$_['entry_mission_txt_long']    = 'Votre longue mission';	
$_['entry_mission_long'] = 'Ceci est la déclaration longue.';

$_['entry_mission_french_txt_long']    = 'Votre longue mission française';	
$_['entry_mission_french_long'] = "Laissez cet espace vide si vous n'avez pas votre mission en anglais";

					/* 
					  Copyright (C) 2016-2017 Apptiko.
					  Created by Apptiko - http://extension.apptiko.com.
					  Time-stamp-code:Sat1117071154					  
					*/