<?php

// affiliate/account
$_['text_signup_link'] = 'Liens pour vos membres';
$_['text_aff_link_any_page'] = '';
$_['text_affiliate_downline'] = 'Vos membres';

// affiliate/transaction
$_['text_total_earnings']       = 'Total de commissions:';
$_['text_view_subs'] = 'Voir vos membres';
$_['column_level'] = 'Niveau';
$_['column_num_affs'] = 'Nombre de membres';

$_['text_order_id']           = 'No. commande:';
$_['text_recurring_commission'] = 'Commande #%s récurrente';

// mail
$_['text_approve_subject']      = '%s - Votre compte de consultant a été activé!';
$_['text_approve_welcome']      = 'Bienvenu et merci de vous êtes inscrit à %s!';
$_['text_approve_login']        = 'Votre compte a été activé et vous pouvez maintenant vous connecter à:';
$_['text_approve_services']     = 'Vous avez accès à vos informations une fois connecté.';
$_['text_approve_thanks']       = 'Merci,';
$_['text_transaction_subject']  = '%s - Commission';
$_['text_transaction_received'] = 'Vous avez reçu %s commission!';
$_['text_transaction_total']    = 'Votre commission totale est maintenant %s.';
$_['text_transaction_level']    = ' Niveau %d';