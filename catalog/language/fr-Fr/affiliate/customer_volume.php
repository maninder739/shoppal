<?php
// Heading
$_['heading_title']    = "Volume d'achats";

// Text
$_['text_account']     = 'Compte';
$_['show_empty_text']  = "Aucun volume à afficher";
$_['serialNumber']     = 'S.No';
$_['firstname']        = 'Prénom';
$_['lastname']         = 'Nom De Famille';
$_['order_status']         = 'Statut de la commande';
$_['view_all']		= 'AFFICHER TOUT';
$_['current_month']		= 'Mois en cours';
$_['previous_month']		= 'Mois précédent';
$_['next_month']		= 'Mois suivant';
$_['shopping_volume']		= 'VOLUME DE MAGASINAGE';
$_['cust_order_date']		= 'DATE COMMANDE';
$_['cust_name']				= 'CLIENT';
$_['cust_retail']		= 'MAGASINAGE AU DÉTAIL';
$_['cust_percentage']		= 'VOTRE POURCENTAGE';
$_['table_total']		= 'Total';
$_['table_total_cust']		= ' Client';
$_['table_total_custs']		= ' Clients';
$_['table_total_none']		= 'Aucun volume à afficher';