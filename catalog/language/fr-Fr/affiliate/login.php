<?php

// Heading

$_['heading_title']                 = 'Section des membres';


// Text

$_['text_account']                  = 'Compte';
$_['text_login']                    = 'S`identifier';
$_['text_description']              = '</p> 
';
$_['text_new_affiliate']            = 'Nouvelle affiliation';
$_['text_register_account']         = ' <p> Je ne suis pas actuellement affilié. </p>  <p> Cliquez sur Continuer ci-dessous pour créer un nouveau compte affilié. S`il vous plaît noter que ce n`est pas connecté en aucune façon à votre compte client. </p> 
';
$_['text_returning_affiliate']      = 'Connexion des membres';
$_['text_i_am_returning_affiliate'] = 'Je suis déjà membre';
$_['text_forgotten']                = 'mot de passe oublié';


// Entry

$_['entry_email_affiliate']         = 'Courriel';
$_['entry_password']                = 'Mot de passe';


// Error

$_['error_login']                   = 'Attention: Aucune correspondance pour l`adresse de courriel et / ou le mot de passe.';
$_['error_attempts_warning']       = "Attention: Aucune correspondance pour l'adresse e-mail et / ou le mot de passe. Vous avez encore 1 tentative pour entrer une adresse e-mail valide et / ou un mot de passe.";
$_['error_attempts']               = 'Attention: votre compte a dépassé le nombre autorisé de tentatives de connexion. Veuillez réessayer dans 30 minutes.';
$_['error_approved']                = 'Attention: Votre compte nécessite une approbation avant de pouvoir vous connecter.';




					/* 
					  Copyright (C) 2016-2017 Apptiko.
					  Created by Apptiko - http://extension.apptiko.com.
					  Time-stamp-code:Sat1117071156					  
					*/