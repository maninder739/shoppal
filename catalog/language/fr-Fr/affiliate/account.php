<?php

// Heading

$_['heading_title']        = 'Mon compte d`affilié';


// Text

$_['text_account']         = 'Compte';
$_['text_my_account']      = 'Mon compte d`affilié';
$_['text_my_tracking']     = 'Mes informations de suivi';
$_['text_my_transactions'] = 'Mes transactions';
$_['text_edit']            = 'Modifier les informations de votre compte';
$_['text_password']        = 'changez votre mot de passe';
$_['text_payment']         = 'Modifier vos préférences de paiement';
$_['text_tracking']        = 'Code de suivi des affiliés personnalisés';
$_['text_transaction']     = 'Voir l`historique de vos transactions';
$_['button_text_logout']   = 'Deconnexion';
$_['text_top_logout']      = 'Fermer votre connexion de membre';

$_['text_customer_volume'] = 'Volume client';
$_['customer_volume']      = 'Le volume';


					/* 
					  Copyright (C) 2016-2017 Apptiko.
					  Created by Apptiko - http://extension.apptiko.com.
					  Time-stamp-code:Sat1117071154					  
					*/