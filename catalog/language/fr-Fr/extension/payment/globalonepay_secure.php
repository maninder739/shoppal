<?php
// Text
$_['text_title'] = 'GlobalOnePay Secure. <a onclick="window.open(\'http://www.globalonepay.com/developers/payments-integration-overview\');"><img src="admin/view/image/payment/plugin-logo.png" alt="GlobalOnePay" title="GlobalOnePay" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_loading'] = 'Loading';
$_['ext_link'] = '<a onclick="window.open(\'http://www.globalonepay.com/developers/payments-integration-overview\');"><img src="admin/view/image/payment/plugin-logo.png" alt="GlobalOnePay" title="GlobalOnePay" style="border: 1px solid #EEEEEE;" /></a>';

$_['response_error'] = "UNE ERREUR EST SURVENUE! Votre transaction n'a pu être complétée. Détails:";

$_['invalid_hash_error'] = "PAIEMENT ÉCHOUÉ: INVASI REPONSE HASH. S'il vous plaît contacter pour clarifier si vous serez facturé pour cette commande";	

$_['payment_error'] = 'Votre paiement a été REFUSÉ! Veuillez vérifier toutes les informations ci-dessous, car il pourrait y avoir un problème avec votre adresse de facturation autant que les détails de la carte elle-même. Merci!';

$_['securecard_reg_error'] = "INSCRIPTION SÉCURE DE VOTRE CARTE A ÉCHOUÉ - Votre livraison mensuelle n'a pas été mis en place.";

$_['unable_to_process'] = "Impossible de traiter le paiement";

$_['expiry_date_error'] = "La date d'expiration devrait être une date future";

$_['expiry_month_error'] = "Veuillez sélectionner le mois d'expiration";

$_['invalid_credentials'] = "Veuillez ajouter les détails de votre carte";

