<?php
// Heading
$_['coupon_title'] = 'Code Promotionnel';

// Text
$_['text_couponsuccess']  = 'Félicitations: Votre promotion a été appliqué avec succès!';

// Entry
$_['text_coupon_entry']  = "Si vous avez un code, veuillez l'inscrire ici";
$_['text_coupon_apply']	 = 'Appliquer';

// Error
$_['error_coupon_expired']  = 'Attention: Votre code est invalide, expiré ou a atteint son usage maximal!';
$_['error_empty_coupon']   = 'Attention: Veuillez inscrire votre code promotionnel!';