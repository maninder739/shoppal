<?php
// Heading
$_['heading_title'] = 'PAGE NOT FOUND';
$_['heading_title2'] = 'We must have already donated this page, because we cannot seem to find it anywhere!';

// Text
$_['text_error']    = 'We are sorry that we couldn not find the page you were looking for.  Here are some useful links to help you find your way:';
$_['link1']    = 'Shop for a cause!';
$_['link2']    = 'Start Fundraising';