<?php
// Heading
$_['heading_title']        = 'Your order has been placed!';

// Text
$_['text_basket']          = 'Shopping Cart';
$_['text_checkout']        = 'Checkout';
$_['text_success']         = 'Success';
$_['text_customer']        = '<p>Your order has been successfully processed!</p><br><br><p>Remember that <b>10&#37;</b> of your order will go to the cause you are supporting!</p><br><p>You can view your order history by going to the <a href="%s">my account</a> page and by clicking on <a href="%s">history</a>.</p><p>If your purchase has an associated download, you can go to the account <a href="%s">downloads</a> page to view them.</p><br><p>Please direct any questions you have to the <a href="%s">Shoppal Support Team</a>.</p><p>Thank you for <b>SHOPPING FOR A CAUSE!</b></p>';
$_['text_guest']           = '<p>Your order has been successfully processed!</p><p>Please direct any questions you have to the <a href="%s">Shoppal Support Team</a>.</p><p>Thank you for <b>SHOPPING FOR A CAUSE!</b></p>';