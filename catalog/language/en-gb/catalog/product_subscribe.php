<?php

// Heading
$_['heading_title']					= 'Product Subscriptions';
$_['heading_title_change']			= 'Change Your Subscription Plan';

// Text
$_['text_subject']					= 'Your subscription for %s, Order #%s';
$_['text_final_subject']			= 'Final notice for final subscription for %s, Order #%s';
$_['text_message']					= 'Your product is getting ready to expire.  You can renew this product on your order history page by logging in to your account, clicking on Order History, and then click on Renew Subscription.';
$_['text_final_message']			= 'This is your final reminder that your product, %s, is expiring in %s days.  You can renew this product on your order history page by logging in to your account, clicking on Order History, and then clicking on Renew Subscription.  If you do not renew your subscription within %s days, your subscription will be cancelled.';
$_['text_customer_link']			= 'You can view your subscription by visiting your account <a href="%s">order history</a> page.';
$_['text_day']						= 'day';
$_['text_week']						= 'week';
$_['text_semi_month']				= 'semi_month';
$_['text_month']					= 'month';
$_['text_year']						= 'year';

// Column
$_['column_name']					= 'Product Name';
$_['column_model']					= 'Model';
$_['column_price']					= 'Price';
$_['column_description']			= 'Subscription Terms';
$_['column_effective_date']			= 'Effective Date';

// Button
$_['button_checkout']				= 'Checkout';

?>