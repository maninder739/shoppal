<?php
// Heading
$_['coupon_title'] = 'Coupon Code';

// Text
$_['text_couponsuccess']  = 'Success: Your coupon discount has been applied!';

// Entry
$_['text_coupon_entry']  = 'If you have one, enter your coupon code here';
$_['text_coupon_apply']	 = 'Apply';
$_['text_warning_msg']	 = 'You must agree to the Terms and Conditions before the coupon can be applied';

// Error
$_['error_coupon_expired']  = 'Warning: Coupon is either invalid, expired or reached its usage limit!';
$_['error_empty_coupon']   = 'Warning: Please enter a coupon code!';