<?php
// Text
$_['text_title'] = 'GlobalOnePay Secure. <a onclick="window.open(\'http://www.globalonepay.com/developers/payments-integration-overview\');"><img src="admin/view/image/payment/plugin-logo.png" alt="GlobalOnePay" title="GlobalOnePay" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_loading'] = 'Loading';
$_['ext_link'] = '<a onclick="window.open(\'http://www.globalonepay.com/developers/payments-integration-overview\');"><img src="admin/view/image/payment/plugin-logo.png" alt="GlobalOnePay" title="GlobalOnePay" style="border: 1px solid #EEEEEE;" /></a>';

$_['response_error'] = 'AN ERROR OCCURED! You transaction was not processed. Error details:';

$_['payment_decline_error'] = 'Your payment was DECLINED! Please validate all your information below, as this can be a problem with your billing address, or the card details, especially the CVV. Thank you!';

$_['securecard_reg_error'] = "SECURECARD REGISTRATION FAILED - Your monthly autoship was not set up properly.";

$_['invalid_hash_error'] = 'PAYMENT FAILED: INVALID RESPONSE HASH. Please contact to clarify if you will get charged for this order';	

$_['unable_to_process'] = "Unable to process payment";

$_['expiry_date_error'] = "Expiry date should be a future date";

$_['expiry_month_error'] = "Please select expiry month";

$_['invalid_credentials'] = "Please add your card details";


