<?php
// Text
$_['text_title']          = 'Secure Payments via GlobalOnePay. <a onclick="window.open(\'http://www.globalonepay.com/developers/payments-integration-overview\');"><img src="admin/view/image/payment/plugin-logo.png" alt="GlobalOnePay" title="GlobalOnePay" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_testmode']	  = 'TEST ACCOUNT in use!<br />The GlobalOnePay payment plugin is using a <b>Test Account</b>. Your card will not be charged.';
$_['text_failure']        = '<b>Your payment failed validation!</b><br />Please contact us <u>immediately</u> to ensure that you do not get charged.<br />Quote order ID: %s';
$_['text_decline']        = '<b>Your payment was not authorised!</b><br />Message from the bank: %s';
$_['text_failure_wait'] = 'Please wait while we redirect you back to your cart';
?>
