<?php
// Heading
$_['heading_title']     = 'My Account Information';

// Text
$_['text_account']      = 'Account';
$_['text_edit']         = 'Edit Information';
$_['text_your_details'] = 'Your Personal Details';
$_['text_your_address'] = 'Your Address';
$_['text_success']      = 'Success: Your account has been successfully updated.';

// Entry
$_['entry_firstname']   = 'First Name';
$_['entry_lastname']    = 'Last Name';
$_['entry_email']       = 'E-Mail';
$_['entry_telephone']   = 'Telephone';
$_['entry_telephone_format']   = '(xxx) xxx-xxxx';
$_['entry_fax']         = 'Fax';
$_['entry_company']     = 'Company';
$_['entry_website']     = 'Web Site';
$_['entry_address_1']   = 'Address 1';
$_['entry_address_2']   = 'Address 2';
$_['entry_postcode']    = 'Post Code';
$_['entry_city']        = 'City';
$_['entry_country']     = 'Country';
$_['entry_zone']        = 'Region / State';
/* new fields */
$_['entry_custom_logo']         = 'Custom Logo';
$_['entry_mission']             = 'Your Mission';
$_['entry_mission_french']      = 'Your Mission French';
$_['entry_slide_info']          = 'Slide Information';
$_['entry_slide_background']    = 'Choose your slide background';	

$_['entry_mission_txt_long']    = 'Your Long Mission';	
$_['entry_mission_long'] = 'This is the Long statement.';

$_['entry_mission_french_txt_long']    = 'Your Long Mission French';	
$_['entry_mission_french_long'] = 'Leave blank if you do not have the French version';
	
/* new fields */

// Error
$_['error_exists']      = 'Warning: E-Mail address is already registered!';
$_['error_firstname']   = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']    = 'Last Name must be between 1 and 32 characters!';
$_['error_email']       = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']   = 'Telephone format is (xxx) xxx-xxxx';
$_['error_address_1']   = 'Address 1 must be between 3 and 128 characters!';
$_['error_city']        = 'City must be between 2 and 128 characters!';
$_['error_country']     = 'Please select a country!';
$_['error_zone']        = 'Please select a region / state!';
$_['error_postcode']    = 'Postcode must be between 2 and 10 characters!';
$_['error_modalheader']    = 'Error';
$_['error_modalfields']    = 'Check form carefully and ALL tabs, as there are more than one';
$_['error_modalclose']    = 'Close';
/* new fields */
$_['error_mission']             = 'Your Mission should be less than or equal to 217 characters!';
$_['error_mission_french']      = 'Your Mission French should be less than or equal to 217 characters!';