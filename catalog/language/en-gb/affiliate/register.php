<?php
// Heading
$_['heading_title']             = 'Member Program';

// Text
$_['text_account']              = 'Account';
$_['text_register']             = 'Member Register';
$_['text_account_already']      = 'If you already have an account with us, please login at the <a href="%s">login page</a>.';
$_['text_signup']               = 'To create an member account, fill in the form below ensuring you complete all the required fields:';
$_['text_your_details']         = 'Your Personal Details';
$_['text_your_address']         = 'Your Address Details';
$_['text_payment']              = 'Payment Information';
$_['text_your_password']        = 'Your Password';
$_['text_cheque']               = 'Cheque';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Bank Transfer';
$_['text_agree']                = 'I have read and agree to the <a href="%s" class="agree"><b>%s</b></a>';

// Entry

 
$_['entry_firstname']           = 'First Name';
$_['entry_lastname']            = 'Last Name';
$_['entry_email']               = 'E-Mail';
$_['entry_telephone']           = 'Telephone';
$_['entry_telephone_format']    = 'xxxxxxxxxx';
$_['entry_extension']             = 'Extension';
$_['entry_fax']                 = 'Fax';
$_['entry_company']             = 'Organization Name';
$_['entry_website']             = 'Web Site';
$_['entry_address_1']           = 'Address 1';
$_['entry_address_2']           = 'Address 2';
$_['entry_postcode']            = 'Postal Code';
$_['entry_city']                = 'City';
$_['entry_country']             = 'Country';
$_['entry_zone']                = 'Region / State';
$_['entry_tax']                 = 'Tax ID';
$_['entry_payment']             = 'Payment Method';
$_['entry_cheque']              = 'Cheque Payee Name';
$_['entry_paypal']              = 'PayPal Email Account';
$_['entry_bank_name']           = 'Bank Name';
$_['entry_bank_branch_number']  = 'ABA/BSB number (Branch Number)';
$_['entry_bank_swift_code']     = 'SWIFT Code';
$_['entry_bank_account_name']   = 'Account Name';
$_['entry_bank_account_number'] = 'Account Number';
$_['entry_password']            = 'Password';
$_['entry_confirm']             = 'Password Confirm';
/* new fields */
$_['entry_custom_logo']         = 'Custom Logo';
$_['entry_mission']             = 'Your Mission';
$_['entry_mission_short'] = 'This is the short statement that goes beside your logo at the top of the your ShopPal replicated site.  If your mission is longer, please use the About Us section to enter all your details.';
$_['entry_mission_max'] = 'Max 211 characters';
$_['entry_mission_french']      = 'Your Mission French';
$_['entry_mission_french_short'] = 'Leave blank if you do not have the French version';
$_['entry_slide_info']          = 'Slide Information';
$_['entry_slide_background']    = 'Choose your slide background';


$_['entry_mission_txt_long']    = 'Your Long Mission';	
$_['entry_mission_long'] = 'This is the Long statement.';

$_['entry_mission_french_txt_long']    = 'Your Long Mission French';	
$_['entry_mission_french_long'] = 'Leave blank if you do not have the French version';

$_['entry_other_basic_information'] = 'Organization Basic Information';
$_['entry_organization_name'] = 'Organization Name';
$_['entry_position_organization'] = 'Position in Organization';
$_['entry_mobile_number'] = 'Mobile Number';
$_['entry_organization_phone_number'] = 'Organization Phone Number';
$_['entry_organization_phone_number_format'] = 'xxxxxxxxxx';
$_['entry_street_address'] = 'Street Address';
$_['entry_street_address2'] = 'Street Address Line 2';
$_['entry_city'] = 'City';
$_['entry_zip_code'] = 'Postal / Zip Code';
$_['entry_country'] = 'Country';
$_['entry_region'] = 'Province/State/Region';
$_['entry_branded_website'] = 'ShopPal Branded Website Name Preferences ( Shoppal.ca/ ??? )';
$_['entry_organization_details'] = 'Organization Details';
$_['entry_organization_website'] = 'Organization Website';
$_['entry_organization_hashtags'] = 'Organization Hashtags';
$_['entry_organization_facebook_link'] = 'Organization Facebook Link';
$_['entry_organization_instagram'] = 'Organization Instagram';
$_['entry_organization_twitter'] = 'Organization Twitter';
$_['entry_organization_linkedIn_link'] = 'Organization LinkedIn Link';
$_['entry_agreement_heading'] = 'The Organization permits Shoppal to use their Logo and Name, not associated with any fundraising amounts, in the following manner';
$_['entry_agreement_checkbox1'] = 'on the main, or other pages of the ShopPal website that display Member information';
$_['entry_agreement_checkbox2'] = 'on Social Media and other Internet Marketing platforms, and/or';
$_['entry_agreement_checkbox3'] = 'on Print Media and or Radio/Television Media.';
$_['entry_logo'] = 'Logo';
$_['entry_organization_mission_statement'] = 'Organization Mission Statement';
$_['entry_organization_detailed'] = 'Organization "What we do" / "Detailed - About Us"';
$_['entry_organization_detailed_french'] = 'Organization "What we do" / "Detailed - About Us French"';
$_['entry_hear_about_us'] = 'How did you hear about us?';
$_['entry_type_question'] = 'Type a question';
$_['entry_shoppal_website'] = 'ShopPal Website';
$_['entry_local_store'] = 'Local Store';
$_['entry_local_event'] = 'Local Event';
$_['entry_local_flyer'] = 'Flyer';
$_['entry_other_fundraising_organization'] = 'Other Fundraising Organization';
$_['entry_other_hear_about_us'] = 'Shoppal Rep. Name';


/* new fields */


// Error
$_['error_organization_name']  = 'Organization Name must be between 1 and 50 characters!';
$_['error_exists']              = 'Warning: E-Mail Address is already registered!';
$_['error_firstname']           = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']            = 'Last Name must be between 1 and 32 characters!';
$_['error_email']               = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']           = 'Phone number must only have 10 digits';
$_['error_password']            = 'Password must be between 4 and 20 characters!';
$_['error_confirm']             = 'Password confirmation does not match password!';
$_['error_address_1']           = 'Address 1 must be between 3 and 128 characters!';
$_['error_city']                = 'City must be between 2 and 128 characters!';
$_['error_country']             = 'Please select a country!';
$_['error_zone']                = 'Please select a region / state!';
$_['error_postcode']            = 'Postcode must be between 2 and 10 characters!';
$_['error_agree']               = 'Warning: You must agree to the %s!';

$_['error_position_in_organization']  = 'Must be between 1 and 32 characters!';
$_['error_organization_phone_number']  = 'Organization phone number must only have 10 digits';
$_['error_organization_mission_statement']  = 'Mission Statement is required!';

// new fields
$_['cmp_required']              = 'Company Name must be between 1 and 100 characters!';
$_['error_company']             = 'Warning: Company is already registered!';
$_['error_mission']             = 'Your Mission should be less than or equal to 217 characters!';
$_['error_mission_french']      = 'Your Mission French should be less than or equal to 217 characters!';
$_['error_mission_both']        = "Your have to enter one of the following missions either 'Your Mission' Or 'Your Mission French'";
$_['error_organization_mission_both']        = "Your have to enter one of the following detail either 'Organization 'What we do' / 'Detailed - About Us' Or 'Organization 'What we do' / 'Detailed - About Us French'";
$_['error_logo1'] = 'Logo Error';
$_['logo_later'] = 'I will upload logo later';
$_['logo_now'] = 'I will attach logo now';
$_['form_error'] = 'Error';
$_['look_below'] = 'Please take a look below for the errors';
$_['close_modal'] = 'Close';
$_['other_details_text'] = 'Other Details';
$_['other_details_input'] = 'other details';
