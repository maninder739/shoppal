<?php
// Heading
$_['heading_title']      = 'Your Transactions';

// Column
$_['column_date']  = 'Date';
$_['column_orderid']  = 'Order ID';
$_['column_retail']  = 'Sales Volume';
$_['column_total'] = 'Total';
$_['column_commission']      = 'Your Percentage';

// Text
$_['text_account']       = 'Account';
$_['text_transaction']   = 'Your Transactions';
$_['text_balance']       = 'Your current balance is:';
$_['text_empty']         = 'You do not have any transactions!';