<?php
// Heading
$_['heading_title']        = 'My Independent Consultant Account';

// Text
$_['text_account']         = 'Account';
$_['text_my_account']      = 'My Independent Consultant Account';
$_['text_my_tracking']     = 'My Tracking Information';
$_['text_my_transactions'] = 'My Transactions';
$_['text_edit']            = 'Edit your account information';
$_['text_password']        = 'Change your password';
$_['text_payment']         = 'Change your payment preferences';
$_['text_tracking']        = 'Custom Tracking Code';
$_['text_transaction']     = 'View your transaction history';
$_['button_text_logout']   = 'Logout';
$_['text_top_logout']      = 'Exit your Account';

$_['text_customer_volume'] = 'Customer Volume';
$_['customer_volume']      = 'Volume';