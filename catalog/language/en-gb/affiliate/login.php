<?php
// Heading
$_['heading_title']                 = 'Shoppal Member Program';

// Text
$_['text_account']                  = 'Account';
$_['text_login']                    = 'Login';
$_['text_description']              = '';
$_['text_new_affiliate']            = 'New Member';
$_['text_register_account']         = '<p>I am not currently a member.</p><p>Click Continue below to create a new affiliate account. Please note that this is not connected in any way to your customer account.</p>';
$_['text_returning_affiliate']      = 'Fundraising Organization Member / Independent Fundraising Specialist';
$_['text_i_am_returning_affiliate'] = 'I am an existing member';
$_['text_forgotten']                = 'Forgotten Password';

// Entry
$_['entry_email_affiliate']         = 'Member E-Mail Address';
$_['entry_password']                = 'Password';

// Error
$_['error_login']                   = 'Warning: No match for E-Mail Address and/or Password.';
$_['error_attempts_warning']       = 'Warning: No match for E-Mail Address and/or Password. You have 1 attempt left to enter valid E-Mail Address and/or Password.';
$_['error_attempts']               = 'Warning: Your account has exceeded allowed number of login attempts. Please try again in 30 minutes.';
$_['error_approved']                = 'Warning: Your account requires approval before you can login.';