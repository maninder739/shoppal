<?php

// affiliate/account
$_['text_signup_link'] = 'Link to refer members and non profits.';
$_['text_aff_link_any_page'] = '';
$_['text_affiliate_downline'] = 'Your Affiliate Downline';

// affiliate/transaction
$_['text_total_earnings']       = 'Total earnings:';
$_['text_view_subs'] = 'View members signed up under you';
$_['column_level'] = 'Level';
$_['column_num_affs'] = 'Number of Members';

$_['text_order_id']           = 'Order ID:';
$_['text_recurring_commission'] = 'Order #%s recurring';

// mail
$_['text_approve_subject']      = '%s - Your Independent Fundraiser account has been activated!';
$_['text_approve_welcome']      = 'Welcome and thank you for registering at %s!';
$_['text_approve_login']        = 'Your account has now been created and you can log in by using your email address and password by visiting our website or at the following URL:';
$_['text_approve_services']     = 'Upon logging in, you will have access to your account information.';
$_['text_approve_thanks']       = 'Thanks,';
$_['text_transaction_subject']  = '%s - Commission';
$_['text_transaction_received'] = 'You have received %s commission!';
$_['text_transaction_total']    = 'Your total amount of commission is now %s.';
$_['text_transaction_level']    = ' Level %d';