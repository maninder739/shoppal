<?php
// Heading
$_['heading_title'] = 'Your Member Account Has Been Created!';

// Text
$_['text_message']  = '<p>Congratulations! Your member account has been successfully created!</p> <p>You are now a fundraising member of %s.</p> <p>If you have ANY questions about the operation of your member account, please send us an email.</p> <p>A confirmation has been sent to the provided e-mail address. If you have not received it within the hour, please <a href="%s">contact the ShopPal Support Team</a>.</p>';
$_['text_approval'] = '<p>Thank you for registering for a member account with %s!</p><p>You will be notified by e-mail once your account has been activated by the ShopPal Team.</p><p>If you have ANY questions about the operation of your member account, please <a href="%s">contact the ShopPal Support Team</a>.</p>';
$_['text_account']  = 'Account';
$_['text_success']  = 'Success';
