<?php
// Heading
$_['heading_title']  = 'Contact Us';
$_['contact_header_sp'] = '<br><h2>Do you have a question?</h2><h2>Interested in finding out more about how we can help your organization?</h2> <h2>You just want to give us some feedback?</h2><br><p>Fill out the form below and we will respond rapidly if your request requires an answer.</p><br><br>';

// Text
$_['text_location']  = 'Our Location';
$_['text_store']     = 'Our Stores';
$_['text_contact']   = 'Contact Form';
$_['text_address']   = 'Address';
$_['text_telephone'] = 'Telephone';
$_['text_fax']       = 'Fax';
$_['text_open']      = 'Opening Times';
$_['text_comment']   = 'Comments';
$_['text_success']   = '<p>Your enquiry has been successfully sent to the store owner!</p>';

// Entry
$_['entry_name']     = 'Your Name';
$_['entry_email']    = 'E-Mail Address';
$_['entry_enquiry']  = 'Enquiry';

// Email
$_['email_subject']  = 'Enquiry %s';

// Errors
$_['error_name']     = 'Name must be between 3 and 32 characters!';
$_['error_email']    = 'E-Mail Address does not appear to be valid!';
$_['error_enquiry']  = 'Enquiry must be between 10 and 3000 characters!';
