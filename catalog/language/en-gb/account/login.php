<?php
// Heading
$_['heading_title']                = 'Account Login';

// Text
$_['text_account']                 = 'Account';
$_['text_login']                   = 'Login';
$_['text_new_customer']            = 'New Customer';
$_['text_register']                = 'Register Account';
$_['text_register_account_customer']        = 'By creating an account you will be able to shop faster, be up to date on an order\'s status, and keep track of the orders you have previously made.';
$_['text_returning_customer']      = 'Customer Sign in';
$_['text_i_am_returning_customer'] = 'I am a returning customer';
$_['text_forgotten']               = 'Forgotten Password';
$_['create_account']				= 'Create an Account';

// Entry
$_['entry_email']                  = 'E-Mail Address';
//$_['entry_email_affiliate']        = 'Affiliate E-Mail Address'; 
$_['entry_password']               = 'Password';

// Error
$_['error_login']                  = 'Warning: No match for E-Mail Address and/or Password.';
$_['error_attempts_warning']       = 'Warning: No match for E-Mail Address and/or Password. You have 1 attempt left to enter valid E-Mail Address and/or Password.';
$_['error_attempts']               = 'Warning: Your account has exceeded allowed number of login attempts. Please try again in 30 minutes.';
$_['error_approved']               = 'Warning: Your account requires approval before you can login.';
$_['fundraise_member_login']       = 'Fundraising Organization Member Login';
$_['fundraise_speciality_login']   = 'Independant Fundraising Specialist Login';
$_['new_to_shoppal']   = 'New To ShopPal?';