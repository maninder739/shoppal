<?php
// Heading
$_['heading_title']         = 'Order History';

// Text
$_['text_account']          = 'Account';
$_['text_order']            = 'Order Information';
$_['text_order_edit']       = 'Order Edit';
$_['text_order_detail']     = 'Order Details';
$_['text_invoice_no']       = 'Invoice No.:';
$_['text_order_id']         = 'Order ID:';
$_['text_date_added']       = 'Date Added:';
$_['text_shipping_address'] = 'Shipping Address';
$_['text_shipping_method']  = 'Shipping Method:';
$_['text_payment_address']  = 'Payment Address';
$_['text_payment_method']   = 'Payment Method:';
$_['text_comment']          = 'Order Comments';
$_['text_history']          = 'Order History';
$_['text_success']          = 'Success: You have added <a href="%s">%s</a> to your <a href="%s">shopping cart</a>!';
$_['text_empty']            = 'You have not made any previous orders!';
$_['text_error']            = 'The order you requested could not be found!';
$_['text_product']               = 'Add Product(s)';
$_['entry_product']              = 'Choose Product';
$_['entry_quantity']             = 'Quantity';
$_['stop_autoship'] = 'Stop Autoship';

$_['text_none']                     = ' --- None --- ';
$_['text_select']                   = ' --- Please Select --- ';
$_['text_default']                  = ' <b>(Default)</b>';
$_['shop_for_a_Cause']              =    'Shop for a Cause';
$_['add_edit_product']              =    'Add/Edit Product';
$_['close'] 			            =    'Close';
$_['save'] 				            =    'Save';
$_['discard_product_list'] 				=    'Discard Product List';
$_['discard_product_msg'] 				=    "Are you sure, you want to discard changes?";
$_['monthly_order'] =  'Monthly Order';

// Column
$_['column_order_id']       = 'Order ID';
$_['org_name']       		= 'Organization Name:';
$_['order_type']            = 'Order Type';
$_['column_customer']       = 'Customer';
$_['column_product']        = 'No. of Products';
$_['column_name']           = 'Product Name';
$_['column_model']          = 'Model';
$_['column_quantity']       = 'Quantity';
$_['column_price']          = 'Price';
$_['column_total']          = 'Total';
$_['column_action']         = 'Action';
$_['column_date_added']     = 'Date Added';
$_['column_status']         = 'Status';
$_['column_comment']        = 'Comment';

$_['text_local_dog_shelter']  = 'Donated';
$_['text_on_behalf_of']       = 'On behalf of ';
$_['text_remain_anonymous']   = 'anonymously';


// Error
$_['error_reorder']         = '%s is not currently available to be reordered.';
