<?php
// Heading
$_['heading_title']        = 'Register Account';

// Text
$_['text_account']         = 'Account';
$_['text_register']        = 'Register';
$_['text_account_already'] = 'If you already have an account with us, please login at the <a href="%s">login page</a>.';
$_['text_your_details']    = 'Your Personal Details';
$_['text_your_address']    = 'Your Address';
$_['text_newsletter']      = 'Newsletter';
$_['text_your_password']   = 'Your Password';
$_['text_agree']           = 'I have read and agree to the <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_customer_group'] = 'Customer Group';
$_['entry_firstname']      = 'First Name';
$_['entry_lastname']       = 'Last Name';
$_['entry_email']          = 'E-Mail';
$_['entry_telephone']      = 'Telephone';
$_['entry_telephone_format']  = 'xxxxxxxxxx';
$_['entry_fax']            = 'Fax';
$_['entry_company']        = 'Company';
$_['entry_address_1']      = 'Address 1';
$_['entry_address_2']      = 'Address 2';
$_['entry_postcode']       = 'Postal Code';
$_['entry_city']           = 'City';
$_['entry_country']        = 'Country';
$_['entry_zone']           = 'Province/State/Region';
$_['entry_newsletter']     = 'Subscribe';
$_['entry_password']       = 'Password';
$_['entry_confirm']        = 'Password Confirm';
$_['entry_hear_about_us'] = 'How did you hear about us?';
$_['entry_shoppal_website'] = 'ShopPal Website';
$_['entry_local_flyer'] = 'Flyer';
$_['entry_other_fundraising_organization'] = 'Other Fundraising Organization';
// new fields
$_['entry_other_basic_information'] = 'Organization Basic Information';
$_['entry_organization_name'] = 'Organization Name';
$_['entry_position_organization'] = 'Position in Organization';
$_['entry_mobile_number'] = 'Mobile Number';
$_['entry_organization_phone_number'] = 'Organization Phone Number';
$_['entry_street_address'] = 'Street Address';
$_['entry_street_address2'] = 'Street Address Line 2';
$_['entry_city'] = 'City';
$_['entry_zip_code'] = 'Postal / Zip Code';
$_['entry_country'] = 'Country';
$_['entry_region'] = 'Province/State/Region';
$_['entry_branded_website'] = 'ShopPal Branded Website Name Preferences ( Shoppal.ca/ ??? )';
$_['entry_organization_details'] = 'Organization Details';
$_['entry_organization_website'] = 'Organization Website';
$_['entry_organization_hashtags'] = 'Organization Hashtags';
$_['entry_organization_facebook_link'] = 'Organization Facebook Link';
$_['entry_organization_instagram'] = 'Organization Instagram';
$_['entry_organization_twitter'] = 'Organization Twitter';
$_['entry_organization_linkedIn_link'] = 'Organization LinkedIn Link';
$_['entry_agreement_heading'] = 'The Organization permits Shoppal to use their Logo and Name, not associated with any fundraising amounts, in the following manner';
$_['entry_agreement_checkbox1'] = 'on the main, or other pages of the ShopPal website that display Memberinformation';
$_['entry_agreement_checkbox2'] = 'on Social Media and other Internet Marketing platforms, and/or';
$_['entry_agreement_checkbox3'] = 'on Print Media and or Radio/Television Media.';
$_['entry_logo'] = 'Logo';
$_['entry_organization_mission_statement'] = 'Organization Mission Statement';
$_['entry_organization_detailed'] = 'Organization "What we do" / "Detailed - About Us"';
$_['entry_hear_about_us'] = 'How did you hear about us?';
$_['entry_type_question'] = 'Type a question';
$_['entry_shoppal_website'] = 'ShopPal Website';
$_['entry_local_store'] = 'Local Store';
$_['entry_local_event'] = 'Local Event';
$_['entry_local_flyer'] = 'Flyer';
$_['entry_other_fundraising_organization'] = 'Other Fundraising Organization';
$_['entry_other_hear_about_us'] = 'Referred by';
$_['entry_facebook'] = 'Facebook';
$_['entry_linkedin'] = 'LinkedIn';


// Error
$_['error_exists']         = 'Warning: E-Mail Address is already registered!';
$_['error_firstname']      = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']       = 'Last Name must be between 1 and 32 characters!';
$_['error_email']          = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']      = 'Telephone must be 10 characters';
$_['error_address_1']      = 'Address 1 must be between 3 and 128 characters!';
$_['error_city']           = 'City must be between 2 and 128 characters!';
$_['error_postcode']       = 'Postalcode must have 6 characters!';
$_['error_country']        = 'Please select a country!';
$_['error_zone']           = 'Please select a region / state!';
$_['error_custom_field']   = '%s required!';
$_['error_password']       = 'Password must be between 4 and 20 characters!';
$_['error_confirm']        = 'Password confirmation does not match password!';
$_['error_agree']          = 'Warning: You must agree to the %s!';

// new
$_['error_organization_name']  = 'Organization Name must be between 1 and 50 characters!';
$_['error_position_in_organization']  = 'Must be between 1 and 32 characters!';
$_['error_organization_phone_number']  = 'Organization phone number is invalid';
$_['error_street_address']  = 'Street Address must be between 1 and 255 characters!';
$_['error_city']  = 'City name is required!';
$_['error_organization_mission_statement']  = 'Mission Statement is required!';
$_['error_zone_id']  = 'Province/State/Region is required!';
$_['error_zip_code']  = 'Postal / Zip Code is required!';
$_['error_country_id']  = 'Country is required!';
$_['error_logo_type']  = 'Logo file type is not valid!';
$_['error_logo']  = 'Logo is required!';