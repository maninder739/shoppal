<?php
$_['heading_title'] = 'Update credit card information';

// Text
$_['text_account']       = 'Account';
$_['text_edit']          = 'Edit Credit Card Information';
$_['text_your_details']  = 'Credit Card Details';
$_['text_success']       = 'Success: Your credit card detalis has been successfully updated.';
$_['entry_telephone_format']  = '(xxx) xxx-xxxx';
$_['example_text'] = 'Enter the number without spacing or hyphens ';
 
// Entry
$_['entry_cardNumber']    	  = 'Card Number';
$_['entry_cardType']     	  = 'Card Type';
$_['entry_cardExpiry']        = 'Card Expiry';
$_['entry_cardHolderName']    = 'Card Holder Name';
$_['last_update_on']          = 'Last Update ON';
$_['button_autoship']          = 'Start Autoship';


// Error
$_['error_exists']        = 'Warning: E-Mail address is already registered!';
$_['error_cardHolderName']     = 'Card holder name must be between 1 and 32 characters!';
$_['error_cardNumber']      = 'Card Number is required';
$_['error_cardExpiry']         = 'Card Expiry is required';
$_['error_cardType']     = 'Card type is required';
$_['error_warning']  = 'Please varify card details again.';
 
?>