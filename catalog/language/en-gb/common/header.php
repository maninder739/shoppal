<?php
// Text
$_['text_home']          = 'Home';
$_['text_wishlist']      = 'Wish List (%s)';
$_['text_shopping_cart'] = 'Shopping Cart';
$_['text_category']      = 'Categories';
$_['text_account']       = 'My Account';
$_['text_register']      = 'Register';
$_['text_login']         = 'Login';
$_['text_order']         = 'Order History';
$_['text_transaction']   = 'Transactions';
$_['text_download']      = 'Downloads';
$_['text_logout']        = 'Logout';
$_['text_checkout']      = 'Checkout';
$_['text_search']        = 'Search';
$_['text_all']           = 'Show All';
$_['text_mission']       = "ShopPal's mission is to provide unique products that offer unparalleled value to the consumer while maintaining a continuous source of funding for charities and non-profits.";$_['share_cart']         = 'Share your cart via link';$_['get_link']           = 'Get link';