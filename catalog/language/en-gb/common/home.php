<?php
// Text
$_['text_how_it_works']       = 'How it works';
$_['text_how_it_works1']      = 'Make a one-time order, or sign up for a monthly order.';
$_['text_how_it_works2']      = '10% from every order goes to support';
$_['text_how_it_works3']      = 'Your items are delivered right to your door!';
$_['text_show_support']      = "It's SIMPLE and EASY to show your support!";
$_['text_show_support1']     = 'You get your items, and a portion of the proceeds from every order goes to help support';
$_['text_want_to_support']     = "I want to shop to support";
$_['text_want_to_support_text'] = "ShopPal’s mission is to help all non-profits and charitable causes reach their fundraising goals. When you shop with us, 10% of your sales volume goes directly to the charity that referred you. Thank you for your continued support! When you shop, shop for a cause!";
 $_['cancel_order_text'] = "* You can cancel or modify your order at any time on the website.";
