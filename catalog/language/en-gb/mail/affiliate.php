<?php
// Text
$_['text_subject']		        = '%s - Member Program';
$_['text_welcome']		        = 'Thank you for joining the %s Fundraising Program!';
$_['text_login']                = 'Your account has now been activated and you can log in by using your e-mail address and the password your provided at sign up by visiting our website or at the following URL:';
$_['text_approval']		        = 'Your account is pending. Once activated, you will be able to log in by using your e-mail address and password by visiting our website or at the following URL:';
$_['text_services']		        = 'Once logged in, you will be able to place orders for your organization, track supporter registrations, track shopping volume, track your monthly member benefits and much more.';
$_['text_thanks']		        = 'Thanks,';
$_['text_new_affiliate']        = 'New Member';
$_['text_signup']		        = 'A new member has signed up:';
$_['text_store']		        = 'Store:';
$_['text_firstname']	        = 'First Name:';
$_['text_lastname']		        = 'Last Name:';
$_['text_company']		        = 'Company:';
$_['text_email']		        = 'E-Mail:';
$_['text_telephone']	        = 'Telephone:';
$_['text_website']		        = 'Web Site:';
$_['text_order_id']             = 'Order ID:';
$_['text_transaction_subject']  = '%s - Member Benefit';
$_['text_transaction_received'] = 'Your supporters have shopped and fundraised %s for you!';
$_['text_transaction_total']    = 'Your total Member Benefit is now %s.';
$_['text_welcome_referring']	= 'A New Affiliate Has Signed Up';
$_['date']	= "Date:";
$_['text_organization']		        = 'Organization:';
$_['joined_under']      = 'Joined Under:';
