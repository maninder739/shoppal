<?php
// Text
 
$_['text_update_subject']          = 'Stop Autship Order'; 
$_['text_header'] = "Hello %s ";
$_['text_message']  = 'We have processed your cancellation of the autoship. The current order has already or will ship very soon. This will be the last one. If you wish to reorder, you may do at any time.';
$_['text_thanks'] = "Thank you for your support."; 
 