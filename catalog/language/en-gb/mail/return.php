<?php
$_['text_subject']	 = 'Hello  %s';
$_['text_body']      = 'We have received a request for the return order';
$_['text_order_id']  =   'Order ID - %s';
$_['text_thanks'] = "Thank you for shopping with us!"; 
$_['text_approve_subject']  = 'Order';
 
$_['text_review'] = "We will review your request and get back to you with the next 5 to 7 days";



 //admin

$_['admin_approve_subject']  = 'New Product Return Request - %s';
$_['admin_subject']  = 'A new product request has been made in your store';
$_['customer_name']  = 'Customer:';
$_['customer_email']  = 'Email:';
$_['customer_phone']  = 'Phone:';
$_['admin_order_id']  =   'Order ID:';
$_['admin_order_date']  =   'Date Ordered:';
$_['admin_order_product']  =   'Product:';
$_['admin_order_product_model']  =   'Product Model:';
$_['admin_order_return_reason']  =   'Return reason:';
$_['admin_order_review_admin']  =   'You can review the order return from you shoppal administration:';

