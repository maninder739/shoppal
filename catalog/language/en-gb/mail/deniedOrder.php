<?php
// Text

$_['text_new_subject']          = '%s - Order %s';
$_['text_header'] = "Dear ";
$_['text_greeting'] = 'Thank you for being a ShopPal monthly order customer and supporting an organization each month.';
$_['text_message']  = 'We attempted to charge your monthly order to the credit card on file, however we were unable to do so because the card issuer declined the transaction. As such we are unable to ship your order at this time.';
$_['text_message1']  = 'To avoid missing your order this month, please update your credit card information or enter a new credit card.';
$_['text_message2']  = 'To do this, login to your account (https://spdev.shoppalweb.ca/login) and click on your name at the top right, and then Update Stored Credit Cards in your customer menu. Once you have updated, your monthly order will be charged and shipped as usual.';
$_['text_alert']   = 'Please note that your monthly order will expire in 30 days if you do not update this information and you may be subject to new pricing for your current products on the monthly order.
';
$_['text_footer']   = "If you have questions about your account, please contact ShopPal's Customer Service. (hotlink to customerservice@shoppal.ca)";
$_['text_thanks'] = "Thank you for shopping with us!";
$_['team'] = "The ShopPal Team";
//$_['text_update_order_status']  = 'Your order has been updated to the following status:';
