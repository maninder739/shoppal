<?php
// Text
$_['text_subject']	= '%s - Product Review';$_['store']	= 'A New Review Product Under Store of:';
$_['text_waiting']	= 'You have a new product review waiting.';
$_['text_product']	= 'Product: %s';
$_['text_reviewer']	= 'Reviewer: %s';
$_['text_rating']	= 'Rating: %s';
$_['text_review']	= 'Review Text:';