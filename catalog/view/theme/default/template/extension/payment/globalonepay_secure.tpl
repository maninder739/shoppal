<div style="margin-top:40px;">
<div class="alert alert-warning" style="display: none; margin-left: 298px; color: red;"><i class="fa fa-exclamation-circle"></i></div>
<h3>Add Card Details <?php echo $ext_link; ?></h3>
<form action="" method="POST" id="globalonepayhpp_payment" class="form-horizontal">
<div class="form-group required">
    <label class="col-sm-2 control-label" for="input-payment-cardnumber">Card Number</label>
    <div class="col-sm-10">
      <input type="text" name="cardnumber" placeholder="Card Number" id="input-payment-cardnumber" class="form-control" required>
    </div>
  </div>
   <div class="form-group required card-select">
    <label class="col-sm-2 control-label" for="input-payment-cardtype">Card Type</label>
    <div class="col-sm-10">
      <select class="form-control" id="cardtype" name="cardtype" required>
            <?php foreach($card_types as $key=>$val){ ?>
            <option value="<?php echo $key ?>"><?php echo $val; ?></option>
            <?php } ?>
          </select>
    </div>
  </div>
   <div class="form-group required">
    <label class="col-sm-2 control-label" for="input-payment-cardexpiry">Card Expiry Date</label>
      <div class="col-sm-10 expiry-date">
       <div class="xl-10">
        <select class="form-control" name="expiry_month" id="expiry-month" required>
            <option>Month</option>
              <?php
              for ($m=1; $m<=12; $m++) {
              if($m < 10){
                $m = '0'.$m;
              }
              ?>
              <option value="<?php echo $m; ?>"><?php echo $m; ?></option>
              <?php
              }
              ?>
        </select>
      </div>
      <div class="xl-10">
        <select class="form-control" name="expiry_year" required>
        <?php
        foreach($years as $year)
        {
        ?>
           <option value="<?php echo substr($year,2); ?>"><?php echo $year; ?></option>
       <?php }
        ?>
        </select>
      </div>
    </div>
  </div>
   <div class="form-group required">
    <label class="col-sm-2 control-label" for="input-payment-cardholdername">Card Holder Name</label>
    <div class="col-sm-10">
      <input type="text" name="cardholdername" placeholder="Card Holder Name" id="input-payment-cardnumber" class="form-control" required>
    </div>
  </div>
     <div class="form-group required">
    <label class="col-sm-2 control-label" for="input-payment-cvv">CVV</label>
    <div class="col-sm-10">
      <input type="text" name="cvv" placeholder="CVV" id="input-payment-cvv" class="form-control" required>
    </div>
  </div>

<div class="buttons">
<div class="pull-right">
  <input type="button" value="<?php echo $button_confirm; ?>" id="button-confirm" class="btn btn-primary" data-loading-text="<?php echo $text_loading; ?>"/>
</div>
</div>
</form>
</div>
<script type="text/javascript"><!--
$('#button-confirm').on('click', function() {
$.ajax({
  type: 'POST',
  url:'<?php echo $action; ?>',
  cache: false,
  dataType: 'json',
  data: $("#globalonepayhpp_payment").serialize(),
  beforeSend: function() {
    $('#button-confirm').button('loading');
  },
  complete: function() {
    $('#button-confirm').button('reset');
  },
  success: function(res) {

    if('error' in res){
      $('.alert-warning').text(res.error.warning).show();
    }else if( 'redirect' in res ) {
        location = res.redirect;
    }else{

      $('.alert-warning').hide();
      location = '<?php echo $continue; ?>';
    }
  
  },
   error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
  }
});
});
//--></script>
