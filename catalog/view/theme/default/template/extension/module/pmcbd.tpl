<link type="text/css" href="catalog/view/javascript/pmcbd/css/component.css" rel="stylesheet" />
<h3><?php echo $heading_title;?></h3> 	 

<ul id="tabs" class="nav nav-tabs htabs">
  <?php $flag = false; foreach ($pmcbddata as $key => $cddata) { ?> 
	<?php if($key < $limit) { ?>
		<li <?php if($key==0) { ?> class="active" <?php } ?>><a data-toggle="tab" href="#<?php echo $randstr.$key;?>"><?php echo $cddata['tabtitle'];?></a></li>
	<?php } else if($key == $limit) { $flag = true;?>
		<li class="dropdown">
		<a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo $text_morecombo;?> <span class="caret"></span></a>
		<ul class="dropdown-menu">
		<li><a data-toggle="tab" href="#<?php echo $randstr.$key;?>"><?php echo $cddata['tabtitle'];?></a></li>
	<?php } else { ?>
		<li><a data-toggle="tab" href="#<?php echo $randstr.$key;?>"><?php echo $cddata['tabtitle'];?></a></li>
	<?php } ?>
		 
	<?php } ?>
<?php if($flag) { ?>
	</ul>
	</li>
<?php } ?>
	
</ul> 
 
<div class="tabs-content">
	<?php $inc_tx_flag = false; foreach ($pmcbddata as $key => $cddata) { ?> 
    <div id="<?php echo $randstr.$key;?>" class="tab-pane tab-content fade <?php if($key==0) { ?> in active <?php } ?> ">
      <h3><?php echo $cddata['tabtitle'];?></h3>
      <div class="mainproddiv_pmcbd">
		<div id="cbp-vm" class="cbp-vm-switcher_pmcbd cbp-vm-view-grid_pmcbd">
		<ul>
			<?php foreach ($cddata['product'] as $pdkey => $product) { ?>
			<li>
 				<a class="cbp-vm-image_pmcbd" href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
				<h3 class="cbp-vm-title_pmcbd"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h3>
				<div class="cbp-vm-price_pmcbd pmcbdprodprc">	
					<?php if ( $product['price'] ) { ?> 
						<?php if (!$product['special']) { ?>  
							<p class="prcnew"> <?php echo $product['price']; ?>  </p>
						<?php } else { ?> 
							<p class="prcold"> <?php echo $product['price']; ?>  </p>
							<p class="prcnew"> <?php echo $product['special']; ?>  </p>
						<?php } ?>
						<?php if ($product['tax']) { $inc_tx_flag = true; ?>
						  <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
						<?php } ?>
					<?php } ?>
				</div>
			</li>    
			<?php if($pdkey == count($cddata['product'])-1) { ?> <li><span class="plussign">=</span></li> <?php } else { ?> <li><span class="plussign">+</span></li> <?php } ?>
			<?php } ?>
			<li>  			  
				<div class="cbp-vm-price_pmcbd pmcbdtotalval">
					<p class="prcold"><?php echo sprintf($text_pmcbdttlprc,$cddata['pmcbdttlprc']);?></p> 
					<p class="prcsave"> <?php echo sprintf($text_pmcbd,$cddata['pmcbd']);?></p>
					<p class="prcsave"> <?php echo sprintf($text_pmcbdprc,$cddata['pmcbdprc']);?></p>
					<p class="prcsave"> <?php echo sprintf(($inc_tx_flag) ? $text_pmcbdsave_tx : $text_pmcbdsave ,$cddata['pmcbdsave']);?></p>
					<a class="cbp-vm-icon_pmcbd cbp-vm-add_pmcbd" id="addtocartbundle" onclick="addtocartbundle('<?php echo $cddata['product_id_str'];?>','<?php echo $pmcbd_id;?>','<?php echo $target_product_id;?>','<?php echo $key;?>');"><?php echo $text_pmcbdaddtocart; ?></a>
				</div>
			</li>
		</ul>
		</div>
	</div>
     </div>
	<?php } ?>
</div>
 
  
<script language="javascript">
function addtocartbundle(product_id_str,pmcbd_id,target_product_id, pmcbd_id_key) {
	$.ajax({
		url: '<?php echo htmlspecialchars_decode($addcombourl);?>',
		type: 'post',
		data: 'product_id_str=' + product_id_str +'&pmcbd_id=' + pmcbd_id +'&target_product_id=' + target_product_id +'&pmcbd_id_key=' + pmcbd_id_key ,
		dataType: 'json',
		beforeSend: function() {
			$('#addtocartbundle'+pmcbd_id_key).button('loading');
		},
		complete: function() {
			$('#addtocartbundle'+pmcbd_id_key).button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();

			if (json['redirect']) {
				location = json['redirect'];
			}

			if (json['success']) {
				$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				// Need to set timeout otherwise it wont update the total
				setTimeout(function () {
					$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
				}, 100);

				$('html, body').animate({ scrollTop: 0 }, 'slow');

				$('#cart > ul').load('index.php?route=common/cart/info ul li');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
}
</script>