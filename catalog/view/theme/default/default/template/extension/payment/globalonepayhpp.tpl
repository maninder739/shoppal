<?php if ($test) { ?>
  <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $text_testmode ?></div>
<?php } ?>
<form action="<?php echo $action; ?>" method="get" id="globalonepayhpp_payment">
  <input type="hidden" name="TERMINALID" value="<?php echo $terminalid; ?>" />
  <input type="hidden" name="ORDERID" value="<?php echo $order_id; ?>" />
  <input type="hidden" name="AMOUNT" value="<?php echo $amount; ?>" />
  <input type="hidden" name="CURRENCY" value="<?php echo $currency; ?>" />
  <input type="hidden" name="DESCRIPTION" value="<?php echo $description; ?>" />
  <input type="hidden" name="DATETIME" value="<?php echo $date_time; ?>" />
  <input type="hidden" name="CARDHOLDERNAME" value="<?php echo $name; ?>" />
  <input type="hidden" name="ADDRESS1" value="<?php echo $address1; ?>" />
  <input type="hidden" name="ADDRESS2" value="<?php echo $address2; ?>" />
  <input type="hidden" name="POSTCODE" value="<?php echo $postcode; ?>" />
  <input type="hidden" name="COUNTRY" value="<?php echo $country; ?>" />
  <input type="hidden" name="PHONE" value="<?php echo $telephone; ?>" />
<?php if(isset($email) && $email !='') { ?>
  <input type="hidden" name="EMAIL" value="<?php echo $email; ?>" />
<?php } ?>
  <input type="hidden" name="RECEIPTPAGEURL" value="<?php echo $receipt_page_URL; ?>" />
  <input type="hidden" name="VALIDATIONURL" value="<?php echo $validation_URL; ?>" />
  <input type="hidden" name="HASH" value="<?php echo $hash; ?>" />
</form>
<div class="buttons">
  <div class="pull-right">
    <input type="button" value="<?php echo $button_confirm; ?>" id="button-confirm" class="btn btn-primary" />
  </div>
</div>
<script type="text/javascript"><!--
$('#button-confirm').bind('click', function() {
  $('#globalonepayhpp_payment').submit();
});
//--></script>
