<div class="journal-slider-<?php echo $module_id; ?> tp-banner-container box <?php echo $js_options['hideThumbs'] ? 'nav-on-hover' : '' ?> <?php echo $slider_class; ?> <?php echo $js_options['thumbAmount'] === '' ? 'full-thumbs' : ''; ?> <?php echo implode(' ', $disable_on_classes); ?> <?php echo Journal2Utils::getProperty($js_options, 'navigationType') === 'none' ? 'hide-navigation' : ''; ?>" style="<?php echo $width; ?>; height: <?php echo $height; ?>px;">
    <div class="tp-banner" id="journal-slider-<?php echo $module; ?>" style="display: none; background-image: url('<?php echo $slides[0]['image']; ?>'); background-size: cover; background-position: center center;">
        <ul>
            <?php  $index = 0;$m=0;foreach ($slides as $slide):$index = $index+1; ?>
            <li <?php echo $slide['data']; ?> >
			<!-- this is for affiliate page -->
			<?php
			if($affiliate)
			{
				if($index==1)
				{
					$image_name_slide1 = $slide['image'];
					$slide_name_slide1 = $slide['name'];
				}
				else
				{
					$image_name = $slide['image'];
					$slide_name = $slide['name'];
				}

				if($index!=1)
				{

					if ($preload_images)
					{?>
						<img src="<?php echo $dummy_image; ?>" data-lazyload="<?php echo $slide['image']; ?>" width="<?php echo $js_options['startwidth']; ?>" height="<?php echo $js_options['startheight']; ?>" alt="<?php echo $slide['name']; ?>" />
					<?php
					}
				 	else
				 	{
						if($index == 2)
						{
						?>
							<img src="<?php echo $image_name_slide1; ?>" width="<?php echo $js_options['startwidth']; ?>" height="<?php echo $js_options['startheight']; ?>" alt="<?php echo $slide_name_slide1; ?>" />
						<?php 
						}
						else
						{?>
							<img src="<?php echo $image_name; ?>" width="<?php echo $js_options['startwidth']; ?>" height="<?php echo $js_options['startheight']; ?>" alt="<?php echo $slide_name; ?>" />	
						<?php
						}
					} 	
				}
				else
				{?>

					<img src="image/slider/<?php echo $slider_image;?>" width="{{ js_options.startwidth }}" height="{{ 	js_options.startheight }}" alt="{{ slide.name }}"/>
				<?php
				}
				
			}
			// this is for affiliate page
			
			// this is for general page
			else
			{
				if ($preload_images): ?>
					<img src="<?php echo $dummy_image; ?>" data-lazyload="<?php echo $slide['image']; ?>" width="<?php echo $js_options['startwidth']; ?>" height="<?php echo $js_options['startheight']; ?>" alt="<?php echo $slide['name']; ?>" />
					<?php else: ?>
					<img src="<?php echo $slide['image']; ?>" width="<?php echo $js_options['startwidth']; ?>" height="<?php echo $js_options['startheight']; ?>" alt="<?php echo $slide['name']; ?>" />
				<?php endif; 	
			}
			
			?>
			<!--this is for general page-->
			
			
			<!-- this is for affiliate page -->	
			
			
            <?php 
			if($affiliate)
			{
				if($index == 1)
				{?>
					<div class="main-affiliate">
						<?php if($affiliate_name){?>
							<div class="affiliate-name">
								<h2><?php echo $total_purchases .'<br/>'. $affiliate_name ; ?></h2>
								 
								<?php
								if($custom_logo)
								{?>
									<img src="image/data/<?php echo $custom_logo;?>" alt="" />
									
								<?php
								}?>
							</div>
							<?php }
							else {?>
								<div class="affiliate-name" style="display:none;">
								<h2><?php echo $total_purchases .'<br/>'. $affiliate_name ; ?></h2>
								<?php
								if($custom_logo)
								{?>
									<img src="image/data/<?php echo $custom_logo;?>" alt="" />
									
								<?php
								}?>
							</div>
							<?php } ?>
					  	<div class="affiliate-mission fadeout start"> 
							<h2><?php echo $mission;?></h2> 
					  	</div>
					  	<div class="button-center">
					  		<a href="/shop-for-a-cause; ?>" class="btn btn-info" role="button" style="font-weight: 400;font-family: Ubuntu;font-size: 20px;font-style: normal;text-transform: uppercase;color: rgb(255, 255, 255);text-align: center;background-color: rgb(221, 0, 23);padding: 12px 20px;""><?php echo $shop_now;?></a>
					  	</div>
					</div>
				<?php
				}
				if($index != 1)
				{
					
					if($m == 0)
					{
						if($first_slide)
						{
							$m=1;
							
							//{% for captionIst in first_slide[0].captions %} 
							foreach($first_slide[0]['captions'] as $captionIst)
							{
								if($captionIst['link'])
								{?>
									<a id="jcaption-<?php echo $captionIst['id'];?>" href="<?php echo $captionIst['link'] ;?>" <?php echo $captionIst['target'];?> class="tp-caption <?php echo $captionIst['classes'];?>" style="<?php echo $captionIst['css'];?>" <?php echo $captionIst['data'];?>>
										<?php echo $captionIst['content'];?>
									</a>
								<?php							
								}
								else
								{?>
									<div id="jcaption-<?php echo $captionIst['id'];?>" class="tp-caption <?php echo $captionIst['classes'];?>" style="<?php echo $captionIst['css'];?>" <?php echo $captionIst['data'];?>>
									<?php echo $captionIst['content'];?>
									</div>
								<?php
								}
							}
						}
				    }
					else
					{
						foreach ($slide['captions'] as $caption): ?>
						<?php if ($caption['link']): ?>
						<a id="jcaption-<?php echo $caption['id']; ?>" href="<?php echo $caption['link']; ?>" <?php echo $caption['target']; ?> class="tp-caption <?php echo $caption['classes']; ?>" style="<?php echo $caption['css']; ?>" <?php echo $caption['data']; ?>>
						<?php echo $caption['content']; ?>
						</a>
						<?php else: ?>
						<div id="jcaption-<?php echo $caption['id']; ?>" class="tp-caption <?php echo $caption['classes']; ?>" style="<?php echo $caption['css']; ?>" <?php echo $caption['data']; ?>>
						<?php echo $caption['content']; ?>
						</div>
						<?php endif; ?>
						<?php endforeach;
					}

				}

			}
			//this is for affiliate page 
			// this is for general page
			else
			{
				foreach ($slide['captions'] as $caption): ?>
				<?php if ($caption['link']): ?>
				<a id="jcaption-<?php echo $caption['id']; ?>" href="<?php echo $caption['link']; ?>" <?php echo $caption['target']; ?> class="tp-caption <?php echo $caption['classes']; ?>" style="<?php echo $caption['css']; ?>" <?php echo $caption['data']; ?>>
				<?php echo $caption['content']; ?>
				</a>
				<?php else: ?>
				<div id="jcaption-<?php echo $caption['id']; ?>" class="tp-caption  <?php echo $caption['classes']; ?>" style="<?php echo $caption['css']; ?>" <?php echo $caption['data']; ?>>
				<?php echo $caption['content']; ?>
				</div>
				<?php endif; ?>
				<?php endforeach;
			}
			// this is for general page
			?>
			</li>
			<?php endforeach; ?>
         </ul>
        <?php if ($timer === 'top'): ?>
        <div class="tp-bannertimer"></div>
        <?php elseif ($timer === 'bottom'): ?>
        <div class="tp-bannertimer tp-bottom"></div>
        <?php endif; ?>
    </div>
</div>

<?php if (isset($css) && $css): ?>
<style>
<?php echo implode (' ', $css); ?>
</style>
<?php endif; ?>

<script>
    (function () {
        $('<style><?php echo implode(" ", $global_style); ?></style>').appendTo($('head'));

        var opts = $.parseJSON('<?php echo json_encode($js_options); ?>');
        opts.hideThumbs = 0;
        $('#journal-slider-<?php echo $module; ?>').show().revolution(opts);
        <?php if ($timer !== 'top' && $timer !== 'bottom'): ?>
        $('#journal-slider-<?php echo $module; ?> .tp-bannertimer').hide();
        <?php endif; ?>
        setTimeout(function() {
            $('#journal-slider-<?php echo $module; ?>').css('background-image', 'none');
        }, 2500);
    })();
</script>


<style type="text/css">
	.button-center{ width: 100%; float: left; text-align: center; }
	.add-iocon-block{ width: 100%; float: left; } 
	.add-iocon-block .cms-block {width: 100%; float: left;  }
	.add-iocon-block .btn-primary {font-size: 16px; padding: 10px 50px; background-color: red; border-radius: 100px;}

</style>
