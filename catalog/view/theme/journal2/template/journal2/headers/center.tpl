 <?php //echo  $member_type; die('ds'); ?>
<header class="journal-header-center">
    <div class="header">
    <div class="journal-top-header j-min z-1"></div>
    <div class="journal-menu-bg z-0"> </div>
    <div class="journal-center-bg j-100 z-0"> </div>

    <div id="header" class="journal-header z-2">

        <div class="header-assets top-bar">
            <div class="journal-links j-min xs-100 sm-100 md-50 lg-50 xl-50">
                <div class="links">
                   <ul class="top-menu">
                    <?php echo $this->journal2->settings->get('config_primary_menu'); ?>
                    </ul>
                </div>
            </div>

            <?php if ($language): ?>
            <div class="journal-language j-min">
                <?php echo $language; ?>
            </div>
            <?php endif; ?>

            <?php if ($currency): ?>
            <div class="journal-currency j-min">
                <?php echo $currency; ?>
            </div>
            <?php endif; ?>

            <div class="journal-secondary j-min xs-100 sm-100 md-50 lg-50 xl-50">
                <div class="links">
                    <ul class="top-menu">
                    <?php echo $this->journal2->settings->get('config_secondary_menu'); ?>
                    <?php 
                    if(isset($logged_in))
                    {?>
                    <li class="affi-logout">
                        <p>
                            <a href=" <?php echo $this->url->link('affiliate/account');?>"><i style="margin-right: 5px; font-size: 16px" data-icon=""></i><?php echo $firstname;?>
                            </a>
                        </p>
                    </li>
                    <li class=""><p>
                            <a href="<?php echo $this->url->link('affiliate/logout');?>"><i style="margin-right: 5px; font-size: 15px; top: -1px" data-icon=""></i><?php echo $text_logout;?></a>
                             </p>
                    </li> 
                    <?php 
                    }?>    
                    </ul>
                </div>
            </div>
        </div>

        <!-- hide logo -->
         <div class="header-assets">
        
             <div class="journal-logo j-100 xs-100 sm-100 md-50 lg-50 xl-25"> 
                <?php if ($logo) { ?>
                    <div id="logo" class="default-logo" <?php if($custom_logo) echo "style='height:60px;width:85px;margin-top:22px;'";?>>
                    <!--
                        <a href="/">
                            <?php //echo Journal2Utils::getLogo($this->config); ?>
                        </a>
                    -->
                    </div>
                        <?php 
                    }
                    if($custom_logo)
                    {?>
                        <div class="custom-logo">
                        <a href="<?php echo str_replace('index.php?route=common/home', '', $home); ?>">
                            <img src="image/data/<?php echo $custom_logo; ?>" alt="ShopPal" title="ShopPal" class="logo-1x">
                        </a>
                        </div>
                    <?php   
                    }
                    
                ?>
            </div>
            <div class="journal-search j-min xs-100 sm-50 md-25 lg-25 xl-50">
                <p style="font-size: 16px;color: #fff;margin-top: -25px;padding-right: 65px;">
                    <?php echo $text_mission;?>
                 </p>
            </div>
            <div class="journal-cart j-min xs-100 sm-50 md-25 lg-25 xl-25">
                <?php if (version_compare(VERSION, '2', '>=')): ?>
                    <?php echo $search; ?>
                <?php else: ?>
                    <div id="search" class="j-min">
                        <div class="button-search j-min"><i></i></div>
                        <?php if (isset($filter_name)): /* v1541 compatibility */ ?>
                            <?php if ($filter_name) { ?>
                                <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" autocomplete="off" />
                            <?php } else { ?>
                                <input type="text" name="filter_name" value="<?php echo $text_search; ?>" autocomplete="off" onclick="this.value = '';" onkeydown="this.style.color = '#000000';" />
                            <?php } ?>
                        <?php else: ?>
                            <input type="text" name="search" placeholder="<?php echo $this->journal2->settings->get('search_placeholder_text'); ?>" value="<?php echo $search; ?>" autocomplete="off" />
                        <?php endif; /* end v1541 compatibility */ ?>
                    </div>
                <?php endif; ?>
            
                <?php echo $cart; ?>
            </div>
        </div>

        <div class="journal-menu j-min xs-100 sm-100 md-100 lg-100 xl-100">
            <?php echo $this->journal2->settings->get('config_mega_menu'); ?>
        </div>
    </div>
    </div>
    <?php 
 
    if($class == 'information-information-12' || $class == 'information-information-13' || $class == 'information-information-14'   ) { ?>
     <style type="text/css">
            .breadcrumb  {display: none;}
            .extended-container::before { display: none; }
            .journal-header-center .journal-menu-bg{ top: 40px; display: none; }
            .journal-menu-bg {display: none; }
            .journal-menu{display:none;}
            .heading-title{display:none;}    
            .header-assets {display: none;}   
            .top-bar{display: block;} 
     </style>
    <?php  } ?>
</header>
