 <?php echo $header; ?>
<?php 
//CB replace image by pics
$spimgfolder = 'pics';
$spimgfolderdata = 'pics/data/';
?>

<div id="container" class="container j-container">
     <?php if( ($member_type == 2)  || $REQUEST_URI == '/' ) { ?>
    <div class="row">
        <?php echo $column_left; ?><?php echo $column_right; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?><?php echo $content_bottom; ?></div>
    </div>
</div>
<?php } else { ?>
<div class="row">
    <div id="content" class="col-sm-12">
        <p class="main-title"><?php echo $custom_title; ?></p>
        <div class="xl-50 f-img-box sm-100" style="margin-top: 0;">
            <?php if(!empty($member_image1)) { ?> 
            <img class="img-one" alt="" src="<?php echo $spimgfolderdata. "thump_".$member_image1; ?>"  />
            <?php } else { ?>
            <img alt="" src="<? echo $spimgfolderdata; ?>standard/image1b.jpeg" class="img-one" />
            <?php } ?>
        </div>
        <div class="xl-50 f-text-box sm-100">
            <p class="mission-p text-left"><?php  echo isset($mission) ? html_entity_decode(nl2br($mission)) : html_entity_decode($short_mission) ; ?></p>
        </div>
        <div class="funder-box" style="margin-top: 40px;">
            <h3><?php echo $text_how_it_works; ?></h3>
            <div class="funder-inbox">
                <ol>
                    <li class="funder-step"><?php echo $text_how_it_works1; ?></li>
                    <li class="funder-step"><?php echo $text_how_it_works2 .' '. $organization; ?>.</li>
                    <li class="funder-step"><?php echo $text_how_it_works3; ?></li>
                </ol>
            </div>
        </div>
        <?php
            if(!empty($member_video)) { ?>
        <div class="funder-box">
            <?php
                $url = $member_video;
                 preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
                $id = $matches[1];
                
                ?>
            <iframe id="ytplayer" type="text/html" width="50%" height="333px" class="youtube-link" src="https://www.youtube.com/embed/<?php echo $id ?>?rel=0&showinfo=0&color=white&iv_load_policy=3" frameborder="0" allowfullscreen></iframe> 
            <h3 class="simple-easy" style=""><?php echo $text_show_support; ?></h3>
            <p class="mission-p1"> <?php echo $text_show_support1 .' '. $organization; ?></p>
        </div>
        <?php } else { ?>
        <div class="funder-box">
            <?php if(!empty($member_image2)) { ?>
            <img class="img-baby" alt="" src="<?php echo $spimgfolderdata . "thump_".$member_image2; ?>" style="width:100px">    
            <p>&nbsp;</p>
            <?php } else { ?>
            <img class="img-baby" alt="" src="<?php echo $spimgfolderdata; ?>/standard/image2b.jpg">   
            <p>&nbsp;</p>
            <?php } ?>
            <h3 class="simple-easy" style=""><?php echo $text_show_support; ?></h3>
            <p class="mission-p1"> <?php echo $text_show_support1 .' '. $organization; ?>.</p>
        </div>
        <?php }
            ?>
        <div class="funder-box">
            <a class="btn-primary button" href="<?php echo $action_tracking; ?>"><?php echo $text_want_to_support .' '. $organization; ?></a>
        </div>
        <div class="funder-box">
            <p class="mission-p"><?php echo $text_want_to_support_text ?></p>
            <br>
            <br>
            <p class="mission-p"><?php echo $cancel_order_text ?></p>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
</div>
<style> 
#content {
    background-color: rgb(244, 244, 244);
    padding: 10px 20px !important;
    margin-bottom: 30px;
    margin-top: 20px;
}

.funder-inbox {
    width: 60%;
}

.start-shopping {
    display: none;
}

.funder-box {
    margin-bottom: 20px;
}

.mission-p {
    margin-bottom: 10px;
}

.text-left {
    text-align: left;
}

.heading-title {
    display: none;
}

.main-title {
    font-family: Georgia;
    font-size: 20pt;
    text-align: left;
    font-weight: normal;
    margin-bottom: 15px;
}

.mission-p1 {
    font-size: 15pt;
    font-family: Georgia;
    text-align: center;
    font-weight: normal;
    width: 50%;
    margin: auto;
    margin-top: 20px;
}

.img-baby {
    width: 50% !important;
    height: auto;
}

.f-text-box {
    padding: 0px 40px;
}

.simple-easy {
    font-size: 23pt;
    margin-top: 30px;
}

@media only screen and (max-width: 768px) {
    .funder-inbox {
        width: 100%;
    }
    .img-baby {
        width: 80% !important;
    }
    .f-text-box {
        margin-top: 20px;
        padding: 0px 10px;
    }
    .main-title {
        font-size: 14pt;
    }
    .simple-easy {
        font-size: 18pt;
        margin-top: 10px;
    }
    .mission-p1 {
        font-size: 13pt;
        width: 100%;
    }
    .funder-step {
        font-size: 12pt;
    }
    .funder-box h3 {
        font-size: 18pt;
    }
}

.funder-step {
    font-family: Georgia;
    font-size: 16pt;
    text-align: left;
    font-weight: normal;
    margin-left: 25px;
    padding: 10px;
}

.funder-inbox {
    margin: 0 auto;
}</style>
<?php } ?>
 <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
<script>
 <?php if($lang == 'en' || $browser_lang == 'en') { ?>  
window.addEventListener("load", function(){
    window.cookieconsent.initialise({
        "palette": {
            "popup": {
              "background": "#000"
            },
            "button": {
              "background": "#f1d600"
            }
        },
        "content": {
        "message" : "This website uses cookies to ensure you get the best experience on our website.",
        "link": "Terms & Conditions",
        "href": 'https://shoppal.ca/terms',
         "dismiss": "Got it!"
        }
    })

});

<?php } else if($lang == 'fr' || $browser_lang == 'fr') { ?>
 
window.addEventListener("load", function(){
    window.cookieconsent.initialise({
        "palette": {
            "popup": {
              "background": "#000"
            },
            "button": {
              "background": "#f1d600"
            }
        },
        "content": {
        "message" : "Ce site utilise des cookies afin d'assurer une meilleur experience sur notre site.",
        "link": "Termes & Conditions",
        "href": 'https://shoppal.ca/terms',
        "dismiss": "Ok, merci!"
        }
    })

});
<?php } ?>
</script>
<?php echo $footer; ?>
