<div class="checkout-content">
    <?php if ($error_warning) { ?>
    <div class="alert alert-warning warning"><i class="fa fa-exclamation-circle"></i>
        <?php echo $error_warning; ?>
    </div>
    <?php } ?>
    
    <p>
        <?php echo $text_payment_method; ?>
    </p>
    <?php if (($payment_methods) && empty($card_details) ) { ?>
    <?php foreach ($payment_methods as $payment_method) { ?>
    <div class="radio">
        <label>
      <?php if ($payment_method['code'] == $code || !$code) { ?>
      <?php $code = $payment_method['code']; ?>
      <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" checked="checked" />
      <?php } else { ?>
      <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" />
      <?php } ?>
      <?php echo $payment_method['title']; ?>
      <?php if (isset($payment_method['terms']) && $payment_method['terms']) { ?>
      (<?php echo $payment_method['terms']; ?>)
      <?php } ?>
    </label>
    </div>
    <?php } ?>
    <?php } else { ?> 
    <div class="radio">
        <label><input type="radio" name="payment_method" value="cod" checked="checked"><?php echo $text_use_the_credit_card_on_file; ?></label>
    </div>
    <div class="radio">
        <label><input type="radio" name="payment_method" value="globalonepay_secure" ><?php echo $text_use_a_new_credit_card; ?></label>
    </div>
    <?php } ?>
    <p><strong><?php echo $text_comments; ?></strong></p>
    <p>
        <textarea name="comment" rows="8" class="form-control"><?php echo $comment; ?></textarea>
    </p>
    <?php if ($text_agree) { ?>
    <div class="buttons">
        <div class="pull-right">
            <?php echo $text_agree; ?>
            <?php if ($agree) { ?>
            <input type="checkbox" name="agree" value="1" checked="checked" />
            <?php } else { ?>
            <input type="checkbox" name="agree" value="1" />
            <?php } ?> &nbsp;
            <input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary button" />
        </div>
    </div>
    <?php } else { ?>
    <div class="buttons">
        <div class="pull-right">
            <input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary button" />
        </div>
    </div>
    <?php } ?>
</div>