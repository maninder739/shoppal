<div class="checkout-content">
  
  <p> 
    <input type="radio" name="order_type" value="monthly" checked> <?php echo $monthly_autoship; ?> <br>
    <input type="radio" name="order_type" value="one time"> <?php echo $one_time_order; ?><br>
  </p>
  
   <div class="buttons">
    <div class="pull-right">
      <input type="button" value="<?php echo $button_continue; ?>" id="button-order-type" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary button" />
    </div>
  </div>

</div>