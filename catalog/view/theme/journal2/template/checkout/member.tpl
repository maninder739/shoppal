<div class="checkout-content">
  
  <div>

    <label class="control-label" for="input-member"><?php echo $member; ?></label>

    <?php if ($affiliates) {  ?>
        
        <select name="member" id="input-member" class="form-control">
        <option value=""> --- <?php echo $default_option; ?> --- </option>
        <?php 
        foreach ($affiliates as $affiliate) { ?>

          <option value="<?php echo  $affiliate['code'] ?>"><?php echo  $affiliate['lastname'].' '.$affiliate['firstname'] ?></option>
           
          <?php } ?>
        </select>
        <?php 
     } 
     else{
      echo '<select name="member" id="input-member" class="form-control" style="display:none"><option></option></select>';
     } ?>

  </div>
  
  <div class="buttons">
    <div class="pull-right">
      <input type="button" value="<?php echo $button_continue; ?>" id="button-choose-member" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary button" />
    </div>
  </div>
 
</div>