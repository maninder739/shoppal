<?php   echo $header; ?>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php echo $breadcrumb['href']; ?>" itemprop="url"><span itemprop="title"><?php echo $breadcrumb['text']; ?></span></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="row">

    <?php //echo $column_left; ?><?php //echo $column_right; ?>
    <?php //if ($column_left && $column_right) { ?>
    <?php //$class = 'col-sm-6'; ?>
    <?php //} elseif ($column_left || $column_right) { ?>
    <?php //$class = 'col-sm-9'; ?>
    <?php //} else { ?>
    <?php //$class = 'col-sm-12'; ?>
    <?php //} ?>
    <div id="content" class="<?php //echo $class; ?>">
      <?php echo $content_top; ?>
      <div class="row login-content">
        <!--<div class="col-sm-4 left">
          <div class="well">
            <h2 class="secondary-title"><?php echo $text_new_customer; ?></h2>
            <div class="login-wrap">
            <p><strong><?php echo $text_register; ?></strong></p>
            <p><?php echo $text_register_account_customer; ?></p>
            </div>
            <hr/>
            <a href="<?php echo $register; ?>" class="btn btn-primary button"><?php echo $button_continue; ?></a></div>
        </div>
        <div class="col-sm-4 right">
          <div class="well">
            <h2 class="secondary-title"><?php echo $text_returning_affiliate; ?></h2>
            <form action="<?php echo $action_affiliate; ?>" method="post" enctype="multipart/form-data">
              <div class="login-wrap">
                <p><?php echo $text_i_am_returning_affiliate; ?></p>
              <div class="form-group">
                <label class="control-label" for="input-email"><?php echo $entry_email_affiliate; ?></label>
                <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email_affiliate; ?>" id="input-email" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-password"><?php echo $entry_password; ?></label>
                <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
                <a href="<?php echo $forgotten_affiliate; ?>"><?php echo $text_forgotten; ?></a></div>
                </div>
              <hr/>
              <input type="submit" value="<?php echo $button_login; ?>" class="btn btn-primary button" />
              <?php if ($action_affiliate) { ?>
              <input type="hidden" name="action_affiliate" value="<?php echo $action_affiliate; ?>" />
              <?php } ?>
            </form>
          </div>
        </div>-->
		  
       


      <?php echo $content_bottom; ?>
</div>

<div class="col-sm-4 login-box">
	<div class="col-sm-12 login-form">
	<div class="well">
            <h2><?php echo $text_returning_customer; ?></h2>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
              <div class="login-wrap">
                <!--<p><?php echo $text_i_am_returning_customer; ?></p>-->
              <div class="form-group">
                <label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
                <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-password"><?php echo $entry_password; ?></label>
                <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
                </div>
				<div class="form-group">
					<a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
					<input type="submit" value="<?php echo $button_login; ?>" class="btn btn-primary button btn-sub" style=""/>
				</div>
			</div>
              
              <?php if ($action) { ?>
              <input type="hidden" name="action" value="<?php echo $action ; ?>" />
              <?php } ?>
            </form>
          </div>
	</div>
	<div class="col-sm-12 login-btn">
	<h2><?php echo $new_to_shoppal; ?></h2><br>
		  <a href="<?php echo $register; ?>" class="btn btn-primary button" style="text-decoration: none;float:left;"><?php echo $create_account; ?></a>
	</div>
</div>
<div class="login-detail-btn">
	<a href="<?php echo $action_affiliate ; ?>" class="btn btn-primary button" style=" margin-bottom:10px;"><?php echo $fundraise_member_login; ?></a><br><br><br><hr width="100%"><br><br><br>
		<a href="<?php echo $action_affiliate; ?>" class="btn btn-primary button"><?php echo $fundraise_speciality_login; ?></a><br><br><br><br><br><br>
</div>






<?php echo $footer; ?>
<style type="text/css">
.login-form{ float:left;}
.login-box{ width: 40%; float: left; padding: 10px; margin-left: 100px;}
.login-form{background-color: #f0f0f0; padding: 15px;}
.login-btn h3{ display:inline-block; margin-bottom:10px;}
.login-btn a{display: inline-block;float: right;}
.login-btn{ padding: 15px; width: 100%; float: left;}
.btn-sub{float:right;}
.login-detail-btn{ width:40%; float:left; text-align:center; margin-top:100px;}

.login-content > div{ width: 32%; position: relative; padding: 15px; min-height: 345px; margin-right: 15px;}
.login-content .login-wrap{ min-height:auto;}
  
  

</style>