<?php echo $header; ?>
<div id="container" class="container j-container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php echo $breadcrumb['href']; ?>" itemprop="url"><span itemprop="title"><?php echo $breadcrumb['text']; ?></span></a></li>
        <?php } ?>
    </ul>
    <?php if ($success) { ?>
        <div class="alert alert-success success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
    <?php } ?>
    <div class="row">
        <div id="content" class='col-md-9 main-text-box'  >
            <?php echo $content_top; ?>
            <h2 class="secondary-title"><?php echo $text_my_account; ?>&nbsp;(<?php echo isset($first_name) ? ($first_name) : '' ?>&nbsp;<?php echo isset($last_name) ? ($last_name) : ''; ?>)</h2>
            <div class="content my-account">
                <ul class="list-unstyled">
                    <li><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
                    <li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
                    <li><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></li>
                    <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
                    <li><a href="<?php echo $credit_card_info; ?>"><?php echo $text_credit_card_info; ?></a></li>
                </ul>
            </div>
            <?php if (isset($credit_cards) && $credit_cards) { ?>
                <h2><?php echo $text_credit_card; ?></h2>
                <ul class="list-unstyled">
                    <?php foreach ($credit_cards as $credit_card) { ?>
                        <li><a href="<?php echo $credit_card['href']; ?>"><?php echo $credit_card['name']; ?></a></li>
                    <?php } ?>
                </ul>
            <?php } ?>
            <h2 class="secondary-title"><?php echo $text_my_orders; ?></h2>
            <div class="content my-orders">
                <ul class="list-unstyled">
                    <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                    <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
                    <?php if ($reward) { ?>
                        <li><a href="<?php echo $reward; ?>"><?php echo $text_reward; ?></a></li>
                    <?php } ?>
                    <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
                    <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
                    <li><a href="<?php echo $recurring; ?>"><?php echo $text_recurring; ?></a></li>
                </ul>
            </div>
            <h2 class="secondary-title"><?php echo $text_my_newsletter; ?></h2>
            <div class="content my-newsletter">
                <ul class="list-unstyled">
                    <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-3 side-box">
            <!-- <img src="./pics/catalog/SiteImages/MainSite/DigitalHotLinkENG.png"> -->
        </div>
        <?php echo $content_bottom; ?>
    
    </div>
</div>
<?php echo $footer; ?>
 
<style type="text/css">
    .main-text-box{width: 80%; float: left;}
    .side-box{width: 20%; float: right;}
    .side-box img{ width: 100%; margin-top: 60px }
</style>