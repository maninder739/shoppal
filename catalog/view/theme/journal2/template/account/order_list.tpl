<?php echo $header; ?>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php echo $breadcrumb['href']; ?>" itemprop="url"><span itemprop="title"><?php echo $breadcrumb['text']; ?></span></a></li>
	<?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?><?php echo $column_right; ?>
	<?php if ($column_left && $column_right) { ?>
	<?php $class = 'col-sm-6'; ?>
	<?php } elseif ($column_left || $column_right) { ?>
	<?php $class = 'col-sm-9'; ?>
	<?php } else { ?>
	<?php $class = 'col-sm-12'; ?>
	<?php } ?>
	  
	<div id="content" class="<?php echo $class; ?> order-list">
	  <h1 class="heading-title"><?php echo $heading_title; ?></h1>
	  <?php echo $content_top; ?>
	  <?php if ($orders) { ?>
	  <div class="table-responsive">
		<table class="table table-bordered table-hover list">
		  <thead>
			<tr>
			  <td class="text-right"><?php echo $column_order_id; ?></td>
			   <td class="text-right"><?php echo $order_type; ?></td>
			  <td class="text-left"><?php echo $column_status; ?></td>
			  <td class="text-left"><?php echo $column_date_added; ?></td>
			 <!--  <td class="text-right"><?php echo $column_product; ?></td> -->
			  <td class="text-left"><?php echo $column_customer; ?></td>
			  <td class="text-right"><?php echo $column_total; ?></td>
			  <td></td>
			</tr>
		  </thead>
		  <tbody>
			<?php $i=0;?>
			<?php foreach ($orders as $order) {?>
			<tr>
			  <td class="text-right">#<?php echo $order['order_id']; ?></td>
			  <td class="text-right">
				<?php 
				$order_type = $order['order_type'];
				if($order['order_type'] == 'monthly')
				{
				  $order_type = 'Monthly';
				}
				else if($order_type== 'one time')
				{
					$order_type = 'One Time';
				}
				echo $order_type;
				?>
			  </td>
			  <td class="text-left"><?php echo $order['status']; ?></td>
			  <td class="text-left"><?php echo $order['date_added']; ?></td>
			  <!-- <td class="text-right"><?php echo $order['products']; ?></td> -->
			  <td class="text-left"><?php echo $order['name']; ?></td>
			  <td class="text-right"><?php echo $order['total']; ?></td>
			  <td class="text-right">
				<?php

				$next_date_intial = date('Y-m-d',strtotime('+1 month', strtotime($order['order_added'])));
				$current_date_intial = date('Y-m-d');
				$next_date =  new DateTime($next_date_intial);
				$current_date = new DateTime($current_date_intial);
				$interval = $current_date->diff($next_date);
				$days_left = $interval->days;
				if(  $order['order_type'] == 'monthly' && $days_left > '2' && $current_date <= $next_date)
				{
				?>
					<a href="<?php echo $order[version_compare(VERSION, '2.2', '<') ? 'href' : 'edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-info btn-primary"><i class="fa fa-pencil"></i></a>
				<?php 
				}?>
				<a href="<?php echo $order[version_compare(VERSION, '2.2', '<') ? 'href' : 'view']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-info btn-primary"><i class="fa fa-eye"></i></a></td>
			</tr>
			<?php if($order['order_type'] == 'monthly') {$i++;} } ?>
		  </tbody>
		</table>
	  </div>
	  <?php if (version_compare(VERSION, '2.3', '>=')): ?>
	  <div class="row">
		<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
		<div class="col-sm-6 text-right"><?php echo $results; ?></div>
	  </div>
	  <?php else: ?>
	  <div class="text-right"><?php echo $pagination; ?></div>
	  <?php endif; ?>
	  <?php } else { ?>
	  <p><?php echo $text_empty; ?></p>
	  <?php } ?>
	  <div class="buttons">
		<div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary button"><?php echo $button_continue; ?></a></div>
	  </div>
	  <?php echo $content_bottom; ?></div>
	</div>
</div>
<?php echo $footer; ?>
