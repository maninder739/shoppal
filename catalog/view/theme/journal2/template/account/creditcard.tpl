<?php ob_start(); echo $header; ?>
<div id="container" class="container j-container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php echo $breadcrumb['href']; ?>" itemprop="url"><span itemprop="title"><?php echo $breadcrumb['text']; ?></span></a></li>
        <?php } ?>
    </ul>
    <?php if ($warning) { ?>
    <div class="alert alert-danger warning"><i class="fa fa-exclamation-circle"></i> <?php echo $warning; ?></div>
    <?php } ?>
    <div id="msg" ></div>
    <div class="row"><?php echo $column_left; ?><?php echo $column_right; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>">
                          
             <h1 class="heading-title"> <span class="pull-left"><?php echo $heading_title; ?> </span> <span class="pull-right" style="padding-right: 15px;"><?php echo $last_update_on . ' : ' . ($creditCardStatus) ; ?></span></h1>
            <div class="content">
                
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <fieldset>
                        <h2 class="secondary-title"><?php echo $text_your_details; ?></h2>
                        
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-firstname"><?php echo $entry_cardNumber; ?> </label>
                            <div class="col-sm-10">
                                <input type="text" name="cardnumber" value="<?php echo $cardnumber; ?>" placeholder="<?php echo $entry_cardNumber; ?>" id="cardNumber" class="form-control" autocomplete="off" required />
                                <em style="font-size: 12px">Enter the number without spacing or hyphens</em>
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-cardtype"><?php echo $entry_cardType; ?></label>
                            <div class="col-sm-10">
                                <select class="form-control" id="cardtype" name="cardType"  required>
                                    <?php foreach($card_types as $key => $val) { ?>
                                    <option value="<?php echo $key ?>"><?php echo $val; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_cardExpiry; ?></label>
                            <div class="col-sm-5">
                                <select class="form-control" name="expiry_month" id="expiry-month" autocomplete="off" required>
                                    <option value="">Month</option>
                                    <?php
                                    for ($m=1; $m<=12; $m++) {
                                    if($m < 10) {
                                        $m = '0'.$m;
                                    }
                                    ?>
                                    <option value="<?php echo $m; ?>"><?php echo $m; ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                                <?php if ($error_email) { ?>
                                <div class="text-danger"><?php echo $error_email; ?></div>
                                <?php } ?>
                                <select class="form-control" name="expiry-year" autocomplete="off" required>
                                    <?php
                                    for ($y=2017; $y<=2035; $y++) { ?>
                                        <option value="<?php echo $y; ?>"><?php echo $y; ?></option>
                                    <?php }
                                    ?>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-fax"><?php echo $entry_cardHolderName; ?></label>
                            <div class="col-sm-10">
                                <input type="text" name="cardHolderName" value="<?php echo $cardHolderName; ?>" placeholder="<?php echo $entry_cardHolderName; ?>" id="input-fax" class="form-control" autocomplete="off" required/>
                            </div>
                        </div>
                    </fieldset>

                    <div class="buttons">
                        <div class="pull-left">
                            <!-- <a href="<?php echo $back; ?>" class="btn btn-default button"><?php echo $button_back; ?></a> -->
                            <!-- <a href="<?php echo $button_autoship; ?>" class="btn btn-danger"><?php echo $button_autoship; ?></a>  -->
                            <?php if(!empty($total_denied_status)) { ?>
                            <button type="button" class="btn btn-primary autoshipBtn" data-toggle="modal" data-target="#myModal"><?php echo $button_autoship; ?></button>
                            <?php } ?>
                        </div>
                        <div class="pull-right">
                            <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary button" />
                        </div>
                    </div>

                </form>
            </div>
        <?php echo $content_bottom; ?></div>
    </div>
</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Denied orders</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to start auto-ship for denied order?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        <button type="button" id="startAutoship" class="btn btn-primary" data-dismiss="modal">Yes</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $('#startAutoship').click(function () { 
        $.ajax({                                      
            url: 'index.php?route=account/creditcard/CheckDeniedOrder',              
            type: "post",         
            dataType: 'json',          
            success: function(data){
                console.log(data);
                var htmlData = '<div class="alert alert-danger warning">'+data.msg+'</div>';
                $('#msg').show().html(htmlData); 
                if(data.denied_status == 1) {
                     $(".autoshipBtn").hide();
                }  
            }
        });
    });  
</script>
<?php echo $footer; ob_end_flush();?>