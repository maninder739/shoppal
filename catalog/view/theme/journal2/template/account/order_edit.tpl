<?php echo $header; ?>
<div id="container" class="container j-container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php echo $breadcrumb['href']; ?>" itemprop="url"><span itemprop="title"><?php echo $breadcrumb['text']; ?></span></a></li>
        <?php } ?>
    </ul>
    <?php if ($success) { ?>
    <div class="alert alert-success success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="row">
        <?php echo $column_left; ?><?php echo $column_right; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>">
            <h1 class="heading-title"> <span class="pull-left"><?php echo $heading_title; ?> </span> <span class="pull-right" style="padding-right: 15px;"><?php echo $get_order_type; ?></span></h1>
            <?php echo $content_top; ?>
            <table class="table table-bordered table-hover list">
                <thead>
                    <tr>
                        <td class="text-left" colspan="2"><?php echo $text_order_detail; ?></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-left" style="width: 50%;"><?php if ($invoice_no) { ?>
                            <b><?php echo $text_invoice_no; ?></b> <?php echo $invoice_no; ?><br />
                            <?php } ?>
                            <b><?php echo $text_order_id; ?></b> #<?php echo $order_id; ?><br />
                            <b><?php echo $text_date_added; ?></b> <?php echo $date_added; ?><br />
                            <b><?php echo $org_name; ?></b> <?php echo $company_name; ?></td>
                        </td>
                        <td class="text-left"><?php if ($payment_method) { ?>
                            <b><?php echo $text_payment_method; ?></b> <?php echo $payment_method; ?><br />
                            <?php } ?>
                            <?php if ($shipping_method) { ?>
                            <b><?php echo $text_shipping_method; ?></b> <?php echo $shipping_method; ?>
                            <?php } ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-bordered table-hover list">
                <thead>
                    <tr>
                        <td class="text-left" style="width: 50%;"><?php echo $text_payment_address; ?></td>
                        <?php if ($shipping_address) { ?>
                        <td class="text-left" ><?php echo $text_shipping_address; ?></td>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-left"><?php echo $payment_address; ?></td>
                        <?php if ($shipping_address) { ?>
                        <td class="text-left" id="shipping-address"><?php echo $shipping_address; ?><br><br>  <?php if(!empty($behalf_of) ||  !empty($remain_anonymous) ) {  
                            echo "<b>" . $text_local_dog_shelter . "</b><br>";   
                        }?>
                        <?php if(!empty($behalf_of)) { 
                            echo  $text_on_behalf_of . '&nbsp;<b>' . $name  . "</b><br>"; 
                         } ?>
                         <?php if(!empty($remain_anonymous)) {  
                            echo $text_remain_anonymous ;
                    } ?>    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModal" style="margin-left: 25%"><i class="fa fa-pencil" ></i></button></td>

                        <?php } ?>
                    </tr>

                </tbody>
            </table>
            <div class="table-responsive">
                <table class="table table-bordered table-hover list" id="product_table">
                    <thead>
                        <tr>
                            <td class="text-left"><?php echo $column_name; ?></td>
                            <td class="text-left"><?php echo $column_model; ?></td>
                            <td class="text-right"><?php echo $column_quantity; ?></td>
                            <td class="text-right"><?php echo $column_price; ?></td>
                            <td class="text-right"><?php echo $column_total; ?></td>
                            <?php if ($products) { ?>
                            <td style="width: 20px;"></td>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <script type="text/javascript">
                             optionIds = [];
                        </script>
                        <?php foreach ($products as $product) {
                         ?>
                        <tr id="row-<?php echo $product['product_id'];?>">
                            <td class="text-left"><?php echo $product['name']; ?>
                                <?php foreach ($product['option'] as $option) { ?>
                                <br />
                                &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                                  <script type="text/javascript">
                                     
                                    optionIds.push({option_id : "<?php echo $option['product_option_id'] ?>", product_option_value_id: "<?php echo $option['product_option_value_id'] ?>" , product_id : "<?php echo $product['product_id']; ?>" });
                                    console.log(optionIds);
                                </script>
                                <?php } ?>
                              
                            </td>
                            <td class="text-left"><?php echo $product['model']; ?></td>
                            <td class="text-right"><?php echo $product['quantity']; ?></td>
                            <td class="text-right"><?php echo $product['price']; ?></td>
                            <td class="text-right"><?php echo $product['total']; ?></td>
                            <td class="text-right" style="white-space: nowrap;"> 
                                
                                <a href="javascript:void(0)" data-toggle="tooltip" title="<?php echo $button_reorder; ?>" onclick="addToCart(<?php echo $product['product_id']; ?>,<?php echo $product['quantity']; ?>);" class="btn btn-primary cart-items" style="display:none;"><i class="fa fa-shopping-cart"></i></a>

                                <a href="javascript:void(0)" data="<?php echo $product['product_id']; ?>" dataProductlist ="<?php echo $product['order_product_id']; ?>" data-toggle="tooltip" dataquantity="<?php echo $product['quantity']; ?>" datamodel="<?php echo $product['model']; ?>" dataprice="<?php echo $product['price']; ?>" dataFormat="0" value="<?php echo $product['name']; ?>" title="<?php echo $button_edit; ?>" class="btn btn-primary" onclick="edit_product(event)"><i class="fa fa-pencil" ></i></a>

                                <a href="javascript:void(0)" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="remove_product('<?php echo $product['product_id'];?>','1')"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
             
            <div class="buttons" id="continue-div">
                <div class="pull-left">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"><?php echo $text_product; ?></button>
                     <button type="button" onclick="stop_autoship('<?php echo $order_id; ?>')" id='btnDis' class="btn btn-danger" ><?php echo $stop_autoship ?></button>  
                </div>
                <div class="pull-right">
                    <button type="button" id="buttoncontinue" class="btn btn-primary" ><?php echo $button_continue; ?></button>
                </div>
            </div>
            <?php echo $content_bottom; ?>
        </div>
    </div>
</div>

<div id="addModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width: 800px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="success">Edit Shipping Address</h4>
        <div id="success"></div>
      </div>
      <div class="modal-body">
       <form class="form-inline" id="addressChange">
        <fieldset>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-firstname"><?php echo $entry_firstname; ?></label>
            <div class="col-sm-10">
              <input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control" />
              <?php if ($error_firstname) { ?>
              <div class="text-danger"><?php echo $error_firstname; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-lastname"><?php echo $entry_lastname; ?></label>
            <div class="col-sm-10">
              <input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control" />
              <?php if ($error_lastname) { ?>
              <div class="text-danger"><?php echo $error_lastname; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-company"><?php echo $entry_company; ?></label>
            <div class="col-sm-10">
              <input type="text" name="company" value="<?php echo $company; ?>" placeholder="<?php echo $entry_company; ?>" id="input-company" class="form-control" />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-address-1"><?php echo $entry_address_1; ?></label>
            <div class="col-sm-10">
              <input type="text" name="address_1" value="<?php echo $address_1; ?>" placeholder="<?php echo $entry_address_1; ?>" id="input-address-1" class="form-control" />
              <?php if ($error_address_1) { ?>
              <div class="text-danger"><?php echo $error_address_1; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-address-2"><?php echo $entry_address_2; ?></label>
            <div class="col-sm-10">
              <input type="text" name="address_2" value="<?php echo $address_2; ?>" placeholder="<?php echo $entry_address_2; ?>" id="input-address-2" class="form-control" />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-city"><?php echo $entry_city; ?></label>
            <div class="col-sm-10">
              <input type="text" name="city" value="<?php echo $city; ?>" placeholder="<?php echo $entry_city; ?>" id="input-city" class="form-control" />
              <?php if ($error_city) { ?>
              <div class="text-danger"><?php echo $error_city; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-postcode"><?php echo $entry_postcode; ?></label>
            <div class="col-sm-10">
              <input type="text" name="postcode" value="<?php echo $postcode; ?>" placeholder="<?php echo $entry_postcode; ?>" id="input-postcode" class="form-control" />
              <?php if ($error_postcode) { ?>
              <div class="text-danger"><?php echo $error_postcode; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-country"><?php echo $entry_country; ?></label>
            <div class="col-sm-10">
              <select name="country_id" id="input-country" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($countries as $country) { ?>
                <?php if ($country['country_id'] == $country_id) { ?>
                <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
              <?php if ($error_country) { ?>
              <div class="text-danger"><?php echo $error_country; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-zone"><?php echo $entry_zone; ?></label>
            <div class="col-sm-10">
              <select name="zone_id" id="input-zone" class="form-control">
              </select>
              <?php if ($error_zone) { ?>
              <div class="text-danger"><?php echo $error_zone; ?></div>
              <?php } ?>
            </div>
          </div>
        </fieldset>
        <div class="buttons">
          <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default button"><?php echo $button_back; ?></a></div>
          <div class="pull-right">
            <input type="submit" value="<?php echo $button_save; ?>" class="btn btn-primary"   />
          </div>
        </div>
      </form>
      </div>
     <!--  <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
    </div>

  </div>
</div>
 
<!-- create product  -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog" style="width: 800px;">
        <!-- Modal content-->
        <div class="modal-content" style="">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                    <?php echo $add_edit_product ; ?>
                </h4>
            </div>
            <div id="msg2" style="margin-left:15px; color:red;"></div>
            <div class="modal-body productadd">
                <fieldset>
                    <div class="form-group" >
                        <label for="" class="col-sm-2 control-label" style="width: 20%;">Shop for a Cause</label>
                        <div class="col-sm-10" id="sel1"></div>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-product"><?php echo $entry_product; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="product" value="" id="input-product" class="form-control" />
                        </div>
                    </div>
                    <div class="headingname"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-quantity"><?php echo $entry_quantity; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="quantity" value="" id="input-quantity" class="form-control" />
                        </div>
                    </div>
                    <div id="option">
                        <input type="hidden" name="product_id" class="productid" value="" />
                        <input type="hidden" name="product_price" value="" />
                        <input type="hidden" name="product_model" value="" />
                        <input type="hidden" name="action" value=""  id="actionid" />
                        <input type="hidden" name="old_product_id" value="" />
                        <input type="hidden" name="Format_price" value="" />
                    </div>
                </fieldset>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"> <?php echo $close ; ?></button>
                <button type="button" id="addProduct" class="btn btn-primary" data-dismiss="modal"><?php echo $save ; ?></button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var optionData = [];

    function remove_product(product_id,type) {
        console.log(product_id);
        if(product_id) {
            var x = confirm("Are you sure you want to delete?");
            if(x) {
                var rowCount = $('#product_table tbody tr').length;
                var row_id = 'row';
                $('#'+row_id+'-'+product_id).remove();
            } else {
                return false;
            }
        }
    }
    
    function edit_product(event) {
         
        $('input[name=\'product\']').val(event.currentTarget.attributes.value.value);
        $('input[name=\'product_id\']').val(event.currentTarget.attributes.data.nodeValue);
        $('input[name=\'quantity\']').val(event.currentTarget.attributes.dataquantity.nodeValue);
        $('input[name=\'product_price\']').val(event.currentTarget.attributes.dataprice.nodeValue);
        $('input[name=\'Format_price\']').val(event.currentTarget.attributes.dataFormat.nodeValue);
        $('input[name=\'product_model\']').val(event.currentTarget.attributes.datamodel.nodeValue);
        $('input[name=\'old_product_id\']').val(event.currentTarget.attributes.data.nodeValue);
        $('input[name=\'action\']').val(1);

        var format = event.currentTarget.attributes.dataprice.nodeValue;

        if(!event.currentTarget.attributes.dataProductlist){
           
            $("#product").val("");  
             
        } else {
        
           $("#product").val(event.currentTarget.attributes.dataProductlist.nodeValue); 
           var Myproductid = event.currentTarget.attributes.data.nodeValue;
          
               $.ajax({                                      
                url: 'index.php?route=account/order/getProduct&product_id='+Myproductid,              
                type: "GET",         
                dataType: 'json',                
                success: function(data){

                    $('.formatshow').html('');
                    $('.format').html('');
                    if(data.options.length){
                      
                        for(var i in data.options) {
                            var output = '';
                            for(var j in data.options[i].product_option_value) {
                                
                                var price = '';
                                var val = data.options[i].product_option_value[j].name;

                                if(data.options[i].product_option_value[j].price){
                                    price = data.options[i].product_option_value[j].price;
                                    val = price;
                                }

                                var checked = '';
                                for(let option of optionData){
                                    if(option.name == data.options[i].name && val == option.selected ){
                                        checked = 'checked';
                                    }
                                }

                                output += "<label class='radio-inline'><input type='radio' class='radioselected' name='"+data.options[i].name+"' value='"+val+"' "+checked+" >"+ data.options[i].product_option_value[j].name +  "</label>"

                               
                            }
                            
                                optionData.push({name : data.options[i].name, selected : ''});

                            $('.headingname').after("<div class='form-group formatshow' style='display:block'><label class='col-sm-2 control-label' style='width: 20%;'>"+data.options[i].name+"</label><div class='col-sm-10 format' style='display:block'></div>"+output+"</div>")
                        }


                    }   
                }
            });
        }
        $('#myModal').modal('show');
    }
     
     
</script>

<script type="text/javascript">
//optionIds = [];
function getval(sel) {
    $('input[name=\'product\']').val("");

    $('input[name=\'product\']').autocomplete({
    'source': function(request, response) {
    $.ajax({
        url: 'index.php?route=account/order/autocomplete&filter_name=' + encodeURIComponent(request) + '&id=' + encodeURIComponent(sel.value) ,
        dataType: 'json',
        success: function(json) {
        json.unshift({
            product_id: 0,
            name: '<?php echo $text_none; ?>'
        });
    
        response($.map(json, function(item) {
            return {
                label: item['name'],
                value: item['product_id'],
                data: item['price'],
                model: item['model'],
            }
        }));
      }
    });
    },
    'select': function(item) {
        $('input[name=\'product\']').val(item['label']);
        $('input[name=\'product_id\']').val(item['value']);
        $('input[name=\'product_price\']').val(item['data']);
        $('input[name=\'product_model\']').val(item['model']);

        $.ajax({                                      
            url: 'index.php?route=account/order/getProduct&product_id='+item['value'],              
            type: "GET",         
            dataType: 'json',                
            success: function(data){
                $('.formatshow').html('');
                $('.format').html('');

                optionData = [];
                if(data.options.length){
                    for(var i in data.options) {
                        var output = '';
                        for(var j in data.options[i].product_option_value) {
                            var price = '';
                            var val = data.options[i].product_option_value[j].name;
                            if(data.options[i].product_option_value[j].price){
                                price = data.options[i].product_option_value[j].price;
                                val = price;
                            }
                            output += "<label class='radio-inline'><input type='radio' class='radioselected' name='"+data.options[i].name+"' value='"+val+"' checked>"+ data.options[i].product_option_value[j].name + "</label>";


                          optionIds.push({option_id : data.options[i].product_option_id, product_option_value_id: data.options[i].product_option_value[j].product_option_value_id , product_id : item['value'] });
                            
                        }

                        optionData.push({name : data.options[i].name, selected : ''});

                        $('.headingname').after("<div class='form-group formatshow' style='display:block'><label class='control-label' style=''>"+data.options[i].name+"</label><div class='col-sm-10 format' style='display:block'></div>"+output+"</div>");
                        
                       
                    }

                    //console.log('hete', optionIds);
                }
            }
        });
    }});
}
    
    $('#addProduct').click(function () {   
        
        optionData.forEach(function(element) {
            var radioValue = $("input[name='"+element.name+"']:checked").val();
            if(radioValue){
                element.selected = radioValue;
            }
        }); 


        var action = $('input[name=action]').val().trim(); 
        var addprice = $("input[name='Format']:checked").val();
        if(addprice){
            var ReplacedAmount = parseFloat(addprice.replace(/\$/g,''));    
        } else {
            var ReplacedAmount = 0; 
        }  
        var product_id = $('input[name=product_id]').val().trim();  
        var old_product_id = $('input[name=old_product_id]').val().trim();  
        var product_model = $('input[name=product_model]').val().trim();  
        var product_name = $('input[name=product]').val().trim();
        var product_quantity =  parseFloat($('input[name=quantity]').val().trim());
        var product_price =  parseFloat($('input[name=product_price]').val().trim());
        var store_price = parseFloat($('input[name=product_price]').val().trim());
        if(product_price == 0 ){

        }
       

        var product_total = (product_quantity * (+product_price +  +ReplacedAmount)).toFixed(2); 
        var productlist = $('select[name="productlist"]').val().trim(); 

        if(!product_name || !product_quantity || !product_id || !product_model || !productlist) {
          $('#msg2').show().html('Product type, name and quantity is required.');
          return false;
        }

        if (isNaN(product_quantity) || product_quantity < 1) {
            $('#msg2').show().html('Quantity should be a number.');
            return false
        }  

       
         
        product_price = (product_price + ReplacedAmount).toFixed(2);

        $('#modal-content').modal('hide');
        if(action == 1 && old_product_id) {
            $('#row-'+old_product_id).remove();
        }


        
        $('#product_table tbody').append('<tr id="row-'+product_id+'">'
        +'<td class="text-left">'+product_name+'</td>'
        +'<td class="text-left">'+product_model+'</td>'
        +'<td class="text-right">'+product_quantity+'</td>'
        +'<td class="text-right">'+product_price+'</td>'
         +'<td class="text-right">'+product_total+'</td>'
        +'<td class="text-right" style="white-space: nowrap;">'
             + '<?php if ($product["reorder"]) { ?><a href="javascript:void(0)" data-toggle="tooltip" title="<?php echo $button_reorder; ?>" class="btn btn-primary cart-items" onclick="addToCart('+product_id+','+product_quantity+');" style="display:none;"><i class="fa fa-shopping-cart"></i></a><?php }?>'       

             +'<a href="javascript:void(0)"  data="'+product_id+'"  dataProductlist ="'+productlist+'" datamodel="'+product_model+'"  dataquantity="'+product_quantity+'" dataprice="'+store_price+'" dataFormat="'+addprice+'" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary" value="'+product_name+'" onclick="edit_product(event)"><i class="fa fa-pencil"></i></a>&nbsp;'

             +'<a href="javascript:void(0)" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="remove_product('+product_id+',1)"> <i class="fa fa-times"></i></a>&nbsp;'

        +'</td>'
        +'</tr>');
    
        $(".modal-body input[name=product]").val("")
        $('#msg2').html('');
    });
    
    
    $('#myModal').on('hidden.bs.modal', function() {
       
      $(".modal-body input").val("");
      $(".modal-body select").val("");
      $(".modal-body radio").val("");
      $('.format').hide(); 
      $('.formatshow').hide();
      $('#msg2').html('');
    });

    $( document ).ready(function() {
        $.ajax({                                      
            url: 'index.php?route=account/order/productlist',              
            type: "GET",         
            dataType: 'json',                
            success: function(data){
                var output= "<select class='form-control select-product' name='productlist' id='product' onchange='getval(this);'>";
                output += "<option value= ''>" + '<?php echo $text_select  ?>' + "</option>";
                for (i=0; i < data.length; i++) { 
                    output += "<option value= '"+data[i].category_id+"'>" + data[i].name + "</option>";
                }
                output += "</select>";
                $('#sel1').append(output);
            }
        });

        $('#buttoncontinue').click(function(){
            $('.cart-items').trigger('click');
            window.setTimeout(function () {
                 location.href = "index.php?route=checkout/cart&order_id=<?php echo $order_id; ?>";
            }, 1000);
        });
    });
 
 $('.cart, #cart').hide();
</script>
<script type="text/javascript">
    function stop_autoship(order_id) {
        console.log(order_id);
        if(order_id) {
            var x = confirm("Are you sure you want to stop autoship?");
             if(x){
                $.ajax({
                    url: 'index.php?route=account/order/stopAutoship&tracking=<?php echo $_COOKIE["tracking"] ?>',           
                    data: 'order_id=' + order_id,
                    dataType: 'json',
                    type: 'POST',       
                    success: function(data){
                    // var htmlData = '<div class="alert alert-success success" style="margin-top: 9px;">'+data.msg+'</div>';
                     
                    //  $('#msg').show().html(htmlData);
                       //document.getElementById("btnDis").disabled = true;
                       // $("#btnDis").css({ 'pointer-events': 'none' });
                       // $('#order_type').show().html('Monthly Stopped');
                        window.location = "index.php?route=account/order";

                    }
                });
            } else {
                return false;
            }

        }
    }
</script>
<script type="text/javascript"><!--
// Sort the custom fields
$('.form-group[data-sort]').detach().each(function() {
  <?php if (version_compare(VERSION, '2.2', '<')): ?>
    if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('.form-group').length) {
        $('.form-group').eq($(this).attr('data-sort')).before(this);
    } 
    
    if ($(this).attr('data-sort') > $('.form-group').length) {
        $('.form-group:last').after(this);
    }
        
    if ($(this).attr('data-sort') < -$('.form-group').length) {
        $('.form-group:first').before(this);
    }
  <?php else: ?>
  if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('.form-group').length-2) {
    $('.form-group').eq(parseInt($(this).attr('data-sort'))+2).before(this);
  }

  if ($(this).attr('data-sort') > $('.form-group').length-2) {
    $('.form-group:last').after(this);
  }

  if ($(this).attr('data-sort') == $('.form-group').length-2) {
    $('.form-group:last').after(this);
  }

  if ($(this).attr('data-sort') < -$('.form-group').length-2) {
    $('.form-group:first').before(this);
  }
  <?php endif; ?>
});
//--></script>
<script type="text/javascript"><!--
$('button[id^=\'button-custom-field\']').on('click', function() {
    var node = this;

    $('#form-upload').remove();

    $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

    $('#form-upload input[name=\'file\']').trigger('click');

  if (typeof timer != 'undefined') {
      clearInterval(timer);
  }

    timer = setInterval(function() {
        if ($('#form-upload input[name=\'file\']').val() != '') {
            clearInterval(timer);
        
            $.ajax({
                url: 'index.php?route=tool/upload',
                type: 'post',
                dataType: 'json',
                data: new FormData($('#form-upload')[0]),
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $(node).button('loading');
                },
                complete: function() {
                    $(node).button('reset');
                },
                success: function(json) {
                    $(node).parent().find('.text-danger').remove();
                    
                    if (json['error']) {
                        $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                    }
    
                    if (json['success']) {
                        alert(json['success']);
    
                        $(node).parent().find('input').attr('value', json['code']);
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
    }, 500);
});
//--></script>
 
<script type="text/javascript"><!--
$('select[name=\'country_id\']').on('change', function() {
    $.ajax({
        url: 'index.php?route=account/account/country&country_id=' + this.value,
        dataType: 'json',
        beforeSend: function() {
            $('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
        },
        complete: function() {
            $('.fa-spin').remove();
        },
        success: function(json) {
            $('.fa-spin').remove();
    
            if (json['postcode_required'] == '1') {
                $('input[name=\'postcode\']').parent().parent().addClass('required');
            } else {
                $('input[name=\'postcode\']').parent().parent().removeClass('required');
            }
    
            html = '<option value=""><?php echo $text_select; ?></option>';
    
            if (json['zone'] && json['zone'] != '') {
                for (i = 0; i < json['zone'].length; i++) {
                    html += '<option value="' + json['zone'][i]['zone_id'] + '"';
    
                    if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
                        html += ' selected="selected"';
                    }
    
                    html += '>' + json['zone'][i]['name'] + '</option>';
                }
            } else {
                html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
            }
    
            $('select[name=\'zone_id\']').html(html);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});

$('select[name=\'country_id\']').trigger('change');

//address change ajax

$("#addressChange").submit(function(e) {
    var form = $(this);
    $.ajax({
        url: 'index.php?route=account/address/saveAddress&order_id=<?php echo $order_id; ?>&tracking=<?php echo $_COOKIE["tracking"] ?>',
        type: 'POST',
        data: form.serialize(), 
        dataType: 'JSON',     
        
        success: function(json){
            $('.text-danger').remove();
            $('#success').remove();
            if (json.success) {
              $('#success').after('<div class="alert alert-success success" style="width:93%; margin-top:18px">' + 'Address save successfully' + '</div>'); 
                 
                window.location = "index.php?route=account/order";
            } else {
                $.each( json.error, function( key, value ) {
                    $('#input-'+ key).after('<div class="text-danger">' + value + '</div>');
                });
            }
        }
    });
    e.preventDefault(); 
});
//--></script>
<style type="text/css">
    .formatshow .radioselected{ width: auto !important; float: left; margin-left: 25px; }
    .control-label { padding-right: 0px !important;}
    .form-control{ width: 95% !important; }
</style>


<?php echo $footer; ?>
