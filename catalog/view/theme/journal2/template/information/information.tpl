<?php echo $header; ?>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php echo $breadcrumb['href'] ?>" itemprop="url"><span itemprop="title"><?php echo $breadcrumb['text']; ?></span></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?><?php echo $column_right; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
     <!--  <h1 class="heading-title"><?php echo $heading_title; ?></h1> -->
      <?php echo $content_top; ?>
      <?php echo $description; ?>
      <?php echo $content_bottom; ?>


      <div style="text-align:center;">
        <a class="btn-primary button" href="<?php echo $register_action ?>"><?php echo $start_fundraising;?></a>
        <a class="btn-primary button" href="<?php echo $category_action  ?>"><?php echo $start_shopping;?></a>
      </div>


    </div>
    </div>
</div>
<?php echo $footer; ?> 
