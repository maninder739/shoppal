<?php echo $header; ?>
<div id="container" class="container j-container not-found-page">
  <ul class="breadcrumb">
    <?php if (isset($breadcrumbs) && is_array($breadcrumbs)): ?>
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php echo $breadcrumb['href']; ?>" itemprop="url"><span itemprop="title"><?php echo $breadcrumb['text']; ?></span></a></li>
    <?php } ?>
    <?php endif; ?>
  </ul>
  <div class="row"><?php echo $column_left; ?><?php echo $column_right; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <h1 class="heading-title"><?php echo $heading_title; ?></h1>
      <?php echo $content_top; ?>
      <p><?php echo $text_error; ?></p>
	  <p>
    <?php
    if(isset($link1) && isset($link2))
    {?>
		<ul>
		   <li><a href="http://sp.shoppalweb.ca/index.php?route=product/category&path=195"><?php echo $link1; ?>
          </a>
      </li>
		   <li><a href="http://sp.shoppalweb.ca/index.php?route=information/information&information_id=9"> 
              <?php echo $link2; ?>
          </a>
      </li>
    </ul>
    <?php
    }
    ?>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary button"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    </div>
</div>
<?php echo $footer; ?>
