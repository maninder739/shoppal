<?php echo $header;
if(!empty($get_errors)){ ?>
<script type="text/javascript">
	$(window).load(function() {
		$('#error_form').modal('show');
	});
</script>
<?php }?>
<div id="container" class="container j-container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php echo $breadcrumb['href'] ;  ?>" itemprop="url"><span itemprop="title"><?php echo $breadcrumb['text']; ?></span></a></li>
        <?php } ?>
    </ul>
    <?php if ($error_warning) { ?>
        <div class="alert alert-danger warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
    <?php } ?>
    <div class="row"><?php echo $column_left; ?><?php echo $column_right; ?>
        <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
            <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>">
            <h1 class="heading-title"><?php echo $heading_title; ?></h1>
            <?php echo $content_top; ?>
            <p><?php echo $text_account_already; ?></p>
            <p><?php echo $text_signup; ?></p>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="yourform" class="form-horizontal">
                <fieldset>
                    <h2 class="secondary-title"><?php echo $text_your_details; ?></h2>
                    <!--<div class="form-group required">
                        <label class="col-sm-2 control-label" for="organization_name"><?php echo $entry_organization_name ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="organization_name" value="<?php echo $organization_name ?>" id="organization_name" class="form-control" placeholder="<?php echo $entry_organization_name ?>" />
                            <?php if ($error_organization_name) { ?>
                                <div class="text-danger"><?php echo $error_organization_name  ?></div>
                            <?php } ?>
                        </div>
                    </div> --> 
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-firstname"><?php echo $entry_firstname; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control" />
                            <?php if ($error_firstname) { ?>
                                <div class="text-danger"><?php echo $error_firstname; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-lastname"><?php echo $entry_lastname; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control" />
                            <?php if ($error_lastname) { ?>
                                <div class="text-danger"><?php echo $error_lastname; ?></div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="form-group required group-left">
                        <label class="col-sm-2 control-label" for="position_in_organization"><?php echo $entry_position_organization ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="position_in_organization" value="<?php echo $position_in_organization ?>" id="position_in_organization" class="form-control" placeholder="<?php echo $entry_position_organization ?>"/>
                            <?php if ($error_position_in_organization) { ?>
                                <div class="text-danger"><?php echo $error_position_in_organization ?></div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
                            <?php if ($error_email) { ?>
                                <div class="text-danger"><?php echo $error_email; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-telephone"><?php echo $entry_telephone; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone_format; ?>" id="input-telephone" class="form-control" />
                            <?php if ($error_telephone) { ?>
                                <div class="text-danger"><?php echo $error_telephone; ?></div>
                            <?php } ?>
                        </div>
                    </div>

                     <div class="form-group required group-right">
                        <label class="col-sm-2 control-label" for="organization_phone_number"><?php echo $entry_organization_phone_number ?></label>
                        <div class="col-sm-10">
                        <input type="text" name="organization_phone_number" value="<?php echo $organization_phone_number ?>" id="email" class="form-control" placeholder="<?php echo $entry_organization_phone_number_format ?>"/>
                        <?php if ($error_organization_phone_number) { ?>
                            <div class="text-danger"><?php echo $error_organization_phone_number ?></div>
                        <?php } ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-faxextension"><?php echo $entry_extension; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="extension" value="<?php echo $extension; ?>" placeholder="<?php echo $entry_extension; ?>" id="input-extension" class="form-control" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-fax"><?php echo $entry_fax; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="fax" value="<?php echo $fax; ?>" placeholder="<?php echo $entry_fax; ?>" id="input-fax" class="form-control" />
                        </div>
                    </div>

        <fieldset>
            <h2 class="secondary-title"><?php echo $text_your_address; ?></h2>
            <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-company"><?php echo $entry_company; ?></label>
                <div class="col-sm-10">
                    <input type="text" name="company" value="<?php echo $company; ?>" placeholder="<?php echo $entry_company; ?>" id="input-company" class="form-control" />
                    <?php
                    if ($error_company) {
                        ?>
                        <div class="text-danger"><?php echo $error_company; ?></div>	
                        <?php
                    }
                    if ($error_company_required) {
                        ?>
                        <div class="text-danger"><?php echo $error_company_required; ?></div>	
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="input-website"><?php echo $entry_website; ?></label>
                <div class="col-sm-10">
                    <input type="text" name="website" value="<?php echo $website; ?>" placeholder="<?php echo $entry_website; ?>" id="input-website" class="form-control" />
                </div>
            </div>
            <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-address-1"><?php echo $entry_address_1; ?></label>
                <div class="col-sm-10">
                    <input type="text" name="address_1" value="<?php echo $address_1; ?>" placeholder="<?php echo $entry_address_1; ?>" id="input-address-1" class="form-control" />
                    <?php if ($error_address_1) { ?>
                        <div class="text-danger"><?php echo $error_address_1; ?></div>
                    <?php } ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="input-address-2"><?php echo $entry_address_2; ?></label>
                <div class="col-sm-10">
                    <input type="text" name="address_2" value="<?php echo $address_2; ?>" placeholder="<?php echo $entry_address_2; ?>" id="input-address-2" class="form-control" />
                </div>
            </div>
            <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-city"><?php echo $entry_city; ?></label>
                <div class="col-sm-10">
                    <input type="text" name="city" value="<?php echo $city; ?>" placeholder="<?php echo $entry_city; ?>" id="input-city" class="form-control" />
                    <?php if ($error_city) { ?>
                        <div class="text-danger"><?php echo $error_city; ?></div>
                    <?php } ?>
                </div>
            </div>
            <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-postcode"><?php echo $entry_postcode; ?></label>
                <div class="col-sm-10">
                    <input type="text" name="postcode" value="<?php echo $postcode; ?>" placeholder="<?php echo $entry_postcode; ?>" id="input-postcode" class="form-control" />
                    <?php if ($error_postcode) { ?>
                        <div class="text-danger"><?php echo $error_postcode; ?></div>
                    <?php } ?>
                </div>
            </div>
            <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-country"><?php echo $entry_country; ?></label>
                <div class="col-sm-10">
                    <select name="country_id" id="input-country" class="form-control">
                        <option value="false"><?php echo $text_select; ?></option>
                        <?php foreach ($countries as $country) { ?>
                            <?php if ($country['country_id'] == $country_id) { ?>
                                <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                            <?php } else { ?>
                                <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                    <?php if ($error_country) { ?>
                        <div class="text-danger"><?php echo $error_country; ?></div>
                    <?php } ?>
                </div>
            </div>
            <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-zone"><?php echo $entry_zone; ?></label>
                <div class="col-sm-10">
                    <select name="zone_id" id="input-zone" class="form-control">
                    </select>
                    <?php if ($error_zone) { ?>
                        <div class="text-danger"><?php echo $error_zone; ?></div>
                    <?php } ?>
                </div>
            </div>
        </fieldset>					              
                    <div class="form-group">
                        <label  style="width:18%;display:inline-block;" class="control-label branded-web" for="country "><?php echo $entry_branded_website ?></label>
                        <div class="col-xs-12" style="display: inline-block;">
                            <textarea  style="width:100%;height:75px;" name="branded_website"><?php echo $branded_website ?></textarea>
                        </div>			
                    </div>
                    
                    <fieldset id="account">
                            <h3><?php echo $entry_organization_details ?></h3>
                            <!--<div class="form-group">
                                <label class="control-label" for="organization_website"><?php echo $entry_organization_website ?> </label>
                                <input type="text" name="organization_website" value="<?php echo $organization_website ?>" id="organization_website" class="form-control" placeholder="<?php echo $entry_organization_website ?>"/>
                            </div>-->

                            <div class="form-group">
                                <label class="control-label" for="website_nickname"><?php echo $entry_organization_hashtags ?></label>
                                <input type="text" name="organization_hashtags" value="<?php echo $organization_hashtags ?>" id="organization_hashtags" class="form-control" placeholder="<?php echo $entry_organization_hashtags ?>"/>
                            </div>

                            <div class="form-group group-left">
                                <label class="control-label" for="organization_facebook_link ">
                                    <?php echo $entry_organization_facebook_link ?></label>
                                <input type="text" name="organization_facebook_link" value="<?php echo $organization_facebook_link ?>" id="organization_website" class="form-control" placeholder="<?php echo $entry_organization_facebook_link ?>"/>
                            </div>

                            <div class="form-group group-right">
                                <label class=" control-label" for="organization_instagram"><?php echo $entry_organization_instagram ?></label>
                                <input type="text" name="organization_instagram" value="<?php echo $organization_instagram ?>" id="organization_instagram" class="form-control" placeholder="<?php echo $entry_organization_instagram ?>"/>
                            </div>

                            <div class="form-group group-left">
                                <label class="control-label" for="organization_twitter"><?php echo $entry_organization_twitter ?></label>
                                <input type="text" name="organization_twitter" value="<?php echo $organization_twitter ?>" id="organization_twitter" class="form-control" placeholder="<?php echo $entry_organization_twitter ?>"/>
                            </div>

                            <div class="form-group group-right">
                                <label class="control-label" for="organization_linkedIn_link "><?php echo $entry_organization_linkedIn_link ?></label>
                                <input type="text" name="organization_linkedIn_link" value="<?php echo $organization_linkedIn_link ?>" id="organization_linkedIn_link" class="form-control" placeholder="<?php echo $entry_organization_linkedIn_link ?>"/>
                            </div>

                            <div class="form-group permits-box oc3">
                                <label class="control-label" for=""><?php echo $entry_agreement_heading ?></label><br>

                                <div class="col-xs-12">
                                    <div class="col-md-4">
                                        <label><input type="checkbox" <?php echo $checkBox1 ?> name="agreement[]" value="on the main, or other pages of the ShopPal website that display Memberinformation"/>
                                            <?php echo $entry_agreement_checkbox1 ?></label>
                                    </div>
                                    <div class="col-md-4">
                                        <label> <input type="checkbox" <?php echo $checkBox2 ?> name="agreement[]" value="on Social Media and other Internet Marketing platforms, and/or"/>
                                            <?php echo $entry_agreement_checkbox2 ?></label>
                                    </div>
                                    <div class="col-md-4">
                                        <label>
                                            <input type="checkbox" <?php echo $checkBox3 ?> name="agreement[]" value="on Print Media and or Radio/Television Media."/>
                                            <?php echo $entry_agreement_checkbox3 ?></label>
                                    </div>
                                </div>
                            </div>


                           <!-- <div class="form-group required">
                                <label class="col-sm-2 control-label" for="logo"><?php echo $entry_logo; ?><br></label>
                                <div class="col-sm-10">
                                    <div class="col-xs-12">
                                        <input type="file" name="logo" id="logo" class="form-control"/>
                                    </div>
                                     <?php if ($error_logo) { ?>
                                        <div class="text-danger"><?php echo $error_logo; ?></div>
                                    <?php } ?>
                                </div>

                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="organization_mission_statement"><?php echo $entry_organization_mission_statement; ?></label>
                                <div class="col-sm-10">
                                    <textarea style="width:100%" id="organization_mission_statement" name="organization_mission_statement" class="form-control"><?php echo $entry_organization_mission_statement_short; ?></textarea>

                                     <?php if ($error_organization_mission_statement) { ?>
                                    <div class="text-danger"><?php echo $error_organization_mission_statement; ?>
                                    </div>
                                     <?php } ?> 
                                </div>
                               
                            </div>-->
                        <?php if ($error_mission_both) { ?>
                            <div class="alert alert-danger warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_mission_both; ?></div>
                         <?php } ?> 
						 <br>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-mission"><?php echo $entry_mission; ?></label>
                            <div class="col-sm-10">
							<?php echo $entry_mission_short; ?><br>
                                <textarea rows="2" placeholder="<?php echo $entry_mission; ?>" id="input-mission"  name="mission" cols="94" style="height: 66px;" onkeyup="countChar(this)"><?php echo $entry_mission_view; ?></textarea>
                                 <em><?php echo $entry_mission_max; ?></em>
                                <div id="charNum" class="text-danger" style="display: none;"></div>

                                <?php
                                if ($error_mission) {
                                    ?>
                                    <div class="text-danger"><?php echo $error_mission; ?></div>
                                    <?php
                                }
                                ?>
                            </div>
                            
                           
                        </div>
                        <!--In French -->
						<br>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-mission-french"><?php echo $entry_mission_french; ?></label>
                            <div class="col-sm-10">
							<?php echo $entry_mission_french_short; ?><br>
                                <textarea rows="2" placeholder="<?php echo $entry_mission_french; ?>" id="input-mission-french"  name="mission_french" cols="94" onkeyup="missioncountChar(this)" style="height: 66px;"><?php echo $entry_mission_french_view; ?></textarea>
                                <em><?php echo $entry_mission_max; ?></em>
                                <div id="missioncharNum" class="text-danger" style="display: none;"></div>

                                <?php
                                if ($error_mission_french) {
                                    ?>
                                    <div class="text-danger"><?php echo $error_mission_french; ?></div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>

						<?php if ($error_mission_both) { ?>
                            <div class="alert alert-danger warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_mission_both; ?></div>
                         <?php } ?> 
						 <br>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-mission-long"><?php echo $entry_mission_txt_long; ?></label>
                            <div class="col-sm-10">
							<?php echo $entry_mission_long; ?><br>
                                <textarea rows="2" placeholder="<?php echo $entry_mission_txt_long; ?>" id="input-mission-long"  name="mission_long"><?php echo $entry_mission_view_long; ?></textarea>
                                                       
                            </div>
                        </div>
						
                        <!--In French -->
						<br>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-mission-french"><?php echo $entry_mission_french_txt_long; ?></label>
                            <div class="col-sm-10">
							<?php echo $entry_mission_french_long; ?><br>
                                <textarea rows="2" placeholder="<?php echo $entry_mission_french_txt_long; ?>" id="input-mission-french-long"  name="mission_long_french" ><?php echo $entry_mission_french_view_long; ?></textarea>
                                <div id="missioncharNum" class="text-danger" style="display: none;"></div>

                            </div>
                        </div>

                        <?php if ($error_organization_mission_both) { ?>
                            <div class="alert alert-danger warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_organization_mission_both; ?></div>
                         <?php } ?> 

                          
                        <!--In French --> 
                        <!--In English --> 
                            <div class="form-group">
                                <label style="width:26%;display:inline-block;" class="control-label organization_french" for="organization_detailed" for="organization_mission_statement"><?php echo $entry_organization_detailed ?>
                                </label>
                                <div class="col-xs-12">
                                    <textarea style="width:100%" id="organization_detailed" name="organization_detailed"><?php echo $organization_detailed ?></textarea>
                                </div>
                            </div>
                         <!--In English --> 
                         <!--In French --> 
                            <div class="form-group">
                                <label style="width:26%;display:inline-block;" class="control-label organization_french"  for="organization_detailed_french"><?php echo $entry_organization_detailed_french ?>
                                </label>
                                <div class="col-xs-12">
                                    <textarea style="width:100%" id="organization_detailed_french" name="organization_detailed_french"><?php echo $organization_detailed_french ?></textarea>
                                </div>
                            </div>
                         <!--In French --> 
                    </fieldset>

                    <fieldset id="account">
                            <h3><?php echo $entry_hear_about_us ?></h3>

                            <div class="form-group social-link">

                                <div class="col-xs-12">
                                    <div class="col-md-4">
                                    <label>
                                        <input type="checkbox"  <?php echo $checked_facebook ?> name="hear_about_us[]" value="Facebook"/>
                                        Facebook <br/>
                                    </label>
                                    </div>
                                    <div class="col-md-4">
                                     <label>
                                        <input type="checkbox"  <?php echo $checked_linkedIn ?> name="hear_about_us[]" value="LinkedIn"/>
                                        LinkedIn
                                    </label>
                                    </div>
                                    <div class="col-md-4">
                                    <label>
                                        <input type="checkbox"  <?php echo $checked_shoppal ?> name="hear_about_us[]" value="ShopPal Website"/>
                                        <?php echo $entry_shoppal_website ?>
                                    </label>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="col-md-4">
                                    <label>
                                        <input type="checkbox"  <?php echo $checked_local_store ?> name="hear_about_us[]" value="Local Store"/>
                                        <?php echo $entry_local_store ?>
                                    </label>
                                    </div>
                                    <div class="col-md-4">
                                    <label>
                                        <input type="checkbox"  <?php echo $checked_loal_event ?> name="hear_about_us[]" value="Local Event"/>
                                        Local Event
                                    </label>
                                    </div>
                                    <div class="col-md-4">
                                    <label>
                                        <input type="checkbox"  <?php echo $checked_flyer ?> name="hear_about_us[]" value="Flyer"/>
                                        <?php echo $entry_local_flyer ?>
                                    </label>
                                    </div>
                                </div>								
                                <div class="col-xs-12">
                                    <div class="col-md-4">
                                    <label>
                                        <input type="checkbox"  <?php echo $checked_other_fundraising_organization ?> name="hear_about_us[]" value="Other Fundraising Organization"/>
                                        <?php echo $entry_other_fundraising_organization ?></label>
                                    </div>
								</div>
                                    <div class="col-md-8" style="display:none;">
                                    <label style="width: 100% !important">
                                        <input type="checkbox" disabled <?php echo $checked_other ?> name="hear_about_us_checked" value="other"/>
                                        <input type="text" <?php echo $readonly;?> name="other_hear_about_us" value="<?php echo $other_hear_about_us ?>" id="other_question" class="form-control" placeholder="<?php echo $entry_other_hear_about_us ?>" style="width: 35% !important;" />
                                    </label>
                                    </div>

                                  </div>	
								<div class="col-xs-12">
									<div class="col-md-8">
										<label>
											<input type="radio" name="other_details" value="1"  <?php echo ($other_details== 1) ?  "checked" : "" ;  ?> >
											<?php echo $other_details_text ?>
										</label>
									</div>
									<div class="col-md-8" id="other_details" style="display:none;">
										<label style="width: 100% !important">
											<input type="text"  name="other_details_input" class="form-control" placeholder="<?php echo $other_details_input ?>" value="<?php echo $other_details_input ?>"  style="width: 35% !important; margin-left:27px" />
										</label>
									</div>

								</div>	
                          
                        </fieldset>


                  
                    <!--new fields-->
                </fieldset>
                <fieldset>
                    <div class="custom_fields">
                        <legend><?php echo $entry_slide_info; ?></legend>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input_slide"><?php echo $entry_slide_background; ?></label>

                            <div class="col-sm-10">
                                <input type="hidden" name="slider_image" id="slider_image" value="<?php echo $slider_image; ?>">

                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7', 'slide_img8','slide_img9','slide_img10','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image1.jpg')"><img id="slide_img1" src="image/slider/image1.jpg" alt="" style="<?php if ($slider_image == 'image1.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>
                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img2', 'slide_img1', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7', 'slide_img8' , 'slide_img9','slide_img10','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image2.jpg')"><img id="slide_img2" src="image/slider/image2.jpg" alt="" style="<?php if ($slider_image == 'image2.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>
                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img3', 'slide_img1', 'slide_img2', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7', 'slide_img8','slide_img9','slide_img10','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image3.jpg')"><img id="slide_img3" src="image/slider/image3.jpg" alt=""  style="<?php if ($slider_image == 'image3.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>
                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img4', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img5', 'slide_img6', 'slide_img7', 'slide_img8', 'slide_img9','slide_img10','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image4.jpg')"><img id="slide_img4" src="image/slider/image4.jpg" alt=""  style="<?php if ($slider_image == 'image4.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>
                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img5', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img6', 'slide_img7', 'slide_img8', 'slide_img9','slide_img10','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image5.jpg')"><img id="slide_img5" src="image/slider/image5.jpg" alt=""  style="<?php if ($slider_image == 'image5.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>
                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img6', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img7', 'slide_img8','slide_img9','slide_img10','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20',  'image6.jpg')"><img id="slide_img6" src="image/slider/image6.jpg" alt=""  style="<?php if ($slider_image == 'image6.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>
                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img7', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img8','slide_img9','slide_img10','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image7.jpg')"><img id="slide_img7" src="image/slider/image7.jpg" alt=""  style="<?php if ($slider_image == 'image7.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>
                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img8', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img9','slide_img10','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image8.jpg')"><img id="slide_img8" src="image/slider/image8.jpg" alt=""  style="<?php if ($slider_image == 'image8.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>
                                            

                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img9', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img10','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image9.jpg')"><img id="slide_img9" src="image/slider/image9.jpg" alt=""  style="<?php if ($slider_image == 'image9.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>

                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img10', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image10.jpg')"><img id="slide_img10" src="image/slider/image10.jpg" alt=""  style="<?php if ($slider_image == 'image10.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>

                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img11', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image11.jpg')"><img id="slide_img11" src="image/slider/image11.jpg" alt=""  style="<?php if ($slider_image == 'image11.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>

                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img12', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img11', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image12.jpg')"><img id="slide_img12" src="image/slider/image12.jpg" alt=""  style="<?php if ($slider_image == 'image12.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>

                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img13', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img11', 'slide_img12', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image13.jpg')"><img id="slide_img13" src="image/slider/image13.jpg" alt=""  style="<?php if ($slider_image == 'image13.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>

                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img14', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img11', 'slide_img13', 'slide_img12', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image14.jpg')"><img id="slide_img14" src="image/slider/image14.jpg" alt=""  style="<?php if ($slider_image == 'image14.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>

                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img15', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image15.jpg')"><img id="slide_img15" src="image/slider/image15.jpg" alt=""  style="<?php if ($slider_image == 'image15.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>

                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img16', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image16.jpg')"><img id="slide_img16" src="image/slider/image16.jpg" alt=""  style="<?php if ($slider_image == 'image16.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>

                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img17', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img11', 'slide_img12', 'slide_img13','slide_img14', 'slide_img15', 'slide_img16',  'slide_img18', 'slide_img19', 'slide_img20', 'image17.jpg')"><img id="slide_img17" src="image/slider/image17.jpg" alt=""  style="<?php if ($slider_image == 'image17.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>

                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img18', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img11', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img19', 'slide_img20', 'slide_img12', 'image18.jpg')"><img id="slide_img18" src="image/slider/image18.jpg" alt=""  style="<?php if ($slider_image == 'image18.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>

                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img19', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img11', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18',   'slide_img20', 'slide_img12', 'image19.jpg')"><img id="slide_img19" src="image/slider/image19.jpg" alt=""  style="<?php if ($slider_image == 'image19.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>

                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img20', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img11','slide_img12','slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'image20.jpg')"><img id="slide_img20" src="image/slider/image20.jpg" alt=""  style="<?php if ($slider_image == 'image20.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
        </div>
        <!--custom fields -->

        <fieldset>
            <!--<h2 class="secondary-title"><?php echo $text_payment; ?></h2>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="input-tax"><?php echo $entry_tax; ?></label>
                <div class="col-sm-10">
                    <input type="text" name="tax" value="<?php echo $tax; ?>" placeholder="<?php echo $entry_tax; ?>" id="input-tax" class="form-control" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $entry_payment; ?></label>
                <div class="col-sm-10">
                    <div class="radio">
                        <label>
                            <?php if ($payment == 'cheque') { ?>
                                <input type="radio" name="payment" value="cheque" checked="checked" />
                            <?php } else { ?>
                                <input type="radio" name="payment" value="cheque" />
                            <?php } ?>
                            <?php echo $text_cheque; ?></label>
                    </div>
                    <!-- <div class="radio">
                        <label>
                            <?php if ($payment == 'paypal') { ?>
                                <input type="radio" name="payment" value="paypal" checked="checked" />
                            <?php } else { ?>
                                <input type="radio" name="payment" value="paypal" />
                            <?php } ?>
                            <?php echo $text_paypal; ?></label>
                    </div>
                    <div class="radio">
                        <label>
                            <?php if ($payment == 'bank') { ?>
                                <input type="radio" name="payment" value="bank" checked="checked" />
                            <?php } else { ?>
                                <input type="radio" name="payment" value="bank" />
                            <?php } ?>
                            <?php echo $text_bank; ?></label>
                    </div> 
                </div>
            </div>
            <div class="form-group payment" id="payment-cheque">
                <label class="col-sm-2 control-label" for="input-cheque"><?php echo $entry_cheque; ?></label>
                <div class="col-sm-10">
                    <input type="text" name="cheque" value="<?php echo $cheque; ?>" placeholder="<?php echo $entry_cheque; ?>" id="input-cheque" class="form-control" />
                </div>
            </div>
            <div class="form-group payment" id="payment-paypal">
                <label class="col-sm-2 control-label" for="input-paypal"><?php echo $entry_paypal; ?></label>
                <div class="col-sm-10">
                    <input type="text" name="paypal" value="<?php echo $paypal; ?>" placeholder="<?php echo $entry_paypal; ?>" id="input-paypal" class="form-control" />
                </div>
            </div>
            <div class="payment" id="payment-bank">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-bank-name"><?php echo $entry_bank_name; ?></label>
                    <div class="col-sm-10">
                        <input type="text" name="bank_name" value="<?php echo $bank_name; ?>" placeholder="<?php echo $entry_bank_name; ?>" id="input-bank-name" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-bank-branch-number"><?php echo $entry_bank_branch_number; ?></label>
                    <div class="col-sm-10">
                        <input type="text" name="bank_branch_number" value="<?php echo $bank_branch_number; ?>" placeholder="<?php echo $entry_bank_branch_number; ?>" id="input-bank-branch-number" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-bank-swift-code"><?php echo $entry_bank_swift_code; ?></label>
                    <div class="col-sm-10">
                        <input type="text" name="bank_swift_code" value="<?php echo $bank_swift_code; ?>" placeholder="<?php echo $entry_bank_swift_code; ?>" id="input-bank-swift-code" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-bank-account-name"><?php echo $entry_bank_account_name; ?></label>
                    <div class="col-sm-10">
                        <input type="text" name="bank_account_name" value="<?php echo $bank_account_name; ?>" placeholder="<?php echo $entry_bank_account_name; ?>" id="input-bank-account-name" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-bank-account-number"><?php echo $entry_bank_account_number; ?></label>
                    <div class="col-sm-10">
                        <input type="text" name="bank_account_number" value="<?php echo $bank_account_number; ?>" placeholder="<?php echo $entry_bank_account_number; ?>" id="input-bank-account-number" class="form-control" />
                    </div>
                </div>
            </div>-->
        </fieldset>
		
        <fieldset>
            <h2 class="secondary-title"><?php echo $text_your_password; ?></h2>
            <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-password"><?php echo $entry_password; ?></label>
                <div class="col-sm-10">
                    <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
                    <?php if ($error_password) { ?>
                        <div class="text-danger"><?php echo $error_password; ?></div>
                    <?php } ?>
                </div>
            </div>
            <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-confirm"><?php echo $entry_confirm; ?></label>
                <div class="col-sm-10">
                    <input type="password" name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $entry_confirm; ?>" id="input-confirm" class="form-control" />
                    <?php if ($error_confirm) { ?>
                        <div class="text-danger"><?php echo $error_confirm; ?></div>
                    <?php } ?>
                </div>
            </div>
			<div class="custom_fields">
                        <!-- new fields -->
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-custom_logo"><?php echo $entry_custom_logo; ?></label>
                            <div class="col-sm-10">
                                <input type="file" accept="image/*" name="custom_logo" placeholder="<?php echo $entry_custom_logo; ?>" id="custom_logo" />
                                <img id="blah" src="image/no_image.png" alt="" style="width: 150px;height:150px; margin-top: 5px;">
                            </div>
                        </div>		   

			</div> 
			<input type="hidden" name="custom_logo_status" id="hidden_id" value="0">
        </fieldset>
		 <!--custom fields -->
		<!--<div class="custom_fields">
			
			<div class="form-group">
				<label class="col-sm-2 control-label" for="input-custom_logo"><?php echo $entry_custom_logo; ?></label>
				<div class="col-sm-10" style="margin-left: 304px;">
					<input type="file" accept="image/*" name="custom_logo" placeholder="<?php echo $entry_custom_logo; ?>" id="custom_logo" />
					<img id="blah" src="image/no_image.png" alt="" style="width: 150px;height:150px; margin-top: 5px;">
				</div>
			</div>		   

		</div> -->
        <?php if (version_compare(VERSION, '2.1', '>=')): ?>
            <?php echo $captcha; ?>
        <?php endif; ?>
        <?php if ($text_agree) { ?>
            <div class="buttons">
                <div class="pull-right"><?php echo $text_agree; ?>
                    <?php if ($agree) { ?>
                        <input type="checkbox" name="agree" value="1" checked="checked" />
                    <?php } else { ?>
                        <input type="checkbox" name="agree" value="1" />
                    <?php } ?>
                    &nbsp;
                    <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary button" />
                </div>
            </div>
        <?php } else { ?>
            <div class="buttons">
                <div class="pull-right">
                    <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary button" />
                </div>
            </div>
        <?php } ?>
        </form>
        <?php echo $content_bottom; ?></div>
</div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var other_details = $("input[name='other_details']:checked").val()
        if(other_details == 1) {
            $('#other_details').show();
        } else {
            $('#other_details').hide();
        }
    });
</script>

<script type="text/javascript">

function select_image(img_id,other_image1,other_image2,other_image3,other_image4,other_image5,other_image6,other_image7,other_image8,other_image9,other_image10,other_image11,other_image12,other_image13,other_image14,other_image15,other_image16,other_image17,other_image18,other_image19,image_name)
{
	$('#'+img_id).css({"border-color": "#dd0017","border-width":"4px","border-style":"solid"});
	$('#'+other_image1).css({"border-color": "","border-width":"","border-style":""});
	$('#'+other_image2).css({"border-color": "","border-width":"","border-style":""});
	$('#'+other_image3).css({"border-color": "","border-width":"","border-style":""});
	$('#'+other_image4).css({"border-color": "","border-width":"","border-style":""});
	$('#'+other_image5).css({"border-color": "","border-width":"","border-style":""});
	$('#'+other_image6).css({"border-color": "","border-width":"","border-style":""});
    $('#'+other_image7).css({"border-color": "","border-width":"","border-style":""});
	$('#'+other_image8).css({"border-color": "","border-width":"","border-style":""});
    $('#'+other_image9).css({"border-color": "","border-width":"","border-style":""});
    $('#'+other_image10).css({"border-color": "","border-width":"","border-style":""});
    $('#'+other_image11).css({"border-color": "","border-width":"","border-style":""});
    $('#'+other_image12).css({"border-color": "","border-width":"","border-style":""});
    $('#'+other_image13).css({"border-color": "","border-width":"","border-style":""});
    $('#'+other_image14).css({"border-color": "","border-width":"","border-style":""});
    $('#'+other_image15).css({"border-color": "","border-width":"","border-style":""});
    $('#'+other_image16).css({"border-color": "","border-width":"","border-style":""});
    $('#'+other_image17).css({"border-color": "","border-width":"","border-style":""});
    $('#'+other_image18).css({"border-color": "","border-width":"","border-style":""});
    $('#'+other_image19).css({"border-color": "","border-width":"","border-style":""});

    $('#slider_image').val(image_name);
}

function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#blah').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

var allRadios = document.getElementsByName('other_details');
var booRadio;
var x = 0;
for(x = 0; x < allRadios.length; x++){

        allRadios[x].onclick = function(){

            if(booRadio == this){
                this.checked = false;
        booRadio = null;
		 $('#other_details').hide();
    
            }else{
            booRadio = this;
			$('#other_details').show();
        }
        };

}


$("#custom_logo").change(function() {
  readURL(this);
});
$('select[name=\'country_id\']').on('change', function() {
	$.ajax({
		url: 'index.php?route=affiliate/register/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
			$('.fa-spin').remove();
		},
		success: function(json) {
			// if (json['postcode_required'] == '1') {
			// 	$('input[name=\'postcode\']').parent().parent().addClass('required');
			// } else {
			// 	$('input[name=\'postcode\']').parent().parent().removeClass('required');
			// }

			html = '<option value=""><?php echo $text_select; ?></option>';

			if (json['zone'] && json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
					html += '<option value="' + json['zone'][i]['zone_id'] + '"';

					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
						html += ' selected="selected"';
					}

					html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}

			$('select[name=\'zone_id\']').html(html);
    	},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'country_id\']').trigger('change');
//misssion
function countChar(val) {
    var len = val.value.length;
    if (len > 211) {
        val.value = val.value.substring(0, 211);
    } else {
        $('#charNum').css("display", "inline-block");
        $('#charNum').text(211 - len);
    }
}

//misssion french
function missioncountChar(val) {
    var len = val.value.length;
    if (len > 211) {
        val.value = val.value.substring(0, 211);
    } else {
        $('#missioncharNum').css("display", "inline-block");
        $('#missioncharNum').text(211 - len);
    }
}
</script>
<script type="text/javascript"><!--
$('input[name=\'payment\']').on('change', function() {
	$('.payment').hide();

	$('#payment-' + this.value).show();
});

$('input[name=\'payment\']:checked').trigger('change');
//-->

<?php
if(isset($data_post) && empty($get_errors) && isset($no_custom_logo))
{?>

	$(document).ready(function(){
	
	var custom_logo1 = $("#custom_logo").val();
	if(custom_logo1 == ''){
		var radio_value = $("#hidden_id").val();
		if(radio_value!=1)
		{
			$('#myModal').modal('show');
		}
		var err = $( 'input[name=logo_err]:checked' ).val();
		
		if(err == '1')
		{
			$('#myModal').modal('hide');
			return true;
		}
		else if(err == '2')
		{
			return false;
		}
		return false;
	}

});
<?php
}?>

function getRadioVal(val)
{
	$("#hidden_id").val(val);
	if(val=='1')
	{
		$('#myModal').modal('hide');
		$("form").submit();
	}
	else if(val=='2')
	{
		$('#myModal').modal('hide');
	}
}
</script>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?php echo $error_logo1; ?></h4>
        </div>
        <div class="modal-body" id="logo_div">
			<input type="radio" name="logo_err" value="1" onclick="getRadioVal(this.value)"><?php echo $logo_later; ?></input>
			<input type="radio" name="logo_err" value="2" onclick="getRadioVal(this.value)"><?php echo $logo_now; ?></input>
          
        </div>
        
      </div>
      
    </div>
</div>
<div class="modal fade" id="error_form" role="dialog">
	<div class="modal-dialog">

	<!-- Modal content-->
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title"><?php echo $form_error; ?></h4>
		</div>
		<div class="modal-body">
		<p><?php echo $look_below; ?></p>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $close_modal; ?></button>
		</div>
	</div>

	</div>
</div>
<style>
.oc3 label{ width:25% !important;}
.social-link .radio label{ width:100% !important; }
</style>


<?php echo $footer; ?>
