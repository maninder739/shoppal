<?php echo $header; 
	if ($error_postcode || $error_country || $error_zone || $error_city || $error_address_1 || $error_mission_french || $error_mission || $error_telephone || $error_email || $error_lastname || $error_firstname || $error_warning) {
?>
<script type="text/javascript">
	$(window).load(function() {
		$('#myModal').modal('show');
	});
</script>
<?php }?>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php echo $breadcrumb['href']; ?>" itemprop="url"><span itemprop="title"><?php echo $breadcrumb['text']; ?></span></a></li>
    <?php } ?>
    <span class="welcome-li"><?php echo $text_welcome.' '.$get_firstname;?>, <a href="<?php echo $this->url->link('affiliate/logout');?>"><?php echo $text_logout;?></a> </span>
  </ul>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?><?php echo $column_right; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <h1 class="heading-title"><?php echo $heading_title; ?></h1>
      <?php echo $content_top; ?>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset>
          <legend><?php echo $text_your_details; ?></legend>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-firstname"><?php echo $entry_firstname; ?></label>
            <div class="col-sm-10">
              <input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control" />
              <?php if ($error_firstname) { ?>
              <div class="text-danger"><?php echo $error_firstname; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-lastname"><?php echo $entry_lastname; ?></label>
            <div class="col-sm-10">
              <input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control" />
              <?php if ($error_lastname) { ?>
              <div class="text-danger"><?php echo $error_lastname; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
            <div class="col-sm-10">
              <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
              <?php if ($error_email) { ?>
              <div class="text-danger"><?php echo $error_email; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-telephone"><?php echo $entry_telephone; ?></label>
            <div class="col-sm-10">
              <input type="text" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone_format; ?>" id="input-telephone" class="form-control" />
              <?php if ($error_telephone) { ?>
              <div class="text-danger"><?php echo $error_telephone; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-fax"><?php echo $entry_fax; ?></label>
            <div class="col-sm-10">
              <input type="text" name="fax" value="<?php echo $fax; ?>" placeholder="<?php echo $entry_fax; ?>" id="input-fax" class="form-control" />
            </div>
          </div>
		  <!--new fields-->
			
		
			<div class="form-group">
				<label class="col-sm-2 control-label" for="input-custom_logo"><?php echo $entry_custom_logo; ?></label>
				<div class="col-sm-10">
					<input type="hidden"  value="<?php echo $custom_logo; ?>" name="old_custom_logo" id="old_custom_logo" />
					<input type="file" accept="image/*" name="custom_logo" placeholder="<?php echo $entry_custom_logo; ?>" id="custom_logo" value="<?php echo $custom_logo; ?>" />
					<img id="blah" src="image/data/<?php echo $custom_logo; ?>" alt="" style="width: 150px;height:150px; margin-top: 5px;">
				</div>
			</div>		   
          
			<div class="form-group">
            <label class="col-sm-2 control-label" for="input-mission"><?php echo $entry_mission; ?></label>
            <div class="col-sm-10">
			  <textarea rows="2" placeholder="<?php echo $entry_mission; ?>" id="input-mission"  name="mission" cols="94" style="height: 66px;" onkeyup="countChar(this)"><?php echo $mission; ?></textarea>
        <em>211 words max </em>
        <div id="charNum" class="text-danger" style="display: none;"></div>
				<?php
					if($error_mission)
					{
					?>
						<div class="text-danger"><?php echo $error_mission; ?></div>
					<?php
					}
				?>
			</div>
          </div>
		  <!--In French -->
		  <div class="form-group">
            <label class="col-sm-2 control-label" for="input-mission-french"><?php echo $entry_mission_french; ?></label>
            <div class="col-sm-10">
				<textarea rows="2" placeholder="<?php echo $entry_mission_french; ?>" id="input-mission-french"  name="mission_french" cols="94" onkeyup="missioncountChar(this)" style="height: 66px;"><?php echo $mission_french; ?></textarea>
			    <em>211 words max</em>
          <div id="missioncharNum" class="text-danger" style="display: none;"></div>
				<?php
					if($error_mission_french)
					{
					?>
						<div class="text-danger"><?php echo $error_mission_french; ?></div>
					<?php	
					}
					
				?>
			</div>
          </div>
		  <!--In French --> 
		  
		  
		  <div class="form-group">
            <label class="col-sm-2 control-label" for="input-mission-long"><?php echo $entry_mission_txt_long; ?></label>
            <div class="col-sm-10">
			  <textarea rows="2" placeholder="<?php echo $entry_mission_txt_long; ?>" id="input-mission-long"  name="mission_long"><?php echo $mission_long; ?></textarea>
			</div>
          </div>
		  <!--In French -->
		  <div class="form-group">
            <label class="col-sm-2 control-label" for="input-mission-french"><?php echo $entry_mission_french_txt_long; ?></label>
            <div class="col-sm-10">
				<textarea rows="2" placeholder="<?php echo $entry_mission_french_txt_long; ?>" id="input-mission-french"  name="mission_long_french"><?php echo $mission_long_french; ?></textarea>
			</div>
          </div>
		  
		  
		  <!--new fields-->
        </fieldset>
		<fieldset>
          <legend><?php echo $entry_slide_info;?></legend>
          
		  <div class="form-group">
            <label class="col-sm-2 control-label" for="input_slide"><?php echo $entry_slide_background;?></label>
			
            <div class="col-sm-10">
				<input type="hidden" name="slider_image" id="slider_image" value="<?php echo $slider_image;?>">
				
        <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7', 'slide_img8','slide_img9','slide_img10','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image1.jpg')"><img id="slide_img1" src="image/slider/image1.jpg" alt="" style="<?php if ($slider_image == 'image1.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>
                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img2', 'slide_img1', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7', 'slide_img8' , 'slide_img9','slide_img10','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image2.jpg')"><img id="slide_img2" src="image/slider/image2.jpg" alt="" style="<?php if ($slider_image == 'image2.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>
                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img3', 'slide_img1', 'slide_img2', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7', 'slide_img8','slide_img9','slide_img10','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image3.jpg')"><img id="slide_img3" src="image/slider/image3.jpg" alt=""  style="<?php if ($slider_image == 'image3.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>
                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img4', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img5', 'slide_img6', 'slide_img7', 'slide_img8', 'slide_img9','slide_img10','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image4.jpg')"><img id="slide_img4" src="image/slider/image4.jpg" alt=""  style="<?php if ($slider_image == 'image4.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>
                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img5', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img6', 'slide_img7', 'slide_img8', 'slide_img9','slide_img10','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image5.jpg')"><img id="slide_img5" src="image/slider/image5.jpg" alt=""  style="<?php if ($slider_image == 'image5.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>
                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img6', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img7', 'slide_img8','slide_img9','slide_img10','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20',  'image6.jpg')"><img id="slide_img6" src="image/slider/image6.jpg" alt=""  style="<?php if ($slider_image == 'image6.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>
                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img7', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img8','slide_img9','slide_img10','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image7.jpg')"><img id="slide_img7" src="image/slider/image7.jpg" alt=""  style="<?php if ($slider_image == 'image7.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>
                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img8', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img9','slide_img10','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image8.jpg')"><img id="slide_img8" src="image/slider/image8.jpg" alt=""  style="<?php if ($slider_image == 'image8.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>
                                            

                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img9', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img10','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image9.jpg')"><img id="slide_img9" src="image/slider/image9.jpg" alt=""  style="<?php if ($slider_image == 'image9.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>

                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img10', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image10.jpg')"><img id="slide_img10" src="image/slider/image10.jpg" alt=""  style="<?php if ($slider_image == 'image10.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>

                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img11', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image11.jpg')"><img id="slide_img11" src="image/slider/image11.jpg" alt=""  style="<?php if ($slider_image == 'image11.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>

                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img12', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img11', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image12.jpg')"><img id="slide_img12" src="image/slider/image12.jpg" alt=""  style="<?php if ($slider_image == 'image12.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>

                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img13', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img11', 'slide_img12', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image13.jpg')"><img id="slide_img13" src="image/slider/image13.jpg" alt=""  style="<?php if ($slider_image == 'image13.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>

                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img14', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img11', 'slide_img13', 'slide_img12', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image14.jpg')"><img id="slide_img14" src="image/slider/image14.jpg" alt=""  style="<?php if ($slider_image == 'image14.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>

                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img15', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img11', 'slide_img13', 'slide_img14', 'slide_img12', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image15.jpg')"><img id="slide_img15" src="image/slider/image15.jpg" alt=""  style="<?php if ($slider_image == 'image15.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>

                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img16', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image16.jpg')"><img id="slide_img16" src="image/slider/image16.jpg" alt=""  style="<?php if ($slider_image == 'image16.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>

                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img17', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img11', 'slide_img12', 'slide_img13','slide_img14', 'slide_img15', 'slide_img16',  'slide_img18', 'slide_img19', 'slide_img20', 'image17.jpg')"><img id="slide_img17" src="image/slider/image17.jpg" alt=""  style="<?php if ($slider_image == 'image17.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>

                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img18', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img11', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img19', 'slide_img20', 'slide_img12', 'image18.jpg')"><img id="slide_img18" src="image/slider/image18.jpg" alt=""  style="<?php if ($slider_image == 'image18.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>

                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img19', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img11', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18',   'slide_img20', 'slide_img12', 'image19.jpg')"><img id="slide_img19" src="image/slider/image19.jpg" alt=""  style="<?php if ($slider_image == 'image19.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>

                                <div class="col-sm-12 slider-img">
                                    <a href="javascript:void(0)" onclick="return select_image('slide_img20', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img11', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img12', 'image20.jpg')"><img id="slide_img20" src="image/slider/image20.jpg" alt=""  style="<?php if ($slider_image == 'image20.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                </div>

			</div>
          </div>
        </fieldset>
        <fieldset>
          <legend><?php echo $text_your_address; ?></legend>
         <!-- <div class="form-group">
            <label class="col-sm-2 control-label" for="input-company"><?php //echo $entry_company; ?></label>
            <div class="col-sm-10">
              <input type="text" name="company" value="<?php //echo $company; ?>" placeholder="<?php // echo $entry_company; ?>" id="input-company" class="form-control" />
            </div>
          </div>-->
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-website"><?php echo $entry_website; ?></label>
            <div class="col-sm-10">
              <input type="text" name="website" value="<?php echo $website; ?>" placeholder="<?php echo $entry_website; ?>" id="input-website" class="form-control" />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-address-1"><?php echo $entry_address_1; ?></label>
            <div class="col-sm-10">
              <input type="text" name="address_1" value="<?php echo $address_1; ?>" placeholder="<?php echo $entry_address_1; ?>" id="input-address-1" class="form-control" />
              <?php if ($error_address_1) { ?>
              <div class="text-danger"><?php echo $error_address_1; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-address-2"><?php echo $entry_address_2; ?></label>
            <div class="col-sm-10">
              <input type="text" name="address_2" value="<?php echo $address_2; ?>" placeholder="<?php echo $entry_address_2; ?>" id="input-address-2" class="form-control" />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-city"><?php echo $entry_city; ?></label>
            <div class="col-sm-10">
              <input type="text" name="city" value="<?php echo $city; ?>" placeholder="<?php echo $entry_city; ?>" id="input-city" class="form-control" />
              <?php if ($error_city) { ?>
              <div class="text-danger"><?php echo $error_city; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-postcode"><?php echo $entry_postcode; ?></label>
            <div class="col-sm-10">
              <input type="text" name="postcode" value="<?php echo $postcode; ?>" placeholder="<?php echo $entry_postcode; ?>" id="input-postcode" class="form-control" />
              <?php if ($error_postcode) { ?>
              <div class="text-danger"><?php echo $error_postcode; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-country"><?php echo $entry_country; ?></label>
            <div class="col-sm-10">
              <select name="country_id" id="input-country" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($countries as $country) { ?>
                <?php if ($country['country_id'] == $country_id) { ?>
                <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
              <?php if ($error_country) { ?>
              <div class="text-danger"><?php echo $error_country; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-zone"><?php echo $entry_zone; ?></label>
            <div class="col-sm-10">
              <select name="zone_id" id="input-zone" class="form-control">
              </select>
              <?php if ($error_zone) { ?>
              <div class="text-danger"><?php echo $error_zone; ?></div>
              <?php } ?>
            </div>
          </div>
        </fieldset>
        <div class="buttons">
          <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default button"><?php echo $button_back; ?></a></div>
          <div class="pull-right">
            <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary button" />
          </div>
        </div>
      </form>
      <?php echo $content_bottom; ?></div>
    </div>
</div>
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog">

	<!-- Modal content-->
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title"><?php echo $error_modalheader; ?></h4>
		</div>
		<div class="modal-body">
		<p><?php echo $error_modalfields; ?></p>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $error_modalclose; ?></button>
		</div>
	</div>

	</div>
</div>
<script type="text/javascript"><!--
function select_image(img_id,other_image1,other_image2,other_image3,other_image4,other_image5,other_image6,other_image7,other_image8,other_image9,other_image10,other_image11,other_image12,other_image13,other_image14,other_image15,other_image16,other_image17,other_image18,other_image19,image_name)
{
  $('#'+img_id).css({"border-color": "#dd0017","border-width":"4px","border-style":"solid"});
  $('#'+other_image1).css({"border-color": "","border-width":"","border-style":""});
  $('#'+other_image2).css({"border-color": "","border-width":"","border-style":""});
  $('#'+other_image3).css({"border-color": "","border-width":"","border-style":""});
  $('#'+other_image4).css({"border-color": "","border-width":"","border-style":""});
  $('#'+other_image5).css({"border-color": "","border-width":"","border-style":""});
  $('#'+other_image6).css({"border-color": "","border-width":"","border-style":""});
  $('#'+other_image7).css({"border-color": "","border-width":"","border-style":""});
  $('#'+other_image8).css({"border-color": "","border-width":"","border-style":""});
  $('#'+other_image9).css({"border-color": "","border-width":"","border-style":""});
  $('#'+other_image10).css({"border-color": "","border-width":"","border-style":""});
  $('#'+other_image11).css({"border-color": "","border-width":"","border-style":""});
  $('#'+other_image12).css({"border-color": "","border-width":"","border-style":""});
  $('#'+other_image13).css({"border-color": "","border-width":"","border-style":""});
  $('#'+other_image14).css({"border-color": "","border-width":"","border-style":""});
  $('#'+other_image15).css({"border-color": "","border-width":"","border-style":""});
  $('#'+other_image16).css({"border-color": "","border-width":"","border-style":""});
  $('#'+other_image17).css({"border-color": "","border-width":"","border-style":""});
  $('#'+other_image18).css({"border-color": "","border-width":"","border-style":""});
  $('#'+other_image19).css({"border-color": "","border-width":"","border-style":""});
  $('#slider_image').val(image_name);
}
function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#blah').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#custom_logo").change(function() {
  readURL(this);
});


$('select[name=\'country_id\']').on('change', function() {
	$.ajax({
		url: 'index.php?route=affiliate/edit/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
			$('.fa-spin').remove();
		},
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('input[name=\'postcode\']').parent().parent().addClass('required');
			} else {
				$('input[name=\'postcode\']').parent().parent().removeClass('required');
			}

			html = '<option value=""><?php echo $text_select; ?></option>';

			if (json['zone'] && json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
					html += '<option value="' + json['zone'][i]['zone_id'] + '"';

					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
						html += ' selected="selected"';
					}

					html += '>' + json['zone'][i]['name'] + '</option>';
				}
      } else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}

			$('select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'country_id\']').trigger('change');
function countChar(val) {
    var len = val.value.length;
    if (len > 211) {
        val.value = val.value.substring(0, 211);
    } else {
        $('#charNum').css("display", "inline-block");
        $('#charNum').text(211 - len);
    }
}

//misssion french
function missioncountChar(val) {
    var len = val.value.length;
    if (len > 211) {
        val.value = val.value.substring(0, 211);
    } else {
        $('#missioncharNum').css("display", "inline-block");
        $('#missioncharNum').text(211 - len);
    }
}

//--></script>
<?php echo $footer; ?>
