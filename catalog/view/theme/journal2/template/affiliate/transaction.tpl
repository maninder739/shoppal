<?php echo $header; ?>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php echo $breadcrumb['href']; ?>" itemprop="url"><span itemprop="title"><?php echo $breadcrumb['text']; ?></span></a></li>
    <?php } ?>
    <span class="welcome-li"><?php echo $text_welcome.' '.$firstname;?>, <a href="<?php echo $this->url->link('affiliate/logout');?>"><?php echo $text_logout;?></a> </span>
  </ul>
  <div class="row"><?php echo $column_left; ?><?php echo $column_right; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <h1 class="heading-title"><?php echo $heading_title; ?></h1>
      <?php echo $content_top; ?>
      <p><?php echo $text_balance; ?> <strong><?php echo $balance; ?></strong>.</p>
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <td class="text-left"><?php echo $column_date; ?></td>
              <td class="text-left"><?php echo $column_orderid; ?></td>
              <td class="text-left"><?php echo $column_retail; ?></td>
              <!-- <td class="text-right"><?php echo $column_total; ?></td> -->
              <td class="text-right"><?php echo $column_commission; ?></td>
            </tr>
          </thead>
          <tbody>
            <?php if ($transactions) { ?>
            <?php foreach ($transactions  as $transaction) { ?>
            <tr>
              <td class="text-left"><?php echo $transaction['date']; ?></td>
              <td class="text-left"><?php echo $transaction['order_id']; ?></td>
              <td class="text-left"><?php echo number_format($transaction['retail'], 2); ?></td>
             <!--  <td class="text-right"><?php echo number_format($transaction['total'], 2); ?></td> -->
              <td class="text-right"><?php echo number_format($transaction['commission'], 2); ?></td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="text-center" colspan="5"><?php echo $text_empty; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
      <div class="text-right"><?php echo $pagination; ?></div>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary button"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    </div>
</div>
<?php echo $footer; ?>
