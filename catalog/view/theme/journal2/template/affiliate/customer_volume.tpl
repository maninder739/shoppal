<?php echo $header; ?>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php echo $breadcrumb['href']; ?>" itemprop="url"><span itemprop="title"><?php echo $breadcrumb['text']; ?></span></a></li>
    <?php } ?>
    <span class="welcome-li"><?php echo $text_welcome.' '.$firstname;?>, <a href="<?php echo $this->url->link('affiliate/logout');?>"><?php echo $text_logout;?></a> </span>
  </ul>
  <div class="row"><?php echo $column_left; ?><?php echo $column_right; ?>
      <?php if ($column_left && $column_right) { ?>
      <?php $class = 'col-sm-6'; ?>
      <?php } elseif ($column_left || $column_right) { ?>
      <?php $class = 'col-sm-9'; ?>
      <?php } else { ?>
      <?php $class = 'col-sm-12'; ?>
      <?php } ?>
        <div id="content" class="<?php echo $class; ?>">
          <h1 class="heading-title"><?php echo $heading_title; ?></h1>
        <?php //echo '<pre>'; print_r($customer_list);
        if($customer_list){
			
			$current_month = date('m');
			$current_year = date('Y');
			
			$month = $customer_list['month'];
			$year = $customer_list['year'];
			
			if(!empty($customer_list['month']) && !empty($customer_list['month'])){
				$dateObj   = DateTime::createFromFormat('!m', $month);
				$monthName = $dateObj->format('F');
				$dateformat = $year.'-'.$month;
				$prevdate = date('m-Y', strtotime('-1 month', strtotime($dateformat)));
				$nextdate = date('m-Y', strtotime('+1 month', strtotime($dateformat)));
			}
			
			
			
		?>
         <div class="table-responsive">
		 <div class="btn-filter">
		  <a class="btn-view-all" href="<?php echo $this->url->link('affiliate/customer_volume&filter=all');?>"><?php echo $view_all; ?></a>
		 <?php if(isset($_GET['filter']) && $_GET['filter'] == 'all'){ ?>
		       <a class="btn-current" href="<?php echo $this->url->link('affiliate/customer_volume');?>"><?php echo $current_month; ?></a>
		 <?php } elseif($current_month == $month && $current_year == $year ) { ?>
			   <a class="btn-prev" href="<?php echo $this->url->link('affiliate/customer_volume&filter=prev&current='.$prevdate);?>"><?php echo $previous_month; ?></a> <span style="text-transform: uppercase;">&lt; <?php echo $shopping_volume; ?> <?php echo $monthName .' '. $year; ?> &gt;</span>
		 <?php } else { ?>
			   <a class="btn-prev" href="<?php echo $this->url->link('affiliate/customer_volume&filter=prev&current='.$prevdate);?>"><?php echo $previous_month; ?></a> <span style="text-transform: uppercase;">&lt; <?php echo $shopping_volume; ?> <?php echo $monthName .' '. $year; ?> &gt;</span> <a  class="btn-next" href="<?php echo $this->url->link('affiliate/customer_volume&filter=next&current='.$nextdate);?>"><?php echo $next_month; ?></a>
		 <?php } ?>
		</div>

		  <table class="table table-bordered table-hover list">
            <thead>
              <tr>
                <td class="text-left"><?php echo $cust_order_date;?></td>
                <td class="text-left"><?php echo $cust_name;?></td>
               <td class="text-left"><?php echo $order_status;?></td>
                <td class="text-left"><?php echo $cust_retail;?></td>
                <td class="text-left"><?php echo $cust_percentage;?></td>
              </tr>
            </thead>
            <tbody>
                <?php
                if(isset($customer_list['data'])){
					$total = 0;
					$commission = 0;
					$customers = array();

					foreach($customer_list['data'] as $customer) {
						
						$month = $customer['date_added'];
						$orderdate = date('d/m/Y', strtotime($customer['date_added']));
						
						$total += $customer['total'];
						$commission += $customer['commission'];
						
						$customers[$customer['customer_id']] = $customer['customer_id'];
					?>
					
					  <tr>
						  <td class="text-left"><?php echo $orderdate; ?></td>
						  <td class="text-left"><?php echo $customer['firstname']; ?> <?php echo substr($customer['lastname'], 0, 1) .'.'; ?></td>
						 <td class="text-left"><?php echo $customer['name']; ?> </td>
						  <td style="text-align: right;">$ <?php echo money_format("%i",round($customer['total'],2)); ?></td>
						  <td style="text-align: right;">$ <?php echo money_format("%i",round($customer['commission'],2)); ?></td>
					  </tr>
					<?php
					}
					?>
					<tr><td colspan="5"></td></tr>
					<tr>
					
					  <td class="text-left"><strong><?php echo $table_total;?></strong></td>
					  <td class="text-left"><strong><?php echo count($customers); if(count($customers) > 1){ echo $table_total_custs; }else{ echo $table_total_cust; } ?> </strong></td>
					  <td></td>
					  <td  style="text-align: right;"><strong>$ <?php echo number_format(round($total,2),2,".",","); ?></strong></td>
					  <td  style="text-align: right;"><strong>$ <?php echo number_format(round($commission,2),2,".",","); ?></strong></td>
					</tr>
					<?php
				}else{ ?>
					<tr><td colspan="5" style="text-align: center;"><strong><?php echo $table_total_none;?></strong></td></tr>
			<?php }
              ?>
			  
            </tbody>
          </table>
        </div>
        <?php
        if (version_compare(VERSION, '2.3', '>=')): ?>
          <!--<div class="row">
              <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
              <div class="col-sm-6 text-right"><?php echo $results; ?></div>
          </div>-->
          <?php else: ?>
              <!--<div class="text-right"><?php echo $pagination; ?></div>-->
          <?php endif; 
        }
        else
        {?>
           <p><b><?php echo $show_empty_text; ?></b></p>
        <?php
        }
       
        echo $content_bottom; ?>
        </div>
    </div>
</div>
<style>
.btn-current, .btn-view-all{ float:right;    text-transform: uppercase;
    margin-left: 10px;}
	.btn-filter{margin-bottom:10px; width:100%; float:left;}




</style>
<?php echo $footer; ?> 