<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?php echo $text_new_subject; ?></title>
</head>
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">
<div style="width: 680px;">
  <div>
    <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_greeting; ?></p>
   
    <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_message; ?></p> 	
    <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_message1; ?></p>
    <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_alert; ?></p>
     
    <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_footer; ?></p>
    <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_thanks; ?></p>
    <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $team; ?></p>
  </div>
</div>
</body>
</html>
