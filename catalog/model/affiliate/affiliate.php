<?php
class ModelAffiliateAffiliate extends Model {
	public function addAffiliate($data) {
		$data_mission = '';
		$data_mission_french = '';
		
		if(empty($data['mission']))
		{
			$data_mission = $data['mission_french'];
			$data_mission_french = $data['mission_french'];
		}
		elseif(empty($data['mission_french']))
		{
			$data_mission = $data['mission'];
			$data_mission_french = $data['mission'];
		}
		else
		{
			$data_mission = $data['mission'];
			$data_mission_french = $data['mission_french'];
		}
		
		 
			
 

		$data['tax'] = '';
		$data['payment'] = 'cheque';
		$data['cheque'] = $data['company']; 
		$data['paypal'] = '';
		$data['bank_name'] = '';
		$data['bank_branch_number'] = '';
		$data['bank_swift_code'] = '';
		$data['bank_account_name'] = '';
		$data['bank_account_number'] = '';
 
		$code = preg_replace('/\s+/', '', $data['company']);
		$this->db->query("INSERT INTO `" . DB_PREFIX . "url_alias` SET  query = 'common/home', keyword = '" . $this->db->escape(strtolower($code)) . "'");
		
		if (!preg_match("/^\([0-9]{3}\) [0-9]{3}-[0-9]{4}$/i", $data['telephone'])){
			
			if(  preg_match( '/([0-9]{3})([0-9]{3})([0-9]{4})/', $data['telephone'],  $matches ) )
			{
				$data['telephone'] = '('.$matches[1] . ') ' .$matches[2] . '-' . $matches[3];
			}
		}
		
		if(isset($data['ifs'])) {
			if($data['ifs'] == 1) {
				$member_type = 2;
			} else{
				$member_type = 2;
			}
		}else{
			$member_type = 1;
		}



		$this->db->query("INSERT INTO " . DB_PREFIX . "affiliate SET member_type = '1', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', extension='".$this->db->escape($data['extension'])."', fax = '" . $this->db->escape($data['fax']) . "', salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', company = '" . $this->db->escape($data['company']) . "',`custom_logo` = '" . $this->db->escape($data['custom_logo']) . "',`mission` = '" . $this->db->escape($data_mission) . "', `mission_french` = '" . $this->db->escape($data_mission_french) . "',`slider_image` = '" . $this->db->escape($data['slider_image']) . "',website = '" . $this->db->escape($data['website']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', city = '" . $this->db->escape($data['city']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = '" . (int)$data['country_id'] . "', zone_id = '" . (int)$data['zone_id'] . "', code = '" . $this->db->escape($code) . "', commission = '" . (float)$this->config->get('config_affiliate_commission') . "', tax = '" . $this->db->escape($data['tax']) . "', payment = '" . $this->db->escape($data['payment']) . "', cheque = '" . $this->db->escape($data['cheque']) . "', paypal = '" . $this->db->escape($data['paypal']) . "', bank_name = '" . $this->db->escape($data['bank_name']) . "', bank_branch_number = '" . $this->db->escape($data['bank_branch_number']) . "', bank_swift_code = '" . $this->db->escape($data['bank_swift_code']) . "', bank_account_name = '" . $this->db->escape($data['bank_account_name']) . "', bank_account_number = '" . $this->db->escape($data['bank_account_number']) . "', status = '1', approved = '" . (int)!$this->config->get('config_affiliate_approval') . "', date_added = NOW()");

		$affiliate_id = $this->db->getLastId();

		
		$hear_about_us = '';
		if(isset($data['hear_about_us'])) 
		{
			$hear_about_us  = implode(', ' , $data['hear_about_us']);
		}


		$get_aggrement = '';
		if(isset($data['agreement'])){
			$i=0;
			foreach($data['agreement'] as $agreement)
			{
				$i++;
				if($i == count($data['agreement']))
				{
					$get_aggrement .= $agreement;
				}
				else
				{
					$get_aggrement .= $agreement.'-';
				}
			}
		}
		$other_hear_about_us = '';
		if (isset($data['other_hear_about_us']))
		{
			$other_hear_about_us = $data['other_hear_about_us'];
		}
		
		$other_details_input = '' ;
		if(isset($data['other_details'])){
			$other_details_input = $data['other_details_input'];
		}
		
		$data_organization_detailed = '';
		$data_organization_detailed_mission_french = '';
		if(empty($data['organization_detailed']))
		{
			$data_organization_detailed = $data['organization_detailed_french'];
			$data_organization_detailed_mission_french = $data['organization_detailed_french'];
		}
		elseif(empty($data['organization_detailed_french']))
		{
			$data_organization_detailed = $data['organization_detailed'];
			$data_organization_detailed_mission_french = $data['organization_detailed'];
		}
		else
		{
			$data_organization_detailed = $data['organization_detailed'];
			$data_organization_detailed_mission_french = $data['organization_detailed_french'];
		}

		if (!preg_match("/^\([0-9]{3}\) [0-9]{3}-[0-9]{4}$/i", $data['organization_phone_number'])){
			
			if(  preg_match( '/([0-9]{3})([0-9]{3})([0-9]{4})/', $data['organization_phone_number'],  $matches ) )
			{
				$data['organization_phone_number'] = '('.$matches[1] . ') ' .$matches[2] . '-' . $matches[3];
			}
		}

		$this->db->query("INSERT INTO " . DB_PREFIX . "affiliates_fundraisers SET affiliate_id = '" . (int)$affiliate_id . "',position_in_organization = '" . $this->db->escape($data['position_in_organization']) . "', organization_phone_number = '" . $this->db->escape($data['organization_phone_number']) . "', branded_website = '" . $this->db->escape($data['branded_website']). "', organization_hashtags = '" . $this->db->escape($data['organization_hashtags']). "', organization_facebook_link = '" . $this->db->escape($data['organization_facebook_link']). "', organization_instagram = '" . $this->db->escape($data['organization_instagram']). "', organization_twitter = '" . $this->db->escape($data['organization_twitter']). "', organization_linkedIn_link = '" . $this->db->escape($data['organization_linkedIn_link']). "', agreement = '" .$get_aggrement. "', organization_detailed = '" . $this->db->escape($data_organization_detailed). "', organization_detailed_french = '" . $this->db->escape($data_organization_detailed_mission_french). "', hear_about_us = '" . $hear_about_us. "', other_hear_about_us = '" . $other_hear_about_us. "',other_details_input = '" . $other_details_input . "'");

	 	
		$this->load->language('mail/affiliate');

		$subject = sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));

		$message  = sprintf($this->language->get('text_welcome'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) . "\n\n";

		if (!$this->config->get('config_affiliate_approval')) {
			$message .= $this->language->get('text_login') . "\n";
		} else {
			$message .= $this->language->get('text_approval') . "\n";
		}

		$message .= $this->url->link('affiliate/login', '', true) . "\n\n";
		$message .= $this->language->get('text_services') . "\n\n";
		$message .= $this->language->get('text_thanks') . "\n";
		$message .= html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8');

		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

		$mail->setTo($this->request->post['email']);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
		$mail->setSubject($subject);
		$mail->setText($message);
		//$mail->send();

		// Send to main admin email if new affiliate email is enabled
		if (in_array('affiliate', (array)$this->config->get('config_mail_alert'))) {
			$message  = $this->language->get('text_signup') . "\n\n";
			$message .= $this->language->get('text_store') . ' ' . html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8') . "\n";
			if(isset($this->request->cookie['tracking']))
			{
				$query = $this->db->query("SELECT * FROM ".DB_PREFIX."affiliate  WHERE code = '".$this->request->cookie['tracking']."'");
				if($query->num_rows > '0')
				{
				
					$message .= $this->language->get('joined_under') . ' ' . ucwords($query->row['firstname']) . ' ' . ucwords($query->row['lastname']) . "\n";
				}	
			}
			$message .= $this->language->get('text_firstname') . ' ' . $data['firstname'] . "\n";
			$message .= $this->language->get('text_lastname') . ' ' . $data['lastname'] . "\n";

			if ($data['website']) {
				$message .= $this->language->get('text_website') . ' ' . $data['website'] . "\n";
			}

			if ($data['company']) {
				$message .= $this->language->get('text_company') . ' '  . $data['company'] . "\n";
			}

			$message .= $this->language->get('text_email') . ' '  .  $data['email'] . "\n";
			$message .= $this->language->get('text_telephone') . ' ' . $data['telephone'] . "\n";

			$mail->setTo($this->config->get('config_email'));
			$mail->setSubject(html_entity_decode($this->language->get('text_new_affiliate'), ENT_QUOTES, 'UTF-8'));
			$mail->setText($message);
			//$mail->send();

			// Send to additional alert emails if new affiliate email is enabled
			$emails = explode(',', $this->config->get('config_alert_email'));

			foreach ($emails as $email) {
				if (utf8_strlen($email) > 0 && filter_var($email, FILTER_VALIDATE_EMAIL)) {
					$mail->setTo($email);
					//$mail->send();
				}
			}
		}
		
		// send mail to referring affiliate
		if(isset($this->request->cookie['tracking']))
		{
			$query = $this->db->query("SELECT * FROM ".DB_PREFIX."affiliate  WHERE code = '".$this->request->cookie['tracking']."'");
			if($query->num_rows > '0')
			{
				
				$subject = sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));

				$message  = sprintf($this->language->get('text_welcome_referring'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) . "\n\n";

				$message .= $this->language->get('text_firstname') . ' ' . $data['firstname'] . "\n";
				$message .= $this->language->get('text_lastname') . ' ' . $data['lastname'] . "\n";
				$message .= $this->language->get('text_email') . ' '  .  $data['email'] . "\n";
				$message .= $this->language->get('text_organization') . ' '  . $data['company'] . "\n";
				$message .= $this->language->get('text_telephone') . ' ' . $data['telephone'] . "\n";
				$message .= $this->language->get('date') . ' ' . date("m/d/Y") . "\n";
				
				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
				$mail->smtp_username = $this->config->get('config_mail_smtp_username');
				$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
				$mail->smtp_port = $this->config->get('config_mail_smtp_port');
				$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

				$mail->setTo($query->row['email']);
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
				$mail->setSubject($subject);
				$mail->setText($message);
				//$mail->send();
			}
			
		} 

		return $affiliate_id;
	}

	public function editAffiliate($data) {
		
		
		$affiliate_id = $this->affiliate->getId();

		$this->db->query("UPDATE " . DB_PREFIX . "affiliate SET firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "',`custom_logo` = '" . $this->db->escape($data['custom_logo']) . "',`mission` = '" . $this->db->escape($data['mission']) . "', `mission_french` = '" . $this->db->escape($data['mission_french']) . "',`slider_image` = '" . $this->db->escape($data['slider_image']) . "', website = '" . $this->db->escape($data['website']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', city = '" . $this->db->escape($data['city']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = '" . (int)$data['country_id'] . "', zone_id = '" . (int)$data['zone_id'] . "' WHERE affiliate_id = '" . (int)$affiliate_id . "'");
	}

	public function editPayment($data) {
		$affiliate_id = $this->affiliate->getId();

		$this->db->query("UPDATE " . DB_PREFIX . "affiliate SET tax = '" . $this->db->escape($data['tax']) . "', payment = '" . $this->db->escape($data['payment']) . "', cheque = '" . $this->db->escape($data['cheque']) . "', paypal = '" . $this->db->escape($data['paypal']) . "', bank_name = '" . $this->db->escape($data['bank_name']) . "', bank_branch_number = '" . $this->db->escape($data['bank_branch_number']) . "', bank_swift_code = '" . $this->db->escape($data['bank_swift_code']) . "', bank_account_name = '" . $this->db->escape($data['bank_account_name']) . "', bank_account_number = '" . $this->db->escape($data['bank_account_number']) . "' WHERE affiliate_id = '" . (int)$affiliate_id . "'");
	}

	public function editPassword($email, $password) {
		$this->db->query("UPDATE " . DB_PREFIX . "affiliate SET salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($password)))) . "' WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	}

	public function getAffiliate($affiliate_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "affiliate WHERE affiliate_id = '" . (int)$affiliate_id . "'");

		return $query->row;
	}
	
	public function getAffiliateByEmail($email) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "affiliate WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row;
	}

	public function getAffiliateByCode($code) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "affiliate WHERE code = '" . $this->db->escape($code) . "'");

		return $query->row;
	}
	
	public function getChildAffiliates($parent_affiliate_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX ."mta_affiliate WHERE parent_affiliate_id = '" . $this->db->escape($parent_affiliate_id) . "'");
		return $query->rows;
	}
	public function getChildAffiliatesDetail($parent_affiliate_id) {
		$query = $this->db->query("SELECT af.*,mt_af.* FROM " . DB_PREFIX ."mta_affiliate mt_af,". DB_PREFIX ."affiliate af WHERE mt_af.parent_affiliate_id = '" . $this->db->escape($parent_affiliate_id) . "' AND af.affiliate_id = mt_af.affiliate_id AND af.approved =1");
		return $query->rows;
	}


	public function getAllAffiliates() {

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "affiliate WHERE approved=1");
		$affiliates = array();
		foreach($query->rows as $result){
		    $affiliates[] = $result;
		}
		return $affiliates;

	}

	public function getTotalAffiliatesByEmail($email) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "affiliate WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row['total'];
	}

	public function addTransaction($affiliate_id, $amount = '', $order_id = 0) {
		$affiliate_info = $this->getAffiliate($affiliate_id);

		if ($affiliate_info) {
			$this->load->language('mail/affiliate');

			$this->db->query("INSERT INTO " . DB_PREFIX . "affiliate_transaction SET affiliate_id = '" . (int)$affiliate_id . "', order_id = '" . (float)$order_id . "', description = '" . $this->db->escape($this->language->get('text_order_id') . ' #' . $order_id) . "', amount = '" . (float)$amount . "', date_added = NOW()");

			$affiliate_transaction_id = $this->db->getLastId();

			/*$message  = sprintf($this->language->get('text_transaction_received'), $this->currency->format($amount, $this->config->get('config_currency'))) . "\n\n";
			$message .= sprintf($this->language->get('text_transaction_total'), $this->currency->format($this->getTransactionTotal($affiliate_id), $this->config->get('config_currency')));

			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$mail->setTo($affiliate_info['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
			$mail->setSubject(sprintf($this->language->get('text_transaction_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')));
			$mail->setText($message);
			$mail->send();*/

			return $affiliate_transaction_id;
		}
	}

	public function deleteTransaction($order_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "affiliate_transaction WHERE order_id = '" . (int)$order_id . "'");
	}

	public function getTransactionTotal($affiliate_id) {
		$query = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "affiliate_transaction WHERE affiliate_id = '" . (int)$affiliate_id . "'");

		return $query->row['total'];
	}
	
	public function addLoginAttempt($email) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "affiliate_login WHERE email = '" . $this->db->escape(utf8_strtolower((string)$email)) . "' AND ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "'");

		if (!$query->num_rows) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "affiliate_login SET email = '" . $this->db->escape(utf8_strtolower((string)$email)) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', total = 1, date_added = '" . $this->db->escape(date('Y-m-d H:i:s')) . "', date_modified = '" . $this->db->escape(date('Y-m-d H:i:s')) . "'");
		} else {
			$this->db->query("UPDATE " . DB_PREFIX . "affiliate_login SET total = (total + 1), date_modified = '" . $this->db->escape(date('Y-m-d H:i:s')) . "' WHERE affiliate_login_id = '" . (int)$query->row['affiliate_login_id'] . "'");
		}
	}

	public function getLoginAttempts($email) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "affiliate_login` WHERE email = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row;
	}

	public function deleteLoginAttempts($email) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "affiliate_login` WHERE email = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	}	
	public function getCompanyName($name) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "affiliate WHERE LOWER(company) = '" . $this->db->escape(utf8_strtolower($name)) . "'");

		return $query->row['total'];
	}


	public function setApprovel($affiliate_id,$approved = 0) {
		$this->db->query("UPDATE " . DB_PREFIX . "affiliate SET approved = '".$approved."' WHERE affiliate_id = '" . (int)$affiliate_id . "'");
		return true;

	}

	public function affiliateType($affiliate_id) {

		$query = $this->db->query("SELECT af.firstname FROM " . DB_PREFIX ."mta_affiliate mt_af,". DB_PREFIX ."affiliate af WHERE af.affiliate_id = '" . $this->db->escape($affiliate_id) . "' AND af.affiliate_id = mt_af.affiliate_id AND mt_af.level_original =2");
		return $query;

	}

	public function UpdateAffiliateIfs($affiliate_id) {
		 $this->db->query ("Update " . DB_PREFIX ."mta_affiliate set mta_scheme_id = NULL , parent_affiliate_id = '0'  , all_parent_ids = '' , level_original = 1 where affiliate_id='" . $affiliate_id . "' " );  
		 return true;
 
	}

	public function getAffiliatefundraisers($affiliate_id) {
		$query = $this->db->query("SELECT *  FROM " . DB_PREFIX . "affiliates_fundraisers WHERE affiliate_id = '" . (int)$affiliate_id . "'");
		return $query->row;
	}

}