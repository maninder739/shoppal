<?php
class ModelAffiliateTransaction extends Model {
	public function getTransactions($data = array()) {
		$sql = "SELECT oc_affiliate_transaction.order_id as order_id, oc_affiliate_transaction.affiliate_id as affiliate_id, total, commission, oc_affiliate_transaction.date_added as date_added   FROM `" . DB_PREFIX . "affiliate_transaction` INNER JOIN `" . DB_PREFIX . "order` ON oc_affiliate_transaction.order_id = oc_order.order_id WHERE oc_affiliate_transaction.affiliate_id = '" . (int)$this->affiliate->getId() . "'";

		$sort_data = array(
			'oc_affiliate_transaction.amount',
			'oc_affiliate_transaction.date_added'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY oc_affiliate_transaction.date_added";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	

		 
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalTransactions() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "affiliate_transaction` WHERE affiliate_id = '" . (int)$this->affiliate->getId() . "'");

		return $query->row['total'];
	}

	public function getBalance() {
		$query = $this->db->query("SELECT SUM(amount) AS total FROM `" . DB_PREFIX . "affiliate_transaction` WHERE affiliate_id = '" . (int)$this->affiliate->getId() . "' GROUP BY affiliate_id");

		if ($query->num_rows) {
			return $query->row['total'];
		} else {
			return 0;
		}
	}

	public function getTotalRetails($order_id) {
		$query = $this->db->query("SELECT SUM(total) AS total FROM `" . DB_PREFIX . "order_product` WHERE order_id = '" . (int)$order_id . "' GROUP BY order_id"); 
		return $query->row['total'];
	}

 
}