<?php

class ModelAccountCustomer extends Model {

    public function addCustomer($data) {
        if (isset($data['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($data['customer_group_id'], $this->config->get('config_customer_group_display'))) {
            $customer_group_id = $data['customer_group_id'];
        } else {
            $customer_group_id = $this->config->get('config_customer_group_id');
        }

        $this->load->model('account/customer_group');

        $customer_group_info = $this->model_account_customer_group->getCustomerGroup($customer_group_id);

        $hear_about_us = '';
        if (isset($data['hear_about_us'])) {
            $hear_about_us = implode(', ', $data['hear_about_us']);
        }

        $referred_by = '';
        if (isset($data['referred_by'])) {
            $referred_by = $data['referred_by'];
        }

        if (!preg_match("/^\([0-9]{3}\) [0-9]{3}-[0-9]{4}$/i", $data['telephone'])) {

            if (preg_match('/([0-9]{3})([0-9]{3})([0-9]{4})/', $data['telephone'], $matches)) {
                $data['telephone'] = '(' . $matches[1] . ') ' . $matches[2] . '-' . $matches[3];
            }
        }

        $this->db->query("INSERT INTO " . DB_PREFIX . "customer SET customer_group_id = '" . (int) $customer_group_id . "', store_id = '" . (int) $this->config->get('config_store_id') . "', language_id = '" . (int) $this->config->get('config_language_id') . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', custom_field = '" . $this->db->escape(isset($data['custom_field']['account']) ? json_encode($data['custom_field']['account']) : '') . "', salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', newsletter = '" . (isset($data['newsletter']) ? (int) $data['newsletter'] : 0) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', status = '1', approved = '" . (int) !$customer_group_info['approval'] . "',hear_about_us = '" . $hear_about_us . "', referred_by = '" . $referred_by . "' , date_added = NOW()");

        $customer_id = $this->db->getLastId();

        $this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int) $customer_id . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', company = '" . $this->db->escape($data['company']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', city = '" . $this->db->escape($data['city']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = '" . (int) $data['country_id'] . "', zone_id = '" . (int) $data['zone_id'] . "', custom_field = '" . $this->db->escape(isset($data['custom_field']['address']) ? json_encode($data['custom_field']['address']) : '') . "'");

        $address_id = $this->db->getLastId();

        $this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int) $address_id . "' WHERE customer_id = '" . (int) $customer_id . "'");

        $this->load->language('mail/customer');

        $subject = sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));

        $message = sprintf($this->language->get('text_welcome'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) . "\n\n";


        if (!$customer_group_info['approval']) {
            $message .= $this->language->get('text_login') . "\n";
        } else {
            $message .= $this->language->get('text_approval') . "\n";
        }

        $message .= $this->url->link('account/login', '', true) . "\n\n";
        $message .= $this->language->get('text_services') . "\n\n";
        if (isset($this->request->cookie['tracking'])) {


            $query = $this->db->query("SELECT af.affiliate_id,af.company,af.lastname,af.firstname,mta.level_original,mta.parent_affiliate_id FROM " . DB_PREFIX . "mta_affiliate mta," . DB_PREFIX . "affiliate af WHERE af.code = '" . $this->request->cookie['tracking'] . "' AND mta.affiliate_id = af.affiliate_id");
            if ($query->num_rows > '0') {
                $message .= $this->language->get('supporting_thanks') . ' ' . ucwords($query->row['company']) . ".\n";

                /* if($query->row['level_original'] == '1')
                  {

                  $message .= $this->language->get('supporting_thanks') . ' ' . ucwords($query->row['company']) . ".\n";
                  }
                  elseif($query->row['level_original'] == '2')
                  {
                  $query1 = $this->db->query("SELECT af.company FROM " . DB_PREFIX . "mta_affiliate mta,".DB_PREFIX."affiliate af WHERE mta.parent_affiliate_id = '".$query->row['parent_affiliate_id']."' AND mta.parent_affiliate_id = af.affiliate_id");

                  $message .= $this->language->get('supporting_thanks') . ' ' . ucwords($query1->row['company']) . ".\n";
                  }
                  else
                  {
                  $message .= $this->language->get('supporting_thanks') . ' ' .html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'). ".\n";
                  } */
            } else {
                $message .= $this->language->get('supporting_thanks') . ' ' . html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8') . ".\n";
            }
        } else {
            $message .= $this->language->get('supporting_thanks') . ' ' . html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8') . ".\n";
        }

        $mail = new Mail();
        $mail->protocol = $this->config->get('config_mail_protocol');
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

        $mail->setTo($data['email']);
        $mail->setFrom($this->config->get('config_email'));
        $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
        $mail->setSubject($subject);
        $mail->setText($message);
        //$mail->send();
        // Send to main admin email if new account email is enabled
        if (in_array('account', (array) $this->config->get('config_mail_alert'))) {
            $message = $this->language->get('text_signup') . "\n\n";
            $message .= $this->language->get('text_website') . ' ' . html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8') . "\n";
            if (isset($this->request->cookie['tracking'])) {
                $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "affiliate  WHERE code = '" . $this->request->cookie['tracking'] . "'");
                if ($query->num_rows > '0') {

                    $message .= $this->language->get('joined_under') . ' ' . ucwords($query->row['firstname']) . ' ' . ucwords($query->row['lastname']) . "\n";
                    $message .= $this->language->get('supporting_thanks') . ' ' . ucwords($query->row['company']) . ".\n";
                }
            }

            $message .= $this->language->get('text_firstname') . ' ' . $data['firstname'] . "\n";
            $message .= $this->language->get('text_lastname') . ' ' . $data['lastname'] . "\n";
            $message .= $this->language->get('text_customer_group') . ' ' . $customer_group_info['name'] . "\n";
            $message .= $this->language->get('text_email') . ' ' . $data['email'] . "\n";
            $message .= $this->language->get('text_telephone') . ' ' . $data['telephone'] . "\n";


            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
            $mail->smtp_username = $this->config->get('config_mail_smtp_username');
            $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
            $mail->smtp_port = $this->config->get('config_mail_smtp_port');
            $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

            $mail->setTo($this->config->get('config_email'));
            $mail->setFrom($this->config->get('config_email'));
            $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
            $mail->setSubject(html_entity_decode($this->language->get('text_new_customer'), ENT_QUOTES, 'UTF-8'));
            $mail->setText($message);
            //$mail->send();
            // Send to additional alert emails if new account email is enabled
            $emails = explode(',', $this->config->get('config_alert_email'));

            foreach ($emails as $email) {
                if (utf8_strlen($email) > 0 && filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $mail->setTo($email);
                    //$mail->send();
                }
            }
        }

        return $customer_id;
    }

    public function editCustomer($data) {
        $customer_id = $this->customer->getId();

        $this->db->query("UPDATE " . DB_PREFIX . "customer SET firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', custom_field = '" . $this->db->escape(isset($data['custom_field']) ? json_encode($data['custom_field']) : '') . "' WHERE customer_id = '" . (int) $customer_id . "'");
    }

    public function editPassword($email, $password) {
        $this->db->query("UPDATE " . DB_PREFIX . "customer SET salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($password)))) . "', code = '' WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
    }

    public function editCode($email, $code) {
        $this->db->query("UPDATE `" . DB_PREFIX . "customer` SET code = '" . $this->db->escape($code) . "' WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
    }

    public function editNewsletter($newsletter) {
        $this->db->query("UPDATE " . DB_PREFIX . "customer SET newsletter = '" . (int) $newsletter . "' WHERE customer_id = '" . (int) $this->customer->getId() . "'");
    }

    public function getCustomer($customer_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int) $customer_id . "'");

        return $query->row;
    }

    public function getCustomerByEmail($email) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

        return $query->row;
    }

    public function getCustomerByCode($code) {
        $query = $this->db->query("SELECT customer_id, firstname, lastname, email FROM `" . DB_PREFIX . "customer` WHERE code = '" . $this->db->escape($code) . "' AND code != ''");

        return $query->row;
    }

    public function getCustomerByToken($token) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE token = '" . $this->db->escape($token) . "' AND token != ''");

        $this->db->query("UPDATE " . DB_PREFIX . "customer SET token = ''");

        return $query->row;
    }

    public function getTotalCustomersByEmail($email) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

        return $query->row['total'];
    }

    public function getRewardTotal($customer_id) {
        $query = $this->db->query("SELECT SUM(points) AS total FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int) $customer_id . "'");

        return $query->row['total'];
    }

    public function getIps($customer_id) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_ip` WHERE customer_id = '" . (int) $customer_id . "'");

        return $query->rows;
    }

    public function addLoginAttempt($email) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_login WHERE email = '" . $this->db->escape(utf8_strtolower((string) $email)) . "' AND ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "'");

        if (!$query->num_rows) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "customer_login SET email = '" . $this->db->escape(utf8_strtolower((string) $email)) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', total = 1, date_added = '" . $this->db->escape(date('Y-m-d H:i:s')) . "', date_modified = '" . $this->db->escape(date('Y-m-d H:i:s')) . "'");
        } else {
            $this->db->query("UPDATE " . DB_PREFIX . "customer_login SET total = (total + 1), date_modified = '" . $this->db->escape(date('Y-m-d H:i:s')) . "' WHERE customer_login_id = '" . (int) $query->row['customer_login_id'] . "'");
        }
    }

    public function getLoginAttempts($email) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_login` WHERE email = '" . $this->db->escape(utf8_strtolower($email)) . "'");

        return $query->row;
    }

    public function deleteLoginAttempts($email) {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "customer_login` WHERE email = '" . $this->db->escape(utf8_strtolower($email)) . "'");
    }

    public function addFundraising($data) {

        if (isset($data['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($data['customer_group_id'], $this->config->get('config_customer_group_display'))) {
            $customer_group_id = $data['customer_group_id'];
        } else {
            $customer_group_id = $this->config->get('config_customer_group_id');
        }

        $this->load->model('account/customer_group');

        $customer_group_info = $this->model_account_customer_group->getCustomerGroup($customer_group_id);

        $this->db->query("INSERT INTO " . DB_PREFIX . "customer SET customer_group_id = '" . (int) $customer_group_id . "', store_id = '" . (int) $this->config->get('config_store_id') . "', language_id = '" . (int) $this->config->get('config_language_id') . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', status = '" . (int) !$customer_group_info['approval'] . "', date_added = NOW()");

        $customer_id = $this->db->getLastId();

        if ($customer_group_info['approval']) {
            $this->db->query("INSERT INTO `" . DB_PREFIX . "customer_approval` SET customer_id = '" . (int) $customer_id . "', type = 'customer', date_added = NOW()");
        }
        $hear_about_us = '';
        if (isset($data['hear_about_us'])) {
            $hear_about_us = $data['hear_about_us'];
            if ($data['hear_about_us'] == 'other') {
                $hear_about_us = $data['other_hear_about_us'];
            }
        }
        $get_aggrement = '';
        if (isset($data['agreement'])) {
            $i = 0;
            foreach ($data['agreement'] as $agreement) {
                $i++;
                if ($i == count($data['agreement'])) {
                    $get_aggrement .= $agreement;
                } else {
                    $get_aggrement .= $agreement . '-';
                }
            }
        }

        $this->db->query("INSERT INTO " . DB_PREFIX . "customer_fundraisers SET customer_id = '" . (int) $customer_id . "', organization_name = '" . $this->db->escape($data['organization_name']) . "', position_in_organization = '" . $this->db->escape($data['position_in_organization']) . "', mobile_number = '" . $this->db->escape($data['mobile_number']) . "', organization_phone_number = '" . $this->db->escape($data['organization_phone_number']) . "', street_address = '" . $this->db->escape($data['street_address']) . "', street_address_line2 = '" . $this->db->escape($data['street_address_line2']) . "', city = '" . $this->db->escape($data['city']) . "', zone_id = '" . $this->db->escape($data['zone_id']) . "', zip_code = '" . $this->db->escape($data['zip_code']) . "', country_id = '" . $this->db->escape($data['country_id']) . "', branded_website = '" . $this->db->escape($data['branded_website']) . "', organization_website = '" . $this->db->escape($data['organization_website']) . "', organization_hashtags = '" . $this->db->escape($data['organization_hashtags']) . "', organization_facebook_link = '" . $this->db->escape($data['organization_facebook_link']) . "', organization_instagram = '" . $this->db->escape($data['organization_instagram']) . "', organization_twitter = '" . $this->db->escape($data['organization_twitter']) . "', organization_linkedIn_link = '" . $this->db->escape($data['organization_linkedIn_link']) . "', agreement = '" . $get_aggrement . "', organization_mission_statement = '" . $this->db->escape($data['organization_mission_statement']) . "', organization_detailed = '" . $this->db->escape($data['organization_detailed']) . "', hear_about_us = '" . $this->db->escape($hear_about_us) . "', logo = '" . $data['logo'] . "'");

        return $customer_id;
    }

    public function getAffiliateCode($customer_id) {
        $query = $this->db->query("SELECT cus.firstname,af.code FROM " . DB_PREFIX . "customer cus," . DB_PREFIX . "affiliate af WHERE cus.customer_id = '" . $this->db->escape($customer_id) . "' AND cus.affiliate_id = af.affiliate_id");
        $code = '';
        if ($query->num_rows > '0') {
            $code = $query->row['code'];
        }
        return $code;
    }

    public function getTotalCustomers($affiliate_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "customer` o WHERE affiliate_id = '" . (int) $affiliate_id . "'");

        return $query->row['total'];
    }

    public function getCustomers($affiliate_id) {
        $query = $this->db->query("SELECT customer_id,firstname,lastname FROM " . DB_PREFIX . "customer WHERE affiliate_id = '" . $this->db->escape($affiliate_id) . "' ORDER BY customer_id DESC");
        return (array) $query;
    }

    //and os.order_status_id = '3'
    public function getCustomerOrder($customer_id, $current_month, $current_year) {
        if (empty($current_month)) {
            $query = $this->db->query("SELECT os.name,o.order_id,o.customer_id,o.total,o.commission,o.date_added FROM " . DB_PREFIX . "order o right join " . DB_PREFIX . "order_status os on o.order_status_id=os.order_status_id WHERE o.customer_id = '" . $this->db->escape($customer_id) . "'  and os.language_id = " . (int) $this->config->get('config_language_id') . " ORDER BY o.date_added DESC");
        } else {
            $query = $this->db->query("SELECT os.name,o.order_id,o.customer_id,o.total,o.commission,o.date_added FROM " . DB_PREFIX . "order o right join " . DB_PREFIX . "order_status os on o.order_status_id=os.order_status_id WHERE  o.customer_id = '" . $this->db->escape($customer_id) . "'  and os.language_id = " . (int) $this->config->get('config_language_id') . " AND Year(o.date_added) = '" . $current_year . "' AND MONTH(o.date_added) = '" . $current_month . "' ORDER BY o.date_added DESC");
        }

        return (array) $query;
    }

    public function updateAffiliateid($customer_id) {

        $res = $this->db->query("SELECT affiliate_id FROM " . DB_PREFIX . "customer WHERE `customer_id`= '" . (int) $customer_id . "' ");
        if ($res->num_rows > 0) {
            $res_default_member = $this->db->query("SELECT affiliate_member_id FROM " . DB_PREFIX . "default_member WHERE `affiliate_id`= '" . (int) $res->row['affiliate_id'] . "' ");

            if ($res_default_member->num_rows > '0') {
                $this->db->query("UPDATE " . DB_PREFIX . "customer SET affiliate_id = '" . (int) $res_default_member->row['affiliate_member_id'] . "' WHERE customer_id = '" . (int) $customer_id . "' ");
            }
        }

        return true;
    }

    public function getTotalRetails($order_id) {

        $query = $this->db->query("SELECT SUM(total) AS total FROM `" . DB_PREFIX . "order_product` WHERE order_id = '" . (int) $order_id . "' GROUP BY order_id");
        return $query->row['total'];
    }

}
