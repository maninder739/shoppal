<?php
class ModelAccountReturn extends Model {
	public function addReturn($data) {
		 $this->db->query("INSERT INTO `" . DB_PREFIX . "return` SET order_id = '" . (int)$data['order_id'] . "', customer_id = '" . (int)$this->customer->getId() . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', product = '" . $this->db->escape($data['product']) . "', model = '" . $this->db->escape($data['model']) . "', quantity = '" . (int)$data['quantity'] . "', opened = '" . (int)$data['opened'] . "', return_reason_id = '" . (int)$data['return_reason_id'] . "', return_status_id = '" . (int)$this->config->get('config_return_status_id') . "', comment = '" . $this->db->escape($data['comment']) . "', date_ordered = '" . $this->db->escape($data['date_ordered']) . "', date_added = NOW(), date_modified = NOW()");

     	$return_id = $this->db->getLastId();

			 

		$this->load->language('mail/return');
		
        $message .= html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8') . "\n";
        $message = sprintf($this->language->get('text_order_id'), $data['order_id']). "\n";
        $message .= sprintf($this->language->get('text_subject'), $this->db->escape($data['firstname']) .' ' . $this->db->escape($data['lastname']) .',' ) . "\n"; 
        $message .= $this->language->get('text_body') . "\n";
        $message .= $this->language->get('text_review') . "\n";
        $message .= $this->language->get('text_thanks') . "\n";
       
        $mail = new Mail();
        $mail->protocol = $this->config->get('config_mail_protocol');
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

        $mail->setTo($data['email']);
        $mail->setFrom($this->config->get('config_email'));
        $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
        $mail->setSubject(sprintf($this->language->get('text_approve_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')));
        $mail->setText($message);
        $mail->send();


        //Send mail to admin

 

        $order_option_query = $this->db->query("SELECT rr.name as name FROM " . DB_PREFIX . "return as r inner join " . DB_PREFIX . "return_reason as rr on r.return_status_id = rr.return_reason_id WHERE r.return_id  = '" . (int)$return_id . "'  ");
        
 

        //$admin_mail_data .= html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8') . "\n";        
        $subject = sprintf($this->language->get('admin_approve_subject'),  $this->db->escape($data['firstname']) .' ' . $this->db->escape($data['lastname']) .',' ) . "\n"; 
       
        $admin_mail_data .= $this->language->get('admin_subject') . "\n\n";
        $admin_mail_data .= $this->language->get('customer_name') . ' ' . $this->db->escape($data['firstname']) .' ' . $this->db->escape($data['lastname']) . "\n";
        $admin_mail_data .= $this->language->get('customer_email'). ' ' . $this->db->escape($data['email']) . "\n";
        $admin_mail_data .= $this->language->get('customer_phone') . ' ' . $this->db->escape($data['telephone']) . "\n\n";
       
        $admin_mail_data .= $this->language->get('admin_order_id') . ' ' . (int)$data['order_id'] . "\n";
        $admin_mail_data .= $this->language->get('admin_order_date') . ' ' . $this->db->escape($data['date_ordered']) . "\n";
        $admin_mail_data .= $this->language->get('admin_order_product') . ' ' . $this->db->escape($data['product']) . "\n";
     	$admin_mail_data .= $this->language->get('admin_order_product_model') . ' ' . $this->db->escape($data['model']) . "\n\n";
      
        $admin_mail_data .= $this->language->get('admin_order_return_reason') . ' ' . $order_option_query->row['name'] . "\n";
        $admin_mail_data .= $this->language->get('admin_order_review_admin') ;
       	
         
       
        $mail = new Mail();
        $mail->protocol = $this->config->get('config_mail_protocol');
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

        $mail->setTo('maninder@nascenture.com');
        $mail->setFrom($this->config->get('config_email'));
        $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
        $mail->setSubject(sprintf($subject));
        $mail->setText($admin_mail_data);
        $mail->send();

		return $return_id;
	}

	public function getReturn($return_id) {
		$query = $this->db->query("SELECT r.return_id, r.order_id, r.firstname, r.lastname, r.email, r.telephone, r.product, r.model, r.quantity, r.opened, (SELECT rr.name FROM " . DB_PREFIX . "return_reason rr WHERE rr.return_reason_id = r.return_reason_id AND rr.language_id = '" . (int)$this->config->get('config_language_id') . "') AS reason, (SELECT ra.name FROM " . DB_PREFIX . "return_action ra WHERE ra.return_action_id = r.return_action_id AND ra.language_id = '" . (int)$this->config->get('config_language_id') . "') AS action, (SELECT rs.name FROM " . DB_PREFIX . "return_status rs WHERE rs.return_status_id = r.return_status_id AND rs.language_id = '" . (int)$this->config->get('config_language_id') . "') AS status, r.comment, r.date_ordered, r.date_added, r.date_modified FROM `" . DB_PREFIX . "return` r WHERE r.return_id = '" . (int)$return_id . "' AND r.customer_id = '" . $this->customer->getId() . "'");

		return $query->row;
	}

	public function getReturns($start = 0, $limit = 20) {
		if ($start < 0) {
			$start = 0;
		}

		if ($limit < 1) {
			$limit = 20;
		}

		$query = $this->db->query("SELECT r.return_id, r.order_id, r.firstname, r.lastname, rs.name as status, r.date_added FROM `" . DB_PREFIX . "return` r LEFT JOIN " . DB_PREFIX . "return_status rs ON (r.return_status_id = rs.return_status_id) WHERE r.customer_id = '" . $this->customer->getId() . "' AND rs.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY r.return_id DESC LIMIT " . (int)$start . "," . (int)$limit);

		return $query->rows;
	}

	public function getTotalReturns() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "return`WHERE customer_id = '" . $this->customer->getId() . "'");

		return $query->row['total'];
	}

	public function getReturnHistories($return_id) {
		$query = $this->db->query("SELECT rh.date_added, rs.name AS status, rh.comment FROM " . DB_PREFIX . "return_history rh LEFT JOIN " . DB_PREFIX . "return_status rs ON rh.return_status_id = rs.return_status_id WHERE rh.return_id = '" . (int)$return_id . "' AND rs.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY rh.date_added ASC");

		return $query->rows;
	}
}
