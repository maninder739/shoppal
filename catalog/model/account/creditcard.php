<?php

class ModelAccountCreditCard extends Model {

    public function getCreditCard($customer_id) {

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_sec WHERE customer_id = '" . (int) $customer_id . "'  ");

        return $query->row;
    }

    public function updateCreditCard($data = array()) {

        $cardReference = $data['cardReference'];
        $merchantReference = $data['MerchantReference'];
        $customer_id = $data['customer_id'];
        $cardType = $data['cardType'];
        $cardExpiry = $data['cardExpiry'];

        $query = $this->db->query("Update " . DB_PREFIX . "customer_sec SET `responseref`= '" . $cardReference . "' ,`responsecardtype` = '" . $cardType . "' , `cardexpiry` = '" . $cardExpiry . "', date_modified = NOW()  WHERE customer_id = '" . (int) $customer_id . "' AND merchantref =  '" . $merchantReference . "'  ");

        $query = $this->db->query("Update " . DB_PREFIX . "customer_sec SET cc_update_status = 1 WHERE customer_id = '" . (int) $customer_id . "'  ");
        return true;
    }

    //AND denied_order_autoship = 1
    public function getOrder($customer_id) {
        $newdate = date("Y-m-d", strtotime("-1 months"));
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order WHERE customer_id = '" . (int) $customer_id . "' AND date(date_added) BETWEEN '" . $newdate . "' AND '" . date('Y-m-d') . "'  AND order_status_id = '8' AND denied_order_autoship = '0' AND order_type = 'monthly'    ");

        return $query->rows;
    }

    public function creditCardStatus($customer_id) {
        $query = $this->db->query("SELECT date_modified FROM " . DB_PREFIX . "customer_sec WHERE customer_id = '" . (int) $customer_id . "'  ");
        return $query->row['date_modified'];
    }

    public function deniedOrderAutoshipStatus($order_id) {
        $query = $this->db->query("UPDATE `" . DB_PREFIX . "order` SET `denied_order_autoship` = '1' WHERE `order_id` = '" . (int) $order_id . "'  ");
        return true;
    }

}
