<?php
class ControllerShipworksApi extends Controller {

    public function index() {

        if(isset($_POST['username'])) {
            $user = $_POST['username'];
        } else {
            $user = "";
        }

        if(isset($_POST['password'])) {
            $pass = $_POST['password'];
        } else {
            $pass = "";
        }

        $this->load->model('shipworks/shipworks');
        if ($this->model_shipworks_shipworks->authenticate($user, $pass) && isset($_GET['store_id'])) {
            if(isset($_GET['store_id']) && $_GET['store_id'] != "") {
                if ($_POST['action'] == 'getmodule') {
                    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/shipworks/getmodule.tpl')) {
                        $this->template = $this->config->get('config_template') . '/template/shipworks/getmodule.tpl';
                    } else {
                        $this->template = 'default/template/shipworks/getmodule.tpl';
                    }
                    $this->response->setOutput($this->render());
                } else if ($_POST['action'] == 'getstore') {
                    $store_id = $_GET['store_id'];
                    if($store_id == 0) {
                        $this->data["website"] = HTTPS_SERVER;
                    }

                    $store_query = $this->model_shipworks_shipworks->getStoreSettings($store_id);

                    if(count($store_query->rows) <= 0) {
                        $this->data["msg"] = "err_store";
                        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/shipworks/errorstatus.tpl')) {
                            $this->template = $this->config->get('config_template') . '/template/shipworks/errorstatus.tpl';
                        } else {
                            $this->template = 'default/template/shipworks/errorstatus.tpl';
                        }
                        $this->response->setOutput($this->render());
                    } else {
                        foreach ($store_query->rows as $info_arr) {
                            if(!isset($this->data["website"])) {
                                if ($info_arr['key'] == 'config_url') {
                                    $this->data["website"] = $info_arr['value'];
                                }
                            }
                            if ($info_arr['key'] == 'config_name') {
                                $this->data["store_name"] = $info_arr['value'];
                            }
                            if ($info_arr['key'] == 'config_owner') {
                                $this->data["owner_name"] = $info_arr['value'];
                            }
                            if ($info_arr['key'] == 'config_address') {
                                $this->data["address"] = $info_arr['value'];
                            }
                            if ($info_arr['key'] == 'config_email') {
                                $this->data["email"] = $info_arr['value'];
                            }
                            if ($info_arr['key'] == 'config_telephone') {
                                $this->data["telephone"] = $info_arr['value'];
                            }
                            if ($info_arr['key'] == 'config_country_id') {
                                $store_county_query = $this->model_shipworks_shipworks->getCountryName($info_arr['value']);
                                if($store_county_query) {
                                    $this->data["country_id"] = $store_county_query->row["name"];
                                } else {
                                    $this->data["country_id"] = "";
                                }
                            }
                            if ($info_arr['key'] == 'config_zone_id') {
                                $store_zone_query = $this->model_shipworks_shipworks->getStateName($info_arr['value']);
                                if($store_zone_query) {
                                    $this->data["state_id"] = $store_zone_query->row["name"];
                                } else {
                                    $this->data["state_id"] = "";
                                }
                            }
                        }
                        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/shipworks/getstore.tpl')) {
                            $this->template = $this->config->get('config_template') . '/template/shipworks/getstore.tpl';
                        } else {
                            $this->template = 'default/template/shipworks/getstore.tpl';
                        }
                        $this->response->setOutput($this->render());
                    }
                } else if ($_POST['action'] == 'getstatuscodes') {
                    $status_query = $this->model_shipworks_shipworks->getStatusCodes();
                    $this->data["order_sratus"] = $status_query->rows;
                    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/shipworks/getstatuscodes.tpl')) {
                        $this->template = $this->config->get('config_template') . '/template/shipworks/getstatuscodes.tpl';
                    } else {
                        $this->template = 'default/template/shipworks/getstatuscodes.tpl';
                    }
                    $this->response->setOutput($this->render());
                } else if ($_POST['action'] == 'getcount') {
                    $start_date = $this->toLocalSqlDate($_POST['start']);
                    $end_date = date("Y-m-d H:i:s", time() - 2);
                    $count_order_query = $this->model_shipworks_shipworks->orderCount($start_date, $end_date);
                    $this->data["count"] = $count_order_query->row["count"];
                    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/shipworks/getcount.tpl')) {
                        $this->template = $this->config->get('config_template') . '/template/shipworks/getcount.tpl';
                    } else {
                        $this->template = 'default/template/shipworks/getcount.tpl';
                    }
                    $this->response->setOutput($this->render());
                } else if ($_POST['action'] == 'getorders') {

                    $start_date = $this->toLocalSqlDate($_POST['start']);
                    $end_date = date("Y-m-d H:i:s", time() - 2);

                    $orders_detail = array();
                    $orders = $this->model_shipworks_shipworks->getOrders($start_date, $end_date, $_GET['store_id']);
                    $last_modified = "";
                    $processed_order_id = "";
                    if(count($orders->rows) > 0) {
                        foreach($orders->rows as $order_arr) {

                            //Store Details
                            $orders_detail[$order_arr["order_id"]]['Store']['id'] = $order_arr["store_id"];
                            $orders_detail[$order_arr["order_id"]]['Store']['name'] = $order_arr["store_name"];
                            $orders_detail[$order_arr["order_id"]]['Store']['url'] = $order_arr["store_url"];

                            //Customer Details
                            $orders_detail[$order_arr["order_id"]]['Customer']['id'] = $order_arr["customer_id"];
                            $orders_detail[$order_arr["order_id"]]['Customer']['email'] = $order_arr["email"];
                            $orders_detail[$order_arr["order_id"]]['Customer']['telephone'] = $order_arr["telephone"];
                            $orders_detail[$order_arr["order_id"]]['Customer']['fax'] = $order_arr["fax"];

                            //Payment Details
                            $orders_detail[$order_arr["order_id"]]['payment']['first_name'] = $order_arr["payment_firstname"];
                            $orders_detail[$order_arr["order_id"]]['payment']['last_name'] = $order_arr["payment_lastname"];
                            $orders_detail[$order_arr["order_id"]]['payment']['payment_company'] = $order_arr["payment_company"];
                            $orders_detail[$order_arr["order_id"]]['payment']['payment_address_1'] = $order_arr["payment_address_1"];
                            $orders_detail[$order_arr["order_id"]]['payment']['payment_address_2'] = $order_arr["payment_address_2"];
                            $orders_detail[$order_arr["order_id"]]['payment']['payment_city'] = $order_arr["payment_city"];
                            $orders_detail[$order_arr["order_id"]]['payment']['payment_postcode'] = $order_arr["payment_postcode"];
                            $orders_detail[$order_arr["order_id"]]['payment']['payment_country'] = $order_arr["payment_country"];


                            $payment_zone = $this->model_shipworks_shipworks->getStateCode($order_arr["payment_zone_id"]);
                            if($payment_zone == "") {
                                $payment_zone = $order_arr["payment_zone"];
                            }
                            $orders_detail[$order_arr["order_id"]]['payment']['payment_zone'] = $payment_zone;
                            $orders_detail[$order_arr["order_id"]]['payment']['payment_method'] = $order_arr["payment_method"];

                            //Shipping Details
                            $orders_detail[$order_arr["order_id"]]['shipping']['first_name'] = $order_arr["shipping_firstname"];
                            $orders_detail[$order_arr["order_id"]]['shipping']['last_name'] = $order_arr["shipping_lastname"];
                            $orders_detail[$order_arr["order_id"]]['shipping']['shipping_company'] = $order_arr["shipping_company"];
                            $orders_detail[$order_arr["order_id"]]['shipping']['shipping_address_1'] = $order_arr["shipping_address_1"];
                            $orders_detail[$order_arr["order_id"]]['shipping']['shipping_address_2'] = $order_arr["shipping_address_2"];
                            $orders_detail[$order_arr["order_id"]]['shipping']['shipping_city'] = $order_arr["shipping_city"];
                            $orders_detail[$order_arr["order_id"]]['shipping']['shipping_postcode'] = $order_arr["shipping_postcode"];
                            $orders_detail[$order_arr["order_id"]]['shipping']['shipping_country'] = $order_arr["shipping_country"];

                            $shipping_zone = $this->model_shipworks_shipworks->getStateCode($order_arr["shipping_zone_id"]);
                            if($shipping_zone == "") {
                                $shipping_zone = $order_arr["shipping_zone"];
                            }
                            $orders_detail[$order_arr["order_id"]]['shipping']['shipping_zone'] = $shipping_zone;
                            $orders_detail[$order_arr["order_id"]]['shipping']['shipping_method'] = $order_arr["shipping_method"];

                            //Orders Details
                            $orders_detail[$order_arr["order_id"]]['orders']['status'] = $order_arr["order_status_id"];
                            $orders_detail[$order_arr["order_id"]]['orders']['date_added'] = $this->toGmt($order_arr["date_added"]);
                            $orders_detail[$order_arr["order_id"]]['orders']['date_modified'] = $this->toGmt($order_arr["date_modified"]);


                            //For Date Logic
                            $last_modified = $orders_detail[$order_arr["order_id"]]['orders']['date_modified'];
                            if ($processed_order_id != "") {
                                $processed_order_id .= ", ";
                            }
                            $processed_order_id .= $order_arr["order_id"];

                            //Product Details
                            $orders_detail[$order_arr["order_id"]]['Product'][$order_arr["order_product_id"]]['id'] = $order_arr["product_id"];
                            $orders_detail[$order_arr["order_id"]]['Product'][$order_arr["order_product_id"]]['name'] = $order_arr["product_name"];
                            $orders_detail[$order_arr["order_id"]]['Product'][$order_arr["order_product_id"]]['quantity'] = $order_arr["quantity"];
                            $orders_detail[$order_arr["order_id"]]['Product'][$order_arr["order_product_id"]]['price'] = $order_arr["price"];
                            $orders_detail[$order_arr["order_id"]]['Product'][$order_arr["order_product_id"]]['sku'] = $order_arr["sku"];
                            $orders_detail[$order_arr["order_id"]]['Product'][$order_arr["order_product_id"]]['location'] = $order_arr["location"];
                            $orders_detail[$order_arr["order_id"]]['Product'][$order_arr["order_product_id"]]['weight'] = $order_arr["weight"];
                            $orders_detail[$order_arr["order_id"]]['Product'][$order_arr["order_product_id"]]['image'] = $order_arr["store_url"]."image/".$order_arr["image"];

                            //Product Attributes
                            $orders_detail[$order_arr["order_id"]]['Product'][$order_arr["order_product_id"]]['Attributes'][$order_arr["order_option_id"]]['option_name'] = $order_arr["option_name"];
                            $orders_detail[$order_arr["order_id"]]['Product'][$order_arr["order_product_id"]]['Attributes'][$order_arr["order_option_id"]]['option_value'] = $order_arr["option_value"];

                            //Order Total
                            $orders_detail[$order_arr["order_id"]]['Order_Total'][$order_arr["order_total_id"]]['id'] = $order_arr["order_total_id"];
                            $orders_detail[$order_arr["order_id"]]['Order_Total'][$order_arr["order_total_id"]]['code'] = $order_arr["total_code"];
                            $orders_detail[$order_arr["order_id"]]['Order_Total'][$order_arr["order_total_id"]]['title'] = $order_arr["total_title"];
                            $orders_detail[$order_arr["order_id"]]['Order_Total'][$order_arr["order_total_id"]]['value'] = $order_arr["total_value"];

                            //Order History
                            if($order_arr["usercomment"] != "") {
                                $orders_detail[$order_arr["order_id"]]['History'][0]['id'] = 0;
                                $orders_detail[$order_arr["order_id"]]['History'][0]['notify'] = "1";
                                $orders_detail[$order_arr["order_id"]]['History'][0]['comment'] = $order_arr["usercomment"];
                                $orders_detail[$order_arr["order_id"]]['History'][0]['history_date'] = $this->toGmt($order_arr["date_added"]);
                            }
                            $orders_detail[$order_arr["order_id"]]['History'][$order_arr["order_history_id"]]['id'] = $order_arr["order_history_id"];
                            $orders_detail[$order_arr["order_id"]]['History'][$order_arr["order_history_id"]]['notify'] = $order_arr["notify"];
                            $orders_detail[$order_arr["order_id"]]['History'][$order_arr["order_history_id"]]['comment'] = $order_arr["comment"];
                            $orders_detail[$order_arr["order_id"]]['History'][$order_arr["order_history_id"]]['history_date'] = $this->toGmt($order_arr["history_date"]);
                        }
                    }

                    if($processed_order_id != "") {
                        $orders = $this->model_shipworks_shipworks->getOtherOrders($processed_order_id, $last_modified, $_GET['store_id']);
                        if(count($orders->rows) > 0) {
                            foreach($orders->rows as $order_arr1) {

                                //Store Details
                                $orders_detail[$order_arr1["order_id"]]['Store']['id'] = $order_arr1["store_id"];
                                $orders_detail[$order_arr1["order_id"]]['Store']['name'] = $order_arr1["store_name"];
                                $orders_detail[$order_arr1["order_id"]]['Store']['url'] = $order_arr1["store_url"];

                                //Customer Details
                                $orders_detail[$order_arr1["order_id"]]['Customer']['id'] = $order_arr1["customer_id"];
                                $orders_detail[$order_arr1["order_id"]]['Customer']['email'] = $order_arr1["email"];
                                $orders_detail[$order_arr1["order_id"]]['Customer']['telephone'] = $order_arr1["telephone"];
                                $orders_detail[$order_arr1["order_id"]]['Customer']['fax'] = $order_arr1["fax"];

                                //Payment Details
                                $orders_detail[$order_arr1["order_id"]]['payment']['first_name'] = $order_arr1["payment_firstname"];
                                $orders_detail[$order_arr1["order_id"]]['payment']['last_name'] = $order_arr1["payment_lastname"];
                                $orders_detail[$order_arr1["order_id"]]['payment']['payment_company'] = $order_arr1["payment_company"];
                                $orders_detail[$order_arr1["order_id"]]['payment']['payment_address_1'] = $order_arr1["payment_address_1"];
                                $orders_detail[$order_arr1["order_id"]]['payment']['payment_address_2'] = $order_arr1["payment_address_2"];
                                $orders_detail[$order_arr1["order_id"]]['payment']['payment_city'] = $order_arr1["payment_city"];
                                $orders_detail[$order_arr1["order_id"]]['payment']['payment_postcode'] = $order_arr1["payment_postcode"];
                                $orders_detail[$order_arr1["order_id"]]['payment']['payment_country'] = $order_arr1["payment_country"];

                                $payment_zone = $this->model_shipworks_shipworks->getStateCode($order_arr1["payment_zone_id"]);
                                if($payment_zone == "") {
                                    $payment_zone = $order_arr1["payment_zone"];
                                }
                                $orders_detail[$order_arr1["order_id"]]['payment']['payment_zone'] = $payment_zone;
                                $orders_detail[$order_arr1["order_id"]]['payment']['payment_method'] = $order_arr1["payment_method"];

                                //Shipping Details
                                $orders_detail[$order_arr1["order_id"]]['shipping']['first_name'] = $order_arr1["shipping_firstname"];
                                $orders_detail[$order_arr1["order_id"]]['shipping']['last_name'] = $order_arr1["shipping_lastname"];
                                $orders_detail[$order_arr1["order_id"]]['shipping']['shipping_company'] = $order_arr1["shipping_company"];
                                $orders_detail[$order_arr1["order_id"]]['shipping']['shipping_address_1'] = $order_arr1["shipping_address_1"];
                                $orders_detail[$order_arr1["order_id"]]['shipping']['shipping_address_2'] = $order_arr1["shipping_address_2"];
                                $orders_detail[$order_arr1["order_id"]]['shipping']['shipping_city'] = $order_arr1["shipping_city"];
                                $orders_detail[$order_arr1["order_id"]]['shipping']['shipping_postcode'] = $order_arr1["shipping_postcode"];
                                $orders_detail[$order_arr1["order_id"]]['shipping']['shipping_country'] = $order_arr1["shipping_country"];

                                $shipping_zone = $this->model_shipworks_shipworks->getStateCode($order_arr1["shipping_zone_id"]);
                                if($shipping_zone == "") {
                                    $shipping_zone = $order_arr1["shipping_zone"];
                                }
                                $orders_detail[$order_arr1["order_id"]]['shipping']['shipping_zone'] = $shipping_zone;
                                $orders_detail[$order_arr1["order_id"]]['shipping']['shipping_method'] = $order_arr1["shipping_method"];

                                //Orders Details
                                $orders_detail[$order_arr1["order_id"]]['orders']['status'] = $order_arr1["order_status_id"];
                                $orders_detail[$order_arr1["order_id"]]['orders']['date_added'] = $this->toGmt($order_arr1["date_added"]);
                                $orders_detail[$order_arr1["order_id"]]['orders']['date_modified'] = $this->toGmt($order_arr1["date_modified"]);


                                //Product Details
                                $orders_detail[$order_arr1["order_id"]]['Product'][$order_arr1["order_product_id"]]['id'] = $order_arr1["product_id"];
                                $orders_detail[$order_arr1["order_id"]]['Product'][$order_arr1["order_product_id"]]['name'] = $order_arr1["product_name"];
                                $orders_detail[$order_arr1["order_id"]]['Product'][$order_arr1["order_product_id"]]['quantity'] = $order_arr1["quantity"];
                                $orders_detail[$order_arr1["order_id"]]['Product'][$order_arr1["order_product_id"]]['price'] = $order_arr1["price"];
                                $orders_detail[$order_arr1["order_id"]]['Product'][$order_arr1["order_product_id"]]['sku'] = $order_arr1["sku"];
                                $orders_detail[$order_arr1["order_id"]]['Product'][$order_arr1["order_product_id"]]['location'] = $order_arr1["location"];
                                $orders_detail[$order_arr1["order_id"]]['Product'][$order_arr1["order_product_id"]]['weight'] = $order_arr1["weight"];
                                $orders_detail[$order_arr1["order_id"]]['Product'][$order_arr1["order_product_id"]]['image'] = $order_arr1["store_url"]."image/".$order_arr["image"];

                                //Product Attributes
                                $orders_detail[$order_arr1["order_id"]]['Product'][$order_arr1["order_product_id"]]['Attributes'][$order_arr1["order_option_id"]]['option_name'] = $order_arr1["option_name"];
                                $orders_detail[$order_arr1["order_id"]]['Product'][$order_arr1["order_product_id"]]['Attributes'][$order_arr1["order_option_id"]]['option_value'] = $order_arr1["option_value"];

                                //Order Total
                                $orders_detail[$order_arr1["order_id"]]['Order_Total'][$order_arr1["order_total_id"]]['id'] = $order_arr1["order_total_id"];
                                $orders_detail[$order_arr1["order_id"]]['Order_Total'][$order_arr1["order_total_id"]]['code'] = $order_arr1["total_code"];
                                $orders_detail[$order_arr1["order_id"]]['Order_Total'][$order_arr1["order_total_id"]]['title'] = $order_arr1["total_title"];
                                $orders_detail[$order_arr1["order_id"]]['Order_Total'][$order_arr1["order_total_id"]]['value'] = $order_arr1["total_value"];

                                //Order History
                                if($order_arr1["usercomment"] != "") {
                                    $orders_detail[$order_arr1["order_id"]]['History'][0]['id'] = 0;
                                    $orders_detail[$order_arr1["order_id"]]['History'][0]['notify'] = "1";
                                    $orders_detail[$order_arr1["order_id"]]['History'][0]['comment'] = $order_arr1["usercomment"];
                                    $orders_detail[$order_arr1["order_id"]]['History'][0]['history_date'] = $this->toGmt($order_arr1["date_added"]);
                                }

                                $orders_detail[$order_arr1["order_id"]]['History'][$order_arr1["order_history_id"]]['id'] = $order_arr1["order_history_id"];
                                $orders_detail[$order_arr1["order_id"]]['History'][$order_arr1["order_history_id"]]['notify'] = $order_arr1["notify"];
                                $orders_detail[$order_arr1["order_id"]]['History'][$order_arr1["order_history_id"]]['comment'] = $order_arr1["comment"];
                                $orders_detail[$order_arr1["order_id"]]['History'][$order_arr1["order_history_id"]]['history_date'] = $this->toGmt($order_arr1["history_date"]);
                            }
                        }
                    }
                    $this->data["orders"] = $orders_detail;
                    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/shipworks/getorders.tpl')) {
                        $this->template = $this->config->get('config_template') . '/template/shipworks/getorders.tpl';
                    } else {
                        $this->template = 'default/template/shipworks/getorders.tpl';
                    }
                    $this->response->setOutput($this->render());
                } else if ($_POST['action'] == 'updatestatus') {
                    if(isset($_POST['order']) && $_POST['order'] != "" && isset($_POST['status']) && $_POST['status'] != "") {
                        $count_order_query = $this->model_shipworks_shipworks->statusUpdate($_POST['order'], $_POST['status'], $_POST['comments']);
                        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/shipworks/updatestatus.tpl')) {
                            $this->template = $this->config->get('config_template') . '/template/shipworks/updatestatus.tpl';
                        } else {
                            $this->template = 'default/template/shipworks/updatestatus.tpl';
                        }
                        $this->response->setOutput($this->render());
                    } else {
                        $this->data["msg"] = "err_status_update";
                        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/shipworks/errorstatus.tpl')) {
                            $this->template = $this->config->get('config_template') . '/template/shipworks/errorstatus.tpl';
                        } else {
                            $this->template = 'default/template/shipworks/errorstatus.tpl';
                        }
                        $this->response->setOutput($this->render());
                    }
                }
            } else {
            $this->data["msg"] = "err_store_id";
            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/shipworks/errorstatus.tpl')) {
                $this->template = $this->config->get('config_template') . '/template/shipworks/errorstatus.tpl';
            } else {
                $this->template = 'default/template/shipworks/errorstatus.tpl';
            }
            $this->response->setOutput($this->render());
        }
        } else {
            if(!$this->model_shipworks_shipworks->authenticate($user, $pass)) {
                $this->data["msg"] = "err_login";
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/shipworks/errorstatus.tpl')) {
                    $this->template = $this->config->get('config_template') . '/template/shipworks/errorstatus.tpl';
                } else {
                    $this->template = 'default/template/shipworks/errorstatus.tpl';
                }
                $this->response->setOutput($this->render());
            } elseif(!isset($_GET['store_id'])) {
                $this->data["msg"] = "err_store_id";
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/shipworks/errorstatus.tpl')) {
                    $this->template = $this->config->get('config_template') . '/template/shipworks/errorstatus.tpl';
                } else {
                    $this->template = 'default/template/shipworks/errorstatus.tpl';
                }
                $this->response->setOutput($this->render());
            } else {
                $this->data["msg"] = "err_common";
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/shipworks/errorstatus.tpl')) {
                    $this->template = $this->config->get('config_template') . '/template/shipworks/errorstatus.tpl';
                } else {
                    $this->template = 'default/template/shipworks/errorstatus.tpl';
                }
                $this->response->setOutput($this->render());
            }
        }
    }

    function toGmt($dateSql) {
        $pattern = "/^(\d{4})-(\d{2})-(\d{2})\s+(\d{2}):(\d{2}):(\d{2})$/i";
        if (preg_match($pattern, $dateSql, $dt)) {
            $dateUnix = mktime($dt[4], $dt[5], $dt[6], $dt[2], $dt[3], $dt[1]);
            return gmdate("Y-m-d\TH:i:s", $dateUnix);
        }
        return $dateSql;
    }

    function toLocalSqlDate($sqlUtc) {
        $pattern = "/^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})$/i";
        if (preg_match($pattern, $sqlUtc, $dt)) {
            $unixUtc = gmmktime($dt[4], $dt[5], $dt[6], $dt[2], $dt[3], $dt[1]);
            return date("Y-m-d H:i:s", $unixUtc);
        }
        return $sqlUtc;
    }
}
?>