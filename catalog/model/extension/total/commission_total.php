<?php
class ModelExtensionTotalCommissionTotal extends Model {
	public function getTotal($total) {
		$this->load->language('extension/total/sub_total');

		$commission_total = $this->cart->getCommission(); 
		 
		if (!empty($this->session->data['vouchers'])) {
			foreach ($this->session->data['vouchers'] as $voucher) {
				$sub_total += $voucher['amount'];
			}
		}

		// $total['totals'][] = array(
		// 	'code'       => 'sub_total',
		// 	'title'      => $this->language->get('text_sub_total'),
		// 	'value'      => $sub_total,
		// 	'sort_order' => $this->config->get('sub_total_sort_order')
		// );

		$total['commission_total'] += $commission_total;
	}
}
