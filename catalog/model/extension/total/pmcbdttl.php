<?php
class ModelExtensionTotalpmcbdttl extends Model {
 	private $modpath = '';
 	private $modname = 'pmcbdttl';
	private $modssl = '';
	private $modstatus = false;
	private $modlangid = '';
	private $modstoreid = '';
	private $modcustgrpid = '';
	
	public function __construct($registry) {
		parent::__construct($registry);
 		$this->modpath = (substr(VERSION,0,3)=='2.3') ? 'extension/total/pmcbdttl' : 'total/pmcbdttl';
  		$this->modssl = (substr(VERSION,0,3)=='2.3' || substr(VERSION,0,3)=='2.2') ? true : 'SSL';
		$this->modstatus = $this->config->get($this->modname.'_status');
		$this->modlangid = (int)$this->config->get('config_language_id');
		$this->modstoreid = (int)$this->config->get('config_store_id');
		$this->modcustgrpid = (int)$this->config->get('config_customer_group_id');
 	}
	
 	public function getTotal($total) {
 		$this->load->language($this->modpath);
		//print_r($this->session->data['pmcbd_id']);exit;
		if(isset($this->session->data['pmcbd']) && $this->session->data['pmcbd']) { 
 			$this->session->data['pmcbd'] = array_map("unserialize", array_unique(array_map("serialize", $this->session->data['pmcbd']))); 
			foreach ($this->session->data['pmcbd'] as $session_pmcbd) { 
				if( ! isset($session_pmcbd['pmcbd_id_key'])) { 
					continue;
				}
				$pmcbddata = $this->getcombodisc($session_pmcbd['pmcbd_id'], $session_pmcbd['target_product_id'], $session_pmcbd['pmcbd_id_key'],$total);
				//print_r($pmcbddata);exit;
				if ($pmcbddata) {
  					if(substr(VERSION,0,3)=='2.3' || substr(VERSION,0,3)=='2.2') { 	
						$discsign = $this->currency->format($pmcbddata['discount_value'], $this->session->data['currency']); 
					} else {
						$discsign = $this->currency->format($pmcbddata['discount_value']); 
					}
					
					$discount_total = $pmcbddata['discount_value'];
					if ($discount_total > 0) {
						if(substr(VERSION,0,3)=='2.3' || substr(VERSION,0,3)=='2.2') { 							
							$total['totals'][] = array(
								'code'       => 'pmcbdttl',
								'title'      => sprintf(($pmcbddata['inc_tx_flag']) ? $this->language->get('text_pmcbdttl_tx') : $this->language->get('text_pmcbdttl'), $discsign ,implode(" + ",$pmcbddata['pmcbd_prod']) ),
								'value'      => -$discount_total,
								'sort_order' => $this->config->get('pmcbdttl_sort_order')
							);
			
							$total['total'] -= $discount_total;
						} else {
							$total_data[] = array(
								'code'       => 'pmcbdttl',
								'title'      => sprintf(($pmcbddata['inc_tx_flag']) ? $this->language->get('text_pmcbdttl_tx') : $this->language->get('text_pmcbdttl'), $discsign ,implode(" + ",$pmcbddata['pmcbd_prod']) ),
								'value'      => -$discount_total,
								'sort_order' => $this->config->get('pmcbdttl_sort_order')
							);
							$total -= $discount_total;
						}
					}  
 				}
			} 
		}
	}
	
	public function getpmcbd($pmcbd_id, $target_product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "pmcbd WHERE target_product_id = '" . (int)$target_product_id . "' and pmcbd_id = '" . (int)$pmcbd_id . "' limit 1");
		return $query->row;
	}
	
	public function getcombodisc($pmcbd_id, $target_product_id, $pmcbd_id_key,&$total) {
		$setting = $this->getpmcbd($pmcbd_id, $target_product_id);
		
		if($setting && $setting['status']) {
  			$pmcbddata = array();
 			$combo_setting = json_decode($setting['combo_setting'], true);
 			if($combo_setting) {			
				// cart product array
				$allcpid = array();
				foreach ($this->cart->getProducts() as $cp) {	
					$allcpid['product_id'][$cp['product_id']] = $cp['product_id'];
					$allcpid['product_name'][$cp['product_id']] = $cp['name'];
					$allcpid['total'][$cp['product_id']] = $cp['total'];
					$allcpid['price'][$cp['product_id']] = $cp['price'];
					$allcpid['tax_class_id'][$cp['product_id']] = $cp['tax_class_id']; //
   				}
				if($allcpid) {
					$pmcbdstng = isset($combo_setting[$pmcbd_id_key]) ? $combo_setting[$pmcbd_id_key] : array();
 
 					if(! isset($pmcbdstng['store']) && in_array($this->modstoreid, $pmcbdstng['store'])) {
						return $pmcbddata ;
					}
					if(! isset($pmcbdstng['customer_group']) &&  in_array($this->modcustgrpid, $pmcbdstng['customer_group'])) {
						return $pmcbddata ;
					}
					
					//echo "<pre>";print_r($allcpid);exit;
					if(isset($pmcbdstng['product']) && $pmcbdstng['product']) {
 						$pmcbd_product_ttl = array();
						$pmcbd_product_disc = array();
						$pmcbd_product_name = array();
						$pmcbdflag = false;
						$sub_total = 0;//$this->cart->getSubTotal();
						
						foreach($pmcbdstng['product'] as $product_id) { 							
							if(in_array( $product_id , $allcpid['product_id'])) {
								$pmcbdflag = true; 
								$pmcbd_product_ttl[] = $allcpid['total'][$product_id];
								$pmcbd_product_name[] = $allcpid['product_name'][$product_id];
								if ($pmcbdstng['disctype'] == 'F') {
									$sub_total += $allcpid['total'][$product_id];
								}
							} else {
								$pmcbdflag = false;
								$pmcbd_product_ttl = array();
								$pmcbd_product_disc = array();
								$pmcbd_product_name = array();
								break;
							}
						}
						if($pmcbdflag && $pmcbd_product_ttl) {
							$globalflag = true;
							$discount_total = 0;
							$inc_tx_flag = 1;
  							
							foreach($pmcbdstng['product'] as $product_id) {
								$discount = 0;
								
								if ($pmcbdstng['disctype'] == 'F') {
									$discount = $pmcbdstng['discount'] * ($allcpid['total'][$product_id] / $sub_total);
								} else {
									$discount = $this->tax->calculate($allcpid['price'][$product_id], $allcpid['tax_class_id'][$product_id], $this->config->get('config_tax')) / 100 * $pmcbdstng['discount'];
								}
								
								if (!$this->config->get('config_tax')) { $inc_tx_flag = 0;
									$tax_rates = $this->tax->getRates($allcpid['total'][$product_id] - ($allcpid['total'][$product_id] - $discount), $allcpid['tax_class_id'][$product_id]);
									
									foreach ($tax_rates as $tax_rate) {
										if ($tax_rate['type'] == 'P') {
											$total['taxes'][$tax_rate['tax_rate_id']] -= $tax_rate['amount'];
										}
									}
								}
								$discount_total += $discount;
							}
							$pmcbd_product_disc = $discount_total;	
							return array( "disctype" => $pmcbdstng['disctype'], "discount" => $pmcbdstng['discount'], "discount_value" => $pmcbd_product_disc,  "pmcbd_prod" => $pmcbd_product_name, "inc_tx_flag" => $inc_tx_flag); 							
 						}  
					}
					return $pmcbddata;
				} 
			}
			return false;
		}
	}
}