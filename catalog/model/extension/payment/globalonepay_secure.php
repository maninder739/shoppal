<?php
class ModelExtensionPaymentGlobalonepaySecure extends Model {
	public function getMethod($address, $total) {
		
		$this->load->language('extension/payment/globalonepay_secure');
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('cod_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

		if ($this->config->get('cod_total') > 0 && $this->config->get('cod_total') > $total) {
			$status = false;
		} elseif (!$this->cart->hasShipping()) {
			$status = false;
		} elseif (!$this->config->get('cod_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}

		$method_data = array();

		if ($status) {
			$method_data = array(
				'code'       => 'globalonepay_secure',
				'title'      => $this->language->get('text_title'),
				'terms'      => '',
				'sort_order' => $this->config->get('globalonepay_secure_sort_order')
			);
		}

		return $method_data;
	}

	public function saveCustomerCardRef( $customer_id, $order_id, $merchantRef, $cardReference, $cardType, $cardExpiry){

		$this->db->query("INSERT into " . DB_PREFIX . "customer_sec SET order_id = '" . $order_id. "',customer_id = '" . $customer_id. "',  merchantref = '" . $merchantRef. "', responseref = '" . $cardReference . "', responsecardtype = '" . $cardType . "', cardexpiry = '" . $cardExpiry . "', date_added = now()");
	}
}