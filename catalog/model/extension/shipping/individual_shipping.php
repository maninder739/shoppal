<?php
//==============================================================================
// Individual Shipping v302.1
// 
// Author: Clear Thinking, LLC
// E-mail: johnathan@getclearthinking.com
// Website: http://www.getclearthinking.com
// 
// All code within this file is copyright Clear Thinking, LLC.
// You may not copy or reuse code within this file without written permission.
//==============================================================================

class ModelExtensionShippingIndividualShipping extends Model {
	private $type = 'shipping';
	private $name = 'individual_shipping';
	
	public function getQuote($address) {
		$settings = $this->getSettings();
		
		if (empty($settings['status'])) return array();

		// Check if OpenCart 2 order editor is being used
		if (isset($this->session->data['api_id'])) {
			$order_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_total ot ON (o.order_id = ot.order_id) WHERE o.customer_id = " . (int)$this->session->data['customer_id'] . " AND o.shipping_code = '" . $this->name . "." . $this->name . "' AND ot.code = 'shipping'");
			if (!$order_query->num_rows) return;
			
			foreach ($order_query->rows as $row) {
				$quote_data[$this->name . '_' . $row['order_id']] = array(
					'code'			=> $this->name . '.' . $this->name . '_' . $row['order_id'],
					'title'			=> '(Order #' . $row['order_id'] . ') ' . strip_tags($row['title']),
					'cost'			=> $row['value'],
					'tax_class_id'	=> 0,
					'text'			=> $this->currency->format($row['value'], $this->session->data['currency']),
				);
			}
			
			return array(
				'code'			=> $this->name,
				'title'			=> $settings['heading_' . $this->session->data['language']],
				'quote'			=> $quote_data,
				'sort_order'	=> 'ZZZ',
				'error'			=> '',
			);
		}
		
		// Enable all installed shipping methods
		$enabled_methods = array();
		$disabled_methods = array();
		
		$shipping_methods = $this->db->query("SELECT * FROM " . DB_PREFIX . "extension WHERE `type` = 'shipping' ORDER BY `code` ASC")->rows;
		
		foreach ($shipping_methods as $shipping_method) {
			if (version_compare(VERSION, '3.0', '<')) {
				$shipping_method['code'] = 'shipping_' . $shipping_method['code'];
			}
			if ($this->config->get($shipping_method['code'] . '_status')) {
				$enabled_methods[] = $shipping_method['code'];
			} else {
				$disabled_methods[] = $shipping_method['code'];
			}
			$this->config->set($shipping_method['code'] . '_status', 1);
			$this->db->query("UPDATE `" . DB_PREFIX . "setting` SET `value` = 1 WHERE `key` = '" . $this->db->escape($shipping_method['code']) . "_status'");
		}
		
		// Group products
		$groups = array();
		
		foreach ($this->cart->getProducts() as $product) {
			if (version_compare(VERSION, '2.1', '>=')) {
				$product['key'] = $product['product_id'] . json_encode($product['option']);
			}
			
			$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product WHERE product_id = " . (int)$product['product_id']);
			if (!$product_query->num_rows) continue;
			
			$product = array_merge($product, $product_query->row);
			$product_grouping = $settings['product_grouping'];
			
			if (isset($product[$product_grouping])) {
				$groups[$product[$product_grouping]][] = $product;
			} else {
				/*
				foreach ($product['option'] as $option) {
					$option_value = $option['value'];
					if ($option['name'] == 'Delivery') {
						$groups[$option['name']][] = $product['key'];
					}
				}
				*/
			}
		}
		
		// Get shipping rates
		$shipping_groups = array();
		$this->load->model('tool/image');
		
		foreach ($groups as $grouping => $products) {
			if ($settings['product_grouping'] == 'product_id') {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = " . (int)$grouping);
				$group_data = ($query->num_rows) ? array('name' => $query->row['name'], 'image' => $query->row['image']) : array('name' => '', 'image' => 'no_image.jpg');
			} elseif ($settings['product_grouping'] == 'manufacturer_id') {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "manufacturer WHERE manufacturer_id = " . (int)$grouping);
				$group_data = ($query->num_rows) ? array('name' => $query->row['name'], 'image' => $query->row['image']) : array('name' => '', 'image' => 'no_image.jpg');
			} else {
				$group_data = array('name'	=> $grouping, 'image' => '');
			}
			
			// Perform settings overrides
			$defaults = array();
			foreach ($settings['setting_override'] as $setting_override) {
				$product_groups = array_map('trim', explode(',', $setting_override['product_groups']));
				if (in_array($grouping, $product_groups)) {
					$defaults[$setting_override['setting']] = $this->config->get($setting_override['setting']);
					$this->config->set($setting_override['setting'], $setting_override['override_value']);
				}
			}
			
			// Create product_keys array and set up product display
			$product_keys = array();
			
			foreach ($products as &$product) {
				$product_keys[] = $product['key'];
				$product['image'] = $this->model_tool_image->resize($product['image'], $settings['image_width'], $settings['image_height']);
			}
			
			// Retrieve rates for current group
			$shipping_groups[] = array(
				'key'				=> $grouping,
				'name'				=> $group_data['name'],
				'title'				=> str_replace('[group]', $group_data['name'], $settings['group_text_' . $this->session->data['language']]),
				'products'			=> ($settings['show_products']) ? $products : array(),
				'shipping_rates'	=> $this->getShippingRates($address, $product_keys),
			);
			
			// Restore setting defaults
			foreach ($defaults as $key => $value) {
				$this->config->set($key, $value);
			}
		}
		
		// Re-disable methods not enabled
		foreach ($disabled_methods as $method) {
			$this->config->set($method . '_status', 0);
			$this->db->query("UPDATE `" . DB_PREFIX . "setting` SET `value` = 0 WHERE `key` = '" . $method . "_status'");
		}
		
		// Set the quote data
		$disable = false;
		$error = '';
		$quote_data = array();
		
		foreach ($shipping_groups as $shipping_group) {
			$shipping_group_error = '';
			foreach ($shipping_group['shipping_rates'] as $shipping_rate) {
				if (empty($shipping_rate['error'])) {
					$disable = false;
					$shipping_group_error = '';
					break;
				} else {
					if ($shipping_rate['error'] != $this->config->get($this->name . '_not_required_text_' . $this->session->data['language'])) {
						$disable = true;
					}
					$shipping_group_error = '<li>' . str_replace('[group]', $shipping_group['name'], $shipping_rate['error']) . '</li><br />';
				}
			}
			$error .= $shipping_group_error;
		}
		
		if (!$disable) {
			$cost = 0;
			$tax_class_id = 0;
			
			ob_start();
			$theme = (version_compare(VERSION, '2.2', '<')) ? $this->config->get('config_template') : str_replace('theme_', '', $this->config->get('config_theme'));
			$template = (file_exists(DIR_TEMPLATE . $theme . '/template/extension/' . $this->type . '/' . $this->name . '.twig')) ? $theme : 'default';
			$template_file = DIR_TEMPLATE . $template . '/template/extension/' . $this->type . '/' . $this->name . '.twig';
			require_once(class_exists('VQMod') ? VQMod::modCheck(modification($template_file)) : modification($template_file));
			$html = ob_get_contents();
			ob_end_clean();
			
			$quote_data[$this->name] = array(
				'code'			=> $this->name . '.' . $this->name,
				'title'			=> $html,
				'cost'			=> $cost,
				'tax_class_id'	=> $tax_class_id,
				'text'			=> '<strong>' . $settings['total_shipping_text_' . $this->session->data['language']] . '</strong><br /><span>' . $this->currency->format($this->tax->calculate($cost, $tax_class_id, $this->config->get('config_tax')), $this->session->data['currency']) . '</span>',
			);
		}
		
		return array(
			'code'			=> $this->name,
			'title'			=> $settings['heading_' . $this->session->data['language']],
			'quote'			=> $quote_data,
			'sort_order'	=> 'ZZZ',
			'error'			=> ($disable) ? '<ul>' . $error . '</ul>' : '',
		);
	}
	
	//==============================================================================
	// getSettings()
	//==============================================================================
	private function getSettings() {
		$code = (version_compare(VERSION, '3.0', '<') ? '' : $this->type . '_') . $this->name;
		
		$settings = array();
		$settings_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE `code` = '" . $this->db->escape($code) . "' ORDER BY `key` ASC");
		
		foreach ($settings_query->rows as $setting) {
			$value = $setting['value'];
			if ($setting['serialized']) {
				$value = (version_compare(VERSION, '2.1', '<')) ? unserialize($setting['value']) : json_decode($setting['value'], true);
			}
			$split_key = preg_split('/_(\d+)_?/', str_replace($code . '_', '', $setting['key']), -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
			
				if (count($split_key) == 1)	$settings[$split_key[0]] = $value;
			elseif (count($split_key) == 2)	$settings[$split_key[0]][$split_key[1]] = $value;
			elseif (count($split_key) == 3)	$settings[$split_key[0]][$split_key[1]][$split_key[2]] = $value;
			elseif (count($split_key) == 4)	$settings[$split_key[0]][$split_key[1]][$split_key[2]][$split_key[3]] = $value;
			else 							$settings[$split_key[0]][$split_key[1]][$split_key[2]][$split_key[3]][$split_key[4]] = $value;
		}
		
		return $settings;
	}
	
	//==============================================================================
	// getShippingRates()
	//==============================================================================
	private function getShippingRates($address, $product_keys) {
		$settings = $this->getSettings();
		
		// Save cart and remove ineligible products
		$cart_products = $this->cart->getProducts();
		
		foreach ($cart_products as $product) {
			if (version_compare(VERSION, '2.1', '<')) {
				if (!in_array($product['key'], $product_keys)) {
					$this->cart->remove($product['key']);
				}
			} else {
				$key = $product['product_id'] . json_encode($product['option']);
				if (!in_array($key, $product_keys)) {
					$this->cart->remove($product['cart_id']);
				}
			}
		}
		
		// Get shipping rates
		if ($this->cart->hasShipping()) {
			$eligible_methods = array_map('trim', explode(';', $settings['eligible_methods']));
			
			$shipping_rates = array();
			$shipping_methods = $this->db->query("SELECT * FROM " . DB_PREFIX . "extension WHERE `type` = 'shipping' ORDER BY `code` ASC")->rows;
			
			foreach ($shipping_methods as $shipping_method) {
				if (!in_array($shipping_method['code'], $eligible_methods)) continue;
				
				$status = $this->config->get((version_compare(VERSION, '3.0', '<') ? '' : 'shipping_') . $shipping_method['code'] . '_status');
				if ($status) {
					if (version_compare(VERSION, '2.3', '<')) {
						$this->load->model('shipping/' . $shipping_method['code']);
						$quote = $this->{'model_shipping_' . $shipping_method['code']}->getQuote($address);
					} else {
						$this->load->model('extension/shipping/' . $shipping_method['code']);
						$quote = $this->{'model_extension_shipping_' . $shipping_method['code']}->getQuote($address);
					}
					if ($quote) $shipping_rates[$shipping_method['code']] = $quote;
				}
			}
			
			if (!empty($shipping_rates)) {
				$sort_order = array();
				foreach ($shipping_rates as $key => $value) $sort_order[$key] = $value['sort_order'];
				array_multisort($sort_order, SORT_ASC, $shipping_rates);
			} else {
				$shipping_rates = array(
					array('title' => '', 'error' => $settings['no_rates_text_' . $this->session->data['language']])
				);
			}
		} else {
			$shipping_rates = array(
				array('title' => '', 'error' => $settings['not_required_text_' . $this->session->data['language']])
			);
		}
		
		// Restore cart
		$this->cart->clear();
		
		foreach ($cart_products as $product) {
			$options = array();
			foreach ($product['option'] as $option) {
				if (isset($options[$option['product_option_id']])) {
					if (!is_array($options[$option['product_option_id']])) $options[$option['product_option_id']] = array($options[$option['product_option_id']]);
					$options[$option['product_option_id']][] = $option['product_option_value_id'];
				} else {
					$options[$option['product_option_id']] = (!empty($option['product_option_value_id'])) ? $option['product_option_value_id'] : $option['value'];
				}
			}
			$this->cart->add($product['product_id'], $product['quantity'], $options, $product['recurring']['recurring_id']);
		}
		
		// Return shipping rates
		return $shipping_rates;
	}
}
?>