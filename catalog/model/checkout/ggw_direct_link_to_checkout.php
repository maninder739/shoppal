<?php

class ModelCheckoutGGWDirectLinkToCheckout extends Model {

    public function getProductIdByField($field, $value) {
        $query = $this->db->query("SELECT product_id FROM " . DB_PREFIX . "product WHERE " . $this->db->escape($field) . " = '" . $this->db->escape($value) . "'");

        return $query->row['product_id'];
    }

    public function getProductInCart($product_id, $products = array()) {

        if (!$products) {
            $products = $this->cart->getProducts();
        }

        $arr = array();

        foreach ($products as $product) {
            if ($product['product_id'] == $product_id)
                $arr[] = $product;
        }


        return $arr;
    }

    //Cart vs Request product options in previously added to cart
    public function sameProductOptions($product_options, $request_options) {
        $json = array();

        foreach ($product_options as $product_option) {

            if (empty($request_options[$product_option['product_option_id']])) {
                $json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
            } elseif ($request_options[$product_option['product_option_id']] != $product_option['product_option_value_id'] && $request_options[$product_option['product_option_id']] != $product_option['value']) {
                $json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
            }
        }

        // Think about displaying that error
        if (!$json) {

            return true;
        }
        return false;
    }

    public function getLinkToCart() {
        // Get all products in cart for reuse
        $products = $this->cart->getProducts();

        // Start the URL
        $url = HTTPS_SERVER . 'index.php?route=checkout/cart/addFromDirectLink';


        if (isset($this->request->cookie['tracking'])) {
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "affiliate  WHERE code = '" . $this->request->cookie['tracking'] . "'");
            if ($query->num_rows > '0') {
                $url .= '&tracking=' . $query->row['code'];
            }
        }

        if (isset($this->session->data['coupon'])) {
            $url .= '&coupon=' . $this->session->data['coupon'];
        }

        if (isset($this->session->data['voucher'])) {
            $url .= '&voucher=' . $this->session->data['voucher'];
        }

        if (is_array($products) && count($products) > 0) {


            foreach ($products as $key => $product) {
                $url .= '&' . $key . '%5Bk%5D%5Bproduct_id%5D=' . $product['product_id'];

                $url .= '&' . $key . '%5Bq%5D=' . $product['quantity'];

                foreach ($product['option'] as $option) {
                    $opt_value = $option['product_option_value_id'] != '' ? $option['product_option_value_id'] : $option['value'];
                    $url .= '&' . $key . '%5Bo%5D%5B' . $option['product_option_id'] . '%5D=' . $opt_value;
                }
            } // fpreach product
        }

        return $url;
    }

}
