<?php

class ModelCatalogProductSubscribe extends Model {

	public function createRecur($item,$order_id,$description) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "order_recurring` SET `order_id` = '" . (int)$order_id . "', `date_added` = NOW(), `status` = 2, `product_id` = '" . (int)$item['product_id'] . "', `product_name` = '" . $this->db->escape($item['name']) . "', `product_quantity` = '" . $this->db->escape($item['quantity']) . "', `recurring_id` = '" . (int)$item['recurring']['recurring_id'] . "', `recurring_name` = '" . $this->db->escape($item['recurring']['name']) . "', `recurring_description` = '" . $this->db->escape($description) . "', `recurring_frequency` = '" . $this->db->escape($item['recurring']['frequency']) . "', `recurring_cycle` = '" . (int)$item['recurring']['cycle'] . "', `recurring_duration` = '" . (int)$item['recurring']['duration'] . "', `recurring_price` = '" . (float)$item['recurring']['price'] . "', `trial` = '" . (int)$item['recurring']['trial'] . "', `trial_frequency` = '" . $this->db->escape($item['recurring']['trial_frequency']) . "', `trial_cycle` = '" . (int)$item['recurring']['trial_cycle'] . "', `trial_duration` = '" . (int)$item['recurring']['trial_duration'] . "', `trial_price` = '" . (float)$item['recurring']['trial_price'] . "', `reference` = ''");
		return $this->db->getLastId();
	}

	public function addTransaction($order_recurring_id,$amount) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "order_recurring_transaction` SET `order_recurring_id` = '" . (int)$order_recurring_id . "', `date_added` = NOW(), `amount` = '" . (float)$amount . "', `type` = '1'");
		return;
	}

	public function getShippingAddressId($order_info) {
		$query = $this->db->query("SELECT address_id FROM `" . DB_PREFIX . "address` WHERE customer_id = '" . (int)$order_info['customer_id'] . "' AND firstname = '" . $this->db->escape($order_info['shipping_firstname']) . "' AND lastname = '" . $this->db->escape($order_info['shipping_lastname']) . "' AND address_1 = '" . $this->db->escape($order_info['shipping_address_1']) . "' AND address_2 = '" . $this->db->escape($order_info['shipping_address_2']) . "' AND city = '" . $this->db->escape($order_info['shipping_city']) . "' AND postcode = '" . $this->db->escape($order_info['shipping_postcode']) . "' AND country_id = '" . (int)$order_info['shipping_country_id'] . "' AND zone_id = '" . (int)$order_info['shipping_zone_id'] . "'");
		if ($query->num_rows) {
			return $query->row['address_id'];
		} else {
			return 0;
		}
	}

	public function getOrderSubscriptionInfo($order_subscription_id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_subscriptions` WHERE order_subscription_id = '" . (int)$order_subscription_id . "'");
		return $query->row;
	}

	public function sendNotifications() {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_subscriptions` WHERE active = '1'");
		if ($query->num_rows) {
			$this->load->language('catalog/product_subscribe');
			foreach ($query->rows as $row) {
				if ($row['subscription_id'] && $row['end_date'] > 0) {
					$subscription_info = $this->db->query("SELECT * FROM `" . DB_PREFIX . "subscriptions` WHERE subscription_id = '" . (int)$row['subscription_id'] . "'");
					if ($subscription_info) {
						$product_info = $this->db->query("SELECT name FROM `" . DB_PREFIX . "product_description` WHERE product_id = '" . (int)$row['product_id'] . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");
						$count = $subscription_info->row['reminder_count'] - $row['reminders_sent'];
						if ($count > 0) {
							$timeframe = 86400 * ($count * $subscription_info->row['reminder_period']);
							if ($subscription_info->row['grace_period'] > 0) {
								$grace_period = 86400 * $subscription_info->row['grace_period'];
							} else {
								$grace_period = 0;
							}
							if (time() > ($row['end_date'] + $grace_period) - $timeframe) {
								$email = $this->db->query("SELECT email FROM `" . DB_PREFIX . "order` WHERE order_id = '" . (int)$row['order_id'] . "'");
								if ($count > 1) {
									$subject = sprintf($this->language->get('text_subject'), $product_info->row['name'], $row['order_id']);
								} else {
									$subject = sprintf($this->language->get('text_final_subject'), $product_info->row['name'], $row['order_id']);
								}
								if ($count == 1) {
									$days_left = round((($row['end_date'] + $grace_period) - time()) / 86400);
									$message = sprintf($this->language->get('text_final_message'), $product_info->row['name'], $days_left, $days_left);
								} else {
									$message = $this->language->get('text_message');
								}
								$message .= "<br /><br />";
								$url = HTTP_SERVER . 'index.php?route=account/order/info&order_id=' . $row['order_id'];
								$message .= sprintf($this->language->get('text_customer_link'), $url);
								
								$mail = new Mail();
								$mail->protocol = $this->config->get('config_mail_protocol');
								$mail->parameter = $this->config->get('config_mail_parameter');
								$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
								$mail->smtp_username = $this->config->get('config_mail_smtp_username');
								$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
								$mail->smtp_port = $this->config->get('config_mail_smtp_port');
								$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
								
								$mail->setTo($this->config->get('config_email'));
								$mail->setFrom($this->config->get('config_email'));
								$mail->setSender($store_name);
								$mail->setSubject(sprintf($this->language->get('text_approve_subject'), $store_name));
								$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
								$mail->send();
								
								$mail = new Mail();
								$mail->protocol = $this->config->get('config_mail_protocol');
								$mail->parameter = $this->config->get('config_mail_parameter');
								$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
								$mail->smtp_username = $this->config->get('config_mail_smtp_username');
								$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
								$mail->smtp_port = $this->config->get('config_mail_smtp_port');
								$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
								
								$mail->setTo($this->config->get('config_email'));
								$mail->setFrom($this->config->get('config_email'));
								$mail->setSender($store_name);
								$mail->setSubject(sprintf($this->language->get('text_approve_subject'), $store_name));
								$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
								$mail->send();
								
								$this->db->query("UPDATE `" . DB_PREFIX . "order_subscriptions` SET reminder_sent = '" . time() . "', reminders_sent = (reminders_sent + 1) WHERE order_subscription_id = '" . (int)$row['order_subscription_id'] . "'");
							}
						} else {
							if ($subscription_info->row['grace_period'] > 0) {
								$grace_period = 86400 * $subscription_info->row['grace_period'];
							} else {
								$grace_period = 0;
							}
							if (time() > $row['end_date'] + $grace_period) {
								$this->db->query("UPDATE `" . DB_PREFIX . "order_subscriptions` SET active = 0 WHERE order_subscription_id = '" . (int)$row['order_subscription_id'] . "'");
							}
						}
					}
				}
			}
		}
		return;
	}

}

?>