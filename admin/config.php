<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/spdev/admin/');
define('HTTP_CATALOG', 'http://localhost/spdev/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/spdev/admin/');
define('HTTPS_CATALOG', 'http://localhost/spdev/');

// DIR
define('DIR_APPLICATION', '/var/www/html/spdev/admin/');
define('DIR_SYSTEM', '/var/www/html/spdev/system/');
define('DIR_IMAGE', '/var/www/html/spdev/image/');
define('DIR_LANGUAGE', '/var/www/html/spdev/admin/language/');
define('DIR_TEMPLATE', '/var/www/html/spdev/admin/view/template/');
define('DIR_CONFIG', '/var/www/html/spdev/system/config/');
define('DIR_CACHE', '/var/www/html/spdev/system/storage/cache/');
define('DIR_DOWNLOAD', '/var/www/html/spdev/system/storage/download/');
define('DIR_LOGS', '/var/www/html/spdev/system/storage/logs/');
define('DIR_MODIFICATION', '/var/www/html/spdev/system/storage/modification/');
define('DIR_UPLOAD', '/var/www/html/spdev/system/storage/upload/');
define('DIR_CATALOG', '/var/www/html/spdev/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
define('DB_DATABASE', 'testings');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
