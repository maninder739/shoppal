<?php

// Heading

$_['heading_title']             = 'Affiliés';


// Text

$_['text_success']              = 'Succès: Vous avez modifié les affiliés!';
$_['text_approved']             = 'Toi avoir approuvé %s comptes!';
$_['text_list']                 = 'Liste d`affiliés';
$_['text_add']                  = 'Ajouter un affilié';
$_['text_edit']                 = 'Modifier l`affiliation';
$_['text_affiliate_detail']     = 'Détails de l`affilié';
$_['text_affiliate_address']    = 'Adresse d`affiliation';
$_['text_balance']              = 'Équilibre';
$_['text_cheque']               = 'Vérifier';
$_['text_paypal']               = 'Pay Pal';
$_['text_bank']                 = 'Virement';


// Column

$_['column_name']               = 'Nom d`affilié';
$_['column_email']              = 'Email';
$_['column_organization']       = 'Organisation';

$_['column_code']               = 'Code de suivi';
$_['column_balance']            = 'Équilibre';
$_['column_status']             = 'Statut';
$_['column_approved']           = 'Approuvé';
$_['column_date_added']         = 'date ajoutée';
$_['column_description']        = 'La description';
$_['column_amount']             = 'Montant';
$_['column_action']             = 'action';


// Entry

$_['entry_firstname']           = 'Prénom';
$_['entry_lastname']            = 'Nom de famille';
$_['entry_email']               = 'Email';
$_['entry_telephone']           = 'Téléphone';
$_['entry_extension']       	= 'Extension';
$_['entry_fax']                 = 'Fax';
$_['entry_status']              = 'Statut';
$_['entry_password']            = 'Mot de passe';
$_['entry_confirm']             = 'Confirmer';
$_['entry_company']             = 'Compagnie';
$_['entry_website']             = 'Site Internet';
$_['entry_address_1']           = 'Adresse 1';
$_['entry_address_2']           = 'Adresse 2';
$_['entry_city']                = 'Ville';
$_['entry_postcode']            = 'Code postal';
$_['entry_country']             = 'Pays';
$_['entry_zone']                = 'Région / État';
$_['entry_code']                = 'Code de suivi';
$_['entry_commission']          = 'Commission (%)';
$_['entry_tax']                 = 'Numéro d`identification fiscale';
$_['entry_payment']             = 'Mode de paiement';
$_['entry_cheque']              = 'Vérifier le nom du bénéficiaire';
$_['entry_paypal']              = 'Compte de messagerie PayPal';
$_['entry_bank_name']           = 'Nom de banque';
$_['entry_bank_branch_number']  = 'ABA / BSB nombre ( Branche Nombre )';
$_['entry_bank_swift_code']     = 'Code rapide';
$_['entry_bank_account_name']   = 'Nom du compte';
$_['entry_bank_account_number'] = 'Numéro de compte';
$_['entry_amount']              = 'Montant';
$_['entry_description']         = 'La description';
$_['entry_name']                = 'Nom d`affilié';
$_['entry_approved']            = 'Approuvé';
$_['entry_date_added']          = 'date ajoutée';
$_['entry_member_type']         = 'Type de membre';

/*  new fields  */

$_['entry_custom_logo']         = 'Logo personnalisé';
$_['entry_mission']             = 'Votre mission';
$_['entry_mission_french']      = 'Votre mission française';
$_['entry_slide_background']    = 'Choisissez votre fond de diapositive';	

/* new fields  */

/* organization fields */
$_['entry_position_organization'] = "Position dans l'organisation";
$_['entry_organization_phone_number'] = "Numéro de téléphone de l'organisation";
$_['entry_branded_website'] = "Préférences de nom de site Web de marque ShopPal (Shoppal.ca/ ???)";
$_['entry_organization_details'] = "Détails de l'organisation";
$_['entry_organization_website'] = "Site Web d'organisation";
$_['entry_organization_hashtags'] = "Organisation Hashtags";
$_['entry_organization_facebook_link'] = "Organisation Facebook Lien";
$_['entry_organization_instagram'] = 'Organisation Instagram';
$_['entry_organization_twitter'] = 'Organisation Twitter';
$_['entry_organization_linkedIn_link'] = "Lien LinkedIn de l'organisation";
$_['entry_agreement_heading'] = "L'organisation autorise Shoppal à utiliser son logo et son nom, sans lien avec les montants de collecte de fonds, de la manière suivante";
$_['entry_agreement_checkbox1'] = "sur la page principale ou d'autres pages du site Web ShopPal qui affichent des informations sur les membres";
$_['entry_agreement_checkbox2'] = "sur les médias sociaux et autres plateformes de marketing Internet, et / ou";
$_['entry_agreement_checkbox3'] = 'sur les médias imprimés et / ou les médias radio / télévision.';

$_['entry_organization_detailed'] = 'Organisation "Ce que nous faisons" / "Détaillé - À propos de nous"';
$_['entry_organization_detailed_french'] = 'Organisation "Ce que nous faisons" / "Détaillé - À propos de nous Français"';
$_['entry_hear_about_us'] = 'Comment avez-vous entendu parler de nous?';
$_['entry_type_question'] = 'Tapez une question';
$_['entry_shoppal_website'] = 'Site Web de ShopPal';
$_['entry_local_store'] = 'Magasin local';
$_['entry_local_event'] = 'Événement local';
$_['entry_local_flyer'] = 'Prospectus';
$_['entry_other_fundraising_organization'] = 'Autre organisation de collecte de fonds';
$_['entry_other_hear_about_us'] = "Nom de l'acheteur";
$_['entry_other_details_text'] = 'Autres détails';
$_['entry_other_details_input'] = 'autres détails';


/* organization fields */

// Help

$_['help_code']                 = 'Le code de suivi qui sera utilisé pour suivre les renvois.';
$_['help_commission']           = 'Pourcentage que l`affilié reçoit sur chaque commande.';


// Error

$_['error_warning']             = 'Attention: Veuillez vérifier le formulaire attentivement pour les erreurs!';
$_['error_permission']          = 'Attention: Vous n`avez pas la permission de modifier les affiliés!';
$_['error_exists']              = 'Attention: l`adresse e-mail est déjà enregistrée!';
$_['error_firstname']           = 'Le prénom doit avoir entre 1 et 32 ​​caractères!';
$_['error_lastname']            = 'Le nom de famille doit être compris entre 1 et 32 ​​caractères!';
$_['error_email']               = 'L`adresse e-mail ne semble pas être valide!';
$_['error_cheque']              = 'Vérifiez le nom du bénéficiaire requis!';
$_['error_paypal']              = 'L`adresse e-mail PayPal ne semble pas être valide!';
$_['error_bank_account_name']   = 'Nom du compte requis';
$_['error_bank_account_number'] = 'Numéro de compte requis!';
$_['error_telephone']           = 'Le téléphone doit avoir entre 3 et 32 ​​caractères!';
$_['error_password']            = 'Le mot de passe doit comporter entre 4 et 20 caractères!';
$_['error_confirm']             = 'La confirmation du mot de passe et du mot de passe ne correspond pas!';
$_['error_address_1']           = 'L`adresse 1 doit comporter entre 3 et 128 caractères!';
$_['error_city']                = 'La ville doit avoir entre 2 et 128 caractères!';
$_['error_postcode']            = 'Le code postal doit comporter entre 2 et 10 caractères pour ce pays!';
$_['error_country']             = 'S`il vous plaît sélectionner un pays!';
$_['error_zone']                = 'S`il vous plaît sélectionner une région / état!';
$_['error_code']                = 'Code de suivi requis!';
$_['error_code_exists']         = 'Le code de suivi est utilisé par un autre affilié!';

/* organization fields  */

$_['error_position_in_organization']  = 'Doit être entre 1 et 32 ​​caractères!';
$_['error_organization_phone_number']  = "Le numéro de téléphone de l'organisation est invalide";

/* organization fields */

/* new fields */
$_['error_mission']             = "Votre mission doit être inférieure ou égale à 217 caractères!";
$_['error_mission_french']      = "Votre mission française devrait être inférieure ou égale à 217 caractères!";
/* new fields */



					/* 
					  Copyright (C) 2016-2017 Apptiko.
					  Created by Apptiko - http://extension.apptiko.com.
					  Time-stamp-code:Sat1117071111					  
					*/