<?php
/**
 * Module version: 5.4.6
 *
 * For bugs and suggestions please contact: support@drugoe.de
 *
 * CANADAPOST Smart & Flexible Shipping Module - Terms and conditions
 *
 * 1. Preamble: This Agreement governs the relationship between customer, (hereinafter: Licensee) and "Drugoe",
 *    an approved module vendor (hereinafter: Licensor). This Agreement sets the terms, rights,
 *    restrictions and obligations on using CANADAPOST Smart & Flexible Shipping Module
 *    (hereinafter: The Software) created and owned by Licensor, as detailed herein
 *
 * 2. License Grant: Licensor hereby grants Licensee a Personal, Non-assignable & non-transferable, Perpetual,
 *    Commercial, Royalty free, Including the rights to create but not distribute derivative works,
 *    Non-exclusive license, all with accordance with the terms set forth and other legal restrictions
 *    set forth in 3rd party software used while running Software.
 * 2.1. Limited: Licensee may use Software for the purpose of:
 * 2.1.1. Running Software on Licensee’s Website[s] and Server[s];
 * 2.1.2. Allowing 3rd Parties to run Software on Licensee’s Website[s] and Server[s];
 * 2.1.3. Publishing Software’s output to Licensee and 3rd Parties;
 * 2.1.4. Distribute verbatim copies of Software’s output (including compiled binaries);
 * 2.1.5. Modify Software to suit Licensee’s needs and specifications.
 * 2.2. This license is granted perpetually, as long as you do not materially breach it.
 * 2.3. Binary Restricted: Licensee may sublicense Software as a part of a larger work containing more than Software,
 *      distributed solely in Object or Binary form under a personal, non-sublicensable, limited license.
 *      Such redistribution shall be limited to unlimited codebases.
 * 2.4. Non Assignable & Non-Transferable: Licensee may not assign or transfer his rights and duties under this license.
 * 2.5. Commercial, Royalty Free: Licensee may use Software for any purpose,
 *      including paid-services, without any royalties
 * 2.6. Including the Right to Create Derivative Works: Licensee may create derivative works based on Software,
 *      including amending Software’s source code, modifying it, integrating it into a larger work or removing
 *      portions of Software, as long as no distribution of the derivative works is made
 *
 * 3. Term & Termination: The Term of this license shall be until terminated.
 *    Licensor may terminate this Agreement, including Licensee’s license in the case where Licensee :
 * 3.1. became insolvent or otherwise entered into any liquidation process; or
 * 3.2. exported The Software to any jurisdiction where licensor may not enforce his rights under this agreements in; or
 * 3.3. Licensee was in breach of any of this license's terms and conditions and such breach was not cured,
 *      immediately upon notification; or
 * 3.4. Licensee in breach of any of the terms of clause 2 to this license; or
 * 3.5. Licensee otherwise entered into any arrangement which caused Licensor to be unable to enforce his rights
 *      under this License.
 *
 * 4. Payment: In consideration of the License granted under clause 2, Licensee shall pay Licensor a fee,
 *    via Credit-Card, PayPal or any other mean which Licensor may deem adequate. Failure to perform payment
 *    shall construe as material breach of this Agreement.
 *
 * 5. Upgrades, Updates and Fixes: Licensor may provide Licensee, from time to time, with Upgrades, Updates or Fixes,
 *    as detailed herein and according to his sole discretion. Licensee hereby warrants to keep The Softwareup-to-date
 *    and install all relevant updates and fixes, and may, at his sole discretion, purchase upgrades,
 *    according to the rates set by Licensor. Licensor shall provide any update or Fix free of charge;
 *    however, nothing in this Agreement shall require Licensor to provide Updates or Fixes.
 * 5.1. Upgrades: for the purpose of this license, an Upgrade shall be a material amendment in The Software,
 *    which contains new features and or major performance improvements and shall be marked as a new version number.
 *    For example, should Licensee purchase The Software under version 1.X.X,
 *    an upgrade shall commence under number 2.0.0.
 * 5.2. Updates:  for the purpose of this license, an update shall be a minor amendment in The Software,
 *      which may contain new features or minor improvements and shall be marked as a new sub-version number.
 *      For example, should Licensee purchase The Software under version 1.1.X,
 *      an upgrade shall commence under number 1.2.0.
 * 5.3. Fix: for the purpose of this license, a fix shall be a minor amendment in The Software,
 *      intended to remove bugs or alter minor features which impair the The Software's functionality.
 *      A fix shall be marked as a new sub-sub-version number. For example, should Licensee purchase Software
 *      under version 1.1.1, an upgrade shall commence under number 1.1.2.
 *
 * 6. Support: Software is provided under an AS-IS basis and without any support, updates or maintenance.
 *    Nothing in this Agreement shall require Licensor to provide Licensee with support or fixes to any bug, failure,
 *    mis-performance or other defect in The Software.
 * 6.1. Bug Notification:  Licensee may provide Licensor of details regarding any bug, defect or failure in The Software
 *      promptly and with no delay from such event; Licensee shall comply with Licensor's request for information
 *      regarding bugs, defects or failures and furnish him with information, screenshots and try to reproduce
 *      such bugs, defects or failures.
 * 6.2. Feature Request:  Licensee may request additional features in Software, provided, however, that
 *      (i) Licensee shall waive any claim or right in such feature should feature be developed by Licensor;
 *     (ii) Licensee shall be prohibited from developing the feature, or disclose such feature request, or feature,
 *          to any 3rd party directly competing with Licensor or any 3rd party which may be,
 *          following the development of such feature, in direct competition with Licensor;
 *    (iii) Licensee warrants that feature does not infringe any 3rd party patent, trademark,
 *          trade-secret or any other intellectual property right; and
 *     (iv) Licensee developed, envisioned or created the feature solely by himself.
 *
 * 7. Liability:  To the extent permitted under Law, The Software is provided under an AS-IS basis.
 *    Licensor shall never, and without any limit, be liable for any damage, cost, expense or any other payment incurred
 *    by Licensee as a result of Software’s actions, failure, bugs and/or any other interaction between The Software
 *    and Licensee’s end-equipment, computers, other software or any 3rd party, end-equipment, computer or services.
 *    Moreover, Licensor shall never be liable for any defect in source code written by Licensee
 *    when relying on The Software or using The Software’s source code.
 *
 * 8. Warranty:
 * 8.1. Intellectual Property: Licensor hereby warrants that The Software does not violate or infringe
 *      any 3rd party claims in regards to intellectual property, patents and/or trademarks and that
 *      to the best of its knowledge no legal action has been taken against it for any infringement or violation
 *      of any 3rd party intellectual property rights.
 * 8.2. No-Warranty: The Software is provided without any warranty; Licensor hereby disclaims any warranty
 *      that The Software shall be error free, without defects or code which may cause damage to Licensee’s computers
 *      or to Licensee, and that Software shall be functional. Licensee shall be solely liable to any damage,
 *      defect or loss incurred as a result of operating software and undertake the risks contained in running
 *      The Software on License’s Server[s] and Website[s].
 * 8.3. Prior Inspection:  Licensee hereby states that he inspected The Software thoroughly and found it satisfactory
 *      and adequate to his needs, that it does not interfere with his regular operation and that it does meet
 *      the standards and scope of his computer systems and architecture. Licensee found that The Software interacts
 *      with his development, website and server environment and that it does not infringe any of End User License
 *      Agreement of any software Licensee may use in performing his services. Licensee hereby waives any claims
 *      regarding The Software's incompatibility, performance, results and features, and warrants that he inspected
 *      the The Software.
 *
 * 9. No Refunds: Licensee warrants that he inspected The Software according to clause 7(c) and that it is adequate
 *    to his needs. Accordingly, as The Software is intangible goods, Licensee shall not be, ever,
 *    entitled to any refund, rebate, compensation or restitution for any reason whatsoever, even if The Software
 *    contains material flaws.
 *
 * 10. Indemnification: Licensee hereby warrants to hold Licensor harmless and indemnify Licensor for any lawsuit
 *     brought against it in regards to Licensee’s use of The Software in means that violate, breach or otherwise
 *     circumvent this license, Licensor's intellectual property rights or Licensor's title in The Software.
 *     Licensor shall promptly notify Licensee in case of such legal action and request Licensee’s consent
 *     prior to any settlement in relation to such lawsuit or claim.
 *
 * 11. Governing Law, Jurisdiction: Licensee hereby agrees not to initiate class-action lawsuits against Licensor
 *     in relation to this license and to compensate Licensor for any legal fees, cost or attorney fees should
 *     any claim brought by Licensee against Licensor be denied, in part or in full.
 */

?><?php


$_ = array(
    'button_cancel' => 'Annuler',
    'button_save' => 'Sauver',
    'demo_disabled' => 'Ce paramètre est désactivé en mode démo',
    'demo_info' => 'Vous êtes en train de consulter le panneau de cette extension en mode Démo. Ces paramètres sont affichés pour l&rsquo;examen des fonctions d&rsquo;extension. Vous ne pouvez pas mettre à jour leurs valeurs.',
    'demo_title' => 'Mode démo',
    'entry_account_rates' => 'Afficher les taux de compte',
    'entry_address' => 'Adresse d&rsquo;origine',
    'entry_adjustment_rules' => 'Réglages conditionnels des prix',
    'entry_avoid_delivery' => 'Éviter la livraison',
    'entry_avoid_delivery_saturdays' => 'Évitez les livraisons le samedi',
    'entry_avoid_delivery_sundays' => 'Évitez les livraisons le dimanche',
    'entry_box_weight_adj_fix' => 'Réglage du poids de la boîte (fixe)',
    'entry_boxes_checked' => 'Voici les paquets standard et les enveloppes que vous avez sélectionnés',
    'entry_boxes_unchecked' => 'Voici les paquets standard et les enveloppes disponibles',
    'entry_city' => 'Ville d&rsquo;origine',
    'entry_contact' => 'Contacter le développeur',
    'entry_contract_id' => 'N° du contrat',
    'entry_country_id' => 'Pays d&rsquo;origine',
    'entry_custom_envelopes' => 'Enveloppes personnalisées',
    'entry_custom_packages' => 'Paquets personnalisés',
    'entry_customer_groups' => 'Groupes clients',
    'entry_customer_number' => 'Numéro de client',
    'entry_cutoff' => 'Temps de coupure',
    'entry_cutoff_help' => '<a href="#" onclick="document.getElementById(\'how-to-change-timezone\').style.display=\'block\'; return false;">Ce n&rsquo;est pas l&rsquo;heure locale de mon bureau</a><div id="how-to-change-timezone" style="display: none;">Vous pouvez définir le fuseau horaire du serveur dans le fichier <code>php.ini</code> en modifiant la valeur du paramètre <code>date.timezone</code> pour l&rsquo;une des <a href="http://php.net/manual/en/timezones.php">valeurs prises en charge</a>. Si le fuseau horaire n&rsquo;est pas définie dans le fichier<code>php.ini</code>, vous pouvez également le définir dans le fichier System <code>/system/startup.php</code> en changeant l&rsquo;argument de la fonction <code>date_default_timezone_set</code>. Example: <code>date_default_timezone_set("America/Denver");</code></div>.',
    'entry_date_format' => 'Format de la date de livraison estimé',
    'entry_debug' => 'Niveau de débogage',
    'entry_dimension_adj_fix' => 'Réglage des dimensions de l&rsquo;article (fixe)',
    'entry_display_time' => 'Afficher l&rsquo;estimation du délai de livraison',
    'entry_display_weight' => 'Afficher le poids de livraison',
    'entry_dropoff' => 'Type Dropoff',
    'entry_geo_zones' => 'Zones Géo',
    'entry_height' => 'Hauteurt',
    'entry_inches' => 'Unité de longueur en pouces',
    'entry_individual_tare' => 'Inclure la tare',
    'entry_insurance' => 'Inclure l&rsquo;assurance',
    'entry_insurance_from' => 'Commandes ci-dessus *',
    'entry_key' => 'Clé développeur',
    'entry_label' => 'Téléchargez les étiquettes d&rsquo;expédition',
    'entry_label_format' => 'Format d&rsquo;étiquettes',
    'entry_length' => 'Longueur',
    'entry_maintenance' => 'Maintenance',
    'entry_max_height' => 'Epaisseur maximale',
    'entry_max_load' => 'Charge maximale',
    'entry_measurement_system' => 'Système de mesure',
    'entry_meter' => 'Numéro de compteur',
    'entry_min_cost' => 'Coût minimum',
    'entry_minimum_rate' => 'Taux d&rsquo;expédition minimale',
    'entry_mod' => 'État de la modification',
    'entry_packer' => 'Algorithme emballeur',
    'entry_password' => 'Mot de passe API',
    'entry_pdf_converter' => 'Convertisseur PDF',
    'entry_pickup' => 'Type de ramassage',
    'entry_postcode' => 'Code postal d&rsquo;origine',
    'entry_pounds' => 'Unités de poids',
    'entry_prefix' => 'Préfixe du nom d&rsquo;options d&rsquo;expédition',
    'entry_processing_days' => 'Délais de traitement',
    'entry_processing_holidays' => 'Jours fériés',
    'entry_processing_saturdays' => 'Nous expédions le samedi',
    'entry_processing_sundays' => 'Nous expédions le dimanche',
    'entry_production' => 'Mode d&rsquo;extension',
    'entry_promo' => 'Articles promotionnels et cadeaux',
    'entry_promo_code' => 'Code promo',
    'entry_provider_currency' => 'Devise',
    'entry_rate_adj_fix' => 'Réglage des tarifs (fixe)',
    'entry_rate_adj_per' => 'Réglage du taux (pourcentage)',
    'entry_residential' => 'L&rsquo;adresse d&rsquo;origine est résidentiel',
    'entry_sender_company' => 'Entreprise expéditeur',
    'entry_sender_name' => 'Nom de l&rsquo;expéditeur',
    'entry_sender_telephone' => 'Téléphone de l&rsquo;expéditeur',
    'entry_services' => 'Sélectionnez les services que vous souhaitez expédier avec',
    'entry_sort_options' => 'Ordre de tri Options de livraison',
    'entry_sort_order' => 'Classement',
    'entry_standard_packages' => 'Paquets et enveloppes standard',
    'entry_status' => 'État',
    'entry_stores' => 'Magasins',
    'entry_tare' => 'Tare',
    'entry_tax' => 'Type de taxe',
    'entry_tracking' => 'Envoyer des numéros de suivi au client',
    'entry_user_id' => 'Id utilisateur API',
    'entry_version' => 'Version d&rsquo;extension',
    'entry_weight' => 'Poids',
    'entry_weight_adj_fix' => 'Réglage du poids de l&rsquo;article (fixe)',
    'entry_weight_adj_per' => 'Réglage du poids de l&rsquo;article (pourcentage)',
    'entry_weight_based_faked_box' => 'Dimensions du colis',
    'entry_weight_based_limit' => 'Le poids maximum du colis',
    'entry_width' => 'Largeur',
    'entry_zone_id' => 'Zone d&rsquo;origine',
    'error_address' => 'Adresse d&rsquo;origine requis!',
    'error_city' => 'Ville d&rsquo;origine requis!',
    'error_country_id' => 'Pays d&rsquo;origine requis!',
    'error_customer_number' => 'Numéro de client est requis lorsque les étiquettes sont activés',
    'error_demo' => 'Attention, vous ne pouvez pas mettre à jour les paramètres en Mode démo',
    'error_measurement_system' => 'Système de mesure est nécessaire pour être réglé!',
    'error_password' => 'Mot de passe requis!',
    'error_permission' => 'Attention, vous n&rsquo;êtes pas autorisé à modifier %s!',
    'error_postcode' => 'Code postal Code postal doit contenir 6 alphanumérique',
    'error_provider_currency' => 'Nous n&rsquo;avons pas pu trouver la devise requise dans vos paramètres.',
    'error_required' => 'Obligatoire',
    'error_save' => 'Attention, des erreurs sont survenus lors de la validation de nouvelles valeurs. Veuillez vérifier vos paramètres, fixer des valeurs et réessayer.',
    'error_sender_telephone_10_digits' => 'Attention, le numéro de téléphone de l&rsquo;expéditeur doit contenir 10 chiffres',
    'error_sender_telephone_not_longer_15_digits' => 'Attention: le téléphone expéditeur ne doit pas dépasser 15 chiffres',
    'error_user_id' => 'ID utilisateur requis!',
    'error_weight_based_faked_box' => 'Des dimensions sont nécessaires',
    'error_zone_id' => 'Zone d&rsquo;origine requis!',
    'heading_title' => 'Post Canada Smart & Flexible',
    'help_adjustment_rules' => 'Chaque prochain ajustement de prix conditionnel adapté annulera les modifications des ajustements précédents du même type.',
    'help_avoid_delivery' => 'Ces paramètres permettent d&rsquo;éviter certains jours de livraison, lorsque le tarif peut être coûteux.<br /><strong>Attention:</strong> L&rsquo;activation de ces paramètres produira plus de requêtes API et réduira la vitesse.',
    'help_box_weight_adj_fix' => 'Augmenter le poids de chaque boîte par cette valeur',
    'help_contract_id' => 'Cet élément est nécessaire pour obtenir des tarifs réduits pour les clients commerciaux lorsque l&rsquo;élément de numéro de client est fourni. Elle doit être omise pour les membres de Solutions pour petites entreprises ou pour les utilisateurs commerciaux en général.',
    'help_custom_envelopes' => 'Comme les packages personnalisés, vous pouvez spécifier vos propres enveloppes. Vous pouvez utiliser une valeur facultative pour l&rsquo;épaisseur maximal pour faire passer votre enveloppe aux limites de service.',
    'help_custom_packages' => 'Vous pouvez expédier les articles dans vos propres paquets ainsi que des paquets %s. Entrez les dimensions intérieures du paquet: longueur, largeur et hauteur. La charge maximal et la tare sont facultatives.',
    'help_customer_groups' => 'Définir des groupes de clients qui peuvent utiliser cette méthode d&rsquo;expédition.',
    'help_customer_number' => 'Le numéro de client du propriétaire du mail (posté au nom du client). Ce numéro est nécessaire pour obtenir des tarifs réduits pour les clients commerciaux et les membres Solutions pour les petites entreprises. Il faut omettre d&rsquo;obtenir des taux de consommateur.',
    'help_cutoff' => 'Lorsque l&rsquo;utilisateur effectue le contrôle après cette période, ajoutez un jour de traitement supplémentaire. Ce paramètre est basé sur l&rsquo;heure du serveur.',
    'help_debug' => 'Sélectionnez ce qu&rsquo;il faut faire avec les données envoyées / reçues et les erreurs côté client',
    'help_dimension_adj_fix' => 'Augmenter chaque dimension de l&rsquo;article par cette valeur (à l&rsquo;exclusion des articles promotionnels)',
    'help_display_time' => 'Sélectionnez Oui si vous souhaitez afficher l&rsquo;estimation du délai de livraison (ex.: expédié dans 3 à 5 jours)',
    'help_display_weight' => 'Sélectionnez Oui si vous souhaitez afficher le poids d&rsquo;expédition (ex.: poids du colis: 2,76 Kg)',
    'help_geo_zones' => 'Où expédiez-vous',
    'help_inches' => 'Nous n&rsquo;avons pas pu trouver l&rsquo;unité pouces dans les paramètres, veuillezle le sélectionner dans la liste ou ajouter une nouvelle unité pouce à la configuration du magasin et recharger cette page.',
    'help_individual_tare' => 'Activer cette option pour inclure le poids de tare calculée pour chaque produit en fonction de ses dimensions',
    'help_insurance' => '<strong>Important:</strong> L&rsquo;activation de cette option peut entraîner une réduction du nombre de taux proposé et l&rsquo;augmentation des coûts d&rsquo;expédition.',
    'help_insurance_from' => '* S&rsquo;applique uniquement au total de la commande livrable',
    'help_label' => 'Activez cette option pour demander automatiquement des étiquettes de livraison lorsque la commande est confirmée ou manuellement dans le bordereau d&rsquo;expédition.',
    'help_label_foreign_data' => 'Cette valeur est également utilisé pour demander des étiquettes. Vous pouvez changer cette valeur dans votre <a href="%s">Paramètres de la boutique</a>',
    'help_label_format' => 'Utilisez un format 4x6 "si vous utilisez un périphérique d&rsquo;impression <a href="https://www.youtube.com/watch?v=mnAg-W_0bF8">comme ceci</a>. Cette option est uniquement disponible lorsque Convertisseur PDF (ImageMagick ou GraphicsMagick) et Ghostscript sont installés sur votre serveur.<br /><strong>Attention:</strong> Les étiquettes 4x6" sont disponibles uniquement pour l&rsquo;expédition contractuelle (lorsque le numéro contrat est définie). Veuillez utiliser l&rsquo;étiquette originale si vous utilisez la livraison non contractuelle.',
    'help_measurement_system' => 'La modification de ce paramètre permet de recharger cette page sans perte de données.<br /><strong>Attention:</strong> Si des valeurs sont déjà définies, cela les convertira aux unités de système de mesure sélectionnées (cette opération peut être un peu inexacte).',
    'help_minimum_rate' => 'Le prix d&rsquo;expédition ne sera pas calculé en dessous de cette valeur',
    'help_packer' => 'Sélectionnez le mode d&rsquo;emballage. <strong>Emballeur-3D</strong> Emballe les articles dans des paquets standard %s et paquet personnalisés. Il vous donne également une option d&rsquo;articles promotionnels. Le mode <strong>Individuel</strong> demande des emballages individuels pour chaque article. Le mode <strong>En fonction du poids</strong> demande des taux pour le poids total et la tare sera mise à zéro (utilisez le réglage du poids pour régler la tare des boites)',
    'help_pdf_converter' => 'Quelle bibliothèque doit être utilisée pour convertir les étiquettes PDF en images 4x6"',
    'help_postcode_dub' => 'Vous pouvez modifier cette valeur sur l&rsquo;onglet Général',
    'help_pounds' => 'Nous n&rsquo;avons pas pu trouver l&rsquo;unité livre dans les paramètres, veuillez sélectionner dans la liste ou ajouter une nouvelle unité de livre dans les paramètres du magasin et recharger cette page.',
    'help_prefix' => 'Afficher les options d&rsquo;expédition avec ce préfixe.',
    'help_processing_days' => 'Indiquez la quantité de jours dont vous avez besoin pour traiter la commande avant l&rsquo;expédition',
    'help_processing_holidays' => 'Spécifier les jours fériés pendant laquel vous n&rsquo;expédiez pas de commandes',
    'help_production' => 'Choisissez le mode de production pour obtenir des taux réels corrects. Choisissez le mode développeur pour tester la connexion et pour le débogage',
    'help_promo' => 'Vous pouvez spécifier les dimensions d&rsquo;objets promotionnels et de cadeaux en fonction du coût de la commande.',
    'help_promo_code' => 'Si vous avez un code de réduction promotionnelle, entrez-le ici.',
    'help_provider_currency' => 'Veuillez ajouter de la devise avec le code ISO spécifié à vos paramètres de localisation.',
    'help_rate_adj_fix' => 'Augmenter le prix d&rsquo;expédition par cette valeur',
    'help_rate_adj_per' => 'Augmenter le prix d&rsquo;expédition par ce pourcentage',
    'help_residential' => 'Indiquez Oui si l&rsquo;origine adresse renvoie à un bâtiment non-commercial',
    'help_services' => ' ',
    'help_sort_options' => 'Comment trier les options d&rsquo;expédition',
    'help_sort_order' => 'Ordre de tri parmi les autres services de livraison',
    'help_standard_packages' => 'Sélectionnez les paquets %s et les enveloppes standard que vous envoyez.',
    'help_stores' => 'Spécifier les magasins pouvant utiliser cette méthode d&rsquo;expédition.',
    'help_tracking' => 'Choisir quand envoyer des numéros de suivi au client.',
    'help_user_id' => '<p><a href="#" onclick="document.getElementById(\'canada_registration_manual\').style.display=\'block\'; return false;">Comment s&rsquo;inscrire</a></p><div id="canada_registration_manual" style="display: none"><li>Connectez-vous au <a href="https://www.canadapost.ca/web/en/pages/buserv/default.page">Centre d&rsquo;affaires en ligne de Postes Canada</a>.</li><li>Aller aux Programmes &rarr; Développeur.</li><li>Cliquez sur "Rejoindre le programme développeur".</li><li>Vous obtiendrez une table avec les clés API.</li><li>Copiez le nom d&rsquo;utilisateur et le mot de passe de la production ou du développeur et insérez-les dans les paramètres du module.</li><li>Choisissez le mode Extension correspondant (Production ou Développeur)</li></ol></div>',
    'help_weight_adj_fix' => 'Augmenter le poids de l&rsquo;article par cette valeur',
    'help_weight_adj_per' => 'Augmenter le poids de l&rsquo;article par ce pourcentage',
    'help_weight_based_limit' => 'Laissez ce champ vide pour utiliser la limite du service. Cette valeur est ignorée lorsque suppérieur à la limite du service.',
    'help_zone_id' => ' ',
    'license_buy' => 'Acheter une licence supplémentaire',
    'license_close' => 'Fermer',
    'license_info' => 'Une licence %s vous permet d\'installer le module sur une seule instance "E-Commerce Shop". Si vous avez besoin de licences supplémentaires, visitez <a href="%s">%s</a>. Nous offrons des rabais en cas d\'achats de licences supplémentaires. Si ce module a été installé sur votre site par un tiers développeur, vous devriez lui demander un transfert de licence. Plus de détails sont disponibles dans le fichier <a href="%s">LICENSE.txt</a> du module.',
    'license_title' => 'Notez s\'il vous plaît',
    'tab_adjustments' => 'Ajustments',
    'tab_general' => 'Général',
    'tab_labels' => 'Étiquettes',
    'tab_package' => 'Paquet',
    'tab_services' => 'Services',
    'tab_shipping' => 'Epédition',
    'text_4x6_label' => 'Étiquette imprimable par lot de 4x6 po',
    'text_add_adjustment_rule' => 'Ajouter une règle conditionnelle',
    'text_add_envelope' => 'Ajouter une nouvelle enveloppe personnalisée',
    'text_add_holiday' => 'Ajouter',
    'text_add_package' => 'Ajouter un nouveau paquet personnalisé',
    'text_add_promo' => 'Ajouter nouvelle dépendance',
    'text_all_customer_groups' => 'Tous les groupes de clients',
    'text_all_geo_zones' => 'Toutes les zones géographiques',
    'text_all_stores' => 'Tous les magasins',
    'text_automatic' => 'Automatique',
    'text_bugreport' => 'Signaler un bug',
    'text_debug_show_errors_log_all' => 'Erreurs : afficher au client. Log : tout',
    'text_debug_show_errors_log_errors' => 'Erreurs : afficherr au client. Log : erreurs uniquement',
    'text_debug_show_nothing_log_all' => 'Erreurs : cacher. Log : tout',
    'text_debug_show_nothing_log_errors' => 'Erreurs : cacher. Log : erreurs uniquement',
    'text_debug_show_nothing_log_nothing' => 'Erreurs : cacher. Log : vide',
    'text_developer_mode' => 'Mode développement',
    'text_disabled' => 'Désactivé',
    'text_domestic' => 'Services domestiques',
    'text_domestic_DOM.DT' => 'Delivered Tonight',
    'text_domestic_DOM.EP' => 'Expedited Parcel',
    'text_domestic_DOM.LIB' => 'Library Materials',
    'text_domestic_DOM.PC' => 'Priority',
    'text_domestic_DOM.RP' => 'Regular Parcel',
    'text_domestic_DOM.XP' => 'Default',
    'text_domestic_DOM.XP.CERT' => 'Certified',
    'text_enabled' => 'Activée',
    'text_export_settings' => 'Paramètres d&rsquo;exportation',
    'text_general' => 'Général',
    'text_import_settings' => 'Paramètres d&rsquo;importation',
    'text_international' => 'Services internationaux',
    'text_international_INT.IP.AIR' => 'Air',
    'text_international_INT.IP.SURF' => 'Surface',
    'text_international_INT.PW.ENV' => 'Envelope',
    'text_international_INT.PW.PAK' => 'Pak',
    'text_international_INT.PW.PARCEL' => 'Parcel',
    'text_international_INT.SP.AIR' => 'Air',
    'text_international_INT.SP.SURF' => 'Surface',
    'text_international_INT.TP' => 'Tracked Packet',
    'text_international_INT.XP' => 'Xpresspost',
    'text_labels_automatically' => 'Automatiquement',
    'text_labels_disabled' => 'Désactivé',
    'text_labels_manually' => 'Manuellement',
    'text_measurement_system_changed' => 'Valeurs a été converti. Veuillez vérifier les nouvelles valeurs et cliquez sur <strong>Sauvegarder</strong>.',
    'text_no' => 'Non',
    'text_no_sorting' => 'Pas de tri',
    'text_none' => 'Aucun',
    'text_only_for_admin' => 'Seulement pour l\'admin',
    'text_original_label' => 'Étiquette originale',
    'text_packer_3d_packer' => 'Emballeur-3D',
    'text_packer_individual' => 'Individuel',
    'text_packer_weight_based' => 'En fonction du poids',
    'text_parcel' => 'Parcel',
    'text_pdf_converter_gmagick' => 'GraphicsMagick',
    'text_pdf_converter_imagick' => 'ImageMagick',
    'text_priority' => 'Priority Worldwide',
    'text_production_mode' => 'Mode production',
    'text_refresh' => 'Rafraîchir',
    'text_remove_package' => 'Retirer',
    'text_remove_promo' => 'Retirer',
    'text_select_all' => 'Tout sélectionner',
    'text_services_by_user' => 'Tous les services disponibles',
    'text_shipping' => 'Epédition',
    'text_smallpacket' => 'Small Packet',
    'text_sort_by_price' => 'Trier par prix (croissant)',
    'text_sort_by_time' => 'Trier par délai de livraison (croissant)',
    'text_success' => 'Vous avez modifié %s avec succès!',
    'text_tracked' => 'Tracked Packet',
    'text_tracking_not_send' => 'Ne pas envoyer',
    'text_tracking_send_immediately' => 'Envoyer immédiatement lorsque la commande est passée',
    'text_tracking_send_shipped' => 'Suggérer quand la commande est expédiée',
    'text_unselect_all' => 'Tout déselectionner',
    'text_usa' => 'Services des États-Unis',
    'text_usa_USA.EP' => 'Expedited Parcel',
    'text_usa_USA.PW.ENV' => 'Envelope',
    'text_usa_USA.PW.PAK' => 'Pak',
    'text_usa_USA.PW.PARCEL' => 'Parcel',
    'text_usa_USA.SP.AIR' => 'Small Packet Air',
    'text_usa_USA.TP' => 'Default',
    'text_usa_USA.TP.LVM' => 'Large',
    'text_usa_USA.XP' => 'Xpresspost',
    'text_xpress' => 'Xpresspost',
    'text_yes' => 'Oui'
) + $_;
