<?php
// Heading
$_['heading_title']          = 'Product Multi Combo OR Bundle Discount';

// Text
$_['text_success']           = 'Success: You have modified Product Multi Combo OR Bundle Discounts!';
$_['text_list']              = 'Product Multi Combo OR Bundle Discount List';
$_['text_add']               = 'Add Product Multi Combo OR Bundle Discount';
$_['text_edit']              = 'Edit Product Multi Combo OR Bundle Discount';
$_['text_default']           = 'Default';

// Column
$_['column_product']            = 'Target Product';
$_['column_numofcombo']      = '# Of Combo';
$_['column_status']      = 'Status';
$_['column_limitset']      = 'Limit';
$_['column_date_added']      = 'Date Added';
$_['column_date_modified']      = 'Date Modified';
$_['column_action']          = 'Action';

// Entry
$_['entry_target_product'] = 'Target Product';
$_['entry_tabtitle'] = 'Tab Title';
$_['entry_combo_product'] = 'Combo Products';
$_['entry_discount'] = 'Discount';
$_['entry_discount_type'] = 'Discount Type';
$_['entry_customer_group'] = 'Customer Group';
$_['entry_store'] = 'Store';
$_['entry_status']           = 'Status';
$_['entry_limitset']      = 'Display Combo Limit';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify Product Multi Combo OR Bundle Discounts!';