<?php
// Text
$_['text_approve_subject']      = '%s - Your Member Account has been activated!';
$_['text_approve_welcome']      = 'Congratulations on becoming a member of ShopPal!  Your account has been activated';
$_['text_approve_login']        = 'Using this email, and the password your provided at sign up, you can log in at the following URL:';
$_['text_approve_services']     = 'Once logged in, you will be able to place orders for your organization, track supporter registrations, track shopping volume, track your monthly fundraising benefits and much more.';
$_['text_approve_thanks']       = 'If you have any questions or need assistance, please contact the ShopPal Support Team via the website support link, phone or email info@shoppal.ca\n\nWishing you great success,';
$_['text_transaction_subject']  = '%s - Member Benefit';
$_['text_transaction_received'] = 'Your supporters have shopped and fundraised %s for you!';
$_['text_transaction_total']    = 'Your total Member Benefit is now %s.';