<?php
//==============================================================================
// Individual Shipping v302.1
// 
// Author: Clear Thinking, LLC
// E-mail: johnathan@getclearthinking.com
// Website: http://www.getclearthinking.com
// 
// All code within this file is copyright Clear Thinking, LLC.
// You may not copy or reuse code within this file without written permission.
//==============================================================================

$version = 'v302.1';

//------------------------------------------------------------------------------
// Heading
//------------------------------------------------------------------------------
$_['heading_title']						= 'Individual Shipping';

//------------------------------------------------------------------------------
// Extension Settings
//------------------------------------------------------------------------------
$_['heading_extension_settings']		= 'Extension Settings';
$_['entry_status']						= 'Status: <div class="help-text">Set the status for the extension.</div>';
$_['entry_heading']						= 'Heading: <div class="help-text">The heading under which the individual shipping options will appear. If you are only allowing individual shipping, it\'s recommended to leave this blank.</div>';
$_['entry_eligible_methods']			= 'Eligible Shipping Methods: <div class="help-text">Select which shipping methods are eligible for individual shipping. Only these methods will be utilized in requesting individual shipping rates.<br /><br />To only show a method in Individual Shipping, and not as its own option, make sure to Disable it within its own settings. It will still be able to be utilized by Individual Shipping.</div>';
$_['entry_product_grouping']			= 'Group Products Based On: <div class="help-text">Choose the product data field that determines how products are grouped. To show charges for each individual product, select "product_id". To show charges for each manufacturer, choose "manufacturer_id"<br /><br />For any other custom grouping, choose the appropriate field. For instance, if you entered a value of "Large Items" in the "Location" field for products, then you would choose the "location" value here.</div>';
$_['entry_show_products']				= 'Show Products For Each Group: <div class="help-text">If set to "Yes", the products for each group will be displayed under the product grouping text (as set below).</div>';
$_['entry_image_size']					= 'Image Size: <div class="help-text">If showing products for each group, set the image width and height (in pixels).</div>';

//------------------------------------------------------------------------------
// Front-End Text
//------------------------------------------------------------------------------
$_['heading_front_end_text']			= 'Front-End Text';
$_['entry_group_text']					= 'Product Grouping Text: <div class="help-text">Set how product grouping headings are displayed. Use [group] in place of the manufacturer name if grouping by manufacturer, or the grouping value (e.g. "Large Items") that is entered in the product data field selected for the "Group Products Based On" setting.</div>';
$_['entry_not_required_text']			= '"Shipping Not Required" Text: <div class="help-text">Set the text for products or groups that do not require shipping.</div>';
$_['entry_no_rates_text']				= '"No Eligible Shipping Rates" Text: <div class="help-text">Set the text for when a product or group does not have any eligible shipping rates. This will disable any shipping choice from being made. Use [group] in place of the group that has no rates.</div>';
$_['entry_total_shipping_text']			= '"Total Shipping" Text: <div class="help-text">Set the text that appears above the total combined shipping costs.</div>';
$_['entry_line_item_text']				= '"Line Item" Text: <div class="help-text">Set how the line items are displayed for each shipping choice. Use [name] in place of the product or group name, [method] in place of the shipping method, and [cost] in place of the cost. HTML is supported.</div>';

//------------------------------------------------------------------------------
// Setting Overrides
//------------------------------------------------------------------------------
$_['heading_setting_overrides']			= 'Setting Overrides';
$_['help_setting_overrides']			= '<ul><li>Setting Overrides are optional, and allow you to override specific settings before presenting the individual shipping options. Choose the setting to override for the appropriate shipping method, then enter the qualifying group values. (Note: for products and manufacturers, this is the product_id and manufacturer_id, respectively.) Enter multiple group values separated by commas.</li><br />
<li>For example, to change the UPS "Ship From" postcode for a group of Apple products, you would choose the setting "postcode" under the "ups" group in the "Setting" dropdown, enter the postcode you want in the "Override Value" column, and enter the manufacturer_id for Apple in the "Group Value(s)" column.</li><br />
<li>You can find out your product_id\'s and manufacturer_id\'s by exporting your database, or looking at the browser URL when editing a product or manufacturer.</li></ul>';

$_['column_setting']					= 'Setting (Default Value)';
$_['column_override_value']				= 'Override Value';
$_['column_product_groups']				= 'Product Group(s)';
$_['column_action']						= 'Action';

$_['text_delete']						= 'Delete';
$_['button_add_override']				= 'Add Override';

//------------------------------------------------------------------------------
// Standard Text
//------------------------------------------------------------------------------
$_['copyright']							= '<hr /><div class="text-center" style="margin: 15px">' . $_['heading_title'] . ' (' . $version . ') &copy; <a target="_blank" href="http://www.getclearthinking.com">Clear Thinking, LLC</a></div>';

$_['standard_autosaving_enabled']		= 'Auto-Saving Enabled';
$_['standard_confirm']					= 'This operation cannot be undone. Continue?';
$_['standard_error']					= '<strong>Error:</strong> You do not have permission to modify ' . $_['heading_title'] . '!';
$_['standard_max_input_vars']			= '<strong>Warning:</strong> The number of settings is close to your <code>max_input_vars</code> server value. You should enable auto-saving to avoid losing any data.';
$_['standard_please_wait']				= 'Please wait...';
$_['standard_saved']					= 'Saved!';
$_['standard_saving']					= 'Saving...';
$_['standard_select']					= '--- Select ---';
$_['standard_success']					= 'Success!';
$_['standard_testing_mode']				= 'Your log is too large to open! Clear it first, then run your test again.';

$_['standard_module']					= 'Modules';
$_['standard_shipping']					= 'Shipping';
$_['standard_payment']					= 'Payments';
$_['standard_total']					= 'Order Totals';
$_['standard_feed']						= 'Feeds';
?>