<?php
//Gateway_name
$_['gateway_name']            = 'globalonehpppay';
// Heading
$_['heading_title']           = 'GlobalOnePay (Hosted Payment Page)';

// Text 
$_['text_edit']               = 'Edit plug-in settings';
$_['text_payment']            = 'Payment';
$_['text_extension_name']     = 'GlobalOnePay';
$_['text_success']            = 'Settings successfully saved.';
$_['text_yes']                = 'Yes';
$_['text_no']                 = 'No';
$_['text_enabled']            = 'Enabled';
$_['text_disabled']           = 'Disabled';
$_['text_true']               = 'True';
$_['text_false']              = 'False';
$_['text_globalonepayhpp'] = '<a onclick="window.open(\'http://www.globalonepay.com/developers/payments-integration-overview\');"><img src="view/image/payment/plugin-logo.png" alt="GlobalOnePay" title="GlobalOnePay" style="border: 1px solid #EEEEEE;" /></a>';
// Currency
$_['currency_eur']       = 'Euro';
$_['currency_gbp']       = 'Sterling';
$_['currency_usd']       = 'US Dollar';
$_['currency_sek']       = 'Swedish Krona';
$_['currency_dkk']       = 'Danish Krone';
$_['currency_nok']       = 'Nerwegian Krone';
$_['currency_aud']       = 'Australian Dollar';
$_['currency_cad']       = 'Canadian Dollar';



// Tabs
$_['tab_account']	 = 'Accounts';
$_['tab_order_status']	 = 'Order status';

      
// Accounts Tab
$_['entry_gateway']      = 'Payment Gateway:';
$_['entry_status']       = 'Status:';
$_['entry_test']         = 'Test Account:';
$_['entry_currency1']    = 'Primary Currency:';
$_['entry_currency1_tip']= 'This is the currency of your gateway Terminal ID (or one of your terminal ID\'s if you have multiple currencies).';
$_['entry_terminal1']    = 'Primary Terminal ID:';
$_['entry_terminal1_tip']= 'This is the Gateway Terminal ID for that currency.';
$_['entry_secret1']      = 'Primary Shared Secret:';
$_['entry_secret1_tip']  = 'This is the shared secret for that Terminal ID, exactly as you set it in the SelfCare System.';
$_['entry_currency2']    = 'Secondary Currency:';
$_['entry_terminal2']    = 'Secondary Terminal ID:';
$_['entry_secret2']      = 'Secondary Shared Secret:';
$_['entry_currency3']    = 'Tertiary Currency:';
$_['entry_terminal3']    = 'Tertiary Terminal ID:';
$_['entry_secret3']      = 'Tertiary Shared Secret:';
$_['entry_order_status'] = 'Order Status:';
$_['entry_send_receipt'] = 'Send a receipt?';
$_['entry_send_receipt_tip'] = 'If set to Yes then we will email a credit card receipt to the cardholder. This is not the same as the order confirmation email from OpenCart.';
$_['entry_sort_order']   = 'Sort Order:';


$_['entry_status_success']   = 'Success:';
$_['entry_status_declined']   = 'Declined:';
$_['entry_status_failed']   = 'Failed:';


// Error
$_['error_permission']   = 'Warning: You do not have permission to modify the Payment settings!';
$_['error_terminal1']    = 'Primary Terminal ID required!';
$_['error_secret1']      = 'Primary Shared Secret required!';
?>
