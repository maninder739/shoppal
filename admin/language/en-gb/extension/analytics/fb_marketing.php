<?php
// Heading
$_['heading_title']    	= 'Facebook Marketing';

// Tabs
$_['text_pixel_configuration'] 		= 'FB Pixel Configuration';
$_['text_marketing_api'] 			= 'FB Marketing';
$_['text_advanced_matching_data'] 	= 'Advanced Matching Data';
$_['text_about'] 					= 'About';


// Text
$_['text_extension']    = 'Extensions';
$_['text_module']      	= 'Modules';
$_['text_success']     	= 'Success: You have modified Facebook Marketing module!';
$_['text_edit']       	= 'Edit Facebook Marketing module';

$_['text_status'] 		= 'Status';
$_['text_name'] 		= 'Name';
$_['text_path'] 		= 'Path';
$_['text_value'] 		= 'Value';

$_['text_ajax_save']	= 'Clicking the button will instantly save the new status';

$_['text_activated']    = 'Event activated';
$_['text_deactivated']  = 'Event deactivated';
$_['text_adv_match_activated']    = 'Advanced Matching Data Type activated';
$_['text_adv_match_deactivated']    = 'Advanced Matching Data Type deactivated';

// Placeholder
$_['placeholder_path'] 		= 'Path for the event eg. \'?route=product/manufacturer&manufacturer_id=5\'';
$_['placeholder_value'] 			= 'Set a value for this event';

// Entry
$_['entry_status']     			= 'Status';
$_['entry_pixel_id']     		= 'Facebook Pixel ID';
$_['entry_product_catalog_id']  = 'Product Catalog ID';
$_['entry_events']  			= 'Pixel Events';
$_['entry_advanced_matching']  	= 'Advanced Matching';
$_['entry_values_vat_inc']  	= 'Values VAT inclusive';
$_['entry_manual_only_mode']  	= 'Manual Only Mode';
$_['entry_pid']  				= 'Pixel Product ID';


// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Facebook Marketing module!';

// Help
$_['help_pixel_id']				= 'Login to your <a href=\"https://www.facebook.com/ads/manager/pixel/facebook_pixel/\" target=\"_blank\"><u>Facebook Ads Management</u></a> account and select the Pixel ID, in the upper right corner. Paste the Pixel ID into this field.';
$_['help_product_catalog_id'] 	= 'The Product Catalog ID is required for Dynamic Product Ads';
$_['help_advanced_matching'] 	= 'The Facebook pixel has an advanced matching feature that enables you to send your customer data through the pixel to match more website actions with Facebook users. <a href=\"https://developers.facebook.com/docs/facebook-pixel/pixel-with-ads/conversion-tracking#advanced_match\" target=\"_blank\"><u>More</u></a>';
$_['help_values_vat_inc']		= 'Pixel values are VAT inclusive';
$_['help_manual_only_mode']		= 'Facebook Pixel is able to send button click data (\'SubscribedButtonClick event\') and page metadata (\'Microdata event\') from your website. Enable Manual Only Mode to disable sending this additional data.';
$_['help_pid']					= 'Select which product attribute will be used for the pixel parameter <i>content_ids</i>.';