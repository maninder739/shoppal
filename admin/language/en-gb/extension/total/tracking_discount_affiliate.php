<?php

$_['entry_tracking_discount_amount'] = 'Tracking Discount Amount';
$_['help_tracking_discount_amount'] = 'Global setting is used if left blank';
$_['entry_tracking_discount_type']   = 'Tracking Discount Type';
$_['option_tracking_discount_fixed'] = 'Fixed';
$_['option_tracking_discount_percent'] = 'Percent (%)';
$_['error_tracking_discount_amount']   = 'Error: Only numbers (such as 5 or 5.5) allowed for Tracking Discount Amount, or leave it blank';

