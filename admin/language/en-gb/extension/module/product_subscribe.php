<?php

// Heading
$_['heading_title']       			= 'Product Subscriptions';

// Text
$_['text_edit']						= 'Product Subscriptions Settings';
$_['text_module']      				= 'Modules';
$_['text_yes']						= 'Yes';
$_['text_no']						= 'No';
$_['text_success']					= 'You have successfully modified module product subscribe';
$_['text_product_subscribe']		= 'Product Subscription Settings';

// Entry
$_['entry_add_shipping']			= 'Add Shipping (Global)';
$_['entry_add_taxes']				= 'Add Taxes (Global)';
$_['entry_recurring_add_order']		= 'Add New Order for each Recurring Payment';

// Help
$_['help_add_shipping']				= 'Add shipping charges to ALL recurring charges.  This is a global setting and will override any subscription specific settings for shipping.';
$_['help_add_taxes']				= 'Add taxes to ALL recurring charges.  This is a global global and will override any subscription specific settings for taxes.';
$_['help_recurring_add_order']		= 'Add a new order for each automatic recurring payment made. This is a global setting and will override any subscription specific settings. This setting only works when using PayPal Express.';

// Error
$_['error_permission']    			= 'Warning: You do not have permission to modify module product subscription!';
