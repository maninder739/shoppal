<?php
// Heading
$_['heading_title']    = 'Product Multi Combo OR Bundle Discount';

$_['text_module']      = 'Modules';
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified Product Multi Combo OR Bundle Discount Module!';
$_['text_edit']        = 'Edit Product Multi Combo OR Bundle Discount Module';

// Entry
$_['entry_status']     = 'Status'; 
 
// Error 
$_['error_permission'] = 'Warning: You do not have permission to modify Product Multi Combo OR Bundle Discount Module!';