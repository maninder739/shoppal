<?php
// Heading
$_['heading_title']         = 'Member Orders Report';

// Text
$_['text_list']             = 'Member Orders List';
$_['text_all_status']       = 'All Statuses';

// Column
$_['column_code']       = 'Tracking';
$_['column_company']       = 'Organization';
$_['column_contact']       = 'Org. Contact';
$_['column_customer']       = 'Customer Name';
$_['column_email']          = 'E-Mail';
$_['column_customer_group'] = 'Customer Group';
$_['column_status']         = 'Status';
$_['column_orders']         = 'No. Orders';
$_['column_products']       = 'No. Products';
$_['column_total']          = 'Total';
$_['column_action']         = 'Action';
$_['export_excel']         = 'Export Excel';

// Entry
$_['entry_date_start']      = 'Date Start';
$_['entry_date_end']        = 'Date End';
$_['entry_customer']		= 'Customer';
$_['entry_member']		= 'Organization';
$_['entry_code']		= 'Tracking';
$_['entry_status']          = 'Order Status';