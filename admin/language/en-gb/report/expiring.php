<?php

// Heading
$_['heading_title'] = 'CC Expiring Report';

// Text
$_['text_list'] = 'CC Expiring Report List';

// Column
$_['column_customer_id'] = 'Customer Id';
$_['column_firstname'] = 'Firstname';
$_['column_lastname'] = 'Lastname';
$_['column_email'] = 'Email';
$_['column_telephone'] = 'Telephone';
$_['column_payment_city'] = 'Payment City';
$_['column_payment_zone'] = 'Payment Zone';
$_['column_payment_postcode'] = 'Payment Postcode';
$_['column_expmonth'] = 'Expire Month';
$_['column_expyear'] = 'Expire Year'; 


$_['export_excel'] = 'Export Excel'; 

// Entry
 

