<?php

// Heading
$_['heading_title'] = 'Members Report';

// Text
$_['text_list'] = 'Members List';
$_['text_all_status'] = 'All Statuses';

// Column
$_['column_aff_id'] = 'Affiliate Id';
$_['column_aff_first_name'] = 'Firstname';
$_['column_aff_last_name'] = 'Lastname';
$_['column_email'] = 'E-Mail';
$_['column_telephone'] = 'Telephone';
$_['column_fax'] = 'Fax';
$_['column_company'] = 'Company';
$_['column_code'] = 'Code';
$_['column_address1'] = 'Address1';
$_['column_address2'] = 'Address2';

$_['column_city'] = 'City';
$_['column_postcode'] = 'Postcode';
$_['column_zone'] = 'Zone';
$_['column_country'] = 'Country';
$_['column_name'] = 'Name';
$_['column_agreement'] = 'agreement';

$_['column_action'] = 'Action';
$_['export_excel'] = 'Export Excel';

// Entry
$_['entry_date_start'] = 'Date Start';
$_['entry_date_end'] = 'Date End';
$_['entry_customer'] = 'Customer';
$_['entry_member'] = 'Organization';
$_['entry_code'] = 'Tracking';
$_['entry_status'] = 'Order Status';
