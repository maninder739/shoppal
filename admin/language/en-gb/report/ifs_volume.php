<?php
// Heading
$_['heading_title']     = 'IFS Report';

// Text
$_['text_list']         = 'IFS Report';

// Column
$_['column_parent_affiliate_name']  = 'Parent Affiliate Name';
$_['column_parent_affiliate_code']  = 'Parent Affiliate Code';
$_['column_parent_affiliate_company']  = 'Parent Affiliate Company';


$_['column_tracking_code']  = 'Tracking Code';
$_['column_order_id']     = 'Order Id';
$_['column_order_date']     = 'Order Date';

$_['column_customer_id']     = 'Customer ID';
$_['column_customer_name']  = 'Customer Name';
$_['column_status']     = 'Status';


$_['column_affname']  = 'Affiliate Name';
$_['column_affcode']  = 'Affiliate Code';
$_['column_affcompany']  = 'Affiliate company';


$_['column_total']      = 'Total';
$_['column_action']     = 'Action';




// Entry
$_['entry_date_start']  = 'Date Start';
$_['entry_date_end']    = 'Date End';
$_['entry_tracking_code']    = 'Tracking Code';
$_['entry_customer']     = 'Customer Name';
$_['entry_current_affiliate']      = 'Affiliate';
$_['column_parent_affiliate'] = 'Parent Affiliate';