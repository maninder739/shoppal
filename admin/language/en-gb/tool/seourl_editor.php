<?php
// Heading
$_['heading_title']     = 'SEO Url Editor';

// Text
$_['text_success']      = 'Success: You have modified seo url!';
$_['text_list']         = 'SEO Url List';
$_['text_add']          = 'Add seo url';
$_['text_edit']         = 'Edit seo url';
$_['tab_store']         = 'Store';
$_['tab_product']       = 'Products';
$_['tab_category']      = 'Categories';
$_['tab_information']   = 'Information';
$_['tab_manufacturer']  = 'Manufacturers';
$_['button_edit_out']   = 'Edit on entity page';

// Column
$_['column_query']      = 'Query path';
$_['column_keyword']    = 'SEO Url';
$_['column_action']     = 'Action';

// Entry
$_['entry_query']       = 'Query path';
$_['entry_keyword']     = 'SEO Url';

// Help
$_['help_query']        = 'Enter route path';
$_['help_keyword']      = 'Enter short url';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify seo urls!';
$_['error_query']       = 'Query path must be between 3 and 64 characters!';