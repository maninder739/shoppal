<?php
// Heading
$_['heading_title']    = 'Shopping Volume';

// Text
$_['text_account']     = 'Account';
$_['show_empty_text']  = 'No Volume to display';
$_['serialNumber']     = 'S.No';
$_['firstname']        = 'Name';
$_['lastname']         = 'Last Name';
$_['order_status']     = 'Order Status';
$_['view_all']		    = 'VIEW ALL';
$_['current_month']		= 'Current Month';
$_['previous_month']		= 'Previous Month';
$_['next_month']		= 'Next Month';
$_['shopping_volume']		= "VOLUME OF PURCHASE";
$_['cust_order_date']		= 'ORDER DATE';
$_['cust_name']				= 'CUSTOMER';
$_['cust_retail']		= 'SHOPPING VOLUME';
$_['cust_percentage']		= 'YOUR PERCENTAGE';
$_['table_total']		= 'Total';
$_['table_total_cust']		= ' Customer';
$_['table_total_custs']		= ' Customers';
$_['table_total_none']		= 'No Shopping to Display';

