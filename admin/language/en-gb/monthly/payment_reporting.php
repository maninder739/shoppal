<?php
// Heading
$_['heading_title']             = 'Payment Reporting';

// Text
$_['text_success']              = 'Success: You have modified payment reporting!';
$_['text_approved']             = 'You have approved %s accounts!';
$_['text_list']                 = 'Payment Reporting List';
$_['text_add']                  = 'Add  Payment Reporting';
$_['text_edit']                 = 'Payment';
$_['text_affiliate_detail']     = 'Affiliate Details';
$_['text_affiliate_address']    = 'Affiliate Address';
$_['text_balance']              = 'Balance';
$_['text_cheque']               = 'Cheque';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Bank Transfer';

// Column
$_['column_name']               = 'Affiliate Name';
$_['column_email']              = 'E-Mail';
$_['column_organization']       = 'Organization';
$_['column_paymentId']       	= 'PaymentID';
$_['column_paymentDate']       	= 'PaymentDate';
$_['column_amountCalc']       	= 'Amount Calc';
$_['column_amountSent']       	= 'Amount Sent';
$_['column_paymentRef']       	= 'Payment Ref';
$_['column_paymentNote']       	= 'Payment Note';

$_['column_code']               = 'Tracking Code';
$_['column_balance']            = 'Balance';
$_['column_status']             = 'Status';
$_['column_approved']           = 'Approved';
$_['column_date_added']         = 'Date Added';
$_['column_description']        = 'Description';
$_['column_amount']             = 'Amount';
$_['column_action']             = 'Action';

// Entry
$_['entry_firstname']           = 'First Name';
$_['entry_lastname']            = 'Last Name';
$_['entry_email']               = 'E-Mail';
$_['entry_telephone']           = 'Telephone';
$_['entry_extension']       	= 'Extension';
$_['entry_fax']                 = 'Fax';
$_['entry_status']              = 'Status';
$_['entry_password']            = 'Password';
$_['entry_confirm']             = 'Confirm';
$_['entry_company']             = 'Company';
$_['entry_website']             = 'Web Site';
$_['entry_address_1']           = 'Address 1';
$_['entry_address_2']           = 'Address 2';
$_['entry_city']                = 'City';
$_['entry_postcode']            = 'Postcode';
$_['entry_country']             = 'Country';
$_['entry_zone']                = 'Region / State';
$_['entry_code']                = 'Tracking Code';
$_['entry_commission']          = 'Commission (%)';
$_['entry_tax']                 = 'Tax ID';
$_['entry_payment']             = 'Payment Method';
$_['entry_cheque']              = 'Cheque Payee Name';
$_['entry_paypal']              = 'PayPal Email Account';
$_['entry_bank_name']           = 'Bank Name';
$_['entry_bank_branch_number']  = 'ABA/BSB number (Branch Number)';
$_['entry_bank_swift_code']     = 'SWIFT Code';
$_['entry_bank_account_name']   = 'Account Name';
$_['entry_bank_account_number'] = 'Account Number';
$_['entry_amount']              = 'Amount';
$_['entry_description']         = 'Description';
$_['entry_name']                = 'Affiliate Name';
$_['entry_approved']            = 'Approved';
$_['entry_date_added']          = 'Date Added';
$_['entry_member_type']         = 'Member Type';
$_['entry_date_start']  		= 'Date Start';
$_['entry_date_end']    		= 'Date End';
$_['entry_PaymentNote']    		= 'Payment Note';
$_['entry_amountCalc']       	= 'Amount Calc';
$_['entry_amountSent']       	= 'Amount Sent';
$_['entry_paymentRef']       	= 'Payment Ref';


/*  new fields  */
$_['entry_custom_logo']         = 'Custom Logo';
$_['entry_mission']             = 'Your Mission';
$_['entry_mission_french']      = 'Your Mission French';
$_['entry_slide_background']    = 'Choose your slide background';	

/* new fields  */

/* organization fields */
$_['entry_position_organization'] = 'Position in Organization';
$_['entry_organization_phone_number'] = 'Organization Phone Number';
$_['entry_branded_website'] = 'ShopPal Branded Website Name Preferences ( Shoppal.ca/ ??? )';
$_['entry_organization_details'] = 'Organization Details';
$_['entry_organization_website'] = 'Organization Website';
$_['entry_organization_hashtags'] = 'Organization Hashtags';
$_['entry_organization_facebook_link'] = 'Organization Facebook Link';
$_['entry_organization_instagram'] = 'Organization Instagram';
$_['entry_organization_twitter'] = 'Organization Twitter';
$_['entry_organization_linkedIn_link'] = 'Organization LinkedIn Link';
$_['entry_agreement_heading'] = 'The Organization permits Shoppal to use their Logo and Name, not associated with any fundraising amounts, in the following manner';
$_['entry_agreement_checkbox1'] = 'on the main, or other pages of the ShopPal website that display Memberinformation';
$_['entry_agreement_checkbox2'] = 'on Social Media and other Internet Marketing platforms, and/or';
$_['entry_agreement_checkbox3'] = 'on Print Media and or Radio/Television Media.';

$_['entry_organization_detailed'] = 'Organization "What we do" / "Detailed - About Us"';
$_['entry_organization_detailed_french'] = 'Organization "What we do" / "Detailed - About Us French"';
$_['entry_hear_about_us'] = 'How did you hear about us?';
$_['entry_type_question'] = 'Type a question';
$_['entry_shoppal_website'] = 'ShopPal Website';
$_['entry_local_store'] = 'Local Store';
$_['entry_local_event'] = 'Local Event';
$_['entry_local_flyer'] = 'Flyer';
$_['entry_other_fundraising_organization'] = 'Other Fundraising Organization';
$_['entry_other_hear_about_us'] = 'Shoppal Rep. Name';
$_['entry_other_details_text'] = 'Other Details';
$_['entry_other_details_input'] = 'other details';



/* organization fields */

// Help
$_['help_code']                 = 'The tracking code that will be used to track referrals.';
$_['help_commission']           = 'Percentage the affiliate receives on each order.';

// Error
 
$_['error_warning']             = 'Warning: Please check the form carefully for errors!';

$_['error_permission']          = 'Warning: You do not have permission to modify affiliates!';
$_['error_exists']              = 'Warning: E-Mail Address is already registered!';
$_['error_firstname']           = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']            = 'Last Name must be between 1 and 32 characters!';
$_['error_email']               = 'E-Mail Address does not appear to be valid!';
$_['error_cheque']              = 'Cheque Payee Name required!';
$_['error_paypal']              = 'PayPal Email Address does not appear to be valid!';
$_['error_bank_account_name']   = 'Account Name required!';
$_['error_bank_account_number'] = 'Account Number required!';
$_['error_telephone']           = 'Telephone must be between 3 and 32 characters!';
$_['error_password']            = 'Password must be between 4 and 20 characters!';
$_['error_confirm']             = 'Password and password confirmation do not match!';
$_['error_address_1']           = 'Address 1 must be between 3 and 128 characters!';
$_['error_city']                = 'City must be between 2 and 128 characters!';
$_['error_postcode']            = 'Postcode must be between 2 and 10 characters for this country!';
$_['error_country']             = 'Please select a country!';
$_['error_zone']                = 'Please select a region / state!';
$_['error_code']                = 'Tracking Code required!';
$_['error_code_exists']         = 'Tracking code is being used by another affiliate!';

$_['error_payment']             = 'Warning: Please select the payment!';
$_['error_paymentref']             = 'Warning: Payment reference is required!';

/* organization fields  */

$_['error_position_in_organization']  = 'Must be between 1 and 32 characters!';
$_['error_organization_phone_number']  = 'Organization phone number is invalid';

/* organization fields */

/* new fields */
$_['error_mission']             = 'Your Mission should be less than or equal to 217 characters!';
$_['error_mission_french']      = 'Your Mission French should be less than or equal to 217 characters!';
$_['tracking_code_referral']    = 'Tracking Code Referral';
/* new fields */