<?php
// All text for Admin Dashboard going from here.

//Heading title
$_['birdsview']             = 'Birdsview';
$_['graphchart']            = 'Graph Chart';

//Coloumn Title text which is popup text too
$_['today'] = 'Today';
$_['yesterday']             = 'Yesterday';
$_['last_week']             = 'Last Week';
$_['last_month']            = 'Last Month';
$_['last_year']             = 'Last Year';


//Popups text
$_['otherday']              = 'Other Day';
$_['thismonth']             = 'This Month';
$_['thisweek']              = 'This Week';
$_['thisyear']              = 'This year';

//Row title text which is popup text too
$_['text_total_order']      = 'Order';
$_['text_total_discount']   = 'Discount ';
$_['text_total_product']    = 'Products ';
$_['text_total_revenue']    = 'Revenue ';
$_['text_total_payment']    = 'Payment ';
$_['text_total_miss_order'] = 'Missing Order ';

?>