<?php
/**
 * Module version: 5.4.6
 *
 * For bugs and suggestions please contact: support@drugoe.de
 *
 * CANADAPOST Smart & Flexible Shipping Module - Terms and conditions
 *
 * 1. Preamble: This Agreement governs the relationship between customer, (hereinafter: Licensee) and "Drugoe",
 *    an approved module vendor (hereinafter: Licensor). This Agreement sets the terms, rights,
 *    restrictions and obligations on using CANADAPOST Smart & Flexible Shipping Module
 *    (hereinafter: The Software) created and owned by Licensor, as detailed herein
 *
 * 2. License Grant: Licensor hereby grants Licensee a Personal, Non-assignable & non-transferable, Perpetual,
 *    Commercial, Royalty free, Including the rights to create but not distribute derivative works,
 *    Non-exclusive license, all with accordance with the terms set forth and other legal restrictions
 *    set forth in 3rd party software used while running Software.
 * 2.1. Limited: Licensee may use Software for the purpose of:
 * 2.1.1. Running Software on Licensee’s Website[s] and Server[s];
 * 2.1.2. Allowing 3rd Parties to run Software on Licensee’s Website[s] and Server[s];
 * 2.1.3. Publishing Software’s output to Licensee and 3rd Parties;
 * 2.1.4. Distribute verbatim copies of Software’s output (including compiled binaries);
 * 2.1.5. Modify Software to suit Licensee’s needs and specifications.
 * 2.2. This license is granted perpetually, as long as you do not materially breach it.
 * 2.3. Binary Restricted: Licensee may sublicense Software as a part of a larger work containing more than Software,
 *      distributed solely in Object or Binary form under a personal, non-sublicensable, limited license.
 *      Such redistribution shall be limited to unlimited codebases.
 * 2.4. Non Assignable & Non-Transferable: Licensee may not assign or transfer his rights and duties under this license.
 * 2.5. Commercial, Royalty Free: Licensee may use Software for any purpose,
 *      including paid-services, without any royalties
 * 2.6. Including the Right to Create Derivative Works: Licensee may create derivative works based on Software,
 *      including amending Software’s source code, modifying it, integrating it into a larger work or removing
 *      portions of Software, as long as no distribution of the derivative works is made
 *
 * 3. Term & Termination: The Term of this license shall be until terminated.
 *    Licensor may terminate this Agreement, including Licensee’s license in the case where Licensee :
 * 3.1. became insolvent or otherwise entered into any liquidation process; or
 * 3.2. exported The Software to any jurisdiction where licensor may not enforce his rights under this agreements in; or
 * 3.3. Licensee was in breach of any of this license's terms and conditions and such breach was not cured,
 *      immediately upon notification; or
 * 3.4. Licensee in breach of any of the terms of clause 2 to this license; or
 * 3.5. Licensee otherwise entered into any arrangement which caused Licensor to be unable to enforce his rights
 *      under this License.
 *
 * 4. Payment: In consideration of the License granted under clause 2, Licensee shall pay Licensor a fee,
 *    via Credit-Card, PayPal or any other mean which Licensor may deem adequate. Failure to perform payment
 *    shall construe as material breach of this Agreement.
 *
 * 5. Upgrades, Updates and Fixes: Licensor may provide Licensee, from time to time, with Upgrades, Updates or Fixes,
 *    as detailed herein and according to his sole discretion. Licensee hereby warrants to keep The Softwareup-to-date
 *    and install all relevant updates and fixes, and may, at his sole discretion, purchase upgrades,
 *    according to the rates set by Licensor. Licensor shall provide any update or Fix free of charge;
 *    however, nothing in this Agreement shall require Licensor to provide Updates or Fixes.
 * 5.1. Upgrades: for the purpose of this license, an Upgrade shall be a material amendment in The Software,
 *    which contains new features and or major performance improvements and shall be marked as a new version number.
 *    For example, should Licensee purchase The Software under version 1.X.X,
 *    an upgrade shall commence under number 2.0.0.
 * 5.2. Updates:  for the purpose of this license, an update shall be a minor amendment in The Software,
 *      which may contain new features or minor improvements and shall be marked as a new sub-version number.
 *      For example, should Licensee purchase The Software under version 1.1.X,
 *      an upgrade shall commence under number 1.2.0.
 * 5.3. Fix: for the purpose of this license, a fix shall be a minor amendment in The Software,
 *      intended to remove bugs or alter minor features which impair the The Software's functionality.
 *      A fix shall be marked as a new sub-sub-version number. For example, should Licensee purchase Software
 *      under version 1.1.1, an upgrade shall commence under number 1.1.2.
 *
 * 6. Support: Software is provided under an AS-IS basis and without any support, updates or maintenance.
 *    Nothing in this Agreement shall require Licensor to provide Licensee with support or fixes to any bug, failure,
 *    mis-performance or other defect in The Software.
 * 6.1. Bug Notification:  Licensee may provide Licensor of details regarding any bug, defect or failure in The Software
 *      promptly and with no delay from such event; Licensee shall comply with Licensor's request for information
 *      regarding bugs, defects or failures and furnish him with information, screenshots and try to reproduce
 *      such bugs, defects or failures.
 * 6.2. Feature Request:  Licensee may request additional features in Software, provided, however, that
 *      (i) Licensee shall waive any claim or right in such feature should feature be developed by Licensor;
 *     (ii) Licensee shall be prohibited from developing the feature, or disclose such feature request, or feature,
 *          to any 3rd party directly competing with Licensor or any 3rd party which may be,
 *          following the development of such feature, in direct competition with Licensor;
 *    (iii) Licensee warrants that feature does not infringe any 3rd party patent, trademark,
 *          trade-secret or any other intellectual property right; and
 *     (iv) Licensee developed, envisioned or created the feature solely by himself.
 *
 * 7. Liability:  To the extent permitted under Law, The Software is provided under an AS-IS basis.
 *    Licensor shall never, and without any limit, be liable for any damage, cost, expense or any other payment incurred
 *    by Licensee as a result of Software’s actions, failure, bugs and/or any other interaction between The Software
 *    and Licensee’s end-equipment, computers, other software or any 3rd party, end-equipment, computer or services.
 *    Moreover, Licensor shall never be liable for any defect in source code written by Licensee
 *    when relying on The Software or using The Software’s source code.
 *
 * 8. Warranty:
 * 8.1. Intellectual Property: Licensor hereby warrants that The Software does not violate or infringe
 *      any 3rd party claims in regards to intellectual property, patents and/or trademarks and that
 *      to the best of its knowledge no legal action has been taken against it for any infringement or violation
 *      of any 3rd party intellectual property rights.
 * 8.2. No-Warranty: The Software is provided without any warranty; Licensor hereby disclaims any warranty
 *      that The Software shall be error free, without defects or code which may cause damage to Licensee’s computers
 *      or to Licensee, and that Software shall be functional. Licensee shall be solely liable to any damage,
 *      defect or loss incurred as a result of operating software and undertake the risks contained in running
 *      The Software on License’s Server[s] and Website[s].
 * 8.3. Prior Inspection:  Licensee hereby states that he inspected The Software thoroughly and found it satisfactory
 *      and adequate to his needs, that it does not interfere with his regular operation and that it does meet
 *      the standards and scope of his computer systems and architecture. Licensee found that The Software interacts
 *      with his development, website and server environment and that it does not infringe any of End User License
 *      Agreement of any software Licensee may use in performing his services. Licensee hereby waives any claims
 *      regarding The Software's incompatibility, performance, results and features, and warrants that he inspected
 *      the The Software.
 *
 * 9. No Refunds: Licensee warrants that he inspected The Software according to clause 7(c) and that it is adequate
 *    to his needs. Accordingly, as The Software is intangible goods, Licensee shall not be, ever,
 *    entitled to any refund, rebate, compensation or restitution for any reason whatsoever, even if The Software
 *    contains material flaws.
 *
 * 10. Indemnification: Licensee hereby warrants to hold Licensor harmless and indemnify Licensor for any lawsuit
 *     brought against it in regards to Licensee’s use of The Software in means that violate, breach or otherwise
 *     circumvent this license, Licensor's intellectual property rights or Licensor's title in The Software.
 *     Licensor shall promptly notify Licensee in case of such legal action and request Licensee’s consent
 *     prior to any settlement in relation to such lawsuit or claim.
 *
 * 11. Governing Law, Jurisdiction: Licensee hereby agrees not to initiate class-action lawsuits against Licensor
 *     in relation to this license and to compensate Licensor for any legal fees, cost or attorney fees should
 *     any claim brought by Licensee against Licensor be denied, in part or in full.
 */

?><?php


$_ = array(
    'button_cancel' => 'Cancel',
    'button_save' => 'Save',
    'demo_disabled' => 'This setting is disabled in Demo Mode',
    'demo_info' => 'You are viewing control panel of this extension in Demo Mode. These settings are shown for the extension features review. You can not update their values.',
    'demo_title' => 'Demo Mode',
    'entry_account_rates' => 'Show Account Rates',
    'entry_address' => 'Origin Address',
    'entry_adjustment_rules' => 'Conditional Price Adjustments',
    'entry_avoid_delivery' => 'Avoid Delivery',
    'entry_avoid_delivery_saturdays' => 'Avoid delivery on Saturdays',
    'entry_avoid_delivery_sundays' => 'Avoid delivery on Sundays',
    'entry_box_weight_adj_fix' => 'Box Weight Adjustment (fixed)',
    'entry_boxes_checked' => 'Here are standard packages and envelopes that you selected',
    'entry_boxes_unchecked' => 'Here are available standard packages and envelopes',
    'entry_city' => 'Origin City',
    'entry_contact' => 'Contact Developer',
    'entry_contract_id' => 'Contract Id',
    'entry_country_id' => 'Origin Country',
    'entry_custom_envelopes' => 'Custom Envelopes',
    'entry_custom_packages' => 'Custom Packages',
    'entry_customer_groups' => 'Customer Groups',
    'entry_customer_number' => 'Customer Number',
    'entry_cutoff' => 'Cutoff Time',
    'entry_cutoff_help' => '<a href="#" onclick="document.getElementById(\'how-to-change-timezone\').style.display=\'block\'; return false;"> This is not my office local time</a><div id="how-to-change-timezone" style="display: none;">You can set the server timezone in <code>php.ini</code> file by changing the value of setting <code>date.timezone</code> to the <a href="http://php.net/manual/en/timezones.php">one of supported values</a>. If the timezone is not set in <code>php.ini</code> file, you can also set it in System file <code>/system/startup.php</code> by changing the argument of function <code>date_default_timezone_set</code>. Example: <code>date_default_timezone_set("America/Denver");</code></div>.',
    'entry_date_format' => 'Estimated Delivery Date Format',
    'entry_debug' => 'Debug Level',
    'entry_dimension_adj_fix' => 'Item Dimensions Adjustment (fixed)',
    'entry_display_time' => 'Display Estimated Delivery Time',
    'entry_display_weight' => 'Display Delivery Weight',
    'entry_dropoff' => 'Dropoff Type',
    'entry_geo_zones' => 'Geo Zones',
    'entry_height' => 'Height',
    'entry_inches' => 'Inches Length Class',
    'entry_individual_tare' => 'Include tare weight',
    'entry_insurance' => 'Include Insurance',
    'entry_insurance_from' => 'Orders above *',
    'entry_key' => 'Developer Key',
    'entry_label' => 'Download Shipping Labels',
    'entry_label_format' => 'Label Format',
    'entry_length' => 'Length',
    'entry_maintenance' => 'Maintenance',
    'entry_max_height' => 'Max Thickness',
    'entry_max_load' => 'Max Load',
    'entry_measurement_system' => 'Measurement System',
    'entry_meter' => 'Account Meter Number',
    'entry_min_cost' => 'Min Cost',
    'entry_minimum_rate' => 'Minimal Shipping Rate',
    'entry_mod' => 'Modification Status',
    'entry_packer' => 'Packer Algorithm',
    'entry_password' => 'Password',
    'entry_pdf_converter' => 'PDF Converter',
    'entry_pickup' => 'Pickup Type',
    'entry_postcode' => 'Origin Postal Code',
    'entry_pounds' => 'Pounds Weight Class',
    'entry_prefix' => 'Shipping Options Name Prefix',
    'entry_processing_days' => 'Processing Days',
    'entry_processing_holidays' => 'Holidays',
    'entry_processing_saturdays' => 'We ship on Saturdays',
    'entry_processing_sundays' => 'We ship on Sundays',
    'entry_production' => 'Extension Mode',
    'entry_promo' => 'Promotional Items and Gifts',
    'entry_promo_code' => 'Promo Code',
    'entry_provider_currency' => 'Currency',
    'entry_rate_adj_fix' => 'Rate Adjustment (fixed)',
    'entry_rate_adj_per' => 'Rate Adjustment (percent)',
    'entry_residential' => 'Origin Address is Residential',
    'entry_sender_company' => 'Sender Company',
    'entry_sender_name' => 'Sender Name',
    'entry_sender_telephone' => 'Sender Telephone',
    'entry_services' => 'Select services you want to ship with',
    'entry_sort_options' => 'Shipping Options Sort Order',
    'entry_sort_order' => 'Sort Order',
    'entry_standard_packages' => 'Standard Packages and Envelopes',
    'entry_status' => 'Status',
    'entry_stores' => 'Stores',
    'entry_tare' => 'Tare',
    'entry_tax' => 'Tax Class',
    'entry_tracking' => 'Send Tracking Numbers to Customer',
    'entry_user_id' => 'User ID',
    'entry_version' => 'Extension Version',
    'entry_weight' => 'Weight',
    'entry_weight_adj_fix' => 'Item Weight Adjustment (fixed)',
    'entry_weight_adj_per' => 'Item Weight Adjustment (percent)',
    'entry_weight_based_faked_box' => 'Package dimensions',
    'entry_weight_based_limit' => 'Maximum package weight',
    'entry_width' => 'Width',
    'entry_zone_id' => 'Origin Zone',
    'error_address' => 'Origin Address Required!',
    'error_city' => 'Origin City Required!',
    'error_country_id' => 'Origin Country Required!',
    'error_customer_number' => 'Customer Number is required when labels are enabled',
    'error_demo' => 'Warning: You can not update settings in Demo Mode',
    'error_measurement_system' => 'Measurement system is required to be set!',
    'error_password' => 'Password Required!',
    'error_permission' => 'Warning: You do not have permission to modify %s!',
    'error_postcode' => 'Postal Code must be 6 alphanumeric',
    'error_provider_currency' => 'We could not find required currency in your settings.',
    'error_required' => 'Required',
    'error_save' => 'Warning: There were some errors while validating new values. Please check your settings, fix values and try again.',
    'error_sender_telephone_10_digits' => 'Warning: Sender telephone must contain 10 digits',
    'error_sender_telephone_not_longer_15_digits' => 'Warning: Sender telephone must be not longer than 15 digits',
    'error_user_id' => 'User ID Required!',
    'error_weight_based_faked_box' => 'Dimensions are required',
    'error_zone_id' => 'Origin Zone Required!',
    'heading_title' => 'Canada Post Smart & Flexible',
    'help_adjustment_rules' => 'Every next matched conditional price adjustment will override changes of the previous adjustments of the same type.',
    'help_avoid_delivery' => 'These settings help to avoid delivery on specific days, when rates may be expensive.<br /><strong>Warning:</strong> enabling these settings will produce more API requests and reduce speed.',
    'help_box_weight_adj_fix' => 'Increase each box weight by this value',
    'help_contract_id' => 'This element is required to obtain discounted rates for commercial customers when the customer-number element is provided. It must be omitted for Solutions for Small Business members or general business users.',
    'help_custom_envelopes' => 'Like the Custom Packages, you can specify your own envelopes. You can use an optional value of Max Thickness to make your envelope pass service limits.',
    'help_custom_packages' => 'You can ship items in your own packages as well as %s packages. Enter inside package dimensions: length, width and height. Max load and tare weight are optional.',
    'help_customer_groups' => 'Specify customer groups which can use this shipping method.',
    'help_customer_number' => 'The customer number of the owner of the mail (mailed on behalf of customer). This number is required to obtain discounted rates for commercial customers and Solutions for Small Business members. It must be omitted to obtain consumer rates.',
    'help_cutoff' => 'When user is checking out after this time add one more processing day. This setting is based on server time.',
    'help_debug' => 'Select what to do with sent/received data and customer-side errors',
    'help_dimension_adj_fix' => 'Increase each item dimension by this value (excluding promotional items)',
    'help_display_time' => 'Select Yes if you want to display the estimated shipping time (e.g. Ships within 3 to 5 days)',
    'help_display_weight' => 'Select Yes if you want to display the shipping weight (e.g. Delivery Weight : 2.76 Kg\'s)',
    'help_geo_zones' => 'Where do you ship',
    'help_inches' => 'We could not find Inches unit in the settings, please select it from the list or add new Inches unit to the store settings and reload this page.',
    'help_individual_tare' => 'Enable this option to include calculated tare weight for each product based on its dimensions',
    'help_insurance' => '<strong>Important:</strong> enabling this option may result in reduced number of suggested rates and increased shipping costs.',
    'help_insurance_from' => '* Applies only to shippable order total',
    'help_label' => 'Enable this option to request shipping labels automatically when order is confirmed or manually in the packing list.',
    'help_label_foreign_data' => 'This value is also used to request labels. You can change this value in your <a href="%s">Store Settings</a>',
    'help_label_format' => 'Use 4x6" format if you use a printing device <a href="https://www.youtube.com/watch?v=mnAg-W_0bF8">like this</a>. This option is only available when PDF converter (ImageMagick or GraphicsMagick) and ghostscript are installed on your server.<br /><strong>Warning:</strong> 4x6" labels are only available for Contract Shipping (when then Contract Id is set). Please use Original Label if you use Non-Contract Shipping.',
    'help_measurement_system' => 'Changing this setting will reload this page without data loss.<br /><strong>Warning:</strong> if any values are already set this will convert them to the selected measurement system units (this operation might be a little inaccurate).',
    'help_minimum_rate' => 'Shipping price will not be calculated below than this value',
    'help_packer' => 'Select packaging mode. <strong>3D-Packer</strong> will pack items in standard %s packages and custom packages. Also it gives you an option of promotional items. <strong>Individual</strong> mode will request individual packages for each item. <strong>Weight-based</strong> mode will request rates for total weight and tare will be set to zero (use box weight adjustment to set tare)',
    'help_pdf_converter' => 'Which library should be used to convert PDF labels into 4x6" images',
    'help_postcode_dub' => 'You can change this value on General tab',
    'help_pounds' => 'We could not find Pounds unit in the settings, please select it from the list or add new Pounds unit to the store settings and reload this page.',
    'help_prefix' => 'Display shipping options with this prefix',
    'help_processing_days' => 'Specify the amount of days you need to process the order before shipping',
    'help_processing_holidays' => 'Specify holidays when you do not ship orders',
    'help_production' => 'Choose production mode to get real correct rates. Choose developer mode to test connection and for debugging purposes',
    'help_promo' => 'You can specify dimensions of promotional items and gifts depending on order cost.',
    'help_promo_code' => 'If you have a promotional discount code, enter it here.',
    'help_provider_currency' => 'Please add currency with specified ISO code to your localisation settings.',
    'help_rate_adj_fix' => 'Increase shipping price by this value',
    'help_rate_adj_per' => 'Increase shipping price by this percent',
    'help_residential' => 'Specify Yes if origin address points to a non-commercial building',
    'help_services' => ' ',
    'help_sort_options' => 'How to sort shipping options',
    'help_sort_order' => 'Sorting order among other delivery services',
    'help_standard_packages' => 'Select standard %s packages and envelopes that you ship in.',
    'help_stores' => 'Specify stores which can use this shipping method.',
    'help_tracking' => 'Choose when to send tracking numbers to customer.',
    'help_user_id' => '<p><a href="#" onclick="document.getElementById(\'canada_registration_manual\').style.display=\'block\'; return false;">How to Register</a></p><div id="canada_registration_manual" style="display: none"><p>Please, follow these steps to get the right username and password:</p><ol><li>Sign in to <a href="https://www.canadapost.ca/web/en/pages/buserv/default.page">Canada Post Online Business Center</a>.</li><li>Go to Programs &rarr; Developer.</li><li>Click "Join Developer Program".</li><li>You wll get to a table with API Keys.</li><li>Copy Production or Developer username and password and insert them to the module settings.</li><li>Choose the corresponding Extension Mode (Production or Developer)</li></ol></div>',
    'help_weight_adj_fix' => 'Increase item weight by this value',
    'help_weight_adj_per' => 'Increase item weight by this percent',
    'help_weight_based_limit' => 'Leave this field blank to use service limit. This value is ignored when greater than service limit.',
    'help_zone_id' => ' ',
    'license_buy' => 'Buy Additional License',
    'license_close' => 'Close',
    'license_info' => 'One %s license allows you to install the module to a single E-Commerce shop instance. If you need additional licenses, please visit <a href="%s">%s</a>. We offer discounts in case of additional license purchases. If this module was installed to your website by a third party developer, you should ask him for a license transfer. More details are available in the <a href="%s">LICENSE.txt</a> file of the module.',
    'license_title' => 'Please note',
    'tab_adjustments' => 'Adjustments',
    'tab_general' => 'General',
    'tab_labels' => 'Labels',
    'tab_package' => 'Package',
    'tab_services' => 'Services',
    'tab_shipping' => 'Shipping',
    'text_4x6_label' => '4x6" batch-printable label',
    'text_add_adjustment_rule' => 'Add Conditional Rule',
    'text_add_envelope' => 'Add new custom envelope',
    'text_add_holiday' => 'Add',
    'text_add_package' => 'Add new custom package',
    'text_add_promo' => 'Add new dependency',
    'text_all_customer_groups' => 'All Customer Groups',
    'text_all_geo_zones' => 'All Geo Zones',
    'text_all_stores' => 'All Stores',
    'text_automatic' => 'Automatic',
    'text_bugreport' => 'Report a bug',
    'text_debug_show_errors_log_all' => 'Errors: show to customer. Log: everything',
    'text_debug_show_errors_log_errors' => 'Errors: show to customer. Log: errors only',
    'text_debug_show_nothing_log_all' => 'Errors: hide. Log: everything',
    'text_debug_show_nothing_log_errors' => 'Errors: hide. Log: errors only',
    'text_debug_show_nothing_log_nothing' => 'Errors: hide. Log: nothing',
    'text_developer_mode' => 'Developer Mode',
    'text_disabled' => 'Disabled',
    'text_domestic' => 'Domestic Services',
    'text_domestic_DOM.DT' => 'Delivered Tonight',
    'text_domestic_DOM.EP' => 'Expedited Parcel',
    'text_domestic_DOM.LIB' => 'Library Materials',
    'text_domestic_DOM.PC' => 'Priority',
    'text_domestic_DOM.RP' => 'Regular Parcel',
    'text_domestic_DOM.XP' => 'Default',
    'text_domestic_DOM.XP.CERT' => 'Certified',
    'text_enabled' => 'Enabled',
    'text_export_settings' => 'Export settings',
    'text_general' => 'General',
    'text_import_settings' => 'Import settings',
    'text_international' => 'International Services',
    'text_international_INT.IP.AIR' => 'Air',
    'text_international_INT.IP.SURF' => 'Surface',
    'text_international_INT.PW.ENV' => 'Envelope',
    'text_international_INT.PW.PAK' => 'Pak',
    'text_international_INT.PW.PARCEL' => 'Parcel',
    'text_international_INT.SP.AIR' => 'Air',
    'text_international_INT.SP.SURF' => 'Surface',
    'text_international_INT.TP' => 'Tracked Packet',
    'text_international_INT.XP' => 'Xpresspost',
    'text_labels_automatically' => 'Automatically',
    'text_labels_disabled' => 'Disabled',
    'text_labels_manually' => 'Manually',
    'text_measurement_system_changed' => 'Values has been converted. Please verify new values and click <strong>Save</strong>.',
    'text_no' => 'No',
    'text_no_sorting' => 'No sorting',
    'text_none' => 'None',
    'text_only_for_admin' => 'Only for Admin',
    'text_original_label' => 'Original label',
    'text_packer_3d_packer' => '3D-Packer',
    'text_packer_individual' => 'Individual',
    'text_packer_weight_based' => 'Weight-based',
    'text_parcel' => 'Parcel',
    'text_pdf_converter_gmagick' => 'GraphicsMagick',
    'text_pdf_converter_imagick' => 'ImageMagick',
    'text_priority' => 'Priority Worldwide',
    'text_production_mode' => 'Production Mode',
    'text_refresh' => 'Refresh',
    'text_remove_package' => 'Remove',
    'text_remove_promo' => 'Remove',
    'text_select_all' => 'Select All',
    'text_services_by_user' => 'All available services',
    'text_shipping' => 'Shipping',
    'text_smallpacket' => 'Small Packet',
    'text_sort_by_price' => 'Sort by price (ascending)',
    'text_sort_by_time' => 'Sort by delivery time (ascending)',
    'text_success' => 'Success: You have modified %s!',
    'text_tracked' => 'Tracked Packet',
    'text_tracking_not_send' => 'Do not send',
    'text_tracking_send_immediately' => 'Send immediately when order is placed',
    'text_tracking_send_shipped' => 'Suggest when order is shipped',
    'text_unselect_all' => 'Unselect All',
    'text_usa' => 'United States Services',
    'text_usa_USA.EP' => 'Expedited Parcel',
    'text_usa_USA.PW.ENV' => 'Envelope',
    'text_usa_USA.PW.PAK' => 'Pak',
    'text_usa_USA.PW.PARCEL' => 'Parcel',
    'text_usa_USA.SP.AIR' => 'Small Packet Air',
    'text_usa_USA.TP' => 'Default',
    'text_usa_USA.TP.LVM' => 'Large',
    'text_usa_USA.XP' => 'Xpresspost',
    'text_xpress' => 'Xpresspost',
    'text_yes' => 'Yes'
) + $_;
