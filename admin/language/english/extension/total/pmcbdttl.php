<?php
// Heading
$_['heading_title']    = 'Product Multi Combo OR Bundle Discount';

// Text
$_['text_total']       = 'Order Totals';
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified Product Multi Combo OR Bundle Discount total!';
$_['text_edit']        = 'Edit Product Multi Combo OR Bundle Discount';

// Entry
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Product Multi Combo OR Bundle Discount total!';