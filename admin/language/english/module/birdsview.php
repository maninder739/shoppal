<?php
// All text for admin setting panel going from here.

//Breadcump
$_['birdsview_text_module']     = 'Modules';
$_['birdsview_text_home']       = 'Home';


//Heading Title
$_['heading_title']             = 'Birds View';


//App status text
$_['birdsview_table_status']    = 'Table Status:';
$_['birdsview_chart_status']    = 'Chart Status:';

//Coloumn Text
$_['today']                     = 'Today:';
$_['yesterday']                 = 'Yesterday:';
$_['lastweek']                  = 'Last Week:';
$_['lastmonth']                 = 'Last Month:';
$_['lastyear']                  = 'Last Year:';

//Row text
$_['order']                     = 'Order:';
$_['product']                   = 'Product:';
$_['discount']                  = 'Discount:';
$_['revenue']                   = 'Revenue:';
$_['missorder']                 = 'Miss Order:';


// Error
$_['error_permission']          = 'Warning: You do not have permission to modify Birdsview extension!';

//Successs
$_['text_success']              = 'Success: You have modified Birdview setting.';
?>