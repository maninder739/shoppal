<?php
// ini_set("display_errors", "1");
// error_reporting(E_ALL);
ini_set('memory_limit', '-1');
// Version
define('VERSION', '2.3.0.2');
define("APPLICATION_CONFIG", 'admin'); //admin long login

// Configuration
if (is_file('config.php')) {
	require_once('config.php');
}

// Install
if (!defined('DIR_APPLICATION')) {
	header('Location: ../install/index.php');
	exit;
}

//VirtualQMOD
require_once('../vqmod/vqmod.php');
VQMod::bootup();

// VQMODDED Startup
require_once(VQMod::modCheck(DIR_SYSTEM . 'startup.php'));

start('admin');
