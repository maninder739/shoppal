<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-affiliate" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
                <h1><?php echo $heading_title; ?></h1>
                <ul class="breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
            <?php } ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
                </div>
                <div class="panel-body">
                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-affiliate" class="form-horizontal">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                            <?php if ($affiliate_id) { ?>
                            <li><a href="#tab-organization" data-toggle="tab"><?php echo $tab_organization; ?></a></li>
                            <?php
                            }?>
                            <li><a href="#tab-payment" data-toggle="tab"><?php echo $tab_payment; ?></a></li>
                            <?php if ($affiliate_id) { ?>
                            <li><a href="#tab-transaction" data-toggle="tab"><?php echo $tab_transaction; ?></a></li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-general">
                                <fieldset>
                                    <legend><?php echo $text_affiliate_detail; ?></legend>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-member-type"><?php echo $entry_member_type; ?></label>
                                        <div class="col-sm-10">
                                            <select name="member_type" id="member_type" class="form-control" onchange="get_custom_fields(this.value)">
                                                <option <?php if($member_type == '1') echo 'selected';?> value="1">Affiliate</option>
                                                <option <?php if($member_type == '2') echo 'selected';?> value="2">FST</option>
                                                <option <?php if($member_type == '3') echo 'selected';?> value="3">FST Member</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-firstname"><?php echo $entry_firstname; ?></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control" />
                                            <?php if ($error_firstname) { ?>
                                            <div class="text-danger"><?php echo $error_firstname; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-lastname"><?php echo $entry_lastname; ?></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control" />
                                            <?php if ($error_lastname) { ?>
                                            <div class="text-danger"><?php echo $error_lastname; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
                                            <?php if ($error_email) { ?>
                                            <div class="text-danger"><?php echo $error_email; ?></div>
                                            <?php  } ?>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-telephone"><?php echo $entry_telephone; ?></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" class="form-control" />
                                            <?php if ($error_telephone) { ?>
                                            <div class="text-danger"><?php echo $error_telephone; ?></div>
                                            <?php  } ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-fax"><?php echo $entry_fax; ?></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="fax" value="<?php echo $fax; ?>" placeholder="<?php echo $entry_fax; ?>" id="input-fax" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-code"><span data-toggle="tooltip" title="<?php echo $help_code; ?>"><?php echo $entry_code; ?></span></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="code" value="<?php echo $code; ?>" placeholder="<?php echo $entry_code; ?>" id="input-code" class="form-control" />
                                            <?php if ($error_code) { ?>
                                            <div class="text-danger"><?php echo $error_code; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-password"><?php echo $entry_password; ?></label>
                                        <div class="col-sm-10">
                                            <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" autocomplete="off" id="input-password" class="form-control"  />
                                            <?php if ($error_password) { ?>
                                            <div class="text-danger"><?php echo $error_password; ?></div>
                                            <?php  } ?>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-confirm"><?php echo $entry_confirm; ?></label>
                                        <div class="col-sm-10">
                                            <input type="password" name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $entry_confirm; ?>" autocomplete="off" id="input-confirm" class="form-control" />
                                            <?php if ($error_confirm) { ?>
                                            <div class="text-danger"><?php echo $error_confirm; ?></div>
                                            <?php  } ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                                        <div class="col-sm-10">
                                            <select name="status" id="input-status" class="form-control">
                                                <?php if ($status) { ?>
                                                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                                <option value="0"><?php echo $text_disabled; ?></option>
                                                <?php } else { ?>
                                                <option value="1"><?php echo $text_enabled; ?></option>
                                                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <?php if ($affiliate_id) { ?>
                                    <!-- new details -->
                                    <div class="custom_fields">
                                        <!-- new fields -->
                                        <div class="form-group">
                                            <?php
                                            $affiliate_logo = 'no_image.png';
                                            $affiliate_logo_old = '';
                                            if(!empty($custom_logo))
                                            {
                                            $affiliate_logo = 'data/'.$custom_logo;
                                            $affiliate_logo_old = $custom_logo;
                                            }
                                            ?>
                                            <input type ="hidden" name ="old_custom_logo" value="<?php echo $affiliate_logo_old;?>">
                                            <label class="col-sm-2 control-label" for="input-custom_logo"><?php echo $entry_custom_logo; ?></label>
                                            <div class="col-sm-10">
                                                <input type="file" accept="image/*" name="custom_logo" placeholder="<?php echo $entry_custom_logo; ?>" id="custom_logo" />
                                                <img id="blah" src="../images/<?php echo  $affiliate_logo;?>" alt="" style="width: 150px;height:150px; margin-top: 5px;">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-mission"><?php echo $entry_mission; ?></label>
                                        <div class="col-sm-10">
                                            <textarea rows="2" placeholder="<?php echo $entry_mission; ?>" id="input-mission"  name="mission" cols="94" style="height: 66px;" onkeyup="countChar(this)"><?php echo $entry_mission_view; ?></textarea>
                                            <div class="col-md-12" style="padding: 0px"><em>211 words max</em>
                                                <div id="charNum" class="text-danger" style="display: none;"></div>
                                            </div>
                                            <?php
                                            if ($error_mission) {
                                            ?>
                                            <div class="text-danger"><?php echo $error_mission; ?></div>
                                            <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <!--In French -->
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-mission-french"><?php echo $entry_mission_french; ?></label>
                                        <div class="col-sm-10">
                                            <textarea rows="2" placeholder="<?php echo $entry_mission_french; ?>" id="input-mission-french"  name="mission_french" cols="94" onkeyup="missioncountChar(this)" style="height: 66px;"><?php echo $entry_mission_french_view; ?></textarea>
                                            <div class="col-md-12" style="padding: 0px">
                                                <em>211 words max</em>
                                                <div id="missioncharNum" class="text-danger" style="display: none;"></div>
                                            </div>
                                            <?php
                                            if ($error_mission_french) {
                                            ?>
                                            <div class="text-danger"><?php echo $error_mission_french; ?></div>
                                            <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <!--In French -->
                                    <div class="custom_fields">
                                        
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input_slide"><?php echo $entry_slide_background; ?></label>
                                            <div class="col-sm-10">
                                                <input type="hidden" name="slider_image" id="slider_image" value="<?php echo $slider_image;?>">
                                                <!-- <div class="col-sm-12 slider-img">
                                                    <a href="javascript:void(0)" onclick="return select_image('slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7', 'slide_img8', 'image1.jpg')"><img id="slide_img1" src="../image/slider/image1.jpg" alt="" style="<?php if ($slider_image == 'image1.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                                </div>
                                                <div class="col-sm-12 slider-img">
                                                    <a href="javascript:void(0)" onclick="return select_image('slide_img2', 'slide_img1', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7', 'slide_img8', 'image2.jpg')"><img id="slide_img2" src="../image/slider/image2.jpg" alt="" style="<?php if ($slider_image == 'image2.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                                </div>
                                                <div class="col-sm-12 slider-img">
                                                    <a href="javascript:void(0)" onclick="return select_image('slide_img3', 'slide_img1', 'slide_img2', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7', 'slide_img8', 'image3.jpg')"><img id="slide_img3" src="../image/slider/image3.jpg" alt=""  style="<?php if ($slider_image == 'image3.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                                </div>
                                                <div class="col-sm-12 slider-img">
                                                    <a href="javascript:void(0)" onclick="return select_image('slide_img4', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img5', 'slide_img6', 'slide_img7', 'slide_img8', 'image4.jpg')"><img id="slide_img4" src="../image/slider/image4.jpg" alt=""  style="<?php if ($slider_image == 'image4.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                                </div>
                                                <div class="col-sm-12 slider-img">
                                                    <a href="javascript:void(0)" onclick="return select_image('slide_img5', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img6', 'slide_img7', 'slide_img8', 'image5.jpg')"><img id="slide_img5" src="../image/slider/image5.jpg" alt=""  style="<?php if ($slider_image == 'image5.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                                </div>
                                                <div class="col-sm-12 slider-img">
                                                    <a href="javascript:void(0)" onclick="return select_image('slide_img6', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img7', 'slide_img8', 'image6.jpg')"><img id="slide_img6" src="../image/slider/image6.jpg" alt=""  style="<?php if ($slider_image == 'image6.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                                </div>
                                                <div class="col-sm-12 slider-img">
                                                    <a href="javascript:void(0)" onclick="return select_image('slide_img7', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img8', 'image7.jpg')"><img id="slide_img7" src="../image/slider/image7.jpg" alt=""  style="<?php if ($slider_image == 'image7.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                                </div>
                                                <div class="col-sm-12 slider-img">
                                                    <a href="javascript:void(0)" onclick="return select_image('slide_img8', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7', 'image8.jpg')"><img id="slide_img8" src="../image/slider/image8.jpg" alt=""  style="<?php if ($slider_image == 'image8.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                                </div> -->
                                                <div class="col-sm-12 slider-img">
                                                    <a href="javascript:void(0)" onclick="return select_image('slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7', 'slide_img8','slide_img9','slide_img10','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image1.jpg')"><img id="slide_img1" src="../images/slider/image1.jpg" alt="" style="<?php if ($slider_image == 'image1.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                                </div>
                                                <div class="col-sm-12 slider-img">
                                                    <a href="javascript:void(0)" onclick="return select_image('slide_img2', 'slide_img1', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7', 'slide_img8' , 'slide_img9','slide_img10','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image2.jpg')"><img id="slide_img2" src="../images/slider/image2.jpg" alt="" style="<?php if ($slider_image == 'image2.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                                </div>
                                                <div class="col-sm-12 slider-img">
                                                    <a href="javascript:void(0)" onclick="return select_image('slide_img3', 'slide_img1', 'slide_img2', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7', 'slide_img8','slide_img9','slide_img10','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image3.jpg')"><img id="slide_img3" src="../image/slider/image3.jpg" alt=""  style="<?php if ($slider_image == 'image3.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                                </div>
                                                <div class="col-sm-12 slider-img">
                                                    <a href="javascript:void(0)" onclick="return select_image('slide_img4', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img5', 'slide_img6', 'slide_img7', 'slide_img8', 'slide_img9','slide_img10','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image4.jpg')"><img id="slide_img4" src="../image/slider/image4.jpg" alt=""  style="<?php if ($slider_image == 'image4.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                                </div>
                                                <div class="col-sm-12 slider-img">
                                                    <a href="javascript:void(0)" onclick="return select_image('slide_img5', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img6', 'slide_img7', 'slide_img8', 'slide_img9','slide_img10','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image5.jpg')"><img id="slide_img5" src="../image/slider/image5.jpg" alt=""  style="<?php if ($slider_image == 'image5.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                                </div>
                                                <div class="col-sm-12 slider-img">
                                                    <a href="javascript:void(0)" onclick="return select_image('slide_img6', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img7', 'slide_img8','slide_img9','slide_img10','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20',  'image6.jpg')"><img id="slide_img6" src="../image/slider/image6.jpg" alt=""  style="<?php if ($slider_image == 'image6.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                                </div>
                                                <div class="col-sm-12 slider-img">
                                                    <a href="javascript:void(0)" onclick="return select_image('slide_img7', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img8','slide_img9','slide_img10','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image7.jpg')"><img id="slide_img7" src="../image/slider/image7.jpg" alt=""  style="<?php if ($slider_image == 'image7.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                                </div>
                                                <div class="col-sm-12 slider-img">
                                                    <a href="javascript:void(0)" onclick="return select_image('slide_img8', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img9','slide_img10','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image8.jpg')"><img id="slide_img8" src="../image/slider/image8.jpg" alt=""  style="<?php if ($slider_image == 'image8.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                                </div>
                                                
                                                <div class="col-sm-12 slider-img">
                                                    <a href="javascript:void(0)" onclick="return select_image('slide_img9', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img10','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image9.jpg')"><img id="slide_img9" src="../image/slider/image9.jpg" alt=""  style="<?php if ($slider_image == 'image9.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                                </div>
                                                <div class="col-sm-12 slider-img">
                                                    <a href="javascript:void(0)" onclick="return select_image('slide_img10', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image10.jpg')"><img id="slide_img10" src="../image/slider/image10.jpg" alt=""  style="<?php if ($slider_image == 'image10.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                                </div>
                                                <div class="col-sm-12 slider-img">
                                                    <a href="javascript:void(0)" onclick="return select_image('slide_img11', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image11.jpg')"><img id="slide_img11" src="../image/slider/image11.jpg" alt=""  style="<?php if ($slider_image == 'image11.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                                </div>
                                                <div class="col-sm-12 slider-img">
                                                    <a href="javascript:void(0)" onclick="return select_image('slide_img12', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img11', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image12.jpg')"><img id="slide_img12" src="../image/slider/image12.jpg" alt=""  style="<?php if ($slider_image == 'image12.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                                </div>
                                                <div class="col-sm-12 slider-img">
                                                    <a href="javascript:void(0)" onclick="return select_image('slide_img13', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img11', 'slide_img12', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image13.jpg')"><img id="slide_img13" src="../image/slider/image13.jpg" alt=""  style="<?php if ($slider_image == 'image13.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                                </div>
                                                <div class="col-sm-12 slider-img">
                                                    <a href="javascript:void(0)" onclick="return select_image('slide_img14', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img11', 'slide_img13', 'slide_img12', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image14.jpg')"><img id="slide_img14" src="../image/slider/image14.jpg" alt=""  style="<?php if ($slider_image == 'image14.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                                </div>
                                                <div class="col-sm-12 slider-img">
                                                    <a href="javascript:void(0)" onclick="return select_image('slide_img15', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img11', 'slide_img13', 'slide_img14', 'slide_img12', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image15.jpg')"><img id="slide_img15" src="../image/slider/image15.jpg" alt=""  style="<?php if ($slider_image == 'image15.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                                </div>
                                                <div class="col-sm-12 slider-img">
                                                    <a href="javascript:void(0)" onclick="return select_image('slide_img16', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img11', 'slide_img12', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img20', 'image16.jpg')"><img id="slide_img16" src="../image/slider/image16.jpg" alt=""  style="<?php if ($slider_image == 'image16.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                                </div>
                                                <div class="col-sm-12 slider-img">
                                                    <a href="javascript:void(0)" onclick="return select_image('slide_img17', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img11', 'slide_img12', 'slide_img13','slide_img14', 'slide_img15', 'slide_img16',  'slide_img18', 'slide_img19', 'slide_img20', 'image17.jpg')"><img id="slide_img17" src="../image/slider/image17.jpg" alt=""  style="<?php if ($slider_image == 'image17.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                                </div>
                                                <div class="col-sm-12 slider-img">
                                                    <a href="javascript:void(0)" onclick="return select_image('slide_img18', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img11', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img19', 'slide_img20', 'slide_img12', 'image18.jpg')"><img id="slide_img18" src="../image/slider/image18.jpg" alt=""  style="<?php if ($slider_image == 'image18.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                                </div>
                                                <div class="col-sm-12 slider-img">
                                                    <a href="javascript:void(0)" onclick="return select_image('slide_img19', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img11', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18',   'slide_img20', 'slide_img12', 'image19.jpg')"><img id="slide_img19" src="../image/slider/image19.jpg" alt=""  style="<?php if ($slider_image == 'image19.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                                </div>
                                                <div class="col-sm-12 slider-img">
                                                    <a href="javascript:void(0)" onclick="return select_image('slide_img20', 'slide_img1', 'slide_img2', 'slide_img3', 'slide_img4', 'slide_img5', 'slide_img6', 'slide_img7','slide_img8','slide_img9','slide_img10', 'slide_img11', 'slide_img13', 'slide_img14', 'slide_img15', 'slide_img16', 'slide_img17', 'slide_img18', 'slide_img19', 'slide_img12', 'image20.jpg')"><img id="slide_img20" src="../image/slider/image20.jpg" alt=""  style="<?php if ($slider_image == 'image20.jpg') echo 'border-color:#dd0017;border-width:4px;border-style:solid;'; ?>"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--new fields-->
                                    <?php
                                    }
                                    ?>
                                </fieldset>
                                <fieldset>
                                    <legend><?php echo $text_affiliate_address; ?></legend>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-company"><?php echo $entry_company; ?></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="company" value="<?php echo $company; ?>" placeholder="<?php echo $entry_company; ?>" id="input-company" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-website"><?php echo $entry_website; ?></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="website" value="<?php echo $website; ?>" placeholder="<?php echo $entry_website; ?>" id="input-website" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-address-1"><?php echo $entry_address_1; ?></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="address_1" value="<?php echo $address_1; ?>" placeholder="<?php echo $entry_address_1; ?>" id="input-address-1" class="form-control" />
                                            <?php if ($error_address_1) { ?>
                                            <div class="text-danger"><?php echo $error_address_1; ?></div>
                                            <?php  } ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-address-2"><?php echo $entry_address_2; ?></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="address_2" value="<?php echo $address_2; ?>" placeholder="<?php echo $entry_address_2; ?>" id="input-address-2" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-city"><?php echo $entry_city; ?></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="city" value="<?php echo $city; ?>" placeholder="<?php echo $entry_city; ?>" id="input-city" class="form-control" />
                                            <?php if ($error_city) { ?>
                                            <div class="text-danger"><?php echo $error_city; ?></div>
                                            <?php  } ?>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-postcode"><?php echo $entry_postcode; ?></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="postcode" value="<?php echo $postcode; ?>" placeholder="<?php echo $entry_postcode; ?>" id="input-postcode" class="form-control" />
                                            <?php if ($error_postcode) { ?>
                                            <div class="text-danger"><?php echo $error_postcode; ?></div>
                                            <?php  } ?>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-country"><?php echo $entry_country; ?></label>
                                        <div class="col-sm-10">
                                            <select name="country_id" id="input-country" class="form-control">
                                                <option value=""><?php echo $text_select; ?></option>
                                                <?php foreach ($countries as $country) { ?>
                                                <?php if ($country['country_id'] == $country_id) { ?>
                                                <option value="<?php echo $country['country_id']; ?>" selected="selected"> <?php echo $country['name']; ?> </option>
                                                <?php } else { ?>
                                                <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                                                <?php } ?>
                                                <?php } ?>
                                            </select>
                                            <?php if ($error_country) { ?>
                                            <div class="text-danger"><?php echo $error_country; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-zone"><?php echo $entry_zone; ?></label>
                                        <div class="col-sm-10">
                                            <select name="zone_id" id="input-zone" class="form-control">
                                            </select>
                                            <?php if ($error_zone) { ?>
                                            <div class="text-danger"><?php echo $error_zone; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <!-- Organizations Details -->
                            <?php if ($affiliate_id) { ?>
                            <div class="tab-pane" id="tab-organization">
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="position_in_organization"><?php echo $entry_position_organization ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="position_in_organization" value="<?php echo $position_in_organization ?>" id="position_in_organization" class="form-control" placeholder="<?php echo $entry_position_organization ?>"/>
                                        <?php if ($error_position_in_organization) { ?>
                                        <div class="text-danger"><?php echo $error_position_in_organization ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="organization_phone_number"><?php echo $entry_organization_phone_number ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="organization_phone_number" value="<?php echo $organization_phone_number; ?>" id="email" class="form-control" placeholder="<?php echo $entry_organization_phone_number; ?>"/>
                                        <?php if ($error_organization_phone_number) { ?>
                                        <div class="text-danger"><?php echo $error_organization_phone_number ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="extension"><?php echo $entry_extension ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="extension" value="<?php echo $extension; ?>" id="extension" class="form-control" placeholder="<?php echo $entry_extension; ?>"/>
                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="country "><?php echo $entry_branded_website ?></label>
                                    <div class="col-sm-10" style="display: inline-block;">
                                        <textarea  style="width:100%;height:75px;" name="branded_website"><?php echo $branded_website ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="website_nickname"><?php echo $entry_organization_hashtags ?></label>
                                    <div class="col-sm-10"><input type="text" name="organization_hashtags" value="<?php echo $organization_hashtags ?>" id="organization_hashtags" class="form-control" placeholder="<?php echo $entry_organization_hashtags ?>"/>
                                </div>
                            </div>
                            <div class="form-group group-left">
                                <label class="col-sm-2 control-label" for="organization_facebook_link ">
                                <?php echo $entry_organization_facebook_link ?></label>
                                <div class="col-sm-10"><input type="text" name="organization_facebook_link" value="<?php echo $organization_facebook_link ?>" id="organization_website" class="form-control" placeholder="<?php echo $entry_organization_facebook_link ?>"/>
                            </div>
                        </div>
                        <div class="form-group group-right">
                            <label class="col-sm-2 control-label" for="organization_instagram"><?php echo $entry_organization_instagram ?></label>
                            <div class="col-sm-10"><input type="text" name="organization_instagram" value="<?php echo $organization_instagram ?>" id="organization_instagram" class="form-control" placeholder="<?php echo $entry_organization_instagram ?>"/>
                        </div>
                    </div>
                    <div class="form-group group-left">
                        <label class="col-sm-2 control-label" for="organization_twitter"><?php echo $entry_organization_twitter ?></label>
                        <div class="col-sm-10"><input type="text" name="organization_twitter" value="<?php echo $organization_twitter ?>" id="organization_twitter" class="form-control" placeholder="<?php echo $entry_organization_twitter ?>"/>
                    </div>
                </div>
                <div class="form-group group-right">
                    <label class="col-sm-2 control-label" for="organization_linkedIn_link "><?php echo $entry_organization_linkedIn_link ?></label>
                    <div class="col-sm-10"><input type="text" name="organization_linkedIn_link" value="<?php echo $organization_linkedIn_link ?>" id="organization_linkedIn_link" class="form-control" placeholder="<?php echo $entry_organization_linkedIn_link ?>"/>
                </div>
            </div>
            <div class="form-group permits-box oc3">
                <label class="col-sm-2 control-label" for=""><?php echo $entry_agreement_heading ?></label><br>
                <div class="col-sm-10">
                    <div class="col-md-3">
                        <label><input type="checkbox" <?php echo $checkBox1 ?> name="agreement[]" value="on the main, or other pages of the ShopPal website that display Memberinformation"/>
                    <?php echo $entry_agreement_checkbox1 ?></label>
                </div>
                <div class="col-md-3">
                    <label> <input type="checkbox" <?php echo $checkBox2 ?> name="agreement[]" value="on Social Media and other Internet Marketing platforms, and/or"/>
                <?php echo $entry_agreement_checkbox2 ?></label>
            </div>
            <div class="col-md-4">
                <label>
                    <input type="checkbox" <?php echo $checkBox3 ?> name="agreement[]" value="on Print Media and or Radio/Television Media."/>
                <?php echo $entry_agreement_checkbox3 ?></label>
            </div>
        </div>
    </div>
    <!--In English -->
    <div class="form-group">
        <label class="col-sm-2 control-label" for="organization_detailed" for="organization_mission_statement"><?php echo $entry_organization_detailed ?>
        </label>
        <div class="col-sm-10">
            <textarea style="width:100%" id="organization_detailed" name="organization_detailed"><?php echo $organization_detailed ?></textarea>
        </div>
    </div>
    <!--In English -->
    <!--In French -->
    <div class="form-group">
        <label class="col-sm-2 control-label"  for="organization_detailed_french"><?php echo $entry_organization_detailed_french ?>
        </label>
        <div class="col-sm-10">
            <textarea style="width:100%" id="organization_detailed_french" name="organization_detailed_french"><?php echo $organization_detailed_french ?></textarea>
        </div>
    </div>
    <!--In French -->
    <h3><?php echo $entry_hear_about_us ?></h3>
    <div class="form-group social-link">
        <label class="col-sm-2 control-label" for="organization_linkedIn_link"></label>
        <div  class="col-sm-10">
            <div class="radio" style="width: 100%">
                <label>
                    <?php if(in_array("Facebook",$hear_about_us)){?>
                    <input type="checkbox" checked name="hear_about_us[]" value="Facebook"/>
                    <?php } else { ?>
                    <input type="checkbox" <?php echo $checked_facebook ?> name="hear_about_us[]" value="Facebook"/>
                    <?php } ?>
                    Facebook <br/>
                </label>
            </div>
            <div class="radio" style="width: 100%">
                <label>
                    <?php if(in_array("LinkedIn",$hear_about_us)){?>
                    <input type="checkbox" checked name="hear_about_us[]" value="LinkedIn"/>
                    <?php } else { ?>
                    <input type="checkbox" <?php echo $checked_linkedIn ?> name="hear_about_us[]" value="LinkedIn"/>
                    <?php } ?>
                    LinkedIn
                </label>
            </div>
            <div class="radio" style="width: 100%">
                <label>
                    <?php if(in_array("ShopPal Website",$hear_about_us)){?>
                    <input type="checkbox" checked name="hear_about_us[]" value="ShopPal Website"/>
                    <?php } else { ?>
                    <input type="checkbox" <?php echo $checked_shoppal ?> name="hear_about_us[]" value="ShopPal Website"/>
                    <?php } ?>
                    <?php echo $entry_shoppal_website ?>
                </label>
            </div>
            <div class="radio" style="width: 100%">
                <label>
                    <?php if(in_array("Local Store",$hear_about_us)){?>
                    <input type="checkbox" checked name="hear_about_us[]" value="Local Store"/>
                    <?php } else { ?>
                    <input type="checkbox"  <?php echo $checked_local_store ?> name="hear_about_us[]" value="Local Store"/>
                    <?php } ?>
                    <?php echo $entry_local_store ?>
                </label>
            </div>
            <div class="radio" style="width: 100%">
                <label>
                    <?php if(in_array("Local Event",$hear_about_us)){?>
                    <input type="checkbox" checked name="hear_about_us[]" value="Local Event"/>
                    <?php } else { ?>
                    <input type="checkbox" <?php echo $checked_loal_event ?> name="hear_about_us[]" value="Local Event"/>
                    <?php } ?>
                    Local Event
                </label>
            </div>
            <div class="radio" style="width: 100%">
                <label>
                    <?php if(in_array("Flyer",$hear_about_us)){?>
                    <input type="checkbox" checked name="hear_about_us[]" value="Flyer"/>
                    <?php } else { ?>
                    <input type="checkbox" <?php echo $checked_flyer ?> name="hear_about_us[]" value="Flyer"/>
                    <?php } ?>
                    <?php echo $entry_local_flyer ?>
                    
                </label>
            </div>
            <div class="radio" style="width: 100%">
                <label>
                    <?php if(in_array("Other Fundraising Organization",$hear_about_us)){?>
                    <input  type="checkbox" checked name="hear_about_us[]" value="Other Fundraising Organization"/>
                    <?php } else { ?>
                    <input  type="checkbox" <?php echo $checked_other_fundraising_organization ?> name="hear_about_us[]" value="Other Fundraising Organization"/>
                    <?php } ?>
                <?php echo $entry_other_fundraising_organization ?></label>
            </div>
            <!-- <div class="radio" style="float: left;">
                <label class="other_hear">
                    <input type="checkbox" class="check-box-lable"  <?php echo $hear_about_us_checked; ?> name="hear_about_us_checked" value="other"/>
                    <input type="text" name="other_hear_about_us" value="<?php echo $other_hear_about_us ?>" id="other_question" class="form-control" placeholder="<?php echo $entry_other_hear_about_us ?>" style="display:inline-block;" />
                </label>
            </div> -->
        </div>
    </div>
    
    <div class="form-group">
        
        <div class="col-sm-2 control-label">
            <?php echo $tracking_code_referral ?>
        </div>
        <div  class="col-sm-10">
            <div class="radio"  style="display:block;">
                <label>
                    
                    <input type="text" name="other_hear_about_us" value="<?php echo $other_hear_about_us ?>" id="other_question" class="form-control" placeholder="<?php echo $entry_other_hear_about_us ?>" style="display:inline-block;" />
                </label>
            </div>
        </div>
        
        
    </div>
    
    
    
    <div class="form-group">
        
        <div class="col-sm-2 control-label">
            <input type="radio" name="other_details" value="1" <?php echo $other_details ?> />&nbsp;<?php echo $other_details_text ?>
        </div>
        <div  class="col-sm-10">
            <div class="radio"  style="display:block;">
                <label>
                    <!--   <input type="text"  name="other_details_input" class="form-control"  value="" /> -->
                    <textarea class="form-control" rows="2" cols="94" name="other_details_input" placeholder="<?php echo $other_details_text ?>" style="margin: 0px; width: 943px; height: 54px;"><?php echo $other_details_input ?></textarea>
                </label>
            </div>
        </div>
        
        
    </div>
    <div class="custom_fields">
        <!-- new fields -->
        <div class="form-group">
            <?php
            
            
            if(!empty($member_image1))
            {
            $member_image1 = $member_image1;
            $member_image_old = $member_image1;
            } else {
            $member_image1 = 'no_image.png';
            $member_image_old = '';
            }
            ?>
            <input type ="hidden" name ="member_image_old" value="<?php echo $member_image_old;?>">
            <label class="col-sm-2 control-label" for="input-member_image1"><?php echo $entry_member_image1; ?></label>
            <div class="col-sm-10">
                <input type="file" accept="image/*" name="member_image1" placeholder="<?php echo $entry_member_image1; ?>" id="member_image1" />
                <img id="member_image" src="../image/data/<?php echo  $member_image1;?>" alt="" style="width: 150px;height:150px; margin-top: 5px;">
            </div>
        </div>
    </div>
    <!-- member image 2 -->
    <div class="custom_fields">
        <!-- new fields -->
        <div class="form-group">
            <?php
            
            if(!empty($member_image2))
            {
            $member_image2 = $member_image2;
            $member_image_old2 = $member_image2;
            } else {
            $member_image2 = 'no_image.png';
            $member_image_old = '';
            }
            ?>
            <input type ="hidden" name ="member_image_old2" value="<?php echo $member_image_old2;?>">
            <label class="col-sm-2 control-label" for="input-member_image2"><?php echo $entry_member_image2; ?></label>
            <div class="col-sm-10">
                <input type="file" accept="image/*" name="member_image2" placeholder="<?php echo $entry_member_image2; ?>" id="member_image2" />
                <img id="blah2" src="../image/data/<?php echo  $member_image2;?>" alt="" style="width: 150px;height:150px; margin-top: 5px;">
            </div>
        </div>
    </div>
    <div class="custom_fields">
        <div class="form-group">
            <label class="col-sm-2 control-label" for="input-firstname"><?php echo $entry_member_video; ?></label>
            <div class="col-sm-10">
                <input type="text" name="member_video" value="<?php echo $member_video; ?>" placeholder="<?php echo $member_video; ?>" id="input-member_video" class="form-control" />
            </div>
        </div>
    </div>
</div>
<?php
}?>
<!-- Organizations Details-->
<div class="tab-pane" id="tab-payment">
    <div class="form-group">
        <label class="col-sm-2 control-label" for="input-commission"><span data-toggle="tooltip" title="<?php echo $help_commission; ?>"><?php echo $entry_commission; ?></span></label>
        <div class="col-sm-10">
            <input type="text" name="commission" value="<?php echo $commission; ?>" placeholder="<?php echo $entry_commission; ?>" id="input-commission" class="form-control" />
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="input-tax"><?php echo $entry_tax; ?></label>
        <div class="col-sm-10">
            <input type="text" name="tax" value="<?php echo $tax; ?>" placeholder="<?php echo $entry_tax; ?>" id="input-tax" class="form-control" />
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label"><?php echo $entry_payment; ?></label>
        <div class="col-sm-10">
            <div class="radio">
                <label>
                    <?php if ($payment == 'cheque') { ?>
                    <input type="radio" name="payment" value="cheque" checked="checked" />
                    <?php } else { ?>
                    <input type="radio" name="payment" value="cheque" />
                    <?php } ?>
                <?php echo $text_cheque; ?></label>
            </div>
            <div class="radio">
                <label>
                    <?php if ($payment == 'paypal') { ?>
                    <input type="radio" name="payment" value="paypal" checked="checked" />
                    <?php } else { ?>
                    <input type="radio" name="payment" value="paypal" />
                    <?php } ?>
                <?php echo $text_paypal; ?></label>
            </div>
            <div class="radio">
                <label>
                    <?php if ($payment == 'bank') { ?>
                    <input type="radio" name="payment" value="bank" checked="checked" />
                    <?php } else { ?>
                    <input type="radio" name="payment" value="bank" />
                    <?php } ?>
                <?php echo $text_bank; ?></label>
            </div>
        </div>
    </div>
    <div id="payment-cheque" class="payment">
        <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-cheque"><?php echo $entry_cheque; ?></label>
            <div class="col-sm-10">
                <input type="text" name="cheque" value="<?php echo $cheque; ?>" placeholder="<?php echo $entry_cheque; ?>" id="input-cheque" class="form-control" />
                <?php if ($error_cheque) { ?>
                <div class="text-danger"><?php echo $error_cheque; ?></div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div id="payment-paypal" class="payment">
        <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-paypal"><?php echo $entry_paypal; ?></label>
            <div class="col-sm-10">
                <input type="text" name="paypal" value="<?php echo $paypal; ?>" placeholder="<?php echo $entry_paypal; ?>" id="input-paypal" class="form-control" />
                <?php if ($error_paypal) { ?>
                <div class="text-danger"><?php echo $error_paypal; ?></div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div id="payment-bank" class="payment">
        <div class="form-group">
            <label class="col-sm-2 control-label" for="input-bank-name"><?php echo $entry_bank_name; ?></label>
            <div class="col-sm-10">
                <input type="text" name="bank_name" value="<?php echo $bank_name; ?>" placeholder="<?php echo $entry_bank_name; ?>" id="input-bank-name" class="form-control" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="input-bank-branch-number"><?php echo $entry_bank_branch_number; ?></label>
            <div class="col-sm-10">
                <input type="text" name="bank_branch_number" value="<?php echo $bank_branch_number; ?>" placeholder="<?php echo $entry_bank_branch_number; ?>" id="input-bank-branch-number" class="form-control" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="input-bank-swift-code"><?php echo $entry_bank_swift_code; ?></label>
            <div class="col-sm-10">
                <input type="text" name="bank_swift_code" value="<?php echo $bank_swift_code; ?>" placeholder="<?php echo $entry_bank_swift_code; ?>" id="input-bank-swift-code" class="form-control" />
            </div>
        </div>
        <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-bank-account-name"><?php echo $entry_bank_account_name; ?></label>
            <div class="col-sm-10">
                <input type="text" name="bank_account_name" value="<?php echo $bank_account_name; ?>" placeholder="<?php echo $entry_bank_account_name; ?>" id="input-bank-account-name" class="form-control" />
                <?php if ($error_bank_account_name) { ?>
                <div class="text-danger"><?php echo $error_bank_account_name; ?></div>
                <?php } ?>
            </div>
        </div>
        <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-bank-account-number"><?php echo $entry_bank_account_number; ?></label>
            <div class="col-sm-10">
                <input type="text" name="bank_account_number" value="<?php echo $bank_account_number; ?>" placeholder="<?php echo $entry_bank_account_number; ?>" id="input-bank-account-number" class="form-control" />
                <?php if ($error_bank_account_number) { ?>
                <div class="text-danger"><?php echo $error_bank_account_number; ?></div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php if ($affiliate_id) { ?>
<div class="tab-pane" id="tab-transaction">
    <div id="transaction"></div>
    <br />
    <div class="form-group">
        <label class="col-sm-2 control-label" for="input-description"><?php echo $entry_description; ?></label>
        <div class="col-sm-10">
            <input type="text" name="description" value="" placeholder="<?php echo $entry_description; ?>" id="input-description" class="form-control" />
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="input-amount"><?php echo $entry_amount; ?></label>
        <div class="col-sm-10">
            <input type="text" name="amount" value="" placeholder="<?php echo $entry_amount; ?>" id="input-amount" class="form-control" />
        </div>
    </div>
    <div class="text-right">
        <button type="button" id="button-transaction" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i> <?php echo $button_transaction_add; ?></button>
    </div>
</div>
<?php } ?>
</div>
</form>
</div>
</div>
</div>
<script type="text/javascript"><!--
function select_image(img_id,other_image1,other_image2,other_image3,other_image4,other_image5,other_image6,other_image7,other_image8,other_image9,other_image10,other_image11,other_image12,other_image13,other_image14,other_image15,other_image16,other_image17,other_image18,other_image19,image_name)
{
$('#'+img_id).css({"border-color": "#dd0017","border-width":"4px","border-style":"solid"});
$('#'+other_image1).css({"border-color": "","border-width":"","border-style":""});
$('#'+other_image2).css({"border-color": "","border-width":"","border-style":""});
$('#'+other_image3).css({"border-color": "","border-width":"","border-style":""});
$('#'+other_image4).css({"border-color": "","border-width":"","border-style":""});
$('#'+other_image5).css({"border-color": "","border-width":"","border-style":""});
$('#'+other_image6).css({"border-color": "","border-width":"","border-style":""});
$('#'+other_image7).css({"border-color": "","border-width":"","border-style":""});
$('#'+other_image8).css({"border-color": "","border-width":"","border-style":""});
$('#'+other_image9).css({"border-color": "","border-width":"","border-style":""});
$('#'+other_image10).css({"border-color": "","border-width":"","border-style":""});
$('#'+other_image11).css({"border-color": "","border-width":"","border-style":""});
$('#'+other_image12).css({"border-color": "","border-width":"","border-style":""});
$('#'+other_image13).css({"border-color": "","border-width":"","border-style":""});
$('#'+other_image14).css({"border-color": "","border-width":"","border-style":""});
$('#'+other_image15).css({"border-color": "","border-width":"","border-style":""});
$('#'+other_image16).css({"border-color": "","border-width":"","border-style":""});
$('#'+other_image17).css({"border-color": "","border-width":"","border-style":""});
$('#'+other_image18).css({"border-color": "","border-width":"","border-style":""});
$('#'+other_image19).css({"border-color": "","border-width":"","border-style":""});
//$('#'+other_image20).css({"border-color": "","border-width":"","border-style":""});
$('#slider_image').val(image_name);
}
function readURL(input) {
if (input.files && input.files[0]) {
var reader = new FileReader();
reader.onload = function(e) {
$('#blah').attr('src', e.target.result);
}
reader.readAsDataURL(input.files[0]);
}
}
$("#custom_logo").change(function() {
readURL(this);
});
function readMemURL(input) {
if (input.files && input.files[0]) {
var reader = new FileReader();
reader.onload = function(e) {
$('#member_image').attr('src', e.target.result);
}
reader.readAsDataURL(input.files[0]);
}
}
$("#member_image1").change(function() {
readMemURL(this);
});
function readMemImgURL(input) {
if (input.files && input.files[0]) {
var reader = new FileReader();
reader.onload = function(e) {
$('#blah2').attr('src', e.target.result);
}
reader.readAsDataURL(input.files[0]);
}
}
$("#member_image2").change(function() {
readMemImgURL(this);
});
$('select[name=\'country_id\']').on('change', function() {
    $.ajax({
url: 'index.php?route=localisation/country/country&token=<?php echo $token; ?>&country_id=' + this.value,
dataType: 'json',
beforeSend: function() {
$('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
},
complete: function() {
$('.fa-spin').remove();
},
success: function(json) {
if (json['postcode_required'] == '1') {
$('input[name=\'postcode\']').parent().parent().addClass('required');
} else {
$('input[name=\'postcode\']').parent().parent().removeClass('required');
}
html = '<option value=""><?php echo $text_select; ?></option>';
if (json['zone'] && json['zone'] != '') {
for (i = 0; i < json['zone'].length; i++) {
html += '<option value="' + json['zone'][i]['zone_id'] + '"';

if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
html += ' selected="selected"';
}
html += '>' + json['zone'][i]['name'] + '</option>';
}
} else {
html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
}
$('select[name=\'zone_id\']').html(html);
},
error: function(xhr, ajaxOptions, thrownError) {
alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
}
});
});
$('select[name=\'country_id\']').trigger('change');
//--></script>
<script type="text/javascript"><!--
$('input[name=\'payment\']').on('change', function() {
    $('.payment').hide();
    $('#payment-' + this.value).show();
});
$('input[name=\'payment\']:checked').trigger('change');
//--></script>
<script type="text/javascript"><!--
$('#transaction').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();
    $('#transaction').load(this.href);
});
$('#transaction').load('index.php?route=marketing/affiliate/transaction&token=<?php echo $token; ?>&affiliate_id=<?php echo $affiliate_id; ?>');
$('#button-transaction').on('click', function() {
$.ajax({
url: 'index.php?route=marketing/affiliate/addtransaction&token=<?php echo $token; ?>&affiliate_id=<?php echo $affiliate_id; ?>',
type: 'post',
dataType: 'json',
data: 'description=' + encodeURIComponent($('#tab-transaction input[name=\'description\']').val()) + '&amount=' + encodeURIComponent($('#tab-transaction input[name=\'amount\']').val()),
beforeSend: function() {
$('#button-transaction').button('loading');
},
complete: function() {
$('#button-transaction').button('reset');
},
success: function(json) {
$('.alert').remove();

if (json['error']) {
$('#tab-transaction').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div></div>');
}
if (json['success']) {
$('#tab-transaction').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div></div>');
$('#transaction').load('index.php?route=marketing/affiliate/transaction&token=<?php echo $token; ?>&affiliate_id=<?php echo $affiliate_id; ?>');

$('#tab-transaction input[name=\'amount\']').val('');
$('#tab-transaction input[name=\'description\']').val('');
}
}
});
});
function countChar(val) {
var len = val.value.length;
if (len > 211) {
val.value = val.value.substring(0, 211);
} else {
$('#charNum').css("display", "inline-block");
$('#charNum').text(211 - len);
}
}
//misssion french
function missioncountChar(val) {
var len = val.value.length;
if (len > 211) {
val.value = val.value.substring(0, 211);
} else {
$('#missioncharNum').css("display", "inline-block");
$('#missioncharNum').text(211 - len);
}
}
//--></script>
<script type="text/javascript">
$(document).ready(function() {
var other_details = $("input[name='other_details']:checked").val()
if(other_details == 1) {
$('#other_details').show();
} else {
$('#other_details').hide();
}
});
var allRadios = document.getElementsByName('other_details');
var booRadio;
var x = 0;
for(x = 0; x < allRadios.length; x++) {
allRadios[x].onclick = function() {
if(booRadio == this) {
this.checked = false;
booRadio = null;
$('#other_details').hide();
} else {
booRadio = this;
$('#other_details').show();
}
};
}
</script>
<style>
.other_hear .check-box-lable{margin-right:15px; width:15px;}
.other_hear input{ display:inline-block; width:auto;}
</style>
</div>
<?php echo $footer; ?>
