<?php if ($birdsview_table_status == 1) { ?>    <!--        Checking whether plugin enabled or not      -->
    
    <!--Including css for popup and table-->
<link rel="stylesheet" type="text/css" href="view/stylesheet/birdsview.css" />  

<div class="statistic" style="width:100%;">
        <div class="dashboard-heading"><?php echo $birdsview ?></div>
        <div class="dashboard-content" style="min-width:auto;min-height:0;">
           
            <table class="list">
            <!-- Headings for table -->
                <thead>
                    <tr>
                        <?php
                        if ($today_status == 1) {
                            echo '<td class="left">';
                            echo $today . '</td>';
                        }
                        ?>

                        <?php
                        if ($yesterday_status == 1) {
                            echo '<td class="left">';
                            echo $yesterday . '</td>';
                        }
                        ?>

                        <?php
                        if ($lastweek_status == 1) {
                            echo '<td class="left">';
                            echo $last_week . '</td>';
                        }
                        ?>

                        <?php
                        if ($lastmonth_status == 1) {
                            echo '
                    <td class="left">';
                            echo $last_month . '</td>';
                        }
                        ?>

                        <?php
                        if ($lastyear_status == 1) {
                            echo '
                    <td class="left">';
                            echo $last_year . '</td>';
                        }
                        ?>
                    </tr>
                </thead>
                <tbody>



                    

<!-- Todays column starts here-->

                    <?php
                    if ($today_status == 1) {
                        echo '<tr>
                    <td class="left">';
                        if ($order_status == 1) {
                            echo $text_total_order;
                            echo "<div  class='popper'   data-popbox='pop1'>" . $torder[0];
                            if ($torder[4] < 0)
                                echo "<div class='diff'><font color='red'> &#x25BC;" . -$torder[4] . '</font></div></div>';
                            else
                                 echo '<div class="diff"><font color="green">&#x25B2;' . $torder[4] . '</font></div></div>';
                            echo "<hr>";
                        }

                        if ($discount_status == 1) {
                            echo $text_total_discount;
                            echo "<div class='popper' data-popbox='pop2'>" . -$tdiscount[5] = $tdiscount[0] + $tdiscount[1];
                            if ($tdiscount[4] > 0)
                                 echo '<div class="diff"><font color="red"> &#x25BC;' . $tdiscount[4] . '</font></div></div>';
                            else
                                 echo '<div class="diff"><font color="green">&#x25B2;' . -$tdiscount[4] . '</font></div></div>';
                            echo "<hr>";
                        }

                        if ($product_status == 1) {
                            echo $text_total_product;
                            echo "<div class='popper' data-popbox='pop3'>" . $tproduct[0];
                            if ($tproduct[4] < 0)
                                 echo '<div class="diff"><font color="red"> &#x25BC;' . -$tproduct[4] . '</font></div></div>';
                            else
                                 echo '<div class="diff"><font color="green">&#x25B2;' . $tproduct[4] . '</font></div></div>';
                            echo "<hr>";
                        }

                        if ($revenue_status == 1) {
                            echo $text_total_revenue;
                            echo "<div class='popper' data-popbox='pop4'>" . round($trevenue[0], 3);
                            if ($trevenue[4] < 0)
                                 echo '<div class="diff"><font color="red"> &#x25BC;' . -$trevenue[4] . '</font></div></div>';
                            else
                                 echo '<div class="diff"><font color="green">&#x25B2;' . $trevenue[4] . '</font></div></div>';
                            echo "<hr>";
                        }

                        if ($missorder_status == 1) {
                            echo $text_total_miss_order;
                            echo "<div class='popper' data-popbox='pop5'>" . $tmissorder[0];
                            if ($tmissorder[4] < 0)
                                 echo '<div class="diff"><font color="red"> &#x25BC;' . -$tmissorder[4] . '</font></div></div>';
                            else
                                 echo '<div class="diff"><font color="green">&#x25B2;' . $tmissorder[4] . '</font></div></div>';
                            echo "<hr> </td>";
                        }
                    }
                    ?>

<!-- Todays column end -->




<!-- Yesterday's column starts here-->

                    <?php
                    if ($yesterday_status == 1) {
                        echo '
                    <td class="left">';
                        if ($order_status == 1) {
                            echo $text_total_order;
                            echo "<div class='popper' data-popbox='pop6'>" . $yorder[0];
                            if ($yorder[4] < 0)
                                 echo '<div class="diff"><font color="red"> &#x25BC;' . -$yorder[4] . '</font></div></div>';
                            else
                                 echo '<div class="diff"><font color="green">&#x25B2;' . $yorder[4] . '</font></div></div>';
                            echo "<hr>";
                        }

                        if ($discount_status == 1) {
                            echo $text_total_discount;
                            echo "<div class='popper' data-popbox='pop7'>" . -$ydiscount[5] = $ydiscount[0] + $ydiscount[1];
                            if ($ydiscount[4] > 0)
                                 echo '<div class="diff"><font color="red"> &#x25BC;' . $ydiscount[4] . '</font></div></div>';
                            else
                                 echo '<div class="diff"><font color="green">&#x25B2;' . -$ydiscount[4] . '</font></div></div>';
                            echo "<hr>";
                        }

                        if ($product_status == 1) {
                            echo $text_total_product;
                            echo "<div class='popper' data-popbox='pop8'>" . $yproduct[0];
                            if ($yproduct[4] < 0)
                                 echo '<div class="diff"><font color="red"> &#x25BC;' . -$yproduct[4] . '</font></div></div>';
                            else
                                 echo '<div class="diff"><font color="green">&#x25B2;' . $yproduct[4] . '</font></div></div>';
                            echo "<hr>";
                        }

                        if ($revenue_status == 1) {
                            echo $text_total_revenue;
                            echo "<div class='popper' data-popbox='pop9'>" . round($yrevenue[0], 3);
                            if ($yrevenue[4] < 0)
                                 echo '<div class="diff"><font color="red"> &#x25BC;' . -$yrevenue[4] . '</font></div></div>';
                            else
                                 echo '<div class="diff"><font color="green">&#x25B2;' . $yrevenue[4] . '</font></div></div>';
                            echo "<hr>";
                        }

                        if ($missorder_status == 1) {
                            echo $text_total_miss_order;
                            echo "<div class='popper' data-popbox='pop10'>" . $ymissorder[0];
                            if ($ymissorder[4] < 0)
                                 echo '<div class="diff"><font color="red"> &#x25BC;' . -$ymissorder[4] . '</font></div></div>';
                            else
                                 echo '<div class="diff"><font color="green">&#x25B2;' . $ymissorder[4] . '</font></div></div>';
                            echo "<hr> </td>";
                        }
                    }
                    ?>

<!-- Yesterday's column end -->




<!-- Week's column starts here-->
                    <?php
                    if ($lastweek_status == 1) {
                        echo '
                    <td class="left">';
                        if ($order_status == 1) {
                            echo $text_total_order;
                            echo "<div class='popper' data-popbox='pop16'>" . $worder[0];
                            if ($worder[4] < 0)
                                 echo '<div class="diff"><font color="red"> &#x25BC;' . -$worder[4] . '</font></div></div>';
                            else
                                 echo '<div class="diff"><font color="green">&#x25B2;' . $worder[4] . '</font></div></div>';
                            echo "<hr>";
                        }

                        if ($discount_status == 1) {
                            echo $text_total_discount;
                            echo "<div class='popper' data-popbox='pop17'>" . -$wdiscount[5] = $wdiscount[0] + $wdiscount[1];
                            if ($wdiscount[4] > 0)
                                 echo '<div class="diff"><font color="red"> &#x25BC;' . $wdiscount[4] . '</font></div></div>';
                            else
                                 echo '<div class="diff"><font color="green">&#x25B2;' . -$wdiscount[4] . '</font></div></div>';
                            echo "<hr>";
                        }

                        if ($product_status == 1) {
                            echo $text_total_product;
                            echo "<div class='popper' data-popbox='pop18'>" . $wproduct[0];
                            if ($wproduct[4] < 0)
                                 echo '<div class="diff"><font color="red"> &#x25BC;' . -$wproduct[4] . '</font></div></div>';
                            else
                                 echo '<div class="diff"><font color="green">&#x25B2;' . $wproduct[4] . '</font></div></div>';
                            echo "<hr>";
                        }

                        if ($revenue_status == 1) {
                            echo $text_total_revenue;
                            echo "<div class='popper' data-popbox='pop19'>" . round($wrevenue[0], 3);
                            if ($wrevenue[4] < 0)
                                 echo '<div class="diff"><font color="red"> &#x25BC;' . -$wrevenue[4] . '</font></div></div>';
                            else
                                 echo '<div class="diff"><font color="green">&#x25B2;' . $wrevenue[4] . '</font></div></div>';
                            echo "<hr>";
                        }

                        if ($missorder_status == 1) {
                            echo $text_total_miss_order;
                            echo "<div class='popper' data-popbox='pop20'>" . $wmissorder[0];
                            if ($wmissorder[4] < 0)
                                 echo '<div class="diff" ><font color="red"> &#x25BC;' . -$wmissorder[4] . '</font></div></div>';
                            else
                                 echo '<div class="diff"><font color="green">&#x25B2;' . $wmissorder[4] . '</font></div></div>';
                            echo "<hr></td>";
                        }
                    }
                    ?>
<!-- Week's column end -->





<!-- Month's column starts here -->

               <?php
                    if ($lastmonth_status == 1) {
                        echo '
                    <td class="left">';
                        if ($order_status == 1) {
                            echo $text_total_order;
                            echo "<div class='popper' data-popbox='pop11'>" . $morder[0];
                            if ($morder[4] < 0)
                                 echo '<div class="diff"><font color="red"> &#x25BC;' . -$morder[4] . '</font></div></div>';
                            else
                                 echo '<div class="diff"><font color="green">&#x25B2;' . $morder[4] . '</font></div></div>';
                            echo "<hr>";
                        }

                        if ($discount_status == 1) {
                            echo $text_total_discount;
                            echo "<div class='popper' data-popbox='pop12'>" . -$mdiscount[5] = $mdiscount[0] + $mdiscount[1];
                            if ($mdiscount[4] > 0)
                                 echo '<div class="diff"><font color="red"> &#x25BC;' . $mdiscount[4] . '</font></div></div>';
                            else
                                 echo '<div class="diff"><font color="green">&#x25B2;' . -$mdiscount[4] . '</font></div></div>';
                            echo "<hr>";
                        }

                        if ($product_status == 1) {
                            echo $text_total_product;
                            echo "<div class='popper' data-popbox='pop13'>" . $mproduct[0];
                            if ($mproduct[4] < 0)
                                 echo '<div class="diff"><font color="red"> &#x25BC;' . -$mproduct[4] . '</font></div></div>';
                            else
                                 echo '<div class="diff"><font color="green">&#x25B2;' . $mproduct[4] . '</font></div></div>';
                            echo "<hr>";
                        }

                        if ($revenue_status == 1) {
                            echo $text_total_revenue;
                            echo "<div class='popper' data-popbox='pop14'>" . round($mrevenue[0], 3);
                            if ($mrevenue[4] < 0)
                                 echo '<div class="diff"><font color="red"> &#x25BC;' . -$mrevenue[4] . '</font></div></div>';
                            else
                                 echo '<div class="diff"><font color="green">&#x25B2;' . $mrevenue[4] . '</font></div></div>';
                            echo "<hr>";
                        }

                        if ($missorder_status == 1) {
                            echo $text_total_miss_order;
                            echo "<div class='popper' data-popbox='pop15'>" . $mmissorder[0];
                            if ($mmissorder[4] < 0)
                                 echo '<div class="diff"><font color="red"> &#x25BC;' . -$mmissorder[4] . '</font></div></div>';
                            else
                                 echo '<div class="diff"><font color="green">&#x25B2;' . $mmissorder[4] . '</font></div></div>';
                            echo "<hr> </td>";
                        }
                    }
                    ?>
<!-- Month's column end -->




<!-- Year's column starts here-->

    <?php
    if ($lastyear_status == 1) {
        echo '
                    <td class="left">';
        if ($order_status == 1) {
            echo $text_total_order;
            echo "<div class='popper' data-popbox='pop21'>" . $yyorder[0];
            if ($yyorder[4] < 0)
                 echo '<div class="diff"><font color="red"> &#x25BC;' . -$yyorder[4] . '</font></div></div>';
            else
                 echo '<div class="diff"><font color="green">&#x25B2;' . $yyorder[4] . '</font></div></div>';
            echo "<hr>";
        }

        if ($discount_status == 1) {
            echo $text_total_discount;
            echo "<div class='popper' data-popbox='pop22'>" . -$yydiscount[5] = $yydiscount[0] + $yydiscount[1];
            if ($yydiscount[4] > 0)
                 echo '<div class="diff"><font color="red"> &#x25BC;' . $yydiscount[4] . '</font></div></div>';
            else
                 echo '<div class="diff"><font color="green">&#x25B2;' . -$yydiscount[4] . '</font></div></div>';
            echo "<hr>";
        }

        if ($product_status == 1) {
            echo $text_total_product;
            echo "<div class='popper' data-popbox='pop23'>" . $yyproduct[0];
            if ($yyproduct[4] < 0)
                 echo '<div class="diff"><font color="red"> &#x25BC;' . -$yyproduct[4] . '</font></div></div>';
            else
                 echo '<div class="diff"><font color="green">&#x25B2;' . $yyproduct[4] . '</font></div></div>';
            echo "<hr>";
        }

        if ($revenue_status == 1) {
            echo $text_total_revenue;
            echo "<div class='popper' data-popbox='pop24'>" . round($yyrevenue[0], 3);
            if ($yyrevenue[4] < 0)
                 echo '<div class="diff"><font color="red"> &#x25BC;' . -$yyrevenue[4] . '</font></div></div>';
            else
                 echo '<div class="diff"><font color="green">&#x25B2;' . $yyrevenue[4] . '</font></div></div>';
            echo "<hr>";
        }

        if ($missorder_status == 1) {
            echo $text_total_miss_order;
            echo "<div class='popper' data-popbox='pop25'>" . $yymissorder[0];
            if ($yymissorder[4] < 0)
                 echo '<div class="diff"><font color="red"> &#x25BC;' . -$yymissorder[4] . '</font></div></div>';
            else
                 echo '<div class="diff"><font color="green">&#x25B2;' . $yymissorder[4] . '</font></div></div>';
            echo "<hr> </td>";
        }
    }
    ?>

<!-- Year's column end -->














<!-- popup for Today-->


        <div id="pop1" class="popbox1">
        <br> <?php echo $today.' '.$text_total_order.': <br>' . $torder[0] . '<br><br><br>' ?>
        <?php echo $yesterday.' '.$text_total_order.' <br>' . $torder[1] . '<br>' ?>
        </div>


        <div id="pop2" class="popbox1">
        <br>  <?php echo $today.' '.$text_total_discount.'<br>';
        echo -($tdiscount[0] + $tdiscount[1]) . '<br><br><br>'
        ?>
       <?php echo $yesterday.' '.$text_total_discount.' <br>';
        echo -($tdiscount[2] + $tdiscount[3]) . '<br>'
        ?>
        </div>


        <div id="pop3" class="popbox1 ">
        <br> <?php echo $today.' '.$text_total_product.' <br>'  . $tproduct[0] . '<br><br><br>' ?>
        <?php echo $yesterday.' '.$text_total_product.' <br>'. $tproduct[1] . '<br>' ?>
        </div>


         <div id="pop4" class="popbox1">
         <br> <?php echo $today.' '.$text_total_revenue.' <br>' .round($trevenue[0],2 ). '<br><br><br>' ?>
        <?php echo $yesterday.' '.$text_total_revenue.' <br>' . round($trevenue[1],2 ) . '<br>' ?>
        </div>


        <div id="pop5" class="popbox1">
        <br> <?php echo $today.' '.$text_total_miss_order.' <br>' . $tmissorder[0] . '<br><br><br>' ?>
        <?php echo $yesterday.' '.$text_total_miss_order.' <br>' . $tmissorder[1] . '<br>' ?>
        </div>





<!-- popup for Yesterday-->

        <div id="pop6" class="popbox1 ">
        <br> <?php echo $yesterday.' '.$text_total_order.' <br>'. $yorder[0] . '<br><br><br>' ?>
        <?php echo $otherday.' '.$text_total_order.' <br>' . $yorder[1] . '<br>' ?>
        </div>


        <div id="pop7" class="popbox1">
        <br> <?php
        echo $yesterday . ' ' . $text_total_discount . ' <br>';
        $ydiscount[5] = $ydiscount[0] + $ydiscount[1];
        echo -$ydiscount[5] . '<br><br><br>'
        ?>
        <?php
        echo $otherday . ' ' . $text_total_discount . ' <br>';
        $ydiscount[6] = $ydiscount[2] + $ydiscount[3];
        echo -$ydiscount[6] . '<br><br>'
        ?>
        </div>


         <div id="pop8" class="popbox1">
         <br> <?php echo $yesterday.' '.$text_total_product.' <br>' . $yproduct[0] . '<br><br><br>' ?>
         <?php echo $otherday.' '.$text_total_product.' <br>' . $yproduct[1] . '<br>' ?>
         </div>


        <div id="pop9" class="popbox1">
         <br> <?php echo $yesterday.' '.$text_total_revenue.' <br>' . round($yrevenue[0],2) . '<br><br><br>' ?>
        <?php echo $otherday.' '.$text_total_revenue.' <br>' . round($yrevenue[1],2) . '<br>' ?>
        </div>


        <div id="pop10" class="popbox1">
        <br> <?php echo $yesterday.' '.$text_total_miss_order.' <br>'. $ymissorder[0] . '<br><br><br>' ?>
        <?php echo $otherday.' '.$text_total_miss_order.' <br>' . $ymissorder[1] . '<br>' ?>
        </div>





<!-- popup for Months-->

        <div id="pop11" class="popbox1">
         <br> <?php echo $thismonth.' '.$text_total_order.' <br>' . $morder[0] . '<br><br><br>' ?>
        <?php echo $last_month.' '.$text_total_order.' <br>' . $morder[1] . '<br>' ?>
        </div>


        <div id="pop12" class="popbox1">
        <br> <?php echo $thismonth.' '.$text_total_discount.' <br>';
        echo -($mdiscount[0] + $mdiscount[1]) . '<br><br><br>'
        ?>
        <?php echo $last_month.' '.$text_total_discount.' <br>';
        echo -($mdiscount[2] + $mdiscount[3]) . '<br>'
        ?>
        </div>


        <div id="pop13" class="popbox1">
        <br> <?php echo $thismonth.' '.$text_total_product.' <br>' . $mproduct[0] . '<br><br><br>' ?>
        <?php echo $last_month.' '.$text_total_product.' <br>' . $mproduct[1] . '<br>' ?>
        </div>


        <div id="pop14" class="popbox1">
        <br> <?php echo $thismonth.' '.$text_total_revenue.' <br>' . round($mrevenue[0],2) . '<br><br><br>' ?>
        <?php echo $last_month.' '.$text_total_revenue.' <br>'. round($mrevenue[1],2) . '<br>' ?>
        </div>


        <div id="pop15" class="popbox1">
        <br> <?php echo $thismonth.' '.$text_total_miss_order.' <br>'. $mmissorder[0] . '<br><br><br>' ?>
        <?php echo $last_month.' '.$text_total_miss_order.' <br>' . $mmissorder[1] . '<br>' ?>
        </div>




<!-- popup for Week-->


        <div id="pop16" class="popbox1">
        <br> <?php echo $thisweek.' '.$text_total_order.' <br>' . $worder[0] . '<br><br><br>' ?>
        <?php echo $last_week.' '.$text_total_order.' <br>' . $worder[1] . '<br>' ?>
        </div>


        <div id="pop17" class="popbox1">
         <br> <?php echo $thisweek.' '.$text_total_discount.' <br>';
        echo -($wdiscount[0] + $wdiscount[1]) . '<br><br><br>'
        ?>
        <?php echo $last_week.' '.$text_total_discount.' <br>';
        echo-($wdiscount[2] + $wdiscount[3]) . '<br>'
        ?>
        </div>


        <div id="pop18" class="popbox1">
        <br> <?php echo $thisweek.' '.$text_total_product.' <br>' . $wproduct[0] . '<br><br><br>' ?>
        <?php echo $last_week.' '.$text_total_product.' <br>' . $wproduct[1] . '<br>' ?>
        </div>


        <div id="pop19" class="popbox1">
         <br> <?php echo $thisweek.' '.$text_total_revenue.' <br>' . round($wrevenue[0],2) . '<br><br><br>' ?>
        <?php echo $last_week.' '.$text_total_revenue.' <br>' . round($wrevenue[1],2) . '<br>' ?>
        </div>


        <div id="pop20" class="popbox1">
        <br> <?php echo $thisweek.' '.$text_total_miss_order.' <br>'. $wmissorder[0] . '<br><br><br>' ?>
          <?php echo $last_week.' '.$text_total_miss_order.'<br>'. $wmissorder[1] . '<br>' ?>
        </div>



<!-- popup for Year-->

        <div id="pop21" class="popbox1">
        <br> <?php echo $thisyear.' '.$text_total_order.'<br>'. $yyorder[0] . '<br><br><br>' ?>
       <?php echo $last_year.' '.$text_total_order.' <br>' . $yyorder[1] . '<br>' ?>
        </div>


        <div id="pop22" class="popbox1">
        <br> <?php echo $thisyear.' '.$text_total_discount.' <br>';
        echo -($yydiscount[0] + $yydiscount[1]) . '<br><br><br>'
        ?>
        <?php echo $last_year.' '.$text_total_discount.' <br>';
        echo -($yydiscount[2] + $yydiscount[3]) . '<br>'
        ?>
        </div>


        <div id="pop23" class="popbox1">
        <br> <?php echo $thisyear.' '.$text_total_product.' <br>'. $yyproduct[0] . '<br><br><br>' ?>
        <?php echo $last_year.' '.$text_total_product.' <br>' . $yyproduct[1] . '<br>' ?>
        </div>


        <div id="pop24" class="popbox1">
        <br> <?php echo $thisyear.' '.$text_total_revenue.' <br>'. round($yyrevenue[0],2) . '<br><br><br>' ?>
        <?php echo $last_year.' '.$text_total_revenue.' <br>'. round($yyrevenue[1],2) . '<br>' ?>
        </div>


        <div id="pop25"  class="popbox1">
       <br> <?php echo $thisyear.' '.$text_total_miss_order.' <br>' . $yymissorder[0] . '<br><br><br>' ?>
       <?php echo $last_year.' '.$text_total_miss_order.' <br>' . $yymissorder[1] . '<br>' ?>
        </div>




    </tr>

    </tbody>
                </table>
            </div>
        </div>

<?php if ($birdsview_chart_status == 1) {?>

<div class="statistic" style="width:100%;">
    <div class="range">
        <label for="range1">Order:</label><input style="margin-right:8px;" type="radio" id="range1" name="option" value="order" />
        <label for="range2">Revenue:</label><input style=" margin-top:5px;margin-right:8px;" type="radio"  id="range2"  name="option" value="revenue" />
        <label for="range3">Both:</label><input type="radio" id="range3" checked="checked" name="option" value="both"/>
    </div>

    <div class="dashboard-heading"><?php echo $graphchart; ?></div>
    <div class="dashboard-content" style="min-height:0;">
        
        <div  id="chart_div" style="margin:auto;width:1120px; height:370px;"></div>







    </div>
</div>

<?php }?>


   <!-- including Script for popup-->
   <script type="text/javascript" src="view/javascript/birdsview/birdsview.js"></script>
   <!-- javascript for graphchart-->
   <script type="text/javascript">
       $(document).ready(function(){
draw('both');

$("#range1").click(function(){ 
var range=$("#range1").val();
draw(range);
      

});
   
   $("#range2").click(function(){ 
var range=$("#range2").val();
draw(range);
      

});

$("#range3").click(function(){ 
var range=$("#range3").val();
draw(range);
      

});
        
        function draw(range){
          
         if(range=='both')
         {
          $('#chart_div').highcharts({

                    title: {
                    text: 'Order & Revenue Chart',
                            x: - 20 //center
                    },
                            xAxis: {title:  {
                            text: 'Hours'
                            },
                                    categories: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24']
                            },
                            yAxis: [   { // Primary yAxis
                            min:0,
                                    title: {
                                    text: 'Order'

                                    }
                            },
                            { // Secondary yAxis
                            opposite: true, title:  {
                            text: 'Revenue'
                            }


                            }],
                            legend: {
                            layout: 'vertical',
                                    align: 'right',
                                    verticalAlign: 'middle',
                                    borderWidth: 0
                            },
                            series: [
                            {
                            name: 'Todays Revenue',
                                    type:'column', yAxis:1,
                                    data:
                                    [  <?php $data = $this->model_birdsview_total->trevenue(); ?>
<?php
for ($i = 0; $i < 24; $i++) {
    echo $data[$i] . ",";
    if ($i == 23)
        echo $data[$i];
}
?>   ]
                            },
                            {
                            name: 'Last 7 days Revenue',
                                    type:'column', yAxis:1,
                                    data:
                                    [  <?php $data = $this->model_birdsview_total->wrevenue(); ?>
<?php
for ($i = 0; $i < 24; $i++) {
    echo $data[$i] . ",";
    if ($i == 23)
        echo $data[$i];
}
?>   ]
                            },
                            {
                            name: 'Last 30 days Revenue',
                                    type:'column', yAxis:1,
                                    data:
                                    [  <?php $data = $this->model_birdsview_total->mrevenue(); ?>
<?php
for ($i = 0; $i < 24; $i++) {
    echo $data[$i] . ",";
    if ($i == 23)
        echo $data[$i];
}
?>   ]
                            },
                            {
                            name: 'Last 90 Revenue',
                                    type:'column', yAxis:1,
                                    data:
                                    [  <?php $data = $this->model_birdsview_total->qrevenue(); ?>
<?php
for ($i = 0; $i < 24; $i++) {
    echo $data[$i] . ",";
    if ($i == 23)
        echo $data[$i];
}
?>   ]
                            }


                            ,
                            {
                            name: 'Todays Orders',
                                    type:'line',
                                    data:
                                    [  <?php $data = $this->model_birdsview_total->torder(); ?>
<?php
for ($i = 0; $i < 24; $i++) {
    echo $data[$i] . ",";
    if ($i == 23)
        echo (int) $data[$i];
}
?>   ]
                            },
                            {
                            name: 'Last 7 days Orders',
                                    type:'line',
                                    data:  [  <?php $data = $this->model_birdsview_total->worder(); ?>
<?php
for ($i = 0; $i < 24; $i++) {
    echo $data[$i] . ",";
    if ($i == 23)
        echo (int) $data[$i];
}
?>   ]
                            },
                            {
                            name: 'Last 30 days Orders',
                                    type:'line',
                                    data:  [  <?php $data = $this->model_birdsview_total->morder(); ?>
<?php
for ($i = 0; $i < 24; $i++) {
    echo $data[$i] . ",";
    if ($i == 23)
        echo (int) $data[$i];
}
?>   ]
                            },
                            {
                            name: 'Last 90 Days Orders',
                                    type:'line',
                                    data:  [  <?php $data = $this->model_birdsview_total->qorder(); ?>
<?php
for ($i = 0; $i < 24; $i++) {
    echo $data[$i] . ",";
    if ($i == 23)
        echo (int) $data[$i];
}
?>   ]
                            }


                            ]


                    }); // both high chart
                    } //if range==both






if(range=='order')
         {
          $('#chart_div').highcharts({

                    title: {
                    text: 'Orders Chart',
                            x: - 20 //center
                    },
                            xAxis: {title:  {
                            text: 'Hours'
                            },
                                    categories: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24']
                            },
                            yAxis: [   { // Primary yAxis
                                    min:0,
                                    title: {
                                    text: 'Order'

                                    }
                            }],
                            legend: {
                            layout: 'vertical',
                                    align: 'right',
                                    verticalAlign: 'middle',
                                    borderWidth: 0
                            },
                            series: [
                            
                            {
                            name: 'Todays Orders',
                                    type:'line',
                                    data:
                                    [  <?php $data = $this->model_birdsview_total->torder(); ?>
<?php
for ($i = 0; $i < 24; $i++) {
    echo $data[$i] . ",";
    if ($i == 23)
        echo (int) $data[$i];
}
?>   ]
                            },
                            {
                            name: 'Last 7 days Orders',
                                    type:'line',
                                    data:  [  <?php $data = $this->model_birdsview_total->worder(); ?>
<?php
for ($i = 0; $i < 24; $i++) {
    echo $data[$i] . ",";
    if ($i == 23)
        echo (int) $data[$i];
}
?>   ]
                            },
                            {
                            name: 'Last 30 days Orders',
                                    type:'line',
                                    data:  [  <?php $data = $this->model_birdsview_total->morder(); ?>
<?php
for ($i = 0; $i < 24; $i++) {
    echo $data[$i] . ",";
    if ($i == 23)
        echo (int) $data[$i];
}
?>   ]
                            },
                            {
                            name: 'Last 90 Days Orders',
                                    type:'line',
                                    data:  [  <?php $data = $this->model_birdsview_total->qorder(); ?>
<?php
for ($i = 0; $i < 24; $i++) {
    echo $data[$i] . ",";
    if ($i == 23)
        echo (int) $data[$i];
}
?>   ]
                            }


                            ]


                    }); // order high chart
                    } //if range==order



else
         if(range=='revenue')
         {
          $('#chart_div').highcharts({

                    title: {
                    text: 'Revenue Chart',
                            x: - 20 //center
                    },
                            xAxis: {title:  {
                            text: 'Hours'
                            },
                                    categories: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24']
                            },
                            yAxis: [   { // Primary yAxis
                            min:0,
                                    title: {
                                    text: 'Revenue'

                                    }
                            }],
                            legend: {
                            layout: 'vertical',
                                    align: 'right',
                                    verticalAlign: 'middle',
                                    borderWidth: 0
                            },
                            series: [
                            {
                            name: 'Todays Revenue',
                                    type:'column',
                                    data:
                                    [  <?php $data = $this->model_birdsview_total->trevenue(); ?>
<?php
for ($i = 0; $i < 24; $i++) {
    echo $data[$i] . ",";
    if ($i == 23)
        echo $data[$i];
}
?>   ]
                            },
                            {
                            name: 'Last 7 days Revenue',
                                    type:'column', 
                                    data:
                                    [  <?php $data = $this->model_birdsview_total->wrevenue(); ?>
<?php
for ($i = 0; $i < 24; $i++) {
    echo $data[$i] . ",";
    if ($i == 23)
        echo $data[$i];
}
?>   ]
                            },
                            {
                            name: 'Last 30 days Revenue',
                                    type:'column',
                                    data:
                                    [  <?php $data = $this->model_birdsview_total->mrevenue(); ?>
<?php
for ($i = 0; $i < 24; $i++) {
    echo $data[$i] . ",";
    if ($i == 23)
        echo $data[$i];
}
?>   ]
                            },
                            {
                            name: 'Last 90 Revenue',
                                    type:'column', 
                                    data:
                                    [  <?php $data = $this->model_birdsview_total->qrevenue(); ?>
<?php
for ($i = 0; $i < 24; $i++) {
    echo $data[$i] . ",";
    if ($i == 23)
        echo $data[$i];
}
?>   ]
                            }


                            ]


                    }); // revenue high chart function
                    } // if range==revenue


























                    }// draw function


 });//ready function



       </script>
       
   <script type="text/javascript" src="view/javascript/birdsview/highcharts.js"></script>
   <script type="text/javascript" src="view/javascript/birdsview/exporting.js"></script>


<!-- closing braces for plug-in status check if condition-->
<?php
}
?>