<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-seourl').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
          <ul class="nav nav-tabs">
              <li<?php echo (!isset($tab) || $tab=='store') ? ' class="active"' : ''; ?>><a href="#" class="tab-url" data-tab="store"><?php echo $tab_store; ?></a></li>
              <li<?php echo (isset($tab) && $tab=='product') ? ' class="active"' : ''; ?>><a href="#" class="tab-url" data-tab="product"><?php echo $tab_product; ?></a></li>
              <li<?php echo (isset($tab) && $tab=='category') ? ' class="active"' : ''; ?>><a href="#" class="tab-url" data-tab="category"><?php echo $tab_category; ?></a></li>
              <li<?php echo (isset($tab) && $tab=='information') ? ' class="active"' : ''; ?>><a href="#" class="tab-url" data-tab="information"><?php echo $tab_information; ?></a></li>
              <li<?php echo (isset($tab) && $tab=='manufacturer') ? ' class="active"' : ''; ?>><a href="#" class="tab-url" data-tab="manufacturer"><?php echo $tab_manufacturer; ?></a></li>
          </ul>
        <div class="tab-content">
          <!-- Tab Store -->
          <div class="tab-pane active" id="tab-<?php echo $tab ? $tab : 'store'; ?>">
            <div class="well">
              <div class="row">
                <div class="col-sm-5">
                  <div class="form-group">
                    <label class="control-label" for="input-product"><?php echo $entry_query; ?></label>
                    <input type="text" name="filter_query" value="<?php echo $filter_query; ?>" placeholder="<?php echo $entry_query; ?>" id="input-query" class="form-control" />
                  </div>
                </div>
                <div class="col-sm-7">
                  <div class="form-group">
                      <label class="control-label" for="input-keyword"><?php echo $entry_keyword; ?></label>
                      <input type="text" name="filter_keyword" value="<?php echo $filter_keyword; ?>" placeholder="<?php echo $entry_keyword; ?>" id="input-keyword" class="form-control" />
                  </div>
                <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
                </div>
              </div>
            </div>
            <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-seourl">
              <div class="table-responsive">
                <table class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                      <td class="text-left"><?php if ($sort == 'ua.query') { ?>
                        <a href="<?php echo $sort_query; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_query; ?></a>
                        <?php } else { ?>
                        <a href="<?php echo $sort_query; ?>"><?php echo $column_query; ?></a>
                        <?php } ?></td>
                      <td class="text-left"><?php if ($sort == 'ua.keyword') { ?>
                        <a href="<?php echo $sort_keyword; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_keyword; ?></a>
                        <?php } else { ?>
                        <a href="<?php echo $sort_keyword; ?>"><?php echo $column_keyword; ?></a>
                        <?php } ?></td>
                      <td class="text-right"><?php echo $column_action; ?></td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if ($seourls) { ?>
                    <?php foreach ($seourls as $seourl) { ?>
                    <tr>
                      <td class="text-center"><?php if (in_array($seourl['url_alias_id'], $selected)) { ?>
                          <input type="checkbox" name="selected[]" value="<?php echo $seourl['url_alias_id']; ?>" checked="checked" />
                        <?php } else { ?>
                          <input type="checkbox" name="selected[]" value="<?php echo $seourl['url_alias_id']; ?>" />
                        <?php } ?></td>
                      <td class="text-left"><?php echo $seourl['query']; ?></td>
                      <td class="text-left"><?php echo $seourl['keyword']; ?></td>
                      <td class="text-right">
                          <a href="<?php echo $seourl['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                          <?php if ($seourl['edit_out']) { ?>
                          <a href="<?php echo $seourl['edit_out']; ?>" data-toggle="tooltip" title="<?php echo $button_edit_out; ?>" class="btn btn-info" target="_blank"><i class="fa fa-edit"></i></a>
                          <?php } ?>
                      </td>
                    </tr>
                    <?php } ?>
                    <?php } else { ?>
                    <tr>
                      <td class="text-center" colspan="7"><?php echo $text_no_results; ?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </form>
            <div class="row">
              <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
              <div class="col-sm-6 text-right"><?php echo $results; ?></div>
            </div>
          </div>
        </div><!-- Tab content -->
      </div><!-- Panel body -->
    </div>
    <div style="text-align:center;">SEO Url Editor by <a target="_blank" href="http://instup.com">Instup.com</a></div>
  </div>
<script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	url = 'index.php?route=tool/seourl_editor&token=<?php echo $token; ?>&tab=<?php echo $tab; ?>';
	var filter_query = $('input[name=\'filter_query\']').val();
	if (filter_query) {
		url += '&filter_query=' + encodeURIComponent(filter_query);
	}
	var filter_keyword = $('input[name=\'filter_keyword\']').val();
	if (filter_keyword) {
		url += '&filter_keyword=' + encodeURIComponent(filter_keyword);
	}
	location = url;
});
$('a.tab-url').on('click', function(event) {
    event.preventDefault();
    url = 'index.php?route=tool/seourl_editor&token=<?php echo $token; ?>';
    url += '&tab=' + encodeURIComponent($(this).data('tab'));
    location = url;
});
//--></script>
</div>
<?php echo $footer; ?>