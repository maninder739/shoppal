<!DOCTYPE html>

<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link href="view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
<link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
<link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="all" />
<style type="text/css">
thead tr td{
    font-weight:bold;
  }
  .table-responsive thead td {
    background: #fff none repeat scroll 0 0;
    padding: 5px !important;
    text-align: center;
  border-bottom:2px solid #cccccc;
}



.table-responsive tbody td {
    padding: 5px !important;
  line-height: 1.7;
}
.panel-heading {
    background: #eeeeee none repeat scroll 0 0;
}
.page-header{
  text-align:center;
}
.table.table-bordered tbody tr:nth-child(2n) {
    background-color: #eeeeee !important;
}


.header{
  width: 100%;
}
.logo{
  width: 25%;
  float:left;
  margin-top:20px !important;
}
.company{
  width: 72%;
  float:right;
  margin-top:20px !important;
}
.logo img, .company_info p{
  width:100%;
}
.company h2{
  float:right;
}
.panel-heading{
  display:none;
}
.table-bordered, .table-bordered td {
    border: 1px solid #dddddd;
  border-collapse:collapse;
}
.company_info p{
  width: 100%;
  font-weight: bold;
  margin:0px;
}
.company_info p span{
  font-size:12px;
}
.date span{
  float:right;
}

/*mail type*/
.mail_type{
  width:100%;
}
.mail{
  float:left;
  width:50%;
}
.type{
  float:right;
  width:30%;
  font-weight:bold;
  font-size:12px;
}
.type span{
  float:right;
  font-size:12px;
  font-weight:normal;
}
/*mail type*/

.owner-date{
  width: 100%;
}
.owner{
  float:left;
  width: 50%;
}
.date{
  float:right;
  width: 17%;
  font-weight: bold;
  font-size:12px;
}
.date span{
  float:right;
  font-size:12px;
  font-weight:normal;
}
table{
  margin:10px auto;
  width:100%;
  font-family:Verdana, Geneva, sans-serif;
  font-size:12px;
  text-align:center;
}
td{
  width:auto;
}
p{
  font-size:12px;
}
p span{
  font-weight:normal;
}
#content{
  font-family:Verdana, Geneva, sans-serif;
}
.footer{
  width:100%;
}
.address
{
  width:70%;
  float:left;
}
.pageno{
  width:4%;
  float:right;
}
</style>
</head>
<body>
<div class="container">
  <?php foreach ($orders as $order) {
  ?>
  <div style="page-break-after: always;">
  	<table style="width: 100%; margin-bottom:10px;">
  		<tr>
  			<td style="text-align:center; width: 100%;"><img src="<?php echo $logo;?>" title="" alt="" class="img-responsive" style="width: 100px;height:100px; margin: 0 auto;"/></td>
        </tr>
    </table>
  	<table class="table">
      <thead>
        <tr>
      	<td class="text-center text-uppercase"><h3><strong><?php echo $text_invoice; ?></strong></h3></td> 
        </tr>
      </thead>
    </table>
    <table class="table table-responsive table-bordered"> 
      <thead>
        <tr>
          <td style="width: 34%;"><b><?php echo $text_order_detail; ?></b></td>
          <td style="width: 33%;"><b><?php echo $text_payment_address; ?></b></td>
          <td style="width: 33%;"><b><?php echo $text_shipping_address; ?></b></td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style ="vertical-align: top; text-align: left;">
            <b><?php echo $customer_name.':'; ?></b> <?php echo $order['firstname'].' '.$order['lastname'] ; ?><br />
            <b><?php echo $text_date_added; ?></b> <?php echo $order['date_added']; ?><br />
            <?php if ($order['invoice_no']) { ?>
            <b><?php echo $text_invoice_no; ?></b> <?php echo $order['invoice_no']; ?><br />
            <?php } ?>
            <b><?php echo $text_order_id; ?></b> <?php echo $order['order_id']; ?><br />
            <b><?php echo $text_payment_method; ?></b> <?php echo $order['payment_method']; ?><br />
            <?php if ($order['shipping_method']) { ?>
            <b><?php echo $text_shipping_method; ?></b> <?php echo $order['shipping_method']; ?><br />
            <?php } ?>
             <b><?php echo $order_type.':'; ?></b> 
             <?php
              $get_order_status = 'one time'; 
              if(isset($order['get_order_type']))
              {
                $get_order_status = $order['get_order_type']; 
               
              }
               echo ucwords($get_order_status); 

             ?><br />
          </td>
          <td style ="vertical-align: top; text-align: left;" ><address>
            <?php echo $order['payment_address']; ?>
            </address></td> 
          <td style ="vertical-align: top; text-align: left;"><address>
            <?php echo $order['shipping_address']; ?>
            </address></td>
        </tr>
      </tbody>
    </table>
    <table class="table table-responsive table-bordered">
      <thead>
        <tr>
          <td><b><?php echo $column_product; ?></b></td>
          <td><b><?php echo $column_model; ?></b></td>
          <td class="text-right"><b><?php echo $column_quantity; ?></b></td>
          <td class="text-right"><b><?php echo $column_price; ?></b></td>
          <td class="text-right"><b><?php echo $column_total; ?></b></td>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($order['product'] as $product) { ?>
        <tr>
          <td><?php echo $product['name']; ?>
            <?php foreach ($product['option'] as $option) { ?>
            <br />
            &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
            <?php } ?></td>
          <td><?php echo $product['model']; ?></td>
          <td class="text-right"><?php echo $product['quantity']; ?></td>
          <td class="text-right"><?php echo $product['price']; ?></td>
          <td class="text-right"><?php echo $product['total']; ?></td>
        </tr>
        <?php } ?>
        <?php foreach ($order['voucher'] as $voucher) { ?>
        <tr>
          <td><?php echo $voucher['description']; ?></td>
          <td></td>
          <td class="text-right">1</td>
          <td class="text-right"><?php echo $voucher['amount']; ?></td>
          <td class="text-right"><?php echo $voucher['amount']; ?></td>
        </tr>
        <?php } ?>
        <?php foreach ($order['total'] as $total) { ?>
        <tr>
          <td class="text-right" colspan="4"><b><?php echo $total['title']; ?></b></td>
          <td class="text-right"><?php echo $total['text']; ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
    <?php if ($order['comment']) { ?>
    <table class="table table-bordered">
      <thead>
        <tr>
          <td><b><?php echo $text_comment; ?></b></td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><?php echo $order['comment']; ?></td>
        </tr>
      </tbody>
    </table>
    <?php } ?>
    <table class="table table-responsive table-bordered">
      <tbody>
        <tr>
          <td class="text-left" style="width: 50%;"><address>
            <strong><?php echo $order['store_name']; ?></strong><br />
            <?php echo $order['store_address']; ?>
            </address></td>
          <td class="text-right" style="width: 50%;"><b><?php echo $text_telephone; ?></b> <?php echo $order['store_telephone']; ?><br />
            <?php if ($order['store_fax']) { ?>
            <b><?php echo $text_fax; ?></b> <?php echo $order['store_fax']; ?><br />
            <?php } ?>
            <b><?php echo $text_email; ?></b> <?php echo $order['store_email']; ?><br />
          <b><?php echo $text_website; ?></b> <?php echo $order['store_url']; ?></td>
        </tr>
      </tbody>
    </table>
  </div>
  <?php } ?>
</div>
</body>
</html>
