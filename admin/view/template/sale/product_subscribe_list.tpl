<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right"><a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<?php if ($success) { ?>
			<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
			</div>
			<form action="" method="post" enctype="multipart/form-data" id="form-product-subscribe">
				<div style="width: 100%; text-align: center; margin-bottom: 9px;"><?php echo $text_color_code; ?></div>
				<div style="width: 100%; text-align: center; margin-bottom: 9px;" class="help"><?php echo $text_update_cost_help; ?></div>
				<div class="table-responsive">
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<td class="text-center"><?php echo $column_active; ?></td>
								<td class="text-center"><?php echo $column_status; ?></td>
								<td class="text-center"><?php echo $column_order_id; ?></td>
								<td class="text-left"><?php echo $column_customer; ?></td>
								<td class="text-left"><?php echo $column_product_name; ?></td>
								<td class="text-left" style="width: 150px;"><?php echo $column_terms; ?></td>
								<td class="text-center"><?php echo $column_quantity; ?></td>
								<td class="text-center"><?php echo $column_trial_end; ?></td>
								<td class="text-center"><?php echo $column_start_date; ?></td>
								<td class="text-center"><?php echo $column_end_date; ?></td>
								<td class="text-center"><?php echo $column_amount; ?></td>
								<td class="text-center"><?php echo $column_next_due; ?></td>
								<td class="text-center"><?php echo $column_last_paid; ?></td>
								<td class="text-center"><?php echo $column_action; ?></td>
							</tr>
						</thead>
						<tbody>
							<?php if ($subscriptions) { ?>
								<?php foreach ($subscriptions as $subscription) { ?>
									<tr>
										<?php if ($subscription['active'] == 1 && $subscription['expiring'] == 0 && $subscription['terms'] && $subscription['overdue'] == 0) { ?>
											<td class="text-center" style="background-color: green !important;;"></td>
										<?php } elseif ($subscription['active'] == 1 && $subscription['expiring'] == 1 && $subscription['terms']) { ?>
											<td class="text-center" style="background-color: orange !important;;"></td>
										<?php } elseif ($subscription['active'] == 1 && $subscription['overdue']) { ?>
											<td class="text-center" style="background-color: purple !important;"></td>
										<?php } elseif ($subscription['terms']) { ?>
											<td class="text-center" style="background-color: red !important;"></td>
										<?php } else { ?>
											<td class="text-center" style="background-color: yellow !important;"></td>
										<?php } ?>
										<td class="text-center"><?php echo $subscription['status']; ?></td>
										<td class="text-center"><?php echo $subscription['order_id']; ?></td>
										<td class="text-left"><?php echo $subscription['customer']; ?></td>
										<td class="text-left"><?php echo $subscription['name']; ?></td>
										<td class="text-left"><?php echo $subscription['terms']; ?></td>
										<td class="text-center"><?php echo $subscription['quantity']; ?></td>
										<td class="text-center"><?php echo $subscription['trial_end']; ?></td>
										<td class="text-center">
											<?php if ($subscription['terms']) { ?>
												<input style="text-align: center !important; width: 75px !important;" type="text" id="start_date-<?php echo $subscription['order_subscription_id']; ?>" name="start_date" title="<?php echo $subscription['order_subscription_id']; ?>" value="<?php echo $subscription['start_date']; ?>" class="date" />
											<?php } else { ?>
												<?php echo $subscription['start_date']; ?>
											<?php } ?>
										</td>
										<td class="text-center">
											<?php if ($subscription['end_date']) { ?>
												<input style="text-align: center !important; width: 75px !important;" type="text" id="end_date-<?php echo $subscription['order_subscription_id']; ?>" name="end_date" value="<?php echo $subscription['end_date']; ?>" title="<?php echo $subscription['order_subscription_id']; ?>" class="date" />
											<?php } elseif ($subscription['terms'] && $subscription['active'] == 1) { ?>
												<?php echo $text_until_cancelled; ?>
											<?php } ?>
										</td>
										<td class="text-center">
											<?php if ($subscription['terms']) { ?>
												<input style="text-align: right !important; width: 70px !important;" type="text" id="cost-<?php echo $subscription['order_subscription_id']; ?>" name="cost" value="<?php echo $subscription['amount']; ?>" title="<?php echo $subscription['order_subscription_id']; ?>_<?php echo $subscription['order_recurring_id']; ?>" />
											<?php } else { ?>
												<?php echo $subscription['amount']; ?>
											<?php } ?>
										</td>
										<td class="text-center">
											<?php if ($subscription['terms']) { ?>
												<input style="text-align: center !important; width: 75px !important;" type="text" id="next_due-<?php echo $subscription['order_subscription_id']; ?>" name="next_due" value="<?php echo $subscription['next_due']; ?>" title="<?php echo $subscription['order_subscription_id']; ?>" class="date" />
											<?php } ?>
										</td>
										<td class="text-center">
											<?php if ($subscription['terms']) { ?>
												<input style="text-align: center !important; width: 75px !important;" type="text" id="last_paid-<?php echo $subscription['order_subscription_id']; ?>" name="last_paid" value="<?php echo $subscription['last_paid']; ?>" title="<?php echo $subscription['order_subscription_id']; ?>" class="date" />
											<?php } else { ?>
												<?php echo $subscription['last_paid']; ?>
											<?php } ?>
										</td>
										<td class="center">
											<a href="<?php echo $subscription['href']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-info"><i class="fa fa-eye"></i></a> <a href="<?php echo $subscription['delete']; ?>" id="button-delete<?php echo $subscription['order_subscription_id']; ?>" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
										</td>
									</tr>
								<?php } ?>
							<?php } else { ?>
								<tr>
									<td class="text-center" colspan="15"><?php echo $text_no_records; ?></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</form>
			<div class="row">
				<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
				<div class="col-sm-6 text-right"><?php echo $results; ?></div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">

$(document).ready(function() {

	$('a[id^=\'button-delete\']').on('click', function(e) {
		e.preventDefault();
		if (confirm('<?php echo $text_confirm; ?>')) {
			location = $(this).attr('href');
		}
	});

	$('input[name=\'cost\']').on('focus', function() {
		$(this).select();
	});

	$('input[name=\'cost\']').on('change', function() {
		var row = $(this).attr('title');
		var cost = $(this).val();
		$.ajax({
			url: 'index.php?route=sale/product_subscribe/updateCost&token=<?php echo $token; ?>',
			type: 'POST',
			dataType: 'json',
			data: 'form_data=' + row + '&cost=' + cost,
			success: function(json) {
				location.href = json;
			},
			error: function(xhr, ajaxOptions, thrownError) {
           alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});

	$('input[name=\'start_date\']').on('change', function() {
		var row = $(this).attr('title');
		var new_date = $(this).val();
		$.ajax({
			url: 'index.php?route=sale/product_subscribe/updateStartDate&token=<?php echo $token; ?>',
			type: 'POST',
			dataType: 'json',
			data: 'order_subscription_id=' + row + '&new_date=' + new_date,
			success: function(json) {
				location.href = json;
			},
			error: function(xhr, ajaxOptions, thrownError) {
           alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});

	$('input[name=\'end_date\']').on('change', function() {
		var row = $(this).attr('title');
		var new_date = $(this).val();
		$.ajax({
			url: 'index.php?route=sale/product_subscribe/updateEndDate&token=<?php echo $token; ?>',
			type: 'POST',
			dataType: 'json',
			data: 'order_subscription_id=' + row + '&new_date=' + new_date,
			success: function(json) {
				location.href = json;
			},
			error: function(xhr, ajaxOptions, thrownError) {
           alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});

	$('input[name=\'next_due\']').on('change', function() {
		var row = $(this).attr('title');
		var new_date = $(this).val();
		$.ajax({
			url: 'index.php?route=sale/product_subscribe/updateNextDue&token=<?php echo $token; ?>',
			type: 'POST',
			dataType: 'json',
			data: 'order_subscription_id=' + row + '&new_date=' + new_date,
			success: function(json) {
				location.href = json;
			},
			error: function(xhr, ajaxOptions, thrownError) {
           alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});

	$('input[name=\'last_paid\']').on('change', function() {
		var row = $(this).attr('title');
		var new_date = $(this).val();
		$.ajax({
			url: 'index.php?route=sale/product_subscribe/updateLastPaid&token=<?php echo $token; ?>',
			type: 'POST',
			dataType: 'json',
			data: 'order_subscription_id=' + row + '&new_date=' + new_date,
			success: function(json) {
				location.href = json;
			},
			error: function(xhr, ajaxOptions, thrownError) {
           alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});

	$('.delete').on('click', function() {
		if (confirm('<?php echo $text_delete_confirm; ?>')) {
			var order_subscription_id = $(this).attr('title');
			$.ajax({
				url: 'index.php?route=sale/product_subscribe/singleDelete&token=<?php echo $token; ?>',
				type: 'POST',
				dataType: 'json',
				data: 'order_subscription_id=' + order_subscription_id,
				success: function(json) {
					location.href = json;
				},
				error: function(xhr,j,i) {
					alert(i);
				}
			});
		} else {
			return false;
		}
	});

});

</script>
<script type="text/javascript"><!--
	$('.date').datetimepicker({
		pickTime: false
	});
//--></script> 
<?php echo $footer; ?>