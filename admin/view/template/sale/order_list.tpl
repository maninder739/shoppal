<?php echo $header; ?>
<?php echo $column_left; ?> 
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right"> 

                 <button class="btn btn-info" data-toggle="tooltip" form="form-order" formaction="<?php echo $shipping; ?>"  id="button-print" title="<?php echo $button_print_selected_shipping_lists; ?>" type="submit">
                    <i class="fa fa-truck">
                    </i>
                </button>

                <button class="btn btn-success" data-toggle="tooltip" form="form-order" formaction="<?php echo $pdf; ?>" formtarget="_blank" id="button-print" title="<?php echo $Print_Outstanding_Invoices; ?>" type="submit">
                    <i class="fa fa-file-pdf-o">
                    </i>
                </button>
                <button class="btn btn-info" data-toggle="tooltip" form="form-order" formaction="<?php echo $invoice; ?>" formtarget="_blank" id="button-invoice" title="<?php echo $button_invoice_print; ?>" type="submit">
                    <i class="fa fa-print">
                    </i>
                </button>
                <a class="btn btn-primary" data-toggle="tooltip" href="<?php echo $add; ?>" title="<?php echo $button_add; ?>">
                    <i class="fa fa-plus">
                    </i>
                </a>
                <button class="btn btn-danger" data-toggle="tooltip" form="form-order" formaction="<?php echo $delete; ?>" id="button-delete" title="<?php echo $button_delete; ?>" type="button">
                    <i class="fa fa-trash-o">
                    </i>
                </button>
            </div>
            <h1>
                <?php echo $heading_title; ?>
            </h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li>
                    <a href="<?php echo $breadcrumb['href']; ?>">
                        <?php echo $breadcrumb['text']; ?>
                    </a>
                </li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-circle">
            </i>
            <?php echo $error_warning; ?>
            <button class="close" data-dismiss="alert" type="button">
                ×
            </button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success">
            <i class="fa fa-check-circle">
            </i>
            <?php echo $success; ?>
            <button class="close" data-dismiss="alert" type="button">
                ×
            </button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fa fa-list">
                    </i>
                    <?php echo $text_list; ?>
                </h3>
            </div>
            <div class="panel-body">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-order-id">
                                    <?php echo $entry_order_id; ?>
                                </label>
                                <input class="form-control" id="input-order-id" name="filter_order_id" placeholder="<?php echo $entry_order_id; ?>" type="text" value="<?php echo $filter_order_id; ?>"/>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-customer">
                                    <?php echo $entry_customer; ?>
                                </label>
                                <input class="form-control" id="input-customer" name="filter_customer" placeholder="<?php echo $entry_customer; ?>" type="text" value="<?php echo $filter_customer; ?>"/>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-order-recur">
                                    <?php echo $entry_OrderType; ?>
                                </label>
                                <select class="form-control" id="" name="filter_order_recur">
                                    <option value="">
                                    </option>
                                    <!--  <option value="one time">One Time</option>
                           <option value="monthly">Monthly</option> -->
                                    <?php foreach ($all_order_recur as $allorderrecur) { ?>
                                    <?php if ($allorderrecur['order_type'] == $filter_order_recur) { ?>
                                    <option selected="selected" value="<?php echo $allorderrecur['order_type']; ?>">
                                        <?php echo $allorderrecur['order_type']; ?>
                                    </option>
                                    <?php } else { ?>
                                    <option value="<?php echo $allorderrecur['order_type']; ?>">
                                        <?php echo ucwords($allorderrecur['order_type']); ?>
                                    </option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-order-status">
                                    <?php echo $entry_order_status; ?>
                                </label>
                                <select class="form-control" id="input-order-status" name="filter_order_status">
                                    <option value="*">
                                    </option>
                                    <?php if ($filter_order_status == '0') { ?>
                                    <option selected="selected" value="0">
                                        <?php echo $text_missing; ?>
                                    </option>
                                    <?php } else { ?>
                                    <option value="0">
                                        <?php echo $text_missing; ?>
                                    </option>
                                    <?php } ?>
                                    <?php foreach ($order_statuses as $order_status) { ?>
                                    <?php if ($order_status['order_status_id'] == $filter_order_status) { ?>
                                    <option selected="selected" value="<?php echo $order_status['order_status_id']; ?>">
                                        <?php echo $order_status['name']; ?>
                                    </option>
                                    <?php } else { ?>
                                    <option value="<?php echo $order_status['order_status_id']; ?>">
                                        <?php echo $order_status['name']; ?>
                                    </option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-total">
                                    <?php echo $entry_total; ?>
                                </label>
                                <input class="form-control" id="input-total" name="filter_total" placeholder="<?php echo $entry_total; ?>" type="text" value="<?php echo $filter_total; ?>"/>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-date-added">
                                    <?php echo $entry_date_added; ?>
                                </label>
                                <div class="input-group date">
                                    <input class="form-control" data-date-format="YYYY-MM-DD" id="input-date-added" name="filter_date_added" placeholder="<?php echo $entry_date_added; ?>" type="text" value="<?php echo $filter_date_added; ?>"/>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">
                                            <i class="fa fa-calendar">
                                            </i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-date-modified">
                                    <?php echo $entry_date_modified; ?>
                                </label>
                                <div class="input-group date">
                                    <input class="form-control" data-date-format="YYYY-MM-DD" id="input-date-modified" name="filter_date_modified" placeholder="<?php echo $entry_date_modified; ?>" type="text" value="<?php echo $filter_date_modified; ?>"/>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">
                                            <i class="fa fa-calendar">
                                            </i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label style="display: block;">
                                </label>
                                <button class="btn btn-primary pull-right" id="button-filter" type="button">
                                    <i class="fa fa-filter">
                                    </i>
                                    <?php echo $button_filter; ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="" enctype="multipart/form-data" id="form-order" method="post">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td class="text-center" style="width: 1px;">
                                        <input onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" type="checkbox"/>
                                    </td>
                                    <td class="text-right">
                                        <?php if ($sort == 'o.order_id') { ?>
                                        <a class="<?php echo strtolower($order); ?>" href="<?php echo $sort_order; ?>">
                                            <?php echo $column_order_id; ?>
                                        </a>
                                        <?php } else { ?>
                                        <a href="<?php echo $sort_order; ?>">
                                            <?php echo $column_order_id; ?>
                                        </a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'customer') { ?>
                                        <a class="<?php echo strtolower($order); ?>" href="<?php echo $sort_customer; ?>">
                                            <?php echo $column_customer; ?>
                                        </a>
                                        <?php } else { ?>
                                        <a href="<?php echo $sort_customer; ?>">
                                            <?php echo $column_customer; ?>
                                        </a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                          <?php echo $column_code; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'order_recur') { ?>
                                        <a class="<?php echo strtolower($order); ?>" href="<?php echo $sort_order_recur; ?>">
                                            <?php echo $column_OrderType; ?>
                                        </a>
                                        <?php } else { ?>
                                        <a href="<?php echo $sort_order_recur; ?>">
                                            <?php echo $column_OrderType; ?>
                                        </a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'order_status') { ?>
                                        <a class="<?php echo strtolower($order); ?>" href="<?php echo $sort_status; ?>">
                                            <?php echo $column_status; ?>
                                        </a>
                                        <?php } else { ?>
                                        <a href="<?php echo $sort_status; ?>">
                                            <?php echo $column_status; ?>
                                        </a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-right">
                                        <?php if ($sort == 'o.total') { ?>
                                        <a class="<?php echo strtolower($order); ?>" href="<?php echo $sort_total; ?>">
                                            <?php echo $column_total; ?>
                                        </a>
                                        <?php } else { ?>
                                        <a href="<?php echo $sort_total; ?>">
                                            <?php echo $column_total; ?>
                                        </a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'o.date_added') { ?>
                                        <a class="<?php echo strtolower($order); ?>" href="<?php echo $sort_date_added; ?>">
                                            <?php echo $column_date_added; ?>
                                        </a>
                                        <?php } else { ?>
                                        <a href="<?php echo $sort_date_added; ?>">
                                            <?php echo $column_date_added; ?>
                                        </a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'o.date_modified') { ?>
                                        <a class="<?php echo strtolower($order); ?>" href="<?php echo $sort_date_modified; ?>">
                                            <?php echo $column_date_modified; ?>
                                        </a>
                                        <?php } else { ?>
                                        <a href="<?php echo $sort_date_modified; ?>">
                                            <?php echo $column_date_modified; ?>
                                        </a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-right">
                                        <?php echo $column_action; ?>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($orders) { ?>
                                <?php foreach ($orders as $order) { ?>
                                <tr>
                                    <td class="text-center">
                                        <?php if (in_array($order['order_id'], $selected)) { ?>
                                        <input checked="checked" name="selected[]" type="checkbox" value="<?php echo $order['order_id']; ?>"/>
                                        <?php } else { ?>
                                        <input name="selected[]" type="checkbox" value="<?php echo $order['order_id']; ?>"/>
                                        <?php } ?>
                                        <input name="shipping_code[]" type="hidden" value="<?php echo $order['shipping_code']; ?>"/>
                                    </td>
                                    <td class="text-right">
                                        <?php echo $order['order_id']; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo $order['customer']; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo $order['code']; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo $order['order_recur']; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo $order['order_status']; ?>
                                    </td>
                                    <td class="text-right">
                                        <?php echo $order['total']; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo $order['date_added']; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo $order['date_modified']; ?>
                                    </td>
                                    <td class="text-right">
                                        <a class="btn btn-info" data-toggle="tooltip" href="<?php echo $order['view']; ?>" title="<?php echo $button_view; ?>">
                                            <i class="fa fa-eye">
                                            </i>
                                        </a>
                                        <a class="btn btn-primary" data-toggle="tooltip" href="<?php echo $order['edit']; ?>" title="<?php echo $button_edit; ?>">
                                            <i class="fa fa-pencil">
                                            </i>
                                        </a>
                                    </td>
                                </tr>
                                <?php } ?>
                                <?php } else { ?>
                                <tr>
                                    <td class="text-center" colspan="8">
                                        <?php echo $text_no_results; ?>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left">
                        <?php echo $pagination; ?>
                    </div>
                    <div class="col-sm-6 text-right">
                        <?php echo $results; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        <!--
      $('#button-filter').on('click', function() {
        url = 'index.php?route=sale/order&token=<?php echo $token; ?>';
      
        var filter_order_id = $('input[name=\'filter_order_id\']').val();
      
        if (filter_order_id) {
            url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
        }
      
        var filter_customer = $('input[name=\'filter_customer\']').val();
      
        if (filter_customer) {
            url += '&filter_customer=' + encodeURIComponent(filter_customer);
        }
        
        var filter_order_recur = $('select[name=\'filter_order_recur\']').find(":selected").val();
        if (filter_order_recur != '') {
              
            url += '&filter_order_recur=' + encodeURIComponent(filter_order_recur);
        } 

         
      
        var filter_order_status = $('select[name=\'filter_order_status\']').val();
         
        if (filter_order_status != '*') {
              
            url += '&filter_order_status=' + encodeURIComponent(filter_order_status);
        }
      
        var filter_total = $('input[name=\'filter_total\']').val();
      
        if (filter_total) {
            url += '&filter_total=' + encodeURIComponent(filter_total);
        }
      
        var filter_date_added = $('input[name=\'filter_date_added\']').val();
      
        if (filter_date_added) {
            url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
        }
      
        var filter_date_modified = $('input[name=\'filter_date_modified\']').val();
      
        if (filter_date_modified) {
            url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
        }
      
        location = url;
      });
      //-->
    </script>
    <script type="text/javascript">
        <!--
      $('input[name=\'filter_customer\']').autocomplete({
        'source': function(request, response) {
            $.ajax({
                url: 'index.php?route=customer/customer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item['name'],
                            value: item['customer_id']
                        }
                    }));
                }
            });
        },
        'select': function(item) {
            $('input[name=\'filter_customer\']').val(item['label']);
        }
      });
      //-->
    </script>
    <script type="text/javascript">
        <!--
      $('input[name^=\'selected\']').on('change', function() {
        //$('#button-shipping, #button-invoice').prop('disabled', true);
      
        var selected = $('input[name^=\'selected\']:checked');
      
        if (selected.length) {
            $('#button-invoice').prop('disabled', false);
        }
      
        for (i = 0; i < selected.length; i++) {
            if ($(selected[i]).parent().find('input[name^=\'shipping_code\']').val()) {
                $('#button-shipping').prop('disabled', false);
      
                break;
            }
        }
      });
      
      //$('#button-shipping, #button-invoice').prop('disabled', true);
      
      $('input[name^=\'selected\']:first').trigger('change');
      
      // IE and Edge fix!
      $('#button-shipping, #button-invoice').on('click', function(e) {
        $('#form-order').attr('action', this.getAttribute('formAction'));
      });
      
      $('#button-delete').on('click', function(e) {
        $('#form-order').attr('action', this.getAttribute('formAction'));
        
        if (confirm('<?php echo $text_confirm; ?>')) {
            $('#form-order').submit();
        } else {
            return false;
        }
      });
      //-->
    </script>
    <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript">
    </script>
    <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" media="screen" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">
        <!--
      $('.date').datetimepicker({
        pickTime: false
      });
      //-->
    </script>
</div>
<?php echo $footer; ?>
