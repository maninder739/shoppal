<?php echo $header; ?><?php echo $column_left; ?>

<div id="content">

  <div class="page-header">

    <div class="container-fluid">
	<div class="pull-right">
		 <a href="<?php echo $export; ?>" class="btn btn-info" target="_blank" style="color: #eff2f4;"><?php echo $export_excel; ?></a>
	</div>

      <h1><?php echo $heading_title; ?></h1>

      <ul class="breadcrumb">

        <?php foreach ($breadcrumbs as $breadcrumb) { ?>

        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

        <?php } ?>

      </ul>

    </div>

  </div>

  <div class="container-fluid">

    <div class="panel panel-default">

      <div class="panel-heading">

        <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>

      </div>

      <div class="panel-body">

        <div class="table-responsive">

          <table class="table table-bordered">

            <thead>

			  <tr>

                <!--<td class="text-left"><?php echo $column_product_name;?></td>

                <td class="text-left"><?php echo $column_quantity;?></td>-->
				<td class="text-left"><?php if ($sort == 'product_description') { ?>

                    <a href="<?php echo $product_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_product_name; ?></a>

                    <?php } else { ?>

                    <a href="<?php echo $product_name; ?>"><?php echo $column_product_name; ?></a>

                    <?php } ?></td>
					<td class="text-left"><?php if ($sort == 'product') { ?>

                    <a href="<?php echo $quantity; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_quantity; ?></a>

                    <?php } else { ?>

                    <a href="<?php echo $quantity; ?>"><?php echo $column_quantity; ?></a>

                    <?php } ?></td>

              </tr>

            </thead>

            <tbody>

				<?php foreach($stock_details as $stock_detail){?>

					<tr>

						<!--<td><?php echo $stock_detail['name'];?></td>

						<td><?php echo $stock_detail['quantity'];?></td>-->
						<td><?php echo $stock_detail['product_description'];?></td>

						<td><?php echo $stock_detail['product'];?></td>

					</tr>

				<?php } ?>

			</tbody>

          </table>

		</div>

        <div class="row">

          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>

          <div class="col-sm-6 text-right"><?php echo $results; ?></div>

        </div>

      </div>

    </div>

  </div>

 </div>

<?php echo $footer; ?>