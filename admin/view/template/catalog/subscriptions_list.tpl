<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-subscriptions').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-subscriptions">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
					<thead>
						<tr>
							<td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
							<td class="center"><?php echo $column_subscription_id; ?></td>
							<td class="center"><?php echo $column_trial_terms; ?></td>
							<td class="center"><?php echo $column_trial_status; ?></td>
							<td class="left"><?php echo $column_subscription; ?></td>
							<td class="left"><?php echo $column_specific_day; ?></td>
							<td class="center"><?php echo $column_discount; ?></td>
							<td class="left"><?php echo $column_customer_groups; ?></td>
							<td class="center"><?php echo $column_status; ?></td>
							<td class="center"><?php echo $column_action; ?></td>
						</tr>
					</thead>
					<tbody>
						<?php if ($subscriptions) { ?>
							<?php foreach ($subscriptions as $subscription) { ?>
								<tr>
									<td style="text-align: center;">
										<?php if ($subscription['selected']) { ?>
											<input type="checkbox" name="selected[]" value="<?php echo $subscription['subscription_id']; ?>" checked="checked" />
										<?php } else { ?>
											<input type="checkbox" name="selected[]" value="<?php echo $subscription['subscription_id']; ?>" />
										<?php } ?>
									</td>
									<td class="center"><?php echo $subscription['subscription_id']; ?></td>
									<td class="center"><?php echo $subscription['trial_terms']; ?></td>
									<td class="center"><?php echo $subscription['trial_status']; ?></td>
									<td class="left"><?php echo $subscription['subscription']; ?></td>
									<td class="left"><?php echo $subscription['specific_day']; ?></td>
									<td class="center">
										<?php if ($subscription['discount']) { ?>
											<?php if ($subscription['discount_type'] == "Percent") { ?>
												<?php echo $subscription['discount']; ?>&nbsp;%
											<?php } else { ?>
												<?php echo $subscription['discount']; ?>
											<?php } ?>
										<?php } ?>
									</td>
									<td class="left"><?php echo $subscription['customer_groups']; ?></td>
									<td class="center">
										<?php if ($subscription['status']) { ?>
											<?php echo $text_enabled; ?>
										<?php } else { ?>
											<?php echo $text_disabled; ?>
										<?php } ?>
									</td>
									 <td class="center">
										<?php foreach ($subscription['action'] as $action) { ?>
										<a href="<?php echo $action['href']; ?>" data-toggle="tooltip" title="<?php echo $action['text']; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
										<?php } ?>
									</td>
								</tr>
							<?php } ?>
						<?php } else { ?>
							<tr>
								<td class="text-center" colspan="10"><?php echo $text_no_results; ?></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">

	$(document).ready(function() {


	});
	
</script>
<?php echo $footer; ?>