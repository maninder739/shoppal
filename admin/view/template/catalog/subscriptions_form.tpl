<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-subscriptions" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
	  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-subscriptions" class="form-horizontal">
         <form onsubmit="$('select[name=\'specific_day\']').prop('disabled', false);" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
				<legend>Subscription Details</legend>
				<div class="alert alert-info"><?php echo $help_recurring_help; ?></div>
				<fieldset>
				<div class="form-group">
				  <label class="col-sm-2 control-label" for="input-cycle"><?php echo $entry_cycle; ?></span></label>
				  <div class="col-sm-10">
					<input type="text" name="cycle" value="<?php echo $cycle; ?>" class="form-control" />
				  </div>
				</div>
				<div class="form-group">
				<label class="col-sm-2 control-label" for="input-frequency"><?php echo $entry_frequency; ?></label>
				<div class="col-sm-10">
				  <select name="frequency" class="form-control">
					<?php foreach ($frequencies as $key => $value) { ?>
						<?php if ($frequency == $key) { ?>
							<option value="<?php echo $key ?>" selected="selected"><?php echo $value; ?></option>
						<?php } else { ?>
							<option value="<?php echo $key ?>"><?php echo $value; ?></option>
						<?php } ?>
					<?php } ?>
				  </select>
				</div>
				</div>
				<div class="form-group">
				  <label class="col-sm-2 control-label" for="input-cycle"><?php echo $entry_duration; ?></span></label>
				  <div class="col-sm-10">
					<input type="text" name="duration" value="<?php echo $duration; ?>" class="form-control" />
				  </div>
				</div>
				<div class="form-group">
				<label class="col-sm-2 control-label" for="input-frequency"><span data-toggle="tooltip" title="<?php echo $help_specific_day; ?>"><?php echo $entry_specific_day; ?></span></label>
				<div class="col-sm-10">
				  <select name="specific_day" class="form-control">
					<?php foreach ($days as $day) { ?>
						<?php if ($day['value'] == $specific_day) { ?>
							<option value="<?php echo $day['value']; ?>" selected="selected"><?php echo $day['name']; ?></option>
						<?php } else { ?>
							<option value="<?php echo $day['value']; ?>"><?php echo $day['name']; ?></option>
						<?php } ?>
					<?php } ?>
				  </select>
				</div>
				</div>
				<div class="form-group">
				<label class="col-sm-2 control-label" for="input-discount"><?php echo $entry_discount; ?></label>
				<div class="col-sm-5">
					<input type="text" name="discount" id="input-discount" value="<?php echo $discount; ?>" class="form-control" />
				</div>
				<div class="col-sm-2">
					<select name="discount_type" class="form-control">
						<?php if ($discount_type == "Percent") { ?>
							<option value="Percent" selected="selected"><?php echo $text_percent; ?></option>
							<option value="Amount"><?php echo $text_amount; ?></option>
						<?php } else { ?>
							<option value="Percent"><?php echo $text_percent; ?></option>
							<option value="Amount" selected="selected"><?php echo $text_amount; ?></option>
						<?php } ?>
					</select>
				</div>
				</div>
				<div class="form-group">
				<label class="col-sm-2 control-label" for="input-grace-period"><span data-toggle="tooltip" title="<?php echo $help_grace_period; ?>"><?php echo $entry_grace_period; ?></span></label>
				<div class="col-sm-5">
					<input type="text" name="grace_period" id="input-grace-period" value="<?php echo $grace_period; ?>" class="form-control" />
				</div>
				</div>
				<div class="form-group">
				<label class="col-sm-2 control-label" for="input-reminder-count"><span data-toggle="tooltip" title="<?php echo $help_reminder_count; ?>"><?php echo $entry_reminder_count; ?></span></label>
				<div class="col-sm-5">
					<input type="text" name="reminder_count" id="input-reminder-count" value="<?php echo $reminder_count; ?>" class="form-control" />
				</div>
				</div>
				<div class="form-group">
				<label class="col-sm-2 control-label" for="input-reminder-period"><span data-toggle="tooltip" title="<?php echo $help_reminder_period; ?>"><?php echo $entry_reminder_period; ?></span></label>
				<div class="col-sm-5">
					<input type="text" name="reminder_period" id="input-reminder-period" value="<?php echo $reminder_count; ?>" class="form-control" />
				</div>
				</div>
				<div class="form-group">
				<label class="col-sm-2 control-label" for="input-reinstatement-fee"><span data-toggle="tooltip" title="<?php echo $help_reinstatement_fee; ?>"><?php echo $entry_reinstatement_fee; ?></span></label>
				<div class="col-sm-5">
					<input type="text" name="reinstatement_fee" id="input-reinstatement-fee" value="<?php echo $reinstatement_fee; ?>" class="form-control" />
				</div>
				</div>
				<div class="form-group">
				<label class="col-sm-2 control-label" for="input-reinstatement-period"><span data-toggle="tooltip" title="<?php echo $help_reinstatement_period; ?>"><?php echo $entry_reinstatement_period; ?></span></label>
				<div class="col-sm-5">
					<input type="text" name="reinstatement_period" id="input-reinstatement-period" value="<?php echo $reinstatement_period; ?>" class="form-control" />
				</div>
				</div>
				<div class="form-group">
				<label class="col-sm-2 control-label" for="input-include-totals"><span data-toggle="tooltip" title="<?php echo $help_include_totals; ?>"><?php echo $entry_include_totals; ?></span></label>
				<div class="col-sm-10">
				  <div class="checkbox">
					<label>
					  <?php if ($include_taxes) { ?>
							<input type="checkbox" name="include_taxes" value="1" checked="checked" />
						<?php } else { ?>
							<input type="checkbox" name="include_taxes" value="1" />
						<?php } ?><?php echo $text_taxes; ?>
						<?php if ($include_shipping) { ?>
							<input style="margin-left: 10px;" type="checkbox" name="include_shipping" value="1" checked="checked" />
						<?php } else { ?>
							<input style="margin-left: 10px;" type="checkbox" name="include_shipping" value="1" />
						<?php } ?><?php echo $text_shipping; ?>
					  &nbsp; </label>
					</div>
				  </div>
				</div>
				<div class="form-group">
				  <label class="col-sm-2 control-label"><?php echo $entry_customer_groups; ?></label>
					<div class="col-sm-10">
						<?php foreach ($customer_groups as $customer_group) { ?>
							<?php if (!empty($allowed_customer_groups)) { ?>
								<?php if (in_array($customer_group['customer_group_id'], $allowed_customer_groups)) { ?>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="allowed_customer_groups[]" value="<?php echo $customer_group['customer_group_id']; ?>" checked="checked" />
											<?php echo $customer_group['name']; ?>
										</label>
									</div>
								<?php } else { ?>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="allowed_customer_groups[]" value="<?php echo $customer_group['customer_group_id']; ?>" />
											<?php echo $customer_group['name']; ?>
										</label>
									</div>
								<?php } ?>
							<?php } else { ?>
								<div class="checkbox">
									<label>
										<input type="checkbox" name="allowed_customer_groups[]" value="<?php echo $customer_group['customer_group_id']; ?>" />
										<?php echo $customer_group['name']; ?>
									</label>
								</div>
							<?php } ?>
						<?php } ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
					<div class="col-sm-10">
						<select name="status" id="input-status" class="form-control">
							<?php if ($status) { ?>
								<option value="0"><?php echo $text_disabled; ?></option>
								<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
							<?php } else { ?>
								<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
								<option value="1"><?php echo $text_enabled; ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
			</fieldset>
			<div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
            <div class="col-sm-10">
              <input type="text" name="sort_order" value="<?php echo $sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control"/>
            </div>
          </div>
		  <fieldset>
			<legend>Trial Period</legend>
			<div class="alert alert-info"><?php echo $help_trial_help; ?></div>
			<div class="form-group">
			<label class="col-sm-2 control-label" for="input-trial"><?php echo $entry_trial; ?></label>
			<div class="col-sm-10">
			  <div class="checkbox">
				<label>
				  <?php if ($trial) { ?>
				  <input type="checkbox" name="trial" value="1" checked="checked" id="input-trial" />
				  <?php } else { ?>
				  <input type="checkbox" name="trial" value="1" id="input-trial" />
				  <?php } ?>
				  &nbsp; </label>
				</div>
			  </div>
			</div>
			<div class="form-group">
			<label class="col-sm-2 control-label" for="input-trial-only"><span data-toggle="tooltip" title="<?php echo $help_trial_only; ?>"><?php echo $entry_trial_only; ?></span></label>
			<div class="col-sm-10">
			  <div class="checkbox">
				<label>
				  <?php if ($trial_only) { ?>
				  <input type="checkbox" name="trial_only" value="1" checked="checked" id="input-trial" />
				  <?php } else { ?>
				  <input type="checkbox" name="trial_only" value="1" id="input-trial" />
				  <?php } ?>
				  &nbsp; </label>
				</div>
			  </div>
			</div>
			<div class="form-group">
			  <label class="col-sm-2 control-label" for="input-trial-duration"><?php echo $entry_trial_duration; ?></span></label>
			  <div class="col-sm-10">
				<input type="text" name="trial_duration" value="<?php echo $trial_duration; ?>" class="form-control" />
			  </div>
			</div>
			<div class="form-group">
			  <label class="col-sm-2 control-label" for="input-trial-cycle"><?php echo $entry_trial_cycle; ?></span></label>
			  <div class="col-sm-10">
				<input type="text" name="trial_cycle" value="<?php echo $trial_cycle; ?>" class="form-control" />
			  </div>
			</div>
			<div class="form-group">
			<label class="col-sm-2 control-label" for="input-trial-frequency"><?php echo $entry_trial_frequency; ?></label>
			<div class="col-sm-10">
			  <select name="trial_frequency" class="form-control">
				<?php foreach ($frequencies as $key => $value) { ?>
					<?php if ($trial_frequency == $key) { ?>
						<option value="<?php echo $key ?>" selected="selected"><?php echo $value; ?></option>
					<?php } else { ?>
						<option value="<?php echo $key ?>"><?php echo $value; ?></option>
					<?php } ?>
				<?php } ?>
			  </select>
			</div>
			</div>
			<div class="form-group">
			  <label class="col-sm-2 control-label" for="input-trial-price"><?php echo $entry_trial_price; ?></span></label>
			  <div class="col-sm-10">
				<input type="text" name="trial_price" value="<?php echo $trial_price; ?>" class="form-control" />
			  </div>
			</div>
			<div class="form-group">
			<label class="col-sm-2 control-label" for="input-trial-status"><?php echo $entry_trial_status; ?></label>
			<div class="col-sm-10">
			  <select name="trial_status" class="form-control">
				<?php if ($trial_status) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
				<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
				<?php } ?>
			  </select>
			</div>
			</div>
			</fieldset>
        </form>
      </div>
    </div>
  </div>
<script type="text/javascript">
	$(document).ready(function() {
		if ($('select[name=\'frequency\']').val() == 'week') {
			$('select[name=\'specific_day\']').prop('disabled', false);
		} else {
			$('select[name=\'specific_day\']').prop('disabled', true);
			$('select[name=\'specific_day\']').val('');
		}
		$('select[name=\'frequency\']').on('change', function() {
			if ($('select[name=\'frequency\']').val() == 'week') {
				$('select[name=\'specific_day\']').prop('disabled', false);
			} else {
				$('select[name=\'specific_day\']').prop('disabled', true);
				$('select[name=\'specific_day\']').val('');
			}
		});
	z});
</script>
<?php echo $footer; ?>
