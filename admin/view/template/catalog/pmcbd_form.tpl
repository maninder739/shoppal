<?php echo $header; ?><?php echo $column_left; ?>

<div id="content">
  <script type="text/javascript"> 
function buyproductloadajax(rowid) {
//alert(grpid  +'-'+ rowid) ;
	$('input[name=\'combo_setting['+rowid+'][product]\']').autocomplete({
		source: function(request, response) {
			$.ajax({
				url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
				dataType: 'json',
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item['name'],
							value: item['product_id']
						}
					}));
				}
			});
		},
		select: function(item) {
			$('input[name=\'combo_setting[pmcbd]['+rowid+'][product]\']').val('');
			
			$('#bundleproduct-'+rowid + item['value']).remove();
			
			$('#bundleproduct-'+rowid).append('<div id="bundleproduct-'+rowid+'-' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="combo_setting['+rowid+'][product][]" value="' + item['value'] + '" /></div>');	
		}
	});
		
	$('#bundleproduct-'+rowid).delegate('.fa-minus-circle', 'click', function() {
		$(this).parent().remove();
	});
} 
</script>
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-pmcbd" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
		<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-pmcbd" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-target_product"><?php echo $entry_target_product; ?></label>
            <div class="col-sm-10">
              <input type="text" name="target_product" value="" placeholder="<?php echo $entry_target_product; ?>" id="input-target_product" class="form-control" />
              <div id="target_product" class="well well-sm" style="height: 150px; overflow: auto;">
                <?php if ($target_product_data) { ?>
                <div id="target_product<?php echo $target_product_data['product_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $target_product_data['name']; ?>
                  <input type="hidden" name="target_product_id" value="<?php echo $target_product_id; ?>" />
                </div>
                <?php } ?>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="status" id="input-status" class="form-control">
                <?php if ($status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-limitset"><?php echo $entry_limitset; ?></label>
            <div class="col-sm-10">
              <input type="text" name="limitset" id="input-limitset" class="form-control" value="<?php echo $limitset; ?>"  />
            </div>
          </div>
          <div class="table-responsive">
            <table id="pmcbd" class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <td class="text-left" style="width:22%"><?php echo $entry_tabtitle; ?></td>
                  <td class="text-left" style="width:22%"><?php echo $entry_combo_product; ?></td>
                  <td class="text-left" style="width:22%"><?php echo $entry_store; ?></td>
				  <td class="text-left" style="width:22%"><?php echo $entry_customer_group; ?></td>                  
                  <td class="text-left" style="width:22%"><?php echo $entry_discount; ?></td>
                  <td></td>
                </tr>
              </thead>
              <tbody>
                <?php $row = 0; ?>
                <?php if(isset($combo_setting)) { ?>
                <?php foreach ($combo_setting as $pmcbdstng) { ?>
                <script language="javascript">
				$( document ).ready(function() {
					buyproductloadajax(<?php echo $row; ?>);
				});
				</script>
                <tr id="pmcbdstng-row-<?php echo $row; ?>">
				  <td class="text-left" style="width:22%">
					  <?php foreach ($languages as $language) { 
					if(substr(VERSION,0,3)=='2.3' || substr(VERSION,0,3)=='2.2') {
						$lagimgsrc = 'language/'. $language['code'] .'/'. $language['code']. '.png';
					} else {
						$lagimgsrc = 'view/image/flags/' . $language['image'];
					} ?>
                    <div class="input-group pull-left"><span class="input-group-addon"><img src="<?php echo $lagimgsrc; ?>" /> </span>
                      <input type="text" name="combo_setting[<?php echo $row; ?>][tabtitle][<?php echo $language['language_id']; ?>]" value="<?php echo $pmcbdstng['tabtitle'][$language['language_id']];?>" placeholder="<?php echo $entry_tabtitle; ?>" id="input-tabtitle-<?php echo $row; ?>" class="form-control" />
                    </div>
                    <?php } ?>
                  </td>
                  <td class="text-left" style="width:22%"><input type="text" name="combo_setting[<?php echo $row; ?>][product]" value="" placeholder="<?php echo $entry_combo_product; ?>" id="input-prod-<?php echo $row; ?>" class="form-control" />
                    <div id="bundleproduct-<?php echo $row; ?>" class="well well-sm" style="height: 150px; overflow: auto;">
                      <?php if(isset($pmcbdstng['product']) && $pmcbdstng['product']) { foreach ($pmcbdstng['product'] as $product) { ?>
                      <div id="bundleproduct-<?php echo $row; ?>-<?php echo $product['product_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product['name']; ?>
                        <input type="hidden" name="combo_setting[<?php echo $row; ?>][product][]" value="<?php echo $product['product_id']; ?>" />
                      </div>
                      <?php } } ?>
                    </div></td>
 					 
 					<td class="text-left">
					  <div class="well well-sm" style="height: 100px; overflow: auto;margin:0">
						<div class="checkbox">
						  <label>
							<?php if (isset($pmcbdstng['store']) && in_array(0, $pmcbdstng['store'])) { ?>
							<input type="checkbox" name="combo_setting[<?php echo $row;?>][store][]" value="0" checked="checked" />
							<?php echo $text_default; ?>
							<?php } else { ?>
							<input type="checkbox" name="combo_setting[<?php echo $row;?>][store][]" value="0" />
							<?php echo $text_default; ?>
							<?php } ?>
						  </label>
						</div>
						<?php foreach ($stores as $store) { ?>
						<div class="checkbox">
						  <label>
							<?php if (isset($pmcbdstng['store']) && in_array($store['store_id'], $pmcbdstng['store'])) { ?>
							<input type="checkbox" name="combo_setting[<?php echo $row;?>][store][]" value="<?php echo $store['store_id']; ?>" checked="checked" />
							<?php echo $store['name']; ?>
							<?php } else { ?>
							<input type="checkbox" name="combo_setting[<?php echo $row;?>][store][]" value="<?php echo $store['store_id']; ?>" />
							<?php echo $store['name']; ?>
							<?php } ?>
						  </label>
						</div>
						<?php } ?>
					  </div>
				  	</td>
				 
				 
 					<td class="text-left">
					 <div class="well well-sm" style="height: 100px; overflow: auto;margin:0">
 						<?php foreach ($customer_group as $cgp) { ?>
						<div class="checkbox">
						  <label>
							<?php if (isset($pmcbdstng['customer_group']) &&  in_array($cgp['customer_group_id'], $pmcbdstng['customer_group'])) { ?>
							<input type="checkbox" name="combo_setting[<?php echo $row;?>][customer_group][]" value="<?php echo $cgp['customer_group_id']; ?>" checked="checked" />
							<?php echo $cgp['name']; ?>
							<?php } else { ?>
							<input type="checkbox" name="combo_setting[<?php echo $row;?>][customer_group][]" value="<?php echo $cgp['customer_group_id']; ?>" />
							<?php echo $cgp['name']; ?>
							<?php } ?>
						  </label>
						</div>
						<?php } ?>
					  </div>
				  	</td>
				 
                  <td class="text-left" style="width:22%">
				  	<select name="combo_setting[<?php echo $row; ?>][disctype]" id="input-disctype-<?php echo $row; ?>" class="form-control">
                      <option value="P" <?php echo ($pmcbdstng['disctype'] == 'P') ? 'selected' : '';?> >Percentage</option>
                      <option value="F" <?php echo ($pmcbdstng['disctype'] == 'F') ? 'selected' : '';?> >Fixed</option>
                    </select>
					<input type="text" name="combo_setting[<?php echo $row; ?>][discount]" value="<?php echo $pmcbdstng['discount'];?>" placeholder="<?php echo $entry_discount; ?>" id="input-discount-<?php echo $row; ?>" class="form-control" />
                  </td>
                  
                  <td class="text-left"><button type="button" onclick="$('#pmcbdstng-row-<?php echo $row; ?>').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                </tr>
                <?php $row++; ?>
                <?php } ?>
                <?php } ?>
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="5"></td>
                  <td class="text-left"><button type="button" onclick="addComboDiscount();" data-toggle="tooltip" title="Add New" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                </tr>
              </tfoot>
            </table>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
var row = <?php echo $row; ?>;
function addComboDiscount() {
 	
	html  = '<tr id="pmcbdstng-row-' + row + '">'; 
	
	html += '<td class="text-left" style="width:22%">';
 	<?php foreach ($languages as $language) { 
		if(substr(VERSION,0,3)=='2.3' || substr(VERSION,0,3)=='2.2') {
			$lagimgsrc = 'language/'. $language['code'] .'/'. $language['code']. '.png';
		} else {
			$lagimgsrc = 'view/image/flags/' . $language['image'];
		} ?>
         html += '<div style="width:100%" class="input-group pull-left"><span class="input-group-addon"><img src="<?php echo $lagimgsrc; ?>"/> </span>';
		html += '<input type="text" name="combo_setting[' + row + '][tabtitle][<?php echo $language['language_id']; ?>]" placeholder="<?php echo $entry_tabtitle; ?>" id="input-tabtitle-' + row + '" class="form-control" /></div>';
	<?php } ?>
	html += ' </td>';
  	
    html += '  <td class="text-left" style="width:22%"><input type="text" name="combo_setting[' + row + '][product]" value="" placeholder="<?php echo $entry_combo_product; ?>" id="input-prod-' + row + '" class="form-control" /><div id="bundleproduct-' + row + '" class="well well-sm" style="height: 150px; overflow: auto;"></div></td>';
	
	html += '  <td class="text-left" style="width:22%"><div class="well well-sm" style="height: 100px; overflow: auto;margin:0">';
	html += '  <div class="checkbox"><label><input type="checkbox" name="combo_setting[' + row + '][store][]" value="0" /><?php echo $text_default; ?></label></div>';
	<?php foreach ($stores as $store) { ?>
	html += '  <div class="checkbox"><label><input type="checkbox" name="combo_setting[' + row + '][store][]" value="<?php echo $store['store_id']; ?>" /><?php echo $store['name']; ?></label></div>';
	<?php } ?>
	html += '  </div></td>';
	
	html += '  <td class="text-left" style="width:22%"><div class="well well-sm" style="height: 100px; overflow: auto;margin:0">';
 	<?php foreach ($customer_group as $cgp) { ?>
	html += '  <div class="checkbox"><label><input type="checkbox" name="combo_setting[' + row + '][customer_group][]" value="<?php echo $cgp['customer_group_id']; ?>" /><?php echo $cgp['name']; ?></label></div>';
	<?php } ?>
	html += '  </div></td>'; 
  	
 	html += '  <td class="text-left" style="width:22%"><select name="combo_setting[' + row + '][disctype]" id="input-disctype-' + row + '" class="form-control" >';
		html += '<option value="P">Percentage</option>';
		html += '<option value="F">Fixed</option>';
  	html += ' </select><input type="text" name="combo_setting[' + row + '][discount]" value="" placeholder="<?php echo $entry_discount; ?>" id="input-discount-' + row + '" class="form-control" /></td>';
      
	html += '  <td class="text-left"><button type="button" onclick="$(\'#pmcbdstng-row-' + row + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
	html += '</tr>';
	
	$('#pmcbd tbody').append(html);
	
	buyproductloadajax(row);
  
	row++;
}
//--></script>
<script type="text/javascript"><!--
$('input[name=\'target_product\']').autocomplete({
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	select: function(item) {
		$('input[name=\'target_product\']').val('');
		
		$('#target_product' + item['value']).remove();
		
		$('#target_product').html('<div id="target_product' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="target_product_id" value="' + item['value'] + '" /></div>');	
	}
});
	
$('#target_product').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});
//--></script>
<?php echo $footer; ?>