<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button id="save-button" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      	<i class="fa fa-code pull-right text-muted" data-toggle="modal" data-target="#settingsModal"></i>
      </div>
      <div class="panel-body">
       <form method="post" enctype="multipart/form-data" id="form-fb-marketing" class="form-horizontal">
          <!-- Navigation Buttons -->
          <div class="col-md-2">
			<ul class="nav nav-pills nav-stacked" id="configTabs">
				<li class="active"><a href="#pixel-config" data-toggle="pill" ><?php echo $text_pixel_configuration; ?></a></li>
				<li><a href="#advanced_matching_data" data-toggle="pill"><?php echo $text_advanced_matching_data; ?></a></li>
				<li class="hidden"><a href="#marketing-api" data-toggle="pill"><?php echo $text_marketing_api; ?></a></li>
				<li><a href="#about" data-toggle="pill"><?php echo $text_about; ?></a></li>
				<li><a href="#pro-version" data-toggle="pill"><?php echo $text_pro_version; ?></a></li>
			</ul>
		  </div>
          <!-- Content -->
		  <div class="col-md-10">
			<div class="tab-content">
          		<div class="tab-pane fade in active" id="pixel-config">
          		  <ul class="nav nav-tabs">
					 <?php foreach ($stores as $store) { ?>
						 <li><a href="#tab-store<?php echo $store['store_id']; ?>" data-toggle="tab"><?php echo $store['name']; ?></a></li>
					<?php } ?>
				  </ul>
	 			  <div class="tab-content">	
	 			  		<?php if(!empty($_GET['debug'])) {
	 			  			ini_set('xdebug.var_display_max_depth', 5);
							ini_set('xdebug.var_display_max_children', 256);
							ini_set('xdebug.var_display_max_data', 1024);
							
							var_dump($fb_marketing_advanced_matching_data);
	 			  			echo '<br />';
	 			  			var_dump($fb_marketing_status);
	 			  			echo '<br />';
	 			  			var_dump($fb_marketing_pixel_id);
	 			  			echo '<br />';
	 			  			var_dump($fb_marketing_product_catalog_id);
	 			  			echo '<br />';
	 			  			var_dump($fb_marketing_advanced_matching);
	 			  			echo '<br />';
	 			  			var_dump($fb_marketing_events);
	 			  			echo '<br />';
	 			  		
	 			  		    }
	 			  		?>
					  <?php foreach ($stores as $store) { ?>
					  <div class="tab-pane fade" id="tab-store<?php echo $store['store_id']; ?>">
						  <input type="hidden" id="fb_marketing_version_<?php echo $store['store_id']; ?>" name="fb_marketing_version[<?php echo $store['store_id']; ?>]" value="<?php echo $fb_marketing_version[$store['store_id']]; ?>">
						  <div class="form-group">
							<label class="col-sm-2 control-label" for="fb-marketing-status<?php echo $store['store_id']; ?>"><?php echo $entry_status; ?></label>
							<div class="col-sm-10">
							 <input type="checkbox" <?php echo (!empty($fb_marketing_status[$store['store_id']]) ? 'checked="checked"' : '' ); ?> id="fb_marketing_status<?php echo $store['store_id']; ?>" value="<?php echo !empty($fb_marketing_status[$store['store_id']]) ? '1' : '0' ; ?>" name="fb_marketing_status[<?php echo $store['store_id']; ?>]" class="form-control" data-toggle="toggle" data-on="<?php echo $text_enabled; ?>" data-off="<?php echo $text_disabled; ?>" data-onstyle="success" data-offstyle="danger" data-size="large">
							</div>
						  </div>
						
						  <div class="form-group">
							<label for="fb_marketing_pixel_id_<?php echo $store['store_id']; ?>" class="col-sm-2 control-label"><span data-toggle="tooltip" data-html="true" data-delay="{"show":25,"hide":500}" title="<?php echo htmlspecialchars($help_pixel_id); ?>"><?php echo $entry_pixel_id; ?></span></label>
							<div class="col-sm-10">
							  <input type="text" id="fb_marketing_pixel_id_<?php echo $store['store_id']; ?>" name="fb_marketing_pixel_id[<?php echo $store['store_id']; ?>]" value="<?php echo $fb_marketing_pixel_id[$store['store_id']]; ?>" placeholder="<?php echo $entry_pixel_id; ?>" class="required form-control">
							</div>
						  </div>
						  <div class="form-group">
							<label for="fb_marketing_product_catalog_id_<?php echo $store['store_id']; ?>" class="col-sm-2 control-label"><span data-toggle="tooltip" data-html="true" data-delay="{"show":25,"hide":500}" title="<?php echo htmlspecialchars($help_product_catalog_id); ?>"><?php echo $entry_product_catalog_id; ?></span></label>
							<div class="col-sm-10">
							 <input type="text" id="fb_marketing_product_catalog_id_<?php echo $store['store_id']; ?>" name="fb_marketing_product_catalog_id[<?php echo $store['store_id']; ?>]" value="<?php echo $fb_marketing_product_catalog_id[$store['store_id']]; ?>" placeholder="<?php echo $entry_product_catalog_id; ?>" class="required form-control" >
							</div>
						  </div>
						  <div class="form-group">
							<label for="fb_marketing_advanced_matching_<?php echo $store['store_id']; ?>" class="col-sm-2 control-label"><span data-toggle="tooltip" data-html="true" data-delay="{"show":25,"hide":5000}" title="<?php echo htmlspecialchars($help_advanced_matching); ?>"><?php echo $entry_advanced_matching; ?></span></label>
							<div class="col-sm-10">
							 <input type="checkbox" <?php echo ($fb_marketing_advanced_matching[$store['store_id']] == 'on' ? 'checked="checked"' : '' ); ?> id="fb_marketing_advanced_matching_<?php echo $store['store_id']; ?>" name="fb_marketing_advanced_matching[<?php echo $store['store_id']; ?>]" class="form-control" data-toggle="toggle">
							</div>
						  </div>
						  <div class="form-group">
							<label for="fb_marketing_values_vat_inc_<?php echo $store['store_id']; ?>" class="col-sm-2 control-label"><span data-toggle="tooltip" data-html="true" data-delay="{"show":25,"hide":5000}" title="<?php echo htmlspecialchars($help_values_vat_inc); ?>"><?php echo $entry_values_vat_inc; ?></span></label>
							<div class="col-sm-10 pro-version-only">
							 <input type="checkbox" <?php echo ($fb_marketing_values_vat_inc[$store['store_id']] == 'on' ? 'checked="checked"' : '' ); ?> id="fb_marketing_values_vat_inc_<?php echo $store['store_id']; ?>" name="fb_marketing_values_vat_inc[<?php echo $store['store_id']; ?>]" class="form-control" disabled data-toggle="toggle">
							</div>
						  </div>
						  <div class="form-group">
							<label for="fb_marketing_manual_only_mode_<?php echo $store['store_id']; ?>" class="col-sm-2 control-label"><span data-toggle="tooltip" data-html="true" data-delay="{"show":25,"hide":5000}" title="<?php echo htmlspecialchars($help_manual_only_mode); ?>"><?php echo $entry_manual_only_mode; ?></span></label>
							<div class="col-sm-10 pro-version-only">
							 <input type="checkbox" <?php echo ($fb_marketing_manual_only_mode[$store['store_id']] == 'on' ? 'checked="checked"' : '' ); ?> id="fb_marketing_manual_only_mode_<?php echo $store['store_id']; ?>" name="fb_marketing_manual_only_mode[<?php echo $store['store_id']; ?>]" class="form-control" disabled data-toggle="toggle">
							</div>
						  </div>
						  <div class="form-group">
							<label for="fb_marketing_pid_<?php echo $store['store_id']; ?>" class="col-sm-2 control-label"><span data-toggle="tooltip" data-html="true" data-delay="{"show":25,"hide":5000}" title="<?php echo $help_pid; ?>"><?php echo $entry_pid; ?></span></label>
							<div class="col-sm-10 pro-version-only">
								 <select id="fb_marketing_pid_<?php echo $store['store_id']; ?>" name="fb_marketing_pid[<?php echo $store['store_id']; ?>]" class="form-control" disabled>
							 		<?php foreach ($pid_options as $key => $value) { ?>
							 			<option value="<?php echo $key; ?>" <?php echo  $key == $fb_marketing_pid[$store['store_id']] ? 'selected="selected"' : '' ; ?>><?php echo $value; ?></option>
							 		<?php } ?>
								 </select>
							 </div>
						  </div>
						  <div class="table-responsive">
						  <h3><?php echo $entry_events; ?></h3>
						  <table class="table table-condensed table-hover">
						  	<thead>
						  		<th><?php echo $text_status; ?></th>
						  		<th><?php echo $text_name; ?></th>
						  		<th><?php echo $text_path; ?></th>
						  		<th><?php echo $text_value; ?></th>
						  	</thead>
						  	<tbody>
							  <?php foreach($events as $event) { ?>
						  		<tr>
									<td>
									<?php if (!empty($fb_marketing_events[$store['store_id']][strtolower($event)]['status'])) { ?>
										<a  id="active-<?php echo strtolower($event); ?>_<?php echo $store['store_id']; ?>" class="btn btn-sm btn-success pro-version-only" data-toggle="tooltip" title="<?php echo $text_enabled; ?>">
										<i class="fa fa-minus-circle fa-rotate-90 fa-2x"></i>
										</a>
										<input name="fb_marketing_events[<?php echo $store['store_id']; ?>][<?php echo strtolower($event); ?>][status]" id="event-status-<?php echo strtolower($event); ?>_<?php echo $store['store_id']; ?>" type="hidden" value="1">
									<?php } ?>
									</td>
									<td ><?php echo $event; ?></td>
									<td >
										<input type="text" <?php $end = end($fb_marketing_events[$store['store_id']][strtolower($event)]['path']); echo empty($end) ? 'readonly' : '' ;?> value="<?php echo !empty($fb_marketing_events[$store['store_id']][strtolower($event)]['path'][0]) ? $fb_marketing_events[$store['store_id']][strtolower($event)]['path'][0] : '' ;?>" name="fb_marketing_events[<?php echo $store['store_id']; ?>][<?php echo strtolower($event); ?>][path]" placeholder="<?php echo $placeholder_path; ?>" class="form-control"></td>
									<td >
										<input type="text" <?php $end = end($fb_marketing_events[$store['store_id']][strtolower($event)]['value']); echo empty($end) ? 'readonly' : '' ;?> value="<?php echo !empty($fb_marketing_events[$store['store_id']][strtolower($event)]['value'][0]) ? $fb_marketing_events[$store['store_id']][strtolower($event)]['value'][0] : '' ;?>" name="fb_marketing_events[<?php echo $store['store_id']; ?>][<?php echo strtolower($event); ?>][value]" placeholder="<?php echo $placeholder_value; ?>" class="form-control pro-version-only"></td>						  		
						  		</tr>
							   <?php } ?>
							</tbody>
						  </table>
						</div> <!-- // .table-responsive -->
					  </div> <!-- // .tab-pane --> 
					  <?php } ?>
				  </div><!-- // .tab-content -->
				</div>
				<div class="tab-pane fade" id="advanced_matching_data">
					<h4>Advanced Matching with the Pixel</h4>
					<p>The Facebook pixel has an advanced matching feature that enables you to send your customer data through the pixel to match more website actions with Facebook users. With this additional data, you can report and optimize your ads for more conversions and build larger re-marketing audiences. You can pass the customer identifiers such as email, phone number that you collect from your website during the check-out, account sign-in or registration process as parameters in the pixel. Facebook will then use this information to match pixel events with Facebook users when the Facebook cookie is not present on the browser that fires the pixel.</p>
				
					<?php foreach($adv_match_types as $key => $value) { ?>
					    <label for="fb_marketing_advanced_matching_data_<?php echo $key; ?>" class="checkbox-inline btn btn-default pro-version-only">
						  <input type="checkbox" <?php echo (!empty($fb_marketing_advanced_matching_data[$key]) ? 'checked="checked"' : '' ); ?> id="fb_marketing_advanced_matching_data_<?php echo $key; ?>" name="fb_marketing_advanced_matching_data[<?php echo $key; ?>]" class="form-control" disabled data-toggle="toggle">
						  <?php echo $value; ?>
						</label>
					<?php } ?>
					<p class="hidden"><small><?php echo $text_ajax_save; ?></small></p>
				</div>
          		<div class="tab-pane fade" id="marketing-api">
          		</div>
          		<div class="tab-pane fade" id="about">
          			<p>To create your Facebook pixel:
						<ol>
						<li>Go to your Facebook Pixel tab in <a href="https://www.facebook.com/ads/manager/pixel/facebook_pixel/" target="_blank">Facebook Ads Manager</a>.</li>
						<li>Click <b>Create a Pixel</b>.</li>
						<li>Enter a name for your pixel. There's only one pixel per ad account, so choose a name that represents your business.</li>
						<li>Make sure you've checked the box to accept the terms.</li>
						<li>Click <b>Create Pixel</b>.</li>
						<li>Copy the Pixel ID and paste it here.</li>
						</ol>
						<a href="https://www.facebook.com/business/help/742478679120153" target="_blank">More</a>
					</p>
          		</div>
          		<div class="tab-pane fade" id="pro-version">
          			<p>This is a trimmed down version of the <a href="https://www.opencart.com/index.php?route=marketplace/extension/info&extension_id=31443" target="_blank">Facebook Pixel PRO</a> extension.<br/> 
          				The <a href="https://www.opencart.com/index.php?route=marketplace/extension/info&extension_id=31443" target="_blank">Facebook Pixel PRO</a> version offers extra functionality and configurability: 
          				<ul>
          					<li>Advanced Matching data identifiers can be enabled/disabled separately (includes City, State and Zip for more accuracy)</li>
          					<li>Manual Only Mode: disables the <b><i>SubscribedButtonClick</i></b> and <b><i>Microdata</i></b> event</li>
          					<li>Option to set Pixel event values to include VAT</li>
          					<li>Separately enabled/disable Pixel events </li>
          					<li>Specify a custom value for certain Pixel events: <b><i>PageviewSearch, Search, AddPaymentInfo, CompleteRegistration</i></b> </li>
          				</ul>
          				<a href="https://www.opencart.com/index.php?route=marketplace/extension/info&extension_id=31443" target="_blank">Purchase Facebook Pixel PRO version</a>
          			</p>
          		</div>
          	</div> <!-- // .tab-pane -->
          	</div> <!-- // .tab-content -->
		  </div> <!-- // Content -->
        </form>
    </div>
        <div class="panel-footer"><p>Facebook Pixel & Marketing - v<?php echo $version; ?> - <a href="http://jorimvanhove.com" target="_blank">Jorim van Hove</a> &copy; 2015 - <?php echo date('Y'); ?> - <a href="http://jorimvanhove.com/plugins/facebook-marketing/" target="_blank">Online documentation</a></p></div>
      </div>
    </div>
</div>

<div id="settings-modal"></div>

<script type="text/javascript"><!--
	/*
	$(function () {
  		$('[data-toggle="tooltip"]').tooltip(
  			trigger : 'click hover focus',
  			delay : { "show" : 25 , "hide" : 500 },
  			)
	})
	*/
<?php if ($permission) { ?>
	$('#save-button').click(function(){
		validateForm();
	});
<?php } else { ?>
	$('#save-button').click(function(){
		$('.alert').remove();
		
		$(".panel-default").before('<div class="alert alert-warning"><?php echo $error_permission; ?></div>');
	});
<?php } ?>
	
	$(function() {
		$('input[name^=\'fb_marketing_status\']').change(function() {
		  var value = $(this).val();
			if(value == 1) {
		 		$(this).val(0);
		 	} else {
		 		$(this).val(1);
		 	}
		})
		
		loadSettingsModal();
		
	})
	
	function loadSettingsModal() {
		$('#settings-modal').load('index.php?route=<?php echo $controller_path; ?>/loadSettingsModal&token=<?php echo $token; ?>');
	}
	
	function validateForm() {
		if ( $('input[name^="fb_marketing"]').val() === '' ) {
			$(this).after('<span class="error"> Please enter a value </span>');
		} else {
		 	saveData();
		}	
	}
	
	function saveOCEvent(row) {
		<?php if ($permission) { ?>
			$.ajax({
				url:'index.php?route=<?php echo $controller_path; ?>/saveOCEvent&token=<?php echo $token; ?>',
				type: 'post',
				dataType: 'json',
				data: { 
					trigger : $('#oc-event' + row + ' input[name="oc_event_trigger"]').val(),
					action :  $('#oc-event' + row + ' input[name="oc_event_action"]').val(),
				},
				success: function(json) {
					alertJson('alert alert-success', json);
				},
				error: function(json) {
					alertJson('alert alert-warning', json);
				}
			});
			return false;
	    <?php } else { ?>	    
	    	$('.alert').remove();
			$(".panel-default").before('<div class="alert alert-warning"><?php echo $error_permission; ?></div>');
		 <?php } ?>
	}
	
	function saveData() {
		$.ajax({
			url:'index.php?route=<?php echo $controller_path; ?>/saveSettings&token=<?php echo $token; ?>',
			type: 'post',
			dataType: 'json',
			data: { settings : $('#form-fb-marketing').serialize() },
			
			success: function(json) {
				alertJson('alert alert-success', json);
			},
			error: function(json) {
				alertJson('alert alert-warning', json);
			}
		});
		return false;
	}
	
	function saveAdvMatchingData(id) {
		$.ajax({
			url:'index.php?route=<?php echo $controller_path; ?>/toggleAdvMatchTypes&token=<?php echo $token; ?>',
			type: 'post',
			dataType: 'json',
			data: {
				id: id,
			},
			success: function(json) {
				alertJson('alert alert-success', json);
			},
			error: function(json) {
				alertJson('alert alert-warning', json);
			}
		});
	}
	
	function alertJson(action, json) {
		
		$('.alert').remove();
		
		if (json['success']) {
			$(".panel-default").before('<div class="' + action + '">' + json['success'] + '</div>');
		} else if (json['warning']) {
			$(".panel-default").before('<div class="' + action + '">' + json['warning'] + '</div>');
		}
		
	}
	
	function activate(row) {
		
		$.ajax({
			url:'index.php?route=<?php echo $controller_path; ?>/toggle&token=<?php echo $token; ?>',
			type: 'post',
			dataType: 'json',
			data: {
				row: row,
			},
			success: function(json) {
				alertJson('alert alert-success', json);
				$('#inactive-' + row).replaceWith('<a  id="active-' + row + '" onclick="<?php if ($permission) { ?>deactivate(\'' + row + '\');<?php } ?>" class="btn btn-sm btn-success" data-toggle="tooltip" title="<?php echo $text_enabled; ?>"><i class="fa fa-minus-circle fa-rotate-90 fa-2x"></i></a>');
				$('#event-status-' + row).val(1);
			},
			error: function(json) {
				alertJson('alert alert-warning', json);
			}
		});
	
	}
	
	function deactivate(row) {
		
		$.ajax({
			url:'index.php?route=<?php echo $controller_path; ?>/toggle&token=<?php echo $token; ?>',
			type: 'post',
			dataType: 'json',
			data: {
				row: row,
			},
			success: function(json) {
				alertJson('alert alert-success', json);
				$('#active-' + row).replaceWith('<a id="inactive-' + row + '" onclick="<?php if ($permission) { ?>activate(\'' + row + '\');<?php } ?>" class="btn btn-sm btn-danger" data-toggle="tooltip" title="<?php echo $text_disabled; ?>"><i class="fa fa-minus-circle fa-rotate-90 fa-2x"></i></a>');
				$('#event-status-' + row).val(0);
			},
			error: function(json) {
				alertJson('alert alert-warning', json);
			}
		});
	
	}
//--></script>
<script type="text/javascript"><!--
	$('#pixel-config a:first').tab('show');
	
	// Override popover so as to mark everything that uses a popover.
	var old_popover = $.fn.popover;
	function my_popover() {
	  this.addClass('has-popover');
	  return old_popover.apply(this, arguments);
	}
	$.fn.popover = my_popover;
	
	$(function() {
		$('.pro-version-only').popover({
		  html: true,
		  trigger: 'click',
		  title: "PRO version only",
		  content: "Changing this setting is only possible in the PRO version of the extension. You can purchase the PRO version here:<br/><a href=\"https://www.opencart.com/index.php?route=marketplace/extension/info&extension_id=31443\" target=\"_blank\">Facebook Pixel PRO</a>",
		  placement: 'auto'	
		}).hover(function(e) {
		  $(".has-popover").popover('hide');
		});
	});
//--></script></div>
<?php echo $footer; ?>