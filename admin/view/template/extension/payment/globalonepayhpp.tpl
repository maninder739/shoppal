<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } ?>
  </ul>
  

<div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-globalonepayhpp" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-globalonepayhpp" class="form-horizontal">
          <ul class="nav nav-tabs" id="tabs">
            <li class="active"><a href="#tab-account" data-toggle="tab"><?php echo $tab_account; ?></a></li>
            <li><a href="#tab-order-status" data-toggle="tab"><?php echo $tab_order_status; ?></a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-account">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="globalonepayhpp_status" id="input-status" class="form-control">
                <?php if ($globalonepayhpp_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
           <div class="form-group required">
            <label class="col-sm-2 control-label" for="globalonepayhpp_gateway"><?php echo $entry_gateway; ?></label>
            <div class="col-sm-10">
              <select name="globalonepayhpp_gateway" id="globalonepayhpp_gateway" class="form-control">
               	<?php if ($globalonepayhpp_gateway == 'globalonepay') { ?>
                <option value="globalonepay" selected="selected"><?php echo $text_extension_name; ?></option>
                <?php } else { ?>
                <option value="globalonepay"><?php echo $text_extension_name; ?></option>
                <?php } ?>
		
              </select>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="globalonepayhpp_test"><?php echo $entry_test; ?></label>
            <div class="col-sm-10">
              <select name="globalonepayhpp_test" id="globalonepayhpp_test" class="form-control">
                <?php if ($globalonepayhpp_test == '1') { ?>
                <option value="1" selected="selected"><?php echo $text_true; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_true; ?></option>
                <?php } ?>
                <?php if ($globalonepayhpp_test == '0') { ?>
                <option value="0" selected="selected"><?php echo $text_false; ?></option>
                <?php } else { ?>
                <option value="0"><?php echo $text_false; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="globalonepayhpp_currency1"><span data-toggle="tooltip" title="<?php echo $entry_currency1_tip; ?>"><?php echo $entry_currency1; ?></span></label>
            <div class="col-sm-10">
              <select name="globalonepayhpp_currency1" id="globalonepayhpp_currency1" class="form-control">
              <?php if ($globalonepayhpp_currency1 == 'EUR') { ?>
              <option value="EUR" selected="selected"><?php echo $currency_eur; ?></option>
              <?php } else { ?>
              <option value="EUR"><?php echo $currency_eur; ?></option>
              <?php } ?>
              <?php if ($globalonepayhpp_currency1 == 'GBP') { ?>
              <option value="GBP" selected="selected"><?php echo $currency_gbp; ?></option>
              <?php } else { ?>
              <option value="GBP"><?php echo $currency_gbp; ?></option>
              <?php } ?>
              <?php if ($globalonepayhpp_currency1 == 'USD') { ?>
              <option value="USD" selected="selected"><?php echo $currency_usd; ?></option>
              <?php } else { ?>
              <option value="USD"><?php echo $currency_usd; ?></option>
              <?php } ?>
              <?php if ($globalonepayhpp_currency1 == 'SEK') { ?>
              <option value="SEK" selected="selected"><?php echo $currency_sek; ?></option>
              <?php } else { ?>
              <option value="SEK"><?php echo $currency_sek; ?></option>
              <?php } ?>
              <?php if ($globalonepayhpp_currency1 == 'DKK') { ?>
              <option value="DKK" selected="selected"><?php echo $currency_dkk; ?></option>
              <?php } else { ?>
              <option value="DKK"><?php echo $currency_dkk; ?></option>
              <?php } ?>
	      <?php if ($globalonepayhpp_currency1 == 'NOK') { ?>
              <option value="NOK" selected="selected"><?php echo $currency_nok; ?></option>
              <?php } else { ?>
              <option value="NOK"><?php echo $currency_nok; ?></option>
              <?php } ?>
	      <?php if ($globalonepayhpp_currency1 == 'AUD') { ?>
              <option value="AUD" selected="selected"><?php echo $currency_aud; ?></option>
              <?php } else { ?>
              <option value="AUD"><?php echo $currency_aud; ?></option>
              <?php } ?>
	      <?php if ($globalonepayhpp_currency1 == 'CAD') { ?>
              <option value="CAD" selected="selected"><?php echo $currency_cad; ?></option>
              <?php } else { ?>
              <option value="CAD"><?php echo $currency_cad; ?></option>
              <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="globalonepayhpp_terminal1"><span data-toggle="tooltip" title="<?php echo $entry_terminal1_tip; ?>"><?php echo $entry_terminal1; ?></span></label>
            <div class="col-sm-10">
              <input type="text" name="globalonepayhpp_terminal1" id="globalonepayhpp_terminal1" name="globalonepayhpp_terminal1" value="<?php echo $globalonepayhpp_terminal1; ?>" class="form-control" />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="globalonepayhpp_secret1"><span data-toggle="tooltip" title="<?php echo $entry_secret1_tip; ?>"><?php echo $entry_secret1; ?></span></label>
            <div class="col-sm-10">
              <input type="text" name="globalonepayhpp_secret1" id="globalonepayhpp_secret1" value="<?php echo $globalonepayhpp_secret1; ?>" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="globalonepayhpp_currency2"><?php echo $entry_currency2; ?></label>
            <div class="col-sm-10">
              <select name="globalonepayhpp_currency2" id="globalonepayhpp_currency2" class="form-control">
              <?php if ($globalonepayhpp_currency2 == 'EUR') { ?>
              <option value="EUR" selected="selected"><?php echo $currency_eur; ?></option>
              <?php } else { ?>
              <option value="EUR"><?php echo $currency_eur; ?></option>
              <?php } ?>
              <?php if ($globalonepayhpp_currency2 == 'GBP') { ?>
              <option value="GBP" selected="selected"><?php echo $currency_gbp; ?></option>
              <?php } else { ?>
              <option value="GBP"><?php echo $currency_gbp; ?></option>
              <?php } ?>
              <?php if ($globalonepayhpp_currency2 == 'USD') { ?>
              <option value="USD" selected="selected"><?php echo $currency_usd; ?></option>
              <?php } else { ?>
              <option value="USD"><?php echo $currency_usd; ?></option>
              <?php } ?>
              <?php if ($globalonepayhpp_currency2 == 'SEK') { ?>
              <option value="SEK" selected="selected"><?php echo $currency_sek; ?></option>
              <?php } else { ?>
              <option value="SEK"><?php echo $currency_sek; ?></option>
              <?php } ?>
              <?php if ($globalonepayhpp_currency2 == 'DKK') { ?>
              <option value="DKK" selected="selected"><?php echo $currency_dkk; ?></option>
              <?php } else { ?>
              <option value="DKK"><?php echo $currency_dkk; ?></option>
              <?php } ?>
              <?php if ($globalonepayhpp_currency2 == 'NOK') { ?>
              <option value="NOK" selected="selected"><?php echo $currency_nok; ?></option>
              <?php } else { ?>
              <option value="NOK"><?php echo $currency_nok; ?></option>
              <?php } ?>
	      <?php if ($globalonepayhpp_currency2 == 'AUD') { ?>
              <option value="AUD" selected="selected"><?php echo $currency_aud; ?></option>
              <?php } else { ?>
              <option value="AUD"><?php echo $currency_aud; ?></option>
              <?php } ?>
	      <?php if ($globalonepayhpp_currency2 == 'CAD') { ?>
              <option value="CAD" selected="selected"><?php echo $currency_cad; ?></option>
              <?php } else { ?>
              <option value="CAD"><?php echo $currency_cad; ?></option>
              <?php } ?>
               </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="globalonepayhpp_terminal2"><?php echo $entry_terminal2; ?></label>
            <div class="col-sm-10">
              <input type="text" name="globalonepayhpp_terminal2" id="globalonepayhpp_terminal2" value="<?php echo $globalonepayhpp_terminal2; ?>" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="globalonepayhpp_secret2"><?php echo $entry_secret2; ?></label>
            <div class="col-sm-10">
              <input type="text" name="globalonepayhpp_secret2" id="globalonepayhpp_secret2" value="<?php echo $globalonepayhpp_secret2; ?>" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="globalonepayhpp_currency3"><?php echo $entry_currency3; ?></label>
            <div class="col-sm-10">
              <select name="globalonepayhpp_currency3" id="globalonepayhpp_currency3" class="form-control">
              <?php if ($globalonepayhpp_currency3 == 'EUR') { ?>
              <option value="EUR" selected="selected"><?php echo $currency_eur; ?></option>
              <?php } else { ?>
              <option value="EUR"><?php echo $currency_eur; ?></option>
              <?php } ?>
              <?php if ($globalonepayhpp_currency3 == 'GBP') { ?>
              <option value="GBP" selected="selected"><?php echo $currency_gbp; ?></option>
              <?php } else { ?>
              <option value="GBP"><?php echo $currency_gbp; ?></option>
              <?php } ?>
              <?php if ($globalonepayhpp_currency3 == 'USD') { ?>
              <option value="USD" selected="selected"><?php echo $currency_usd; ?></option>
              <?php } else { ?>
              <option value="USD"><?php echo $currency_usd; ?></option>
              <?php } ?>
              <?php if ($globalonepayhpp_currency3 == 'SEK') { ?>
              <option value="SEK" selected="selected"><?php echo $currency_sek; ?></option>
              <?php } else { ?>
              <option value="SEK"><?php echo $currency_sek; ?></option>
              <?php } ?>
              <?php if ($globalonepayhpp_currency3 == 'DKK') { ?>
              <option value="DKK" selected="selected"><?php echo $currency_dkk; ?></option>
              <?php } else { ?>
              <option value="DKK"><?php echo $currency_dkk; ?></option>
              <?php } ?>
              <?php if ($globalonepayhpp_currency3 == 'NOK') { ?>
              <option value="NOK" selected="selected"><?php echo $currency_nok; ?></option>
              <?php } else { ?>
              <option value="NOK"><?php echo $currency_nok; ?></option>
              <?php } ?>
	      <?php if ($globalonepayhpp_currency3 == 'AUD') { ?>
              <option value="AUD" selected="selected"><?php echo $currency_aud; ?></option>
              <?php } else { ?>
              <option value="AUD"><?php echo $currency_aud; ?></option>
              <?php } ?>
	      <?php if ($globalonepayhpp_currency3 == 'CAD') { ?>
              <option value="CAD" selected="selected"><?php echo $currency_cad; ?></option>
              <?php } else { ?>
              <option value="CAD"><?php echo $currency_cad; ?></option>
              <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="globalonepayhpp_terminal3"><?php echo $entry_terminal3; ?></label>
            <div class="col-sm-10">
              <input type="text" name="globalonepayhpp_terminal3" id="globalonepayhpp_terminal3" value="<?php echo $globalonepayhpp_terminal3; ?>" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="globalonepayhpp_secret3"><?php echo $entry_secret3; ?></label>
            <div class="col-sm-10">
              <input type="text" name="globalonepayhpp_secret3" id="globalonepayhpp_secret3" value="<?php echo $globalonepayhpp_secret3; ?>" class="form-control" />
            </div>
          </div>
           <div class="form-group required">
            <label class="col-sm-2 control-label" for="globalonepayhpp_send_receipt"><?php echo $entry_send_receipt; ?></label>
            <div class="col-sm-10">
              <select name="globalonepayhpp_send_receipt" id="globalonepayhpp_send_receipt" class="form-control">
              <?php if ($globalonepayhpp_send_receipt == '1') { ?>
              <option value="1" selected="selected"><?php echo $text_yes; ?></option>
              <?php } else { ?>
              <option value="1"><?php echo $text_yes; ?></option>
              <?php } ?>
              <?php if ($globalonepayhpp_send_receipt == '0') { ?>
              <option value="0" selected="selected"><?php echo $text_no; ?></option>
              <?php } else { ?>
              <option value="0"><?php echo $text_no; ?></option>
              <?php } ?>
              </select>
            </div>
          </div>
	</div>
        <div class="tab-pane" id="tab-order-status">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="globalonepayhpp_status_success"><?php echo $entry_status_success; ?></label>
            <div class="col-sm-10">
              <select name="globalonepayhpp_status_success" id="globalonepayhpp_status_success" class="form-control">
                <?php foreach ($order_statuses as $order_status) { ?>
                <?php if ($order_status['order_status_id'] == $globalonepayhpp_status_success) { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="globalonepayhpp_status_declined"><?php echo $entry_status_declined; ?></label>
            <div class="col-sm-10">
              <select name="globalonepayhpp_status_declined" id="globalonepayhpp_status_declined" class="form-control">
                <?php foreach ($order_statuses as $order_status) { ?>
                <?php if ($order_status['order_status_id'] == $globalonepayhpp_status_declined) { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="globalonepayhpp_status_failed"><?php echo $entry_status_failed; ?></label>
            <div class="col-sm-10">
              <select name="globalonepayhpp_status_failed" id="globalonepayhpp_status_failed" class="form-control">
                <?php foreach ($order_statuses as $order_status) { ?>
                <?php if ($order_status['order_status_id'] == $globalonepayhpp_status_failed) { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
            </div>
          </div>
	</div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?> 
