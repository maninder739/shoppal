<?php echo $header; ?>
<?php echo $column_left; ?> 

<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
               
               <!--  <button class="btn btn-danger" data-toggle="tooltip" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-affiliate').submit() : false;" title="<?php echo $button_delete; ?>" type="button">
                <i class="fa fa-trash-o">
                </i>
                </button> -->
            </div>
            <h1>
            <?php echo $heading_title; ?>
            </h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li>
                    <a href="<?php echo $breadcrumb['href']; ?>">
                        <?php echo $breadcrumb['text']; ?>
                    </a>
                </li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-circle">
            </i>
            <?php echo $error_warning; ?>
            <button class="close" data-dismiss="alert" type="button">
            ×
            </button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success">
            <i class="fa fa-check-circle">
            </i>
            <?php echo $success; ?>
            <button class="close" data-dismiss="alert" type="button">
            ×
            </button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <!--  <h3 class="panel-title">
                <i class="fa fa-list"></i> <?php echo $text_list; ?>
                </h3> -->
                <div class="container">
                    <h3>
                    <i class="fa fa-list">
                    </i>
                    <?php echo $text_list; ?>
                    </h3>
                    <ul class="nav nav-tabs" id="MyTab">
                        <li class="<?php if(!isset($_GET['ID']) || $_GET['ID'] == 1) { echo 'active'; } ?>" id="1">
                            <a href="index.php?route=monthly/payment_reporting&token=<?php echo $token; ?>&ID=1">
                                IFS
                            </a>
                        </li>
                        <li class="<?php if($_GET['ID'] == 2){ echo 'active'; } ?>" id="2">
                            <a href="index.php?route=monthly/payment_reporting&token=<?php echo $token; ?>&ID=2">
                                Members
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="panel-body">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label" for="input-date-start">
                                    <?php echo $entry_date_start; ?>
                                </label>
                                <div class="input-group date">
                                    <input class="form-control" data-date-format="YYYY-MM-DD" id="input-date-start" name="filter_date_start" placeholder="<?php echo $entry_date_start; ?>" type="text" value="<?php echo $filter_date_start; ?>"/>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">
                                            <i class="fa fa-calendar">
                                            </i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                             
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label" for="input-date-end">
                                    <?php echo $entry_date_end; ?>
                                </label>
                                <div class="input-group date">
                                    <input class="form-control" data-date-format="YYYY-MM-DD" id="input-date-end" name="filter_date_end" placeholder="<?php echo $entry_date_end; ?>" type="text" value="<?php echo $filter_date_end; ?>"/>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">
                                            <i class="fa fa-calendar">
                                            </i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-filter"></i> <?php echo $button_filter; ?></button>
                            </div>
                            
                        </div>
                        <div class="col-sm-4">
                        </div>
                    </div>
                </div>
                <form action="<?php echo $delete; ?>" enctype="multipart/form-data" id="form-affiliate" method="post">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td class="text-center">
                                        <input onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" type="checkbox"/>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'name') { ?>
                                        <a class="<?php echo strtolower($order); ?>" href="<?php echo $sort_name; ?>">
                                            <?php echo $column_name; ?>
                                        </a>
                                        <?php } else { ?>
                                        <a href="<?php echo $sort_name; ?>">
                                            <?php echo $column_name; ?>
                                        </a>
                                        <?php } ?>
                                    </td>
                                    
                                    <!--Organization name-->
                                    <td class="text-left">
                                        <?php if ($sort == 'a.company') { ?>
                                        <a class="<?php echo strtolower($order); ?>" href="<?php echo $sort_company; ?>">
                                            <?php echo $entry_company; ?>
                                        </a>
                                        <?php } else { ?>
                                        <a href="<?php echo $sort_company; ?>">
                                            <?php echo $entry_company; ?>
                                        </a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                      <?php if ($sort == 'a.commission') { ?>
                                        <a class="<?php echo strtolower($order); ?>" href="<?php echo $sort_commission; ?>">
                                            <?php echo $column_amountCalc; ?>
                                        </a>
                                        <?php } else { ?>
                                        <a href="<?php echo $sort_commission; ?>">
                                            <?php echo $column_amountCalc; ?>
                                        </a>
                                        <?php } ?>
                                    </td>

                                    <td class="text-right" style="width: 15%">
                                        <?php echo $column_amountSent; ?>
                                    </td>
                                    
                                    <!--  <td class="text-right"><?php echo $column_balance; ?></td> -->
                                     
                                    
                                    <td class="text-right" style="width: 15%">
                                        <?php echo $column_action; ?>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($affiliates) { ?>
                                <?php foreach ($affiliates  as  $affiliate) { 
                                ?>
                                 
                                <tr>
                                    <td class="text-center">
                                        <?php if (in_array($affiliate['affiliate_id'], $selected)) { ?>
                                        <input checked="checked" name="selected[]" type="checkbox" value="<?php echo $affiliate['affiliate_id']; ?>"/>
                                        <?php } else { ?>
                                        <input name="selected[]" type="checkbox" value="<?php echo $affiliate['affiliate_id']; ?>"/>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo $affiliate['name']; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo $affiliate['company']; ?>
                                    </td>
                                     <td class="text-left">
                                        <?php echo $affiliate['mta_commission']; ?>
                                    </td>
                                    <td class="text-left" contenteditable="true" onBlur="saveToDatabase(this,'answer','<?php echo $affiliate['affiliate_id']; ?>')" onClick="showEdit(this);" id="myeditablediv">
                                         <?php echo $affiliate['mta_commission']; ?>
                                    </td>
                                    
                                    <td class="text-right">
                                         <a class="btn btn-primary" data-toggle="tooltip" href="<?php echo $affiliate['edit']; ?>" title="<?php echo $button_add  ; ?>">
                                            <i class="fa fa-plus">
                                            </i>
                                        </a>
                                    </td>
                                </tr>
                                <?php } ?>
                                <?php } else { ?>
                                <tr>
                                    <td class="text-center" colspan="10">
                                        <?php echo $text_no_results; ?>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left">
                        <?php echo $pagination; ?>
                    </div>
                    <div class="col-sm-6 text-right">
                        <?php echo $results; ?>
                    </div>
                </div>
            </div>
        </div>  
    </div>

<script type="text/javascript">
    $('#button-filter').on('click', function() {
        url = 'index.php?route=monthly/payment_reporting&token=<?php echo $token; ?>';

        var filter_date_start = $('input[name=\'filter_date_start\']').val();
        if (filter_date_start) {
            url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
        }
        
        var filter_date_end = $('input[name=\'filter_date_end\']').val();
        if (filter_date_end) {
            url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
        }
        location = url;
    });
</script>
<script type="text/javascript">
    <!--
    $('input[name=\'filter_company\']').autocomplete({
        'source': function(request, response) {
            $.ajax({
                url: 'index.php?route=marketing/affiliate/autocomplete&token=<?php echo $token; ?>&ID=<?php echo isset($_GET["ID"]) ?  $_GET["ID"] : 1 ?>&filter_company=' + encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item['company'],
                            value: item['affiliate_id']
                        }
                    }));
                }
            });
        },
        'select': function(item) {
            $('input[name=\'filter_company\']').val(item['label']);
        }
    });
    $('input[name=\'filter_name\']').autocomplete({
        'source': function(request, response) {
            $.ajax({
                url: 'index.php?route=marketing/affiliate/autocomplete&token=<?php echo $token; ?>&ID=<?php echo isset($_GET["ID"]) ?  $_GET["ID"] : 1 ?>&filter_name=' + encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item['name'],
                            value: item['affiliate_id']
                        }
                    }));
                }
            });
        },
        'select': function(item) {
            $('input[name=\'filter_name\']').val(item['label']);
        }
    });
    $('input[name=\'filter_email\']').autocomplete({
        'source': function(request, response) {
            $.ajax({
                url: 'index.php?route=marketing/affiliate/autocomplete&token=<?php echo $token; ?>&ID=<?php echo isset($_GET["ID"]) ?  $_GET["ID"] : 1 ?>&filter_email=' + encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item['email'],
                            value: item['affiliate_id'],
                        }
                    }));
                }
            });
        },
        'select': function(item) {
            $('input[name=\'filter_email\']').val(item['label']);
        }
    });
    $('input[name=\'filter_code\']').autocomplete({
        'source': function(request, response) {
            $.ajax({
                url: 'index.php?route=marketing/affiliate/autocomplete&token=<?php echo $token; ?>&ID=<?php echo isset($_GET["ID"]) ?  $_GET["ID"] : 1 ?>&filter_code=' + encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item['code'],
                            value: item['affiliate_id'],
                        }
                    }));
                }
            });
        },
        'select': function(item) {
            $('input[name=\'filter_code\']').val(item['label']);
        }
    });
</script>
<script type="text/javascript">
    $('.date').datetimepicker({
        pickTime: false
    });
</script>
<script type="text/javascript">
    $("#MyTab li").click(function() {
        // var ID = $(this).attr('id');

        //  if(ID == 2 ){
        //      $('.membershide').show();
        //      $('.filter').css('margin-top', '-65px');
        //  } else {
        //      $('.membershide').hide();
        //      $('.filter').css('margin-top', '22px');
        //  }
    });

    function getvalue(obj) {
        $('#myModal').modal('hide');
        var mystr = obj.getAttribute("id");
        var myarr = mystr.split("|");
        var myvar = myarr[0];
        var myvar1 = myarr[1];
        $('#affilatename').val(myvar);
        $('#affilateid').val(myvar1);
    }

    $(document).ready(function() {
        var ID = "<?php echo isset($_GET['ID']) ?  $_GET['ID'] : '1' ?>";

        if (ID == 2) {
            $('.membershide').show();
            $('.filter').css('margin-top', '-65px');
        } else {
            $('.membershide').hide();
            $('.filter').css('margin-top', '22px');
        }
        //$('.filter').css('margin-top', '22px');
    });
</script>

<script>
    function showEdit(editableObj) {
        $(editableObj).css("background", "#FBFBFB");
    }

    function saveToDatabase(editableObj, column, id) {
        console.log(editableObj);
        $.ajax({
            url: 'index.php?route=monthly/payment_reporting/saveToDatabase&token=<?php echo $token; ?>&ID=<?php echo isset($_GET["ID"]) ?  $_GET["ID"] : 1 ?>',
            type: "POST",
            data: 'column=' + column + '&editval=' + editableObj.innerHTML + '&id=' + id,
            success: function(data) {
                $(editableObj).css("background", "#FDFDFD");
            }
        });
    }

    $("#myeditablediv").keypress(function(e) {
        if (isNaN(String.fromCharCode(e.which))) e.preventDefault();
    });
</script>


</div>
<?php echo $footer; ?>
<?php die; ?>