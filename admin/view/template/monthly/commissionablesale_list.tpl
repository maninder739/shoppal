<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <!-- <button class="btn btn-success" data-toggle="tooltip" form="form-order" formaction="<?php echo $pdf; ?>" formtarget="_blank" id="button-print" title="<?php echo $Print_Outstanding_Invoices; ?>" type="submit">
                    <i class="fa fa-file-pdf-o">
                    </i>
                </button>
                <button class="btn btn-info" data-toggle="tooltip" form="form-order" formaction="<?php echo $invoice; ?>" formtarget="_blank" id="button-invoice" title="<?php echo $button_invoice_print; ?>" type="submit">
                    <i class="fa fa-print">
                    </i>
                </button>
                <a class="btn btn-primary" data-toggle="tooltip" href="<?php echo $add; ?>" title="<?php echo $button_add; ?>">
                    <i class="fa fa-plus">
                    </i>
                </a>
                <button class="btn btn-danger" data-toggle="tooltip" form="form-order" formaction="<?php echo $delete; ?>" id="button-delete" title="<?php echo $button_delete; ?>" type="button">
                    <i class="fa fa-trash-o">
                    </i>
                </button> -->
            </div>
            <h1>
                <?php echo $heading_title; ?>
            </h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li>
                    <a href="<?php echo $breadcrumb['href']; ?>">
                        <?php echo $breadcrumb['text']; ?>
                    </a>
                </li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-circle">
            </i>
            <?php echo $error_warning; ?>
            <button class="close" data-dismiss="alert" type="button">
                ×
            </button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success">
            <i class="fa fa-check-circle">
            </i>
            <?php echo $success; ?>
            <button class="close" data-dismiss="alert" type="button">
                ×
            </button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fa fa-list">
                    </i>
                    <?php echo $text_list; ?>
                </h3>
            </div>
            <div class="panel-body">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label" for="input-date-start">
                                    <?php echo $entry_date_start; ?>
                                </label>
                                <div class="input-group date">
                                    <input class="form-control" data-date-format="YYYY-MM-DD" id="input-date-start" name="filter_date_start" placeholder="<?php echo $entry_date_start; ?>" type="text" value="<?php echo $filter_date_start; ?>"/>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">
                                            <i class="fa fa-calendar">
                                            </i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-date-end">
                                    <?php echo $entry_date_end; ?>
                                </label>
                                <div class="input-group date">
                                    <input class="form-control" data-date-format="YYYY-MM-DD" id="input-date-end" name="filter_date_end" placeholder="<?php echo $entry_date_end; ?>" type="text" value="<?php echo $filter_date_end; ?>"/>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">
                                            <i class="fa fa-calendar">
                                            </i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <?php
                      
                        $filter_order_status = explode(',', $filter_order_status);
                        foreach ($filter_order_status as $filter_order_val) {
                            $order_status_val[] = $filter_order_val;
                        }
                    ?>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label" for="input-order-status">
                                    <?php echo $entry_order_status; ?>
                                </label>
                                <select class="multipleSelect" multiple name="filter_order_status[]">
                                    <option value="">
                                    </option>
                                    <?php foreach ($order_statuses as $order_status) { ?>
                                    <?php if (in_array($order_status['order_status_id'] , $order_status_val)) { ?>
                                    <option selected="selected" value="<?php echo $order_status['order_status_id']; ?>">
                                        <?php echo $order_status['name']; ?>
                                    </option>
                                    <?php } else { ?>
                                    <option value="<?php echo $order_status['order_status_id']; ?>">
                                        <?php echo $order_status['name']; ?>
                                    </option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-total">
                                    <?php echo $entry_trackingcode; ?>
                                </label>
                                <input class="form-control" id="input-total" name="filter_tracking_code" placeholder="<?php echo $entry_trackingcode; ?>" type="text" value="<?php echo $filter_tracking_code; ?>"/>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label style="">
                                </label>
                                <button class="btn btn-primary pull-right" id="button-filter" type="button">
                                    <i class="fa fa-filter">
                                    </i>
                                    <?php echo $button_filter; ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <form action="" enctype="multipart/form-data" id="form-order" method="post">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <td class="text-right">
                                    <?php if ($sort == 'o.order_id') { ?>
                                    <a class="<?php echo strtolower($order); ?>" href="<?php echo $sort_order; ?>">
                                        <?php echo $column_order_id; ?>
                                    </a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_order; ?>">
                                        <?php echo $column_order_id; ?>
                                    </a>
                                    <?php } ?>
                                </td>
                                <td class="text-left">
                                    <?php if ($sort == 'order_status') { ?>
                                    <a class="<?php echo strtolower($order); ?>" href="<?php echo $sort_status; ?>">
                                        <?php echo $column_status; ?>
                                    </a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_status; ?>">
                                        <?php echo $column_status; ?>
                                    </a>
                                    <?php } ?>
                                </td>
                                <td class="text-left">
                                    <?php if ($sort == 'o.date_added') { ?>
                                    <a class="<?php echo strtolower($order); ?>" href="<?php echo $sort_date_added; ?>">
                                        <?php echo $column_date_added; ?>
                                    </a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_date_added; ?>">
                                        <?php echo $column_date_added; ?>
                                    </a>
                                    <?php } ?>
                                </td>
                                <td class="text-left">
                                    <?php if ($sort == 'customer') { ?>
                                    <a class="<?php echo strtolower($order); ?>" href="<?php echo $sort_customer; ?>">
                                        <?php echo $column_customer; ?>
                                    </a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_customer; ?>">
                                        <?php echo $column_customer; ?>
                                    </a>
                                    <?php } ?>
                                </td>
                                <td class="text-left">
                                    <?php if ($sort == 'tracking') { ?>
                                    <a class="<?php echo strtolower($order); ?>" href="<?php echo $sort_tracking; ?>">
                                        <?php echo $column_trackingcode; ?>
                                    </a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_tracking; ?>">
                                        <?php echo $column_trackingcode; ?>
                                    </a>
                                    <?php } ?>
                                </td>
                                <td class="text-left">
                                    <?php if ($sort == 'orgname') { ?>
                                    <a class="<?php echo strtolower($order); ?>" href="<?php echo $sort_orgname; ?>">
                                        <?php echo $column_orgname; ?>
                                    </a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_orgname; ?>">
                                        <?php echo $column_orgname; ?>
                                    </a>
                                    <?php } ?>
                                </td>
                                
                                <td class="text-left">
                                    <?php if ($sort == 'retail_total') { ?>
                                    <a class="<?php echo strtolower($order); ?>" href="<?php echo $sort_retail_total; ?>">
                                        <?php echo $column_retailtotal; ?>
                                    </a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_retail_total; ?>">
                                        <?php echo ($column_retailtotal); ?>
                                    </a>
                                    <?php } ?>
                                </td>
                                <td class="text-left">
                                    <?php if ($sort == 'refaffiliate') { ?>
                                     <a class="<?php echo strtolower($refaffiliate); ?>" href="<?php echo $sort_refaffiliate; ?>">
                                        <?php echo $column_refaffiliate; ?>
                                    </a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_refaffiliate; ?>">
                                        <?php echo ($column_refaffiliate); ?>
                                    </a>
                                    <?php } ?>
                                </td>

                                <td class="text-left">
                                    <?php if ($sort == 'refamount') { ?>
                                     <a class="<?php echo strtolower($refamount); ?>" href="<?php echo $sort_refamount; ?>">
                                        <?php echo $column_refamount; ?>
                                    </a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_refamount; ?>">
                                        <?php echo ($column_refamount); ?>
                                    </a>
                                    <?php } ?>
                                </td>
                                
                                <td class="text-right">
                                    <?php echo $column_action; ?>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($orders) { ?>
                            <?php foreach ($orders as $order) { ?>
                            <tr>
                                 
                                <td class="text-right">
                                    <?php echo $order['order_id']; ?>
                                </td>
                                <td class="text-left">
                                    <?php echo $order['order_status']; ?>
                                </td>
                                <td class="text-left">
                                    <?php echo $order['date_added']; ?>
                                </td>
                                <td class="text-left">
                                    <?php echo $order['customer']; ?>
                                </td>
                                <td class="text-left">
                                    <?php echo $order['tracking']; ?>
                                </td>
                                <td class="text-left">
                                    <?php echo $order['orgname']; ?>
                                </td>
                               
                                <td class="text-left">
                                    <?php echo  $order['retails']; ?>
                                </td>
                                <td class="text-right">
                                    <?php echo $order['refaffiliate']; ?>
                                </td>
                                
                                <td class="text-left">
                                        <?php echo $order['refamount']; ?>
                                    </td>
                                <td class="text-right">
                                     <a class="btn btn-success" data-toggle="tooltip" href="<?php echo $order['customer_edit']; ?>" title="<?php echo $button_view_customer; ?>">
                                        <i class="fa fa-eye">
                                        </i>
                                    </a>
                                    
                                    <a class="btn btn-info" data-toggle="tooltip" href="<?php echo $order['order_edit']; ?>" title="<?php echo $button_view_order; ?>">
                                        <i class="fa fa-eye">
                                        </i>
                                    </a>
                                </td>
                            </tr>
                            <?php } ?>
                            <?php } else { ?>
                            <tr>
                                <td class="text-center" colspan="11">
                                    <?php echo $text_no_results; ?>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </form>
            <div class="row">
                <div class="col-sm-6 text-left">
                    <?php echo $pagination; ?>
                </div>
                <div class="col-sm-6 text-right">
                    <?php echo $results; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
   
      $('#button-filter').on('click', function() {
        url = 'index.php?route=monthly/commissionable_sales&token=<?php echo $token; ?>';
       
        $("select[name^='filter_order_status']").each(function () {
            if($(this).val() != null ) {
                url += '&filter_order_status=' + encodeURIComponent($(this).val());
            }
            
        });
          
        var filter_tracking_code = $('input[name=\'filter_tracking_code\']').val();
      
        if (filter_tracking_code) {
            url += '&filter_tracking_code=' + encodeURIComponent(filter_tracking_code);
        }
      
        var filter_date_start = $('input[name=\'filter_date_start\']').val();
      
        if (filter_date_start) {
            url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
        }
      
        var filter_date_end = $('input[name=\'filter_date_end\']').val();
      
        if (filter_date_end) {
            url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
        }
      
        location = url;
      });
</script>
<script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript">
</script>
<link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" media="screen" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
    <!--
      $('.date').datetimepicker({
        pickTime: false
      });
      //-->
</script>
<script type="text/javascript">
    $('.multipleSelect').fastselect();
</script>
<?php echo $footer; ?>
