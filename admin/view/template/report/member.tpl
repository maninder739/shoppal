<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="<?php echo $export; ?>" class="btn btn-info" role="button"><?php echo $export_excel ?></a>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                               <td class="text-left"><?php if ($sort == 'firstname') { ?>
                                    <a href="<?php echo $sort_firstname; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_aff_first_name; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_firstname; ?>">
                                    <?php echo $column_aff_first_name; ?></a>
                                <?php } ?></td>  
                                 <td class="text-left"><?php if ($sort == 'lastname') { ?>
                                    <a href="<?php echo $sort_lastname; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_aff_last_name; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_lastname; ?>">
                                    <?php echo $column_aff_last_name; ?></a>
                                <?php } ?></td> 
                                <td class="text-left"><?php if ($sort == 'email') { ?>
                                    <a href="<?php echo $sort_email; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_email; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_email; ?>">
                                    <?php echo $column_email; ?></a>
                                <?php } ?></td>
                                <td class="text-left"><?php echo $column_telephone; ?></td>
                                <td class="text-left"><?php echo $column_fax; ?></td>
                                <td class="text-left"><?php if ($sort == 'company') { ?>
                                    <a href="<?php echo $sort_company; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_company; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_company; ?>">
                                    <?php echo $column_company; ?></a>
                                <?php } ?></td>
                                <td class="text-left"><?php if ($sort == 'code') { ?>
                                    <a href="<?php echo $sort_code; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_code; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_code; ?>">
                                    <?php echo $column_code; ?></a>
                                <?php } ?></td>
                                <td class="text-left"><?php echo $column_address1; ?></td>
                                <td class="text-right"><?php echo $column_address2; ?></td>
                                <td class="text-right"><?php echo $column_city; ?></td>
                                <td class="text-right"><?php echo $column_postcode; ?></td>
                                <td class="text-right"><?php echo $column_zone; ?></td>
                                <td class="text-right"><?php echo $column_country; ?></td>
                                <td class="text-right"><?php echo $column_name; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($members) { ?>
                            <?php foreach ($members as $member) { ?>
                            <tr>
                                <td class="text-left"><?php echo $member['aff_first_name']; ?></td>
                                <td class="text-left"><?php echo $member['aff_last_name']; ?></td>
                                <td class="text-left"><?php echo $member['email']; ?></td>
                                <td class="text-left"><?php echo $member['telephone']; ?></td>
                                <td class="text-left"><?php echo $member['fax']; ?></td>
                                <td class="text-left"><?php echo $member['company']; ?></td>
                                <td class="text-left"><?php echo $member['code']; ?></td>
                                <td class="text-left"><?php echo $member['address_1']; ?></td>
                                <td class="text-right"><?php echo $member['address_2']; ?></td>
                                <td class="text-right"><?php echo $member['city']; ?></td>
                                <td class="text-right"><?php echo $member['postcode']; ?></td>
                                <td class="text-right"><?php echo $member['zone']; ?></td>
                                <td class="text-right"><?php echo $member['country']; ?></td>
                                <td class="text-right"><?php echo $member['name']; ?></td>
                            </tr>
                            <?php } ?>
                            <?php } else { ?>
                            <tr>
                                <td class="text-center" colspan="12"><?php echo $text_no_results; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
