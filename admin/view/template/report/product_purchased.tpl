<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
        <div class="pull-right">
              <button type="button" id="button-export" value="1" name="export_excel" class="btn btn-success"><?php echo $exportexcel; ?></button>
        </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-date-start"><?php echo $entry_date_start; ?></label>
                <div class="input-group date">
                  <input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" placeholder="<?php echo $entry_date_start; ?>" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-date-end"><?php echo $entry_date_end; ?></label>
                <div class="input-group date">
                  <input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" placeholder="<?php echo $entry_date_end; ?>" data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span>
                </div>
              </div>
                <?php
                $filter_category_ids_lists = explode(',', $filter_category_ids);
                    foreach ($filter_category_ids_lists as $filter_category_ids_list) {
                    $filter_category_ids_listing[] = $filter_category_ids_list;
                }?>
              <div class="form-group">
                <label class="control-label" for="input-date-end"><?php echo $entry_category_name; ?></label>
                <select name="filter_category_id[]" id="input-status" class="form-control multipleSelect" multiple>
                  <?php foreach ($category_lisings as $category_lising) { ?>
                        <?php if(in_array($category_lising['category_id'] , $filter_category_ids_listing)) { ?>
                            <option value="<?php echo $category_lising['category_id']; ?>" selected="selected"><?php echo $category_lising['name']; ?></option>   
                         <?php } else { ?>
                            <option value="<?php echo $category_lising['category_id']; ?>"><?php echo $category_lising['name']; ?></option>
                      <?php } ?>    
                  <?php } ?>
                </select>
              </div>
            </div>
            <?php
                $filter_order_status_id_lists = explode(',', $filter_order_status_id);
                foreach ($filter_order_status_id_lists as $filter_order_status_id_list) {
                $filter_order_status[] = $filter_order_status_id_list;
            }
           
            ?>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-status"><?php echo $entry_status; ?></label>
                <select name="filter_order_status_id[]" id="input-status" class="form-control multipleSelect" multiple>
                  <?php foreach ($order_statuses as $order_status) { ?>
                      <?php if (in_array($order_status['order_status_id'] , $filter_order_status)) { ?>
                            <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                      <?php } else { ?>
                            <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                      <?php } ?>
                  <?php } ?>
                </select>
              </div>
               <?php
                $filter_product_ids_lists = explode(',', $filter_product_ids);
                foreach ($filter_product_ids_lists as $filter_product_ids_list) {
                $filter_product_ids_listing[] = $filter_product_ids_list;
                }
              ?>
              <div class="form-group">
                <label class="control-label"><?php echo $entry_product_name; ?></label>
                    <select class="form-control multipleSelect" multiple name="filter_product_id[]">
                     <?php foreach ($prouduct_lisings as $prouduct_lising) { ?>
                         <?php  if(in_array($prouduct_lising['product_id'] , $filter_product_ids_listing)) { ?>
                            <option value="<?php echo $prouduct_lising['product_id'] ?>" selected="selected"><?php echo $prouduct_lising['name']  ?></option>
                            <?php } else { ?>
                             <option value="<?php echo $prouduct_lising['product_id']; ?>"><?php echo $prouduct_lising['name']; ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </div>
              <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-filter"></i> <?php echo $button_filter; ?></button>
            </div>
          </div>
        </div>
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                 <td class="text-left"><?php if ($sort == 'productname') { ?>
                    <a href="<?php echo $sort_productname; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_productname; ?>">
                    <?php echo $column_name; ?></a>
                <?php } ?></td>  

                <td class="text-left"><?php if ($sort == 'categoryname') { ?>
                    <a href="<?php echo $sort_category_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_category_name; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_category_name; ?>">
                    <?php echo $column_category_name; ?></a>
                <?php } ?></td>  

                <td class="text-left"><?php if ($sort == 'model') { ?>
                    <a href="<?php echo $sort_model; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_model; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_model; ?>">
                    <?php echo $column_model; ?></a>
                <?php } ?></td>  

                <td class="text-left"><?php if ($sort == 'quantity') { ?>
                    <a href="<?php echo $sort_quantity; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_quantity; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_quantity; ?>">
                    <?php echo $column_quantity; ?></a>
                <?php } ?></td>  

                 <td class="text-left"><?php if ($sort == 'productname') { ?>
                    <a href="<?php echo $sort_total; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_total; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_total; ?>">
                    <?php echo $column_total; ?></a>
                <?php } ?></td>
         
              </tr>
            </thead>
            <tbody>
              <?php if ($products) { ?>
              <?php foreach ($products as $product) { ?>
              <tr>
                <td class="text-left"><?php echo $product['name']; ?></td>
                <td class="text-left"><?php echo $product['category']; ?></td>
                <td class="text-left"><?php echo $product['model']; ?></td>
                <td class="text-right"><?php echo $product['quantity']; ?></td>
                <td class="text-right"><?php echo $product['total']; ?></td>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="text-center" colspan="4"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	url = 'index.php?route=report/product_purchased&token=<?php echo $token; ?>';
	
	var filter_date_start = $('input[name=\'filter_date_start\']').val();
	
	if (filter_date_start) {
		url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
	}

	var filter_date_end = $('input[name=\'filter_date_end\']').val();
	
	if (filter_date_end) {
		url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
	}
	
    $("select[name^='filter_order_status_id']").each(function() {
        if ($(this).val() != null) {
            url += '&filter_order_status_id=' + encodeURIComponent($(this).val());
        }
    });	

    $("select[name^='filter_product_id']").each(function() {
        if ($(this).val() != null) {
            url += '&filter_product_id=' + encodeURIComponent($(this).val());
        }
    }); 

    
    $("select[name^='filter_category_id']").each(function() {
        if ($(this).val() != null) {
            url += '&filter_category_id=' + encodeURIComponent($(this).val());
        }
    }); 

	location = url;
});
//--></script> 
<script type="text/javascript">
    url = 'index.php?route=report/product_purchased&token=<?php echo $token; ?>';
    $('#button-export').on('click', function() {

        url = 'index.php?route=report/product_purchased&token=<?php echo $token; ?>';
        var filter_date_start = $('input[name=\'filter_date_start\']').val();
    
    if (filter_date_start) {
        url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
    }

    var filter_date_end = $('input[name=\'filter_date_end\']').val();
    
    if (filter_date_end) {
        url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
    }
    
    $("select[name^='filter_order_status_id']").each(function() {
        if ($(this).val() != null) {
            url += '&filter_order_status_id=' + encodeURIComponent($(this).val());
        }
    }); 

    $("select[name^='filter_product_id']").each(function() {
        if ($(this).val() != null) {
            url += '&filter_product_id=' + encodeURIComponent($(this).val());
        }
    }); 

    
    $("select[name^='filter_category_id']").each(function() {
        if ($(this).val() != null) {
            url += '&filter_category_id=' + encodeURIComponent($(this).val());
        }
    });

    $("select[name^='filter_category_id']").each(function() {
        if ($(this).val() != null) {
            url += '&filter_category_id=' + encodeURIComponent($(this).val());
        }
    });
 
    var export_excel = $('#button-export').val();
    if(export_excel){
         url += '&export_excel=' + encodeURIComponent(export_excel);
    }    
        location = url;
    });
</script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script> 
<script type="text/javascript">
    $('.multipleSelect').fastselect();
</script>

</div>
<?php echo $footer; ?>
