<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="<?php echo $export; ?>" class="btn btn-info" role="button"><?php echo $export_excel ?></a>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <td class="text-left"><?php echo $column_customer_id; ?></td>
                                <td class="text-right"><?php echo $column_firstname; ?></td>
                                <td class="text-right"><?php echo $column_lastname; ?></td>
                                <td class="text-right"><?php echo $column_email; ?></td>
                                <td class="text-right"><?php echo $column_telephone; ?></td>
                                <td class="text-right"><?php echo $column_payment_city; ?></td>
                                <td class="text-right"><?php echo $column_payment_zone; ?></td>
                                <td class="text-right"><?php echo $column_payment_postcode; ?></td>
                                <td class="text-right"><?php echo $column_expmonth; ?></td>
                                <td class="text-right"><?php echo $column_expyear; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($members) { ?>
                            <?php foreach ($expirings as $expiring) { ?>
                            <tr>
                                <td class="text-left"><?php echo $expiring['customer_id']; ?></td>
                                <td class="text-left"><?php echo $expiring['firstname']; ?></td>
                                <td class="text-left"><?php echo $expiring['lastname']; ?></td>
                                <td class="text-left"><?php echo $expiring['email']; ?></td>
                                <td class="text-left"><?php echo $expiring['telephone']; ?></td>
                                <td class="text-left"><?php echo $expiring['payment_city']; ?></td>
                                <td class="text-left"><?php echo $expiring['payment_zone']; ?></td>
                                <td class="text-left"><?php echo $expiring['payment_postcode']; ?></td>
                                <td class="text-right"><?php echo $expiring['expmonth']; ?></td>
                                <td class="text-right"><?php echo $expiring['expyear']; ?></td>
                            </tr>
                            <?php } ?>
                            <?php } else { ?>
                            <tr>
                                <td class="text-center" colspan="12"><?php echo $text_no_results; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
