<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
             <button class="btn btn-primary button-filter" name="filter_export_excel" value="1" >Export Excel</button>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label" for="input-date-start"><?php echo $entry_date_start; ?></label>
                                <div class="input-group date">
                                    <input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" placeholder="<?php echo $entry_date_start; ?>" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="input-date-end"><?php echo $entry_date_end; ?></label>
                                    <div class="input-group date">
                                        <input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" placeholder="<?php echo $entry_date_end; ?>" data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control" />
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                        </span></div>
                                    </div>
                                    
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label" for="input-email"><?php echo $entry_tracking_code; ?></label>
                                        <input type="text" name="filter_tracking_code" value="<?php echo $filter_tracking_code; ?>" placeholder="<?php echo $entry_tracking_code; ?>" id="input-email" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label" for="input-email"><?php echo $entry_customer; ?></label>
                                        <input type="text" name="filter_customer" value="<?php echo $filter_customer; ?>" placeholder="<?php echo $entry_customer; ?>" id="input-email" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label" for="input-email"><?php echo $entry_current_affiliate; ?></label>
                                        <input type="text" name="filter_affiliate" value="<?php echo $filter_affiliate; ?>" placeholder="<?php echo $entry_current_affiliate; ?>" id="input-email" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label" for="input-email"><?php echo $column_parent_affiliate; ?></label>
                                        <input type="text" name="filter_parent_affiliate" value="<?php echo $filter_parent_affiliate; ?>" placeholder="<?php echo $column_parent_affiliate; ?>" id="input-email" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="button" id="button-filter" class="btn btn-primary pull-right button-filter"><i class="fa fa-filter"></i> <?php echo $button_filter; ?></button>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <td class="text-left"><?php echo $column_parent_affiliate_name; ?></td>
                                        <td class="text-left"><?php echo $column_parent_affiliate_code; ?></td>
                                        <td class="text-left"><?php echo $column_parent_affiliate_company; ?></td>
                                        <td class="text-left"><?php echo $column_tracking_code; ?></td>
                                        <td class="text-left"><?php echo $column_order_id; ?></td>
                                        <td class="text-left"><?php echo $column_order_date; ?></td>
                                        <td class="text-left"><?php echo $column_customer_id; ?></td>
                                        <td class="text-left"><?php echo $column_customer_name; ?></td>
                                        <td class="text-left"><?php echo $column_status; ?></td>
                                        <td class="text-left"><?php echo $column_affname; ?></td>
                                        <td class="text-left"><?php echo $column_affcode; ?></td>
                                        <td class="text-left"><?php echo $column_affcompany; ?></td>
                                        <td class="text-left"><?php echo $column_total; ?></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if ($affiliates) { ?>
                                    <?php foreach ($affiliates as $affiliate) { ?>
                                    <tr>
                                        <td class="text-left"><?php echo $affiliate['parentaffname']; ?></td>
                                        <td class="text-left"><?php echo $affiliate['parentaffcode']; ?></td>
                                        <td class="text-left"><?php echo $affiliate['parentaffcompany']; ?></td>
                                        <td class="text-left"><?php echo $affiliate['tracking']; ?></td>
                                        <td class="text-left"><?php echo $affiliate['order_id']; ?></td>
                                        <td class="text-left"><?php echo $affiliate['date_added']; ?></td>
                                        <td class="text-left"><?php echo $affiliate['custId']; ?></td>
                                        <td class="text-left"><?php echo $affiliate['cust_name']; ?></td>
                                        <td class="text-left"><?php echo $affiliate['order_status']; ?></td>
                                        <td class="text-left"><?php echo $affiliate['affname']; ?></td>
                                        <td class="text-left"><?php echo $affiliate['affcode']; ?></td>
                                        <td class="text-right"><?php echo $affiliate['affcompany']; ?></td>
                                        
                                        <td class="text-right"><?php echo $affiliate['total']; ?></td>
                                        
                                        
                                    </tr>
                                    <?php } ?>
                                    <?php } else { ?>
                                    <tr>
                                        <td class="text-center" colspan="13"><?php echo $text_no_results; ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                            <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- </div>
    </div> -->
    <script type="text/javascript"><!--
    $('.button-filter').on('click', function() {
        
    url = 'index.php?route=report/ifs_volume&token=<?php echo $token; ?>';
    
    var filter_date_start = $('input[name=\'filter_date_start\']').val();
    
    if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
    }
    var filter_date_end = $('input[name=\'filter_date_end\']').val();
    
    if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
    }
    var filter_tracking_code = $('input[name=\'filter_tracking_code\']').val();
    if (filter_tracking_code) {
    url += '&filter_tracking_code=' + encodeURIComponent(filter_tracking_code);
    
    }
    var filter_affiliate = $('input[name=\'filter_affiliate\']').val();
    if (filter_affiliate) {
    url += '&filter_affiliate=' + encodeURIComponent(filter_affiliate);
    
    }
    var filter_parent_affiliate = $('input[name=\'filter_parent_affiliate\']').val();
    if (filter_parent_affiliate) {
    url += '&filter_parent_affiliate=' + encodeURIComponent(filter_parent_affiliate);
    
    }

    var filter_export_excel = $(this).val();
    if (filter_export_excel) {
    url += '&filter_export_excel=' + encodeURIComponent(filter_export_excel);
    
    }

    var filter_customer = $('input[name=\'filter_customer\']').val();
    if (filter_customer) {
    url += '&filter_customer=' + encodeURIComponent(filter_customer);


    
    }
    
    location = url;
    });
    //--></script>
    <script type="text/javascript"><!--
    $('.date').datetimepicker({
        pickTime: false
    });
//--></script>
</div>
        <?php echo $footer; ?>