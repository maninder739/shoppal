var objSmartFlexible = (function($) {
    return {
        total_custom_packages: 0,
        total_custom_envelopes: 0,
        total_promo: 0,
        total_holidays: 0,
        total_adjustment_rules: 0,
        holidays: {},
        params: {},
        tabsLinks: null,
        servicesPromise: $.Deferred().resolve(),
        init: function(params) {
            this.params = params;
            this.addUserIdListener();
            this.addPackerListener();
            this.addLabelListener();
            this.addLabelFormatListener();
            this.addPostcodeListener();
            this.addImportSettingsListener();
            this.addHolidayListener();
            this.addInsuranceListener();
            this.addMeasurementSystemListener();
            this.addCountryListener();
            this.addBillingSameListener();
            this.addBillingCountryListener();
        },
        initForVersion1: function() {
            var tabs = $('#tabs');
            this.tabsLinks = tabs.find('a');
            if (this.tabsLinks.length) {
                this.tabsLinks.tabs();
            }
            this.tabsLinks.click(function (e) {
                e.preventDefault();
                window.location.hash = '!' + $(e.target).attr("href").substr(1);
            });
            tabs.find('a[href="#' + window.location.hash.substr(2) + '"]').trigger('click');
            this.highlightTabsWithError();
        },
        initForVersion2: function() {
            this.tabsLinks = $('.nav-tabs a');
            this.tabsLinks.click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });
            this.tabsLinks.on('shown.bs.tab', function (e) {
                window.location.hash = '!' + $(e.target).attr("href").substr(1);
            });
            if ($().tab) {
                $('.nav-tabs a[href="#' + window.location.hash.substr(2) + '"]').tab('show');
            }
            this.highlightTabsWithError();
        },
        initForVersion3: function() {
            this.initForVersion2(); // same
        },
        licenseClose: function() {
            document.cookie = this.params.licenseCookie + '=true; expires=Fri, 31 Dec 9999 23:59:59 GMT';
            $('#license').hide();
        },
        highlightTabsWithError: function() {
            $('.tab-pane').each(function() {
                if ($(this).has('span.error, p.text-danger').length) {
                    objSmartFlexible.tabsLinks.filter('[href="#' + this.id + '"]').addClass('_has-error');
                } else {
                    objSmartFlexible.tabsLinks.filter('[href="#' + this.id + '"]').removeClass('_has-error');
                }
            });
        },
        sortNumbers: function(a, b) {
            if (a < b) {
                return -1;
            }
            if (a > b) {
                return 1;
            }
            return 0;
        },
        options: function(data, keyField, valueField, selected) {
            return data.map(function(row) {
                if (row.type === 'optgroup') {
                    return '<optgroup label="' + row.label + '">' +
                        objSmartFlexible.options(row.options, keyField, valueField, selected) + '</optgroup>';
                }
                var isSelected = Array.isArray(selected) ? (selected.indexOf(row[keyField]) !== -1) :
                    (row[keyField] === selected);
                return '<option value="' + row[keyField] + '"' + (isSelected ? ' selected' : '') + '>' +
                    row[valueField] + '</option>';
            }).join('\n');
        },
        appendHTML: function(target, holderElement, html) {
            if (target) {
                var holder = document.createElement(holderElement);
                holder.innerHTML = html;
                target.appendChild(holder);
            }
        },
        moveElementTo: function(element, newParent) {
            var eold = $(element);
            var enew = eold.clone().appendTo(newParent);
            enew.css('visibility', 'hidden'); // hold the place
            var newOffset = enew.offset();
            var oldOffset = eold.offset();
            var etemp = eold.clone().appendTo('body');
            etemp
                .css('position', 'absolute')
                .css('left', oldOffset.left)
                .css('top', oldOffset.top)
                .css('zIndex', 1000);
            eold.hide();
            etemp.animate({'top': newOffset.top, 'left': newOffset.left}, 'slow', function() {
                enew.css('visibility', 'visible');
                eold.remove();
                etemp.remove();
            });
        },
        addCustomPackage: function(length, width, height, max_load, tare) {
            var name = this.params.customPackages;
            var center = this.params.center;
            this.total_custom_packages++;
            var package_id = this.total_custom_packages;
            var html =
                '<td class="' + center + '">' +
                '   <input' +
                '       name="' + name + '[' + package_id + '][length]"' +
                '       value="' + length + '"' +
                '       class="form-control small-number" />' +
                '</td>' +
                '<td class="' + center + '">' +
                '   <input' +
                '       name="' + name + '[' + package_id + '][width]"' +
                '       value="' + width + '"' +
                '       class="form-control small-number" />' +
                '</td>' +
                '<td class="' + center + '">' +
                '   <input' +
                '       name="' + name + '[' + package_id + '][height]"' +
                '       value="' + height + '"' +
                '       class="form-control small-number" />' +
                '</td>' +
                '<td class="' + center + '">' +
                '   <input' +
                '       name="' + name + '[' + package_id + '][max_load]"' +
                '       value="' + (max_load != '0' ? max_load : '') + '"' +
                '       class="form-control small-number" />' +
                '</td>' +
                '<td class="' + center + '">' +
                '   <input' +
                '       name="' + name + '[' + package_id + '][tare]"' +
                '       value="' + (tare != '0' ? tare : '') + '"' +
                '       class="form-control small-number" />' +
                '</td>' +
                '<td class="' + center + '">' +
                '   <input' +
                '       type="hidden"' +
                '       name="' + name + '[' + package_id + '][package_id]"' +
                '       value="' + package_id + '"/>' +
                '   <a href="#" onclick="objSmartFlexible.removeCustomPackage(this); return false;">' +
                    this.params.textRemovePackage +
            '   </a>' +
            '</td>';
            this.appendHTML(document.getElementById('custom_packages_holder'), 'tr', html);
            $('input[name="' + name + '[' + package_id + '][length]' + '"], ' +
                'input[name="' + name + '[' + package_id + '][width]' + '"], ' +
                'input[name="' + name + '[' + package_id + '][height]' + '"]'
            ).on('change', this.handleCustomBoxChange);
            this.handleCustomBoxChange.call(
                $('input[name="' + name + '[' + package_id + '][length]' + '"]')
            );
        },
        addCustomEnvelope: function(length, width, max_height, max_load, tare) {
            var name = this.params.customEnvelopes;
            var center = this.params.center;
            this.total_custom_envelopes++;
            var envelope_id = this.total_custom_envelopes;
            var html =
                '<td class="' + center + '">' +
                '   <input' +
                '       name="' + name + '[' + envelope_id + '][length]"' +
                '       value="' + length + '"' +
                '       class="form-control small-number" />' +
                '</td>' +
                '<td class="' + center + '">' +
                '   <input' +
                '       name="' + name + '[' + envelope_id + '][width]"' +
                '       value="' + width + '"' +
                '       class="form-control small-number" />' +
                '</td>' +
                '<td class="' + center + '">' +
                '   <input' +
                '       name="' + name + '[' + envelope_id + '][max_height]"' +
                '       value="' + (max_height != '0' ? max_height : '') + '"' +
                '       class="form-control small-number" />' +
                '</td>' +
                '<td class="' + center + '">' +
                '   <input' +
                '       name="' + name + '[' + envelope_id + '][max_load]"' +
                '       value="' + (max_load != '0' ? max_load : '') + '"' +
                '       class="form-control small-number" />' +
                '</td>' +
                '<td class="' + center + '">' +
                '   <input' +
                '       name="' + name + '[' + envelope_id + '][tare]"' +
                '       value="' + (tare != '0' ? tare : '') + '"' +
                '       class="form-control small-number" />' +
                '</td>' +
                '<td class="' + center + '">' +
                '   <input' +
                '       type="hidden"' +
                '       name="' + name + '[' + envelope_id + '][envelope_id]"' +
                '       value="' + envelope_id + '"/>' +
                '   <a href="#" onclick="objSmartFlexible.removeCustomPackage(this); return false;">' +
                    this.params.textRemovePackage +
            '   </a>' +
            '</td>';
            this.appendHTML(document.getElementById('custom_envelopes_holder'), 'tr', html);
            $('input[name="' + name + '[' + envelope_id + '][length]' + '"], ' +
                'input[name="' + name + '[' + envelope_id + '][width]' + '"]'
            ).on('change', this.handleCustomEnvelopeChange);
            this.handleCustomEnvelopeChange.call(
                $('input[name="' + name + '[' + envelope_id + '][length]' + '"]')
            );
        },
        removeCustomPackage: function(link) {
            var target = $(link).parents('tr')[0];
            target.remove();
        },
        addPromo: function(min_cost, length, width, height, weight) {
            var name = this.params.promo;
            var center = this.params.center;
            this.total_promo++;
            var promo_id = this.total_promo;
            var html =
                '<td class="' + center + '">' +
                '   <input' +
                '       name="' + name + '[' + promo_id + '][min_cost]"' +
                '       value="' + min_cost + '"' +
                '       class="form-control small-number" />' +
                '</td>' +
                '<td class="' + center + '">' +
                '   <input' +
                '       name="' + name + '[' + promo_id + '][length]"' +
                '       value="' + length + '"' +
                '       class="form-control small-number" />' +
                '</td>' +
                '<td class="' + center + '">' +
                '   <input' +
                '       name="' + name + '[' + promo_id + '][width]"' +
                '       value="' + width + '"' +
                '       class="form-control small-number" />' +
                '</td>' +
                '<td class="' + center + '">' +
                '   <input' +
                '       name="' + name + '[' + promo_id + '][height]"' +
                '       value="' + height + '"' +
                '       class="form-control small-number" />' +
                '</td>' +
                '<td class="' + center + '">' +
                '   <input' +
                '       name="' + name + '[' + promo_id + '][weight]"' +
                '       value="' + weight + '"' +
                '       class="form-control small-number" />' +
                '</td>' +
                '<td class="' + center + '">' +
                '   <input' +
                '       type="hidden"' +
                '       name="' + name + '[' + promo_id + '][promo_id]"' +
                '       value="' + promo_id + '" />' +
                '   <a href="#" onclick="objSmartFlexible.removePromo(this); return false;">' +
                    this.params.textRemovePromo +
            '   </a>' +
            '</td>';
            this.appendHTML(document.getElementById('promo_holder'), 'tr', html);
        },
        removePromo: function(link) {
            var target = $(link).parents('tr')[0];
            target.remove();
        },
        addStandardBox: function(id, title, image, size, weight, checked) {
            var name = this.params.standardPackages;
            var path = this.params.imagesPath;
            var html =
                '<div' +
                '   id="standard-box-' + id + '"' +
                '   class="standard-box"' +
                '   onclick="objSmartFlexible.checkStandardBox(this);">' +
                '   <div class="standard-box__image-holder">' +
                '       <img src="' + path + '/' + image + '" />' +
                '       <div class="standard-box__checker' + (checked ? ' _checked' : '') + '">' +
                '           <input' +
                '               name="' + name + '[' + id + ']"' +
                '               type="hidden"' +
                '               value="' + (checked ? '1' : '0') + '" />' +
                '       </div>' +
                '   </div>' +
                '   <div class="standard-box__caption">' +
                '       <div class="standard-box__title">' + title + '</div>' +
                '       <div class="standard-box__dimensions">' + size + '</div>' +
                '       <div class="standard-box__weight">' + weight + '</div>' +
                '   </div>' +
                '</div>';
            var target = checked ? $('#standard-boxes-checked') : $('#standard-boxes-unchecked');
            target.append(html);
        },
        addHoliday: function(month, day) {
            if (this.holidays[month] === undefined) {
                this.holidays[month] = [];
            }
            if (this.holidays[month].indexOf(day) !== -1) {
                return;
            }
            this.holidays[month].push(day);
            this.holidays[month].sort(this.sortNumbers);
            this.showHolidays(month);
        },
        applyFieldInactivity: function(input) {
            if ($(input).val() === '') {
                $(input).parents('.adjustment-rule__field').addClass('_inactive');
            } else {
                $(input).parents('.adjustment-rule__field').removeClass('_inactive');
            }
        },
        applyMasterCheckbox: function(checkbox) {
            if (checkbox.checked) {
                $(checkbox).parents('.adjustment-rule__master').next('.adjustment-rule__row').removeClass('_hidden');
            } else {
                $(checkbox).parents('.adjustment-rule__master').next('.adjustment-rule__row').addClass('_hidden')
                    .find('input').val('');
            }
        },
        addAdjustmentRule: function(data) {
            this.servicesPromise.then(function() {
                var compareOperators = [
                    {key: '', value: ''},
                    {key: 'gt', value: '&gt;'},
                    {key: 'gte', value: '&gt;='},
                    {key: 'eq', value: '='},
                    {key: 'lte', value: '&lt;='},
                    {key: 'lt', value: '&lt;'}
                ];
                var currency = $('input[name*="rate_adj_fix"]').parent().find('span').text();
                var rateCondition = {};
                var totalCondition = {};
                var selectedServices = [''];
                var percentAction = {};
                var fixedAction = {};
                var grouping = {};
                if (data.condition && data.condition.conditions && data.actions) {
                    rateCondition = data.condition.conditions.filter(function(condition) {
                        return condition.type === 'comparison' && condition.argument === 'rate';
                    }).pop() || {};
                    totalCondition = data.condition.conditions.filter(function(condition) {
                        return condition.type === 'comparison' && condition.argument === 'total';
                    }).pop() || {};
                    var servicesCondition = data.condition.conditions.filter(function(condition) {
                        return condition.type === 'services';
                    }).pop() || {};
                    percentAction = data.actions.filter(function(action) {
                        return action.type === 'ratePercent';
                    }).pop() || {};
                    fixedAction = data.actions.filter(function(action) {
                        return action.type === 'rateFixed';
                    }).pop() || {};
                    grouping = data.grouping || {};
                    selectedServices = servicesCondition.services ? servicesCondition.services : [''];
                }
                var services = [{key: '', value: 'Any'}];
                var servicesTopLevelArray = $.makeArray($('.services .services__service'));
                servicesTopLevelArray.forEach(function(s) {
                    var column = $(s).parents('td')[0];
                    $.makeArray($(column).find('.services__group')).forEach(function(g) {
                        services.push({
                            type: 'optgroup',
                            label: (servicesTopLevelArray.length > 1 ? (s.innerText.replace(/ Services/i, '') + ': ') : '') +
                                g.innerText,
                            options: $.makeArray($(g).nextUntil('.services__group', '.checkbox')).map(function(o) {
                                return {
                                    type: 'option',
                                    key: $(o).find('input[type=checkbox]')[0].name.split(/[\[\]]/)[1],
                                    value: o.innerText
                                };
                            })
                        });
                    });
                });
                var name = objSmartFlexible.params.adjustmentRules;
                objSmartFlexible.total_adjustment_rules++;
                var html =
                    '<div class="adjustment-rule"><div class="adjustment-rule__label">' +
                    'If all conditions are satisfied</div>' +
                    '<div class="adjustment-rule__row"><div class="adjustment-rule__col">' +
                    '<div class="adjustment-rule__field">' +
                    '<label class="adjustment-rule__caption" for="adjustment-rule__rate_' + objSmartFlexible.total_adjustment_rules + '">' +
                    'Rate Shipping Price</label>' +
                    '<div class="adjustment-rule__input"><select name="' + name + '[' + objSmartFlexible.total_adjustment_rules + '][rate_operator]" class="adjustment-rule__select">' +
                    objSmartFlexible.options(compareOperators, 'key', 'value', rateCondition.operator ? rateCondition.operator : '') +
                    '</select>&#32;<div class="adjustment-rule__group"><div class="input-group small-number">' +
                    '<input name="' + name + '[' + objSmartFlexible.total_adjustment_rules + '][rate]" id="adjustment-rule__rate_' + objSmartFlexible.total_adjustment_rules + '" class="form-control" value="' + (rateCondition.value ? rateCondition.value : '') + '">' +
                    '<span class="input-group-addon">' + currency + '</span></div></div>' +
                    '</div></div>' +
                    '<div class="adjustment-rule__field">' +
                    '<label class="adjustment-rule__caption" for="adjustment-rule__total_' + objSmartFlexible.total_adjustment_rules + '">' +
                    'Order Total</label>' +
                    '<div class="adjustment-rule__input"><select name="' + name + '[' + objSmartFlexible.total_adjustment_rules + '][total_operator]" class="adjustment-rule__select">' +
                    objSmartFlexible.options(compareOperators, 'key', 'value', totalCondition.operator ? totalCondition.operator : '') +
                    '</select>&#32;<div class="adjustment-rule__group"><div class="input-group small-number">' +
                    '<input name="' + name + '[' + objSmartFlexible.total_adjustment_rules + '][total]" id="adjustment-rule__total_' + objSmartFlexible.total_adjustment_rules + '" class="form-control" value="' + (totalCondition.value ? totalCondition.value : '') + '">' +
                    '<span class="input-group-addon">' + currency + '</span></div></div>' +
                    '</div><div class="adjustment-rule__hint">' +
                    'Total price of all ordered items, does not include shipping and other costs.' +
                    '</div></div></div><div class="adjustment-rule__col"><div class="adjustment-rule__field">' +
                    '<label class="adjustment-rule__caption" for="adjustment-rule__services_' + objSmartFlexible.total_adjustment_rules + '">' +
                    $('a[href="#tab-ext-services"]').text() + '</label>' +
                    '<div class="adjustment-rule__input">' +
                    '<select name="' + name + '[' + objSmartFlexible.total_adjustment_rules + '][services][]" class="adjustment-rule__multiselect" multiple id="adjustment-rule__services_' + objSmartFlexible.total_adjustment_rules + '">' +
                    objSmartFlexible.options(services, 'key', 'value', selectedServices) +
                    '</select></div>' +
                    '<div class="adjustment-rule__hint">Use SHIFT / CTRL (or CMD) keys to select multiple items.</div>' +
                    '</div></div></div><div class="adjustment-rule__label">Apply adjustments</div>' +
                    '<div class="adjustment-rule__row"><div class="adjustment-rule__col">' +
                    '<div class="adjustment-rule__field">' +
                    '<label class="adjustment-rule__caption" for="adjustment-rule__fixed_' + objSmartFlexible.total_adjustment_rules + '">' +
                    'Set Fixed Rate Adjustment</label><div class="adjustment-rule__input">' +
                    '<div class="input-group small-number">' +
                    '<input name="' + name + '[' + objSmartFlexible.total_adjustment_rules + '][fixed]" id="adjustment-rule__fixed_' + objSmartFlexible.total_adjustment_rules + '" class="form-control" value="' + (fixedAction.value ? fixedAction.value : '') + '" onkeyup="objSmartFlexible.applyFieldInactivity(this)">' +
                    '<span class="input-group-addon">' + currency + '</span>' +
                    '</div></div><div class="adjustment-rule__hint">' +
                    'Increase shipping price by this value. Negative numbers are supported.' +
                    '</div></div></div><div class="adjustment-rule__col">' +
                    '<div class="adjustment-rule__field">' +
                    '<label class="adjustment-rule__caption" for="adjustment-rule__percent_' + objSmartFlexible.total_adjustment_rules + '">' +
                    'Set Rate Adjustment by %</label>' +
                    '<div class="adjustment-rule__input"><div class="input-group small-number">' +
                    '<input name="' + name + '[' + objSmartFlexible.total_adjustment_rules + '][percent]" id="adjustment-rule__percent_' + objSmartFlexible.total_adjustment_rules + '" class="form-control" value="' + (percentAction.value ? percentAction.value : '') + '" onkeyup="objSmartFlexible.applyFieldInactivity(this)">' +
                    '<span class="input-group-addon">%</span>' +
                    '</div></div><div class="adjustment-rule__hint">' +
                    'Increase shipping price by this percent. Negative numbers are supported.' +
                    '</div></div></div></div><div class="adjustment-rule__label">Grouping</div>' +
                    '<div class="adjustment-rule__master"><label class="adjustment-rule__caption">' +
                    '<input type="checkbox" ' + (grouping.title ? 'checked' : '') + ' onclick="objSmartFlexible.applyMasterCheckbox(this)" id="adjustment-rule__grouping_' + objSmartFlexible.total_adjustment_rules + '"> Group matched rates under a single title</label></div>' +
                    '<div class="adjustment-rule__row">' +
                    '<div class="adjustment-rule__col"><div class="adjustment-rule__field">' +
                    '<label class="adjustment-rule__caption" for="adjustment-rule__group_' + objSmartFlexible.total_adjustment_rules + '">Rate Title</label>' +
                    '<div class="adjustment-rule__input"><input name="' + name + '[' + objSmartFlexible.total_adjustment_rules + '][group]" id="adjustment-rule__group_' + objSmartFlexible.total_adjustment_rules + '" class="form-control" value="' + (grouping.title ? grouping.title : '') + '"></div>' +
                    '</div></div><div class="adjustment-rule__col adjustment-rule__hint">When multiple rates are grouped under a single title, the cheapest one should be picked in order to generate labels.</div></div>' +
                    '<a class="btn btn-default adjustment-rule__remove" href="#" onclick="this.parentNode.parentNode.removeChild(this.parentNode); return false">' +
                    'Remove This Rule</a>' +
                    '</div>';
                var btn = $('.adjustment-rule__add')[0];
                var div = document.createElement('div');
                div.innerHTML = html;
                btn.parentNode.insertBefore(div.firstChild, btn);
                objSmartFlexible.applyFieldInactivity($('#adjustment-rule__fixed_' + objSmartFlexible.total_adjustment_rules)[0]);
                objSmartFlexible.applyFieldInactivity($('#adjustment-rule__percent_' + objSmartFlexible.total_adjustment_rules)[0]);
                objSmartFlexible.applyMasterCheckbox($('#adjustment-rule__grouping_' + objSmartFlexible.total_adjustment_rules)[0]);
            });
        },
        showHolidays: function (month) {
            var name = this.params.processingHolidays;
            var target = $('#holidays_' + month);
            if (this.holidays[month].length > 0) {
                target.addClass('_active');
            } else {
                target.removeClass('_active');
            }
            target = $('#holidays_' + month + ' > .holidays__list');
            target.find('.holidays__item').remove();
            target.find('input').remove();
            var groups = [];
            var lastDay = null;
            var html = '';
            this.holidays[month].forEach(function(day) {
                this.total_holidays++;
                html += '<input ' +
                '   type="hidden"' +
                '   name="' + name + '[' + this.total_holidays + '][0]" ' +
                '   value="' + month + '" />' +
                '<input ' +
                '   type="hidden"' +
                '   name="' + name + '[' + this.total_holidays + '][1]" ' +
                '   value="' + day + '" />';
                if (day - lastDay === 1 && lastDay !== null) {
                    groups[groups.length - 1].push(day);
                } else {
                    groups.push([day]);
                }
                lastDay = day;
            }, this);
            target.append(html);
            groups.forEach(function(group) {
                var firstDay = group[0];
                var lastDay = group[group.length - 1];
                target.append('<div class="holidays__item">' +
                (firstDay === lastDay ? firstDay : firstDay + '&ndash;' + lastDay) +
                '</div>');
            });
        },
        validateHolidays: function(frm, month) {
            var source = $(frm).find('input').val();
            var holidays = source.split(',');
            holidays = holidays.map(function(v) {
                return parseInt(v, 10);
            }).filter(function(v) {
                return (v >= 1) && (v <= 31);
            });
            this.holidays[month] = [];
            if (holidays.length > 0) {
                holidays.forEach(function(day) {
                    this.addHoliday(month, day);
                }, this);
                return;
            }
            this.showHolidays(month);
        },
        importHolidays: function() {
            /* https://gist.github.com/mattn/1438183 */
            var cal = encodeURIComponent($('select.google-holidays').val());
            var key = 'AIzaSyB_Ze7RT4a2gz6l188iLCUo4Z0S8uSg0OY';
            $.ajax({
                type: 'GET',
                url: 'https://www.googleapis.com/calendar/v3/calendars/' + cal + '/events?key=' + key,
                dataType: 'json',
                success: function(response) {
                    if (!response.items) {
                        return;
                    }
                    response.items.forEach(function(item) {
                        var dateParts = item.start.date.split('-');
                        if (parseInt(dateParts[0], 10) === (new Date()).getFullYear()) {
                            this.addHoliday(parseInt(dateParts[1], 10), parseInt(dateParts[2], 10));
                        }
                    }, this);
                }.bind(this),
                error: function(response) {
                    alert(
                        response.responseJSON.error.errors[0].message + ': ' +
                        response.responseJSON.error.errors[0].reason
                    );
                }
            });
        },
        clearHolidays: function() {
            for(var i = 1; i <= 12; i++) {
                this.holidays[i] = [];
                this.showHolidays(i);
            }
        },
        checkStandardBox: function(div) {
            var checker = $(div).find('.standard-box__checker')[0];
            var input = $(checker).find('input');
            if ($(checker).hasClass('_checked')) {
                $(checker).removeClass('_checked');
                input.attr('value', 0);
                this.moveElementTo(div, $('#standard-boxes-unchecked'));
            } else {
                $(checker).addClass('_checked');
                input.attr('value', 1);
                this.moveElementTo(div, $('#standard-boxes-checked'));
            }
        },
        addUserIdListener: function() {
            var name = this.params.userId;
            var input = $('input[name="' + name + '"]');
            if (this.params.methodsDependOnUserId) {
                this.fetchServicesByUser();
                input.on('change', this.fetchServicesByUser);
            }
        },
        addPackerListener: function() {
            var name = this.params.packer;
            var select = $('select[name="' + name + '"]');
            select.on('change', this.handlePackerChange);
            this.handlePackerChange.call(select);
        },
        addLabelListener: function() {
            var name = this.params.label;
            var options = $('input[name="' + name + '"]');
            options.on('change', this.handleLabelChange);
            this.handleLabelChange.call(options);
        },
        addLabelFormatListener: function() {
            var name = this.params.labelFormat;
            var select = $('select[name="' + name + '"]');
            select.on('change', this.handleLabelFormatChange);
            this.handleLabelFormatChange.call(select);
        },
        addPostcodeListener: function() {
            var name = this.params.postcode;
            var input = $('input[name="' + name + '"]');
            input.on('change', this.handlePostcodeChange);
            this.handlePostcodeChange.call(input);
        },
        addImportSettingsListener: function() {
            var link = $('#import-settings');
            var uploader = $('#upload');
            link.click(function() {
                alert('Just drag and drop the settings file on this link');
            });
            link.on('dragover', function(e) {
                e.preventDefault();
                e.stopPropagation();
                $(this).addClass('_hover');
            });
            link.on('dragleave', function(e) {
                e.preventDefault();
                e.stopPropagation();
                $(this).removeClass('_hover');
            });
            link.on('drop', function(e) {
                e.preventDefault();
                e.stopPropagation();
                $(this).removeClass('_hover');
                var file = e.originalEvent.dataTransfer.files[0];
                uploader.find('input[name="source"]').prop('files', e.originalEvent.dataTransfer.files);
                uploader.css('display', 'flex');
            });
            uploader.find('#upload__cancel').on('click', function() {
                uploader.hide();
            });
        },
        addHolidayListener: function() {
            var _obj = this;
            $('.holidays__month').click(function(e) {
                var edit = $('.holidays__edit');
                e.stopPropagation();
                edit.remove();
                var month = parseInt(this.id.match(/\d+/), 10);
                if (_obj.holidays[month] === undefined) {
                    _obj.holidays[month] = [];
                }
                $(this).append('<div class="holidays__edit">' +
                '<input value="' + _obj.holidays[month].join(',') + '" placeholder="1,2,15,...">' +
                '<button>OK</button>' +
                '</div>');
                var editInput = $('.holidays__edit input');
                editInput.focus();
                if (editInput[0].setSelectionRange) {
                    editInput[0].setSelectionRange(editInput.val().length, editInput.val().length);
                }
                edit.click(function(e) {
                    e.stopPropagation();
                });
                $('.holidays__edit button').click(function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    $('.holidays__edit').remove();
                    _obj.validateHolidays(this.parentNode, month);
                });
            });
        },
        addInsuranceListener: function() {
            var name = this.params.insurance;
            var options = $('input[name="' + name + '"]');
            options.on('change', this.handleInsuranceChange);
            this.handleInsuranceChange.call(options);
        },
        proceedHide: function(whatToHide, hideOn) {
            var source = this;
            if ($(this).is('input[type="radio"]')) {
                source = $(this).filter(':checked');
            }
            whatToHide.map(function(obj) {
                var isMustBeHidden = false;
                if (Array.isArray(hideOn)) {
                    isMustBeHidden = hideOn.indexOf($(source).val()) > -1;
                } else {
                    isMustBeHidden = $(source).val() === hideOn;
                }
                if (isMustBeHidden) {
                    $(obj).hide();
                } else {
                    $(obj).show();
                }
            }.bind(this));
        },
        addMeasurementSystemListener: function() {
            var name = this.params.measurementSystem;
            var target = $('select[name="' + name + '"]');
            target.data('oldValue', target.val());
            target.on('change', function(e) {
                var form = e.target.form;
                $(form).append(
                    '<input type="hidden" name="measurement_system_old" value="' +
                    target.data('oldValue') + '">'
                );
                $(form).append('<input type="hidden" name="measurement_system_changed" value="1">');
                form.submit();
            });
        },
        addCountryListener: function() {
            this.fetchZones();
            var select = $('select[name="' + this.params.countryId + '"]');
            select.on('change', this.fetchZones);
            if (this.params.methodsDependOnShipperCountry) {
                this.fetchServicesByCountry();
                select.on('change', this.fetchServicesByCountry);
            }
        },
        addBillingSameListener: function() {
            var name = this.params.billingSame;
            var options = $('input[name="' + name + '"]');
            options.on('change', this.handleBillingSameChange);
            this.handleBillingSameChange.call(options);
        },
        addBillingCountryListener: function() {
            this.fetchBillingZones();
            var select = $('select[name="' + this.params.billingCountryId + '"]');
            select.on('change', this.fetchBillingZones);
        },
        handleBillingSameChange: function() {
            var hideOn = '1';
            var country_id = objSmartFlexible.params.billingCountryId;
            var zone_id = objSmartFlexible.params.billingZoneId;
            var city = objSmartFlexible.params.billingCity;
            var postcode = objSmartFlexible.params.billingPostcode;
            var address = objSmartFlexible.params.billingAddress;
            var whatToHide = [
                $('select[name="' + country_id + '"]').parents('.form-group, tr'),
                $('select[name="' + zone_id + '"]').parents('.form-group, tr'),
                $('input[name="' + city + '"]').parents('.form-group, tr'),
                $('input[name="' + postcode + '"]').parents('.form-group, tr'),
                $('textarea[name="' + address + '"]').parents('.form-group, tr'),
            ];
            objSmartFlexible.proceedHide.call(this, whatToHide, hideOn);
        },
        handlePackerChange: function() {
            var hideOn = [objSmartFlexible.params.packerIndividual, objSmartFlexible.params.packerWeightBased];
            var whatToHide = [
                $('#standard-boxes-checked').parents('.form-group, tr'),
                $('#custom_packages_holder').parents('.form-group, tr'),
                $('#custom_envelopes_holder').parents('.form-group, tr'),
                $('#promo_holder').parents('.form-group, tr')
            ];
            objSmartFlexible.proceedHide.call(this, whatToHide, hideOn);
            hideOn = [objSmartFlexible.params.packer3dPacker, objSmartFlexible.params.packerWeightBased];
            whatToHide = [
                $('input[name="' + objSmartFlexible.params.individualTare + '"]').parents('.form-group, tr')
            ];
            objSmartFlexible.proceedHide.call(this, whatToHide, hideOn);
            hideOn = [objSmartFlexible.params.packer3dPacker, objSmartFlexible.params.packerIndividual];
            whatToHide = [
                $('input[name="' + objSmartFlexible.params.weightBasedLimit + '"]').parents('.form-group, tr'),
                $('input[name*="' + objSmartFlexible.params.weightBasedFakedBox + '"]').parents('.form-group, tr')
            ];
            objSmartFlexible.proceedHide.call(this, whatToHide, hideOn);
        },
        handleLabelChange: function() {
            var hideOn = objSmartFlexible.params.labelDisabled;
            var label_format = objSmartFlexible.params.labelFormat;
            var tracking = objSmartFlexible.params.tracking;
            var country_id = objSmartFlexible.params.countryId;
            var zone_id = objSmartFlexible.params.zoneId;
            var city = objSmartFlexible.params.city;
            var address = objSmartFlexible.params.address;
            var postcode_dub = objSmartFlexible.params.postcodeDub;
            var sender_name = objSmartFlexible.params.senderName;
            var sender_company = objSmartFlexible.params.senderCompany;
            var sender_telephone = objSmartFlexible.params.senderTelephone;
            var tab = $('#tab-ext-labels');
            var whatToHide = [
                $('select[name="' + label_format + '"]').parents('.form-group, tr'),
                $('select[name="' + tracking + '"]').parents('.form-group, tr'),
                $(tab).find('select[name="' + country_id + '"]').parents('.form-group, tr'),
                $(tab).find('select[name="' + zone_id + '"]').parents('.form-group, tr'),
                $(tab).find('input[name="' + city + '"]').parents('.form-group, tr'),
                $(tab).find('textarea[name="' + address + '"]').parents('.form-group, tr'),
                $(tab).find('input[name="' + postcode_dub + '"]').parents('.form-group, tr'),
                $(tab).find('input[name="' + sender_name + '"]').parents('.form-group, tr'),
                $(tab).find('input[name="' + sender_company + '"]').parents('.form-group, tr'),
                $(tab).find('input[name="' + sender_telephone + '"]').parents('.form-group, tr')
            ];
            objSmartFlexible.proceedHide.call(this, whatToHide, hideOn);
            hideOn = objSmartFlexible.params.labelManually;
            var tracking_immediately = objSmartFlexible.params.trackingImmediately;
            whatToHide = [
                $('option[value="' + tracking_immediately + '"]')
            ];
            objSmartFlexible.proceedHide.call(this, whatToHide, hideOn);
        },
        handleLabelFormatChange: function() {
            var hideOn = objSmartFlexible.params.labelOriginal;
            var pdf_converter = objSmartFlexible.params.pdfConverter;
            var whatToHide = [
                $('select[name="' + pdf_converter + '"]').parents('.form-group, tr')
            ];
            objSmartFlexible.proceedHide.call(this, whatToHide, hideOn);
        },
        handlePostcodeChange: function() {
            var name = objSmartFlexible.params.postcodeDub;
            $('input[name="' + name + '"]').val($(this).val());
        },
        handleCustomBoxChange: function() {
            var measureName = objSmartFlexible.params.measurementSystem;
            var row = $(this).parents('tr')[0];
            var data = {
                route: objSmartFlexible.params.currentRoute + '/tareofcustompackagefixed',
                length: $(row).find('input[name*="length"]').val(),
                width: $(row).find('input[name*="width"]').val(),
                height: $(row).find('input[name*="height"]').val(),
                measurement_system: $('select[name="' + measureName + '"]').val()
            };
            data[objSmartFlexible.params.tokenName] = objSmartFlexible.params.token;
            var handlerSuccess = function(tare) {
                $(row).find('input[name*="tare"]').attr('placeholder', tare);
            };
            var handlerError = function() {
                $(row).find('input[name*="tare"]').attr('placeholder', '');
            };
            $.ajax({
                url: objSmartFlexible.params.entryPoint,
                data: data,
                success: handlerSuccess,
                error: handlerError,
                dataType: 'text'
            });
        },
        handleCustomEnvelopeChange: function() {
            var measureName = objSmartFlexible.params.measurementSystem;
            var row = $(this).parents('tr')[0];
            var data = {
                route: objSmartFlexible.params.currentRoute + '/tareofcustompackageexpandable',
                length: $(row).find('input[name*="length"]').val(),
                width: $(row).find('input[name*="width"]').val(),
                measurement_system: $('select[name="' + measureName + '"]').val()
            };
            data[objSmartFlexible.params.tokenName] = objSmartFlexible.params.token;
            var handlerSuccess = function(tare) {
                $(row).find('input[name*="tare"]').attr('placeholder', tare);
            };
            var handlerError = function() {
                $(row).find('input[name*="tare"]').attr('placeholder', '');
            };
            $.ajax({
                url: objSmartFlexible.params.entryPoint,
                data: data,
                success: handlerSuccess,
                error: handlerError,
                dataType: 'text'
            });
        },
        handleInsuranceChange: function() {
            var whatToHide = [
                $('input[name="' + objSmartFlexible.params.insuranceFrom + '"]').parent(),
                $('#insurance_from_help')
            ];
            objSmartFlexible.proceedHide.call(this, whatToHide, '0');
        },
        fetchServicesByCountry: function() {
            objSmartFlexible.servicesPromise = $.get(
                objSmartFlexible.params.entryPoint + '&route=' + objSmartFlexible.params.currentRoute +
                '/services&' + objSmartFlexible.params.tokenName + '=' + objSmartFlexible.params.token +
                '&country_id=' + $('select[name="' + objSmartFlexible.params.countryId + '"]').val(), {},
                function(response) {
                    $('table.services').parent().html(response);
                }
            ).fail(function(xhr) {
                $('table.services').html('<tr><td><p class="error text-danger">' + xhr.responseText +
                '</p></td></tr>');
            });
        },
        fetchServicesByUser: function() {
            var userId = $('input[name="' + objSmartFlexible.params.userId + '"]');
            objSmartFlexible.servicesPromise = $.get(objSmartFlexible.params.entryPoint +
                '&route=' + objSmartFlexible.params.currentRoute + '/services&' + objSmartFlexible.params.tokenName +
                '=' + objSmartFlexible.params.token + (userId.length ? ('&user_id=' + userId.val()) : ''), {},
                function(response, status) {
                    userId.parent().find('.error, .text-danger').remove();
                    $('table.services').parent().html(response);
                    objSmartFlexible.highlightTabsWithError();
                }
            ).fail(function(xhr) {
                userId.parent().find('.error, .text-danger').remove();
                $('table.services').html('<tr><td><p class="error text-danger">' + xhr.responseText +
                '</p></td></tr>');
                userId.parent().append('<p class="error text-danger">' + xhr.responseText + '</p>');
                objSmartFlexible.highlightTabsWithError();
            });
        },
        fetchZones: function() {
            $('select[name="' + objSmartFlexible.params.zoneId + '"]').parent().load(
                objSmartFlexible.params.entryPoint + '&route=' + objSmartFlexible.params.currentRoute +
                '/zone&' + objSmartFlexible.params.tokenName + '=' + objSmartFlexible.params.token + '&country_id=' +
                $('select[name="' + objSmartFlexible.params.countryId + '"]').val(),
                function(response, status) {
                    if (status == 'error') {
                        $(this).html('');
                    }
                }
            );
        },
        fixDocumentHeight: function() { // Opera 12.16 fix
            var holder = $('#label');
            var img = holder.find('img');
            if (img.height() > holder.height()) {
                img.css('height', '100%');
            }
        },
        printFrame: function(id, fallbackUrl) {
            var frame = $('#document')[0];
            var fwindow = frame.contentWindow;
            fwindow.focus();
            try {
                fwindow.print();
            } catch (e) {
                alert('Your browser does not allow to print a window frame. ' +
                'So, press Cmd+P (Mac) or Ctrl+P (Windows) after closing this message');
                window.location = fallbackUrl;
            }
        },
        fetchBillingZones: function() {
            $('select[name="' + objSmartFlexible.params.billingZoneId + '"]').parent().load(
                objSmartFlexible.params.entryPoint + '&route=' + objSmartFlexible.params.currentRoute +
                '/billingzone&' + objSmartFlexible.params.tokenName + '=' + objSmartFlexible.params.token +
                '&country_id=' + $('select[name="' + objSmartFlexible.params.billingCountryId + '"]').val(),
                function(response, status) {
                    if (status == 'error') {
                        $(this).html('');
                    }
                }
            );
        },
        requestLabels: function(url, button) {
            var originalText = $(button).text();
            $(button).text('Requesting...').attr('disabled', true);
            var handleError = function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + (xhr.responseText ? xhr.responseText :
                    'Request was terminated'));
                $(button).text(originalText).attr('disabled', false);
            };
            var addHistory = function(entry, token, orderId, statusId, notify, comment, override, tokenName) {
                $.ajax({
                    url: entry + '&route=api/order/history&' + tokenName + '=' + token +
                        '&order_id=' + orderId,
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        order_status_id: statusId,
                        notify: notify,
                        comment: comment,
                        override: override
                    },
                    success: function(json) {
                        if (json.error) {
                            alert(json.error);
                            $(button).text(originalText).attr('disabled', false);
                        }
                        if (json.success) {
                            window.location.reload(-1);
                        }
                    },
                    error: handleError
                });
            };
            var getToken = function(entry, key, username, password, cb) {
                $.ajax({
                    url: entry + '&route=api/login',
                    type: 'POST',
                    data: {
                        key: key,
                        username: username,
                        password: password
                    },
                    dataType: 'json',
                    success: function(json) {
                        if (json.error) {
                            if (json.error.key) {
                                alert(json.error.key);
                            }
                            if (json.error.ip) {
                                alert(json.error.ip)
                            }
                            $(button).text(originalText).attr('disabled', false);
                        }
                        if (json.token || json.cookie) {
                            $(button).text('Logging...');
                            cb(json.token);
                        } else {
                            alert('token or cookie expected');
                        }
                    },
                    error: handleError
                });
            };
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function(json) {
                    if (json.error) {
                        alert(json.error);
                        $(button).text(originalText).attr('disabled', false);
                    }
                    if (json.next === 'redirect') {
                        window.location.reload(-1);
                    } else if (json.next === 'ajax') {
                        if (json.api_key || json.api_token || (json.api_username && json.api_password)) {
                            if (json.entry && json.order_id && json.history) {
                                var finalAction = function (token) {
                                    addHistory(
                                        json.entry, token, json.order_id, json.history.order_status_id,
                                        json.history.notify, json.history.comment, json.history.override,
                                        json.api_token ? 'api_token' : 'token'
                                    );
                                };
                                if (json.api_token) {
                                    finalAction(json.api_token);
                                } else {
                                    $(button).text('Token...');
                                    getToken(
                                        json.entry, json.api_key, json.api_username, json.api_password, finalAction
                                    );
                                }
                            } else {
                                alert('entry, order or history information missing');
                            }
                        } else {
                            alert('api credentials missing');
                        }
                    }
                },
                error: handleError
            });
        },
        getPackagingMapApplication: function() {
            /**
             * @author mrdoob / http://mrdoob.com/
             */
            return {
                Player: function() {
                    var loader = new THREE.ObjectLoader();
                    var raycaster = new THREE.Raycaster();
                    var camera, scene, renderer;
                    var box, mouse = {
                        x: 0, y: 0, isDown: false,
                        downX: 0, downY: 0,
                        rotatedX: 0, rotatedY: 0,
                        vector: new THREE.Vector2()
                    };
                    var descriptor = $('#description');
                    this.dom = document.createElement('div');
                    this.width = 500;
                    this.height = 500;
                    this.load = function(json) {
                        try {
                            renderer = new THREE.WebGLRenderer({antialias: true});
                        } catch (e) {
                            descriptor.html(
                                '<strong>Error loading WebGL Renderer.</strong><br>' +
                                'Please read <a href="https://superuser.com/a/836833">this manual</a> ' +
                                'to solve this problem.<br><br>' +
                                'Original error message:<br>' +
                                '<em>' + e + '</em>'
                            ).show();
                            return false;
                        }
                        renderer.setClearColor(0x000000);
                        renderer.setPixelRatio(window.devicePixelRatio);
                        if (json.project.gammaInput) { renderer.gammaInput = true; }
                        if (json.project.gammaOutput) { renderer.gammaOutput = true; }
                        if (json.project.shadows) { renderer.shadowMap.enabled = true; }
                        this.dom.appendChild(renderer.domElement);
                        this.setScene(loader.parse(json.scene));
                        this.setCamera(loader.parse(json.camera));
                        box = scene.getObjectByName('Box');
                        document.addEventListener('mousemove', function(event) {
                            mouse.x = event.clientX;
                            mouse.y = event.clientY;
                            if (mouse.isDown) {
                                var rotateY = mouse.rotatedY + (mouse.downY - mouse.y) / 120;
                                var rotateX = mouse.rotatedX + (mouse.downX - mouse.x) / 120;
                                box.rotation.y = rotateX;
                                box.rotation.z = rotateY;
                            }
                            mouse.vector.x = (mouse.x / window.innerWidth) * 2 - 1;
                            mouse.vector.y = -(mouse.y / window.innerHeight) * 2 + 1;
                            raycaster.setFromCamera(mouse.vector, camera);
                            var intersects = raycaster.intersectObjects(box.children);
                            if (intersects.length > 0 && !mouse.isDown) {
                                descriptor.html(intersects[0].object.userData.description);
                                descriptor.css('left', mouse.x + 'px');
                                descriptor.css('top', mouse.y + 'px');
                                descriptor.show();
                            } else {
                                descriptor.hide();
                            }
                        }, false);
                        document.body.addEventListener('mousedown', function(event) {
                            mouse.isDown = true;
                            mouse.downX = event.clientX;
                            mouse.downY = event.clientY;
                            mouse.rotatedX = box.rotation.y;
                            mouse.rotatedY = box.rotation.z;
                        }, false);
                        document.body.addEventListener('mouseup', function(event) {
                            mouse.isDown = false;
                        }, false);
                        return true;
                    };

                    this.setCamera = function(value) {
                        camera = value;
                        camera.aspect = this.width / this.height;
                        camera.updateProjectionMatrix();
                        camera.lookAt(scene.position);
                    };

                    this.setScene = function(value) {
                        scene = value;
                    };

                    this.setSize = function(width, height) {
                        this.width = width;
                        this.height = height;
                        if (camera) {
                            camera.aspect = this.width / this.height;
                            camera.updateProjectionMatrix();
                        }
                        if (renderer) {
                            renderer.setSize(width, height);
                        }
                    };

                    var prevTime, request;
                    function animate(time) {
                        setTimeout(function() {
                            request = requestAnimationFrame(animate);
                            renderer.render(scene, camera);
                        }, 100);
                        prevTime = time;
                    }

                    this.play = function() {
                        request = requestAnimationFrame(animate);
                        prevTime = performance.now();
                    };

                    this.stop = function() {
                        cancelAnimationFrame(request);
                    };

                    this.dispose = function() {
                        var totalChildren = this.dom.children.length;
                        for (var i = 0; i < totalChildren; i++) {
                            this.dom.removeChild(this.dom.firstChild);
                        }
                        renderer.dispose();
                        camera = undefined;
                        scene = undefined;
                        renderer = undefined;
                    };
                }
            };
        }
    }
})(jQuery);
