function Hide(element, seconds)//funciton to hide popup after 3 seconds
{
    if (element) {
        setTimeout(function() {
            element.style.display = 'none';
        }, seconds * 1000);
    }
}

$(function() {
    var hover = 1
    // var click=0;
    var moveLeft = 0;
    var moveDown = 0;
    $('div.popper').hover(function(e) { //Function to show popup on mousehover
        if (hover == 1)
            var target = '#' + ($(this).attr('data-popbox'));

        $(target).show();
        moveLeft = $(this).outerWidth();
        moveDown = ($(target).outerHeight() / 2);
    }, function() {
        var target = '#' + ($(this).attr('data-popbox'));
        if (!($("div.popper").hasClass("show"))) {
            $(target).hide(); //dont hide popup if it is clicked
        }
    });
    $('div.popper').click(function(e) { //Function to make popup static on click


        hover = 0;
        var target = '#' + ($(this).attr('data-popbox'));
        if (!($(this).hasClass("show")))
        {
            $(target).show();
            //Funciton calls to hide popup after 3 seconds
            Hide(document.getElementById('pop25'), 3);
            Hide(document.getElementById('pop24'), 3);
            Hide(document.getElementById('pop23'), 3);
            Hide(document.getElementById('pop22'), 3);
            Hide(document.getElementById('pop21'), 3);

            Hide(document.getElementById('pop20'), 3);
            Hide(document.getElementById('pop19'), 3);
            Hide(document.getElementById('pop18'), 3);
            Hide(document.getElementById('pop17'), 3);
            Hide(document.getElementById('pop16'), 3);

            Hide(document.getElementById('pop15'), 3);
            Hide(document.getElementById('pop14'), 3);
            Hide(document.getElementById('pop13'), 3);
            Hide(document.getElementById('pop12'), 3);
            Hide(document.getElementById('pop11'), 3);

            Hide(document.getElementById('pop10'), 3);
            Hide(document.getElementById('pop9'), 3);
            Hide(document.getElementById('pop8'), 3);
            Hide(document.getElementById('pop7'), 3);
            Hide(document.getElementById('pop6'), 3);

            Hide(document.getElementById('pop5'), 3);
            Hide(document.getElementById('pop4'), 3);
            Hide(document.getElementById('pop3'), 3);
            Hide(document.getElementById('pop2'), 3);
            Hide(document.getElementById('pop1'), 3);



        }
        $(this).toggleClass("show");


    });

    $('div.popper').mousemove(function(e) { //Function to calculate popup position
        var target = '#' + ($(this).attr('data-popbox'));

        leftD = e.pageX + parseInt(moveLeft);
        maxRight = leftD + $(target).outerWidth();
        windowLeft = $(window).width() - 40;
        windowRight = 0;
        maxLeft = e.pageX - (parseInt(moveLeft) + $(target).outerWidth() + 20);

        if (maxRight > windowLeft && maxLeft > windowRight)
        {
            leftD = maxLeft;
        }

        topD = e.pageY - parseInt(moveDown);
        maxBottom = parseInt(e.pageY + parseInt(moveDown) + 20);
        windowBottom = parseInt(parseInt($(document).scrollTop()) + parseInt($(window).height()));
        maxTop = topD;
        windowTop = parseInt($(document).scrollTop());
        if (maxBottom > windowBottom)
        {
            topD = windowBottom - $(target).outerHeight() - 20;
        } else if (maxTop < windowTop) {
            topD = windowTop + 20;
        }

        $(target).css('top', topD).css('left', leftD);


    });

});


