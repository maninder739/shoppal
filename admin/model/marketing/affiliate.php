<?php

class ModelMarketingAffiliate extends Model {

    public function addAffiliate($data) {
        $this->db->query("INSERT INTO `" . DB_PREFIX . "url_alias` SET  query = 'common/home', keyword = '" . $this->db->escape(strtolower($this->db->escape($data['code']))) . "'");

        $this->db->query("INSERT INTO " . DB_PREFIX . "affiliate SET member_type = '1', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', extension = '" . $this->db->escape($data['extension']) . "', fax = '" . $this->db->escape($data['fax']) . "', salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', company = '" . $this->db->escape($data['company']) . "', website = '" . $this->db->escape($data['website']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', city = '" . $this->db->escape($data['city']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = '" . (int) $data['country_id'] . "', zone_id = '" . (int) $data['zone_id'] . "', code = '" . $this->db->escape($data['code']) . "', commission = '" . (float) $data['commission'] . "', tax = '" . $this->db->escape($data['tax']) . "', payment = '" . $this->db->escape($data['payment']) . "', cheque = '" . $this->db->escape($data['cheque']) . "', paypal = '" . $this->db->escape($data['paypal']) . "', bank_name = '" . $this->db->escape($data['bank_name']) . "', bank_branch_number = '" . $this->db->escape($data['bank_branch_number']) . "', bank_swift_code = '" . $this->db->escape($data['bank_swift_code']) . "', bank_account_name = '" . $this->db->escape($data['bank_account_name']) . "', bank_account_number = '" . $this->db->escape($data['bank_account_number']) . "', status = '" . (int) $data['status'] . "', date_added = NOW()");

        $getLastId = $this->db->getLastId();

        $this->db->query("INSERT INTO `" . DB_PREFIX . "affiliates_fundraisers` SET  affiliate_id = '" . $getLastId . "'");

        return $getLastId;
    }

    public function editAffiliate($affiliate_id, $data) {

        if (!empty($this->request->files['custom_logo']['tmp_name'])) {
            $uploads_dir = '../image/data/';
            if (is_uploaded_file($this->request->files['custom_logo']['tmp_name'])) {
                $path = $this->request->files['custom_logo']['name'];
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $image = substr(md5(uniqid(rand(), true)), 0, 12) . ".$ext";
                move_uploaded_file($this->request->files['custom_logo']['tmp_name'], $uploads_dir . $image);
                $custom_logo = $image;
            }
        } else {
            $custom_logo = $data['old_custom_logo'];
        }

        /**
         * member_image1 image start here
         */
        if (!empty($this->request->files['member_image1']['tmp_name'])) {
            if (is_uploaded_file($this->request->files['member_image1']['tmp_name'])) {
                $file = $this->request->files['member_image1']['tmp_name'];
                $sourceProperties = getimagesize($file);
                $fileNewName = substr(md5(uniqid(rand(), true)), 0, 12);
                $folderPath = "../image/data/";
                $ext = pathinfo($this->request->files['member_image1']['name'], PATHINFO_EXTENSION);
                $imageType = $sourceProperties[2];

                switch ($imageType) {

                    case IMAGETYPE_PNG:

                        $imageResourceId = imagecreatefrompng($file);
                        $targetLayer = $this->imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1], 550, 550);
                        imagepng($targetLayer, $folderPath . "thump_" . $fileNewName . '.' . $ext);
                        break;


                    case IMAGETYPE_GIF:

                        $imageResourceId = imagecreatefromgif($file);
                        $targetLayer = $this->imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1], 550, 550);
                        imagegif($targetLayer, $folderPath . "thump_" . $fileNewName . '.' . $ext);
                        break;


                    case IMAGETYPE_JPEG:

                        $imageResourceId = imagecreatefromjpeg($file);
                        $targetLayer = $this->imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1], 550, 550);
                        imagejpeg($targetLayer, $folderPath . "thump_" . $fileNewName . '.' . $ext);
                        break;


                    default:
                        echo "Invalid Image type.";
                        exit;
                        break;
                }

                move_uploaded_file($file, $folderPath . $fileNewName . "." . $ext);
                $member_image1 = $fileNewName . "." . $ext;
            }
        } else {
            $member_image1 = $data['member_image_old'];
        }


        /**
         * member_image2 image start here
         */
        if (!empty($this->request->files['member_image2']['tmp_name'])) {
            if (is_uploaded_file($this->request->files['member_image2']['tmp_name'])) {
                $file = $this->request->files['member_image2']['tmp_name'];
                $sourceProperties = getimagesize($file);
                $fileNewName = substr(md5(uniqid(rand(), true)), 0, 12);
                $folderPath = "../image/data/";
                $ext = pathinfo($this->request->files['member_image2']['name'], PATHINFO_EXTENSION);
                $imageType = $sourceProperties[2];

                switch ($imageType) {
                    case IMAGETYPE_PNG:
                        $imageResourceId = imagecreatefrompng($file);
                        $targetLayer = $this->imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1], 600, 400);
                        imagepng($targetLayer, $folderPath . "thump_" . $fileNewName . '.' . $ext);
                        break;


                    case IMAGETYPE_GIF:
                        $imageResourceId = imagecreatefromgif($file);
                        $targetLayer = $this->imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1], 600, 400);
                        imagegif($targetLayer, $folderPath . "thump_" . $fileNewName . '.' . $ext);
                        break;


                    case IMAGETYPE_JPEG:

                        $imageResourceId = imagecreatefromjpeg($file);
                        $targetLayer = $this->imageResize($imageResourceId, $sourceProperties[0], $sourceProperties[1], 600, 400);
                        imagejpeg($targetLayer, $folderPath . "thump_" . $fileNewName . '.' . $ext);
                        break;


                    default:
                        echo "Invalid Image type.";
                        exit;
                        break;
                }
                move_uploaded_file($file, $folderPath . $fileNewName . "." . $ext);
                $member_image2 = $fileNewName . "." . $ext;
            }
        } else {
            $member_image2 = $data['member_image_old2'];
        }

        $query_url_alias = $this->db->query("SELECT * FROM `" . DB_PREFIX . "url_alias` WHERE query = 'common/home' AND keyword = '" . $this->db->escape(strtolower($this->db->escape($data['code']))) . "'");
        if (!$query_url_alias->row) {
            $this->db->query("INSERT INTO `" . DB_PREFIX . "url_alias` SET  query = 'common/home', keyword = '" . $this->db->escape(strtolower($this->db->escape($data['code']))) . "'");
        }



        $this->db->query("UPDATE " . DB_PREFIX . "affiliate SET member_type = '" . $this->db->escape($data['member_type']) . "',firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', extension = '" . $this->db->escape($data['extension']) . "' , fax = '" . $this->db->escape($data['fax']) . "', company = '" . $this->db->escape($data['company']) . "',custom_logo = '" . $this->db->escape($custom_logo) . "',mission = '" . $this->db->escape($data['mission']) . "', mission_french = '" . $this->db->escape($data['mission_french']) . "', slider_image = '" . $this->db->escape($data['slider_image']) . "', website = '" . $this->db->escape($data['website']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', city = '" . $this->db->escape($data['city']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = '" . (int) $data['country_id'] . "', zone_id = '" . (int) $data['zone_id'] . "', code = '" . $this->db->escape($data['code']) . "', commission = '" . (float) $data['commission'] . "', tax = '" . $this->db->escape($data['tax']) . "', payment = '" . $this->db->escape($data['payment']) . "', cheque = '" . $this->db->escape($data['cheque']) . "', paypal = '" . $this->db->escape($data['paypal']) . "', bank_name = '" . $this->db->escape($data['bank_name']) . "', bank_branch_number = '" . $this->db->escape($data['bank_branch_number']) . "', bank_swift_code = '" . $this->db->escape($data['bank_swift_code']) . "', bank_account_name = '" . $this->db->escape($data['bank_account_name']) . "', bank_account_number = '" . $this->db->escape($data['bank_account_number']) . "', status = '" . (int) $data['status'] . "' WHERE affiliate_id = '" . (int) $affiliate_id . "'");

        if ($data['password']) {
            $this->db->query("UPDATE " . DB_PREFIX . "affiliate SET salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "' WHERE affiliate_id = '" . (int) $affiliate_id . "'");
        }

        /* ......affiliates fundraisers.......... */

        $hear_about_us = '';
        if (isset($data['hear_about_us'])) {
            $hear_about_us = implode(', ', $data['hear_about_us']);
        }
        $other_hear_about_us = '';
        if (isset($data['other_hear_about_us'])) {
            $other_hear_about_us = $data['other_hear_about_us'];
        }

        $other_details_input = '';
        if (isset($data['other_details'])) {
            $other_details_input = $data['other_details_input'];
        }


        $get_aggrement = '';
        if (isset($data['agreement'])) {
            $i = 0;
            foreach ($data['agreement'] as $agreement) {
                $i++;
                if ($i == count($data['agreement'])) {
                    $get_aggrement .= $agreement;
                } else {
                    $get_aggrement .= $agreement . '-';
                }
            }
        }

        $this->db->query("UPDATE " . DB_PREFIX . "affiliates_fundraisers SET position_in_organization = '" . $this->db->escape($data['position_in_organization']) . "', organization_phone_number = '" . $this->db->escape($data['organization_phone_number']) . "', branded_website = '" . $this->db->escape($data['branded_website']) . "', organization_hashtags = '" . $this->db->escape($data['organization_hashtags']) . "', organization_facebook_link = '" . $this->db->escape($data['organization_facebook_link']) . "', organization_instagram = '" . $this->db->escape($data['organization_instagram']) . "', organization_twitter = '" . $this->db->escape($data['organization_twitter']) . "', organization_linkedIn_link = '" . $this->db->escape($data['organization_linkedIn_link']) . "', agreement = '" . $get_aggrement . "', organization_detailed = '" . $this->db->escape($data['organization_detailed']) . "', organization_detailed_french = '" . $this->db->escape($data['organization_detailed_french']) . "', hear_about_us = '" . $hear_about_us . "', other_hear_about_us = '" . $other_hear_about_us . "',other_details_input = '" . $other_details_input . "', member_image1 = '" . $this->db->escape($member_image1) . "' , member_image2 = '" . $this->db->escape($member_image2) . "', member_video = '" . $this->db->escape($data['member_video']) . "'  WHERE affiliate_id = '" . (int) $affiliate_id . "'");

        /* ......affiliates fundraisers........... */
    }

    public function deleteAffiliate($affiliate_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "affiliate WHERE affiliate_id = '" . (int) $affiliate_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "affiliate_activity WHERE affiliate_id = '" . (int) $affiliate_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "affiliate_transaction WHERE affiliate_id = '" . (int) $affiliate_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "affiliates_fundraisers WHERE affiliate_id = '" . (int) $affiliate_id . "'");
    }

    public function getAffiliate($affiliate_id) {

        $query = $this->db->query("SELECT DISTINCT af.*,fund.* FROM " . DB_PREFIX . "affiliate af," . DB_PREFIX . "affiliates_fundraisers fund WHERE af.affiliate_id=fund.affiliate_id AND  af.affiliate_id = '" . (int) $affiliate_id . "'");
        return $query->row;
    }

    public function getAffiliateByEmail($email) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "affiliate WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

        return $query->row;
    }

    public function getAffiliateByCode($code) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "affiliate WHERE code = '" . $this->db->escape($code) . "'");

        return $query->row;
    }

    public function getAffiliates($data = array()) {
        $sql = "SELECT *, CONCAT(a.firstname, ' ', a.lastname) AS name, (SELECT SUM(at.amount) FROM " . DB_PREFIX . "affiliate_transaction at WHERE at.affiliate_id = a.affiliate_id GROUP BY at.affiliate_id) AS balance FROM " . DB_PREFIX . "affiliate a";

        $implode = array();

        if (!empty($data['filter_name'])) {
            $implode[] = "CONCAT(a.firstname, ' ', a.lastname) LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['filter_email'])) {
            $implode[] = "LCASE(a.email) = '" . $this->db->escape(utf8_strtolower($data['filter_email'])) . "'";
        }


        if (!empty($data['filter_company'])) {
            $implode[] = "LCASE(a.company) = '" . $this->db->escape(utf8_strtolower($data['filter_company'])) . "'";
        }

        if (!empty($data['filter_code'])) {
            $implode[] = "a.code = '" . $this->db->escape($data['filter_code']) . "'";
        }

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $implode[] = "a.status = '" . (int) $data['filter_status'] . "'";
        }

        if (isset($data['filter_approved']) && !is_null($data['filter_approved'])) {
            $implode[] = "a.approved = '" . (int) $data['filter_approved'] . "'";
        }

        if (!empty($data['filter_date_added'])) {
            $implode[] = "DATE(a.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
        }

        if ($implode) {
            $sql .= " WHERE " . implode(" AND ", $implode);
        }

        $sort_data = array(
            'name',
            'a.email',
            'a.company',
            'a.code',
            'a.status',
            'a.approved',
            'a.date_added'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function approve($affiliate_id) {
        $affiliate_info = $this->getAffiliate($affiliate_id);
        //  echo '<pre>'; print_r($affiliate_info); die;
        if ($affiliate_info) {
            $this->db->query("UPDATE " . DB_PREFIX . "affiliate SET approved = '1' WHERE affiliate_id = '" . (int) $affiliate_id . "'");

            $this->load->language('mail/affiliate');

            $message = sprintf($this->language->get('text_approve_welcome'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) . "\n\n";
            $message .= $this->language->get('text_approve_login') . "\n";
            $message .= HTTP_CATALOG . 'index.php?route=affiliate/login' . "\n\n";
            $message .= $this->language->get('text_approve_services') . "\n\n";
            $message .= $this->language->get('text_approve_thanks') . "\n";
            $message .= html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8');

            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
            $mail->smtp_username = $this->config->get('config_mail_smtp_username');
            $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
            $mail->smtp_port = $this->config->get('config_mail_smtp_port');
            $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

            $mail->setTo($affiliate_info['email']);
            $mail->setFrom($this->config->get('config_email'));
            $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
            $mail->setSubject(sprintf($this->language->get('text_approve_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')));
            $mail->setText($message);
            // $mail->send();
            return true;
        }
    }

    public function getAffiliatesByNewsletter() {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "affiliate WHERE newsletter = '1' ORDER BY firstname, lastname, email");
        return $query->rows;
    }

    public function getTotalAffiliates($data = array()) {

        $sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "affiliate";
        $implode = array();

        if (!empty($data['filter_name'])) {
            $implode[] = " (firstname) LIKE '" . $this->db->escape($data['filter_name']) . "%' OR LCASE(company) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%' OR  (lastname) LIKE '%" . $this->db->escape($data['filter_name']) . "%' ";
        }

        if (!empty($data['filter_email'])) {
            $implode[] = "LCASE(email) = '" . $this->db->escape(utf8_strtolower($data['filter_email'])) . "'";
        }

        if (!empty($data['filter_organization'])) {
            $implode[] = "LCASE(company) = '" . $this->db->escape(utf8_strtolower($data['filter_organization'])) . "'";
        }

        if (!empty($data['affiliate_id']) && !empty($data['affiliate_name'])) {
            $implode[] = "affiliate_id = '" . $this->db->escape($data['affiliate_id']) . "'";
        }

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $implode[] = "status = '" . (int) $data['filter_status'] . "'";
        }

        if (isset($data['filter_approved']) && !is_null($data['filter_approved'])) {
            $implode[] = "approved = '" . (int) $data['filter_approved'] . "'";
        }

        if (!empty($data['filter_date_added'])) {
            $implode[] = "DATE(date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
        }

        if ($implode) {
            $sql .= " WHERE " . implode(" AND ", $implode);
        }
        //echo '<pre>'; print_r($sql); die;
        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getTotalAffiliatesAwaitingApproval() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "affiliate WHERE status = '0' OR approved = '0'");

        return $query->row['total'];
    }

    public function getTotalAffiliatesByCountryId($country_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "affiliate WHERE country_id = '" . (int) $country_id . "'");

        return $query->row['total'];
    }

    public function getTotalAffiliatesByZoneId($zone_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "affiliate WHERE zone_id = '" . (int) $zone_id . "'");
        return $query->row['total'];
    }

    public function addTransaction($affiliate_id, $description = '', $amount = '', $order_id = 0) {

        $affiliate_info = $this->getAffiliate($affiliate_id);

        if ($affiliate_info) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "affiliate_transaction SET affiliate_id = '" . (int) $affiliate_id . "', order_id = '" . (float) $order_id . "', description = '" . $this->db->escape($description) . "', amount = '" . (float) $amount . "', date_added = NOW()");

            $affiliate_transaction_id = $this->db->getLastId();

            $this->load->language('mail/affiliate');

              $message  = sprintf($this->language->get('text_transaction_received'), $this->currency->format($amount, $this->config->get('config_currency'))) . "\n\n";
              $message .= sprintf($this->language->get('text_transaction_total'), $this->currency->format($this->getTransactionTotal($affiliate_id), $this->config->get('config_currency')));

              $mail = new Mail();
              $mail->protocol = $this->config->get('config_mail_protocol');
              $mail->parameter = $this->config->get('config_mail_parameter');
              $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
              $mail->smtp_username = $this->config->get('config_mail_smtp_username');
              $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
              $mail->smtp_port = $this->config->get('config_mail_smtp_port');
              $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

              $mail->setTo($affiliate_info['email']);
              $mail->setFrom($this->config->get('config_email'));
              $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
              $mail->setSubject(sprintf($this->language->get('text_transaction_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')));
              $mail->setText($message);
              $mail->send();  

            return $affiliate_transaction_id;
        }
    }

    public function deleteTransaction($order_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "affiliate_transaction WHERE order_id = '" . (int) $order_id . "'");
    }

    public function getTransactions($affiliate_id, $start = 0, $limit = 10) {
        if ($start < 0) {
            $start = 0;
        }

        if ($limit < 1) {
            $limit = 10;
        }

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "affiliate_transaction WHERE affiliate_id = '" . (int) $affiliate_id . "' ORDER BY date_added DESC LIMIT " . (int) $start . "," . (int) $limit);
        return $query->rows;
    }

    public function getTotalTransactions($affiliate_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total  FROM " . DB_PREFIX . "affiliate_transaction WHERE affiliate_id = '" . (int) $affiliate_id . "'");
        return $query->row['total'];
    }

    public function getTransactionTotal($affiliate_id) {
        $query = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "affiliate_transaction WHERE affiliate_id = '" . (int) $affiliate_id . "'");
        return $query->row['total'];
    }

    public function getTotalTransactionsByOrderId($order_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "affiliate_transaction WHERE order_id = '" . (int) $order_id . "'");
        return $query->row['total'];
    }

    public function getTotalLoginAttempts($email) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "affiliate_login` WHERE `email` = '" . $this->db->escape($email) . "'");
        return $query->row;
    }

    public function deleteLoginAttempts($email) {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "affiliate_login` WHERE `email` = '" . $this->db->escape($email) . "'");
    }

    public function getData($data = array()) {
        
        //echo "<pre>"; print_r($data); die;

        $sql = "SELECT *, CONCAT(a.firstname, ' ', a.lastname) AS name, (SELECT SUM(at.amount) FROM " . DB_PREFIX . "affiliate_transaction at WHERE at.affiliate_id = a.affiliate_id GROUP BY at.affiliate_id) AS balance, mta_a.mta_scheme_id as scheme_id, mta_a.level_original as level FROM " . DB_PREFIX . "affiliate AS a left join " . DB_PREFIX . "mta_affiliate AS mta_a on mta_a.affiliate_id=a.affiliate_id";
        $implode = array();

        if (!empty($data['ID'])) {
            if ($data['ID'] == 1) {
              //  echo "1"; die;
                $implode[] = "mta_a.level_original = '" . $data['ID'] . "'";
            } else {
             //   echo "2"; die;
                $implode[] = "mta_a.level_original != '" . 1 . "'";
            }
        } else {
            //  echo "3"; die;
            $implode[] = "mta_a.level_original = '" . 1 . "'";
        }

        if (!empty($data['filter_name'])) {
            $implode[] = "CONCAT(a.firstname, ' ', a.lastname) LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['filter_email'])) {
            $implode[] = "LCASE(a.email) = '" . $this->db->escape(utf8_strtolower($data['filter_email'])) . "'";
        }

        if (!empty($data['filter_company'])) {
            $implode[] = "LCASE(a.company) = '" . $this->db->escape(utf8_strtolower($data['filter_company'])) . "'";
        }

        if (!empty($data['affiliate_id']) && !empty($data['affiliate_name'])) {
            $implode[] = "mta_a.parent_affiliate_id = '" . $this->db->escape($data['affiliate_id']) . "'";
        }

        if (!empty($data['filter_code'])) {
            $implode[] = "a.code = '" . $this->db->escape($data['filter_code']) . "'";
        }

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $implode[] = "a.status = '" . (int) $data['filter_status'] . "'";
        }

        if (isset($data['filter_approved']) && !is_null($data['filter_approved'])) {
            $implode[] = "a.approved = '" . (int) $data['filter_approved'] . "'";
        }

        if (!empty($data['filter_date_added'])) {
            $implode[] = "DATE(a.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
        }

        if ($implode) {
            $sql .= " WHERE " . implode(" AND ", $implode);
        }

        $sort_data = array(
            'name',
            'a.email',
            'a.company',
            'balance',
            'a.code',
            'a.status',
            'a.approved',
            'a.date_added',
            'a.affiliate_name'
        );


        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

      //  echo "<pre>"; print_r($sql); die;
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getIFSMemberlisting() {

        $query = $this->db->query("SELECT *, CONCAT(a.firstname, ' ', a.lastname) AS name, (SELECT SUM(at.amount) FROM " . DB_PREFIX . "affiliate_transaction at WHERE at.affiliate_id = a.affiliate_id GROUP BY at.affiliate_id) AS balance, mta_a.mta_scheme_id as scheme_id, mta_a.level_original as level FROM " . DB_PREFIX . "affiliate AS a left join " . DB_PREFIX . "mta_affiliate AS mta_a on mta_a.affiliate_id=a.affiliate_id");
        return $query->rows;
    }

    public function getTotal($data = array()) {

        $sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "affiliate as a inner join " . DB_PREFIX . "mta_affiliate as mta_a on a.affiliate_id= mta_a.affiliate_id ";
        $implode = array();

        $var = "=1";
        if (!empty($data['ID'])) {
            if ($data['ID'] == 1) {
                $var = "= 1";
            } else {
                $var = "!= 1";
            }
        }
        $implode[] = "level_original $var";

        if (!empty($data['filter_name'])) {
            $implode[] = "CONCAT(a.firstname, ' ', a.lastname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['filter_email'])) {
            $implode[] = "LCASE(a.email) = '" . $this->db->escape(utf8_strtolower($data['filter_email'])) . "'";
        }

        if (!empty($data['filter_company'])) {
            $implode[] = "LCASE(a.company) = '" . $this->db->escape(utf8_strtolower($data['filter_company'])) . "'";
        }

        if (!empty($data['affiliate_id']) && !empty($data['affiliate_name'])) {
            $implode[] = "a.affiliate_id = '" . $this->db->escape($data['affiliate_id']) . "'";
        }

        if (!empty($data['filter_code'])) {
            $implode[] = "a.code = '" . $this->db->escape($data['filter_code']) . "'";
        }

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $implode[] = "a.status = '" . (int) $data['filter_status'] . "'";
        }

        if (isset($data['filter_approved']) && !is_null($data['filter_approved'])) {
            $implode[] = "a.approved = '" . (int) $data['filter_approved'] . "'";
        }

        if (!empty($data['filter_date_added'])) {
            $implode[] = "DATE(a.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
        }

        if ($implode) {
            $sql .= " WHERE " . implode(" AND ", $implode);
        }

        $query = $this->db->query($sql);
        return $query->row['total'];
    }

    public function affiliateType($affiliate_id) {
        $query = $this->db->query("SELECT af.firstname FROM " . DB_PREFIX . "mta_affiliate mt_af," . DB_PREFIX . "affiliate af WHERE af.affiliate_id = '" . $this->db->escape($affiliate_id) . "' AND af.affiliate_id = mt_af.affiliate_id AND mt_af.level_original =2");

        return $query;
    }

    function imageResize($imageResourceId, $width, $height, $targetWidth, $targetHeight) {
        $targetLayer = imagecreatetruecolor($targetWidth, $targetHeight);
        imagecopyresampled($targetLayer, $imageResourceId, 0, 0, 0, 0, $targetWidth, $targetHeight, $width, $height);
        return $targetLayer;
    }



}
