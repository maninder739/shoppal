<?php
class ModelBirdsviewTotal extends Model {

    //Order calculation
    public function worder() {
        $wordtotal = array();
        for($j = 0; $j < 24; $j++) {
            $query = $this->db->query("SELECT worder AS total FROM `" . DB_PREFIX . "midnight` WHERE hour = ".$j."");
            if(isset($query->row['total'])) {
                $wordtotal[$j] = (int)$query->row['total'];
            } else {
                $wordtotal[$j] = 0;
            }
        }
        return $wordtotal;
    }


    public function torder() {
        $data1 = array();
        $data1[0] = array ("HOURS","Today Order", "Last 7 Days","Last 30 Days Order","Last 90 Days Order");
        $tordtotal = array();
        for ($i = 0; $i < 24; $i++) {
            $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE order_status_id > 0 &&  DATE(date_added) = DATE(NOW()) AND HOUR(date_added) = ".$i."");
            if(isset($query->row['total'])) {
                $tordtotal[$i] = (int)$query->row['total'];
            } else {
                $tordtotal[$i] = 0;
            }

        }
        return $tordtotal;
    }


    public function morder() {
        $mordtotal =  array();
        for($j = 0; $j < 24; $j++) {
            $query = $this->db->query("SELECT morder AS total FROM `" . DB_PREFIX . "midnight` WHERE  hour = ".$j."");
            if(isset($query->row['total'])) {
                $mordtotal[$j] = (int)$query->row['total'];
            } else {
                $mordtotal[$j] = 0;
            }
        }
        return $mordtotal;
    }

    public function qorder() {
        $qordtotal = array();
        for($j = 0; $j < 24; $j++) {
            $query = $this->db->query("SELECT qorder AS total FROM `" . DB_PREFIX . "midnight` WHERE   hour = ".$j."");
            if(isset($query->row['total'])) {
                $qordtotal[$j] = (int)$query->row['total'];
            } else {
                $qordtotal[$j] = 0;
            }
        }
        return $qordtotal;
    }

    public function trevenue() {
        $data1 = array();
        $data1[0] = array ("HOURS","Today revenue", "Last 7 Days revenue","Last 30 Days revenue","Last 90 Days revenue");
        $trevtotal = array();
        for ($i = 0; $i < 24; $i++) {
            $query = $this->db->query("SELECT sum(total) AS total FROM `" . DB_PREFIX . "order` WHERE  order_status_id > 0 &&  DATE(date_added) = DATE(NOW()) AND HOUR(date_added) = ".$i."");
            if(isset($query->row['total'])) {
                $trevtotal[$i] = $query->row['total'];
            } else {
                $trevtotal[$i] = 0;
            }
        }
        return $trevtotal;
    }

    public function wrevenue() {
        $wrevtotal = array();
        for($j = 0; $j < 24; $j++) {
            if(!isset($wrevtotal[$j])) {
                $wrevtotal[$j] = 0;
            }
            $query = $this->db->query("SELECT wrevenue AS total FROM `" . DB_PREFIX . "midnight` WHERE   hour= ".$j."");
            if(isset($query->row['total'])) {
                $wrevtotal[$j] = $query->row['total'];
            } else {
                $wrevtotal[$j] = 0;
            }
        }
        return $wrevtotal;
    }

    public function mrevenue() {
        $mrevtotal = array();
        for($j = 0; $j < 24 ; $j++) {
            if(!isset($mrevtotal[$j])) {
                $mrevtotal[$j]=0;
            }
            $query = $this->db->query("SELECT mrevenue AS total FROM `" . DB_PREFIX . "midnight` WHERE  hour = ".$j."");
            if(isset($query->row['total'])) {
                $mrevtotal[$j] = $query->row['total'];
            } else {
                $mrevtotal[$j] = 0;
            }
        }
        return $mrevtotal;
    }

    public function qrevenue() {
        $qrevtotal = array();
        for($j = 0; $j < 24; $j++) {
            if(!isset($qrevtotal[$j])) {
                $qrevtotal[$j]=0;
            }
            $query = $this->db->query("SELECT qrevenue AS total FROM `" . DB_PREFIX . "midnight` WHERE   hour = ".$j."");
            if(isset($query->row['total'])) {
                $qrevtotal[$j] = $query->row['total'];
            } else {
                $qrevtotal[$j] = 0;
            }
        }
        return $qrevtotal;
    }

    //Order Calculation
    public function tord() {
        $query1 = $this->db->query("SELECT COUNT(*)as total1 FROM `".DB_PREFIX."order` where order_status_id > 0 && DATE(date_added)=DATE(DATE_SUB(CURDATE(), INTERVAL 0 DAY))");
        $query2 = $this->db->query("SELECT COUNT(*)as total2 FROM `".DB_PREFIX."order` where order_status_id > 0 && DATE(date_added)=DATE(DATE_SUB(CURDATE(), INTERVAL 1 DAY))");
        $total = array();
        if(isset($query1->row['total1'])) {
            $total[0] = $query1->row['total1'];
        } else {
            $total[0] = '0';
        }

        if(isset($query2->row['total2'])) {
            $total[1] = $query2->row['total2'];
        } else {
            $total[1] = '0';
        }

        $total[4] = $total[0] - $total[1];
        return $total;
    }

    public function yord() {
        $query1 = $this->db->query("SELECT COUNT(*)as total1 FROM `".DB_PREFIX."order` where order_status_id > 0 && DATE(date_added)=DATE(DATE_SUB(CURDATE(), INTERVAL 1 DAY))");
        $query2 = $this->db->query("SELECT COUNT(*)as total2 FROM `".DB_PREFIX."order` where order_status_id > 0 && DATE(date_added)=DATE(DATE_SUB(CURDATE(), INTERVAL 2 DAY))");
        $total = array();
        if(isset($query1->row['total1'])) {
            $total[0]= $query1->row['total1'];
        } else {
            $total[0] = '0';
        }
        if(isset($query2->row['total2'])) {
            $total[1]= $query2->row['total2'];
        } else {
            $total[1] = '0';
        }
        $total[4] = $total[0] - $total[1];
        return $total;
    }

    public function word() {
        $query1 = $this->db->query("SELECT COUNT(*)as total1 FROM `".DB_PREFIX."order` where order_status_id > 0 && DATE(date_added)BETWEEN DATE(DATE_SUB(CURRENT_DATE(), INTERVAL (WEEKDAY(CURRENT_DATE()) + 1 ) % 7 DAY))AND DATE(DATE_SUB(CURDATE(), INTERVAL 0 DAY))");
        $query2 = $this->db->query("SELECT COUNT(*)as total2 FROM `".DB_PREFIX."order` where order_status_id > 0 && DATE(date_added) BETWEEN DATE(DATE_SUB(CURDATE(), INTERVAL 1 WEEK)) AND DATE(DATE_SUB(CURRENT_DATE(), INTERVAL (WEEKDAY(CURRENT_DATE()) + 1 ) % 7 DAY))");
        $total = array();
        if(isset($query1->row['total1'])) {
            $total[0] = $query1->row['total1'];
        } else {
            $total[0] = '0';
        }
        if(isset($query2->row['total2'])) {
            $total[1] = $query2->row['total2'];
        } else {
            $total[1] = '0';
        }
        $total[4] = $total[0] - $total[1];
        return $total;
    }

    public function mord() {
        $query1 = $this->db->query("SELECT COUNT(*)as total1 FROM `".DB_PREFIX."order` where order_status_id > 0 && MONTH(date_added)=MONTH(CURDATE())");
        $query2 = $this->db->query("SELECT COUNT(*)as total2 FROM `".DB_PREFIX."order` where order_status_id > 0 && MONTH(date_added)=MONTH(CURDATE())-1 ");
        $total = array();
        if(isset($query1->row['total1'])) {
            $total[0]= $query1->row['total1'];
        } else {
            $total[0]='0';
        }
        if(isset($query2->row['total2'])) {
            $total[1]= $query2->row['total2'];
        } else {
            $total[1]='0';
        }
        $total[4] = $total[0] - $total[1];
        return $total;
    }

    public function yyord() {
        $query1 = $this->db->query("SELECT COUNT(*)as total1 FROM `".DB_PREFIX."order` where order_status_id > 0 && YEAR(date_added)=YEAR(CURDATE())");
        $query2 = $this->db->query("SELECT COUNT(*)as total2 FROM `".DB_PREFIX."order` where order_status_id > 0 && YEAR(date_added)=YEAR(CURDATE())-1 ");
        $total = array();
        if(isset($query1->row['total1'])) {
            $total[0] = $query1->row['total1'];
        } else {
            $total[0] = '0';
        }
        if(isset($query2->row['total2'])) {
            $total[1] = $query2->row['total2'];
        } else {
            $total[1] = '0';
        }
        $total[4] = $total[0] - $total[1];
        return $total;
    }

    //Product Calculation
    public function tpro() {
        $query1 = $this->db->query("SELECT sum(quantity)as total1 FROM `".DB_PREFIX. "order_product` , `".DB_PREFIX."order` where order_status_id > 0 && DATE(`".DB_PREFIX ."order`.date_added)=DATE(DATE_SUB(CURDATE(), INTERVAL 0 DAY)) AND`".DB_PREFIX."order`.order_id=`".DB_PREFIX."order_product`.order_id");
        $query2 = $this->db->query("SELECT sum(quantity)as total2 FROM `".DB_PREFIX. "order_product`,`".DB_PREFIX."order` where order_status_id > 0 && DATE(`".DB_PREFIX ."order`.date_added)=DATE(DATE_SUB(CURDATE(), INTERVAL 1 DAY))AND`".DB_PREFIX."order`.order_id=`".DB_PREFIX."order_product`.order_id");
        $total = array();
        if(isset($query1->row['total1'])) {
            $total[0] = $query1->row['total1'];
        } else {
            $total[0] = '0';
        }
        if(isset($query2->row['total2'])) {
            $total[1] = $query2->row['total2'];
        } else {
            $total[1] = '0';
        }
        $total[4] = $total[0] - $total[1];
        return $total;
    }

    public function ypro() {
        $query1 = $this->db->query("SELECT sum(quantity)as total1 FROM `".DB_PREFIX."order_product` ,`".DB_PREFIX."order` where order_status_id > 0 && DATE(`".DB_PREFIX."order`.date_added)=DATE(DATE_SUB(CURDATE(), INTERVAL 1 DAY))AND `".DB_PREFIX."order`.order_id=`".DB_PREFIX."order_product`.order_id");
        $query2 = $this->db->query("SELECT sum(quantity)as total2 FROM `".DB_PREFIX."order_product` ,`".DB_PREFIX."order` where order_status_id > 0 && DATE(`".DB_PREFIX."order`.date_added)=DATE(DATE_SUB(CURDATE(), INTERVAL 2 DAY))AND `".DB_PREFIX."order`.order_id=`".DB_PREFIX."order_product`.order_id");
        $total = array();
        if(isset($query1->row['total1'])) {
            $total[0] = $query1->row['total1'];
        } else {
            $total[0] = '0';
        }

        if(isset($query2->row['total2'])) {
            $total[1]= $query2->row['total2'];
        } else {
            $total[1]='0';
        }
        $total[4] = $total[0] - $total[1];
        return $total;
    }

    public function mpro() {
        $query1 = $this->db->query("SELECT sum(quantity)as total1 FROM  `".DB_PREFIX."order_product` ,`".DB_PREFIX."order` where order_status_id > 0 && MONTH(`".DB_PREFIX."order`.date_added)=MONTH(CURDATE())AND `".DB_PREFIX."order`.order_id=`".DB_PREFIX."order_product`.order_id");
        $query2 = $this->db->query("SELECT sum(quantity)as total2 FROM  `".DB_PREFIX."order_product` ,`".DB_PREFIX."order` where order_status_id > 0 && MONTH(`".DB_PREFIX."order`.date_added)=MONTH(CURDATE())-1 AND `".DB_PREFIX."order`.order_id=`".DB_PREFIX."order_product`.order_id");
        $total = array();
        if(isset($query1->row['total1'])) {
            $total[0]= $query1->row['total1'];
        } else {
            $total[0]='0';
        }

        if(isset($query2->row['total2'])) {
            $total[1]= $query2->row['total2'];
        } else {
            $total[1]='0';
        }
        $total[4] = $total[0] - $total[1];
        return $total;
    }

    public function wpro() {
        $query1 = $this->db->query("SELECT sum(quantity)as total1 FROM `".DB_PREFIX."order_product` , `".DB_PREFIX."order` where order_status_id > 0 && DATE(`".DB_PREFIX."order`.date_added) BETWEEN DATE(DATE_SUB(CURRENT_DATE(), INTERVAL (WEEKDAY(CURRENT_DATE()) + 1 ) % 7 DAY))AND DATE(DATE_SUB(CURDATE(), INTERVAL 0 DAY))AND `".DB_PREFIX."order`.order_id=`".DB_PREFIX."order_product`.order_id");
        $query2 = $this->db->query("SELECT sum(quantity)as total2 FROM `".DB_PREFIX."order_product` ,`".DB_PREFIX."order` where order_status_id > 0 && DATE(`".DB_PREFIX."order`.date_added) BETWEEN DATE(DATE_SUB(CURDATE(), INTERVAL 1 WEEK)) AND DATE(DATE_SUB(CURRENT_DATE(), INTERVAL (WEEKDAY(CURRENT_DATE()) + 1 ) % 7 DAY))AND `".DB_PREFIX."order`.order_id=`".DB_PREFIX."order_product`.order_id");
        $total = array();
        if(isset($query1->row['total1'])) {
            $total[0] = $query1->row['total1'];
        } else {
            $total[0] = '0';
        }
        if(isset($query2->row['total2'])) {
            $total[1] = $query2->row['total2'];
        } else {
            $total[1] = '0';
        }
        $total[4] = $total[0] - $total[1];
        return $total;
    }

    public function yypro() {
        $query1 = $this->db->query("SELECT sum(quantity) as total1 FROM `".DB_PREFIX."order_product` , `".DB_PREFIX."order` where order_status_id > 0 && YEAR(`".DB_PREFIX."order`.date_added)=YEAR(CURDATE())AND `".DB_PREFIX."order`.order_id=`".DB_PREFIX."order_product`.order_id");
        $query2 = $this->db->query("SELECT sum(quantity) as total2 FROM `".DB_PREFIX."order_product` , `".DB_PREFIX."order` where order_status_id > 0 && YEAR(`".DB_PREFIX."order`.date_added)=YEAR(CURDATE())-1 AND `".DB_PREFIX."order`.order_id=`".DB_PREFIX."order_product`.order_id ");
        $total = array();
        if(isset($query1->row['total1'])) {
            $total[0] = $query1->row['total1'];
        } else {
            $total[0] = '0';
        }
        if(isset($query2->row['total2'])) {
            $total[1] = $query2->row['total2'];
        } else {
            $total[1] = '0';
        }
        $total[4] = $total[0] - $total[1];
        return $total;
    }


    //Missing order calculation
    public function tmiss() {
        $query1 = $this->db->query("SELECT COUNT(*)as total1 FROM `".DB_PREFIX."order` where DATE(date_added)=DATE(DATE_SUB(CURDATE(), INTERVAL 0 DAY)) AND order_status_id=0");
        $query2 = $this->db->query("SELECT COUNT(*)as total2 FROM `".DB_PREFIX."order` where DATE(date_added)=DATE(DATE_SUB(CURDATE(), INTERVAL 1 DAY)) AND order_status_id=0");
        $total = array();
        if(isset($query1->row['total1'])) {
            $total[0] = $query1->row['total1'];
        } else {
            $total[0] = '0';
        }
        if(isset($query2->row['total2'])) {
            $total[1] = $query2->row['total2'];
        } else {
            $total[1] = '0';
        }
        $total[4] = $total[0] - $total[1];
        return $total;
    }

    public function ymiss() {
        $query1 = $this->db->query("SELECT COUNT(*)as total1 FROM `".DB_PREFIX."order` where DATE(date_added)=DATE(DATE_SUB(CURDATE(), INTERVAL 1 DAY))AND order_status_id=0");
        $query2 = $this->db->query("SELECT COUNT(*)as total2 FROM `".DB_PREFIX."order` where DATE(date_added)=DATE(DATE_SUB(CURDATE(), INTERVAL 2 DAY))AND order_status_id=0");
        $total = array();
        if(isset($query1->row['total1'])) {
            $total[0] = $query1->row['total1'];
        } else {
            $total[0] = '0';
        }
        if(isset($query2->row['total2'])) {
            $total[1]= $query2->row['total2'];
        } else {
            $total[1]='0';
        }
        $total[4] = $total[0] - $total[1];
        return $total;
    }

    public function mmiss() {
        $query1 = $this->db->query("SELECT COUNT(*)as total1 FROM  `".DB_PREFIX."order`  where MONTH(`".DB_PREFIX."order`.date_added)=MONTH(CURDATE())AND order_status_id=0");
        $query2 = $this->db->query("SELECT COUNT(*)as total2 FROM  `".DB_PREFIX."order` where MONTH(`".DB_PREFIX."order`.date_added)=MONTH(CURDATE())-1 AND order_status_id=0");
        $total = array();
        if(isset($query1->row['total1'])) {
            $total[0] = $query1->row['total1'];
        } else {
            $total[0] = '0';
        }
        if(isset($query2->row['total2'])) {
            $total[1] = $query2->row['total2'];
        } else {
            $total[1] = '0';
        }
        $total[4] = $total[0] - $total[1];
        return $total;
    }

    public function wmiss() {
        $query1 = $this->db->query("SELECT COUNT(*)as total1 FROM `".DB_PREFIX."order`  where DATE(`".DB_PREFIX."order`.date_added)BETWEEN DATE(DATE_SUB(CURRENT_DATE(), INTERVAL (WEEKDAY(CURRENT_DATE()) + 1 ) % 7 DAY))AND DATE(DATE_SUB(CURDATE(), INTERVAL 0 DAY))AND order_status_id=0");
        $query2 = $this->db->query("SELECT COUNT(*)as total2 FROM `".DB_PREFIX."order`  where DATE(`".DB_PREFIX."order`.date_added) BETWEEN DATE(DATE_SUB(CURDATE(), INTERVAL 1 WEEK)) AND DATE(DATE_SUB(CURRENT_DATE(), INTERVAL (WEEKDAY(CURRENT_DATE()) + 1 ) % 7 DAY))AND order_status_id=0");

        $total = array();
        if(isset($query1->row['total1'])) {
            $total[0] = $query1->row['total1'];
        } else {
            $total[0] = '0';
        }
        if(isset($query2->row['total2'])) {
            $total[1] = $query2->row['total2'];
        } else {
            $total[1] = '0';
        }
        $total[4] = $total[0] - $total[1];
        return $total;
    }

    public function yymiss() {
        $query1 = $this->db->query("SELECT COUNT(*)as total1 FROM `".DB_PREFIX."order`  where YEAR(`".DB_PREFIX."order`.date_added)=YEAR(CURDATE()) AND order_status_id=0");
        $query2 = $this->db->query("SELECT COUNT(*)as total2 FROM `".DB_PREFIX."order`  where YEAR(`".DB_PREFIX."order`.date_added)=YEAR(CURDATE())-1 AND order_status_id=0");
        $total = array();
        if(isset($query1->row['total1'])) {
            $total[0] = $query1->row['total1'];
        } else {
            $total[0] = '0';
        }

        if(isset($query2->row['total2'])) {
            $total[1] = $query2->row['total2'];
        } else {
            $total[1] = '0';
        }
        $total[4] = $total[0] - $total[1];
        return $total;
    }


    //Revennue calculation
    public function trev() {
        $query1 = $this->db->query("SELECT sum(total)as total1 FROM  `".DB_PREFIX."order` where order_status_id > 0 && DATE(date_added)=DATE(DATE_SUB(CURDATE(), INTERVAL 0 DAY)) ");
        $query2 = $this->db->query("SELECT sum(total)as total2 FROM `".DB_PREFIX."order` where order_status_id > 0 && DATE(date_added)=DATE(DATE_SUB(CURDATE(), INTERVAL 1 DAY))");
        $total = array();
        if(isset($query1->row['total1'])) {
            $total[0] = $query1->row['total1'];
        } else {
            $total[0] = '0';
        }
        if(isset($query2->row['total2'])) {
            $total[1] = $query2->row['total2'];
        } else {
            $total[1] = '0';
        }
        $total[4] = $total[0] - $total[1];
        return $total;
    }

    public function yrev() {
        $query1 = $this->db->query("SELECT sum(total)as total1 FROM `".DB_PREFIX."order` where order_status_id > 0 && DATE(date_added)=DATE(DATE_SUB(CURDATE(), INTERVAL 1 DAY))");
        $query2 = $this->db->query("SELECT sum(total)as total2 FROM `".DB_PREFIX."order` where order_status_id > 0 && DATE(date_added)=DATE(DATE_SUB(CURDATE(), INTERVAL 2 DAY))");
        $total = array();
        if(isset($query1->row['total1'])) {
            $total[0] = $query1->row['total1'];
        } else {
            $total[0] = '0';
        }
        if(isset($query2->row['total2'])) {
            $total[1] = $query2->row['total2'];
        } else {
            $total[1] = '0';
        }
        $total[4] = $total[0] - $total[1];
        return $total;
    }

    public function mrev() {
        $query1 = $this->db->query("SELECT sum(total)as total1 FROM  `".DB_PREFIX."order` where order_status_id > 0 && MONTH(date_added)=MONTH(CURDATE())");
        $query2 = $this->db->query("SELECT sum(total)as total2 FROM  `".DB_PREFIX."order` where order_status_id > 0 && MONTH(date_added)=MONTH(CURDATE())-1 ");
        $total = array();
        if(isset($query1->row['total1'])) {
            $total[0] = $query1->row['total1'];
        } else {
            $total[0] = '0';
        }
        if(isset($query2->row['total2'])) {
            $total[1] = $query2->row['total2'];
        } else {
            $total[1] = '0';
        }
        $total[4] = $total[0] - $total[1];
        return $total;
    }

    public function wrev() {
        $query1 = $this->db->query("SELECT sum(total)as total1 FROM  `".DB_PREFIX."order` where order_status_id > 0 && DATE(`".DB_PREFIX."order`.date_added)BETWEEN DATE(DATE_SUB(CURRENT_DATE(), INTERVAL (WEEKDAY(CURRENT_DATE()) + 1 ) % 7 DAY))AND DATE(DATE_SUB(CURDATE(), INTERVAL 0 DAY))");
        $query2 = $this->db->query("SELECT sum(total)as total2 FROM `".DB_PREFIX."order` where order_status_id > 0 && DATE(`".DB_PREFIX."order`.date_added) BETWEEN DATE(DATE_SUB(CURDATE(), INTERVAL 1 WEEK)) AND DATE(DATE_SUB(CURRENT_DATE(), INTERVAL (WEEKDAY(CURRENT_DATE()) + 1 ) % 7 DAY))");
        $total = array();
        if(isset($query1->row['total1'])) {
            $total[0]= $query1->row['total1'];
        } else {
            $total[0]='0';
        }
        if(isset($query2->row['total2'])) {
            $total[1]= $query2->row['total2'];
        } else {
            $total[1]='0';
        }
        $total[4] = $total[0] - $total[1];
        return $total;
    }

    public function yyrev() {
        $query1 = $this->db->query("SELECT sum(total) as total1 FROM  `".DB_PREFIX."order` where order_status_id > 0 && YEAR(date_added)=YEAR(CURDATE())");
        $query2 = $this->db->query("SELECT sum(total) as total2 FROM  `".DB_PREFIX."order` where order_status_id > 0 && YEAR(date_added)=YEAR(CURDATE())-1  ");
        $total = array();
        if(isset($query1->row['total1'])) {
            $total[0]= $query1->row['total1'];
        } else {
            $total[0]='0';
        }
        if(isset($query2->row['total2'])) {
            $total[1]= $query2->row['total2'];
        } else {
            $total[1]='0';
        }
        $total[4] = $total[0] - $total[1];
        return $total;
    }


    //Discount calculation
    public function tdis() {
        $query1 = $this->db->query("SELECT sum(amount)as total1 FROM  `".DB_PREFIX. "voucher_history` where DATE(date_added)=DATE(DATE_SUB(CURDATE(), INTERVAL 0 DAY)) ");
        $query2 = $this->db->query("SELECT sum(amount)as total2 FROM  `".DB_PREFIX."coupon_history` where DATE(date_added)=DATE(DATE_SUB(CURDATE(), INTERVAL 0 DAY)) ");
        $query3 = $this->db->query("SELECT sum(amount)as total3 FROM `".DB_PREFIX."voucher_history` where DATE(date_added)=DATE(DATE_SUB(CURDATE(), INTERVAL 1 DAY))");
        $query4 = $this->db->query("SELECT sum(amount)as total4 FROM `".DB_PREFIX."coupon_history` where DATE(date_added)=DATE(DATE_SUB(CURDATE(), INTERVAL 1 DAY))");
        $total = array();

        if(isset($query1->row['total1'])) {
            $total[0] = $query1->row['total1'];
        } else {
            $total[0] = '0';
        }

        if(isset($query2->row['total2'])) {
            $total[1] = $query2->row['total2'];
        } else {
            $total[1] = '0';
        }

        if(isset($query3->row['total3'])) {
            $total[2] = $query3->row['total3'];
        } else {
            $total[2] = '0';
        }

        if(isset($query4->row['total4'])) {
            $total[3] = $query4->row['total4'];
        } else {
            $total[3] = '0';
        }
        $total[4] = $total[0] + $total[1] - $total[2] - $total[3];
        return $total;
    }

    public function ydis() {
        $query1 = $this->db->query("SELECT sum(amount)as total1 FROM `".DB_PREFIX."voucher_history` where DATE(date_added)=DATE(DATE_SUB(CURDATE(), INTERVAL 1 DAY))");
        $query2 = $this->db->query("SELECT sum(amount)as total2 FROM `".DB_PREFIX."coupon_history` where DATE(date_added)=DATE(DATE_SUB(CURDATE(), INTERVAL 1 DAY))");
        $query3 = $this->db->query("SELECT sum(amount)as total3 FROM `".DB_PREFIX."voucher_history` where DATE(date_added)=DATE(DATE_SUB(CURDATE(), INTERVAL 2 DAY))");
        $query4 = $this->db->query("SELECT sum(amount)as total4 FROM `".DB_PREFIX."coupon_history` where DATE(date_added)=DATE(DATE_SUB(CURDATE(), INTERVAL 2 DAY))");
        $total = array();
        if(isset($query1->row['total1'])) {
            $total[0]= $query1->row['total1'];
        } else {
            $total[0]='0';
        }

        if(isset($query2->row['total2'])) {
            $total[1] = $query2->row['total2'];
        } else {
            $total[1] = '0';
        }

        if(isset($query3->row['total3'])) {
            $total[2] = $query3->row['total3'];
        } else {
            $total[2] = '0';
        }

        if(isset($query4->row['total4'])) {
            $total[3]= $query4->row['total4'];
        } else {
            $total[3]='0';
        }
        $total[4] = $total[0] + $total[1] - $total[2] - $total[3];
        return $total;
    }

    public function mdis() {

        $query1 = $this->db->query("SELECT sum(amount)as total1 FROM  `".DB_PREFIX."voucher_history` where MONTH(date_added)=MONTH(CURDATE())");
        $query2 = $this->db->query("SELECT sum(amount)as total2 FROM  `".DB_PREFIX."coupon_history` where MONTH(date_added)=MONTH(CURDATE())");
        $query3 = $this->db->query("SELECT sum(amount)as total3 FROM  `".DB_PREFIX."voucher_history` where MONTH(date_added)=MONTH(CURDATE())-1 ");
        $query4 = $this->db->query("SELECT sum(amount)as total4 FROM  `".DB_PREFIX."coupon_history` where MONTH(date_added)=MONTH(CURDATE())-1 ");

        $total = array();
        if(isset($query1->row['total1'])) {
            $total[0] = $query1->row['total1'];
        } else {
            $total[0] = '0';
        }
        if(isset($query2->row['total2'])) {
            $total[1] = $query2->row['total2'];
        } else {
            $total[1] = '0';
        }
        if(isset($query3->row['total3'])) {
            $total[2] = $query3->row['total3'];
        } else {
            $total[2] = '0';
        }
        if(isset($query4->row['total4'])) {
            $total[3]= $query4->row['total4'];
        } else {
            $total[3]='0';
        }
        $total[4] = $total[0] + $total[1] - $total[2] - $total[3];
        return $total;
    }

    public function wdis() {
        $query1 = $this->db->query("SELECT sum(amount)as total1 FROM  `".DB_PREFIX."voucher_history` where DATE(date_added)BETWEEN DATE(DATE_SUB(CURRENT_DATE(), INTERVAL (WEEKDAY(CURRENT_DATE()) + 1 ) % 7 DAY))AND DATE(DATE_SUB(CURDATE(), INTERVAL 0 DAY))");
        $query2 = $this->db->query("SELECT sum(amount)as total2 FROM  `".DB_PREFIX."coupon_history` where DATE(date_added)BETWEEN DATE(DATE_SUB(CURRENT_DATE(), INTERVAL (WEEKDAY(CURRENT_DATE()) + 1 ) % 7 DAY))AND DATE(DATE_SUB(CURDATE(), INTERVAL 0 DAY))");
        $query3 = $this->db->query("SELECT sum(amount)as total3 FROM `".DB_PREFIX."voucher_history` where DATE(date_added) BETWEEN DATE(DATE_SUB(CURDATE(), INTERVAL 1 WEEK)) AND DATE(DATE_SUB(CURRENT_DATE(), INTERVAL (WEEKDAY(CURRENT_DATE()) + 1 ) % 7 DAY))");
        $query4 = $this->db->query("SELECT sum(amount)as total4 FROM `".DB_PREFIX."coupon_history` where DATE(date_added) BETWEEN DATE(DATE_SUB(CURDATE(), INTERVAL 1 WEEK)) AND DATE(DATE_SUB(CURRENT_DATE(), INTERVAL (WEEKDAY(CURRENT_DATE()) + 1 ) % 7 DAY))");
        $total = array();
        if(isset($query1->row['total1'])) {
            $total[0] = $query1->row['total1'];
        } else {
            $total[0] = '0';
        }

        if(isset($query2->row['total2'])) {
            $total[1] = $query2->row['total2'];
        } else {
            $total[1] = '0';
        }

        if(isset($query3->row['total3'])) {
            $total[2] = $query3->row['total3'];
        } else {
            $total[2] = '0';
        }
        if(isset($query4->row['total4'])) {
            $total[3] = $query4->row['total4'];
        } else {
            $total[3] = '0';
        }
        $total[4] = $total[0] + $total[1] - $total[2] - $total[3];
        return $total;
    }

    public function yydis() {
        $query1 = $this->db->query("SELECT sum(amount) as total1 FROM  `".DB_PREFIX."voucher_history` where YEAR(date_added)=YEAR(CURDATE())");
        $query2 = $this->db->query("SELECT sum(amount) as total2 FROM  `".DB_PREFIX."coupon_history` where YEAR(date_added)=YEAR(CURDATE())");
        $query3 = $this->db->query("SELECT sum(amount) as total3 FROM  `".DB_PREFIX."voucher_history` where YEAR(date_added)=YEAR(CURDATE())-1  ");
        $query4 = $this->db->query("SELECT sum(amount) as total4 FROM  `".DB_PREFIX."coupon_history` where YEAR(date_added)=YEAR(CURDATE())-1  ");
        $total = array();
        if(isset($query1->row['total1'])) {
            $total[0] = $query1->row['total1'];
        } else {
            $total[0] = '0';
        }

        if(isset($query2->row['total2'])) {
            $total[1] = $query2->row['total2'];
        } else {
            $total[1] = '0';
        }

        if(isset($query3->row['total3'])) {
            $total[2] = $query3->row['total3'];
        } else {
            $total[2] = '0';
        }

        if(isset($query4->row['total4'])) {
            $total[3] = $query4->row['total4'];
        } else {
            $total[3] = '0';
        }
        $total[4] = $total[0] + $total[1] - $total[2] - $total[3];
        return $total;
    }

    public function install() {
        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "midnight` (
		  hour int,
		  worder int ,
		  morder int,
		  qorder int,
		  wrevenue decimal(15,2) ,
		  mrevenue decimal(15,2),
		  qrevenue decimal(15,2))");
    }

}

?>