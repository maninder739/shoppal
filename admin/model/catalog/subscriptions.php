<?php
class ModelCatalogSubscriptions extends Model {

	public function getSubscriptions() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "subscriptions");
		
		return $query->rows;
	
	}

	public function getSubscription($subscription_id) {
		$return_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "subscriptions WHERE subscription_id = '" . (int)$subscription_id . "'");
		
		foreach ($query->rows as $row) {
			$return_data = array(
				'trial'						=> $row['trial'],
				'trial_only'				=> $row['trial_only'],
				'trial_frequency'			=> $row['trial_frequency'],
				'trial_duration'			=> $row['trial_duration'],
				'trial_cycle'				=> $row['trial_cycle'],
				'trial_price'				=> $row['trial_price'],
				'trial_status'				=> $row['trial_status'],
				'frequency'					=> $row['frequency'],
				'duration'					=> $row['duration'],
				'cycle'						=> $row['cycle'],
				'specific_day'				=> (isset($row['specific_day']) ? $row['specific_day'] : ''),
				'discount'					=> $row['discount'],
				'discount_type'				=> $row['discount_type'],
				'reinstatement_fee'			=> $row['reinstatement_fee'],
				'reinstatement_period'		=> $row['reinstatement_period'],
				'reminder_count'			=> $row['reminder_count'],
				'reminder_period'			=> $row['reminder_period'],
				'grace_period'				=> $row['grace_period'],
				'include_taxes'				=> $row['include_taxes'],
				'include_shipping'			=> $row['include_shipping'],
				'status'					=> $row['status'],
				'allowed_customer_groups'	=> unserialize($row['allowed_customer_groups']),
				'sort_order'				=> $row['sort_order']
			);
		}
		
		return $return_data;
	}

	public function getSubscriptionPrice($product_id, $subscription_id) {
		$price = '';
		
		$query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_to_subscription WHERE product_id = '" . (int)$product_id . "' AND subscription_id = '" . (int)$subscription_id . "'");
		
		if ($query->num_rows) {
			$price = $query->row['price'];
		}
		
		return $price;
	}

	public function getCustomerGroupName($customer_group_id) {
		if (version_compare(VERSION, '1.5.2.1', '>')) {
			$query = $this->db->query("SELECT name FROM " . DB_PREFIX . "customer_group_description WHERE customer_group_id = '" . (int)$customer_group_id . "'");
		} else {
			$query = $this->db->query("SELECT name FROM " . DB_PREFIX . "customer_group WHERE customer_group_id = '" . (int)$customer_group_id . "'");
		}
		return $query->row['name'];
	}

	public function addSubscription($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "subscriptions SET trial = '" . (isset($data['trial']) ? (int)$data['trial'] : 0) . "', trial_only = '" . (isset($data['trial_only']) ? (int)$data['trial_only'] : 0) . "', trial_frequency = '" . $this->db->escape($data['trial_frequency']) . "', trial_duration = '" . (($data['trial_duration']) ? (int)$data['trial_duration'] : 1) . "', trial_cycle = '" . (int)$data['trial_cycle'] . "', trial_price = '" . (float)$data['trial_price'] . "', trial_status = '" . (int)$data['trial_status'] . "', frequency = '" . $this->db->escape($data['frequency']) . "', duration = '" . (int)$data['duration'] . "', cycle = '" . (int)$data['cycle'] . "', specific_day = '" . (isset($data['specific_day']) ? $this->db->escape($data['specific_day']) : '') . "', discount = '" . (int)$data['discount'] . "', discount_type = '" . $this->db->escape($data['discount_type']) . "', status = '" . (int)$data['status'] . "', reinstatement_fee = '" . (float)$data['reinstatement_fee'] . "', reinstatement_period = '" . (int)$data['reinstatement_period'] . "', reminder_count = '" . (int)$data['reminder_count'] . "', reminder_period = '" . (int)$data['reminder_period'] . "', grace_period = '" . (int)$data['grace_period'] . "', allowed_customer_groups = '" . (isset($data['allowed_customer_groups']) ? $this->db->escape(serialize($data['allowed_customer_groups'])) : '') . "', include_taxes = '" . (isset($data['include_taxes']) ? (int)$data['include_taxes'] : 0) . "', include_shipping = '" . (isset($data['include_shipping']) ? (int)$data['include_shipping'] : 0) . "', sort_order = '" . (int)$data['sort_order'] . "'");
		return;
	}

	public function editSubscription($subscription_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "subscriptions SET trial = '" . (isset($data['trial']) ? (int)$data['trial'] : 0) . "', trial_only = '" . (isset($data['trial_only']) ? (int)$data['trial_only'] : 0) . "', trial_frequency = '" . $this->db->escape($data['trial_frequency']) . "', trial_duration = '" . (($data['trial_duration']) ? (int)$data['trial_duration'] : 1) . "', trial_cycle = '" . (int)$data['trial_cycle'] . "', trial_price = '" . (float)$data['trial_price'] . "', trial_status = '" . (int)$data['trial_status'] . "', frequency = '" . $this->db->escape($data['frequency']) . "', duration = '" . (int)$data['duration'] . "', cycle = '" . (int)$data['cycle'] . "', specific_day = '" . (isset($data['specific_day']) ? $this->db->escape($data['specific_day']) : '') . "', discount = '" . (int)$data['discount'] . "', discount_type = '" . $this->db->escape($data['discount_type']) . "', status = '" . (int)$data['status'] . "', reinstatement_fee = '" . (float)$data['reinstatement_fee'] . "', reinstatement_period = '" . (int)$data['reinstatement_period'] . "', reminder_count = '" . (int)$data['reminder_count'] . "', reminder_period = '" . (int)$data['reminder_period'] . "', grace_period = '" . (int)$data['grace_period'] . "', allowed_customer_groups = '" . (isset($data['allowed_customer_groups']) ? $this->db->escape(serialize($data['allowed_customer_groups'])) : '') . "', include_taxes = '" . (isset($data['include_taxes']) ? (int)$data['include_taxes'] : 0) . "', include_shipping = '" . (isset($data['include_shipping']) ? (int)$data['include_shipping'] : 0) . "', sort_order = '" . (int)$data['sort_order'] . "' WHERE subscription_id = '" . (int)$subscription_id . "'");
		return;
	}

	public function deleteSubscription($subscription_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "subscriptions WHERE subscription_id = '" . (int)$subscription_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_subscription WHERE subscription_id = '" . (int)$subscription_id . "'");
		return;
	}

    public function getFrequencies() {
        return array(
            'day' => $this->language->get('text_day'),
            'week' => $this->language->get('text_week'),
            'semi_month' => $this->language->get('text_semi_month'),
            'month' => $this->language->get('text_month'),
            'year' => $this->language->get('text_year'),
        );
    }

	public function install() {
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "order_subscriptions` (
			order_subscription_id int(11) NOT NULL auto_increment,
			order_id int(11) NOT NULL DEFAULT 0,
			customer_id int(11) NOT NULL DEFAULT 0,
			subscription_id int(11) NOT NULL DEFAULT 0,
			product_id int(11) NOT NULL DEFAULT 0,
			start_date int(11) NOT NULL DEFAULT 0,
			end_date int(11) NOT NULL DEFAULT 0,
			terms varchar(150) COLLATE utf8_general_ci NOT NULL DEFAULT '',
			recurring tinyint(1) NOT NULL DEFAULT 0,
			trial tinyint(1) NOT NULL DEFAULT 0,
			trial_start_date int(11) NOT NULL DEFAULT 0,
			trial_end_date int(11) NOT NULL DEFAULT 0,
			trial_terms varchar(150) COLLATE utf8_general_ci NOT NULL DEFAULT '',
			payment_amount decimal(15,4) NOT NULL DEFAULT 0.0000,
			reinstatement_fee decimal(15,4) NOT NULL DEFAULT 0.0000,
			next_due int(11) NOT NULL DEFAULT 0,
			last_paid int(11) NOT NULL DEFAULT 0,
			active tinyint(1) NOT NULL DEFAULT 0,
			reminder_sent int(11) NOT NULL DEFAULT 0,
			reminders_sent tinyint(2) NOT NULL DEFAULT 0,
			PRIMARY KEY (order_subscription_id)
		);");
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "order_subscriptions_transaction` (
			order_subscription_transaction_id int(11) NOT NULL auto_increment,
			order_subscription_id int(11) NOT NULL,
			date_added int(11) NOT NULL,
			amount decimal(15,4) NOT NULL,
			description varchar(255),
			PRIMARY KEY (order_subscription_transaction_id)
		);");
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "subscriptions` (
			subscription_id int(11) NOT NULL auto_increment,
			trial tinyint(1) NOT NULL DEFAULT 0,
			trial_only tinyint(1) NOT NULL DEFAULT 0,
			trial_frequency enum('day', 'week', 'semi_month', 'month', 'year') COLLATE utf8_general_ci NOT NULL,
			trial_duration int(10) NOT NULL DEFAULT 0,
			trial_cycle int(10) NOT NULL DEFAULT 0,
			trial_price decimal(15,4) NOT NULL DEFAULT 0.0000,
			trial_status tinyint(1) NOT NULL DEFAULT 0,
			frequency enum('day', 'week', 'semi_month', 'month', 'year') COLLATE utf8_general_ci NOT NULL,
			duration int(10) NOT NULL DEFAULT 0,
			cycle int(10) NOT NULL DEFAULT 0,
			specific_day varchar(20) COLLATE utf8_general_ci NOT NULL,
			discount varchar(10) COLLATE utf8_general_ci NOT NULL DEFAULT '',
			discount_type varchar(10) COLLATE utf8_general_ci NOT NULL DEFAULT '',
			status tinyint(1) NOT NULL DEFAULT 0,
			reinstatement_fee decimal(15,4) NOT NULL DEFAULT 0.0000,
			reinstatement_period int(5) NOT NULL DEFAULT 0,
			reminder_count int(5) NOT NULL DEFAULT 0,
			reminder_period int(5) NOT NULL DEFAULT 0,
			grace_period int(5) NOT NULL DEFAULT 0,
			allowed_customer_groups text COLLATE utf8_general_ci NOT NULL DEFAULT '',
			include_taxes tinyint(1) NOT NULL DEFAULT 0,
			include_shipping tinyint(1) NOT NULL DEFAULT 0,
			sort_order int(11) NOT NULL DEFAULT 0,
			PRIMARY KEY (subscription_id)
		);");
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "product_to_subscription` (
			product_id int(11) NOT NULL,
			subscription_id int(11) NOT NULL,
			price decimal(15,4) NOT NULL DEFAULT 0.0000,
			PRIMARY KEY (product_id,subscription_id)
		);");
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "order_recurring` (
			order_recurring_id int(11) NOT NULL AUTO_INCREMENT,
			order_id int(11) NOT NULL,
			date_added datetime NOT NULL,
			status tinyint(4) NOT NULL,
			product_id int(11) NOT NULL,
			product_name varchar(255) COLLATE utf8_general_ci NOT NULL,
			product_quantity int(11) NOT NULL,
			recurring_id int(11) NOT NULL,
			recurring_name varchar(255) COLLATE utf8_general_ci NOT NULL,
			recurring_description varchar(255) COLLATE utf8_general_ci NOT NULL,
			recurring_frequency varchar(25) COLLATE utf8_general_ci NOT NULL,
			recurring_cycle smallint(6) NOT NULL,
			recurring_duration smallint(6) NOT NULL,
			recurring_price decimal(10,4) NOT NULL,
			trial tinyint(1) NOT NULL,
			trial_frequency varchar(25) COLLATE utf8_general_ci NOT NULL,
			trial_cycle smallint(6) NOT NULL,
			trial_duration smallint(6) NOT NULL,
			trial_price decimal(10,4) NOT NULL,
			reference varchar(255) COLLATE utf8_general_ci NOT NULL,
			PRIMARY KEY (order_recurring_id)
		);");
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "order_recurring_transaction` (
			order_recurring_transaction_id int(11) NOT NULL AUTO_INCREMENT,
			order_recurring_id int(11) NOT NULL,
			date_added datetime NOT NULL,
			amount decimal(10,4) NOT NULL,
			type varchar(255) COLLATE utf8_general_ci NOT NULL,
			PRIMARY KEY (order_recurring_transaction_id)
		);");
		$this->db->query("ALTER TABLE `" . DB_PREFIX . "cart` ADD COLUMN subscription_id int(11) NOT NULL");
		$this->db->query("ALTER TABLE `" . DB_PREFIX . "product` ADD COLUMN single_sale tinyint(1) NOT NULL DEFAULT 0");
		$this->db->query("ALTER TABLE `" . DB_PREFIX . "product` ADD COLUMN choose_payment_date tinyint(1) NOT NULL DEFAULT 0");
		$this->db->query("ALTER TABLE `" . DB_PREFIX . "download` ADD COLUMN subscription tinyint(1) NOT NULL DEFAULT 0");
		$this->db->query("ALTER TABLE `" . DB_PREFIX . "order_product` ADD COLUMN pro_rated tinyint(1) NOT NULL DEFAULT 0");
		return;
	}

	public function uninstall() {
		$this->db->query("DROP TABLE `" . DB_PREFIX . "order_subscriptions`");
		$this->db->query("DROP TABLE `" . DB_PREFIX . "order_subscriptions_transaction`");
		$this->db->query("DROP TABLE `" . DB_PREFIX . "subscriptions`");
		$this->db->query("DROP TABLE `" . DB_PREFIX . "product_to_subscription`");
		$this->db->query("ALTER TABLE `" . DB_PREFIX . "cart` DROP COLUMN subscription_id");
		$this->db->query("ALTER TABLE `" . DB_PREFIX . "product` DROP COLUMN single_sale");
		$this->db->query("ALTER TABLE `" . DB_PREFIX . "product` DROP COLUMN choose_payment_date");
		$this->db->query("ALTER TABLE `" . DB_PREFIX . "download` DROP COLUMN subscription");
		$this->db->query("ALTER TABLE `" . DB_PREFIX . "order_product` DROP COLUMN pro_rated");
		return;
	}

}
