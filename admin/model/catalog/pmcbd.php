<?php
class ModelCatalogPmcbd extends Model {
	public function addPmcbd($data) { 
		$this->db->query("INSERT INTO " . DB_PREFIX . "pmcbd SET target_product_id = '" . (int)$data['target_product_id'] . "', `combo_setting` = '" . $this->db->escape(json_encode($data['combo_setting'])) . "', status = '" . (int)$data['status'] . "',limitset = '" . (int)$data['limitset'] . "', date_modified = NOW(), date_added = NOW()");

		$pmcbd_id = $this->db->getLastId();

		$this->cache->delete('pmcbd'); 

		return $pmcbd_id;
	}

	public function editPmcbd($pmcbd_id, $data) { 
  		$this->db->query("UPDATE " . DB_PREFIX . "pmcbd SET target_product_id = '" . (int)$data['target_product_id'] . "', `combo_setting` = '" . $this->db->escape(json_encode($data['combo_setting'])) . "', status = '" . (int)$data['status'] . "', limitset = '" . (int)$data['limitset'] . "', date_modified = NOW() WHERE pmcbd_id = '" . (int)$pmcbd_id . "'");
 	   
		$this->cache->delete('pmcbd'); 
	}

	public function deletePmcbd($pmcbd_id) {
  		$this->db->query("DELETE FROM " . DB_PREFIX . "pmcbd WHERE pmcbd_id = '" . (int)$pmcbd_id . "'");
 
		$this->cache->delete('pmcbd');
  	}
  
	public function getPmcbd($pmcbd_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "pmcbd WHERE pmcbd_id = '" . (int)$pmcbd_id . "' ");

		return $query->row;
	}

	public function getPmcbds($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "pmcbd WHERE 1";
 		  
  		$sort_data = array(
			'date_added',
			'date_modified'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY date_added";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
 	 
	public function getTotalPmcbd() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "pmcbd");

		return $query->row['total'];
	} 	 
}
