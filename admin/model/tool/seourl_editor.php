<?php
class ModelToolSeourlEditor extends Model {
	public function addSeourl($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = '" . $this->db->escape($data['query']) . "', keyword = '" . $this->db->escape($data['keyword']) . "'");

		$seourl_id = $this->db->getLastId();

		return $seourl_id;
	}

	public function editSeourl($url_alias_id, $data) {
        $query = $this->db->query("SELECT query FROM " . DB_PREFIX . "url_alias ua WHERE ua.url_alias_id = " . (int)$url_alias_id);
        if (isset($query->row['query'])) $this->cache->delete($query->row['query']);

		$this->db->query("UPDATE " . DB_PREFIX . "url_alias SET query = '" . $this->db->escape($data['query']) . "', keyword = '" . $this->db->escape($data['keyword']) . "' WHERE url_alias_id = " . (int)$url_alias_id);
	}

	public function deleteSeourl($url_alias_id) {
        $query = $this->db->query("SELECT query FROM " . DB_PREFIX . "url_alias ua WHERE ua.url_alias_id = " . (int)$url_alias_id);
        if (isset($query->row['query'])) $this->cache->delete($query->row['query']);

        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE url_alias_id = " . (int)$url_alias_id);
	}

	public function getSeourl($url_alias_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias ua WHERE ua.url_alias_id = " . (int)$url_alias_id);

		return $query->row;
	}

	public function getSeourls($data = array()) {
        $like = '';
        if (isset($data['tab'])) {
            $like = ' WHERE ua.query ';
            switch ($data['tab']) {
                case 'store':
                    $like .= 'NOT LIKE "%_id%"';
                    break;
                case 'product':
                    $like .= 'LIKE "product_id%"';
                    break;
                case 'category':
                    $like .= 'LIKE "category_id%"';
                    break;
                case 'manufacturer':
                    $like .= 'LIKE "manufacturer_id%"';
                    break;
                case 'information':
                    $like .= 'LIKE "information_id%"';
                    break;
                default:
                    $like = '';
            }
        }

		$sql = 'SELECT * FROM ' . DB_PREFIX . 'url_alias ua' . $like;

        if (!empty($data['filter_query'])) {
            $sql .= " AND ua.query LIKE '" . $this->db->escape($data['filter_query']) . "%'";
        }

        if (!empty($data['filter_keyword'])) {
            $sql .= " AND ua.keyword LIKE '%" . $this->db->escape($data['filter_keyword']) . "%'";
        }

		$sort_data = array(
			'ua.query',
			'ua.keyword',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY ua.query";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalSeourls($data = array()) {
        $like = '';
        if (isset($data['tab'])) {
            $like = ' WHERE ua.query ';
            switch ($data['tab']) {
                case 'store':
                    $like .= 'NOT LIKE "%_id%"';
                    break;
                case 'product':
                    $like .= 'LIKE "product_id%"';
                    break;
                case 'category':
                    $like .= 'LIKE "category_id%"';
                    break;
                case 'manufacturer':
                    $like .= 'LIKE "manufacturer_id%"';
                    break;
                case 'information':
                    $like .= 'LIKE "information_id%"';
                    break;
                default:
                    $like = '';
            }
        }
        $sql = 'SELECT COUNT(*) AS total FROM ' . DB_PREFIX . 'url_alias ua' . $like;

		if (!empty($data['filter_query'])) {
			$sql .= " AND ua.query LIKE '" . $this->db->escape($data['filter_query']) . "%'";
		}

		if (!empty($data['filter_keyword'])) {
			$sql .= " AND ua.keyword LIKE '%" . $this->db->escape($data['filter_keyword']) . "%'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}
}