<?php

class ModelExtensionPaymentGlobalonepaySecure extends Model {

	public function install() {
		$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "customer_sec` (
			  `customer_sec_id` INT(11) NOT NULL AUTO_INCREMENT,
			  `customer_id` INT(11) NOT NULL,
			  `order_id` INT(11) NOT NULL,
			  `merchantref` VARCHAR(50),
			  `responseref` VARCHAR(100),
			  `responsecardtype`  VARCHAR(20),
			  `cardexpiry` VARCHAR(20),
			  `date_added` DATETIME NOT NULL,
			  `date_modified` DATETIME NULL,
			  PRIMARY KEY (`customer_sec_id`)
			) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci;");
	}

	public function uninstall() {
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "customer_sec`;");
	}

}
