<?php
class ModelMonthlyPaymentReporting extends Model  {  

    public function getTotalLoginAttempts($email) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "affiliate_login` WHERE `email` = '" . $this->db->escape($email) . "'");

        return $query->row;
    }

    public function deleteLoginAttempts($email) {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "affiliate_login` WHERE `email` = '" . $this->db->escape($email) . "'");
    }

    public function getData($data = array()) { 
       
       $sql = "SELECT a.affiliate_id,CONCAT(a.firstname, ' ', a.lastname) AS name, a.company,SUM(com.mta_commission) as AmountCalc  FROM `" . DB_PREFIX . "commissions` as com INNER JOIN `" . DB_PREFIX . "mta_order` as mo on com.`mta_order_id`= mo.`mta_order_id` INNER JOIN " . DB_PREFIX . "order as o ON mo.order_id = o.order_id inner JOIN " . DB_PREFIX . "affiliate as a on o.affiliate_id=a.affiliate_id  "; 
        
        $implode = array();
 
        if (!empty($data['affiliate_id'])) {
            $implode[] = "a.affiliate_id = '" . $this->db->escape($data['affiliate_id']) . "'";
        }
        
        if (!empty($data['filter_date_start'])) {
            $implode[] = "DATE(a.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
        }

        if (!empty($data['filter_date_end'])) {
            $implode[] = "DATE(a.date_added) = DATE('" . $this->db->escape($data['filter_date_end']) . "')";
        }

        if ($implode) {
            $sql .= " WHERE " . implode(" AND ", $implode);
        }

        $sort_data = array(
            'name',
            'a.company',
        );
        
        $sql .= " GROUP by a.affiliate_id" ; 

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }
        
       
        $query = $this->db->query($sql);
        return $query->rows;
    } 


     

    public function getIFSMemberlisting() {
        
        $query = $this->db->query("SELECT *, CONCAT(a.firstname, ' ', a.lastname) AS name, (SELECT SUM(at.amount) FROM " . DB_PREFIX . "affiliate_transaction at WHERE at.affiliate_id = a.affiliate_id GROUP BY at.affiliate_id) AS balance, mta_a.mta_scheme_id as scheme_id, mta_a.level_original as level FROM " . DB_PREFIX . "affiliate AS a left join " . DB_PREFIX . "mta_affiliate AS mta_a on mta_a.affiliate_id=a.affiliate_id");
        return $query->rows;
    }


    public function getTotal($data = array()) {

    $sql = "SELECT COUNT(*) AS total, CONCAT(a.firstname, ' ', a.lastname) AS name FROM `" . DB_PREFIX . "commissions` as com INNER JOIN `" . DB_PREFIX . "mta_order` as mo on com.`mta_order_id`= mo.`mta_order_id` INNER JOIN " . DB_PREFIX . "order as o ON mo.order_id = o.order_id inner JOIN " . DB_PREFIX . "affiliate as a on o.affiliate_id=a.affiliate_id";
 
        $implode = array();
 
        if (!empty($data['affiliate_id'])) {
            $implode[] = "a.affiliate_id = '" . $this->db->escape($data['affiliate_id']) . "'";
        }
        
        if (!empty($data['filter_date_start'])) {
            $implode[] = "DATE(a.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
        }

        if (!empty($data['filter_date_end'])) {
            $implode[] = "DATE(a.date_added) = DATE('" . $this->db->escape($data['filter_date_end']) . "')";
        }

        if ($implode) {
            $sql .= " WHERE " . implode(" AND ", $implode);
        }

        $sort_data = array(
            'name',
            'a.company',
        );
        
        $sql .= " GROUP by a.affiliate_id" ; 

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY name";
        }
 
        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }
         
        //echo '<pre>'; print_r($sql); die;
        $query = $this->db->query($sql);
        return $query->row['total'];
    } 


    public function affiliateType($affiliate_id) {

        $query = $this->db->query("SELECT af.firstname FROM " . DB_PREFIX ."mta_affiliate mt_af,". DB_PREFIX ."affiliate af WHERE af.affiliate_id = '" . $this->db->escape($affiliate_id) . "' AND af.affiliate_id = mt_af.affiliate_id AND mt_af.level_original =2");
        //echo "<pre>"; print_r(expression)
        return $query; 

    }

    public function amountSent($amount, $id) {
            
    }

    public function savePayment($data = array()) {
      
         $paymentref = $data['paymentref'];
         $affiliate_id = $this->request->get['affiliate_id'];
         $amountsent = $data['amountsent'];
         $amountcalc = $data['amountcalc'];
         $payment = $data['payment'];
         $paymentnote = $data['paymentnote'];
           
         $query = $this->db->query("INSERT INTO `". DB_PREFIX . "commission_payments`(`payment_reference`, `payment_affiliate_id`, `payment_amount_calculated`, `payment_amount_sent`, `payment_note`, `payment_type`, `payment_date`) VALUES ('$paymentref','$affiliate_id','$amountcalc','$amountsent','$paymentnote','$payment', NOW())") ; 
         return true; 
    }
}