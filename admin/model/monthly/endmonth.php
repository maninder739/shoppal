<?php
class ModelMonthlyEndMonth extends Model  
{
    public function getOrders($data = array())
    {
        $sql = "SELECT o.order_id, o.customer_id, o.order_status_id,  CONCAT(o.firstname, ' ', o.lastname) AS customer, (SELECT os.name FROM " . DB_PREFIX . "order_status os WHERE os.order_status_id = o.order_status_id AND os.language_id = '1')  AS order_status, (SELECT SUM(total) FROM `" . DB_PREFIX . "order_product` WHERE order_id = o.order_id GROUP BY order_id) as retail_total, (SELECT a.company FROM " . DB_PREFIX . "affiliate a WHERE a.code = o.tracking ) as orgname  , o.currency_code, o.tracking, o.total, o.currency_value,  mo.affiliate_id, mo.mta_order_id, (select concat(b.firstname, ' ', b.lastname) from " . DB_PREFIX . "affiliate b where b.affiliate_id=mo.affiliate_id) as refaffiliate, mo.lock ,mo.comission_added_status,mo.commission, mo.commission_added,  o.date_added, o.date_modified  from " . DB_PREFIX . "mta_order as mo inner join " . DB_PREFIX . "affiliate_transaction as at on at.affiliate_id = mo.affiliate_id AND at.order_id = mo.order_id inner join  " . DB_PREFIX . "order as o on mo.order_id = o.order_id ";

        if (isset($data['filter_order_status'])) {

            $implode = array();

            $order_statuses = explode(',', $data['filter_order_status']);

            foreach ($order_statuses as $order_status_id) {
                $implode[] = "o.order_status_id = '" . (int) $order_status_id . "'";
            }

            if ($implode) {
                $sql .= " WHERE (" . implode(" OR ", $implode) . ")";
            }
        } else {

            $sql .= " WHERE o.order_status_id >= '0' ";
        }

        if (!empty($data['filter_tracking_code'])) {
            $sql .= " AND o.tracking = '" . $data['filter_tracking_code'] . "'";
        }

        if (!empty($data['filter_date_start'])) {
            $sql .= " AND DATE(o.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
        }

        if (!empty($data['filter_date_end'])) {
            $sql .= " AND DATE(o.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
        }

        $sort_data = array(
            'o.order_id',
            'customer',
            'tracking',
            'retail_total',
            'orgname',
            'order_status',
            'o.date_added',
            'mo.commission',
            'refaffiliate',
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {

            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY o.order_id";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        
        $query = $this->db->query($sql);
        return $query->rows;

    }

    public function getTotalOrders($data = array())
    {
    	 
        $sql = "SELECT COUNT(*) AS total from " . DB_PREFIX . "mta_order as mo inner join " . DB_PREFIX . "affiliate_transaction as at on at.affiliate_id = mo.affiliate_id AND at.order_id = mo.order_id inner join  " . DB_PREFIX . "order as o on mo.order_id = o.order_id";

        if (isset($data['filter_order_status'])) {
            $implode = array();

            $order_statuses = explode(',', $data['filter_order_status']);

            foreach ($order_statuses as $order_status_id) {
                $implode[] = "order_status_id = '" . (int) $order_status_id . "'";
            }

            if ($implode) {
                $sql .= " WHERE (" . implode(" OR ", $implode) . ")";
            }
        } else {
            $sql .= " WHERE order_status_id >= '0'";
        }

        if (!empty($data['filter_tracking_code'])) {
            $sql .= " AND o.tracking = '" . $data['filter_tracking_code'] . "'";
        }

        if (!empty($data['filter_date_start'])) {
            $sql .= " AND DATE(o.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
        }

        if (!empty($data['filter_date_end'])) {
            $sql .= " AND DATE(o.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getMemberOrderReportExport()
    {
        $sql = "SELECT aff.code, aff.company, CONCAT(aff.firstname, ' ', aff.lastname) AS contact, c.customer_id, CONCAT(c.firstname, ' ', c.lastname) AS customer, c.email, cgd.name AS customer_group, c.status, o.order_id, SUM(op.quantity) as products, o.total AS total FROM `" . DB_PREFIX . "order` o LEFT JOIN `" . DB_PREFIX . "order_product` op ON (o.order_id = op.order_id) LEFT JOIN `" . DB_PREFIX . "customer` c ON (o.customer_id = c.customer_id) LEFT JOIN `" . DB_PREFIX . "customer_group_description` cgd ON (c.customer_group_id = cgd.customer_group_id) LEFT JOIN `" . DB_PREFIX . "affiliate` aff on (c.affiliate_id=aff.affiliate_id) WHERE o.customer_id > 0 AND cgd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND o.order_status_id > '0' GROUP BY o.order_id ";

        $sql = "SELECT t.code, t.company, t.contact, t.customer_id, t.customer, t.email, t.customer_group, t.status, COUNT(DISTINCT t.order_id) AS orders, SUM(t.products) AS products, SUM(t.total) AS total FROM (" . $sql . ") AS t GROUP BY t.customer_id ORDER BY t.company, total DESC";

        $query = $this->db->query($sql);
        return $query->rows;
        
    }

    public function applyamount($data = array()) {
          
        $mtaOrder_ids = array();
        if(!empty($data)) {
             $allmtaOrderid = array();
            foreach ($data['selected'] as $key => $value) {
                $mtaOrder_ids = explode('|', $value);
                $order_id =  $mtaOrder_ids[0];
                $mta_order_id =  $mtaOrder_ids[1];
                $this->db->query("UPDATE `". DB_PREFIX . "mta_order` SET `commission_added` = '1'  WHERE `order_id` =   '" . (int)$order_id . "' AND `mta_order_id` = '" . (int)$mta_order_id . "'   "); 
            }
            
            foreach ($data['unselected'] as $key => $value) {
                $mtaOrder_ids = explode('|', $value);
                $order_id =  $mtaOrder_ids[0];
                $mta_order_id =  $mtaOrder_ids[1];
                $this->db->query("UPDATE `". DB_PREFIX . "mta_order` SET `commission_added` = '0'  WHERE `order_id` =   '" . (int)$order_id . "' AND `mta_order_id` = '" . (int)$mta_order_id . "'   ");  

                // $this->db->query("UPDATE `". DB_PREFIX . "mta_order` SET `comission_added_status` = '0'  WHERE   `mta_order_id` = '" . (int)$mta_order_id . "'   "); 

                //$this->db->query("DELETE FROM `". DB_PREFIX . "commissions`  WHERE   `mta_order_id` = '" . (int)$mta_order_id . "'");
            }
           return true;
        }
          
       return false;
    }

    public function approve($affiliate_id, $mta_order_id, $mta_commission, $customer,$order_status) {
        
        // $this->db->query("INSERT INTO `". DB_PREFIX . "commissions`(`commission_affiliate_id`, `mta_order_id`, `mta_commission`, `commissions_date`, `commission_status_id`, `user_id`, `action_date`) VALUES ('$affiliate_id','$mta_order_id', '$mta_commission' , NOW() ,'$order_status', '$customer', NOW() )" );
         $sql = $this->db->query("SELECT `comission_added_status` FROM ". DB_PREFIX . "mta_order  WHERE `mta_order_id` = '" . (int)$mta_order_id . "'     ");
          
         foreach ($sql->row as $key => $value) {
            if($value == 0) {
                $this->db->query("UPDATE `". DB_PREFIX . "mta_order` SET `comission_added_status` = '1'  WHERE `mta_order_id` = '" . (int)$mta_order_id . "'   "); 
            } else {
                 $this->db->query("UPDATE `". DB_PREFIX . "mta_order` SET `comission_added_status` = '0'  WHERE `mta_order_id` = '" . (int)$mta_order_id . "'   "); 
            }
         }
        return true;
 
    }

    public function lock()
    {
        
        $affiliate_id = $this->request->get['affiliate_id'];
        $mta_order_id = $this->request->get['mta_order_id'];
        $mta_commission = $this->request->get['mta_commission'];
        $customer_id = $this->request->get['customer_id'];
        $order_status_id = $this->request->get['order_status_id'];

        $this->db->query("UPDATE `". DB_PREFIX . "mta_order` SET `lock` = '1'  WHERE   `mta_order_id` = '" . (int)$this->request->get['mta_order_id'] . "'   ");

        $query = $this->db->query("SELECT `comission_added_status` FROM ". DB_PREFIX . "mta_order WHERE   `mta_order_id` = '" . (int)$this->request->get['mta_order_id'] . "'   ");   
        $row = $query->row;
          
        if($row['comission_added_status'] == 1) {
         $this->db->query("INSERT INTO `". DB_PREFIX . "commissions`(`commission_affiliate_id`, `mta_order_id`, `mta_commission`, `commissions_date`, `commission_status_id`, `user_id`, `action_date`) VALUES ('$affiliate_id','$mta_order_id', '$mta_commission' , NOW() ,'$order_status_id', '$customer_id', NOW() )" );  
        }

         return true;
    }
}
