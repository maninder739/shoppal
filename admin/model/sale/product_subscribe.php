<?php
class ModelSaleProductSubscribe extends Model {

	public function getOrderSubscriptions($data) {
		$sql = "SELECT * FROM " . DB_PREFIX . "order_subscriptions ORDER BY order_id DESC";
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$start = 0;
			} else {
				$start = $data['start'];
			}
			if ($data['limit'] < 1) {
				$limit = 10;
			} else {
				$limit = $data['limit'];
			}
			$sql .= " LIMIT " . (int)$start . "," . (int)$limit;
		}
		$query = $this->db->query($sql);
		return $query->rows;
	}
	
	public function getOrderSubscription($order_subscription_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_subscriptions WHERE order_subscription_id = '" . (int)$order_subscription_id . "'");
		return $query->row;
	}
	
	public function getSubscription($subscription_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "subscriptions WHERE subscription_id = '" . (int)$subscription_id . "'");
		return $query->row;
	}

	public function getSubscriptionId($order_subscription_id) {
		$query = $this->db->query("SELECT subscription_id FROM " . DB_PREFIX . "order_subscriptions WHERE order_subscription_id = '" . (int)$order_subscription_id . "'");
		return $query->row['subscription_id'];
	}

	public function getProduct($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product_id . "'");
		return $query->row;
	}
	
	public function countOrderSubscriptions($data) {
		$query = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "order_subscriptions");
		return $query->row['total'];
	}
	
	public function updateCost($order_subscription_id, $amount, $terms) {
		$this->db->query("UPDATE " . DB_PREFIX . "order_subscriptions SET terms = '" . $this->db->escape($terms) . "', payment_amount = '" . (float)$amount . "' WHERE order_subscription_id = '" . (int)$order_subscription_id . "'");
		return;
	}

	public function updateStartDate($data) {
		$this->db->query("UPDATE " . DB_PREFIX . "order_subscriptions SET start_date = '" . (int)strtotime($data['new_date']) . "' WHERE order_subscription_id = '" . (int)$data['order_subscription_id'] . "'");
		return;
	}

	public function updateEndDate($data) {
		$this->db->query("UPDATE " . DB_PREFIX . "order_subscriptions SET end_date = '" . (int)strtotime($data['new_date']) . "', next_due = '" . (int)strtotime($data['new_date']) . "' WHERE order_subscription_id = '" . (int)$data['order_subscription_id'] . "'");
		return;
	}

	public function updateNextDue($data) {
		$this->db->query("UPDATE " . DB_PREFIX . "order_subscriptions SET next_due = '" . (int)strtotime($data['new_date']) . "' WHERE order_subscription_id = '" . (int)$data['order_subscription_id'] . "'");
		return;
	}

	public function updateLastPaid($data) {
		$this->db->query("UPDATE " . DB_PREFIX . "order_subscriptions SET last_paid = '" . (int)strtotime($data['new_date']) . "' WHERE order_subscription_id = '" . (int)$data['order_subscription_id'] . "'");
		return;
	}

	public function deleteSubscription($order_subscription_id) {
		$order_subscription_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_subscriptions WHERE order_subscription_id = '" . (int)$order_subscription_id . "'");
		$order_id = $order_subscription_query->row['order_id'];
		$recurring_id = $order_subscription_query->row['subscription_id'];
		
		$order_recurring_query = $this->db->query("SELECT order_recurring_id, reference FROM " . DB_PREFIX . "order_recurring WHERE order_id = '" . (int)$order_id . "' AND recurring_id = '" . (int)$recurring_id . "'");

		$status = true;
		$order_recurring_id = 0;

		if ($order_recurring_query->num_rows) {
			$order_recurring_id = $order_recurring_query->row['order_recurring_id'];
			if ($order_recurring_query->row['reference']) {
				$payment_code = $this->db->query("SELECT payment_code FROM `" . DB_PREFIX . "order` WHERE order_id = '" . (int)$order_id . "'");
				if ($payment_code->row['payment_code'] == 'pp_express') {
					$this->load->model('extension/payment/pp_express');
					$result = $this->model_extension_payment_pp_express->recurringCancel($order_recurring_query->row['reference']);
					if (!isset($result['PROFILEID']) && !$result['ACK'] == "Success") {
						$status = false;
					}
				} elseif ($payment_code->row['payment_code'] == 'authorizenet_aim_arb') {
					$this->load->model('extension/payment/authorizenet_aim_arb');
					$result = $this->model_extension_payment_authorizenet_aim_arb->updateRecurring($order_recurring_query->row['order_recurring_id'], 4);
					if (!$result) {
						$status = false;
					}
				}
			}
		}
		if ($status) {
			$this->db->query("DELETE FROM " . DB_PREFIX . "order_subscriptions WHERE order_subscription_id = '" . (int)$order_subscription_id . "'");
			$this->db->query("DELETE FROM " . DB_PREFIX . "order_subscriptions_transaction WHERE order_subscription_id = '" . (int)$order_subscription_id . "'");
			if ($order_recurring_id) {
				$this->db->query("DELETE FROM " . DB_PREFIX . "order_recurring WHERE order_recurring_id = '" . (int)$order_recurring_id . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "order_recurring_transaction WHERE order_recurring_id = '" . (int)$order_recurring_id . "'");
			}
		}
		return;
	}
	
	public function getCustomerId($order_subscription_id) {
		$customer_id = 0;
		$query = $this->db->query("SELECT customer_id FROM " . DB_PREFIX . "order_subscriptions WHERE order_subscription_id = '" . (int)$order_subscription_id . "'");
		if ($query->num_rows) {
			$customer_id = $query->row['customer_id'];
		}
		return $customer_id;
	}

	public function getProfile($order_recurring_id) {
		$profile = array();
		$result = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_recurring` WHERE order_recurring_id = '" . (int)$order_recurring_id . "'")->row;
		if ($result) {
			$profile = array(
				'order_recurring_id'	=> $result['order_recurring_id'],
				'order_id'				=> $result['order_id'],
				'status'				=> $this->getStatus($result['status']),
				'status_id'				=> $result['status'],
				'recurring_id'			=> $result['recurring_id'],
				'recurring_name'			=> $result['recurring_name'],
				'recurring_description'	=> $result['recurring_description'],
				'reference'		=> $result['reference'],
				'product_name'			=> $result['product_name'],
				'product_quantity'		=> $result['product_quantity'],
				'recurring_price'		=> $result['recurring_price']
			);
		}
		return $profile;
	}

	public function getProfileTransactions($order_recurring_id) {
		$results =  $this->db->query("SELECT amount, type, date_added FROM " . DB_PREFIX . "order_recurring_transaction WHERE order_recurring_id = " . (int)$order_recurring_id . " ORDER BY date_added DESC")->rows;
		$transactions = array();
		foreach ($results as $result) {
			switch ($result['type']) {
				case 0:
					$type = $this->language->get('text_transaction_created');
					break;
				case 1:
					$type = $this->language->get('text_transaction_payment');
					break;
				case 2:
					$type = $this->language->get('text_transaction_outstanding_payment');
					break;
				case 3:
					$type = $this->language->get('text_transaction_skipped');
					break;
				case 4:
					$type = $this->language->get('text_transaction_failed');
					break;
				case 5:
					$type = $this->language->get('text_transaction_cancelled');
					break;
				case 6:
					$type = $this->language->get('text_transaction_suspended');
					break;
				case 7:
					$type = $this->language->get('text_transaction_suspended_failed');
					break;
				case 8:
					$type = $this->language->get('text_transaction_outstanding_failed');
					break;
				case 9:
					$type = $this->language->get('text_transaction_expired');
					break;
				default:
					$type = '';
					break;
			}
			$transactions[] = array(
				'date_added' => $result['date_added'],
				'amount' => $result['amount'],
				'type' => $type,
			);
		}
		return $transactions;
	}

	public function getOrderRecurringId($order_id, $product_id) {
		$pro_ref = 0;
		$query = $this->db->query("SELECT order_recurring_id FROM " . DB_PREFIX . "order_recurring WHERE order_id = '" . (int)$order_id . "' AND product_id = '" . (int)$product_id . "'");
		if ($query->num_rows) {
			$pro_ref = $query->row['order_recurring_id'];
		}
		return $pro_ref;
	}

	private function getStatus($status) {
		switch ($status) {
			case 1:
				$result = $this->language->get('text_status_inactive');
				break;
			case 2:
				$result = $this->language->get('text_status_active');
				break;
			case 3:
				$result = $this->language->get('text_status_suspended');
				break;
			case 4:
				$result = $this->language->get('text_status_cancelled');
				break;
			case 5:
				$result = $this->language->get('text_status_expired');
				break;
			case 6:
				$result = $this->language->get('text_status_pending');
				break;
			default:
				$result = '';
				break;
		}
		return $result;
	}

	public function activate($order_subscription_id) {
		$subscription_query = $this->db->query("SELECT order_id, product_id, subscription_id, end_date FROM " . DB_PREFIX . "order_subscriptions WHERE order_subscription_id = '" . (int)$order_subscription_id . "'");
		$reinstatement_fee = 0;
		$reinstatement_period = 0;
		$product_name = '';
		$fee_query = $this->db->query("SELECT reinstatement_fee, reinstatement_period FROM " . DB_PREFIX . "subscriptions WHERE subscription_id = '" . (int)$subscription_query->row['subscription_id'] . "'");
		$reinstatement_fee = $fee_query->row['reinstatement_fee'];
		$reinstatement_period = $fee_query->row['reinstatement_period'];
		$product_name = $this->db->query("SELECT name FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$subscription_query->row['product_id'] . "'");
		if ($subscription_query->row['end_date'] > 0) {
			$end_date = strtotime(date('Y-m-d', time()) . '+ ' . $reinstatement_period . ' days');
		} else {
			$end_date = 0;
		}
		$this->db->query("UPDATE " . DB_PREFIX . "order_subscriptions SET active = 1, end_date = '" . (int)$end_date . "', reinstatement_fee = '" . (float)$reinstatement_fee . "' WHERE order_subscription_id = '" . (int)$order_subscription_id . "'");
		$email = $this->db->query("SELECT email FROM `" . DB_PREFIX . "order` WHERE order_id = '" . (int)$subscription_query->row['order_id'] . "'");
		$subject = sprintf($this->language->get('text_reinstatement_subject'), $product_name->row['name']);
		$message = sprintf($this->language->get('text_reinstatement_message'), $product_name->row['name'], $reinstatement_period, $reinstatement_period);
		if ($reinstatement_fee > 0) {
			$message .= "<br /><br />" . sprintf($this->language->get('text_reinstatement_fee'), $this->currency->format($reinstatement_fee));
		}
		$message .= "<br /><br />";
		$url = HTTP_CATALOG . 'index.php?route=account/order/info&order_id=' . $subscription_query->row['order_id'];
		$message .= sprintf($this->language->get('text_customer_link'), $url);
		
		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
		
		$mail->setTo($this->config->get('config_email'));
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
		$mail->setSubject(sprintf($this->language->get('text_approve_subject'), $store_name));
		$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
		$mail->send();
		return;
	}

	public function sendNotifications() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_subscriptions WHERE active = '1'");
		if ($query->num_rows) {
			$this->load->language('sale/product_subscribe');
			foreach ($query->rows as $row) {
				if ($row['subscription_id'] && $row['end_date'] > 0) {
					$subscription_info = $this->db->query("SELECT * FROM " . DB_PREFIX . "subscriptions WHERE subscription_id = '" . (int)$row['subscription_id'] . "'");
					if ($subscription_info) {
						$product_info = $this->db->query("SELECT name FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$row['product_id'] . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");
						$count = $subscription_info->row['reminder_count'] - $row['reminders_sent'];
						if ($count > 0) {
							$timeframe = 86400 * ($count * $subscription_info->row['reminder_period']);
							if ($subscription_info->row['grace_period'] > 0) {
								$grace_period = 86400 * $subscription_info->row['grace_period'];
							} else {
								$grace_period = 0;
							}
							$value = $row['end_date'] + $grace_period - $timeframe;
							if (time() > ($row['end_date'] + $grace_period) - $timeframe) {
								$email = $this->db->query("SELECT email FROM `" . DB_PREFIX . "order` WHERE order_id = '" . (int)$row['order_id'] . "'");
								if ($count > 1) {
									$subject = sprintf($this->language->get('text_subject'), $product_info->row['name'], $row['order_id']);
								} else {
									$subject = sprintf($this->language->get('text_final_subject'), $product_info->row['name'], $row['order_id']);
								}
								if ($count == 1) {
									$days_left = round((($row['end_date'] + $grace_period) - time()) / 86400);
									$message = sprintf($this->language->get('text_final_message'), $product_info->row['name'], $days_left, $days_left);
								} else {
									$message = $this->language->get('text_message');
								}
								$message .= "<br /><br />";
								$url = HTTP_SERVER . 'index.php?route=account/order/info&order_id=' . $row['order_id'];
								$message .= sprintf($this->language->get('text_customer_link'), $url);
								
								$mail = new Mail();
								$mail->protocol = $this->config->get('config_mail_protocol');
								$mail->parameter = $this->config->get('config_mail_parameter');
								$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
								$mail->smtp_username = $this->config->get('config_mail_smtp_username');
								$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
								$mail->smtp_port = $this->config->get('config_mail_smtp_port');
								$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
								
								$mail->setTo($email->row['email']);
								$mail->setFrom($this->config->get('config_email'));
								$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
								$mail->setSubject($subject);
								$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
								$mail->send();
								
								$mail = new Mail();
								$mail->protocol = $this->config->get('config_mail_protocol');
								$mail->parameter = $this->config->get('config_mail_parameter');
								$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
								$mail->smtp_username = $this->config->get('config_mail_smtp_username');
								$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
								$mail->smtp_port = $this->config->get('config_mail_smtp_port');
								$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
								
								$mail->setTo($this->config->get('config_email'));
								$mail->setFrom($this->config->get('config_email'));
								$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
								$mail->setSubject(sprintf($this->language->get('text_approve_subject'), $store_name));
								$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
								$mail->send();
								
								$this->db->query("UPDATE " . DB_PREFIX . "order_subscriptions SET reminder_sent = '" . time() . "', reminders_sent = (reminders_sent + 1) WHERE order_subscription_id = '" . (int)$row['order_subscription_id'] . "'");
							}
						} else {
							if ($subscription_info->row['grace_period'] > 0) {
								$grace_period = 86400 * $subscription_info->row['grace_period'];
							} else {
								$grace_period = 0;
							}
							if (time() > $row['end_date'] + $grace_period) {
								$this->db->query("UPDATE " . DB_PREFIX . "order_subscriptions SET active = 0 WHERE order_subscription_id = '" . (int)$row['order_subscription_id'] . "'");
							}
						}
					}
				}
			}
		}
		return;
	}

}
