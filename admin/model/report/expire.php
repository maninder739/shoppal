<?php
class ModelReportExpire extends Model {

    public function getExpiring($data = array()) {

    $sql = "SELECT c.customer_id, o.firstname, o.lastname, o.email, o.telephone, o.payment_city, o.payment_zone, o.payment_postcode, substring(cardexpiry,1,2) as expmonth, substring(cardexpiry,-2,2) as expyear FROM `" . DB_PREFIX . "customer_sec` as c, `" . DB_PREFIX . "order` as o WHERE c.order_id = o.order_id order by expyear, expmonth";
 
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalExpiring($data = array()) {

         $sql = "SELECT COUNT(DISTINCT c.customer_id) as total FROM `" . DB_PREFIX . "customer_sec` as c, `" . DB_PREFIX . "order` as o WHERE c.order_id = o.order_id ";
    
        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getExpiringReportExport() {
        $sql = "SELECT c.customer_id, o.firstname, o.lastname, o.email, o.telephone, o.payment_city, o.payment_zone, o.payment_postcode, substring(cardexpiry,1,2) as expmonth, substring(cardexpiry,-2,2) as expyear FROM `" . DB_PREFIX . "customer_sec` as c, `" . DB_PREFIX . "order` as o WHERE c.order_id = o.order_id order by expyear, expmonth";
        $query = $this->db->query($sql);
        return $query->rows;
    }

}
