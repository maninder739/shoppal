<?php

class ModelReportProduct extends Model {

    public function getProductsViewed($data = array()) {
        $sql = "SELECT pd.name, p.model, p.viewed FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND p.viewed > 0 ORDER BY p.viewed DESC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalProductViews() {
        $query = $this->db->query("SELECT SUM(viewed) AS total FROM " . DB_PREFIX . "product");

        return $query->row['total'];
    }

    public function getTotalProductsViewed() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE viewed > 0");

        return $query->row['total'];
    }

    public function reset() {
        $this->db->query("UPDATE " . DB_PREFIX . "product SET viewed = '0'");
    }

    public function getPurchased($data = array()) {
        $sql = "SELECT  op.product_id, op.name, op.model, SUM(op.quantity) AS quantity, SUM(op.price + (op.tax * op.quantity)) AS total FROM `" . DB_PREFIX . "category` oct LEFT JOIN `" . DB_PREFIX . "product_to_category` AS ocp ON (oct.category_id = ocp.category_id) LEFT JOIN `" . DB_PREFIX . "order_product` AS op ON (op.product_id = ocp.product_id) LEFT JOIN `" . DB_PREFIX . "order` AS o ON (op.order_id = o.order_id) ";




        if (!empty($data['filter_order_status_id'])) {
            $implode = array();

            $order_statuses = explode(',', $data['filter_order_status_id']);

            foreach ($order_statuses as $order_status_id) {
                $implode[] = "order_status_id = '" . (int) $order_status_id . "'";
            }

            if ($implode) {
                $sql .= " WHERE (" . implode(" OR ", $implode) . ")";
            }
        } else {
            $sql .= " WHERE (order_status_id = '3' OR  order_status_id = '5') ";
        }

        if (!empty($data['filter_product_id'])) {
            $implodes = array();

            $filter_product_ids = explode(',', $data['filter_product_id']);

            foreach ($filter_product_ids as $filter_product_id) {
                $implodes[] = "op.product_id = '" . (int) $filter_product_id . "'";
            }

            if ($implodes) {
                $sql .= " AND (" . implode(" OR ", $implodes) . ")";
            }
        }

        if (!empty($data['filter_category_id'])) {
            $implodes_filter = array();

            $filter_category_ids = explode(',', $data['filter_category_id']);
            // print_r( $filter_product_ids); die;
            foreach ($filter_category_ids as $filter_category_id) {
                $implodes_filter[] = "oct.category_id = '" . (int) $filter_category_id . "'";
            }

            if ($implodes_filter) {
                $sql .= " AND (" . implode(" OR ", $implodes_filter) . ")";
            }
        }


        if (!empty($data['filter_date_start'])) {
            $sql .= " AND DATE(o.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
        }

        if (!empty($data['filter_date_end'])) {
            $sql .= " AND DATE(o.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
        }

        $sql .= " GROUP BY op.product_id ";
        $sort_data = array(
            'op.name',
            'op.model',
            'total',
            'category_name',
            'quantity'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY total ";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (!empty($data['start']) || !empty($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

//        echo "<pre>";
//        print_r($sql);
//        die;
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalPurchased($data) {

        $sql = "SELECT COUNT(DISTINCT op.product_id) AS total FROM `" . DB_PREFIX . "category` oct LEFT JOIN `" . DB_PREFIX . "product_to_category` AS ocp ON (oct.category_id = ocp.category_id) LEFT JOIN `" . DB_PREFIX . "order_product` AS op ON (op.product_id = ocp.product_id) LEFT JOIN `" . DB_PREFIX . "order` AS o ON (op.order_id = o.order_id) ";

        if (!empty($data['filter_order_status_id'])) {
            $implode = array();

            $order_statuses = explode(',', $data['filter_order_status_id']);

            foreach ($order_statuses as $order_status_id) {
                $implode[] = "order_status_id = '" . (int) $order_status_id . "'";
            }

            if ($implode) {
                $sql .= " WHERE (" . implode(" OR ", $implode) . ")";
            }
        } else {
            $sql .= " WHERE (order_status_id = '3' OR  order_status_id = '5') ";
        }

        if (!empty($data['filter_category_id'])) {
            $implodes_filter = array();

            $filter_category_ids = explode(',', $data['filter_category_id']);
            // print_r( $filter_product_ids); die;
            foreach ($filter_category_ids as $filter_category_id) {
                $implodes_filter[] = "oct.category_id = '" . (int) $filter_category_id . "'";
            }

            if ($implodes_filter) {
                $sql .= " AND (" . implode(" OR ", $implodes_filter) . ")";
            }
        }


        if (!empty($data['filter_product_id'])) {
            $implodes = array();

            $filter_product_ids = explode(',', $data['filter_product_id']);

            foreach ($filter_product_ids as $filter_product_id) {
                $implodes[] = "op.product_id = '" . (int) $filter_product_id . "'";
            }

            if ($implodes) {
                $sql .= " and (" . implode(" OR ", $implodes) . ")";
            }
        }

        if (!empty($data['filter_date_start'])) {
            $sql .= " AND DATE(o.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
        }

        if (!empty($data['filter_date_end'])) {
            $sql .= " AND DATE(o.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
        }

//        echo "<pre>";
//        print_r($sql);
//        die;
        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function productlising() {
        $sql = "SELECT p.product_id, pd.name FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "' ORDER BY pd.name ASC";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function categorylising() {
        $sql = "SELECT distinct(cd.name), cd.category_id FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) WHERE cd.language_id = '" . (int) $this->config->get('config_language_id') . "' ORDER BY cd.category_id ASC";
        $query = $this->db->query($sql);
        //echo "<pre>"; print_r($query); die;
        return $query->rows;
    }

}
