<?php
class ModelReportIfs extends Model {
	public function getIfs($data = array()) { 

		 
		$sql = "SELECT CONCAT(a2.firstname , ' ', a2.lastname) AS parentaffname, a2.code parentaffcode, a2.company parentaffcompany, o.tracking, o.order_id, o.date_added OrderDate, o.customer_id CustId, CONCAT(o.firstname , ' ', o.lastname ) AS cust_name, os.name, CONCAT(a1.firstname , ' ',  a1.lastname) AS affname, a1.code affcode, a1.company affcompany, sum(op.total) as total from `" . DB_PREFIX . "order` o LEFT join  `" . DB_PREFIX . "order_status` os on (o.order_status_id = os.order_status_id AND os.language_id = 1) LEFT JOIN  `" . DB_PREFIX . "affiliate` a1 ON (a1.affiliate_id = o.affiliate_id) LEFT JOIN  `" . DB_PREFIX . "order_product` op ON (o.order_id = op.order_id) LEFT join `" . DB_PREFIX . "mta_affiliate` mtap ON (mtap.affiliate_id=a1.affiliate_id) LEFT join `" . DB_PREFIX . "affiliate` a2 ON (mtap.parent_affiliate_id = a2.affiliate_id)  ";


		$implode = array();

		if (!empty($data['filter_date_start'])) {
			$implode[] = "DATE(o.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$implode[] = "DATE(o.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

		if (!empty($data['filter_order_id'])) {
			$implode[] = " o.order_id = '" . (int)$data['filter_order_id'] . "'";
		}

		if (!empty($data['filter_customer'])) {
			$implode[] = " CONCAT(o.firstname, ' ', o.lastname) LIKE '%" . $this->db->escape($data['filter_customer']) . "%'";
		}

		if (!empty($data['filter_affiliate'])) {
			$implode[] = " CONCAT(a1.firstname , ' ',  a1.lastname) LIKE '%" . $this->db->escape($data['filter_affiliate']) . "%'";
		}

		if (!empty($data['filter_tracking_code'])) {
			$implode[] = " (o.tracking) LIKE '%" . $this->db->escape($data['filter_tracking_code']) . "%'";
		}

		if (!empty($data['filter_parent_affiliate'])) {
			$implode[] = " CONCAT(a2.firstname , ' ', a2.lastname) LIKE '%" . $this->db->escape($data['filter_parent_affiliate']) . "%'";
		}

		if ($implode) {
			$sql .= " WHERE " . implode(" AND ", $implode);
		}

		$sql .= " GROUP BY o.order_id ORDER BY o.order_id DESC";

		 

		if (isset($data['start']) || isset($data['limit'])) {
			if($data['filter_export_excel'] != 1 ) { 
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}


				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}
		}

	  
		$query = $this->db->query($sql);

		return $query->rows;
	}


	public function getTotalIfs($data = array()) {

		$sql = "SELECT COUNT(DISTINCT o.order_id) AS total from `" . DB_PREFIX . "order` o LEFT join  `" . DB_PREFIX . "order_status` os on (o.order_status_id = os.order_status_id AND os.language_id = 1) LEFT JOIN  `" . DB_PREFIX . "affiliate` a1 ON (a1.affiliate_id = o.affiliate_id) LEFT JOIN  `" . DB_PREFIX . "order_product` op ON (o.order_id = op.order_id) LEFT join `" . DB_PREFIX . "mta_affiliate` mtap ON (mtap.affiliate_id=a1.affiliate_id) LEFT join `" . DB_PREFIX . "affiliate` a2 ON (mtap.parent_affiliate_id = a2.affiliate_id)  ";

		$implode = array();

		if (!empty($data['filter_date_start'])) {
			$implode[] = "DATE(o.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$implode[] = "DATE(o.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

		if (!empty($data['filter_order_id'])) {
			$implode[] = " o.order_id = '" . (int)$data['filter_order_id'] . "'";
		}

		if (!empty($data['filter_customer'])) {
			$implode[] = " CONCAT(o.firstname, ' ', o.lastname) LIKE '%" . $this->db->escape($data['filter_customer']) . "%'";
		}

		if (!empty($data['filter_affiliate'])) {
			$implode[] = " CONCAT(a1.firstname , ' ',  a1.lastname) LIKE '%" . $this->db->escape($data['filter_affiliate']) . "%'";
		}

		if (!empty($data['filter_tracking_code'])) {
			$implode[] = " (o.tracking) LIKE '%" . $this->db->escape($data['filter_tracking_code']) . "%'";
		}

		if (!empty($data['filter_parent_affiliate'])) {
			$implode[] = " CONCAT(a2.firstname , ' ', a2.lastname) LIKE '%" . $this->db->escape($data['filter_parent_affiliate']) . "%'";
		}


		if ($implode) {
			$sql .= " WHERE " . implode(" AND ", $implode);
		}

		$sql .= " ORDER BY o.order_id DESC";

		 
		$query = $this->db->query($sql);

		return $query->row['total'];  
	}
}