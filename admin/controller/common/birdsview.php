<?php
#Controller to handle logic of dashboard .
class ControllerCommonBirdsview extends Controller {

    public function index() {

        #Loading Langauge file to get text for dashboard view
        $this->language->load('common/birdsview');

        #Getting heading title text
        $this->data['birdsview'] = $this->language->get('birdsview');
        $this->data['graphchart'] = $this->language->get('graphchart');

        #Getting row title text which is popup text too.
        $this->data['text_total_order'] = $this->language->get('text_total_order');
        $this->data['text_total_discount'] = $this->language->get('text_total_discount');
        $this->data['text_total_product'] = $this->language->get('text_total_product');
        $this->data['text_total_revenue'] = $this->language->get('text_total_revenue');
        $this->data['text_total_payment'] = $this->language->get('text_total_payment');
        $this->data['text_total_miss_order'] = $this->language->get('text_total_miss_order');

        #Getting Coloumn title text
        $this->data['today'] = $this->language->get('today');
        $this->data['yesterday'] = $this->language->get('yesterday');
        $this->data['last_week'] = $this->language->get('last_week');
        $this->data['last_month'] = $this->language->get('last_month');
        $this->data['last_year'] = $this->language->get('last_year');

        #Other popup text
        $this->data['thismonth'] = $this->language->get('thismonth');
        $this->data['otherday'] = $this->language->get('otherday');
        $this->data['thisweek'] = $this->language->get('thisweek');
        $this->data['thisyear'] = $this->language->get('thisyear');

        #Loading model to run database queries
        $this->load->model('birdsview/total');

        #Storing app status in variable
        $this->data['birdsview_table_status'] = $this->config->get('birdsview_table_status');
        $this->data['birdsview_chart_status'] = $this->config->get('birdsview_chart_status');

        #Storing Coloumn status in variables
        $this->data['today_status'] = $this->config->get('today');
        $this->data['yesterday_status'] = $this->config->get('yesterday');
        $this->data['lastweek_status'] = $this->config->get('lastweek');
        $this->data['lastmonth_status'] = $this->config->get('lastmonth');
        $this->data['lastyear_status'] = $this->config->get('lastyear');

        #Storing Row status in variables
        $this->data['order_status'] = $this->config->get('order');
        $this->data['product_status'] = $this->config->get('product');
        $this->data['discount_status'] = $this->config->get('discount');
        $this->data['revenue_status'] = $this->config->get('revenue');
        $this->data['missorder_status'] = $this->config->get('missorder');


        //Calling order calculation function in model
        $this->data['torder'] = $this->model_birdsview_total->tord();
        $this->data['yorder'] = $this->model_birdsview_total->yord();
        $this->data['worder'] = $this->model_birdsview_total->word();
        $this->data['morder'] = $this->model_birdsview_total->mord();
        $this->data['yyorder'] = $this->model_birdsview_total->yyord();

        //Calling product calculation function in model
        $this->data['tproduct'] = $this->model_birdsview_total->tpro();
        $this->data['yproduct'] = $this->model_birdsview_total->ypro();
        $this->data['wproduct'] = $this->model_birdsview_total->wpro();
        $this->data['mproduct'] = $this->model_birdsview_total->mpro();
        $this->data['yyproduct'] = $this->model_birdsview_total->yypro();


        //Calling missing order calculation function in model
        $this->data['tmissorder'] = $this->model_birdsview_total->tmiss();
        $this->data['ymissorder'] = $this->model_birdsview_total->ymiss();
        $this->data['wmissorder'] = $this->model_birdsview_total->wmiss();
        $this->data['mmissorder'] = $this->model_birdsview_total->mmiss();
        $this->data['yymissorder'] = $this->model_birdsview_total->yymiss();


        //Calling revenue calculation function in model
        $this->data['trevenue'] = $this->model_birdsview_total->trev();
        $this->data['yrevenue'] = $this->model_birdsview_total->yrev();
        $this->data['wrevenue'] = $this->model_birdsview_total->wrev();
        $this->data['mrevenue'] = $this->model_birdsview_total->mrev();
        $this->data['yyrevenue'] = $this->model_birdsview_total->yyrev();

        //Calling discount calculation function in model
        $this->data['tdiscount'] = $this->model_birdsview_total->tdis();
        $this->data['ydiscount'] = $this->model_birdsview_total->ydis();
        $this->data['wdiscount'] = $this->model_birdsview_total->wdis();
        $this->data['mdiscount'] = $this->model_birdsview_total->mdis();
        $this->data['yydiscount'] = $this->model_birdsview_total->yydis();


        if(isset($_GET['midnight'])) {
            $this->load->model('birdsview/midnight');
            $this->model_birdsview_midnight->midnight();
        }

        #Calling template file to provide view
        $this->template = 'common/birdsview.tpl';

        #Setting output of view
        $this->response->setOutput($this->render());

    }# End of index function

} #End of controller

?>
