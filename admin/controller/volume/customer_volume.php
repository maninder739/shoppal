<?php
class ControllerVolumeCustomerVolume extends Controller {
    public function index() {

        $this->load->model('marketing/affiliate');
        $affiliate_type = $this->model_marketing_affiliate->affiliateType($this->request->get['affiliate_id']);

        $data['text_welcome'] = $this->language->get('text_welcome');
        $data['text_logout']  = $this->language->get('text_logout');
        $this->load->language('marketing/customer_volume');
        $data['show_empty_text']   = $this->language->get('show_empty_text');
        $data['view_all']          = $this->language->get('view_all');
        $data['current_month']     = $this->language->get('current_month');
        $data['previous_month']    = $this->language->get('previous_month');
        $data['next_month']        = $this->language->get('next_month');
        $data['shopping_volume']   = $this->language->get('shopping_volume');
        $data['cust_order_date']   = $this->language->get('cust_order_date');
        $data['cust_name']         = $this->language->get('cust_name');
        $data['cust_retail']       = $this->language->get('cust_retail');
        $data['cust_percentage']   = $this->language->get('cust_percentage');
        $data['table_total']       = $this->language->get('table_total');
        $data['table_total_cust']  = $this->language->get('table_total_cust');
        $data['table_total_custs'] = $this->language->get('table_total_custs');
        $data['table_total_none']  = $this->language->get('table_total_none');
        $data['serialNumber']      = $this->language->get('serialNumber');
        $data['cus_firstname']     = $this->language->get('firstname');
        $data['cus_lastname']      = $this->language->get('lastname');
        $data['order_status']      = $this->language->get('order_status');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home'),
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_account'),
            'href' => $this->url->link('marketing/affiliate', '', true),
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('marketing/affiliate', 'token=' . $this->session->data['token'] . '&affiliate_id=' . $this->request->get['affiliate_id']  , true),
        );
        $data['firstname']      = $this->affiliate->getFirstName();
        $data['heading_title']  = $this->language->get('heading_title');
        $data['column_left']    = $this->load->controller('common/column_left');
        $data['column_right']   = $this->load->controller('common/column_right');
        $data['content_top']    = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer']         = $this->load->controller('common/footer');
        $data['header']         = $this->load->controller('common/header');

        $this->load->model('customer/customer');

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }


        $total_customers = $this->model_customer_customer->getTotalCustomers($this->request->get['affiliate_id']);

        $pagination        = new Pagination();
        $pagination->total = $total_customers;
        $pagination->page  = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url   = $this->url->link('volume/customer_volume', 'token=' . $this->session->data['token'] . '&affiliate_id=' . $this->request->get['affiliate_id'] . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($total_customers) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($total_customers - $this->config->get('config_limit_admin'))) ? $total_customers : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $total_customers, ceil($total_customers / $this->config->get('config_limit_admin')));

        $customer_list = $this->model_customer_customer->getAllCustomers($this->request->get['affiliate_id']);
         

        if (isset($_GET['filter']) && isset($_GET['current']) && $_GET['filter'] != 'all') {
            $str                    = explode("-", $_GET['current']);
            $current_m              = $str[0];
            $current_y              = $str[1];
            $customer_data['month'] = $current_m;
            $customer_data['year']  = $current_y;
        } elseif (isset($_GET['filter']) && $_GET['filter'] == 'all') {
            $current_m              = '';
            $current_y              = '';
            $customer_data['month'] = '';
            $customer_data['year']  = '';
        } else {
            $current_m              = date('m');
            $current_y              = date('Y');
            $customer_data['month'] = $current_m;
            $customer_data['year']  = $current_y;
        }

        $customer_array = array();
        foreach($customer_list['rows'] as $val){
            
            $customer_order = $this->model_customer_customer->getCustomerOrder($val['customer_id'],$current_m,$current_y);
            
            foreach($customer_order['rows'] as $row){
                 
                $getTotalRetails = $this->model_customer_customer->getTotalRetails($row['order_id']);        
                $customer_array['data'][] = array(
                    'customer_id'=>$val['customer_id'],
                    'firstname'=>$val['firstname'],
                    'lastname'=>$val['lastname'],
                    'name'=>isset($row['name']) ? $row['name'] : '' ,
                    'total'=> $getTotalRetails,
                    'commission'=>$row['commission'],
                    'date_added'=>$row['date_added']
                );
            }  
        }


        if ($customer_array['data']) {
            foreach ($customer_array['data'] as $key => $row) {
                $date[$key] = $row['date_added'];
            }
            array_multisort($date, SORT_DESC, $customer_array['data']);
        }
        $customer_data = array_merge($customer_data, $customer_array);
       

        $data['customer_list'] = $customer_data;

        $this->response->setOutput($this->load->view('marketing/customer_volume', $data));
    }

}
