<?php
#Controller to handle Admin panel setting.
class ControllerModuleBirdsview extends Controller {

    public function index() {
        #Loading birdsview language file to get text.
        $this->language->load('module/birdsview');

        #Loading model to run query to store app, coloumn and row status value.
        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('birdsview', $this->request->post);
            $this->session->data['success'] = $this->language->get('text_success');
            $this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
        }


        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
                'text' => $this->language->get('birdsview_text_home'),
                'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
                'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
                'text' => $this->language->get('birdsview_text_module'),
                'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
                'separator' => ' :: '
        );

        $this->data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('module/birdsview', 'token=' . $this->session->data['token'], 'SSL'),
                'separator' => ' :: '
        );



        #Setting title of Admin setting page in browser.
        $this->document->setTitle($this->language->get('heading_title'));

        #Getting Heading text in variable
        $this->data['heading_title'] = $this->language->get('heading_title');

        #Getting text for all setting other thana app status
        $this->data['ttoday'] = $this->language->get('today');
        $this->data['tyesterday'] = $this->language->get('yesterday');
        $this->data['tlastweek'] = $this->language->get('lastweek');
        $this->data['tlastmonth'] = $this->language->get('lastmonth');
        $this->data['tlastyear'] = $this->language->get('lastyear');
        $this->data['torder'] = $this->language->get('order');
        $this->data['tproduct'] = $this->language->get('product');
        $this->data['tdiscount'] = $this->language->get('discount');
        $this->data['trevenue'] = $this->language->get('revenue');
        $this->data['tmissorder'] = $this->language->get('missorder');

        #Dropdown Text
        $this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled');

        #Getting app status text in variable
        $this->data['text_birdsview_table_status'] = $this->language->get('birdsview_table_status');
        $this->data['text_birdsview_chart_status'] = $this->language->get('birdsview_chart_status');

        #Getting Save and cancel button text
        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_cancel'] = $this->language->get('button_cancel');

        #Getitng url link for save and cancel button
        $this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['action'] = $this->url->link('module/birdsview', 'token=' . $this->session->data['token'], 'SSL');



        #Posting and getting App status value via database
        if (isset($this->request->post['birdsview_table_status'])) {
            $this->data['birdsview_table_status'] = $this->request->post['birdsview_table_status'];
        } else {
            $this->data['birdsview_table_status'] = $this->config->get('birdsview_table_status');
        }

        if (isset($this->request->post['birdsview_chart_status'])) {
            $this->data['birdsview_chart_status'] = $this->request->post['birdsview_chart_status'];
        } else {
            $this->data['birdsview_chart_status'] = $this->config->get('birdsview_chart_status');
        }




        #Posting and getting values via database.
        if (isset($this->request->post['order'])) {
            $this->data['order'] = $this->request->post['order'];
        } else {
            $this->data['order'] = $this->config->get('order');
        }


        if (isset($this->request->post['product'])) {
            $this->data['product'] = $this->request->post['product'];
        } else {
            $this->data['product'] = $this->config->get('product');
        }


        if (isset($this->request->post['discount'])) {
            $this->data['discount'] = $this->request->post['discount'];
        } else {
            $this->data['discount'] = $this->config->get('discount');
        }


        if (isset($this->request->post['revenue'])) {
            $this->data['revenue'] = $this->request->post['revenue'];
        } else {
            $this->data['revenue'] = $this->config->get('revenue');
        }


        if (isset($this->request->post['missorder'])) {
            $this->data['missorder'] = $this->request->post['missorder'];
        } else {
            $this->data['missorder'] = $this->config->get('missorder');
        }


        if (isset($this->request->post['today'])) {
            $this->data['today'] = $this->request->post['today'];
        } else {
            $this->data['today'] = $this->config->get('today');
        }


        if (isset($this->request->post['yesterday'])) {
            $this->data['yesterday'] = $this->request->post['yesterday'];
        } else {
            $this->data['yesterday'] = $this->config->get('yesterday');
        }


        if (isset($this->request->post['lastweek'])) {
            $this->data['lastweek'] = $this->request->post['lastweek'];
        } else {
            $this->data['lastweek'] = $this->config->get('lastweek');
        }


        if (isset($this->request->post['lastmonth'])) {
            $this->data['lastmonth'] = $this->request->post['lastmonth'];
        } else {
            $this->data['lastmonth'] = $this->config->get('lastmonth');
        }


        if (isset($this->request->post['lastyear'])) {
            $this->data['lastyear'] = $this->request->post['lastyear'];
        } else {
            $this->data['lastyear'] = $this->config->get('lastyear');
        }



        $this->children = array(
                'common/header',
                'common/footer'
        );

        $this->template = 'module/birdsview.tpl';
        $this->response->setOutput($this->render());
    } # End of index funciton


    protected function validate() {
        if (!$this->user->hasPermission('modify', 'module/birdsview')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    } #End of validate function
	
	public function install() {
		$this->load->model('birdsview/total');
        $this->model_birdsview_total->install();
	}

}#End of controller

?>
