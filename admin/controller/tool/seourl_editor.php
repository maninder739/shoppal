<?php
class ControllerToolSeourlEditor extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('tool/seourl_editor');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('tool/seourl_editor');

		$this->getList();
	}

	public function add() {
		$this->load->language('tool/seourl_editor');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('tool/seourl_editor');

		$ssl = version_compare(VERSION, '2.2', '>=') ? true : 'SSL';

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_tool_seourl_editor->addSeourl($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

            if (isset($this->request->post['tab'])) {
                $tab = $this->request->post['tab'];
            } else if (isset($this->request->get['tab'])) {
                $tab = $this->request->get['tab'];
            } else {
                $tab = 'store';
            }

			$url = '';

            if (isset($this->request->get['filter_query'])) {
                $url .= '&filter_query=' . urlencode(html_entity_decode($this->request->get['filter_query'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_keyword'])) {
                $url .= '&filter_keyword=' . urlencode(html_entity_decode($this->request->get['filter_keyword'], ENT_QUOTES, 'UTF-8'));
            }

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

            if (isset($tab)) {
                $url .= '&tab=' . urlencode(html_entity_decode($tab, ENT_QUOTES, 'UTF-8'));
            }

			$this->response->redirect($this->url->link('tool/seourl_editor', 'token=' . $this->session->data['token'] . $url, $ssl));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('tool/seourl_editor');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('tool/seourl_editor');

		$ssl = version_compare(VERSION, '2.2', '>=') ? true : 'SSL';

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_tool_seourl_editor->editSeourl($this->request->get['url_alias_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

            if (isset($this->request->post['tab'])) {
                $tab = $this->request->post['tab'];
            } else if (isset($this->request->get['tab'])) {
                $tab = $this->request->get['tab'];
            } else {
                $tab = 'store';
            }

			$url = '';

            if (isset($this->request->get['filter_query'])) {
                $url .= '&filter_query=' . urlencode(html_entity_decode($this->request->get['filter_query'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_keyword'])) {
                $url .= '&filter_keyword=' . urlencode(html_entity_decode($this->request->get['filter_keyword'], ENT_QUOTES, 'UTF-8'));
            }

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

            if (isset($tab)) {
                $url .= '&tab=' . urlencode(html_entity_decode($tab, ENT_QUOTES, 'UTF-8'));
            }

			$this->response->redirect($this->url->link('tool/seourl_editor', 'token=' . $this->session->data['token'] . $url, $ssl));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('tool/seourl_editor');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('tool/seourl_editor');

		$ssl = version_compare(VERSION, '2.2', '>=') ? true : 'SSL';

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $url_alias_id) {
				$this->model_tool_seourl_editor->deleteSeourl($url_alias_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

            if (isset($this->request->post['tab'])) {
                $tab = $this->request->post['tab'];
            } else if (isset($this->request->get['tab'])) {
                $tab = $this->request->get['tab'];
            } else {
                $tab = 'store';
            }

			$url = '';

			if (isset($this->request->get['filter_query'])) {
				$url .= '&filter_query=' . urlencode(html_entity_decode($this->request->get['filter_query'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_keyword'])) {
				$url .= '&filter_keyword=' . urlencode(html_entity_decode($this->request->get['filter_keyword'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

            if (isset($tab)) {
                $url .= '&tab=' . urlencode(html_entity_decode($tab, ENT_QUOTES, 'UTF-8'));
            }

			$this->response->redirect($this->url->link('tool/seourl_editor', 'token=' . $this->session->data['token'] . $url, $ssl));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_query'])) {
			$filter_query = $this->request->get['filter_query'];
		} else {
            $filter_query = null;
		}

		if (isset($this->request->get['filter_keyword'])) {
			$filter_keyword = $this->request->get['filter_keyword'];
		} else {
            $filter_keyword = null;
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'ua.query';
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

        if (isset($this->request->post['tab'])) {
            $tab = $this->request->post['tab'];
        } else if (isset($this->request->get['tab'])) {
            $tab = $this->request->get['tab'];
        } else {
            $tab = 'store';
        }

		$ssl = version_compare(VERSION, '2.2', '>=') ? true : 'SSL';

		$url = '';

		if (isset($this->request->get['filter_query'])) {
			$url .= '&filter_query=' . urlencode(html_entity_decode($this->request->get['filter_query'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_keyword'])) {
			$url .= '&filter_keyword=' . urlencode(html_entity_decode($this->request->get['filter_keyword'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

        if (isset($tab)) {
            $url .= '&tab=' . urlencode(html_entity_decode($tab, ENT_QUOTES, 'UTF-8'));
        }

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], $ssl)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('tool/seourl_editor', 'token=' . $this->session->data['token'] . $url, $ssl)
		);

		$data['add'] = $this->url->link('tool/seourl_editor/add', 'token=' . $this->session->data['token'] . $url, $ssl);
		$data['delete'] = $this->url->link('tool/seourl_editor/delete', 'token=' . $this->session->data['token'] . $url, $ssl);

		$data['seourls'] = array();

		$filter_data = array(
			'filter_query'      => $filter_query,
			'filter_keyword'    => $filter_keyword,
            'tab'               => $tab,
			'sort'              => $sort,
			'order'             => $order,
			'start'             => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'             => $this->config->get('config_limit_admin')
		);

		$seourls_total = $this->model_tool_seourl_editor->getTotalSeourls($filter_data);

		$results = $this->model_tool_seourl_editor->getSeourls($filter_data);

		foreach ($results as $result) {
			$data['seourls'][] = array(
                'url_alias_id'  => $result['url_alias_id'],
				'query'         => $result['query'],
				'keyword'       => $result['keyword'],
				'edit'          => $this->url->link('tool/seourl_editor/edit', 'token=' . $this->session->data['token'] . '&url_alias_id=' . $result['url_alias_id'] . $url, $ssl),
                'edit_out'      => (!isset($tab) || $tab == 'store') ? '' : $this->url->link('catalog/'.$tab.'/edit', 'token=' . $this->session->data['token'] . '&' . $result['query'] . $url, $ssl)
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

        $data['tab_store'] = $this->language->get('tab_store');
        $data['tab_product'] = $this->language->get('tab_product');
        $data['tab_category'] = $this->language->get('tab_category');
        $data['tab_information'] = $this->language->get('tab_information');
        $data['tab_manufacturer'] = $this->language->get('tab_manufacturer');

		$data['column_query'] = $this->language->get('column_query');
		$data['column_keyword'] = $this->language->get('column_keyword');
		$data['column_action'] = $this->language->get('column_action');

		$data['entry_query'] = $this->language->get('entry_query');
		$data['entry_keyword'] = $this->language->get('entry_keyword');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_filter'] = $this->language->get('button_filter');
        $data['button_edit_out'] = $this->language->get('button_edit_out');

		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_query'] = $this->url->link('tool/seourl_editor', 'token=' . $this->session->data['token'] . '&sort=ua.query' . $url, $ssl);
		$data['sort_keyword'] = $this->url->link('tool/seourl_editor', 'token=' . $this->session->data['token'] . '&sort=ua.keyword' . $url, $ssl);

		$url = '';

		if (isset($this->request->get['filter_query'])) {
			$url .= '&filter_query=' . urlencode(html_entity_decode($this->request->get['filter_query'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_keyword'])) {
			$url .= '&filter_keyword=' . urlencode(html_entity_decode($this->request->get['filter_keyword'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

        if (isset($tab)) {
            $url .= '&tab=' . urlencode(html_entity_decode($tab, ENT_QUOTES, 'UTF-8'));
        } else {
            $url .= '&tab=store';
        }

		$pagination = new Pagination();
		$pagination->total = $seourls_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('tool/seourl_editor', 'token=' . $this->session->data['token'] . $url . '&page={page}', $ssl);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($seourls_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($seourls_total - $this->config->get('config_limit_admin'))) ? $seourls_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $seourls_total, ceil($seourls_total / $this->config->get('config_limit_admin')));

		$data['filter_query'] = $filter_query;
		$data['filter_keyword'] = $filter_keyword;

		$data['sort'] = $sort;
		$data['order'] = $order;
        $data['tab'] = $tab;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('tool/seourl_editor_list.tpl', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['url_alias_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_query'] = $this->language->get('entry_query');
		$data['entry_keyword'] = $this->language->get('entry_keyword');

		$data['help_query'] = $this->language->get('help_query');
        $data['help_keyword'] = $this->language->get('help_keyword');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['query'])) {
			$data['error_query'] = $this->error['query'];
		} else {
			$data['error_query'] = '';
		}

		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}

        if (isset($this->request->post['tab'])) {
            $tab = $this->request->post['tab'];
        } else if (isset($this->request->get['tab'])) {
            $tab = $this->request->get['tab'];
        } else {
            $tab = 'store';
        }

		$ssl = version_compare(VERSION, '2.2', '>=') ? true : 'SSL';

		$url = '';

		if (isset($this->request->get['filter_query'])) {
			$url .= '&filter_query=' . urlencode(html_entity_decode($this->request->get['filter_query'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_keyword'])) {
			$url .= '&filter_keyword=' . urlencode(html_entity_decode($this->request->get['filter_keyword'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

        if (isset($tab)) {
            $url .= '&tab=' . urlencode(html_entity_decode($tab, ENT_QUOTES, 'UTF-8'));;
        }

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], $ssl)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('tool/seourl_editor', 'token=' . $this->session->data['token'] . $url, $ssl)
		);

		if (!isset($this->request->get['url_alias_id'])) {
			$data['action'] = $this->url->link('tool/seourl_editor/add', 'token=' . $this->session->data['token'] . $url, $ssl);
		} else {
			$data['action'] = $this->url->link('tool/seourl_editor/edit', 'token=' . $this->session->data['token'] . '&url_alias_id=' . $this->request->get['url_alias_id'] . $url, $ssl);
		}

		$data['cancel'] = $this->url->link('tool/seourl_editor', 'token=' . $this->session->data['token'] . $url, $ssl);

		if (isset($this->request->get['url_alias_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$seourl_info = $this->model_tool_seourl_editor->getSeourl($this->request->get['url_alias_id']);
		}

		$data['token'] = $this->session->data['token'];

        if (isset($this->request->post['url_alias_id'])) {
			$data['url_alias_id'] = $this->request->post['url_alias_id'];
		} elseif (!empty($seourl_info)) {
			$data['url_alias_id'] = $seourl_info['url_alias_id'];
		} else {
			$data['url_alias_id'] = '';
		}

		if (isset($this->request->post['query'])) {
			$data['query'] = $this->request->post['query'];
		} elseif (!empty($seourl_info)) {
			$data['query'] = $seourl_info['query'];
		} else {
			$data['query'] = '';
		}

		if (isset($this->request->post['keyword'])) {
			$data['keyword'] = $this->request->post['keyword'];
		} elseif (!empty($seourl_info)) {
			$data['keyword'] = $seourl_info['keyword'];
		} else {
			$data['keyword'] = '';
		}

        $data['tab'] = $tab;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('tool/seourl_editor_form.tpl', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'tool/seourl_editor')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['query']) < 3) || (utf8_strlen($this->request->post['query']) > 255)) {
			$this->error['query'] = $this->language->get('error_query');
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'tool/seourl_editor')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}