<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ControllerReportExpiring extends Controller {

    public function index() {

        $this->load->language('report/expiring');

        $this->document->setTitle($this->language->get('heading_title'));

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';


        $data['export'] = $this->url->link('report/expiring/exportexcel', 'token=' . $this->session->data['token'], true);
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('report/expiring', 'token=' . $this->session->data['token'] . $url, true)
        );

        $this->load->model('report/expire');

        $data['expirings'] = array();

        $filter_data = array(
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $expiring_total = $this->model_report_expire->getTotalExpiring($filter_data);

        $results = $this->model_report_expire->getExpiring($filter_data);

        foreach ($results as $result) {

            $data['expirings'][] = array(
                'customer_id' => $result['customer_id'],
                'firstname' => $result['firstname'],
                'lastname' => $result['lastname'],
                'email' => $result['email'],
                'telephone' => $result['telephone'],
                'payment_city' => $result['payment_city'],
                'payment_zone' => $result['payment_zone'],
                'payment_postcode' => $result['payment_postcode'],
                'expmonth' => $result['expmonth'],
                'expyear' => $result['expyear'],
            );
        }

        $data['heading_title'] = $this->language->get('heading_title');
        $data['export_excel'] = $this->language->get('export_excel');
        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');

        $data['column_customer_id'] = $this->language->get('column_customer_id');
        $data['column_firstname'] = $this->language->get('column_firstname');
        $data['column_lastname'] = $this->language->get('column_lastname');
        $data['column_email'] = $this->language->get('column_email');
        $data['column_telephone'] = $this->language->get('column_telephone');
        $data['column_payment_city'] = $this->language->get('column_payment_city');
        $data['column_payment_zone'] = $this->language->get('column_payment_zone');
        $data['column_payment_postcode'] = $this->language->get('column_payment_postcode');
        $data['column_expmonth'] = $this->language->get('column_expmonth');
        $data['column_expyear'] = $this->language->get('column_expyear');
        $data['token'] = $this->session->data['token'];

        $url = '';
        $pagination = new Pagination();
        $pagination->total = $expiring_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('report/expiring', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

        $data['pagination'] = $pagination->render();
        $data['results'] = sprintf($this->language->get('text_pagination'), ($expiring_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($expiring_total - $this->config->get('config_limit_admin'))) ? $expiring_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $expiring_total, ceil($expiring_total / $this->config->get('config_limit_admin')));

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('report/expiring', $data));
    }

    public function exportexcel() {

        $this->load->language('report/expiring');

        $column_customer_id = $this->language->get('column_customer_id');
        $column_firstname = $this->language->get('column_firstname');
        $column_lastname = $this->language->get('column_lastname');
        $column_email = $this->language->get('column_email');
        $column_telephone = $this->language->get('column_telephone');
        $column_payment_city = $this->language->get('column_payment_city');
        $column_payment_zone = $this->language->get('column_payment_zone');
        $column_payment_postcode = $this->language->get('column_payment_postcode');
        $column_expmonth = $this->language->get('column_expmonth');
        $column_expyear = $this->language->get('column_expyear');

        $this->load->model('report/expire');
        $results = $this->model_report_expire->getExpiringReportExport();

        if (!empty($results)) {
            $columnHeader = '';
            $columnHeader = $column_customer_id . "\t" . $column_firstname . "\t" . $column_lastname . "\t" . $column_email . "\t" . $column_telephone . "\t" . $column_payment_city . "\t" . $column_payment_zone . "\t" . $column_payment_postcode . "\t" . $column_expmonth . "\t" . $column_expyear;
            $setData = '';
            $rowData = '';
            foreach ($results as $value) {
                $rowData .= '"' . $value['customer_id'] . '"' . "\t";
                $rowData .= '"' . $value['firstname'] . '"' . "\t";
                $rowData .= '"' . $value['lastname'] . '"' . "\t";
                $rowData .= '"' . $value['email'] . '"' . "\t";
                $rowData .= '"' . $value['telephone'] . '"' . "\t";
                $rowData .= '"' . $value['payment_city'] . '"' . "\t";
                $rowData .= '"' . $value['payment_zone'] . '"' . "\t";
                $rowData .= '"' . $value['payment_postcode'] . '"' . "\t";
                $rowData .= '"' . $value['expmonth'] . '"' . "\t";
                $rowData .= '"' . $value['expyear'] . '"' . "\n";
            }

            $setData .= trim($rowData) . "\n";

            $file = 'CC_Expiring_Report.xls';

            header("Content-type: application/octet-stream");
            header("Content-Disposition: attachment; filename=" . basename($file));
            header("Pragma: no-cache");
            header("Expires: 0");
            readfile($file);
            echo ucwords($columnHeader) . "\n" . $setData . "\n";
        }
    }

    function mail_attachment() {


        $this->load->language('report/expiring');
        $column_customer_id = $this->language->get('column_customer_id');
        $column_firstname = $this->language->get('column_firstname');
        $column_lastname = $this->language->get('column_lastname');
        $column_email = $this->language->get('column_email');
        $column_telephone = $this->language->get('column_telephone');
        $column_payment_city = $this->language->get('column_payment_city');
        $column_payment_zone = $this->language->get('column_payment_zone');
        $column_payment_postcode = $this->language->get('column_payment_postcode');
        $column_expmonth = $this->language->get('column_expmonth');
        $column_expyear = $this->language->get('column_expyear');

        $this->load->model('report/expire');
        $results = $this->model_report_expire->getExpiringReportExport();

        //if (!empty($results)) {
        $columnHeader = '';
        $columnHeader = $column_customer_id . "\t" . $column_firstname . "\t" . $column_lastname . "\t" . $column_email . "\t" . $column_telephone . "\t" . $column_payment_city . "\t" . $column_payment_zone . "\t" . $column_payment_postcode . "\t" . $column_expmonth . "\t" . $column_expyear;
        $setData = '';
        $rowData = '';
        foreach ($results as $value) {
            $rowData .= '"' . $value['customer_id'] . '"' . "\t";
            $rowData .= '"' . $value['firstname'] . '"' . "\t";
            $rowData .= '"' . $value['lastname'] . '"' . "\t";
            $rowData .= '"' . $value['email'] . '"' . "\t";
            $rowData .= '"' . $value['telephone'] . '"' . "\t";
            $rowData .= '"' . $value['payment_city'] . '"' . "\t";
            $rowData .= '"' . $value['payment_zone'] . '"' . "\t";
            $rowData .= '"' . $value['payment_postcode'] . '"' . "\t";
            $rowData .= '"' . $value['expmonth'] . '"' . "\t";
            $rowData .= '"' . $value['expyear'] . '"' . "\n";
        }

        $setData .= trim($rowData) . "\n";


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="CC_Expiring_Report.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=" . basename($file));
        header("Pragma: no-cache");
        header("Expires: 0");
        readfile($file);
        echo ucwords($columnHeader) . "\n" . $setData . "\n";


        $filename = 'index.php';
        $file = '/home/nascenture/Downloads/index.php';
        $from_mail = 'maninder@nascenture.com';
        $replyto = 'maninder@nascenture.com';
        $from_name = 'maninder';
        $subject = 'tset';
        $file_size = filesize($file);
        $handle = fopen($file, "r");
        $content = fread($handle, $file_size);
        fclose($handle);
        $content = chunk_split(base64_encode($content));
        $uid = md5(uniqid(time()));
        $header = "From: " . $from_name . " <" . $from_mail . ">\r\n";
        $header .= "Reply-To: " . $replyto . "\r\n";
        $header .= "MIME-Version: 1.0\r\n";
        $header .= "Content-Type: multipart/mixed; boundary=\"" . $uid . "\"\r\n\r\n";
        $header .= "This is a multi-part message in MIME format.\r\n";
        $header .= "--" . $uid . "\r\n";
        $header .= "Content-type:text/plain; charset=iso-8859-1\r\n";
        $header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
        $header .= $message . "\r\n\r\n";
        $header .= "--" . $uid . "\r\n";
        $header .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"\r\n"; // use different content types here
        $header .= "Content-Transfer-Encoding: base64\r\n";
        $header .= "Content-Disposition: attachment; filename=\"" . $filename . "\"\r\n\r\n";
        $header .= $content . "\r\n\r\n";
        $header .= "--" . $uid . "--";
        if (mail($mailto, $subject, "", $header)) {
            echo "mail send ... OK"; // or use booleans here
        } else {
            echo "mail send ... ERROR!";
        }
        //}
    }

    function send_csv_mail($to = 'maninder@nascenture.com', $subject = 'Test email with attachment', $from = 'maninder@nascenture.com') {
        $csvData = array(array(1, 2, 3, 4, 5, 6, 7), array(1, 2, 3, 4, 5, 6, 7), array(1, 2, 3, 4, 5, 6, 7));
        $body = "Hello World!!!\r\n This is simple text email message.";

        // This will provide plenty adequate entropy
        $multipartSep = '-----' . md5(time()) . '-----';

        // Arrays are much more readable
        // Make the attachment
        $filename = chunk_split(base64_encode(create_csv_string($csvData)));

        // Make the body of the message
        $header = "From: " . $from_name . " <" . $from_mail . ">\r\n";
        $header .= "Reply-To: " . $replyto . "\r\n";
        $header .= "MIME-Version: 1.0\r\n";
        $header .= "Content-Type: multipart/mixed; boundary=\"" . $uid . "\"\r\n\r\n";
        $header .= "This is a multi-part message in MIME format.\r\n";
        $header .= "--" . $uid . "\r\n";
        $header .= "Content-type:text/plain; charset=iso-8859-1\r\n";
        $header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
        $header .= $message . "\r\n\r\n";
        $header .= "--" . $uid . "\r\n";
        $header .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"\r\n"; // use different content types here
        $header .= "Content-Transfer-Encoding: base64\r\n";
        $header .= "Content-Disposition: attachment; filename=\"" . $filename . "\"\r\n\r\n";
        $header .= $content . "\r\n\r\n";
        $header .= "--" . $uid . "--";

        // Send the email, return the result

        if (mail($to, $subject, "", $header)) {
            $msg = "mail send ... OK"; // or use booleans here
        } else {
            $msg = "mail send ... ERROR!";
        }
        die($msg);
        return $msg;
    }

}
