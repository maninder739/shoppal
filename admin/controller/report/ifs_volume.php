<?php
class ControllerReportIfsVolume extends Controller {
	public function index() {

		$this->load->language('report/ifs_volume');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = '';
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '';
		}

		if (isset($this->request->get['filter_customer'])) {
			$filter_customer = $this->request->get['filter_customer'];
		} else {
			$filter_customer = '';
		}

		if (isset($this->request->get['filter_tracking_code'])) {
			$filter_tracking_code = $this->request->get['filter_tracking_code'];
		} else {
			$filter_tracking_code = '';
		}


		if (isset($this->request->get['filter_affiliate'])) {
			$filter_affiliate = $this->request->get['filter_affiliate'];
		} else {
			$filter_affiliate = '';
		}


		if (isset($this->request->get['filter_parent_affiliate'])) {
			$filter_parent_affiliate = $this->request->get['filter_parent_affiliate'];
		} else {
			$filter_parent_affiliate = '';
		}

		if (isset($this->request->get['filter_parent_affiliate'])) {
			$filter_parent_affiliate = $this->request->get['filter_parent_affiliate'];
		} else {
			$filter_parent_affiliate = '';
		}

		if (isset($this->request->get['filter_export_excel'])) {
			$filter_export_excel = $this->request->get['filter_export_excel'];
		} else {
			$filter_export_excel = '';
		}


		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}


		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . $this->request->get['filter_customer'];
		}

		if (isset($this->request->get['filter_tracking_code'])) {
			$url .= '&filter_tracking_code=' . $this->request->get['filter_tracking_code'];
		}

		if (isset($this->request->get['filter_affiliate'])) {
			$url .= '&filter_affiliate=' . $this->request->get['filter_affiliate'];
		}

		if (isset($this->request->get['filter_parent_affiliate'])) {
			$url .= '&filter_parent_affiliate=' . $this->request->get['filter_parent_affiliate'];
		}



		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('report/affiliate', 'token=' . $this->session->data['token'] . $url, true)
		);

		$this->load->model('report/ifs');

		$data['affiliates'] = array();

		$filter_data = array(
			'filter_date_start'	=> $filter_date_start,
			'filter_date_end'	=> $filter_date_end,
			'filter_tracking_code'	=> $filter_tracking_code,
			'filter_customer'	=> $filter_customer,
			'filter_affiliate'	=> $filter_affiliate,
			'filter_parent_affiliate'	=> $filter_parent_affiliate,
			'filter_export_excel'	=> $filter_export_excel,
			'start'             => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'             => $this->config->get('config_limit_admin')
		);

		$ifs_total = $this->model_report_ifs->getTotalIfs($filter_data);

		$results = $this->model_report_ifs->getIfs($filter_data);
	 
		foreach ($results as $result) {

			$data['affiliates'][] = array(
				'parentaffname'  => $result['parentaffname'],
				'parentaffcode'  => $result['parentaffcode'],
				'parentaffcompany'  => $result['parentaffcompany'],
				'tracking'  => $result['tracking'],
				'order_id'  => $result['order_id'],
				'custId'  => $result['CustId'],
				'order_status'    => $result['name'],
				'cust_name'    => $result['cust_name'],
				'affname'       => $result['affname'],
				'affcode'    => $result['affcode'],
				'affcompany'    => $result['affcompany'],
				'date_added'    => date($this->language->get('date_format_short'), strtotime($result['OrderDate'])),
				'total'      => $this->currency->format($result['total'], $this->config->get('config_currency')),
			 
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');
		$data['column_status'] = $this->language->get('column_status');
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_parent_affiliate_name'] = $this->language->get('column_parent_affiliate_name');
		$data['column_parent_affiliate_code'] = $this->language->get('column_parent_affiliate_code');
		$data['column_parent_affiliate_company'] = $this->language->get('column_parent_affiliate_company');

		$data['column_tracking_code'] = $this->language->get('column_tracking_code');
		$data['column_order_id'] = $this->language->get('column_order_id');
		$data['column_order_date'] = $this->language->get('column_order_date');

		$data['column_customer_id'] = $this->language->get('column_customer_id');
		$data['column_customer_name'] = $this->language->get('column_customer_name');
		$data['column_status'] = $this->language->get('column_status');

		$data['column_affname'] = $this->language->get('column_affname');
		$data['column_affcode'] = $this->language->get('column_affcode');
		$data['column_affcompany'] = $this->language->get('column_affcompany');
 
		$data['column_total'] = $this->language->get('column_total');
		$data['column_action'] = $this->language->get('column_action');
	 

		$data['entry_date_start'] = $this->language->get('entry_date_start');
		$data['entry_date_end'] = $this->language->get('entry_date_end');
		$data['entry_tracking_code'] = $this->language->get('entry_tracking_code');
		$data['entry_customer'] = $this->language->get('entry_customer');
		$data['entry_current_affiliate'] = $this->language->get('entry_current_affiliate');
		$data['column_parent_affiliate'] = $this->language->get('column_parent_affiliate');

		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_filter'] = $this->language->get('button_filter');

		$data['token'] = $this->session->data['token'];

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_tracking_code'])) {
			$url .= '&filter_tracking_code=' . $this->request->get['filter_tracking_code'];
		}	

		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . $this->request->get['filter_customer'];
		}

		if (isset($this->request->get['filter_affiliate'])) {
			$url .= '&filter_affiliate=' . $this->request->get['filter_affiliate'];
		}

		if (isset($this->request->get['filter_parent_affiliate'])) {
			$url .= '&filter_parent_affiliate=' . $this->request->get['filter_parent_affiliate'];
		}

		

		$pagination = new Pagination();
		$pagination->total = $ifs_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('report/ifs_volume', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($ifs_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($ifs_total - $this->config->get('config_limit_admin'))) ? $ifs_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $ifs_total, ceil($ifs_total / $this->config->get('config_limit_admin')));

		$data['filter_date_start'] = $filter_date_start;
		$data['filter_date_end'] = $filter_date_end;
		$data['filter_customer'] = $filter_customer;
		$data['filter_tracking_code'] = $filter_tracking_code;
		$data['filter_affiliate'] = $filter_affiliate;
		$data['filter_parent_affiliate'] = $filter_parent_affiliate;


		if (isset($this->request->get['filter_export_excel']) ) {
		 //echo '<pre>'; print_r($results); die;
		$columnHeader = '';  
		$columnHeader =  $data['column_parent_affiliate_name'] . "\t" .$data['column_parent_affiliate_code'] . "\t" . $data['column_parent_affiliate_company']  . "\t" . $data['column_tracking_code'] . "\t" . $data['column_order_id'] . "\t" . $data['column_order_date'] . "\t" . $data['column_customer_id'] . "\t" . $data['column_customer_name'] . "\t" . $data['column_status'] . "\t" . $data['column_affname'] . "\t" . $data['column_affcode'] . "\t" . $data['column_affcompany'] . "\t" .   $data['column_total'];  

			$date = '';
			$total = '';
			$setData = '';  
				$rowData = '';  
				foreach ($results as $value) {  
					$date = date($this->language->get('date_format_short'), strtotime($value['OrderDate']));
					$total = $this->currency->format($value['total'], $this->config->get('config_currency'));
					$rowData.= '"' . $value['parentaffname'] . '"' . "\t";   
					$rowData.= '"' . $value['parentaffcode'] . '"' . "\t";  
					$rowData.= '"' . $value['parentaffcompany'] . '"' . "\t";  
					$rowData.= '"' . $value['tracking'] . '"' . "\t";  					 
					$rowData.= '"' . $value['order_id'] . '"' . "\t";
					$rowData.= '"' . $date . '"' . "\t";   					 
					$rowData.= '"' . $value['CustId'] . '"' . "\t";  				 
					$rowData.= '"' . $value['name']  . '"' . "\t";  				 
					$rowData.= '"' . $value['cust_name'] . '"' . "\t";  						 
					$rowData.= '"' . $value['affname'] . '"' . "\t";  					 
					$rowData.= '"' . $value['affcode'] . '"' . "\t";  					 
					$rowData.= '"' . $value['affcompany'] . '"' . "\t";  						 
					$rowData.= '"' . $total . '"' . "\n";  								 
				}  
				 
				$setData .= trim($rowData) . "\n";
				
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=IFS_Report.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			echo ucwords($columnHeader) . "\n" . $setData . "\n";  
			exit();

		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('report/ifs_volume', $data));
	}
}