<?php
class ControllerReportMemberOrder extends Controller {
	public function index() {
		$this->load->language('report/member_order');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = '';
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '';
		}

		if (isset($this->request->get['filter_customer'])) {
			$filter_customer = $this->request->get['filter_customer'];
		} else {
			$filter_customer = null;
		}
		
		if (isset($this->request->get['filter_member'])) {
			$filter_member = $this->request->get['filter_member'];
		} else {
			$filter_member = null;
		}

		if (isset($this->request->get['filter_code'])) {
			$filter_code = $this->request->get['filter_code'];
		} else {
			$filter_code = null;
		}		

		if (isset($this->request->get['filter_order_status_id'])) {
			$filter_order_status_id = $this->request->get['filter_order_status_id'];
		} else {
			$filter_order_status_id = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . urlencode($this->request->get['filter_customer']);
		}

		if (isset($this->request->get['filter_member'])) {
			$url .= '&filter_member=' . urlencode($this->request->get['filter_member']);
		}

		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . urlencode($this->request->get['filter_code']);
		}		
		
		if (isset($this->request->get['filter_order_status_id'])) {
			$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['export'] = $this->url->link('report/member_order/exportexcel', 'token=' . $this->session->data['token'], true);

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('report/member_order', 'token=' . $this->session->data['token'] . $url, true)
		);

		$this->load->model('report/member');

		$data['customers'] = array();

		$filter_data = array(
			'filter_date_start'			=> $filter_date_start,
			'filter_date_end'			=> $filter_date_end,
			'filter_customer'			=> $filter_customer,
			'filter_member'			=> $filter_member,
			'filter_code'			=> $filter_code,			
			'filter_order_status_id'	=> $filter_order_status_id,
			'start'						=> ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'						=> $this->config->get('config_limit_admin')
		);

		$customer_total = $this->model_report_member->getTotalOrders($filter_data);

		$results = $this->model_report_member->getOrders($filter_data);

		foreach ($results as $result) {
			$data['customers'][] = array(
				'code'       => $result['code'],
				'company'       => $result['company'],
				'contact'       => $result['contact'],				
				'customer'       => $result['customer'],
				'email'          => $result['email'],
				'customer_group' => $result['customer_group'],
				'status'         => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
				'orders'         => $result['orders'],
				'products'       => $result['products'],
				'total'          => $this->currency->format($result['total'], $this->config->get('config_currency')),
				'edit'           => $this->url->link('customer/customer/edit', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['customer_id'] . $url, true)
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_all_status'] = $this->language->get('text_all_status');

		$data['column_code'] = $this->language->get('column_code');
		$data['column_company'] = $this->language->get('column_company');
		$data['column_contact'] = $this->language->get('column_contact');		
		$data['column_customer'] = $this->language->get('column_customer');
		$data['column_email'] = $this->language->get('column_email');
		$data['column_customer_group'] = $this->language->get('column_customer_group');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_orders'] = $this->language->get('column_orders');
		$data['column_products'] = $this->language->get('column_products');
		$data['column_total'] = $this->language->get('column_total');
		$data['column_action'] = $this->language->get('column_action');

		$data['entry_date_start'] = $this->language->get('entry_date_start');
		$data['entry_date_end'] = $this->language->get('entry_date_end');
		$data['entry_customer'] = $this->language->get('entry_customer');
		$data['entry_member'] = $this->language->get('entry_member');
		$data['entry_code'] = $this->language->get('entry_code');		
		$data['entry_status'] = $this->language->get('entry_status');

		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_filter'] = $this->language->get('button_filter');

		$data['export_excel'] = $this->language->get('export_excel');

		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . urlencode($this->request->get['filter_customer']);
		}
		if (isset($this->request->get['filter_member'])) {
			$url .= '&filter_member=' . urlencode($this->request->get['filter_member']);
		}
		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . urlencode($this->request->get['filter_code']);
		}		
		if (isset($this->request->get['filter_order_status_id'])) {
			$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
		}

		$pagination = new Pagination();
		$pagination->total = $customer_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('report/member_order', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($customer_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($customer_total - $this->config->get('config_limit_admin'))) ? $customer_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $customer_total, ceil($customer_total / $this->config->get('config_limit_admin')));

		$data['filter_date_start'] = $filter_date_start;
		$data['filter_date_end'] = $filter_date_end;
		$data['filter_customer'] = $filter_customer;
		$data['filter_member'] = $filter_member;
		$data['filter_code'] = $filter_code;		
		$data['filter_order_status_id'] = $filter_order_status_id;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('report/member_order', $data));
	}


	public function exportexcel() {
		$this->load->language('report/member_order');
		$this->document->setTitle($this->language->get('heading_title'));

		$column_code = $this->language->get('column_code');
		$column_company = $this->language->get('column_company');
		$column_contact = $this->language->get('column_contact');		
		$column_customer = $this->language->get('column_customer');
		$column_email = $this->language->get('column_email');
		$column_customer_group = $this->language->get('column_customer_group');
		$column_status = $this->language->get('column_status');
		$column_orders = $this->language->get('column_orders');
		$column_products = $this->language->get('column_products');
		$column_total = $this->language->get('column_total');
		
		$this->load->model('report/member'); 
		$member_order_report_details = $this->model_report_member->getMemberOrderReportExport();

		if(!empty($member_order_report_details)){
			$columnHeader = '';  
			$columnHeader =  $column_code . "\t" . $column_company . "\t" . $column_contact  . "\t" . $column_customer . "\t" . $column_email . "\t" . $column_customer_group . "\t" . $column_status . "\t" . $column_products . "\t" . $column_total;  
			  
			$setData = '';  
				$rowData = '';  
				foreach ($member_order_report_details as $value) {  
					$rowData.= '"' . $value['code'] . '"' . "\t";   
					$rowData.= '"' . $value['company'] . '"' . "\t";  
					$rowData.= '"' . $value['contact'] . '"' . "\t";  
					$rowData.= '"' . $value['customer'] . '"' . "\t";  					 
					$rowData.= '"' . $value['email'] . '"' . "\t";  					 
					$rowData.= '"' . $value['customer_group'] . '"' . "\t";  				 
					$rowData.= '"' . ($value['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')) . '"' . "\t";  				 
					$rowData.= '"' . $value['orders'] . '"' . "\t";  						 
					$rowData.= '"' . $value['total'] . '"' . "\n";  					 
				}  
				$setData .= trim($rowData) . "\n";
				
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=Member_Orders_Report.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			echo ucwords($columnHeader) . "\n" . $setData . "\n";  
		}
	}
}