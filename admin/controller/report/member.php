<?php
class ControllerReportMember extends Controller { 
    public function index() {

        $this->load->language('report/member');

        $this->document->setTitle($this->language->get('heading_title'));
 
        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        }  


        $url = '';

        // if (isset($this->request->get['page'])) {
        //  $url .= '&page=' . $this->request->get['page'];
        // }
 

     
        $data['export'] = $this->url->link('report/member/exportexcel', 'token=' . $this->session->data['token'], true);

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('report/member', 'token=' . $this->session->data['token'] . $url, true)
        );



        $this->load->model('report/member');

        $data['members'] = array();

        $filter_data = array(
            'sort'  => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

 

        $results = $this->model_report_member->getTotalmembers($filter_data);

        $getTotalMembers = $this->model_report_member->getMembers($filter_data);

        foreach ($results as $result) {
            $data['members'][] = array(
                'affiliate_id'   => $result['affiliate_id'],
                'aff_first_name' => $result['firstname'],
                'aff_last_name'  => $result['lastname'],
                'email'          => $result['email'],
                'telephone'      => $result['telephone'],
                'fax'            => $result['fax'],
                'company'        => $result['company'],
                'code'           => $result['code'],
                'address_1'      => $result['address_1'],
                'address_2'      => $result['address_2'],
                'city'           => $result['city'],
                'postcode'       => $result['postcode'],
                'zone'           => $result['zone'],
                'country'        => $result['country'],
                'par_fname'      => $result['pfname'],
                'par_lname'      => $result['plname'],
            );
        }

        
        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');
        $data['text_all_status'] = $this->language->get('text_all_status');

        $data['column_aff_first_name'] = $this->language->get('column_aff_first_name');
        $data['column_aff_last_name'] = $this->language->get('column_aff_last_name');
         
        $data['column_email'] = $this->language->get('column_email');
        $data['column_telephone'] = $this->language->get('column_telephone');
        
        $data['column_fax'] = $this->language->get('column_fax');
        $data['column_company'] = $this->language->get('column_company');
        $data['column_code'] = $this->language->get('column_code');
        $data['column_address1'] = $this->language->get('column_address1');
        $data['column_address2'] = $this->language->get('column_address2');


        $data['column_city'] = $this->language->get('column_city');     
        $data['column_postcode'] = $this->language->get('column_postcode');
        $data['column_zone'] = $this->language->get('column_zone');
        $data['column_country'] = $this->language->get('column_country');
        $data['column_parentfname'] = $this->language->get('column_parentfname');
        $data['column_parentlname'] = $this->language->get('column_parentlname');
     
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_filter'] = $this->language->get('button_filter');

        $data['export_excel'] = $this->language->get('export_excel');

        $data['token'] = $this->session->data['token'];
        $pagination = new Pagination();
        $pagination->total = $getTotalMembers;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('report/member', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($getTotalMembers) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($getTotalMembers - $this->config->get('config_limit_admin'))) ? $getTotalMembers : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $getTotalMembers, ceil($getTotalMembers / $this->config->get('config_limit_admin')));

        $data['sort'] = $sort;
        $data['order'] = $order;


        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }
        $data['sort_firstname'] = $this->url->link('report/member', 'token=' . $this->session->data['token'] . '&sort=oc_affiliate.firstname' . $url, true);
        $data['sort_lastname'] = $this->url->link('report/member', 'token=' . $this->session->data['token'] . '&sort=oc_affiliate.lastname' . $url, true);
        $data['sort_email'] = $this->url->link('report/member', 'token=' . $this->session->data['token'] . '&sort=oc_affiliate.email' . $url, true);
        $data['sort_company'] = $this->url->link('report/member', 'token=' . $this->session->data['token'] . '&sort=oc_affiliate.company' . $url, true); 
        $data['sort_code'] = $this->url->link('report/member', 'token=' . $this->session->data['token'] . '&sort=oc_affiliate.code' . $url, true); 

 

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('report/member', $data));
    }

    public function exportexcel() {
        $this->load->language('report/member');
        $this->document->setTitle($this->language->get('heading_title'));
 
        $aff_id = $this->language->get('column_aff_id');
        $first_name = $this->language->get('column_aff_first_name');
        $last_name = $this->language->get('column_aff_last_name');
        $email = $this->language->get('column_email');
        $telephone = $this->language->get('column_telephone');
        
        $fax = $this->language->get('column_fax');
        $company = $this->language->get('column_company');
        $code = $this->language->get('column_code');
        $address1 = $this->language->get('column_address1');
        $address2 = $this->language->get('column_address2');

        $city = $this->language->get('column_city');     
        $postcode = $this->language->get('column_postcode');
        $zone = $this->language->get('column_zone');
        $country = $this->language->get('column_country');
        $pfname = $this->language->get('column_parentfname');
        $plname = $this->language->get('column_parentlname');
        $agreement = $this->language->get('column_agreement');
        
        $this->load->model('report/member'); 
        $getMemberExportdetails = $this->model_report_member->getMembersExport();
        
        if(!empty($getMemberExportdetails)){
            $columnHeader = '';  
            $columnHeader =  $aff_id . "\t" . $first_name . "\t" . $last_name . "\t" . $email  . "\t" . $telephone . "\t" . $fax . "\t"  . $company . "\t" . $code . "\t" . $address1 . "\t" . $address2 . "\t" . $city . "\t" . $postcode . "\t" . $zone . "\t" . $country . "\t" . $pfname  . "\t" . $plname . "\t" . $agreement  ;  

            $setData = '';  
                $rowData = '';  
                foreach ($getMemberExportdetails as $value) {  
                    $rowData.= '"' . $value['affiliate_id'] . '"' . "\t";   
                    $rowData.= '"' . $value['firstname'] . '"' . "\t";  
                    $rowData.= '"' . $value['lastname'] . '"' . "\t";  
                    $rowData.= '"' . $value['email'] . '"' . "\t";  
                    $rowData.= '"' . $value['telephone'] . '"' . "\t";                    
                    $rowData.= '"' . $value['fax'] . '"' . "\t";                       
                    $rowData.= '"' . $value['company'] . '"' . "\t";                  
                    $rowData.= '"' . $value['code'] . '"' . "\t";                 
                    $rowData.= '"' . $value['address_1'] . '"' . "\t";                          
                    $rowData.= '"' . $value['address_2'] . '"' . "\t";                       
                    $rowData.= '"' . $value['city'] . '"' . "\t";                       
                    $rowData.= '"' . $value['postcode'] . '"' . "\t";                       
                    $rowData.= '"' . $value['zone'] . '"' . "\t";                       
                    $rowData.= '"' . $value['country'] . '"' . "\t";                       
                    $rowData.= '"' . $value['pfname'] . '"' . "\t";                       
                    $rowData.= '"' . $value['plname'] . '"' . "\t";                       
                    $rowData.= '"' . $value['agreement'] . '"' . "\n";                       
                }  
            $setData .= trim($rowData) . "\n";
                
            header("Content-type: application/octet-stream");
            header("Content-Disposition: attachment; filename=Member_Report.xls");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo ucwords($columnHeader) . "\n" . $setData . "\n";  
        }
    }
}
