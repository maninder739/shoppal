<?php

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class ControllerReportProductPurchased extends Controller {

    public function index() {
        $this->load->language('report/product_purchased');

        $this->document->setTitle($this->language->get('heading_title'));



        if (isset($this->request->get['filter_date_start'])) {
            $filter_date_start = $this->request->get['filter_date_start'];
        } else {
            $filter_date_start = '';
        }

        if (isset($this->request->get['filter_date_end'])) {
            $filter_date_end = $this->request->get['filter_date_end'];
        } else {
            $filter_date_end = '';
        }

        if (isset($this->request->get['filter_order_status_id'])) {
            $filter_order_status_id = $this->request->get['filter_order_status_id'];
        } else {
            $filter_order_status_id = 0;
        }

        if (isset($this->request->get['filter_category_id'])) {
            $filter_category_id = $this->request->get['filter_category_id'];
        } else {
            $filter_category_id = 0;
        }


        if (isset($this->request->get['filter_product_id'])) {
            $filter_product_id = $this->request->get['filter_product_id'];
        } else {
            $filter_product_id = '';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        }

        $url = '';

        if (isset($this->request->get['filter_date_start'])) {
            $url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
        }

        if (isset($this->request->get['filter_date_end'])) {
            $url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
        }

        if (isset($this->request->get['filter_order_status_id'])) {
            $url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('report/product_purchased', 'token=' . $this->session->data['token'] . $url, true)
        );

        //$data['export'] = $this->url->link('report/member/exportexcel', 'token=' . $this->session->data['token'], true);

        $this->load->model('report/product');

        $data['products'] = array();

        $filter_data = array(
            'filter_category_id' => $filter_category_id,
            'filter_date_start' => $filter_date_start,
            'filter_product_id' => $filter_product_id,
            'filter_date_end' => $filter_date_end,
            'filter_order_status_id' => $filter_order_status_id,
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $product_total = $this->model_report_product->getTotalPurchased($filter_data);

        $results = $this->model_report_product->getPurchased($filter_data);

        if ($this->request->get['export_excel']) {
            $exportexcel = $this->exportexcel($filter_data);
        }


        foreach ($results as $result) {
            $data['products'][] = array(
                'name' => $result['name'],
                'model' => $result['model'],
                'quantity' => $result['quantity'],
                'category' => $result['category_name'],
                'total' => $this->currency->format($result['total'], $this->config->get('config_currency'))
            );
        }


        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');
        $data['text_all_status'] = $this->language->get('text_all_status');

        $data['column_name'] = $this->language->get('column_name');
        $data['column_model'] = $this->language->get('column_model');
        $data['column_quantity'] = $this->language->get('column_quantity');
        $data['column_total'] = $this->language->get('column_total');
        $data['column_category_name'] = $this->language->get('column_category_name');

        $data['entry_date_start'] = $this->language->get('entry_date_start');
        $data['entry_date_end'] = $this->language->get('entry_date_end');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_product_name'] = $this->language->get('entry_product_name');
        $data['entry_category_name'] = $this->language->get('entry_category_name');
        $data['exportexcel'] = $this->language->get('exportexcel');

        $data['button_filter'] = $this->language->get('button_filter');

        $data['token'] = $this->session->data['token'];

        $this->load->model('localisation/order_status');

        $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
        $data['prouduct_lisings'] = $this->model_report_product->productlising();
        $data['category_lisings'] = $this->model_report_product->categorylising();

        $url = '';

        if (isset($this->request->get['filter_date_start'])) {
            $url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
        }

        if (isset($this->request->get['filter_date_end'])) {
            $url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
        }

        if (isset($this->request->get['filter_order_status_id'])) {
            $url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
        }


        if (isset($this->request->get['filter_category_id'])) {
            $url .= '&filter_category_id=' . $this->request->get['filter_category_id'];
        }

        if (isset($this->request->get['filter_product_id'])) {
            $url .= '&filter_product_id=' . $this->request->get['filter_product_id'];
        }

        $pagination = new Pagination();
        $pagination->total = $product_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('report/product_purchased', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($product_total - $this->config->get('config_limit_admin'))) ? $product_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $product_total, ceil($product_total / $this->config->get('config_limit_admin')));

        $data['sort'] = $sort;
        $data['order'] = $order;


        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }
        $data['sort_productname'] = $this->url->link('report/product_purchased', 'token=' . $this->session->data['token'] . '&sort=op.name' . $url, true);
        $data['sort_category_name'] = $this->url->link('report/product_purchased', 'token=' . $this->session->data['token'] . '&sort=category_name' . $url, true);
        $data['sort_model'] = $this->url->link('report/product_purchased', 'token=' . $this->session->data['token'] . '&sort=op.model' . $url, true);
        $data['sort_quantity'] = $this->url->link('report/product_purchased', 'token=' . $this->session->data['token'] . '&sort=quantity' . $url, true);
        $data['sort_total'] = $this->url->link('report/product_purchased', 'token=' . $this->session->data['token'] . '&sort=total' . $url, true);

        $data['filter_date_start'] = $filter_date_start;
        $data['filter_date_end'] = $filter_date_end;
        $data['filter_order_status_id'] = $filter_order_status_id;
        $data['filter_product_ids'] = $filter_product_id;
        $data['filter_category_ids'] = $filter_category_id;


        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('report/product_purchased', $data));
    }

    public function exportexcel($data) {

        $this->load->model('report/member');
        $this->load->language('report/product_purchased');
        //require_once $_SERVER['DOCUMENT_ROOT'] . '/spdev/vendor/phpoffice/phpspreadsheet/src/Bootstrap.php';

        $spreadsheet = new Spreadsheet();
        $activeSheet = $spreadsheet->getActiveSheet();
       
        $data['start'] = '';
        $data['limit'] = '';

        $results = $this->model_report_product->getPurchased($data);

        $column_name = $this->language->get('column_name');
        $column_model = $this->language->get('column_model');
        $column_quantity = $this->language->get('column_quantity');
        $column_total = $this->language->get('column_total');
        $column_category_name = $this->language->get('column_category_name');

        if (!empty($results)) {
            //$activeSheet->fromArray(array_keys($results[0]), NULL, 'A4');
            $activeSheet->getColumnDimension('A')->setWidth(40);
            $activeSheet->getColumnDimension('B')->setWidth(30);
            $activeSheet->getColumnDimension('c')->setWidth(10);
            $activeSheet->getColumnDimension('d')->setWidth(40);
            $activeSheet->getColumnDimension('e')->setWidth(20);

            $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue('A1', $column_name)
                    ->setCellValue('B1', $column_model)
                    ->setCellValue('C1', $column_quantity)
                    ->setCellValue('D1', $column_category_name)
                    ->setCellValue('E1', $column_total);
            // echo "<pre>"; print_r( $activeSheet->activeSheetIndex); die('fads');
            foreach ($results as $key => $value) {
                $row = (int) $key + 2;
                $activeSheet->setCellValue('a' . $row, $value['name']);
                $activeSheet->setCellValue('b' . $row, $value['model']);
                $activeSheet->setCellValue('c' . $row, $value['quantity']);
                $activeSheet->setCellValue('d' . $row, $value['category_name']);
                $activeSheet->setCellValue('e' . $row, $this->currency->format($value['total'], $this->config->get('config_currency')));
            }

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="Product_Purchased.xlsx"');
            header('Cache-Control: max-age=0');

            $writer = IOFactory::createWriter($spreadsheet, 'Xlsx', $download = true);
            $name = $_SERVER['DOCUMENT_ROOT'].'/spdev/Product_Purchased.xlsx';
            $writer->save($name);
           // sleep(3);    
            $attachment = $name;

           $mail = new Mail();
           $mail->protocol = $this->config->get('config_mail_protocol');
           $mail->parameter = $this->config->get('config_mail_parameter');
           $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
           $mail->smtp_username = $this->config->get('config_mail_smtp_username');
           $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
           $mail->smtp_port = $this->config->get('config_mail_smtp_port');
           $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

           $mail->setTo('maninder@nascenture.com');
           $mail->setFrom($this->config->get('config_email'));
           $mail->setSender(html_entity_decode('testing', ENT_QUOTES, 'UTF-8'));
           $mail->setSubject(html_entity_decode(sprintf("New order", 'testing'), ENT_QUOTES, 'UTF-8'));
           $mail->setText("testing");
           $mail->addAttachment($attachment);
           $mail->send();
           echo "string";die;
            exit();
        }
    }

}
