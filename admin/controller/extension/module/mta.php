<?php

class ControllerExtensionModuleMta extends Controller {

	private $version = 160603;

	private $_perms = array(
		'mta/mta',
		'mta/mta_affiliate_dt',
		'mta/mta_pds',		
		'mta/mta_set_scheme',
		'mta/affiliate_downline',
		'mta/mta_event'
	);	
	
	public function install() {
		//echo "string"; die;
$this->db->query("create table if not exists " . DB_PREFIX . "mta_scheme (
	mta_scheme_id int(6) unsigned not null auto_increment,
	scheme_name varchar(100) not null default '',
	description text not null,
	max_levels smallint(3) unsigned not null default '1',
	is_default tinyint(1) unsigned not null default '0', 
	all_commissions text not null,	
	all_autoadd text not null,	
	commission_type enum('percentage','fixed') not null default 'percentage',
	before_shipping tinyint(1) unsigned not null default '1',
	eternal smallint(3) unsigned not null default '0',
	signup_code char(13) not null default '',
	primary key (mta_scheme_id),
	unique key (`scheme_name`),
	unique key (`signup_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8");
	
$this->db->query("create table if not exists " . DB_PREFIX . "mta_scheme_levels (
	mta_scheme_level_id int(8) unsigned not null auto_increment,
	mta_scheme_id int(6) unsigned not null default '1',
	num_levels smallint(3) unsigned not null default '1',	
	level smallint(3) unsigned not null default '1',	
	commission decimal(15,4) NOT NULL DEFAULT '0.0000',
	autoadd smallint(2) unsigned not null default '1',
	primary key (mta_scheme_level_id),
	unique key (mta_scheme_id, num_levels, level),
	CONSTRAINT `mta_scheme_level_ibfk_1` FOREIGN KEY (`mta_scheme_id`) REFERENCES `" .  DB_PREFIX . "mta_scheme` (`mta_scheme_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8");

$this->db->query("create table if not exists " . DB_PREFIX . "mta_autoapprove (
	mta_autoapprove_id int(8) unsigned not null auto_increment,
	mta_scheme_id int(6) unsigned not null default '1',
	signup_level smallint(3) unsigned not null default '1',	
	autoapprove smallint(2) unsigned not null default '1',
	primary key (mta_autoapprove_id),
	unique key (mta_scheme_id, signup_level),
	CONSTRAINT `mta_autoapprove_ibfk_1` FOREIGN KEY (`mta_scheme_id`) REFERENCES `" .  DB_PREFIX . "mta_scheme` (`mta_scheme_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8");


$this->db->query("create table if not exists " . DB_PREFIX . "mta_affiliate (	
	affiliate_id int(10) unsigned not null default '0',
	mta_scheme_id int(6) unsigned default null,
	parent_affiliate_id int(10) unsigned not null default '0',
	all_parent_ids text not null,		
	level_original smallint(3) unsigned not null default '1',		
	primary key (affiliate_id),
  KEY `FK_mta_scheme_id` (`mta_scheme_id`),
	CONSTRAINT `mta_affiliate_ibfk_1` FOREIGN KEY (`mta_scheme_id`) REFERENCES `" .  DB_PREFIX . "mta_scheme` (`mta_scheme_id`) ON DELETE SET NULL ON UPDATE CASCADE  	
) ENGINE=InnoDB DEFAULT CHARSET=utf8");

$this->db->query("create table if not exists " . DB_PREFIX . "mta_product (
	mta_product_id int(11) unsigned NOT NULL auto_increment,
	product_id int(11) unsigned NOT NULL default '0',	
	price_mod_type enum('','coupon','special','discount') not null default '',
	price_mod_id int(11) unsigned NOT NULL default '0',	
	mta_scheme_id int(6) unsigned default null,
	primary key (mta_product_id),
	unique key (product_id, price_mod_type, price_mod_id),
	KEY `FK_mta_product_mta_scheme_id` (`mta_scheme_id`),
	CONSTRAINT `mta_product_ibfk_1` FOREIGN KEY (`mta_scheme_id`) REFERENCES `" .  DB_PREFIX . "mta_scheme` (`mta_scheme_id`) ON DELETE CASCADE ON UPDATE CASCADE  	
) ENGINE=InnoDB DEFAULT CHARSET=utf8");

$this->db->query("create table if not exists " . DB_PREFIX . "mta_product_affiliate (
	mta_product_affiliate_id int(10) unsigned not null auto_increment,	
	product_id int(11) unsigned NOT NULL default '0',
	affiliate_id int(10) unsigned not null default '0',
	price_mod_type enum('','coupon','special','discount') not null default '',
	price_mod_id int(11) unsigned NOT NULL default '0',	
	mta_scheme_id int(6) unsigned default null,
	primary key (mta_product_affiliate_id),
	unique key (product_id,affiliate_id,price_mod_type, price_mod_id),
	CONSTRAINT `mta_product_affiliate_ibfk_1` FOREIGN KEY (`affiliate_id`) REFERENCES `" .  DB_PREFIX . "mta_affiliate` (`affiliate_id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `mta_product_affiliate_ibfk_2` FOREIGN KEY (`mta_scheme_id`) REFERENCES `" .  DB_PREFIX . "mta_scheme` (`mta_scheme_id`) ON DELETE CASCADE ON UPDATE CASCADE  	
) ENGINE=InnoDB DEFAULT CHARSET=utf8");

$this->db->query("create table if not exists " . DB_PREFIX . "mta_order (
	mta_order_id int(11) unsigned not null auto_increment,		
	order_id int(11) not null default '0',  	
	affiliate_id int(10) unsigned default null,
	commission decimal(15,4) NOT NULL DEFAULT '0.0000',
	commission_added tinyint(1) unsigned not null default '0', 
	autoadd tinyint(1) unsigned not null default '0', 
	primary key (mta_order_id),
	unique key (affiliate_id, order_id),
	CONSTRAINT `mta_order_ibfk_1` FOREIGN KEY (`affiliate_id`) REFERENCES `" .  DB_PREFIX . "mta_affiliate` (`affiliate_id`) ON DELETE set null ON UPDATE CASCADE  	
) ENGINE=InnoDB DEFAULT CHARSET=utf8");

$this->db->query("create table if not exists " . DB_PREFIX . "mta_order_product (
	mta_order_product_id int(11) unsigned not null auto_increment,		
	mta_order_id int(11) unsigned default null,
	product_id int(11) unsigned NOT NULL default '0',	
	order_product_id int(11) unsigned NOT NULL default '0',	
	affiliate_id int(10) unsigned default null,	
	commission decimal(15,4) NOT NULL DEFAULT '0.0000',
	mta_scheme_id int(6) unsigned default null,
	num_levels smallint(3) unsigned not null default '1',	
	level smallint(3) unsigned not null default '1',
	autoadd tinyint(1) unsigned not null default '0', 
	primary key (mta_order_product_id),
	unique key (affiliate_id, order_product_id),
	KEY `FK_mta_order_id` (`mta_order_id`),
	CONSTRAINT `mta_order_product_ibfk_1` FOREIGN KEY (`mta_order_id`) REFERENCES `" .  DB_PREFIX . "mta_order` (`mta_order_id`) ON DELETE CASCADE ON UPDATE CASCADE  	
) ENGINE=InnoDB DEFAULT CHARSET=utf8");

$this->db->query("CREATE TABLE if not exists `" . DB_PREFIX . "mta_product_default_scheme` (
  `mta_product_default_scheme_id` INT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `entity_type` ENUM('coupon','m','c','m_coupon','m_special','m_discount','c_coupon','c_special','c_discount') NOT NULL DEFAULT 'coupon',
  `entity_id` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `mta_scheme_id` INT(6) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`mta_product_default_scheme_id`),
  UNIQUE KEY `enitity` (`entity_id`, `entity_type`),
  CONSTRAINT `mta_product_default_scheme_ibfk_1` FOREIGN KEY (`mta_scheme_id`) REFERENCES `" . DB_PREFIX . "mta_scheme` (`mta_scheme_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8");	
	
try {
	$_hasc = false;
	$res = $this->db->query("show columns from " . DB_PREFIX . "customer");
	foreach($res->rows as $_r) {
		if($_r['Field'] == 'affiliate_id') {
			$_hasc = true;
			break;
		}		
	}
	if(!$_hasc) $this->db->query("alter table " . DB_PREFIX . "customer add affiliate_id int(11) unsigned not null default '0'");
	$_res = $this->db->query("select customer_id, affiliate_id from `" . DB_PREFIX . "order` where affiliate_id > 0 group by customer_id");
	$res = $_res->num_rows > 0 ? $_res->rows : array();
} catch(Exception $_exc) {
	$res = array();
}

foreach($res as $_r) {
	$this->db->query("update " . DB_PREFIX . "customer set affiliate_id='" . (int) $_r['affiliate_id'] . "' where customer_id='" . (int) $_r['customer_id'] . "'");
}	

		$this->load->model('setting/setting');
		$this->model_setting_setting->editSetting('mta_version', array('mta_version' => $this->version));

		$this->module_permissions('add', $this->_perms);		
		
		$events = array(
			array('admin/model/marketing/coupon/deleteCoupon/before', 'mta/mta_event/eventDeleteCoupon'),
			array('admin/model/catalog/product/addProduct/after', 'mta/mta_event/eventAddProductPost'),
			array('admin/model/marketing/affiliate/deleteAffiliate/before', 'mta/mta_event/eventDeleteAffiliate'),			
			array('admin/model/marketing/affiliate/addAffiliate/after', 'mta/mta_event/eventAddAffiliatePost'),		
			array('admin/model/marketing/affiliate/editAffiliate/before', 'mta/mta_event/eventEditAffiliatePre'),
			array('admin/model/catalog/product/editProduct/before', 'mta/mta_event/eventEditProductPre'),			
			array('admin/model/catalog/product/editProduct/after', 'mta/mta_event/eventEditProductPost'),			
			array('admin/model/catalog/product/deleteProduct/before', 'mta/mta_event/eventDeleteProduct'),						
			array('catalog/model/account/customer/addCustomer/after', 'affiliate/mta_event/eventAddCustomerPost'),			
			array('catalog/model/affiliate/affiliate/addAffiliate/after', 'affiliate/mta_event/eventAddAffiliatePost'),	
			array('catalog/model/checkout/order/addOrderHistory/before', 'affiliate/mta_event/eventAddOrderHistoryPre'),
			array('catalog/model/checkout/order/addOrderHistory/after', 'affiliate/mta_event/eventAutoAddCommissions'),
			array('catalog/model/affiliate/affiliate/addTransaction/after', 'affiliate/mta_event/eventAddTransactionPost'),
			array('catalog/model/checkout/order/deleteOrder/before', 'affiliate/mta_event/eventDeleteOrder'),			
			array('catalog/model/checkout/order/addOrder/after', 'affiliate/mta_event/eventAddOrderPost'),			
			array('catalog/model/checkout/order/editOrder/after', 'affiliate/mta_event/eventEditOrderPost')
		);		
		$this->addEvents('mta', $events);
	}
	
	public function index() {
		$this->response->redirect($this->url->link('mta/mta', 'token=' . $this->session->data['token'], 'SSL'));	
	}

	public function uninstall() {
		if (!$this->user->hasPermission('modify', 'extension/module')) return;		
		$this->load->model('setting/setting');
		$this->model_setting_setting->deleteSetting('mta_version');
		$this->model_setting_setting->deleteSetting('mta_ypx');

		$this->deleteEvents('mta');
		$this->module_permissions('remove', $this->_perms);		
	}

	protected function addEvents($name, $events) {
		$this->load->model('extension/event');
		$this->model_extension_event->deleteEvent($name);
		foreach($events as $e) {			
			$this->model_extension_event->addEvent($name, $e[0], $e[1]);
		}		
	}
	
	protected function deleteEvents($name) {
		$this->load->model('extension/event');
		$this->model_extension_event->deleteEvent($name);	
	}		

	protected function module_permissions($action = 'add', $data = array()) {
		if($action !== 'add') $action = 'remove';
		if(sizeof($data) === 0) {
			if(property_exists($this, 'name') && $this->name) {
				$data = array('extension/module/' . $this->name);
			} else {
				return;
			}
		}
		$this->load->model('user/user_group');
		$id = $this->user->getGroupId();
		foreach($data as $route) {
			$this->model_user_user_group->{$action . 'Permission'}($id, 'access', $route);
			$this->model_user_user_group->{$action . 'Permission'}($id, 'modify', $route);
		}
	}
	
}
