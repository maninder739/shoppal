<?php
class ControllerExtensionModuleAdminMenuMinifier extends Controller {
	private $error = array(); 

	public function index() {   

		$data['button_save'] = 'Save Changes';
		$data['button_cancel'] = 'Cancel';

		$this->document->setTitle('Admin Menu Minifier');

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

			$this->model_setting_setting->editSetting('module_menu_min', $this->request->post);		

			$this->session->data['success'] = 'Success: You have modified module Admin Menu Minifier!';
			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'], 'SSL'));
		}

		$data['heading_title'] = 'Admin Menu Minifier';

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text'      => 'Home',
			'href'      => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], 'SSL'),
			'separator' => false
		);

		$data['breadcrumbs'][] = array(
			'text'      => 'Module',
			'href'      => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'], 'SSL'),
			'separator' => ' :: '
		);

		$data['breadcrumbs'][] = array(
			'text'      => 'Admin Menu Minifier',
			'href'      => $this->url->link('extension/module/adminmenuminifier', 'user_token=' . $this->session->data['user_token'], 'SSL'),
			'separator' => ' :: '
		);

		$data['action'] = $this->url->link('extension/module/adminmenuminifier', 'user_token=' . $this->session->data['user_token'], 'SSL');

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'], 'SSL');

		$data['modules'] = array();

		if (isset($this->request->post['module_menu_min'])) {
			$data['modules'] = $this->request->post['module_menu_min'];
		} elseif ($this->config->get('module_menu_min')) { 
			$data['modules'] = $this->config->get('module_menu_min');

		}

		if (isset($this->request->post['module_menu_min_status'])) {
			$data['module_menu_min_status'] = $this->request->post['module_menu_min_status'];
		} else {
			$data['module_menu_min_status'] = $this->config->get('module_menu_min_status');
		}

		if (isset($this->request->post['module_menu_min_categories'])) {
			$data['module_menu_min_categories'] = $this->request->post['module_menu_min_categories'];
		} else {
			$data['module_menu_min_categories'] = $this->config->get('module_menu_min_categories');
		}

		if (isset($this->request->post['module_menu_min_products'])) {
			$data['module_menu_min_products'] = $this->request->post['module_menu_min_products'];
		} else {
			$data['module_menu_min_products'] = $this->config->get('module_menu_min_products');
		}

		if (isset($this->request->post['module_menu_min_recurring_profiles'])) {
			$data['module_menu_min_recurring_profiles'] = $this->request->post['module_menu_min_recurring_profiles'];
		} else {
			$data['module_menu_min_recurring_profiles'] = $this->config->get('module_menu_min_recurring_profiles');
		}

		if (isset($this->request->post['module_menu_min_filters'])) {
			$data['module_menu_min_filters'] = $this->request->post['module_menu_min_filters'];
		} else {
			$data['module_menu_min_filters'] = $this->config->get('module_menu_min_filters');
		}
		
		if (isset($this->request->post['module_menu_min_attributes'])) {
			$data['module_menu_min_attributes'] = $this->request->post['module_menu_min_attributes'];
		} else {
			$data['module_menu_min_attributes'] = $this->config->get('module_menu_min_attributes');
		}
		
		if (isset($this->request->post['module_menu_min_options'])) {
			$data['module_menu_min_options'] = $this->request->post['module_menu_min_options'];
		} else {
			$data['module_menu_min_options'] = $this->config->get('module_menu_min_options');
		}
		
		if (isset($this->request->post['module_menu_min_manufacturers'])) {
			$data['module_menu_min_manufacturers'] = $this->request->post['module_menu_min_manufacturers'];
		} else {
			$data['module_menu_min_manufacturers'] = $this->config->get('module_menu_min_manufacturers');
		}
		
		if (isset($this->request->post['module_menu_min_downloads'])) {
			$data['module_menu_min_downloads'] = $this->request->post['module_menu_min_downloads'];
		} else {
			$data['module_menu_min_downloads'] = $this->config->get('module_menu_min_downloads');
		}
		
		if (isset($this->request->post['module_menu_min_reviews'])) {
			$data['module_menu_min_reviews'] = $this->request->post['module_menu_min_reviews'];
		} else {
			$data['module_menu_min_reviews'] = $this->config->get('module_menu_min_reviews');
		}
		
		if (isset($this->request->post['module_menu_min_information'])) {
			$data['module_menu_min_information'] = $this->request->post['module_menu_min_information'];
		} else {
			$data['module_menu_min_information'] = $this->config->get('module_menu_min_information');
		}
		
		if (isset($this->request->post['module_menu_min_marketplace'])) {
			$data['module_menu_min_marketplace'] = $this->request->post['module_menu_min_marketplace'];
		} else {
			$data['module_menu_min_marketplace'] = $this->config->get('module_menu_min_marketplace');
		}
		
		if (isset($this->request->post['module_menu_min_installer'])) {
			$data['module_menu_min_installer'] = $this->request->post['module_menu_min_installer'];
		} else {
			$data['module_menu_min_installer'] = $this->config->get('module_menu_min_installer');
		}
		
		if (isset($this->request->post['module_menu_min_extensions'])) {
			$data['module_menu_min_extensions'] = $this->request->post['module_menu_min_extensions'];
		} else {
			$data['module_menu_min_extensions'] = $this->config->get('module_menu_min_extensions');
		}
		
		if (isset($this->request->post['module_menu_min_modifications'])) {
			$data['module_menu_min_modifications'] = $this->request->post['module_menu_min_modifications'];
		} else {
			$data['module_menu_min_modifications'] = $this->config->get('module_menu_min_modifications');
		}
		
		if (isset($this->request->post['module_menu_min_events'])) {
			$data['module_menu_min_events'] = $this->request->post['module_menu_min_events'];
		} else {
			$data['module_menu_min_events'] = $this->config->get('module_menu_min_events');
		}
		
		if (isset($this->request->post['module_menu_min_layouts'])) {
			$data['module_menu_min_layouts'] = $this->request->post['module_menu_min_layouts'];
		} else {
			$data['module_menu_min_layouts'] = $this->config->get('module_menu_min_layouts');
		}
		
		if (isset($this->request->post['module_menu_min_theme_editor'])) {
			$data['module_menu_min_theme_editor'] = $this->request->post['module_menu_min_theme_editor'];
		} else {
			$data['module_menu_min_theme_editor'] = $this->config->get('module_menu_min_theme_editor');
		}
		
		if (isset($this->request->post['module_menu_min_language_editor'])) {
			$data['module_menu_min_language_editor'] = $this->request->post['module_menu_min_language_editor'];
		} else {
			$data['module_menu_min_language_editor'] = $this->config->get('module_menu_min_language_editor');
		}
		
		if (isset($this->request->post['module_menu_min_banners'])) {
			$data['module_menu_min_banners'] = $this->request->post['module_menu_min_banners'];
		} else {
			$data['module_menu_min_banners'] = $this->config->get('module_menu_min_banners');
		}

		if (isset($this->request->post['module_menu_min_seo_urls'])) {
			$data['module_menu_min_seo_urls'] = $this->request->post['module_menu_min_seo_urls'];
		} else {
			$data['module_menu_min_seo_urls'] = $this->config->get('module_menu_min_seo_urls');
		}
		
		if (isset($this->request->post['module_menu_min_orders'])) {
			$data['module_menu_min_orders'] = $this->request->post['module_menu_min_orders'];
		} else {
			$data['module_menu_min_orders'] = $this->config->get('module_menu_min_orders');
		}
		
		if (isset($this->request->post['module_menu_min_recurring_orders'])) {
			$data['module_menu_min_recurring_orders'] = $this->request->post['module_menu_min_recurring_orders'];
		} else {
			$data['module_menu_min_recurring_orders'] = $this->config->get('module_menu_min_recurring_orders');
		}
		
		if (isset($this->request->post['module_menu_min_returns'])) {
			$data['module_menu_min_returns'] = $this->request->post['module_menu_min_returns'];
		} else {
			$data['module_menu_min_returns'] = $this->config->get('module_menu_min_returns');
		}
		
		if (isset($this->request->post['module_menu_min_gift_vouchers'])) {
			$data['module_menu_min_gift_vouchers'] = $this->request->post['module_menu_min_gift_vouchers'];
		} else {
			$data['module_menu_min_gift_vouchers'] = $this->config->get('module_menu_min_gift_vouchers');
		}
		
		if (isset($this->request->post['module_menu_min_customers'])) {
			$data['module_menu_min_customers'] = $this->request->post['module_menu_min_customers'];
		} else {
			$data['module_menu_min_customers'] = $this->config->get('module_menu_min_customers');
		}
		
		if (isset($this->request->post['module_menu_min_customer_groups'])) {
			$data['module_menu_min_customer_groups'] = $this->request->post['module_menu_min_customer_groups'];
		} else {
			$data['module_menu_min_customer_groups'] = $this->config->get('module_menu_min_customer_groups');
		}
		
		if (isset($this->request->post['module_menu_min_customer_approvals'])) {
			$data['module_menu_min_customer_approvals'] = $this->request->post['module_menu_min_customer_approvals'];
		} else {
			$data['module_menu_min_customer_approvals'] = $this->config->get('module_menu_min_customer_approvals');
		}
		
		if (isset($this->request->post['module_menu_min_custom_fields'])) {
			$data['module_menu_min_custom_fields'] = $this->request->post['module_menu_min_custom_fields'];
		} else {
			$data['module_menu_min_custom_fields'] = $this->config->get('module_menu_min_custom_fields');
		}
		
		if (isset($this->request->post['module_menu_min_marketing'])) {
			$data['module_menu_min_marketing'] = $this->request->post['module_menu_min_marketing'];
		} else {
			$data['module_menu_min_marketing'] = $this->config->get('module_menu_min_marketing');
		}
		
		if (isset($this->request->post['module_menu_min_coupons'])) {
			$data['module_menu_min_coupons'] = $this->request->post['module_menu_min_coupons'];
		} else {
			$data['module_menu_min_coupons'] = $this->config->get('module_menu_min_coupons');
		}
		
		if (isset($this->request->post['module_menu_min_mail'])) {
			$data['module_menu_min_mail'] = $this->request->post['module_menu_min_mail'];
		} else {
			$data['module_menu_min_mail'] = $this->config->get('module_menu_min_mail');
		}
		
		if (isset($this->request->post['module_menu_min_settings'])) {
			$data['module_menu_min_settings'] = $this->request->post['module_menu_min_settings'];
		} else {
			$data['module_menu_min_settings'] = $this->config->get('module_menu_min_settings');
		}
		
		if (isset($this->request->post['module_menu_min_users'])) {
			$data['module_menu_min_users'] = $this->request->post['module_menu_min_users'];
		} else {
			$data['module_menu_min_users'] = $this->config->get('module_menu_min_users');
		}
		
		if (isset($this->request->post['module_menu_min_localisation'])) {
			$data['module_menu_min_localisation'] = $this->request->post['module_menu_min_localisation'];
		} else {
			$data['module_menu_min_localisation'] = $this->config->get('module_menu_min_localisation');
		}
		
		if (isset($this->request->post['module_menu_min_maintenance'])) {
			$data['module_menu_min_maintenance'] = $this->request->post['module_menu_min_maintenance'];
		} else {
			$data['module_menu_min_maintenance'] = $this->config->get('module_menu_min_maintenance');
		}
		
		if (isset($this->request->post['module_menu_min_reports'])) {
			$data['module_menu_min_reports'] = $this->request->post['module_menu_min_reports'];
		} else {
			$data['module_menu_min_reports'] = $this->config->get('module_menu_min_reports');
		}
		
		if (isset($this->request->post['module_menu_min_whos_online'])) {
			$data['module_menu_min_whos_online'] = $this->request->post['module_menu_min_whos_online'];
		} else {
			$data['module_menu_min_whos_online'] = $this->config->get('module_menu_min_whos_online');
		}
		
		if (isset($this->request->post['module_menu_min_statistics'])) {
			$data['module_menu_min_statistics'] = $this->request->post['module_menu_min_statistics'];
		} else {
			$data['module_menu_min_statistics'] = $this->config->get('module_menu_min_statistics');
		}

		if (isset($this->request->post['module_menu_min_catalog_tab'])) {
			$data['module_menu_min_catalog_tab'] = $this->request->post['module_menu_min_catalog_tab'];
		} else {
			$data['module_menu_min_catalog_tab'] = $this->config->get('module_menu_min_catalog_tab');
		}
		
		if (isset($this->request->post['module_menu_min_extensions_tab'])) {
			$data['module_menu_min_extensions_tab'] = $this->request->post['module_menu_min_extensions_tab'];
		} else {
			$data['module_menu_min_extensions_tab'] = $this->config->get('module_menu_min_extensions_tab');
		}
		
		if (isset($this->request->post['module_menu_min_design_tab'])) {
			$data['module_menu_min_design_tab'] = $this->request->post['module_menu_min_design_tab'];
		} else {
			$data['module_menu_min_design_tab'] = $this->config->get('module_menu_min_design_tab');
		}
		
		if (isset($this->request->post['module_menu_min_sales_tab'])) {
			$data['module_menu_min_sales_tab'] = $this->request->post['module_menu_min_sales_tab'];
		} else {
			$data['module_menu_min_sales_tab'] = $this->config->get('module_menu_min_sales_tab');
		}
		
		if (isset($this->request->post['module_menu_min_customers_tab'])) {
			$data['module_menu_min_customers_tab'] = $this->request->post['module_menu_min_customers_tab'];
		} else {
			$data['module_menu_min_customers_tab'] = $this->config->get('module_menu_min_customers_tab');
		}
		
		if (isset($this->request->post['module_menu_min_marketing_tab'])) {
			$data['module_menu_min_marketing_tab'] = $this->request->post['module_menu_min_marketing_tab'];
		} else {
			$data['module_menu_min_marketing_tab'] = $this->config->get('module_menu_min_marketing_tab');
		}
		
		if (isset($this->request->post['module_menu_min_system_tab'])) {
			$data['module_menu_min_system_tab'] = $this->request->post['module_menu_min_system_tab'];
		} else {
			$data['module_menu_min_system_tab'] = $this->config->get('module_menu_min_system_tab');
		}
		
		if (isset($this->request->post['module_menu_min_reports_tab'])) {
			$data['module_menu_min_reports_tab'] = $this->request->post['module_menu_min_reports_tab'];
		} else {
			$data['module_menu_min_reports_tab'] = $this->config->get('module_menu_min_reports_tab');
		}
		
		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['footer'] = $this->load->controller('common/footer');
		$data['column_left'] = $this->load->controller('common/column_left');

		$this->response->setOutput($this->load->view('extension/module/adminmenuminifier', $data));

	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/adminmenuminifier')) {
			$this->error['warning'] = 'Warning: You do not have permission to modify module Admin Menu Minifier!';
		}

		return !$this->error;	
	}

}
?>