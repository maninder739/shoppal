<?php
class ControllerExtensionPaymentPPExpressMonthly extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/payment/pp_express_monthly');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('pp_express_monthly', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=payment', true));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_signup'] = $this->language->get('text_signup');
		$data['text_sandbox'] = $this->language->get('text_sandbox');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_authorization'] = $this->language->get('text_authorization');
		$data['text_sale'] = $this->language->get('text_sale');

		$data['entry_username'] = $this->language->get('entry_username');
		$data['entry_password'] = $this->language->get('entry_password');
		$data['entry_signature'] = $this->language->get('entry_signature');
		$data['entry_sandbox_username'] = $this->language->get('entry_sandbox_username');
		$data['entry_sandbox_password'] = $this->language->get('entry_sandbox_password');
		$data['entry_sandbox_signature'] = $this->language->get('entry_sandbox_signature');
		$data['entry_ipn'] = $this->language->get('entry_ipn');
		$data['entry_test'] = $this->language->get('entry_test');
		$data['entry_debug'] = $this->language->get('entry_debug');
		$data['entry_currency'] = $this->language->get('entry_currency');
		$data['entry_recurring_cancel'] = $this->language->get('entry_recurring_cancel');
		$data['entry_transaction'] = $this->language->get('entry_transaction');
		$data['entry_total'] = $this->language->get('entry_total');
		$data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_canceled_reversal_status'] = $this->language->get('entry_canceled_reversal_status');
		$data['entry_completed_status'] = $this->language->get('entry_completed_status');
		$data['entry_denied_status'] = $this->language->get('entry_denied_status');
		$data['entry_expired_status'] = $this->language->get('entry_expired_status');
		$data['entry_failed_status'] = $this->language->get('entry_failed_status');
		$data['entry_pending_status'] = $this->language->get('entry_pending_status');
		$data['entry_processed_status'] = $this->language->get('entry_processed_status');
		$data['entry_refunded_status'] = $this->language->get('entry_refunded_status');
		$data['entry_reversed_status'] = $this->language->get('entry_reversed_status');
		$data['entry_voided_status'] = $this->language->get('entry_voided_status');
		$data['entry_allow_notes'] = $this->language->get('entry_allow_notes');
		$data['entry_logo'] = $this->language->get('entry_logo');
		$data['entry_colour'] = $this->language->get('entry_colour');

		$data['help_total'] = $this->language->get('help_total');
		$data['help_ipn'] = $this->language->get('help_ipn');
		$data['help_currency'] = $this->language->get('help_currency');
		$data['help_logo'] = $this->language->get('help_logo');
		$data['help_colour'] = $this->language->get('help_colour');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_search'] = $this->language->get('button_search');

		$data['tab_api'] = $this->language->get('tab_api');
		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_order_status'] = $this->language->get('tab_order_status');
		$data['tab_checkout'] = $this->language->get('tab_checkout');

		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['username'])) {
			$data['error_username'] = $this->error['username'];
		} else {
			$data['error_username'] = '';
		}

		if (isset($this->error['password'])) {
			$data['error_password'] = $this->error['password'];
		} else {
			$data['error_password'] = '';
		}

		if (isset($this->error['signature'])) {
			$data['error_signature'] = $this->error['signature'];
		} else {
			$data['error_signature'] = '';
		}

		if (isset($this->error['sandbox_username'])) {
			$data['error_sandbox_username'] = $this->error['sandbox_username'];
		} else {
			$data['error_sandbox_username'] = '';
		}

		if (isset($this->error['sandbox_password'])) {
			$data['error_sandbox_password'] = $this->error['sandbox_password'];
		} else {
			$data['error_sandbox_password'] = '';
		}

		if (isset($this->error['sandbox_signature'])) {
			$data['error_sandbox_signature'] = $this->error['sandbox_signature'];
		} else {
			$data['error_sandbox_signature'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true),
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=payment', true),
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/payment/pp_express_monthly', 'token=' . $this->session->data['token'], true),
		);

		$data['action'] = $this->url->link('extension/payment/pp_express_monthly', 'token=' . $this->session->data['token'], true);
		
		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=payment', true);
		
		$data['search'] = $this->url->link('extension/payment/pp_express_monthly/search', 'token=' . $this->session->data['token'], true);

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($this->config->get('config_country_id'));

		$data['signup'] = 'https://www.paypal.com/webapps/merchantboarding/webflow/externalpartnerflow?countryCode=' . $country_info['iso_code_2'] . '&integrationType=F&merchantId=David111&displayMode=minibrowser&partnerId=9PDNYE4RZBVFJ&productIntentID=addipmt&receiveCredentials=TRUE&returnToPartnerUrl=' . base64_encode(html_entity_decode($this->url->link('extension/payment/pp_express_monthly/live', 'token=' . $this->session->data['token'], true))) . '&subIntegrationType=S';
		$data['sandbox'] = 'https://www.sandbox.paypal.com/webapps/merchantboarding/webflow/externalpartnerflow?countryCode=' . $country_info['iso_code_2'] . '&integrationType=F&merchantId=David111&displayMode=minibrowser&partnerId=T4E8WSXT43QPJ&productIntentID=addipmt&receiveCredentials=TRUE&returnToPartnerUrl=' . base64_encode(html_entity_decode($this->url->link('extension/payment/pp_express_monthly/sandbox', 'token=' . $this->session->data['token'], true))) . '&subIntegrationType=S';

		if (isset($this->request->post['pp_express_monthly_username'])) {
			$data['pp_express_monthly_username'] = $this->request->post['pp_express_monthly_username'];
		} else {
			$data['pp_express_monthly_username'] = $this->config->get('pp_express_monthly_username');
		}

		if (isset($this->request->post['pp_express_monthly_password'])) {
			$data['pp_express_monthly_password'] = $this->request->post['pp_express_monthly_password'];
		} else {
			$data['pp_express_monthly_password'] = $this->config->get('pp_express_monthly_password');
		}

		if (isset($this->request->post['pp_express_monthly_signature'])) {
			$data['pp_express_monthly_signature'] = $this->request->post['pp_express_monthly_signature'];
		} else {
			$data['pp_express_monthly_signature'] = $this->config->get('pp_express_monthly_signature');
		}

		if (isset($this->request->post['pp_express_monthly_sandbox_username'])) {
			$data['pp_express_monthly_sandbox_username'] = $this->request->post['pp_express_monthly_sandbox_username'];
		} else {
			$data['pp_express_monthly_sandbox_username'] = $this->config->get('pp_express_monthly_sandbox_username');
		}

		if (isset($this->request->post['pp_express_monthly_sandbox_password'])) {
			$data['pp_express_monthly_sandbox_password'] = $this->request->post['pp_express_monthly_sandbox_password'];
		} else {
			$data['pp_express_monthly_sandbox_password'] = $this->config->get('pp_express_monthly_sandbox_password');
		}

		if (isset($this->request->post['pp_express_monthly_sandbox_signature'])) {
			$data['pp_express_monthly_sandbox_signature'] = $this->request->post['pp_express_monthly_sandbox_signature'];
		} else {
			$data['pp_express_monthly_sandbox_signature'] = $this->config->get('pp_express_monthly_sandbox_signature');
		}

		$data['ipn_url'] = HTTPS_CATALOG . 'index.php?route=extension/payment/pp_express_monthly/ipn';

		if (isset($this->request->post['pp_express_monthly_test'])) {
			$data['pp_express_monthly_test'] = $this->request->post['pp_express_monthly_test'];
		} else {
			$data['pp_express_monthly_test'] = $this->config->get('pp_express_monthly_test');
		}

		if (isset($this->request->post['pp_express_monthly_debug'])) {
			$data['pp_express_monthly_debug'] = $this->request->post['pp_express_monthly_debug'];
		} else {
			$data['pp_express_monthly_debug'] = $this->config->get('pp_express_monthly_debug');
		}

		if (isset($this->request->post['pp_express_monthly_currency'])) {
			$data['pp_express_monthly_currency'] = $this->request->post['pp_express_monthly_currency'];
		} else {
			$data['pp_express_monthly_currency'] = $this->config->get('pp_express_monthly_currency');
		}

		$this->load->model('extension/payment/pp_express_monthly');

		$data['currencies'] = $this->model_extension_payment_pp_express_monthly->getCurrencies();

		if (isset($this->request->post['pp_express_monthly_recurring_cancel'])) {
			$data['pp_express_monthly_recurring_cancel'] = $this->request->post['pp_express_monthly_recurring_cancel'];
		} else {
			$data['pp_express_monthly_recurring_cancel'] = $this->config->get('pp_express_monthly_recurring_cancel');
		}

		if (isset($this->request->post['pp_express_monthly_transaction'])) {
			$data['pp_express_monthly_transaction'] = $this->request->post['pp_express_monthly_transaction'];
		} else {
			$data['pp_express_monthly_transaction'] = $this->config->get('pp_express_monthly_transaction');
		}

		if (isset($this->request->post['pp_express_monthly_total'])) {
			$data['pp_express_monthly_total'] = $this->request->post['pp_express_monthly_total'];
		} else {
			$data['pp_express_monthly_total'] = $this->config->get('pp_express_monthly_total');
		}

		if (isset($this->request->post['pp_express_monthly_geo_zone_id'])) {
			$data['pp_express_monthly_geo_zone_id'] = $this->request->post['pp_express_monthly_geo_zone_id'];
		} else {
			$data['pp_express_monthly_geo_zone_id'] = $this->config->get('pp_express_monthly_geo_zone_id');
		}

		$this->load->model('localisation/geo_zone');

		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		if (isset($this->request->post['pp_express_monthly_status'])) {
			$data['pp_express_monthly_status'] = $this->request->post['pp_express_monthly_status'];
		} else {
			$data['pp_express_monthly_status'] = $this->config->get('pp_express_monthly_status');
		}

		if (isset($this->request->post['pp_express_monthly_sort_order'])) {
			$data['pp_express_monthly_sort_order'] = $this->request->post['pp_express_monthly_sort_order'];
		} else {
			$data['pp_express_monthly_sort_order'] = $this->config->get('pp_express_monthly_sort_order');
		}

		if (isset($this->request->post['pp_express_monthly_canceled_reversal_status_id'])) {
			$data['pp_express_monthly_canceled_reversal_status_id'] = $this->request->post['pp_express_monthly_canceled_reversal_status_id'];
		} else {
			$data['pp_express_monthly_canceled_reversal_status_id'] = $this->config->get('pp_express_monthly_canceled_reversal_status_id');
		}

		if (isset($this->request->post['pp_express_monthly_completed_status_id'])) {
			$data['pp_express_monthly_completed_status_id'] = $this->request->post['pp_express_monthly_completed_status_id'];
		} else {
			$data['pp_express_monthly_completed_status_id'] = $this->config->get('pp_express_monthly_completed_status_id');
		}

		if (isset($this->request->post['pp_express_monthly_denied_status_id'])) {
			$data['pp_express_monthly_denied_status_id'] = $this->request->post['pp_express_monthly_denied_status_id'];
		} else {
			$data['pp_express_monthly_denied_status_id'] = $this->config->get('pp_express_monthly_denied_status_id');
		}

		if (isset($this->request->post['pp_express_monthly_expired_status_id'])) {
			$data['pp_express_monthly_expired_status_id'] = $this->request->post['pp_express_monthly_expired_status_id'];
		} else {
			$data['pp_express_monthly_expired_status_id'] = $this->config->get('pp_express_monthly_expired_status_id');
		}

		if (isset($this->request->post['pp_express_monthly_failed_status_id'])) {
			$data['pp_express_monthly_failed_status_id'] = $this->request->post['pp_express_monthly_failed_status_id'];
		} else {
			$data['pp_express_monthly_failed_status_id'] = $this->config->get('pp_express_monthly_failed_status_id');
		}

		if (isset($this->request->post['pp_express_monthly_pending_status_id'])) {
			$data['pp_express_monthly_pending_status_id'] = $this->request->post['pp_express_monthly_pending_status_id'];
		} else {
			$data['pp_express_monthly_pending_status_id'] = $this->config->get('pp_express_monthly_pending_status_id');
		}

		if (isset($this->request->post['pp_express_monthly_processed_status_id'])) {
			$data['pp_express_monthly_processed_status_id'] = $this->request->post['pp_express_monthly_processed_status_id'];
		} else {
			$data['pp_express_monthly_processed_status_id'] = $this->config->get('pp_express_monthly_processed_status_id');
		}

		if (isset($this->request->post['pp_express_monthly_refunded_status_id'])) {
			$data['pp_express_monthly_refunded_status_id'] = $this->request->post['pp_express_monthly_refunded_status_id'];
		} else {
			$data['pp_express_monthly_refunded_status_id'] = $this->config->get('pp_express_monthly_refunded_status_id');
		}

		if (isset($this->request->post['pp_express_monthly_reversed_status_id'])) {
			$data['pp_express_monthly_reversed_status_id'] = $this->request->post['pp_express_monthly_reversed_status_id'];
		} else {
			$data['pp_express_monthly_reversed_status_id'] = $this->config->get('pp_express_monthly_reversed_status_id');
		}

		if (isset($this->request->post['pp_express_monthly_voided_status_id'])) {
			$data['pp_express_monthly_voided_status_id'] = $this->request->post['pp_express_monthly_voided_status_id'];
		} else {
			$data['pp_express_monthly_voided_status_id'] = $this->config->get('pp_express_monthly_voided_status_id');
		}

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		if (isset($this->request->post['pp_express_monthly_allow_note'])) {
			$data['pp_express_monthly_allow_note'] = $this->request->post['pp_express_monthly_allow_note'];
		} else {
			$data['pp_express_monthly_allow_note'] = $this->config->get('pp_express_monthly_allow_note');
		}

		if (isset($this->request->post['pp_express_monthly_colour'])) {
			$data['pp_express_monthly_colour'] = str_replace('#', '', $this->request->post['pp_express_monthly_colour']);
		} else {
			$data['pp_express_monthly_colour'] = $this->config->get('pp_express_monthly_colour');
		}

		if (isset($this->request->post['pp_express_monthly_logo'])) {
			$data['pp_express_monthly_logo'] = $this->request->post['pp_express_monthly_logo'];
		} else {
			$data['pp_express_monthly_logo'] = $this->config->get('pp_express_monthly_logo');
		}

		$this->load->model('tool/image');

		if (isset($this->request->post['pp_express_monthly_logo']) && is_file(DIR_IMAGE . $this->request->post['pp_express_monthly_logo'])) {
			$data['thumb'] = $this->model_tool_image->resize($this->request->post['pp_express_monthly_logo'], 750, 90);
		} elseif (is_file(DIR_IMAGE . $this->config->get('pp_express_monthly_logo'))) {
			$data['thumb'] = $this->model_tool_image->resize($this->config->get('pp_express_monthly_logo'), 750, 90);
		} else {
			$data['thumb'] = $this->model_tool_image->resize('no_image.png', 750, 90);
		}

		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 750, 90);

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/payment/pp_express_monthly', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/payment/pp_express_monthly')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ($this->request->post['pp_express_monthly_test']) {
			if (!$this->request->post['pp_express_monthly_sandbox_username']) {
				$this->error['sandbox_username'] = $this->language->get('error_sandbox_username');
			}

			if (!$this->request->post['pp_express_monthly_sandbox_password']) {
				$this->error['sandbox_password'] = $this->language->get('error_sandbox_password');
			}

			if (!$this->request->post['pp_express_monthly_sandbox_signature']) {
				$this->error['sandbox_signature'] = $this->language->get('error_sandbox_signature');
			}
		} else {
			if (!$this->request->post['pp_express_monthly_username']) {
				$this->error['username'] = $this->language->get('error_username');
			}

			if (!$this->request->post['pp_express_monthly_password']) {
				$this->error['password'] = $this->language->get('error_password');
			}

			if (!$this->request->post['pp_express_monthly_signature']) {
				$this->error['signature'] = $this->language->get('error_signature');
			}
		}

		return !$this->error;
	}

	public function install() {
		$this->load->model('extension/payment/pp_express_monthly');

		$this->model_extension_payment_pp_express_monthly->install();
	}

	public function uninstall() {
		$this->load->model('extension/payment/pp_express_monthly');

		$this->model_extension_payment_pp_express_monthly->uninstall();
	}

	public function live() {
		if (isset($this->request->get['merchantId'])) {
			$this->load->language('extension/payment/pp_express_monthly');

			$this->load->model('extension/payment/pp_express_monthly');
			$this->load->model('setting/setting');

			$token = $this->model_extension_payment_pp_express_monthly->getTokens('live');

			if (isset($token->access_token)) {
				$user_info = $this->model_extension_payment_pp_express_monthly->getUserInfo($this->request->get['merchantId'], 'live', $token->access_token);
			} else {
				$this->session->data['error_api'] = $this->language->get('error_api');
			}

			if (isset($user_info->api_user_name)) {
				$this->model_setting_setting->editSettingValue('pp_express_monthly', 'pp_express_monthly_username', $user_info->api_user_name);
				$this->model_setting_setting->editSettingValue('pp_express_monthly', 'pp_express_monthly_password', $user_info->api_password);
				$this->model_setting_setting->editSettingValue('pp_express_monthly', 'pp_express_monthly_signature', $user_info->signature);
			} else {
				$this->session->data['error_api'] = $this->language->get('error_api');
			}
		}

		$this->response->redirect($this->url->link('extension/payment/pp_express_monthly', 'token=' . $this->session->data['token'], true));
	}

	public function sandbox() {

		if (isset($this->request->get['merchantId'])) {
			$this->load->language('extension/payment/pp_express_monthly');

			$this->load->model('extension/payment/pp_express_monthly');
			$this->load->model('setting/setting');

			$token = $this->model_extension_payment_pp_express_monthly->getTokens('sandbox');

			if (isset($token->access_token)) {
				$user_info = $this->model_extension_payment_pp_express_monthly->getUserInfo($this->request->get['merchantId'], 'sandbox', $token->access_token);
			} else {
				$this->session->data['error_api'] = $this->language->get('error_api_sandbox');
			}

			if (isset($user_info->api_user_name)) {
				$this->model_setting_setting->editSettingValue('pp_express_monthly', 'pp_express_monthly_sandbox_username', $user_info->api_user_name);
				$this->model_setting_setting->editSettingValue('pp_express_monthly', 'pp_express_monthly_sandbox_password', $user_info->api_password);
				$this->model_setting_setting->editSettingValue('pp_express_monthly', 'pp_express_monthly_sandbox_signature', $user_info->signature);
			} else {
				$this->session->data['error_api'] = $this->language->get('error_api_sandbox');
			}
		}
		$this->response->redirect($this->url->link('extension/payment/pp_express_monthly', 'token=' . $this->session->data['token'], true));
	}
}
