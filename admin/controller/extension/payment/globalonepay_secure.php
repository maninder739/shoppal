<?php
class ControllerExtensionPaymentGlobalonepaySecure extends Controller {
	private $error = array();

	public function index(){

		$this->load->language('extension/payment/globalonepay_secure');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/setting');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {
			$this->model_setting_setting->editSetting('globalonepay_secure', $this->request->post);				
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=payment', true));
		}

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_payment'] = $this->language->get('text_payment');
		$data['text_extension_name'] = $this->language->get('text_extension_name');
		$data['text_success'] = $this->language->get('text_success');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_true'] = $this->language->get('text_true');
		$data['text_false'] = $this->language->get('text_false');
		$data['currency_eur'] = $this->language->get('currency_eur');
		$data['currency_gbp'] = $this->language->get('currency_gbp');
		$data['currency_usd'] = $this->language->get('currency_usd');
		$data['currency_sek'] = $this->language->get('currency_sek');
		$data['currency_dkk'] = $this->language->get('currency_dkk');
		$data['currency_nok'] = $this->language->get('currency_nok');
		$data['currency_aud'] = $this->language->get('currency_aud');
		$data['currency_cad'] = $this->language->get('currency_cad');
		$data['tab_account'] = $this->language->get('tab_account');
		$data['tab_order_status'] = $this->language->get('tab_order_status');
		$data['entry_gateway'] = $this->language->get('entry_gateway');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_test'] = $this->language->get('entry_test');
		$data['entry_currency1'] = $this->language->get('entry_currency1');
		$data['entry_currency1_tip'] = $this->language->get('entry_currency1_tip');
		$data['entry_terminal1'] = $this->language->get('entry_terminal1');
		$data['entry_terminal1_tip'] = $this->language->get('entry_terminal1_tip');
		$data['entry_secret1'] = $this->language->get('entry_secret1');
		$data['entry_secret1_tip'] = $this->language->get('entry_secret1_tip');
		$data['entry_currency2'] = $this->language->get('entry_currency2');
		$data['entry_terminal2'] = $this->language->get('entry_terminal2');
		$data['entry_secret2'] = $this->language->get('entry_secret2');
		$data['entry_currency3'] = $this->language->get('entry_currency3');
		$data['entry_terminal3'] = $this->language->get('entry_terminal3');
		$data['entry_secret3'] = $this->language->get('entry_secret3');
		$data['entry_order_status'] = $this->language->get('entry_order_status');
		$data['entry_send_receipt'] = $this->language->get('entry_send_receipt');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['error_permission'] = $this->language->get('error_permission');
		$data['error_terminal1'] = $this->language->get('error_terminal1');
		$data['error_secret1'] = $this->language->get('error_secret1');
		$data['error_secret1_tip'] = $this->language->get('error_secret1_tip');
		$data['entry_status_success'] = $this->language->get('entry_status_success');
		$data['entry_status_declined'] = $this->language->get('entry_status_declined');
		$data['entry_status_failed'] = $this->language->get('entry_status_failed');
		$data['entry_status_pendingAutoship'] = $this->language->get('entry_status_pendingAutoship');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) $data['error_warning'] = $this->error['warning'];
		else $data['error_warning'] = '';

 		if (isset($this->error['gateway'])) $data['error_gateway'] = $this->error['gateway'];
		else $data['error_gateway'] = '';

  		if (isset($this->error['terminal1'])) $data['error_terminal1'] = $this->error['terminal1'];
		else $data['error_terminal1'] = '';

 		if (isset($this->error['secret1'])) $data['error_secret1'] = $this->error['secret1'];
		else $data['error_secret1'] = '';

  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
	       		'href'      => HTTPS_SERVER . 'index.php?route=common/home&token=' . $this->session->data['token'],
	       		'text'      => $this->language->get('text_home'),
	      		'separator' => FALSE
   		);

   		$data['breadcrumbs'][] = array(
	       		'href'      => HTTPS_SERVER . 'index.php?route=extension/payment&token=' . $this->session->data['token'],
	       		'text'      => $this->language->get('text_payment'),
	      		'separator' => ' :: '
   		);

   		$data['breadcrumbs'][] = array(
	       		'href'      => HTTPS_SERVER . 'index.php?route=extension/payment/globalonepay_secure&token=' . $this->session->data['token'],
	       		'text'      => $this->language->get('heading_title'),
	      		'separator' => ' :: '
   		);

		$data['action'] = $this->url->link('extension/payment/globalonepay_secure', 'token=' . $this->session->data['token'], true);
		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=payment', true);
		
		if (isset($this->request->post['globalonepay_secure_gateway'])) $data['globalonepay_secure_gateway'] = $this->request->post['globalonepay_secure_gateway'];
		else $data['globalonepay_secure_gateway'] = $this->config->get('globalonepay_secure_gateway');

		if (isset($this->request->post['globalonepay_secure_currency1'])) $data['globalonepay_secure_currency1'] = $this->request->post['globalonepay_secure_currency1'];
		else $data['globalonepay_secure_currency1'] = $this->config->get('globalonepay_secure_currency1');

		if (isset($this->request->post['globalonepay_secure_terminal1'])) $data['globalonepay_secure_terminal1'] = $this->request->post['globalonepay_secure_terminal1'];
		else $data['globalonepay_secure_terminal1'] = $this->config->get('globalonepay_secure_terminal1');

		if (isset($this->request->post['globalonepay_secure_secret1'])) $data['globalonepay_secure_secret1'] = $this->request->post['globalonepay_secure_secret1'];
		else $data['globalonepay_secure_secret1'] = $this->config->get('globalonepay_secure_secret1');

		if (isset($this->request->post['globalonepay_secure_currency2'])) $data['globalonepay_secure_currency2'] = $this->request->post['globalonepay_secure_currency2'];
		else $data['globalonepay_secure_currency2'] = $this->config->get('globalonepay_secure_currency2');

		if (isset($this->request->post['globalonepay_secure_terminal2'])) $data['globalonepay_secure_terminal2'] = $this->request->post['globalonepay_secure_terminal2'];
		else $data['globalonepay_secure_terminal2'] = $this->config->get('globalonepay_secure_terminal2');
		
		if (isset($this->request->post['globalonepay_secure_secret2'])) $data['globalonepay_secure_secret2'] = $this->request->post['globalonepay_secure_secret2'];
		else $data['globalonepay_secure_secret2'] = $this->config->get('globalonepay_secure_secret2');

		if (isset($this->request->post['globalonepay_secure_currency3'])) $data['globalonepay_secure_currency3'] = $this->request->post['globalonepay_secure_currency3'];
		else $data['globalonepay_secure_currency3'] = $this->config->get('globalonepay_secure_currency3');

		if (isset($this->request->post['globalonepay_secure_terminal3'])) $data['globalonepay_secure_terminal3'] = $this->request->post['globalonepay_secure_terminal3'];
		else $data['globalonepay_secure_terminal3'] = $this->config->get('globalonepay_secure_terminal3');

		if (isset($this->request->post['globalonepay_secure_secret3'])) $data['globalonepay_secure_secret3'] = $this->request->post['globalonepay_secure_secret3'];
		else $data['globalonepay_secure_secret3'] = $this->config->get('globalonepay_secure_secret3');

		if (isset($this->request->post['globalonepay_secure_test'])) $data['globalonepay_secure_test'] = $this->request->post['globalonepay_secure_test'];
		else $data['globalonepay_secure_test'] = $this->config->get('globalonepay_secure_test');

		if (isset($this->request->post['globalonepay_secure_status_success'])) $data['globalonepay_secure_status_success'] = $this->request->post['globalonepay_secure_status_success'];
		else $data['globalonepay_secure_status_success'] = $this->config->get('globalonepay_secure_status_success');

		if (isset($this->request->post['globalonepay_secure_status_declined'])) $data['globalonepay_secure_status_declined'] = $this->request->post['globalonepay_secure_status_declined'];
		else $data['globalonepay_secure_status_declined'] = $this->config->get('globalonepay_secure_status_declined');

		if (isset($this->request->post['globalonepay_secure_status_failed'])) $data['globalonepay_secure_status_failed'] = $this->request->post['globalonepay_secure_status_failed'];
		else $data['globalonepay_secure_status_failed'] = $this->config->get('globalonepay_secure_status_failed');

		if (isset($this->request->post['globalonepay_secure_status_pendingAutoship'])) $data['globalonepay_secure_status_pendingAutoship'] = $this->request->post['globalonepay_secure_status_pendingAutoship'];
		else $data['globalonepay_secure_status_pendingAutoship'] = $this->config->get('globalonepay_secure_status_pendingAutoship');

		$this->load->model('localisation/order_status');
		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		if (isset($this->request->post['globalonepay_secure_status'])) $data['globalonepay_secure_status'] = $this->request->post['globalonepay_secure_status'];
		else $data['gglobalonepay_secure_status'] = $this->config->get('globalonepay_secure_status');

		if (isset($this->request->post['globalonepay_secure_send_receipt'])) $data['globalonepay_secure_send_receipt'] = $this->request->post['globalonepay_secure_send_receipt'];
		else $data['globalonepay_secure_send_receipt'] = $this->config->get('globalonepay_secure_send_receipt');

		if (isset($this->request->post['globalonepay_secure_sort_order'])) $data['globalonepay_secure_sort_order'] = $this->request->post['globalonepay_secure_sort_order'];
		else $data['globalonepay_secure_sort_order'] = $this->config->get('globalonepay_secure_sort_order');

		if (isset($this->request->post['globalonepay_secure_status'])) $data['globalonepay_secure_status'] = $this->request->post['globalonepay_secure_status'];
		else $data['globalonepay_secure_status'] = $this->config->get('globalonepay_secure_status');

		if (isset($this->request->post['globalonepay_secure_status_pendingAutoship'])) $data['globalonepay_secure_status_pendingAutoship'] = $this->request->post['globalonepay_secure_status_pendingAutoship'];
		else $data['globalonepay_secure_status_pendingAutoship'] = $this->config->get('globalonepay_secure_status_pendingAutoship');
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('extension/payment/globalonepay_secure', $data));

	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'extension/payment/globalonepay_secure')) $this->error['warning'] = $this->language->get('error_permission');
		
		if (!$this->error) return TRUE;
		else return FALSE;
	}

	public function install() {
		$this->load->model('extension/payment/globalonepay_secure');
		$this->model_extension_payment_globalonepay_secure->install();
	}

	public function uninstall() {
		//$this->load->model('extension/payment/globalonesecure');
		//$this->model_extension_payment_globalonesecure->uninstall();
	}
}
?>