<?php
//==============================================================================
// Individual Shipping v302.1
// 
// Author: Clear Thinking, LLC
// E-mail: johnathan@getclearthinking.com
// Website: http://www.getclearthinking.com
// 
// All code within this file is copyright Clear Thinking, LLC.
// You may not copy or reuse code within this file without written permission.
//==============================================================================

class ControllerExtensionShippingIndividualShipping extends Controller {
	private $type = 'shipping';
	private $name = 'individual_shipping';
	
	public function index() {
		$data = array(
			'type'			=> $this->type,
			'name'			=> $this->name,
			'autobackup'	=> false,
			'save_type'		=> 'keepediting',
			'permission'	=> $this->hasPermission('modify'),
		);
		
		$this->loadSettings($data);
		
		// extension-specific
		$table_query = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "order` WHERE Field = 'shipping_method'");
		if (strtoupper($table_query->row['Type']) != 'TEXT') {
			$this->db->query("ALTER TABLE `" . DB_PREFIX . "order` MODIFY `shipping_method` TEXT NOT NULL");
		}
		
		$table_query = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "order_total` WHERE Field = 'title'");
		if (strtoupper($table_query->row['Type']) != 'TEXT') {
			$this->db->query("ALTER TABLE `" . DB_PREFIX . "order_total` MODIFY `title` TEXT NOT NULL");
		}
		
		//------------------------------------------------------------------------------
		// Data Arrays
		//------------------------------------------------------------------------------
		$data['language_array'] = array($this->config->get('config_language') => '');
		$data['language_flags'] = array();
		$this->load->model('localisation/language');
		foreach ($this->model_localisation_language->getLanguages() as $language) {
			$data['language_array'][$language['code']] = $language['name'];
			$data['language_flags'][$language['code']] = (version_compare(VERSION, '2.2', '<')) ? 'view/image/flags/' . $language['image'] : 'language/' . $language['code'] . '/' . $language['code'] . '.png';
		}
		
		foreach (array('shipping', 'payment') as $extension_type) {
			$data[$extension_type . '_extension_array'] = array();
			$extension_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "extension WHERE `type` = '" . $this->db->escape($extension_type) . "' ORDER BY `code` ASC");
			foreach ($extension_query->rows as $extension) {
				if ($extension['code'] == $this->name) continue;
				$this->load->language((version_compare(VERSION, '2.3', '<') ? '' : 'extension/') . $extension_type . '/' . $extension['code']);
				$data[$extension_type . '_extension_array'][] = array('code' => $extension['code'], $extension_type . '_extension_id' => $extension['code'], 'name' => $this->language->get('heading_title'));
			}
		}
		
		//------------------------------------------------------------------------------
		// Extensions Settings
		//------------------------------------------------------------------------------
		$data['settings'] = array();
		
		$data['settings'][] = array(
			'key'		=> 'extension_settings',
			'type'		=> 'heading',
		);
		$data['settings'][] = array(
			'key'		=> 'status',
			'type'		=> 'select',
			'options'	=> array(0 => $data['text_disabled'], 1 => $data['text_enabled']),
			'default'	=> 0,
		);
		$data['settings'][] = array(
			'key'		=> 'heading',
			'type'		=> 'multilingual_text',
			'default'	=> $data['heading_title'],
		);
		
		$shipping_extensions = array();
		foreach ($data['shipping_extension_array'] as $shipping_extension) {
			if ($shipping_extension['code'] != $this->name) {
				$shipping_extensions[$shipping_extension['code']] = $shipping_extension['name'];
			}
		}
		$data['settings'][] = array(
			'key'		=> 'eligible_methods',
			'type'		=> 'checkboxes',
			'options'	=> $shipping_extensions,
		);
		
		$data['product_columns'] = array();
		$product_column_query = $this->db->query("DESCRIBE " . DB_PREFIX . "product");
		foreach ($product_column_query->rows as $column) {
			$data['product_columns'][$column['Field']] = $column['Field'];
		}
		asort($data['product_columns']);
		$data['settings'][] = array(
			'key'		=> 'product_grouping',
			'type'		=> 'select',
			'options'	=> $data['product_columns'],
			'default'	=> 'product_id',
		);
		
		$data['settings'][] = array(
			'key'		=> 'show_products',
			'type'		=> 'select',
			'options'	=> array(0 => $data['text_no'], 1 => $data['text_yes']),
			'default'	=> 0,
		);
		$data['settings'][] = array(
			'type'		=> 'html',
			'content'	=> '
				<input type="text" class="short form-control" name="image_width" value="' . (!empty($data['saved']['image_width']) ? $data['saved']['image_width'] : '50') . '" />
				x
				<input type="text" class="short form-control" name="image_height" value="' . (!empty($data['saved']['image_height']) ? $data['saved']['image_height'] : '50') . '" />
			',
			'title'		=> $data['entry_image_size'],
		);
		
		//------------------------------------------------------------------------------
		// Front-End Text
		//------------------------------------------------------------------------------
		$data['settings'][] = array(
			'key'		=> 'front_end_text',
			'type'		=> 'heading',
		);
		$data['settings'][] = array(
			'key'		=> 'group_text',
			'type'		=> 'multilingual_text',
			'default'	=> '[group] Products',
		);
		$data['settings'][] = array(
			'key'		=> 'not_required_text',
			'type'		=> 'multilingual_text',
			'default'	=> 'No shipping is required',
		);
		$data['settings'][] = array(
			'key'		=> 'no_rates_text',
			'type'		=> 'multilingual_text',
			'default'	=> 'No eligible shipping rates',
		);
		$data['settings'][] = array(
			'key'		=> 'total_shipping_text',
			'type'		=> 'multilingual_text',
			'default'	=> 'Total Shipping:',
		);
		$data['settings'][] = array(
			'key'		=> 'line_item_text',
			'type'		=> 'multilingual_text',
			'default'	=> '<span style="font-size: 11px; font-weight: normal">- [name]: [method] ([cost])</span>',
		);
		
		//------------------------------------------------------------------------------
		// Setting Overrides
		//------------------------------------------------------------------------------
		$data['settings'][] = array(
			'key'		=> 'setting_overrides',
			'type'		=> 'heading',
		);
		$data['settings'][] = array(
			'type'		=> 'html',
			'content'	=> '<div class="text-info" style="padding-bottom: 25px">' . $data['help_setting_overrides'] . '</div>',
		);
		
		$setting_overrides = array('' => ' ');
		if (version_compare(VERSION, '3.0', '<')) {
			$setting_override_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE `code` IN (SELECT `code` FROM `" . DB_PREFIX . "extension` WHERE `type` = 'shipping') AND `code` != '" . $this->db->escape($this->name) . "' ORDER BY `code`, `key` ASC");
		} else {
			$setting_override_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE `code` LIKE 'shipping_%' AND `code` != 'shipping_" . $this->db->escape($this->name) . "' ORDER BY `code`, `key` ASC");	
		}
		
		$data['setting_override_array'] = $setting_override_query->rows;
		$current_group = '';
		
		foreach ($data['setting_override_array'] as $setting_override) {
			$explode = explode('_', $setting_override['code']);
			array_shift($explode);
			$setting_override['code'] = implode('_', $explode);
			
			if ($setting_override['code'] != $current_group) {
				$current_group = $setting_override['code'];
				$setting_overrides[$setting_override['code']] = '';
			}
			
			$setting_overrides[$setting_override['key']] = $setting_override['key'] . ' (' . (strlen($setting_override['value']) > 20 ? substr($setting_override['value'], 0, 20) . '...' : $setting_override['value']) . ')';
		}
		
		$data['settings'][] = array(
			'key'		=> 'setting_override',
			'type'		=> 'table_start',
			'columns'	=> array('action', 'setting', 'override_value', 'product_groups'),
		);
		
		$table = 'setting_override';
		$sortby = 'setting';
		foreach ($this->getTableRowNumbers($data, $table, $sortby) as $num => $rules) {
			$prefix = $table . '_' . $num . '_';
			$data['settings'][] = array(
				'type'		=> 'row_start',
			);
			$data['settings'][] = array(
				'key'		=> 'delete',
				'type'		=> 'button',
			);
			$data['settings'][] = array(
				'type'		=> 'column'
			);
			$data['settings'][] = array(
				'key'		=> $prefix . 'setting',
				'type'		=> 'select',
				'options'	=> $setting_overrides,
			);
			$data['settings'][] = array(
				'type'		=> 'column'
			);
			$data['settings'][] = array(
				'key'		=> $prefix . 'override_value',
				'type'		=> 'textarea',
			);
			$data['settings'][] = array(
				'type'		=> 'column'
			);
			$data['settings'][] = array(
				'key'		=> $prefix . 'product_groups',
				'type'		=> 'textarea',
			);
			$data['settings'][] = array(
				'type'		=> 'row_end'
			);
		}
		
		$data['settings'][] = array(
			'type'		=> 'table_end',
			'buttons'	=> 'add_row',
			'text'		=> 'button_add_override',
		);
		
		//------------------------------------------------------------------------------
		// end settings
		//------------------------------------------------------------------------------
		
		$this->document->setTitle($data['heading_title']);
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$template_file = DIR_TEMPLATE . 'extension/' . $this->type . '/' . $this->name . '.twig';
		
		if (is_file($template_file)) {
			extract($data);
			
			ob_start();
			require(class_exists('VQMod') ? VQMod::modCheck(modification($template_file)) : modification($template_file));
			$output = ob_get_clean();
			
			if (version_compare(VERSION, '3.0', '>=')) {
				$output = str_replace('&token=', '&user_token=', $output);
			}
			
			echo $output;
		} else {
			echo 'Error loading template file';
		}
	}
	
	//==============================================================================
	// Helper functions
	//==============================================================================
	private function hasPermission($permission) {
		return ($this->user->hasPermission($permission, $this->type . '/' . $this->name) || $this->user->hasPermission($permission, 'extension/' . $this->type . '/' . $this->name));
	}
	
	private function loadLanguage($path) {
		$_ = array();
		$language = array();
		$admin_language = (version_compare(VERSION, '2.2', '<')) ? $this->db->query("SELECT * FROM " . DB_PREFIX . "language WHERE `code` = '" . $this->db->escape($this->config->get('config_admin_language')) . "'")->row['directory'] : $this->config->get('config_admin_language');
		foreach (array('english', 'en-gb', $admin_language) as $directory) {
			$file = DIR_LANGUAGE . $directory . '/' . $directory . '.php';
			if (file_exists($file)) require($file);
			$file = DIR_LANGUAGE . $directory . '/default.php';
			if (file_exists($file)) require($file);
			$file = DIR_LANGUAGE . $directory . '/' . $path . '.php';
			if (file_exists($file)) require($file);
			$file = DIR_LANGUAGE . $directory . '/extension/' . $path . '.php';
			if (file_exists($file)) require($file);
			$language = array_merge($language, $_);
		}
		return $language;
	}
	
	private function getTableRowNumbers(&$data, $table, $sorting) {
		$groups = array();
		$rules = array();
		
		foreach ($data['saved'] as $key => $setting) {
			if (preg_match('/' . $table . '_(\d+)_' . $sorting . '/', $key, $matches)) {
				$groups[$setting][] = $matches[1];
			}
			if (preg_match('/' . $table . '_(\d+)_rule_(\d+)_type/', $key, $matches)) {
				$rules[$matches[1]][] = $matches[2];
			}
		}
		
		if (empty($groups)) $groups = array('' => array('1'));
		ksort($groups, defined('SORT_NATURAL') ? SORT_NATURAL : SORT_REGULAR);
		
		foreach ($rules as $key => $rule) {
			ksort($rules[$key], defined('SORT_NATURAL') ? SORT_NATURAL : SORT_REGULAR);
		}
		
		$data['used_rows'][$table] = array();
		$rows = array();
		foreach ($groups as $group) {
			foreach ($group as $num) {
				$data['used_rows'][preg_replace('/module_(\d+)_/', '', $table)][] = $num;
				$rows[$num] = (empty($rules[$num])) ? array() : $rules[$num];
			}
		}
		sort($data['used_rows'][$table]);
		
		return $rows;
	}
	
	//==============================================================================
	// Setting functions
	//==============================================================================
	private $encryption_key = '';
	
	public function loadSettings(&$data) {
		$backup_type = (empty($data)) ? 'manual' : 'auto';
		if ($backup_type == 'manual' && !$this->hasPermission('modify')) {
			return;
		}
		
		$this->cache->delete($this->name);
		unset($this->session->data[$this->name]);
		$code = (version_compare(VERSION, '3.0', '<') ? '' : $this->type . '_') . $this->name;
		
		// Set exit URL
		$data['token'] = $this->session->data[version_compare(VERSION, '3.0', '<') ? 'token' : 'user_token'];
		$data['exit'] = $this->url->link((version_compare(VERSION, '3.0', '<') ? 'extension' : 'marketplace') . '/' . (version_compare(VERSION, '2.3', '<') ? '' : 'extension&type=') . $this->type . '&token=' . $data['token'], '', 'SSL');
		
		// Load saved settings
		$data['saved'] = array();
		$settings_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE `code` = '" . $this->db->escape($code) . "' ORDER BY `key` ASC");
		
		foreach ($settings_query->rows as $setting) {
			$key = str_replace($code . '_', '', $setting['key']);
			$value = $setting['value'];
			if ($setting['serialized']) {
				$value = (version_compare(VERSION, '2.1', '<')) ? unserialize($setting['value']) : json_decode($setting['value'], true);
			}
			
			$data['saved'][$key] = $value;
			
			if (is_array($value)) {
				foreach ($value as $num => $value_array) {
					foreach ($value_array as $k => $v) {
						$data['saved'][$key . '_' . $num . '_' . $k] = $v;
					}
				}
			}
		}
		
		// Load language and run standard checks
		$data = array_merge($data, $this->loadLanguage($this->type . '/' . $this->name));
		
		if (ini_get('max_input_vars') && ((ini_get('max_input_vars') - count($data['saved'])) < 50)) {
			$data['warning'] = $data['standard_max_input_vars'];
		}
		
		// Modify files according to OpenCart version
		if ($this->type == 'total' && version_compare(VERSION, '2.2', '<')) {
			file_put_contents(DIR_CATALOG . 'model/' . $this->type . '/' . $this->name . '.php', str_replace('public function getTotal($total) {', 'public function getTotal(&$total_data, &$order_total, &$taxes) {' . "\n\t\t" . '$total = array("totals" => &$total_data, "total" => &$order_total, "taxes" => &$taxes);', file_get_contents(DIR_CATALOG . 'model/' . $this->type . '/' . $this->name . '.php')));
		}
		
		if (version_compare(VERSION, '2.3', '>=')) {
			$filepaths = array(
				DIR_APPLICATION . 'controller/' . $this->type . '/' . $this->name . '.php',
				DIR_CATALOG . 'controller/' . $this->type . '/' . $this->name . '.php',
				DIR_CATALOG . 'model/' . $this->type . '/' . $this->name . '.php',
			);
			foreach ($filepaths as $filepath) {
				if (file_exists($filepath)) {
					rename($filepath, str_replace('.php', '.php-OLD', $filepath));
				}
			}
		}
		
		// Set save type and skip auto-backup if not needed
		if (!empty($data['saved']['autosave'])) {
			$data['save_type'] = 'auto';
		}
		
		if ($backup_type == 'auto' && empty($data['autobackup'])) {
			return;
		}
		
		// Create settings auto-backup file
		$manual_filepath = DIR_LOGS . $this->name . $this->encryption_key . '.backup';
		$auto_filepath = DIR_LOGS . $this->name . $this->encryption_key . '.autobackup';
		$filepath = ($backup_type == 'auto') ? $auto_filepath : $manual_filepath;
		if (file_exists($filepath)) unlink($filepath);
		
		file_put_contents($filepath, 'SETTING	NUMBER	SUB-SETTING	SUB-NUMBER	SUB-SUB-SETTING	VALUE' . "\n", FILE_APPEND|LOCK_EX);
		
		foreach ($data['saved'] as $key => $value) {
			if (is_array($value)) continue;
			
			$parts = explode('|', preg_replace(array('/_(\d+)_/', '/_(\d+)/'), array('|$1|', '|$1'), $key));
			
			$line = '';
			for ($i = 0; $i < 5; $i++) {
				$line .= (isset($parts[$i]) ? $parts[$i] : '') . "\t";
			}
			$line .= str_replace(array("\t", "\n"), array('    ', '\n'), $value) . "\n";
			
			file_put_contents($filepath, $line, FILE_APPEND|LOCK_EX);
		}
		
		$data['autobackup_time'] = date('Y-M-d @ g:i a');
		$data['backup_time'] = (file_exists($manual_filepath)) ? date('Y-M-d @ g:i a', filemtime($manual_filepath)) : '';
		
		if ($backup_type == 'manual') {
			echo $data['autobackup_time'];
		}
	}
	
	public function saveSettings() {
		if (!$this->hasPermission('modify')) {
			echo 'PermissionError';
			return;
		}
		
		$this->cache->delete($this->name);
		unset($this->session->data[$this->name]);
		$code = (version_compare(VERSION, '3.0', '<') ? '' : $this->type . '_') . $this->name;
		
		if ($this->request->get['saving'] == 'manual') {
			$this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE `code` = '" . $this->db->escape($code) . "' AND `key` != '" . $this->db->escape($this->name . '_module') . "'");
		}
		
		$module_id = 0;
		$modules = array();
		$module_instance = false;
		
		foreach ($this->request->post as $key => $value) {
			if (strpos($key, 'module_') === 0) {
				$parts = explode('_', $key, 3);
				$module_id = $parts[1];
				$modules[$parts[1]][$parts[2]] = $value;
				if ($parts[2] == 'module_id') $module_instance = true;
			} else {
				$key = (version_compare(VERSION, '3.0', '<') ? '' : $this->type . '_') . $this->name . '_' . $key;
				
				if ($this->request->get['saving'] == 'auto') {
					$this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE `code` = '" . $this->db->escape($code) . "' AND `key` = '" . $this->db->escape($key) . "'");
				}
				
				$this->db->query("
					INSERT INTO " . DB_PREFIX . "setting SET
					`store_id` = 0,
					`code` = '" . $this->db->escape($code) . "',
					`key` = '" . $this->db->escape($key) . "',
					`value` = '" . $this->db->escape(stripslashes(is_array($value) ? implode(';', $value) : $value)) . "',
					`serialized` = 0
				");
			}
		}
		
		foreach ($modules as $module_id => $module) {
			if (!$module_id) {
				$this->db->query("
					INSERT INTO " . DB_PREFIX . "module SET
					`name` = '" . $this->db->escape($module['name']) . "',
					`code` = '" . $this->db->escape($this->name) . "',
					`setting` = ''
				");
				$module_id = $this->db->getLastId();
				$module['module_id'] = $module_id;
			}
			$module_settings = (version_compare(VERSION, '2.1', '<')) ? serialize($module) : json_encode($module);
			$this->db->query("
				UPDATE " . DB_PREFIX . "module SET
				`name` = '" . $this->db->escape($module['name']) . "',
				`code` = '" . $this->db->escape($this->name) . "',
				`setting` = '" . $this->db->escape($module_settings) . "'
				WHERE module_id = " . (int)$module_id . "
			");
		}
	}
	
	public function deleteSetting() {
		if (!$this->hasPermission('modify')) {
			echo 'PermissionError';
			return;
		}
		$this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE `code` = '" . $this->db->escape($this->name) . "' AND `key` = '" . $this->db->escape($this->name . '_' . str_replace('[]', '', $this->request->get['setting'])) . "'");
	}
}
?>