<?php
class ControllerExtensionAnalyticsFbMarketing extends Controller {
	private $version = '2.1.2';
	private $setting_keys_array = array('status', 'version', 'pixel_id', 'product_catalog_id', 'events', 'advanced_matching', 'values_vat_inc', 'manual_only_mode', 'pid');
	private $default_events = array(
			'ViewContent',
			'Search',
			'AddToCart',
			'AddToWishlist',
			'InitiateCheckout',
			'AddPaymentInfo',
			'Purchase',
			//'Lead',
			'CompleteRegistration',
			'PageView'
		);
	private $adv_match_types = array(
			'em' 	=> 'Email',
			'fn'	=> 'First Name',
			'ln'	=> 'Last Name',
			'ph' 	=> 'Phone',
			'ct'	=> 'City',
			'st' 	=> 'State',
			'zp'	=> 'Zip'
		);
	private $prod_id_options = array(
			'product_id'	=> 'Product ID',
			'model'	=> 'Model',
			'sku'	=> 'SKU',
			'upc'	=> 'UPC',
			'ean' 	=> 'EAN',
			'jan'	=> 'JAN',
			'isbn' 	=> 'ISBN',
			'mpn' 	=> 'MPN',
		);
	
	private $error = array();
		
	public function index() {		
		
		$path = $this->path();
		$prefix = $this->prefix();
		$folder = $this->folder();
				
		$this->load->language($path);
		$data['permission'] = $this->user->hasPermission('modify', $path);
		$data['controller_path'] = $path;

		$this->document->setTitle($this->language->get('heading_title'));
		$this->document->addScript("https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js");
		$this->document->addStyle("https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css");
		
		$this->load->model('setting/setting');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting($prefix . 'fb_marketing', $this->request->post, $this->request->get['store_id']);

			$this->session->data['success'] = $this->language->get('text_success');
			
			if (version_compare(VERSION, '2.3.0.0', '>=')) {
				$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=analytics', true));
			} else {
				$this->response->redirect($this->url->link('extension/analytics', 'token=' . $this->session->data['token'], 'SSL'));
			}
		}
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_signup'] = $this->language->get('text_signup');
		
		$data['text_pixel_configuration'] = $this->language->get('text_pixel_configuration');
		$data['text_advanced_matching_data'] = $this->language->get('text_advanced_matching_data');
		$data['text_marketing_api'] = $this->language->get('text_marketing_api');
		$data['text_about'] = $this->language->get('text_about');
		
		$data['text_ajax_save'] = $this->language->get('text_ajax_save');
		
		$data['text_name'] = $this->language->get('text_name');
		$data['text_path'] = $this->language->get('text_path');
		$data['text_value'] = $this->language->get('text_value');
		$data['text_status'] = $this->language->get('text_status');
		$data['text_pro_version'] = 'PRO Version';
		
		$data['placeholder_path'] = $this->language->get('placeholder_path');
		$data['placeholder_value'] = $this->language->get('placeholder_value');
		
		$data['help_pixel_id'] = $this->language->get('help_pixel_id');
		$data['help_product_catalog_id'] = $this->language->get('help_product_catalog_id');
		$data['help_advanced_matching'] = $this->language->get('help_advanced_matching');
		$data['help_values_vat_inc'] = $this->language->get('help_values_vat_inc');
		$data['help_manual_only_mode'] = $this->language->get('help_manual_only_mode');
		$data['help_pid'] = $this->language->get('help_pid');
		
		$data['error_permission'] = $this->language->get('error_permission');
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['code'])) {
			$data['error_code'] = $this->error['code'];
		} else {
			$data['error_code'] = '';
		}

		$data['token'] = $this->session->data['token'];
		
		$data['breadcrumbs'] = array();
		
		if (version_compare(VERSION, '2.3.0.0', '>=')) {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true),
			);
		
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_extension'),
				'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=analytics', true),
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('extension/analytics/fb_marketing', 'token=' . $this->session->data['token'], true)
			);

			$data['action'] = $this->url->link('extension/analytics/fb_marketing', 'token=' . $this->session->data['token'], true);
			$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=analytics', true);
			
		} else {
		
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL'),
			);
		
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_extension'),
				'href' => $this->url->link('extension/analytics', 'token=' . $this->session->data['token'], 'SSL'),
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('analytics/fb_marketing', 'token=' . $this->session->data['token'], 'SSL')
			);
		
			$data['action'] = $this->url->link('analytics/fb_marketing', 'token=' . $this->session->data['token'], 'SSL');
			$data['cancel'] = $this->url->link('extension/analytics', 'token=' . $this->session->data['token'], 'SSL');
		}
		
		$data['version'] = $this->version;
		
		// Load Stores
		$this->load->model('setting/store');

		$stores = $this->model_setting_store->getStores();
		array_unshift($stores, array(
			'store_id'	=> 0,
			'name'		=> $this->config->get('config_name')
		));
	
		$data['stores'] = $stores;
		
		$data['events'] = $this->default_events;
		
		$data['adv_match_types'] = $this->adv_match_types;
		
		$data['pid_options'] =  $this->prod_id_options;
		
		foreach ($this->setting_keys_array as $setting) {
			$data['entry_' . $setting] = $this->language->get('entry_' . $setting);	
			$data[$prefix . 'fb_marketing_' . $setting] = array();
		}
		
		foreach($stores as $store) {
			foreach ($this->setting_keys_array as $setting) {
				if ($setting == 'events') {
					foreach($data['events'] as $event) {
						$key = strtolower($event);
						$_key = $prefix . 'fb_marketing_' . $setting;
						$event_data = $this->getEventData($store['store_id'], $key);
					
						if (isset($this->request->post[$_key][$store['store_id']][$key])) {
							$data[$_key][$store['store_id']][$key] = $this->request->post[$_key][$store['store_id']][$key];
						} elseif ($event_data) { 
							$data[$_key][$store['store_id']][$key] = $event_data;
						} else {
							$data[$_key][$store['store_id']][$key] = array();
						}
					}
				} else {
					$$setting = $this->getPixelData($store['store_id'], $setting);
					$_key = $prefix . 'fb_marketing_' . $setting;
					if (isset($this->request->post[$_key][$store['store_id']])) {
						$data[$_key][$store['store_id']] = $this->request->post[$_key][$store['store_id']];
					} elseif ($$setting) { 
						$data[$_key][$store['store_id']] = $$setting;
					} else {
						$data[$_key][$store['store_id']] = '';
					}
				}	
			}
		}
		
		$advanced_matching_data = $this->getPixelData(0, 'advanced_matching_data');
		if (isset($this->request->post[$prefix .'fb_marketing_advanced_matching_data'])) {
			$data[$prefix .'fb_marketing_advanced_matching_data'] = $this->request->post[$prefix .'fb_marketing_advanced_matching_data'];
		} elseif ($advanced_matching_data) { 
			$data[$prefix .'fb_marketing_advanced_matching_data'] = $advanced_matching_data;
		} else {
			$data[$prefix . 'fb_marketing_advanced_matching_data'] = false;
		}
		
		/* Check if update is necessary */ 
		$this->checkUpdate();
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view($path . '.tpl', $data));
	}
	
	public function loadSettingsModal() {
		$path = $this->path();
		
		$data['oc_events'] = $this->getOCEvents('fb_marketing');
		$data['permission'] = $this->user->hasPermission('modify', $path);
		$data['controller_path'] = $path;
		$data['token']	= $this->session->data['token'];
		
		
		$this->response->setOutput($this->load->view($path . '_core_settings.tpl', $data));
	}
	
	protected function getOCEvents($code) {
		
		$events = array();
		
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "event` WHERE `code` = '" . $this->db->escape($code). "'");
		
		if ($query->num_rows) {
			$events = $query->rows;
		}
		
		return $events;
	}
	
	public function saveOCEvent() {
		
		$this->load->language($this->path());
		$json = array();
	
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && !empty($this->request->post['trigger'])  && !empty($this->request->post['action'])) {
			$folder = $this->folder();
			// Register events
			$this->load->model($folder . '/event');
			$event_model_path = 'model_' . $folder . '_event';
		
			//Purchase event
			$this->$event_model_path->addEvent('fb_marketing', $this->db->escape($this->request->post['trigger']), $this->db->escape($this->request->post['action']));
			$json['success'] = 'New OC Event saved succesfully!';
			
		} else {
			$json['warning'] = $this->language->get('error_warning');
		}
		
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function saveSettings() {
		
		$this->load->language($this->path());
		
		$json = array();
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$this->load->model('setting/setting');
			
			$prefix = $this->prefix();
			
			
			parse_str(htmlspecialchars_decode($this->request->post['settings']), $settings);
			
			$save_data = array();
			
			foreach ($this->setting_keys_array as $setting) {
				if (!empty($settings[$prefix . 'fb_marketing_' . $setting])) {
					foreach ($settings[$prefix . 'fb_marketing_' . $setting] as $key => $value) {
				
						$save_data[$key][$prefix . 'fb_marketing_' . $setting] = $value;

					}
				}
			}
			/*
			if (!empty($settings[$prefix . 'fb_marketing_advanced_matching_data'] )) {
				foreach ($settings[$prefix . 'fb_marketing_advanced_matching_data'] as $key => $value) {
				
					$save_data[0][$prefix . 'fb_marketing_advanced_matching_data'][$key] = $value;
				
				}
			}
			*/
			
			// Advanced Matching Data Types
			$adv_matching_data = array(
				'em' 	=> 'on',
				'fn'	=> 'on',
				'ln'	=> 'on',
				'ph'	=> 'on'
			);
		
			$save_data[0][$prefix . 'fb_marketing_advanced_matching_data'] = $adv_matching_data;
			
			foreach ($save_data as $store_id => $data) {
				$this->model_setting_setting->editSetting($prefix . 'fb_marketing', $data, $store_id);
			
			}
			$json['post'] = $save_data;
			
			$json['success'] = $this->language->get('text_success');
			
		} else {
			$json['warning'] = $this->language->get('error_warning');
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		
	}
	
	protected function getPixelData ($store_id, $action) {
		
		$pixel_data = $this->getSettingData($store_id);
		$prefix = $this->prefix();
		
		if (!empty($pixel_data)) {
			return !empty($pixel_data[$prefix . 'fb_marketing_' . $action]) ?  $pixel_data[$prefix . 'fb_marketing_' . $action] : false ;
			
		}
	}
	
	protected function getEventData ($store_id, $event) {
		$event_data = array();
		$prefix = $this->prefix();
		
		$data = $this->getSettingData($store_id);
		
		if (!empty($data[$prefix .'fb_marketing_events'])) {
			$events_data = $data[$prefix . 'fb_marketing_events'];
		}
		
		if(empty($events_data)) {
			$events_data = array();
			foreach($this->default_events as $default_event) {
				$events_data[strtolower($default_event)]['status'] = 1;
			}		
		}
		
		switch ($event) {
			case 'viewcontent': 
				$event_data = array(
					'status'=> $events_data['viewcontent']['status'],
					'path' 	=> array('All Product Pages (product/product)', 0),
					'value'	=> array('Product Price', 0)
				);
				break;
			case 'search':
				$value = (!empty($events_data['search']['value']) ? $events_data['search']['value'] : '' ); 
				$event_data = array(
					'status'=> $events_data['search']['status'],
					'path' 	=> array('Search Page (product/search)', 0),
					'value'	=> array($value, 1)
				);
				break;
			case 'addtocart':
				$event_data = array(
					'status'=> $events_data['addtocart']['status'],
					'path' 	=> array('Dynamic Event on AddToCart click',0),
					'value'	=> array('Product Price', 0)
				);
				break;
			case 'addtowishlist':
				$event_data = array(
					'status'=> $events_data['addtowishlist']['status'],
					'path' 	=> array('Dynamic Event on AddToWishlist click',0),
					'value'	=> array('Product Price',0)
				);
				break;	
			case 'initiatecheckout':
				$event_data = array(
					'status'=> $events_data['initiatecheckout']['status'],
					'path' 	=> array('Checkout page (checkout/checkout)', 0),
					'value'	=> array('Cart Total', 0)
				);
				break;
			case 'addpaymentinfo':
				$value = !empty($events_data['addpaymentinfo']['value']) ? $events_data['addpaymentinfo']['value'] : '' ; 
				$event_data = array(
					'status'=> $events_data['addpaymentinfo']['status'],
					'path' 	=> array('Dynamic Event on Add Payment Info', 0),
					'value'	=> array($value, 1)
				);
				break;
			case 'purchase':
				$event_data = array(
					'status'=> $events_data['purchase']['status'],
					'path' 	=> array('Checkout Success Page (checkout/success)', 0),
					'value'	=> array('Order Total', 0)
				);
				break;
			case 'lead':
				$path = !empty($events_data['lead']['path']) ? $events_data['lead']['path'] : '' ; 
				$value = !empty($events_data['lead']['value']) ? $events_data['lead']['value'] : '' ; 
				$event_data = array(
					'status'=> $events_data['lead']['status'],
					'path' 	=> array($path, 1),
					'value'	=> array($value, 1)
				);
				break;
			case 'completeregistration':
				$value = !empty($events_data['completeregistration']['value']) ? $events_data['completeregistration']['value'] : '' ; 
				$event_data = array(
					'status'=> $events_data['completeregistration']['status'],
					'path' 	=> array('Registration Success Page (account & affiliate/success)', 0),
					'value'	=> array($value , 1)
				);
				break;
			case 'pageview':
				$value = !empty($events_data['pageview']['value']) ? $events_data['pageview']['value'] : '' ;
				$event_data = array(
					'status'=> $events_data['pageview']['status'],
					'path' 	=> array('Default pixel event', 0),
					'value'	=> array($value, 1)
				);
				break;		
		}
		return $event_data;
	}
	
	protected function getSettingData ($store_id) {
		$this->load->model('setting/setting');
		return $this->model_setting_setting->getSetting($this->prefix() . 'fb_marketing',$store_id);
	}
	
	public function install() {
		
		$path = $this->path();
		$prefix = $this->prefix();
		$folder = $this->folder();
		
		// Load Stores
		$this->load->model('setting/store');

		$stores = $this->model_setting_store->getStores();
		array_unshift($stores, array(
			'store_id'	=> 0,
			'name'		=> $this->config->get('config_name')
		));
		
		$data['stores'] = $stores;
		
		//Default Settings
		$events_data = array(
			"viewcontent" => array(
				"status" 	=> "1",
				"path" 		=> "All Product Pages (product/product)",
				"value" 	=> "Product Price"
				),
			"search"	 => array(
				"status" 	=> "1",
				"path"		=> "Search Page (product/search)",
				"value"		=> ""
				),
			"addtocart"  => array(
				"status"	=> "1",
				"path" 		=> "Dynamic Event on AddToCart click",
				"value"		=> "Product Price"
				),
			"addtowishlist" => array(
				"status"	=> "1",
				"path"		=> "Dynamic Event on AddToWishlist click",
				"value"		=> "Product Price"
				),
			"initiatecheckout" => array( 
				"status"	=> "1",
				"path"		=> "Checkout page (checkout/checkout)",
				"value"		=> "Cart Total"
				),
			"addpaymentinfo" => array(
				"status"	=> "1",
				"path"		=> "Dynamic Event on Add Payment Info",
				"value"		=> ""
				),
			"purchase"		=> array(
				"status"	=> "1",
				"path"		=> "Checkout Success Page (checkout/success)",
				"value" 	=> "Order Total"
				),
			"lead"		=> array(
				"status"	=> "1",
				"path"		=> "",
				"value"		=> ""
				),
			"completeregistration"	=> array(
				"status"	=> "1",
				"path"		=> "Registration Success Page (account & affiliate/success)",
				"value"		=>""
				),
			"pageview"		=> array(
				"status"	=> "1",
				"path"		=> "Default pixel event",
				"value"		=> ""
				)
		);
		
		$data = array(
			$prefix . 'fb_marketing_status' 				=> 1 ,
			$prefix . 'fb_marketing_version' 				=> $this->version,
			$prefix . 'fb_marketing_pixel_id' 			=> '',
			$prefix . 'fb_marketing_product_catalog_id' 	=> '',
			$prefix . 'fb_marketing_events'				=> $events_data,
			$prefix . 'fb_marketing_advanced_matching'	=> '1',
			$prefix . 'fb_marketing_values_vat_inc'		=> '0',
			$prefix . 'fb_marketing_pid'					=> 'product_id',
		);
		
		$this->load->model('setting/setting');	
		
		foreach ($stores as $store) {
			$this->model_setting_setting->editSetting($prefix . 'fb_marketing', $data, $store['store_id']);
		}
		
		// Advanced Matching Data Types
		
		$data_types = array(
			'em' 	=> 'on',
			'fn'	=> 'on',
			'ln'	=> 'on',
			'ph'	=> 'on'
		);
		
		$this->db->query("INSERT INTO `". DB_PREFIX . "setting` (`store_id`, `code`, `key`,`value`,`serialized`) VALUES ('0','" . $prefix . "fb_marketing', '" . $prefix . "fb_marketing_advanced_matching_data', '" . json_encode($data_types) . "', '1');");	
		
	 	// Register events
	 	$this->load->model($folder . '/event');
		$event_model_path = 'model_' . $folder . '_event';
	 	
	 	//Purchase event
	 	$this->$event_model_path->addEvent('fb_marketing', 'catalog/controller/checkout/success/before', $path . '/addOrder');
	 	
		//AddToCart event
		$this->$event_model_path->addEvent('fb_marketing', 'catalog/controller/checkout/cart/add/after',  $path . '/addToCart');
		
		//AddToWishlist event
		$this->$event_model_path->addEvent('fb_marketing', 'catalog/controller/account/wishlist/add/after',  $path . '/addToWishlist');
		
		//CompleteRegistration event, dynamic in checkout
		$this->$event_model_path->addEvent('fb_marketing', 'catalog/controller/checkout/register/save/after',  $path . '/CompleteRegistration');
		$this->$event_model_path->addEvent('fb_marketing', 'catalog/controller/checkout/shipping_address/after',  $path . '/CompleteRegistration');
		
		//AddPaymentInfo event, dynamic in checkout
		$this->$event_model_path->addEvent('fb_marketing', 'catalog/controller/checkout/payment_address/after',  $path . '/AddPaymentInfo');
		return true;
	}
	
	public function uninstall() {
		$folder = $this->folder();
		$prefix = $this->prefix();
		
		// Delete settings
		$this->db->query("DELETE FROM `" . DB_PREFIX . "setting` WHERE `code` = '" . $prefix . "fb_marketing'");
		
		// Unregister events
		$this->load->model($folder . '/event');
		$event_model_path = 'model_' . $folder . '_event';
       	$this->$event_model_path->deleteEvent('fb_marketing');

	}
	
	protected function checkUpdate() {
		
		$path = $this->path();
		$folder = $this->folder();
		$prefix = $this->prefix();
		
		$this->load->model($folder . '/event');
		$event_model_path = 'model_' . $folder . '_event';
		
		// Load Stores
		$this->load->model('setting/store');

		$stores = $this->model_setting_store->getStores();
		array_unshift($stores, array(
			'store_id'	=> 0,
			'name'		=> $this->config->get('config_name')
		));
	
		$version_key = $prefix . 'fb_marketing_version';
		$pid_key = $prefix . 'fb_marketing_pid';
		
		foreach($stores as $store) {
			$version = $this->config->get($version_key);
			if(empty($version)) {
				$this->db->query("INSERT INTO `". DB_PREFIX . "setting` (`store_id`, `code`, `key`,`value`,`serialized`) VALUES ('" . (int)$store['store_id']. "','" . $prefix . "fb_marketing', '" . $prefix . "fb_marketing_version', '" . $this->version . "', '0');");
			}
			$pid = $this->config->get($pid_key);
			if (empty($pid)) {
				$this->db->query("INSERT INTO `". DB_PREFIX . "setting` (`store_id`, `code`, `key`,`value`,`serialized`) VALUES ('" . (int)$store['store_id']. "','" . $prefix . "fb_marketing', '" . $prefix . "fb_marketing_pid', 'product_id', '0');");
			}
			if(!empty($version)) {
				if(version_compare($this->config->get($version_key), $this->version, '!=')) {
					$this->db->query("UPDATE `". DB_PREFIX . "setting` SET `value` = '" . $this->version . "' WHERE `store_id` = '" . (int)$store['store_id']. "' AND `key` = '" . $prefix . "fb_marketing_version'");
				}
			}
		}

		if (version_compare(VERSION, '2.2.0.0', '>')) {
			$this->deleteEvent('catalog/model/checkout/order/addOrder/after');
			$this->deleteEvent('catalog/model/d_quickcheckout/order/addOrder/after');
		
			$this->addEvent('catalog/controller/checkout/success/before', 'addOrder');
			
			/* AJAX Quick Checkout fix */
			if(is_file(DIR_APPLICATION . 'controller/module/d_quickcheckout.php')) {
				$trigger = 'catalog/controller/d_quickcheckout/confirm/updateOrder/after';
				$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "event` WHERE `trigger` = '" . $trigger . "'");
				if(empty($query->num_rows)) {
					$this->addEvent('catalog/controller/extension/d_quickcheckout/confirm/updateOrder/after', 'addOrder');
				}
			}
		} else {
			$this->addEvent('post.order.add', 'addOrder');
		}
	}
	
	public function addEvent($trigger, $action){		
		$path = $this->path();
		$folder = $this->folder();
		
		$this->load->model($folder . '/event');
		$event_model_path = 'model_' . $folder . '_event';
		
		$sql = "SELECT * FROM `" . DB_PREFIX . "event` WHERE `trigger` = '" . $trigger . "'";
		
		$query = $this->db->query($sql);
		if(empty($query->num_rows)) {
			$this->$event_model_path->addEvent('fb_marketing', $trigger , $path . '/' . $action );
		}
		
	}
	
	public function deleteEvent($trigger){		
		$sql = "SELECT * FROM `" . DB_PREFIX . "event` WHERE `trigger` = '" . $trigger . "'";
		
		$query = $this->db->query($sql);
		if(!empty($query->num_rows)) {
			$this->db->query("DELETE FROM `" . DB_PREFIX . "event` WHERE `trigger` = '" . $this->db->escape($trigger) . "'");
		}
	}
	
	protected function validate() {
		
		if (!$this->user->hasPermission('modify', $this->path())) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
	
	public function toggle() {
		
		$this->load->language($this->path());
		
		$json = array();
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && isset($this->request->post['row'])) {
			
			$explode = explode('_',$this->request->post['row']);
			$event = reset($explode);
			$store_id = end($explode);
			
			
			$old_settings = $this->getSettingValue('fb_marketing_events', $store_id);
			
			$json['old'] = $old_settings[$event];
			
			$new_settings = $old_settings;
			
			$new_settings[$event]['status'] = !$old_settings[$event]['status'];
			$json['new'] = $new_settings[$event];
			
			$this->db->query("UPDATE `". DB_PREFIX . "setting` SET `value` = '" . json_encode($new_settings) . "' WHERE `key` = 'fb_marketing_events' AND `store_id` = '" . (int)$store_id . "'");
			$json['success'] = $new_settings[$event]['status'] ? $this->language->get('text_activated') : $this->language->get('text_deactivated') ;
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function toggleAdvMatchTypes() {
		
		$this->load->language($this->path());
		$json = array();
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && isset($this->request->post['id'])) {
			
			$explode = explode('_',$this->request->post['id']);
			$type = end($explode);			
			
			$old_settings = $this->getSettingValue('fb_marketing_advanced_matching_data', 0);
			$new_settings = $old_settings;
			
			if(key_exists($type,$old_settings)) {
				unset($new_settings[$type]);
				$json['success'] = $this->language->get('text_adv_match_deactivated');
			} else {
				$new_settings[$type] = "on";
				$json['success'] = $this->language->get('text_adv_match_activated');
			}
			
			$json['old'] = $old_settings;
			$json['new'] = $new_settings;
			
			$this->db->query("UPDATE `". DB_PREFIX . "setting` SET `value` = '" . json_encode($new_settings) . "' WHERE `key` = 'fb_marketing_advanced_matching_data' AND `store_id` = '0'");
			
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	
	public function deactivate() {
		$this->load->language($this->path());
		
		$json = array();
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && isset($this->request->post['row'])) {
			//Implement logic to save the event status
			//$this->db->query("UPDATE `". DB_PREFIX . "category_discount` SET `status` = '0' WHERE `category_discount_id` = '" . (int)$this->request->post['row'] . "' ");
		}
		
		$json['success'] = $this->language->get('text_deactivated');
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	protected function getSettingValue($key,$store_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `key` = '" . $this->db->escape($key) . "'");

		if ($query->num_rows) {
			$result = $query->row;
			if (!$result['serialized']) {
				return $result['value'];
			} else {
				return json_decode($result['value'], true);
			}
		} else {
			return null;	
		}
	}
	
	private function path () {
		if (version_compare(VERSION, '2.3.0.0', '>=')) {
			return 'extension/analytics/fb_marketing';
		} else {
			return 'analytics/fb_marketing';
		}
	}
	
	private function folder () {
		if (version_compare(VERSION, '3.0.0.0', '>=')) {
			return 'setting';
		} else {
			return 'extension';
		}	
	}
	
	private function prefix () {
		if (version_compare(VERSION, '3.0.0.0', '>=')) {
			return 'analytics_';
		} else {
			return '';
		}	
	}
}