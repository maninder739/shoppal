<?php
class ControllerMonthlyPaymentReporting extends Controller { 

	public function index()
	{
		$this->load->language('monthly/payment_reporting');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('monthly/paymentreporting');

        $this->getList();
	}
 
	protected function getList() {

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = null;
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = null;
		} 
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
 	
		$url = ''; 

		if (isset($this->request->get['ID'])) {
			$url .= '&ID='.$this->request->get['ID'];
		} else {
			$url .= '&ID=1';
		}

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}		 

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

	 
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}


		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('monthly/payment_reporting/', 'token=' . $this->session->data['token'] . $url, true)
		);

	 
		$data['add'] = $this->url->link('monthly/payment_reporting/add', 'token=' . $this->session->data['token'] . $url, true);
		 

		$data['affiliates'] = array();

		$filter_data = array(
			'ID' 				=> isset($this->request->get['ID']) ? $this->request->get['ID'] : 1 , 
			'filter_date_start' => $filter_date_start,
			'filter_date_end'   => $filter_date_end,
			'sort'              => $sort,
			'order'             => $order,
			'start'             => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'             => $this->config->get('config_limit_admin')
		);
 		

		$affiliate_total = $this->model_monthly_paymentreporting->getTotal($filter_data);
		$results = $this->model_monthly_paymentreporting->getData($filter_data);
 	 
		  
		foreach ($results as $result) {
			if (!$result['approved']) {
				$approve = $this->url->link('marketing/affiliate/approve', 'token=' . $this->session->data['token'] . '&affiliate_id=' . $result['affiliate_id'] . $url, true);
			} else {
				$approve = '';
			}

			$login_info = $this->model_monthly_paymentreporting->getTotalLoginAttempts($result['email']);

			if ($login_info && $login_info['total'] >= $this->config->get('config_login_attempts')) {
				$unlock = $this->url->link('marketing/affiliate/unlock', 'token=' . $this->session->data['token'] . '&email=' . $result['email'] . $url, true);
			} else {
				$unlock = '';
			}

			$data['affiliates'][] = array(
				'affiliate_id' => $result['affiliate_id'],
				'name'         => $result['name'],
				'company'      => $result['company'],
				'mta_commission'      => number_format($result['AmountCalc'],2),
				'code'      => $result['code'],
				'edit'         => $this->url->link('monthly/payment_reporting/add', 'token=' . $this->session->data['token'] . '&affiliate_id=' . $result['affiliate_id'] . $url, true)
			);	 
		}
	

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_none'] = $this->language->get('text_none');

		
		//$data['filter_code'] = $this->language->get('filter_code');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_organization'] = $this->language->get('column_organization');
		$data['column_amountCalc'] = $this->language->get('column_amountCalc');
		$data['column_amountSent'] = $this->language->get('column_amountSent');
		$data['column_action'] = $this->language->get('column_action'); 

		$data['entry_company'] = $this->language->get('entry_company');
		$data['entry_name'] = $this->language->get('entry_name'); 
		$data['entry_date_start'] = $this->language->get('entry_date_start');
		$data['entry_date_end']   = $this->language->get('entry_date_end');

		$data['button_approve'] = $this->language->get('button_approve');
		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_filter'] = $this->language->get('button_filter');
		$data['button_unlock'] = $this->language->get('button_unlock');
		
		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if(isset($this->request->post['ID'])) {
			$url .= '&ID='.$this->request->post['ID'];
		} else {
			$url .= '&ID=1';
		}
 

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}


		$data['sort_name'] = $this->url->link('monthly/payment_reporting', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_company'] = $this->url->link('monthly/payment_reporting', 'token=' . $this->session->data['token'] . '&sort=a.company' . $url, true);
		$data['sort_commission'] = $this->url->link('monthly/payment_reporting', 'token=' . $this->session->data['token'] . '&sort=commission' . $url, true);

		 
		$url = '';

		 

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

			if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if(isset($this->request->post['ID'])) {
			$url .= '&ID='.$this->request->post['ID'];
		} else {
			$url .= '&ID=1';
		}

		$pagination = new Pagination();
		$pagination->total = $affiliate_total; 
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('monthly/payment_reporting', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();


		$data['results'] = sprintf($this->language->get('text_pagination'), ($affiliate_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($affiliate_total - $this->config->get('config_limit_admin'))) ? $affiliate_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $affiliate_total, ceil($affiliate_total / $this->config->get('config_limit_admin')));

		$data['filter_date_start'] = $filter_date_start;
		$data['filter_date_end'] = $filter_date_end;
		$data['sort'] = $sort;
		$data['order'] = $order;


		$getlistings = $this->IfsMemberslisting();
		foreach ($getlistings as $key => $value) {
			
			$data['Memberslisting'][] = array(
				'affiliate_id' => $value['affiliate_id'],
				'name'         => $value['firstname'].' ' .$value['lastname'],
				'email'        => $value['email'],
				'company'      => $value['company'],
				'code'      => $value['code']
			);  
		}

		//echo '<pre>'; print_r($data['Memberslisting']); die;	
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('monthly/paymentreporting_list', $data));
	}

	Public function IfsMemberslisting() {
		$results = $this->model_monthly_paymentreporting->getIFSMemberlisting();
		return $results;
		 
	}

	//custom amount sent
	public function saveToDatabase() {
		$this->load->model('monthly/paymentreporting');		 
		if(!empty($this->request->post['editval'])) {
		$results = $this->model_monthly_paymentreporting->amountSent($this->request->post['editval'], $this->request->post['id']); 

		}
	}

	public function add() {
		$this->load->language('monthly/payment_reporting');
 		$this->load->model('monthly/paymentreporting');

		$this->document->setTitle($this->language->get('heading_title'));
		 

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$this->model_marketing_affiliate->addAffiliate($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['ID'])) { 
				$url .= '&ID=' . $this->request->get['ID'];
			}

			if (isset($this->request->get['affiliate_id'])) { 
				$url .= '&affiliate_id=' . $this->request->get['affiliate_id'];
			}

			if (isset($this->request->get['filter_date_start'])) { 
				$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
			}

			if (isset($this->request->get['filter_date_end'])) { 
				$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('marketing/affiliate', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	 

	protected function getForm() {

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['affiliate_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_affiliate_detail'] = $this->language->get('text_affiliate_detail');
		$data['text_affiliate_address'] = $this->language->get('text_affiliate_address');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_loading'] = $this->language->get('text_loading');
		$data['text_cheque'] = $this->language->get('text_cheque');
		$data['text_paypal'] = $this->language->get('text_paypal');
		$data['text_bank'] = $this->language->get('text_bank');

		 
		$data['entry_commission'] = $this->language->get('entry_commission');
		$data['entry_amountCalc'] = $this->language->get('entry_amountCalc');
		$data['entry_amountSent'] = $this->language->get('entry_amountSent');

		$data['entry_tax'] = $this->language->get('entry_tax');
		$data['entry_payment'] = $this->language->get('entry_payment');
		$data['entry_cheque'] = $this->language->get('entry_cheque');
		$data['entry_paypal'] = $this->language->get('entry_paypal');
		$data['entry_bank_name'] = $this->language->get('entry_bank_name');
		$data['entry_PaymentNote'] = $this->language->get('entry_PaymentNote');
		$data['entry_paymentRef'] = $this->language->get('entry_paymentRef');

		$data['help_code'] = $this->language->get('help_code');
		$data['help_commission'] = $this->language->get('help_commission'); 

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_transaction_add'] = $this->language->get('button_transaction_add');
		$data['button_remove'] = $this->language->get('button_remove');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_organization'] = $this->language->get('tab_organization');
		$data['tab_payment'] = $this->language->get('tab_payment');
		$data['tab_transaction'] = $this->language->get('tab_transaction');
 
		$url = '';

		if(isset($this->request->get['ID'])) {
			$url .= '&ID='.$this->request->get['ID'];
		} else {
			$url .= '&ID=1';
		}
		
		if (isset($this->request->get['affiliate_id'])) { 
			$url .= '&affiliate_id=' . $this->request->get['affiliate_id'];
		}

		if (isset($this->request->get['filter_start_date'])) {
			$url .= '&filter_start_date=' . $this->request->get['filter_start_date'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('marketing/affiliate', 'token=' . $this->session->data['token'] . $url, true)
		);

		 
		$data['cancel'] = $this->url->link('monthly/payment_reporting', 'token=' . $this->session->data['token'] . $url, true);

		$data['action'] = $this->url->link('monthly/payment_reporting/save', 'token=' . $this->session->data['token'] . '&affiliate_id=' . $this->request->get['affiliate_id'] . $url, true);
		 
		$data['token'] = $this->session->data['token'];


		if (isset($this->request->get['affiliate_id'])) {
			$affiliate_id = $this->request->get['affiliate_id'];
		} else {
			$affiliate_id = null;
		}

	 	if(isset($this->request->post['mta_commission'])) {
			$data['mta_commission'] = $this->request->post['mta_commission'];
		} else {
			$data['mta_commission'] = '';
		}
 

		if(isset($this->request->post['paymentref'])) {
			$data['paymentref'] = $this->request->post['paymentref'];
		} else {
			$data['paymentref'] = '';
		}


		if(isset($this->request->post['paymentnote'])) {
			$data['paymentnote'] = $this->request->post['paymentnote'];
		} else {
			$data['paymentnote'] = '';
		}

		if (isset($this->request->post['cheque'])) {
			$data['cheque'] = $this->request->post['cheque'];
		} else {
			$data['cheque'] = '';
		}

		if (isset($this->request->post['bank_name'])) {
			$data['bank_name'] = $this->request->post['bank_name'];
		} else {
			$data['bank_name'] = '';
		}

		$filter_data = array(
			'affiliate_id' => $affiliate_id,
		);
 		

		$affiliate_total = $this->model_monthly_paymentreporting->getData($filter_data);


		$data['affiliates'] = array(
			'affiliate_id' => $affiliate_total['0']['affiliate_id'],
			'name'         => $affiliate_total['0']['name'],
			'company'      => $affiliate_total['0']['company'],
			'mta_commission' => number_format($affiliate_total['0']['AmountCalc'],2),
		);	  
		  	
		/*  Organization fields  */

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		// echo "<pre>"; print_r($data); die;

		$this->response->setOutput($this->load->view('monthly/affiliate_form', $data));
	}

	public function save() {	
		$this->load->model('monthly/paymentreporting');
 		$savePayment = $this->model_monthly_paymentreporting->savePayment($this->request->post);
 		 
 		return $this->index();
	}
}

?>
 