<?php
class ControllerMonthlyEndMonth extends Controller 
{ 

    public function index() 
    {
       //echo "<pre>"; print_r($this->request->post['approved_selected']); die;
        $this->load->language('monthly/end_month');

        if (isset($this->request->get['filter_date_start'])) {
            $filter_date_start = $this->request->get['filter_date_start'];
        } else {
            $filter_date_start = date("Y-m-d", strtotime("-3 months"));
        }

        if (isset($this->request->get['filter_date_end'])) {
            $filter_date_end = $this->request->get['filter_date_end'];
        } else {
            $filter_date_end = date('Y-m-d');
        }

        if (isset($this->request->get['filter_order_status'])) {
            $filter_order_status = $this->request->get['filter_order_status'];
        } else {
            $filter_order_status = '3';
        }

        if (isset($this->request->get['filter_tracking_code'])) {
            $filter_tracking_code = $this->request->get['filter_tracking_code'];
        } else {
            $filter_tracking_code = null;
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'o.order_id';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['filter_tracking_code'])) {
            $url .= '&filter_tracking_code=' . $this->request->get['filter_tracking_code'];
        }

        if (isset($this->request->get['filter_date_start'])) {
            $url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
        }

        if (isset($this->request->get['filter_date_end'])) {
            $url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
        }

        if (isset($this->request->get['filter_order_status'])) {
            $url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }
 

        $data['thumb_approve'] = $this->url->link('monthly/end_month/allapproved', 'token=' . $this->session->data['token']. $url, true);
        $data['save_apply_amount'] = $this->url->link('monthly/end_month/applyamount', 'token=' . $this->session->data['token']. $url, true);

        $data['save_apply_amount'] = $this->url->link('monthly/end_month/applyamount', 'token=' . $this->session->data['token']. $url, true);

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true),
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('monthly/end_month', 'token=' . $this->session->data['token'] . $url, true),
        );

        

        $data['orders'] = array();

        $filter_data = array(
            'filter_order_status'  => $filter_order_status,
            'filter_tracking_code' => $filter_tracking_code,
            'filter_date_end'      => $filter_date_end,
            'filter_date_start'    => $filter_date_start,
            'sort'                 => $sort,
            'order'                => $order,
            'start'                => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit'                => $this->config->get('config_limit_admin'),
        );

        $this->load->model('monthly/endmonth');
        $customer_total = $this->model_monthly_endmonth->getTotalOrders($filter_data);

        $results = $this->model_monthly_endmonth->getOrders($filter_data);

        if (!empty($results)) {
            foreach ($results as $result) {

	             
				$approve = $this->url->link('monthly/end_month/approve', 'token=' . $this->session->data['token'] . '&affiliate_id=' . $result['affiliate_id']. '&mta_order_id=' . $result['mta_order_id'] .'&mta_commission=' . $result['commission'] .'&customer_id=' . $result['customer_id'].'&order_status_id=' . $result['order_status_id']  . $url, true);
				 
                if  ($result['lock'] == 0 ) {
                    $lock = $this->url->link('monthly/end_month/lock', 'token=' . $this->session->data['token']. '&affiliate_id=' . $result['affiliate_id']. '&mta_order_id=' . $result['mta_order_id'] .'&mta_commission=' . $result['commission'] .'&customer_id=' . $result['customer_id'].'&order_status_id=' . $result['order_status_id']  . $url, true);
                } else {
                    $lock  = '';
                }
			 

  
                $data['orders'][] = array(
                    'order_id'      => $result['order_id'],
                    'customer'      => $result['customer'],
                    'tracking'      => $result['tracking'],
                    'mta_order_id'  => $result['mta_order_id'],
                    'commission_added' => $result['commission_added'],
                    'comission_added_status' => $result['comission_added_status'],
                    'orgname'       => $result['orgname'],
                    'refamount'     => $this->currency->format($result['commission'], $result['currency_code'], $result['currency_value']),
                    'refaffiliate'  => $result['refaffiliate'],
                    'retails'       => $this->currency->format($result['retail_total'], $result['currency_code'], $result['currency_value']),
                    'order_status'  => $result['order_status'] ? $result['order_status'] : $this->language->get('text_missing'),
                    'total'         => $this->currency->format($result['total'], $result['currency_code'], $result['currency_value']),
                    'approve'     => $approve,
                    'lock'     => $lock,
                    'date_added'    => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
                    
                    'order_edit'    => $this->url->link('sale/order/info', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true),
                    'customer_edit' => $this->url->link('customer/customer/edit', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['customer_id'] . $url, true), 

                );
            }
            // echo "<pre>"; print_r($data['orders']); die;
        }

       $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list']       = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm']    = $this->language->get('text_confirm');
        $data['text_missing']    = $this->language->get('text_missing');
        $data['text_loading']    = $this->language->get('text_loading');

        $data['column_order_id']      = $this->language->get('column_order_id');
        $data['column_customer']      = $this->language->get('column_customer');
        $data['column_trackingcode']  = $this->language->get('column_trackingcode');
        $data['column_orgname']       = $this->language->get('column_orgname');
        $data['column_status']        = $this->language->get('column_status');
        $data['column_retailtotal']   = $this->language->get('Retail Total');
        $data['column_total']         = $this->language->get('column_total');
        $data['column_date_added']    = $this->language->get('column_date_added');
        $data['column_date_modified'] = $this->language->get('column_date_modified');
        $data['column_action']        = $this->language->get('column_action');
        $data['column_date_start']    = $this->language->get('column_date_start');
        $data['column_date_end']      = $this->language->get('column_date_end');
        $data['column_refaffiliate']      = $this->language->get('column_refaffiliate');
        $data['column_refamount']      = $this->language->get('column_refamount');

        $data['entry_order_id']      = $this->language->get('entry_order_id');
        $data['entry_orgname']       = $this->language->get('entry_orgname');
        $data['entry_trackingcode']  = $this->language->get('entry_trackingcode');
        $data['entry_customer']      = $this->language->get('entry_customer');
        $data['entry_order_status']  = $this->language->get('entry_order_status');
        $data['entry_total']         = $this->language->get('entry_total');
        $data['entry_retailtotal']   = $this->language->get('entry_retailtotal');
        $data['entry_date_added']    = $this->language->get('entry_date_added');
        $data['entry_date_modified'] = $this->language->get('entry_date_modified');
        $data['entry_date_start']    = $this->language->get('entry_date_start');
        $data['entry_date_end']      = $this->language->get('entry_date_end');
        $data['entry_efaffiliate']      = $this->language->get('entry_efaffiliate');
        $data['entry_efamount']      = $this->language->get('entry_efamount');


        $data['button_invoice_print']       = $this->language->get('button_invoice_print');
        $data['button_shipping_print']      = $this->language->get('button_shipping_print');
        $data['Print_Outstanding_Invoices'] = $this->language->get('Print_Outstanding_Invoices');
        $data['button_add']                 = $this->language->get('button_add');
        $data['button_edit']                = $this->language->get('button_edit');
        $data['button_delete']              = $this->language->get('button_delete');
        $data['button_filter']              = $this->language->get('button_filter');
        $data['button_view']                = $this->language->get('button_view');
        $data['button_view_customer']       = $this->language->get('button_view_customer');
        $data['button_view_order']          = $this->language->get('button_view_order');
        $data['button_ip_add']              = $this->language->get('button_ip_add');
        $data['button_save']              = $this->language->get('button_save');
        $data['button_approve']           = $this->language->get('button_approve');
        $data['button_unapprove']           = $this->language->get('button_unapprove');
        $data['button_aprrove_all']           = $this->language->get('button_aprrove_all');
        $data['button_lock']           = $this->language->get('button_lock');

        
        $data['token'] = $this->session->data['token'];

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }


        $this->load->model('localisation/order_status');

        $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

         $data['action'] = $this->url->link('monthly/end_month', 'token=' . $this->session->data['token'] . $url, true);

        

        $data['sort_order']         = $this->url->link('monthly/end_month', 'token=' . $this->session->data['token'] . '&sort=o.order_id' . $url, true);
        $data['sort_retail_total']  = $this->url->link('monthly/end_month', 'token=' . $this->session->data['token'] . '&sort=retail_total' . $url, true);
        $data['sort_tracking']      = $this->url->link('monthly/end_month', 'token=' . $this->session->data['token'] . '&sort=tracking' . $url, true);
        $data['sort_orgname']       = $this->url->link('monthly/end_month', 'token=' . $this->session->data['token'] . '&sort=orgname' . $url, true);
        $data['sort_customer']      = $this->url->link('monthly/end_month', 'token=' . $this->session->data['token'] . '&sort=customer' . $url, true);
        $data['sort_status']        = $this->url->link('monthly/end_month', 'token=' . $this->session->data['token'] . '&sort=order_status' . $url, true);
        $data['sort_refaffiliate']         = $this->url->link('monthly/end_month', 'token=' . $this->session->data['token'] . '&sort=refaffiliate' . $url, true);
        $data['sort_refamount']         = $this->url->link('monthly/end_month', 'token=' . $this->session->data['token'] . '&sort=mo.commission' . $url, true);
        $data['sort_date_added']    = $this->url->link('monthly/end_month', 'token=' . $this->session->data['token'] . '&sort=o.date_added' . $url, true);
        

        $url = '';
        
        if (isset($this->request->get['filter_order_status'])) {
            $url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
        }

        if (isset($this->request->get['filter_tracking_code'])) {
            $url .= '&filter_tracking_code=' . $this->request->get['filter_tracking_code'];
        }

        if (isset($this->request->get['filter_date_start'])) {
            $url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
        }

        if (isset($this->request->get['filter_date_end'])) {
            $url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order']; 
        }

         
        $pagination        = new Pagination();
        $pagination->total = $customer_total;
        $pagination->page  = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url   = $this->url->link('monthly/end_month', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($customer_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($customer_total - $this->config->get('config_limit_admin'))) ? $customer_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $customer_total, ceil($customer_total / $this->config->get('config_limit_admin')));

        $data['filter_tracking_code'] = $filter_tracking_code;
        $data['filter_order_status']  = $filter_order_status;
        $data['filter_date_start']    = $filter_date_start;
        $data['filter_date_end']      = $filter_date_end;

        $data['sort']  = $sort;
        $data['order'] = $order;

        $data['header']      = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer']      = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('monthly/end_month', $data));
    }

    public function update()
    {

    	$this->load->model('monthly/endmonth');
    	$selected_order_id[] = $this->request->post['selected'];
    	 
    	if(!empty($selected_order_id)) {
    		foreach ($selected_order_id as $key => $value) {
    			$customer_total = $this->model_monthly_endmonth->commissionupdate($value);
    		}
    	} 
		$this->response->setOutput($this->load->view('monthly/end_month'));

    }

    public function approve() {
    	
    	$this->load->language('monthly/end_month');
		$this->load->model('monthly/endmonth');

		$this->document->setTitle($this->language->get('heading_title'));

		 
		 
		if (isset($this->request->get['affiliate_id']) && ($this->request->get['mta_order_id']) && ($this->request->get['mta_commission']) && ($this->request->get['customer_id']) && ($this->request->get['order_status_id']) ) {
			$this->model_monthly_endmonth->approve($this->request->get['affiliate_id'],$this->request->get['mta_order_id'],$this->request->get['mta_commission'],$this->request->get['customer_id'],$this->request->get['order_status_id']);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
	        
	        if (isset($this->request->get['filter_order_status'])) {
	            $url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
	        }

	        if (isset($this->request->get['filter_tracking_code'])) {
	            $url .= '&filter_tracking_code=' . $this->request->get['filter_tracking_code'];
	        }

	        if (isset($this->request->get['filter_date_start'])) {
	            $url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
	        }

	        if (isset($this->request->get['filter_date_end'])) {
	            $url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
	        }

	        if (isset($this->request->get['sort'])) {
	            $url .= '&sort=' . $this->request->get['sort'];
	        }

	        if (isset($this->request->get['order'])) {
	            $url .= '&order=' . $this->request->get['order']; 
	        }

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('monthly/end_month', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->index();
	}	

    public function allapproved() {
        
        $this->load->language('monthly/end_month');
        $this->load->model('monthly/endmonth');
        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'o.order_id';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }
        $filter_data = array(
            'order'                 => $order,
            'start'                => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit'                => $this->config->get('config_limit_admin'),
        );
        $results = $this->model_monthly_endmonth->getOrders($filter_data);
        //echo "<pre>"; print_r($results ); die;
        foreach ($results as $result) {

            if ($result['commission_added'] == 1 ) {
                
                $this->model_monthly_endmonth->approve($result['affiliate_id'],$result['mta_order_id'],$result['commission'],$result['customer_id'],$result['order_status_id']);
            } 
        }

        $this->session->data['success'] = $this->language->get('text_success');

        $url = '';
        
        if (isset($this->request->get['filter_order_status'])) {
            $url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
        }

        if (isset($this->request->get['filter_tracking_code'])) {
            $url .= '&filter_tracking_code=' . $this->request->get['filter_tracking_code'];
        }

        if (isset($this->request->get['filter_date_start'])) {
            $url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
        }

        if (isset($this->request->get['filter_date_end'])) {
            $url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order']; 
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->response->redirect($this->url->link('monthly/end_month', 'token=' . $this->session->data['token'] . $url, true));
       
        $this->index();
    }

    public function applyamount()
    {
         
         $this->load->language('monthly/end_month');
        $this->load->model('monthly/endmonth');

        if (isset($this->request->post['approved_selected'])) {
            $approved_selected = (array) $this->request->post['approved_selected'];
        } else {
            $approved_selected = array();  
        }

        if (isset($this->request->post['approved_unselected'])) {
            $approved_unselected = (array) $this->request->post['approved_unselected'];
        } else {
            $approved_unselected = array();  
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'o.order_id';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }



        $merge = array_merge($approved_selected , $approved_unselected);
        $approved_unselected = array_diff($merge , $approved_selected);

        $filter_data = array(
            'unselected' => $approved_unselected,
            'selected'   =>  $approved_selected,
            'order'                 => $order,
            'start'                => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit'                => $this->config->get('config_limit_admin'),
        );

        $applyamount = $this->model_monthly_endmonth->applyamount($filter_data); 

        $url = '';
        
        if (isset($this->request->get['filter_order_status'])) {
            $url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
        }

        if (isset($this->request->get['filter_tracking_code'])) {
            $url .= '&filter_tracking_code=' . $this->request->get['filter_tracking_code'];
        }

        if (isset($this->request->get['filter_date_start'])) {
            $url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
        }

        if (isset($this->request->get['filter_date_end'])) {
            $url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order']; 
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->response->redirect($this->url->link('monthly/end_month', 'token=' . $this->session->data['token'] . $url, true));
              
        $this->index();
    }

    public function lock()
    {
        $this->load->language('monthly/end_month');
        $this->load->model('monthly/endmonth');

        $this->document->setTitle($this->language->get('heading_title'));
         
        if (isset($this->request->get['affiliate_id']) && ($this->request->get['mta_order_id']) && ($this->request->get['mta_commission']) && ($this->request->get['customer_id']) && ($this->request->get['order_status_id']) ) {
            

            $this->model_monthly_endmonth->lock($result['affiliate_id'],$result['mta_order_id'],$result['commission'],$result['customer_id'],$result['order_status_id']);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';
            
            if (isset($this->request->get['filter_order_status'])) {
                $url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
            }

            if (isset($this->request->get['filter_tracking_code'])) {
                $url .= '&filter_tracking_code=' . $this->request->get['filter_tracking_code'];
            }

            if (isset($this->request->get['filter_date_start'])) {
                $url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
            }

            if (isset($this->request->get['filter_date_end'])) {
                $url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order']; 
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('monthly/end_month', 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->index();
    }
}
