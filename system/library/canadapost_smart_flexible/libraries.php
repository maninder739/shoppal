<?php
/**
 * Module version: 5.4.6
 *
 * For bugs and suggestions please contact: support@drugoe.de
 *
 * CANADAPOST Smart & Flexible Shipping Module - Terms and conditions
 *
 * 1. Preamble: This Agreement governs the relationship between customer, (hereinafter: Licensee) and "Drugoe",
 *    an approved module vendor (hereinafter: Licensor). This Agreement sets the terms, rights,
 *    restrictions and obligations on using CANADAPOST Smart & Flexible Shipping Module
 *    (hereinafter: The Software) created and owned by Licensor, as detailed herein
 *
 * 2. License Grant: Licensor hereby grants Licensee a Personal, Non-assignable & non-transferable, Perpetual,
 *    Commercial, Royalty free, Including the rights to create but not distribute derivative works,
 *    Non-exclusive license, all with accordance with the terms set forth and other legal restrictions
 *    set forth in 3rd party software used while running Software.
 * 2.1. Limited: Licensee may use Software for the purpose of:
 * 2.1.1. Running Software on Licensee’s Website[s] and Server[s];
 * 2.1.2. Allowing 3rd Parties to run Software on Licensee’s Website[s] and Server[s];
 * 2.1.3. Publishing Software’s output to Licensee and 3rd Parties;
 * 2.1.4. Distribute verbatim copies of Software’s output (including compiled binaries);
 * 2.1.5. Modify Software to suit Licensee’s needs and specifications.
 * 2.2. This license is granted perpetually, as long as you do not materially breach it.
 * 2.3. Binary Restricted: Licensee may sublicense Software as a part of a larger work containing more than Software,
 *      distributed solely in Object or Binary form under a personal, non-sublicensable, limited license.
 *      Such redistribution shall be limited to unlimited codebases.
 * 2.4. Non Assignable & Non-Transferable: Licensee may not assign or transfer his rights and duties under this license.
 * 2.5. Commercial, Royalty Free: Licensee may use Software for any purpose,
 *      including paid-services, without any royalties
 * 2.6. Including the Right to Create Derivative Works: Licensee may create derivative works based on Software,
 *      including amending Software’s source code, modifying it, integrating it into a larger work or removing
 *      portions of Software, as long as no distribution of the derivative works is made
 *
 * 3. Term & Termination: The Term of this license shall be until terminated.
 *    Licensor may terminate this Agreement, including Licensee’s license in the case where Licensee :
 * 3.1. became insolvent or otherwise entered into any liquidation process; or
 * 3.2. exported The Software to any jurisdiction where licensor may not enforce his rights under this agreements in; or
 * 3.3. Licensee was in breach of any of this license's terms and conditions and such breach was not cured,
 *      immediately upon notification; or
 * 3.4. Licensee in breach of any of the terms of clause 2 to this license; or
 * 3.5. Licensee otherwise entered into any arrangement which caused Licensor to be unable to enforce his rights
 *      under this License.
 *
 * 4. Payment: In consideration of the License granted under clause 2, Licensee shall pay Licensor a fee,
 *    via Credit-Card, PayPal or any other mean which Licensor may deem adequate. Failure to perform payment
 *    shall construe as material breach of this Agreement.
 *
 * 5. Upgrades, Updates and Fixes: Licensor may provide Licensee, from time to time, with Upgrades, Updates or Fixes,
 *    as detailed herein and according to his sole discretion. Licensee hereby warrants to keep The Softwareup-to-date
 *    and install all relevant updates and fixes, and may, at his sole discretion, purchase upgrades,
 *    according to the rates set by Licensor. Licensor shall provide any update or Fix free of charge;
 *    however, nothing in this Agreement shall require Licensor to provide Updates or Fixes.
 * 5.1. Upgrades: for the purpose of this license, an Upgrade shall be a material amendment in The Software,
 *    which contains new features and or major performance improvements and shall be marked as a new version number.
 *    For example, should Licensee purchase The Software under version 1.X.X,
 *    an upgrade shall commence under number 2.0.0.
 * 5.2. Updates:  for the purpose of this license, an update shall be a minor amendment in The Software,
 *      which may contain new features or minor improvements and shall be marked as a new sub-version number.
 *      For example, should Licensee purchase The Software under version 1.1.X,
 *      an upgrade shall commence under number 1.2.0.
 * 5.3. Fix: for the purpose of this license, a fix shall be a minor amendment in The Software,
 *      intended to remove bugs or alter minor features which impair the The Software's functionality.
 *      A fix shall be marked as a new sub-sub-version number. For example, should Licensee purchase Software
 *      under version 1.1.1, an upgrade shall commence under number 1.1.2.
 *
 * 6. Support: Software is provided under an AS-IS basis and without any support, updates or maintenance.
 *    Nothing in this Agreement shall require Licensor to provide Licensee with support or fixes to any bug, failure,
 *    mis-performance or other defect in The Software.
 * 6.1. Bug Notification:  Licensee may provide Licensor of details regarding any bug, defect or failure in The Software
 *      promptly and with no delay from such event; Licensee shall comply with Licensor's request for information
 *      regarding bugs, defects or failures and furnish him with information, screenshots and try to reproduce
 *      such bugs, defects or failures.
 * 6.2. Feature Request:  Licensee may request additional features in Software, provided, however, that
 *      (i) Licensee shall waive any claim or right in such feature should feature be developed by Licensor;
 *     (ii) Licensee shall be prohibited from developing the feature, or disclose such feature request, or feature,
 *          to any 3rd party directly competing with Licensor or any 3rd party which may be,
 *          following the development of such feature, in direct competition with Licensor;
 *    (iii) Licensee warrants that feature does not infringe any 3rd party patent, trademark,
 *          trade-secret or any other intellectual property right; and
 *     (iv) Licensee developed, envisioned or created the feature solely by himself.
 *
 * 7. Liability:  To the extent permitted under Law, The Software is provided under an AS-IS basis.
 *    Licensor shall never, and without any limit, be liable for any damage, cost, expense or any other payment incurred
 *    by Licensee as a result of Software’s actions, failure, bugs and/or any other interaction between The Software
 *    and Licensee’s end-equipment, computers, other software or any 3rd party, end-equipment, computer or services.
 *    Moreover, Licensor shall never be liable for any defect in source code written by Licensee
 *    when relying on The Software or using The Software’s source code.
 *
 * 8. Warranty:
 * 8.1. Intellectual Property: Licensor hereby warrants that The Software does not violate or infringe
 *      any 3rd party claims in regards to intellectual property, patents and/or trademarks and that
 *      to the best of its knowledge no legal action has been taken against it for any infringement or violation
 *      of any 3rd party intellectual property rights.
 * 8.2. No-Warranty: The Software is provided without any warranty; Licensor hereby disclaims any warranty
 *      that The Software shall be error free, without defects or code which may cause damage to Licensee’s computers
 *      or to Licensee, and that Software shall be functional. Licensee shall be solely liable to any damage,
 *      defect or loss incurred as a result of operating software and undertake the risks contained in running
 *      The Software on License’s Server[s] and Website[s].
 * 8.3. Prior Inspection:  Licensee hereby states that he inspected The Software thoroughly and found it satisfactory
 *      and adequate to his needs, that it does not interfere with his regular operation and that it does meet
 *      the standards and scope of his computer systems and architecture. Licensee found that The Software interacts
 *      with his development, website and server environment and that it does not infringe any of End User License
 *      Agreement of any software Licensee may use in performing his services. Licensee hereby waives any claims
 *      regarding The Software's incompatibility, performance, results and features, and warrants that he inspected
 *      the The Software.
 *
 * 9. No Refunds: Licensee warrants that he inspected The Software according to clause 7(c) and that it is adequate
 *    to his needs. Accordingly, as The Software is intangible goods, Licensee shall not be, ever,
 *    entitled to any refund, rebate, compensation or restitution for any reason whatsoever, even if The Software
 *    contains material flaws.
 *
 * 10. Indemnification: Licensee hereby warrants to hold Licensor harmless and indemnify Licensor for any lawsuit
 *     brought against it in regards to Licensee’s use of The Software in means that violate, breach or otherwise
 *     circumvent this license, Licensor's intellectual property rights or Licensor's title in The Software.
 *     Licensor shall promptly notify Licensee in case of such legal action and request Licensee’s consent
 *     prior to any settlement in relation to such lawsuit or claim.
 *
 * 11. Governing Law, Jurisdiction: Licensee hereby agrees not to initiate class-action lawsuits against Licensor
 *     in relation to this license and to compensate Licensor for any legal fees, cost or attorney fees should
 *     any claim brought by Licensee against Licensor be denied, in part or in full.
 */

?><?php


namespace {



}
namespace {



}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Psr\Log {

/**
 * Describes a logger instance.
 *
 * The message MUST be a string or object implementing __toString().
 *
 * The message MAY contain placeholders in the form: {foo} where foo
 * will be replaced by the context data in key "foo".
 *
 * The context array can contain arbitrary data. The only assumption that
 * can be made by implementors is that if an Exception instance is given
 * to produce a stack trace, it MUST be in a key named "exception".
 *
 * See https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-3-logger-interface.md
 * for the full interface specification.
 */
interface LoggerInterface
{
    /**
     * System is unusable.
     *
     * @param string $message
     * @param array  $context
     *
     * @return null
     */
    public function emergency($message, array $context = array());

    /**
     * Action must be taken immediately.
     *
     * Example: Entire website down, database unavailable, etc. This should
     * trigger the SMS alerts and wake you up.
     *
     * @param string $message
     * @param array  $context
     *
     * @return null
     */
    public function alert($message, array $context = array());

    /**
     * Critical conditions.
     *
     * Example: Application component unavailable, unexpected exception.
     *
     * @param string $message
     * @param array  $context
     *
     * @return null
     */
    public function critical($message, array $context = array());

    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * @param string $message
     * @param array  $context
     *
     * @return null
     */
    public function error($message, array $context = array());

    /**
     * Exceptional occurrences that are not errors.
     *
     * Example: Use of deprecated APIs, poor use of an API, undesirable things
     * that are not necessarily wrong.
     *
     * @param string $message
     * @param array  $context
     *
     * @return null
     */
    public function warning($message, array $context = array());

    /**
     * Normal but significant events.
     *
     * @param string $message
     * @param array  $context
     *
     * @return null
     */
    public function notice($message, array $context = array());

    /**
     * Interesting events.
     *
     * Example: User logs in, SQL logs.
     *
     * @param string $message
     * @param array  $context
     *
     * @return null
     */
    public function info($message, array $context = array());

    /**
     * Detailed debug information.
     *
     * @param string $message
     * @param array  $context
     *
     * @return null
     */
    public function debug($message, array $context = array());

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed  $level
     * @param string $message
     * @param array  $context
     *
     * @return null
     */
    public function log($level, $message, array $context = array());
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Psr\Log {

/**
 * Describes a logger-aware instance.
 */
interface LoggerAwareInterface
{
    /**
     * Sets a logger instance on the object.
     *
     * @param LoggerInterface $logger
     *
     * @return null
     */
    public function setLogger(LoggerInterface $logger);
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Psr\Log {

/**
 * This is a simple Logger implementation that other Loggers can inherit from.
 *
 * It simply delegates all log-level-specific methods to the `log` method to
 * reduce boilerplate code that a simple Logger that does the same thing with
 * messages regardless of the error level has to implement.
 */
abstract class AbstractLogger implements LoggerInterface
{
    /**
     * System is unusable.
     *
     * @param string $message
     * @param array  $context
     *
     * @return null
     */
    public function emergency($message, array $context = array())
    {
        $this->log(LogLevel::EMERGENCY, $message, $context);
    }

    /**
     * Action must be taken immediately.
     *
     * Example: Entire website down, database unavailable, etc. This should
     * trigger the SMS alerts and wake you up.
     *
     * @param string $message
     * @param array  $context
     *
     * @return null
     */
    public function alert($message, array $context = array())
    {
        $this->log(LogLevel::ALERT, $message, $context);
    }

    /**
     * Critical conditions.
     *
     * Example: Application component unavailable, unexpected exception.
     *
     * @param string $message
     * @param array  $context
     *
     * @return null
     */
    public function critical($message, array $context = array())
    {
        $this->log(LogLevel::CRITICAL, $message, $context);
    }

    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * @param string $message
     * @param array  $context
     *
     * @return null
     */
    public function error($message, array $context = array())
    {
        $this->log(LogLevel::ERROR, $message, $context);
    }

    /**
     * Exceptional occurrences that are not errors.
     *
     * Example: Use of deprecated APIs, poor use of an API, undesirable things
     * that are not necessarily wrong.
     *
     * @param string $message
     * @param array  $context
     *
     * @return null
     */
    public function warning($message, array $context = array())
    {
        $this->log(LogLevel::WARNING, $message, $context);
    }

    /**
     * Normal but significant events.
     *
     * @param string $message
     * @param array  $context
     *
     * @return null
     */
    public function notice($message, array $context = array())
    {
        $this->log(LogLevel::NOTICE, $message, $context);
    }

    /**
     * Interesting events.
     *
     * Example: User logs in, SQL logs.
     *
     * @param string $message
     * @param array  $context
     *
     * @return null
     */
    public function info($message, array $context = array())
    {
        $this->log(LogLevel::INFO, $message, $context);
    }

    /**
     * Detailed debug information.
     *
     * @param string $message
     * @param array  $context
     *
     * @return null
     */
    public function debug($message, array $context = array())
    {
        $this->log(LogLevel::DEBUG, $message, $context);
    }
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Psr\Log {

/**
 * This Logger can be used to avoid conditional log calls.
 *
 * Logging should always be optional, and if no logger is provided to your
 * library creating a NullLogger instance to have something to throw logs at
 * is a good way to avoid littering your code with `if ($this->logger) { }`
 * blocks.
 */
class NullLogger extends AbstractLogger
{
    /**
     * Logs with an arbitrary level.
     *
     * @param mixed  $level
     * @param string $message
     * @param array  $context
     *
     * @return null
     */
    public function log($level, $message, array $context = array())
    {
        // noop
    }
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Psr\Log {

/**
 * Describes log levels.
 */
class LogLevel
{
    const EMERGENCY = 'emergency';
    const ALERT     = 'alert';
    const CRITICAL  = 'critical';
    const ERROR     = 'error';
    const WARNING   = 'warning';
    const NOTICE    = 'notice';
    const INFO      = 'info';
    const DEBUG     = 'debug';
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\PackagingMap {

class Map {
    /** @var string */
    protected $packer;
    /** @var Reserved[] */
    protected $reserved;

    /**
     * @param $packer
     * @param Reserved[] $reserved
     */
    public function __construct($packer, $reserved) {
        $this->packer = $packer;
        $this->reserved = $reserved;
    }

    /**
     * @return Reserved[]
     */
    public function getReservedSpace() {
        return $this->reserved;
    }

    /**
     * @param array $data
     * @return Map
     */
    public static function createFromArray($data) {
        $reserved = array_map(function($row) {
            return new Reserved(
                $row['offset_length'], $row['offset_width'], $row['offset_height'],
                $row['length'], $row['width'], $row['height'], $row['description']
            );
        }, $data['reserved']);
        return new self($data['packer'], $reserved);
    }

    /**
     * @return array
     */
    public function toArray() {
        return array(
            'packer' => $this->packer,
            'reserved' => array_map(function($reserved) {
                /** @var Reserved $reserved */
                return array(
                    'offset_length' => $reserved->getOffsetLength(),
                    'offset_width' => $reserved->getOffsetWidth(),
                    'offset_height' => $reserved->getOffsetHeight(),
                    'length' => $reserved->getLength(),
                    'width' => $reserved->getWidth(),
                    'height' => $reserved->getHeight(),
                    'description' => $reserved->getDescription()
                );
            }, $this->reserved)
        );
    }
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\PackagingMap {

class Reserved {
    /** @var float */
    protected $offsetLength;
    /** @var float */
    protected $offsetWidth;
    /** @var float */
    protected $offsetHeight;
    /** @var float */
    protected $length;
    /** @var float */
    protected $width;
    /** @var float */
    protected $height;
    /** @var string */
    protected $description;

    public function __construct($offsetLength, $offsetWidth, $offsetHeight, $length, $width, $height, $description) {
        $this->offsetLength = $offsetLength;
        $this->offsetWidth = $offsetWidth;
        $this->offsetHeight = $offsetHeight;
        $this->length = $length;
        $this->width = $width;
        $this->height = $height;
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @return float
     */
    public function getHeight() {
        return $this->height;
    }

    /**
     * @return float
     */
    public function getLength() {
        return $this->length;
    }

    /**
     * @return float
     */
    public function getOffsetHeight() {
        return $this->offsetHeight;
    }

    /**
     * @return float
     */
    public function getOffsetLength() {
        return $this->offsetLength;
    }

    /**
     * @return float
     */
    public function getOffsetWidth() {
        return $this->offsetWidth;
    }

    /**
     * @return float
     */
    public function getWidth() {
        return $this->width;
    }


}
}

/**
 * Box packing (3D bin packing, knapsack problem)
 * @package BoxPacker
 * @author Doug Wright
 */
namespace CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker {

/**
 * An item to be packed
 * @author Doug Wright
 * @package BoxPacker
 */
interface Item
{

    /**
     * Item SKU etc
     * @return string
     */
    public function getDescription();

    /**
     * Item width in mm
     * @return int
     */
    public function getWidth();

    /**
     * Item length in mm
     * @return int
     */
    public function getLength();

    /**
     * Item depth in mm
     * @return int
     */
    public function getDepth();

    /**
     * Item weight in g
     * @return int
     */
    public function getWeight();

    /**
     * Item volume in mm^3
     * @return int
     */
    public function getVolume();

    /**
     * Does this item need to be kept flat / packed "this way up"?
     * @return bool
     */
    public function getKeepFlat();

}

}

/**
 * Box packing (3D bin packing, knapsack problem)
 * @package BoxPacker
 * @author Doug Wright
 */
namespace CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker {

/**
 * An item to be packed where additional constraints need to be considered. Only implement this interface if you actually
 * need this additional functionality as it will slow down the packing algorithm
 * @author Doug Wright
 * @package BoxPacker
 */
interface ConstrainedItem extends Item
{

    /**
     * Hook for user implementation of item-specific constraints, e.g. max <x> batteries per box
     *
     * @param ItemList $alreadyPackedItems
     * @param Box      $box
     *
     * @return bool
     */
    public function canBePackedInBox(ItemList $alreadyPackedItems, Box $box);

}

}

/**
 * Box packing (3D bin packing, knapsack problem)
 * @package BoxPacker
 * @author Doug Wright
 */
namespace CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker {

/**
 * List of items to be packed, ordered by volume
 * @author Doug Wright
 * @package BoxPacker
 */
class ItemList extends \SplMaxHeap
{

    /**
     * Compare elements in order to place them correctly in the heap while sifting up.
     * @see \SplMaxHeap::compare()
     */
    public function compare($itemA, $itemB)
    {
        if ($itemA->getVolume() > $itemB->getVolume()) {
            return 1;
        } elseif ($itemA->getVolume() < $itemB->getVolume()) {
            return -1;
        } else {
            return $itemA->getWeight() - $itemB->getWeight();
        }
    }

    /**
     * Get copy of this list as a standard PHP array
     * @return array
     */
    public function asArray()
    {
        $return = array();
        foreach (clone $this as $item) {
            $return[] = $item;
        }
        return $return;
    }
}
}

/**
 * Box packing (3D bin packing, knapsack problem)
 * @package BoxPacker
 * @author Doug Wright
 */
namespace CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker {

/**
 * A "box" (or envelope?) to pack items into
 * @author Doug Wright
 * @package BoxPacker
 */
interface Box
{

    /**
     * Reference for box type (e.g. SKU or description)
     * @return string
     */
    public function getReference();

    /**
     * Outer width in mm
     * @return int
     */
    public function getOuterWidth();

    /**
     * Outer length in mm
     * @return int
     */
    public function getOuterLength();

    /**
     * Outer depth in mm
     * @return int
     */
    public function getOuterDepth();

    /**
     * Empty weight in g
     * @return int
     */
    public function getEmptyWeight();

    /**
     * Inner width in mm
     * @return int
     */
    public function getInnerWidth();

    /**
     * Inner length in mm
     * @return int
     */
    public function getInnerLength();

    /**
     * Inner depth in mm
     * @return int
     */
    public function getInnerDepth();

    /**
     * Total inner volume of packing in mm^3
     * @return int
     */
    public function getInnerVolume();

    /**
     * Max weight the packaging can hold in g
     * @return int
     */
    public function getMaxWeight();
}
}

/**
 * Box packing (3D bin packing, knapsack problem)
 * @package BoxPacker
 * @author Doug Wright
 */
namespace CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker {

/**
 * List of boxes available to put items into, ordered by volume
 * @author Doug Wright
 * @package BoxPacker
 */
class BoxList extends \SplMinHeap
{
    /**
     * Compare elements in order to place them correctly in the heap while sifting up.
     * @see \SplMinHeap::compare()
     */
    public function compare($boxA, $boxB)
    {
        if ($boxB->getInnerVolume() > $boxA->getInnerVolume()) {
            return 1;
        } elseif ($boxB->getInnerVolume() < $boxA->getInnerVolume()) {
            return -1;
        } else {
            return 0;
        }
    }
}
}

/**
 * Box packing (3D bin packing, knapsack problem)
 * @package BoxPacker
 * @author Doug Wright
 */
namespace CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\PackagingMap\Map;

/**
 * A "box" with items
 * @author Doug Wright
 * @package BoxPacker
 */
class PackedBox
{

    /**
     * Box used
     * @var Box
     */
    protected $box;

    /**
     * Items in the box
     * @var ItemList
     */
    protected $items;

    /** @var Map|null */
    protected $_packagingMap;

    /**
     * Total weight of box
     * @var int
     */
    protected $weight;

    /**
     * Remaining width inside box for another item
     * @var int
     */
    protected $remainingWidth;

    /**
     * Remaining length inside box for another item
     * @var int
     */
    protected $remainingLength;

    /**
     * Remaining depth inside box for another item
     * @var int
     */
    protected $remainingDepth;

    /**
     * Remaining weight inside box for another item
     * @var int
     */
    protected $remainingWeight;

    /**
     * Used width inside box for packing items
     * @var int
     */
    protected $usedWidth;

    /**
     * Used length inside box for packing items
     * @var int
     */
    protected $usedLength;

    /**
     * Used depth inside box for packing items
     * @var int
     */
    protected $usedDepth;

    /**
     * Get box used
     * @return Box
     */
    public function getBox()
    {
        return $this->box;
    }

    /**
     * Get items packed
     * @return ItemList
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Get packed weight
     * @return int weight in grams
     */
    public function getWeight()
    {

        if (!is_null($this->weight)) {
            return $this->weight;
        }

        $this->weight = $this->box->getEmptyWeight();
        $items = clone $this->items;
        foreach ($items as $item) {
            $this->weight += $item->getWeight();
        }
        return $this->weight;
    }

    /**
     * Get remaining width inside box for another item
     * @return int
     */
    public function getRemainingWidth()
    {
        return $this->remainingWidth;
    }

    /**
     * Get remaining length inside box for another item
     * @return int
     */
    public function getRemainingLength()
    {
        return $this->remainingLength;
    }

    /**
     * Get remaining depth inside box for another item
     * @return int
     */
    public function getRemainingDepth()
    {
        return $this->remainingDepth;
    }

    /**
     * Used width inside box for packing items
     * @return int
     */
    public function getUsedWidth()
    {
        return $this->usedWidth;
    }

    /**
     * Used length inside box for packing items
     * @return int
     */
    public function getUsedLength()
    {
        return $this->usedLength;
    }

    /**
     * Used depth inside box for packing items
     * @return int
     */
    public function getUsedDepth()
    {
        return $this->usedDepth;
    }

    /**
     * Get remaining weight inside box for another item
     * @return int
     */
    public function getRemainingWeight()
    {
        return $this->remainingWeight;
    }

    /**
     * Get volume utilisation of the packed box
     * @return float
     */
    public function getVolumeUtilisation()
    {
        $itemVolume = 0;

        /** @var Item $item */
        foreach (clone $this->items as $item) {
            $itemVolume += $item->getVolume();
        }

        return round($itemVolume / $this->box->getInnerVolume() * 100, 1);
    }

    /**
     * @return null|Map
     */
    public function getPackagingMap() {
        return $this->_packagingMap;
    }

    /**
     * Constructor
     *
     * @param Box      $box
     * @param ItemList $itemList
     * @param int      $remainingWidth
     * @param int      $remainingLength
     * @param int      $remainingDepth
     * @param int      $remainingWeight
     * @param int      $usedWidth
     * @param int      $usedLength
     * @param int      $usedDepth
     * @param Map      $packagingMap
     */
    public function __construct(
        Box $box,
        ItemList $itemList,
        $remainingWidth,
        $remainingLength,
        $remainingDepth,
        $remainingWeight,
        $usedWidth,
        $usedLength,
        $usedDepth,
        $packagingMap
    )
    {
        $this->box = $box;
        $this->items = $itemList;
        $this->remainingWidth = $remainingWidth;
        $this->remainingLength = $remainingLength;
        $this->remainingDepth = $remainingDepth;
        $this->remainingWeight = $remainingWeight;
        $this->usedWidth = $usedWidth;
        $this->usedLength = $usedLength;
        $this->usedDepth = $usedDepth;
        $this->_packagingMap = $packagingMap;
    }
}
}

/**
 * Box packing (3D bin packing, knapsack problem)
 * @package BoxPacker
 * @author Doug Wright
 */
namespace CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker {

/**
 * List of possible packed box choices, ordered by utilisation (item count, volume)
 * @author Doug Wright
 * @package BoxPacker
 */
class PackedBoxList extends \SplMinHeap
{

    /**
     * Average (mean) weight of boxes
     * @var float
     */
    protected $meanWeight;

    /**
     * Compare elements in order to place them correctly in the heap while sifting up.
     * @see \SplMinHeap::compare()
     */
    public function compare($boxA, $boxB)
    {
        $choice = $boxA->getItems()->count() - $boxB->getItems()->count();
        if ($choice === 0) {
            $choice = $boxB->getBox()->getInnerVolume() - $boxA->getBox()->getInnerVolume();
        }
        if ($choice === 0) {
            $choice = $boxA->getWeight() - $boxB->getWeight();
        }
        return $choice;
    }

    /**
     * Reversed version of compare
     * @return int
     */
    public function reverseCompare($boxA, $boxB)
    {
        $choice = $boxB->getItems()->count() - $boxA->getItems()->count();
        if ($choice === 0) {
            $choice = $boxA->getBox()->getInnerVolume() - $boxB->getBox()->getInnerVolume();
        }
        if ($choice === 0) {
            $choice = $boxB->getWeight() - $boxA->getWeight();
        }
        return $choice;
    }

    /**
     * Calculate the average (mean) weight of the boxes
     * @return float
     */
    public function getMeanWeight()
    {

        if (!is_null($this->meanWeight)) {
            return $this->meanWeight;
        }

        foreach (clone $this as $box) {
            $this->meanWeight += $box->getWeight();
        }

        return $this->meanWeight /= $this->count();

    }

    /**
     * Calculate the variance in weight between these boxes
     * @return float
     */
    public function getWeightVariance()
    {
        $mean = $this->getMeanWeight();

        $weightVariance = 0;
        foreach (clone $this as $box) {
            $weightVariance += pow($box->getWeight() - $mean, 2);
        }

        return $weightVariance / $this->count();

    }

    /**
     * Get volume utilisation of the set of packed boxes
     * @return float
     */
    public function getVolumeUtilisation()
    {
        $itemVolume = 0;
        $boxVolume = 0;

        /** @var PackedBox $box */
        foreach (clone $this as $box) {
            $boxVolume += $box->getBox()->getInnerVolume();

            /** @var Item $item */
            foreach (clone $box->getItems() as $item) {
                $itemVolume += $item->getVolume();
            }
        }

        return round($itemVolume / $boxVolume * 100, 1);
    }

    /**
     * Do a bulk insert
     * @param array $boxes
     */
    public function insertFromArray(array $boxes)
    {
        foreach ($boxes as $box) {
            $this->insert($box);
        }
    }
}
}

/**
 * Box packing (3D bin packing, knapsack problem)
 * @package BoxPacker
 * @author Doug Wright
 */
namespace CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker {

/**
 * An item to be packed
 * @author Doug Wright
 * @package BoxPacker
 */
class OrientatedItem
{

    /**
     * @var Item
     */
    protected $item;

    /**
     * @var int
     */
    protected $width;

    /**
     * @var int
     */
    protected $length;

    /**
     * @var int
     */
    protected $depth;

    /**
     * Constructor.
     * @param Item $item
     * @param int $width
     * @param int $length
     * @param int $depth
     */
    public function __construct(Item $item, $width, $length, $depth) {
        $this->item = $item;
        $this->width = $width;
        $this->length = $length;
        $this->depth = $depth;
    }

    /**
     * Item
     *
     * @return Item
     */
    public function getItem() {
        return $this->item;
    }

    /**
     * Item width in mm in it's packed orientation
     *
     * @return int
     */
    public function getWidth() {
        return $this->width;
    }

    /**
     * Item length in mm in it's packed orientation
     *
     * @return int
     */
    public function getLength() {
        return $this->length;
    }

    /**
     * Item depth in mm in it's packed orientation
     *
     * @return int
     */
    public function getDepth() {
        return $this->depth;
    }

    /**
     * Is this orientation stable (low centre of gravity)
     * N.B. Assumes equal weight distribution
     *
     * @return bool
     */
    public function isStable() {
        return $this->getDepth() <= min($this->getLength(), $this->getWidth());
    }
}

}

/**
 * Box packing (3D bin packing, knapsack problem)
 * @package BoxPacker
 * @author Doug Wright
 */
namespace CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\Psr\Log\LoggerAwareInterface;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Psr\Log\LoggerInterface;

/**
 * Figure out orientations for an item and a given set of dimensions
 * @author Doug Wright
 * @package BoxPacker
 */
class OrientatedItemFactory implements LoggerAwareInterface
{
    /**
     * The logger instance.
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Sets a logger.
     * @param LoggerInterface $logger
     * @return null|void
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Get the best orientation for an item
     * @param Box $box
     * @param Item $item
     * @param OrientatedItem|null $prevItem
     * @param Item|null $nextItem
     * @param int $widthLeft
     * @param int $lengthLeft
     * @param int $depthLeft
     * @return OrientatedItem|false
     */
    public function getBestOrientation(Box $box, Item $item, OrientatedItem $prevItem = null, Item $nextItem = null, $widthLeft, $lengthLeft, $depthLeft) {

        $possibleOrientations = $this->getPossibleOrientations($item, $prevItem, $widthLeft, $lengthLeft, $depthLeft);
        $usableOrientations = $this->getUsableOrientations($possibleOrientations, $box, $item, $prevItem, $nextItem);

        $orientationFits = array();
        /** @var OrientatedItem $orientation */
        foreach ($usableOrientations as $o => $orientation) {
            $orientationFit = min($widthLeft - $orientation->getWidth(), $lengthLeft - $orientation->getLength());
            $orientationFits[$o] = $orientationFit;
        }

        if (!empty($orientationFits)) {
            asort($orientationFits);
            reset($orientationFits);
            $bestFit = $usableOrientations[key($orientationFits)];
            $this->logger->debug("Selected best fit orientation", array('orientation' => $bestFit));
            return $bestFit;
        } else {
            return false;
        }
    }

    /**
     * Find all possible orientations for an item
     * @param Item $item
     * @param OrientatedItem|null $prevItem
     * @param int $widthLeft
     * @param int $lengthLeft
     * @param int $depthLeft
     * @return OrientatedItem[]
     */
    public function getPossibleOrientations(Item $item, OrientatedItem $prevItem = null, $widthLeft, $lengthLeft, $depthLeft) {

        $orientations = array();

        //Special case items that are the same as what we just packed - keep orientation
        if ($prevItem && $prevItem->getItem() == $item) {
            $orientations[] = new OrientatedItem($item, $prevItem->getWidth(), $prevItem->getLength(), $prevItem->getDepth());
        } else {

            //simple 2D rotation
            $orientations[] = new OrientatedItem($item, $item->getWidth(), $item->getLength(), $item->getDepth());
            $orientations[] = new OrientatedItem($item, $item->getLength(), $item->getWidth(), $item->getDepth());

            //add 3D rotation if we're allowed
            if (!$item->getKeepFlat()) {
                $orientations[] = new OrientatedItem($item, $item->getWidth(), $item->getDepth(), $item->getLength());
                $orientations[] = new OrientatedItem($item, $item->getLength(), $item->getDepth(), $item->getWidth());
                $orientations[] = new OrientatedItem($item, $item->getDepth(), $item->getWidth(), $item->getLength());
                $orientations[] = new OrientatedItem($item, $item->getDepth(), $item->getLength(), $item->getWidth());
            }
        }

        //remove any that simply don't fit
        return array_filter($orientations, function(OrientatedItem $i) use ($widthLeft, $lengthLeft, $depthLeft) {
            return $i->getWidth() <= $widthLeft && $i->getLength() <= $lengthLeft && $i->getDepth() <= $depthLeft;
        });
    }

    /**
     * @param OrientatedItem[] $possibleOrientations
     * @param Box              $box
     * @param Item             $item
     * @param OrientatedItem   $prevItem
     * @param Item             $nextItem
     *
     * @return array
     */
    protected function getUsableOrientations(
        $possibleOrientations,
        Box $box,
        Item $item,
        OrientatedItem $prevItem = null,
        Item $nextItem = null
    ) {
        /*
         * Divide possible orientations into stable (low centre of gravity) and unstable (high centre of gravity)
         */
        $stableOrientations = array();
        $unstableOrientations = array();

        foreach ($possibleOrientations as $o => $orientation) {
            if ($orientation->isStable()) {
                $stableOrientations[] = $orientation;
            } else {
                $unstableOrientations[] = $orientation;
            }
        }

        $orientationsToUse = array();

        /*
         * We prefer to use stable orientations only, but allow unstable ones if either
         * the item is the last one left to pack OR
         * the item doesn't fit in the box any other way
         */
        if (count($stableOrientations) > 0) {
            $orientationsToUse = $stableOrientations;
        } else if (count($unstableOrientations) > 0) {
            $orientationsInEmptyBox = $this->getPossibleOrientations(
                $item,
                $prevItem,
                $box->getInnerWidth(),
                $box->getInnerLength(),
                $box->getInnerDepth()
            );

            $stableOrientationsInEmptyBox = array_filter(
                $orientationsInEmptyBox,
                function(OrientatedItem $orientation) {
                    return $orientation->isStable();
                }
            );

            if (is_null($nextItem) || count($stableOrientationsInEmptyBox) == 0) {
                $orientationsToUse = $unstableOrientations;
            }
        }

        return $orientationsToUse;
    }
}

}


/**
 * Box packing (3D bin packing, knapsack problem)
 * @package BoxPacker
 * @author Doug Wright
 */
namespace CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker {

/**
 * Class ItemTooLargeException
 * Exception used when an item is too large to pack
 *
 * @package \CanadapostSmartFlexibleRootNS\DVDoug\BoxPacker
 */
class ItemTooLargeException extends \RuntimeException
{

    /** @var Item */
    public $item;

    /**
     * ItemTooLargeException constructor.
     *
     * @param string $message
     * @param Item   $item
     */
    public function __construct($message, Item $item) {
        $this->item = $item;
        parent::__construct($message);
    }

    /**
     * @return Item
     */
    public function getItem() {
        return $this->item;
    }

}
}

/**
 * Box packing (3D bin packing, knapsack problem)
 * @package BoxPacker
 * @author Doug Wright
 */
namespace CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\PackagingMap\Map;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Psr\Log\LoggerAwareInterface;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Psr\Log\LoggerInterface;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Psr\Log\NullLogger;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\PackagingMap\Reserved;

/**
 * Actual packer
 * @author Doug Wright
 * @package BoxPacker
 */
class VolumePacker implements LoggerAwareInterface
{
    /**
     * The logger instance.
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Sets a logger.
     * @param LoggerInterface $logger
     * @return null|void
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Box to pack items into
     * @var Box
     */
    protected $box;

    /**
     * List of items to be packed
     * @var ItemList
     */
    protected $items;

    /**
     * Remaining width of the box to pack items into
     * @var int
     */
    protected $widthLeft;

    /**
     * Remaining length of the box to pack items into
     * @var int
     */
    protected $lengthLeft;

    /**
     * Remaining depth of the box to pack items into
     * @var int
     */
    protected $depthLeft;

    /**
     * Remaining weight capacity of the box
     * @var int
     */
    protected $remainingWeight;

    /**
     * Used width inside box for packing items
     * @var int
     */
    protected $usedWidth = 0;

    /**
     * Used length inside box for packing items
     * @var int
     */
    protected $usedLength = 0;

    /**
     * Used depth inside box for packing items
     * @var int
     */
    protected $usedDepth = 0;

    /** @var Reserved[] */
    protected $_reservedSpace = array();
    /** @var int */
    protected $_offsetLength, $_offsetWidth, $_offsetHeight;

    /**
     * @var int
     */
    protected $layerWidth = 0;

    /**
     * @var int
     */
    protected $layerLength = 0;

    /**
     * @var int
     */
    protected $layerDepth = 0;

    /**
     * Constructor
     *
     * @param Box      $box
     * @param ItemList $items
     */
    public function __construct(Box $box, ItemList $items)
    {
        $this->logger = new NullLogger();

        $this->box = $box;
        $this->items = $items;

        $this->depthLeft = $this->box->getInnerDepth();
        $this->remainingWeight = $this->box->getMaxWeight() - $this->box->getEmptyWeight();
        $this->widthLeft = $this->box->getInnerWidth();
        $this->lengthLeft = $this->box->getInnerLength();
    }

    /**
     * Pack as many items as possible into specific given box
     * @return PackedBox packed box
     */
    public function pack()
    {
        $this->logger->debug("[EVALUATING BOX] {$this->box->getReference()}");

        $packedItems = new ItemList;

        $this->layerWidth = $this->layerLength = $this->layerDepth = 0;

        $prevItem = null;

        while (!$this->items->isEmpty()) {

            $itemToPack = $this->items->extract();
            $nextItem = !$this->items->isEmpty() ? $this->items->top() : null;

            //skip items that are simply too heavy or too large
            if (!$this->checkConstraints($itemToPack, $packedItems, $prevItem, $nextItem)) {
                continue;
            }

            $this->_offsetLength = $this->box->getInnerLength() - $this->lengthLeft;
            $this->_offsetWidth = $this->box->getInnerWidth() - $this->widthLeft;
            $this->_offsetHeight = $this->box->getInnerDepth() - $this->depthLeft;
            $orientatedItem = $this->getOrientationForItem($itemToPack, $prevItem, $nextItem, $this->widthLeft, $this->lengthLeft, $this->depthLeft);

            if ($orientatedItem) {

                $packedItems->insert($orientatedItem->getItem());
                $this->remainingWeight -= $orientatedItem->getItem()->getWeight();

                $this->lengthLeft -= $orientatedItem->getLength();
                $this->layerLength += $orientatedItem->getLength();
                $this->layerWidth = max($orientatedItem->getWidth(), $this->layerWidth);

                $this->layerDepth = max($this->layerDepth, $orientatedItem->getDepth()); //greater than 0, items will always be less deep

                $this->usedLength = max($this->usedLength, $this->layerLength);
                $this->usedWidth = max($this->usedWidth, $this->layerWidth);

                //allow items to be stacked in place within the same footprint up to current layerdepth
                $stackableDepth = $this->layerDepth - $orientatedItem->getDepth();
                $this->_reservedSpace[] = new Reserved(
                    $this->_offsetLength,
                    $this->_offsetWidth,
                    $this->_offsetHeight,
                    $orientatedItem->getLength(),
                    $orientatedItem->getWidth(),
                    $orientatedItem->getDepth(),
                    $itemToPack->getDescription()
                );
                $this->tryAndStackItemsIntoSpace($packedItems, $prevItem, $nextItem, $orientatedItem->getWidth(), $orientatedItem->getLength(), $stackableDepth);

                $prevItem = $orientatedItem;

                if ($this->items->isEmpty()) {
                    $this->usedDepth += $this->layerDepth;
                }
            } else {

                $prevItem = null;

                if ($this->widthLeft >= min($itemToPack->getWidth(), $itemToPack->getLength()) && $this->isLayerStarted()) {
                    $this->logger->debug("No more fit in lengthwise, resetting for new row");
                    $this->lengthLeft += $this->layerLength;
                    $this->widthLeft -= $this->layerWidth;
                    $this->layerWidth = $this->layerLength = 0;
                    $this->items->insert($itemToPack);
                    continue;
                } elseif ($this->lengthLeft < min($itemToPack->getWidth(), $itemToPack->getLength()) || $this->layerDepth == 0) {
                    $this->logger->debug("doesn't fit on layer even when empty");
                    $this->usedDepth += $this->layerDepth;
                    continue;
                }

                $this->widthLeft = $this->layerWidth ? min(floor($this->layerWidth * 1.1), $this->box->getInnerWidth()) : $this->box->getInnerWidth();
                $this->lengthLeft = $this->layerLength ? min(floor($this->layerLength * 1.1), $this->box->getInnerLength()) : $this->box->getInnerLength();
                $this->depthLeft -= $this->layerDepth;
                $this->usedDepth += $this->layerDepth;

                $this->layerWidth = $this->layerLength = $this->layerDepth = 0;
                $this->logger->debug("doesn't fit, so starting next vertical layer");
                $this->items->insert($itemToPack);
            }
        }
        $this->logger->debug("done with this box");
        return new PackedBox(
            $this->box,
            $packedItems,
            $this->widthLeft,
            $this->lengthLeft,
            $this->depthLeft,
            $this->remainingWeight,
            $this->usedWidth,
            $this->usedLength,
            $this->usedDepth,
            new Map(get_class($this), $this->_reservedSpace)
        );
    }

    /**
     * @param Item $itemToPack
     * @param OrientatedItem|null $prevItem
     * @param Item|null $nextItem
     * @param int $maxWidth
     * @param int $maxLength
     * @param int $maxDepth
     *
     * @return OrientatedItem|false
     */
    protected function getOrientationForItem(
        Item $itemToPack,
        OrientatedItem $prevItem = null,
        Item $nextItem = null,
        $maxWidth, $maxLength,
        $maxDepth
    ) {
        $this->logger->debug(
            "evaluating item {$itemToPack->getDescription()} for fit",
            array(
                'item' => $itemToPack,
                'space' => array(
                    'maxWidth'    => $maxWidth,
                    'maxLength'   => $maxLength,
                    'maxDepth'    => $maxDepth,
                    'layerWidth'  => $this->layerWidth,
                    'layerLength' => $this->layerLength,
                    'layerDepth'  => $this->layerDepth
                )
            )
        );

        $orientatedItemFactory = new OrientatedItemFactory();
        $orientatedItemFactory->setLogger($this->logger);
        $orientatedItem = $orientatedItemFactory->getBestOrientation($this->box, $itemToPack, $prevItem, $nextItem, $maxWidth, $maxLength, $maxDepth);

        return $orientatedItem;
    }

    /**
     * Figure out if we can stack the next item vertically on top of this rather than side by side
     * Used when we've packed a tall item, and have just put a shorter one next to it
     *
     * @param ItemList       $packedItems
     * @param OrientatedItem $prevItem
     * @param Item           $nextItem
     * @param int            $maxWidth
     * @param int            $maxLength
     * @param int            $maxDepth
     */
    protected function tryAndStackItemsIntoSpace(
        ItemList $packedItems,
        OrientatedItem $prevItem = null,
        Item $nextItem = null,
        $maxWidth,
        $maxLength,
        $maxDepth
    ) {
        while (!$this->items->isEmpty() && $this->checkNonDimensionalConstraints($this->items->top(), $packedItems)) {
            $stackedItem = $this->getOrientationForItem(
                $this->items->top(),
                $prevItem,
                $nextItem,
                $maxWidth,
                $maxLength,
                $maxDepth
            );
            if ($stackedItem) {
                $this->_reservedSpace[] = new Reserved(
                    $this->_offsetLength,
                    $this->_offsetWidth,
                    $this->_offsetHeight + $this->layerDepth - $maxDepth,
                    $stackedItem->getLength(),
                    $stackedItem->getWidth(),
                    $stackedItem->getDepth(),
                    $this->items->top()->getDescription()
                );
                $this->remainingWeight -= $this->items->top()->getWeight();
                $maxDepth -= $stackedItem->getDepth();
                $packedItems->insert($this->items->extract());
            } else {
                break;
            }
        }
    }

    /**
     * @return bool
     */
    protected function isLayerStarted()
    {
        return $this->layerWidth > 0 && $this->layerLength > 0 && $this->layerDepth > 0;
    }


    /**
     * Check item generally fits into box
     *
     * @param Item            $itemToPack
     * @param ItemList  $packedItems
     * @param OrientatedItem|null $prevItem
     * @param Item|null       $nextItem
     *
     * @return bool
     */
    protected function checkConstraints(
        Item $itemToPack,
        ItemList $packedItems,
        OrientatedItem $prevItem = null,
        Item $nextItem = null
    ) {
        return $this->checkNonDimensionalConstraints($itemToPack, $packedItems) &&
               $this->checkDimensionalConstraints($itemToPack, $prevItem, $nextItem);
    }

    /**
     * As well as purely dimensional constraints, there are other constraints that need to be met
     * e.g. weight limits or item-specific restrictions (e.g. max <x> batteries per box)
     *
     * @param Item     $itemToPack
     * @param ItemList $packedItems
     *
     * @return bool
     */
    protected function checkNonDimensionalConstraints(Item $itemToPack, ItemList $packedItems)
    {
        $weightOK = $itemToPack->getWeight() <= $this->remainingWeight;

        if ($itemToPack instanceof ConstrainedItem) {
            return $weightOK && $itemToPack->canBePackedInBox(clone $packedItems, $this->box);
        }

        return $weightOK;
    }

    /**
     * Check the item physically fits in the box (at all)
     *
     * @param Item            $itemToPack
     * @param OrientatedItem|null $prevItem
     * @param Item|null       $nextItem
     *
     * @return bool
     */
    protected function checkDimensionalConstraints(Item $itemToPack, OrientatedItem $prevItem = null, Item $nextItem = null)
    {
        return !!$this->getOrientationForItem(
            $itemToPack,
            $prevItem,
            $nextItem,
            $this->box->getInnerWidth(),
            $this->box->getInnerLength(),
            $this->box->getInnerDepth()
        );
    }
}
}

/**
 * Box packing (3D bin packing, knapsack problem)
 * @package BoxPacker
 * @author Doug Wright
 */
namespace CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\Psr\Log\LoggerAwareInterface;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Psr\Log\LoggerInterface;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Psr\Log\LogLevel;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Psr\Log\NullLogger;

/**
 * Actual packer
 * @author Doug Wright
 * @package BoxPacker
 */
class WeightRedistributor implements LoggerAwareInterface
{

    /**
     * The logger instance.
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Sets a logger.
     * @param LoggerInterface $logger
     * @return null|void
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * List of box sizes available to pack items into
     * @var BoxList
     */
    protected $boxes;

    /**
     * Constructor
     */
    public function __construct(BoxList $boxList)
    {
        $this->boxes = clone $boxList;
        $this->logger = new NullLogger();
    }

    /**
     * Given a solution set of packed boxes, repack them to achieve optimum weight distribution
     *
     * @param PackedBoxList $originalBoxes
     * @return PackedBoxList
     */
    public function redistributeWeight(PackedBoxList $originalBoxes)
    {

        $targetWeight = $originalBoxes->getMeanWeight();
        $this->logger->log(LogLevel::DEBUG, "repacking for weight distribution, weight variance {$originalBoxes->getWeightVariance()}, target weight {$targetWeight}");

        $packedBoxes = new PackedBoxList;

        $overWeightBoxes = array();
        $underWeightBoxes = array();
        foreach (clone $originalBoxes as $packedBox) {
            $boxWeight = $packedBox->getWeight();
            if ($boxWeight > $targetWeight) {
                $overWeightBoxes[] = $packedBox;
            } elseif ($boxWeight < $targetWeight) {
                $underWeightBoxes[] = $packedBox;
            } else {
                $packedBoxes->insert($packedBox); //target weight, so we'll keep these
            }
        }

        do { //Keep moving items from most overweight box to most underweight box
            $tryRepack = false;
            $this->logger->log(LogLevel::DEBUG, 'boxes under/over target: ' . count($underWeightBoxes) . '/' . count($overWeightBoxes));

            foreach ($underWeightBoxes as $u => $underWeightBox) {
                $this->logger->log(LogLevel::DEBUG, 'Underweight Box ' . $u);
                foreach ($overWeightBoxes as $o => $overWeightBox) {
                    $this->logger->log(LogLevel::DEBUG, 'Overweight Box ' . $o);
                    $overWeightBoxItems = $overWeightBox->getItems()->asArray();

                    //For each item in the heavier box, try and move it to the lighter one
                    foreach ($overWeightBoxItems as $oi => $overWeightBoxItem) {
                        $this->logger->log(LogLevel::DEBUG, 'Overweight Item ' . $oi);
                        if ($underWeightBox->getWeight() + $overWeightBoxItem->getWeight() > $targetWeight) {
                            $this->logger->log(LogLevel::DEBUG, 'Skipping item for hindering weight distribution');
                            continue; //skip if moving this item would hinder rather than help weight distribution
                        }

                        $newItemsForLighterBox = clone $underWeightBox->getItems();
                        $newItemsForLighterBox->insert($overWeightBoxItem);

                        $newLighterBoxPacker = new Packer(); //we may need a bigger box
                        $newLighterBoxPacker->setBoxes($this->boxes);
                        $newLighterBoxPacker->setItems($newItemsForLighterBox);
                        $this->logger->log(LogLevel::INFO, "[ATTEMPTING TO PACK LIGHTER BOX]");
                        $newLighterBox = $newLighterBoxPacker->doVolumePacking()->extract();

                        if ($newLighterBox->getItems()->count() === $newItemsForLighterBox->count()) { //new item fits
                            $this->logger->log(LogLevel::DEBUG, 'New item fits');
                            unset($overWeightBoxItems[$oi]); //now packed in different box

                            $newHeavierBoxPacker = new Packer(); //we may be able to use a smaller box
                            $newHeavierBoxPacker->setBoxes($this->boxes);
                            $newHeavierBoxPacker->setItems($overWeightBoxItems);

                            $this->logger->log(LogLevel::INFO, "[ATTEMPTING TO PACK HEAVIER BOX]");
                            $newHeavierBoxes = $newHeavierBoxPacker->doVolumePacking();
                            if (count($newHeavierBoxes) > 1) { //found an edge case in packing algorithm that *increased* box count
                                $this->logger->log(LogLevel::INFO, "[REDISTRIBUTING WEIGHT] Abandoning redistribution, because new packing is less efficient than original");
                                return $originalBoxes;
                            }

                            $overWeightBoxes[$o] = $newHeavierBoxes->extract();
                            $underWeightBoxes[$u] = $newLighterBox;

                            $tryRepack = true; //we did some work, so see if we can do even better
                            usort($overWeightBoxes, array($packedBoxes, 'reverseCompare'));
                            usort($underWeightBoxes, array($packedBoxes, 'reverseCompare'));
                            break 3;
                        }
                    }
                }
            }
        } while ($tryRepack);

        //Combine back into a single list
        $packedBoxes->insertFromArray($overWeightBoxes);
        $packedBoxes->insertFromArray($underWeightBoxes);

        return $packedBoxes;
    }
}
}

/**
 * Box packing (3D bin packing, knapsack problem)
 * @package BoxPacker
 * @author Doug Wright
 */
namespace CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\Psr\Log\LoggerAwareInterface;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Psr\Log\LoggerInterface;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Psr\Log\LogLevel;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Psr\Log\NullLogger;

/**
 * Actual packer
 * @author Doug Wright
 * @package BoxPacker
 */
class Packer implements LoggerAwareInterface
{
    /**
     * The logger instance.
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Sets a logger.
     * @param LoggerInterface $logger
     * @return null|void
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    const MAX_BOXES_TO_BALANCE_WEIGHT = 12;

    /**
     * List of items to be packed
     * @var ItemList
     */
    protected $items;

    /**
     * List of box sizes available to pack items into
     * @var BoxList
     */
    protected $boxes;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->items = new ItemList();
        $this->boxes = new BoxList();

        $this->logger = new NullLogger();
    }

    /**
     * Add item to be packed
     * @param Item $item
     * @param int  $qty
     */
    public function addItem(Item $item, $qty = 1)
    {
        for ($i = 0; $i < $qty; $i++) {
            $this->items->insert($item);
        }
        $this->logger->log(LogLevel::INFO, "added {$qty} x {$item->getDescription()}");
    }

    /**
     * Set a list of items all at once
     * @param \Traversable|array $items
     */
    public function setItems($items)
    {
        if ($items instanceof ItemList) {
            $this->items = clone $items;
        } else {
            $this->items = new ItemList();
            foreach ($items as $item) {
                $this->items->insert($item);
            }
        }
    }

    /**
     * Add box size
     * @param Box $box
     */
    public function addBox(Box $box)
    {
        $this->boxes->insert($box);
        $this->logger->log(LogLevel::INFO, "added box {$box->getReference()}");
    }

    /**
     * Add a pre-prepared set of boxes all at once
     * @param BoxList $boxList
     */
    public function setBoxes(BoxList $boxList)
    {
        $this->boxes = clone $boxList;
    }

    /**
     * Pack items into boxes
     *
     * @return PackedBoxList
     */
    public function pack()
    {
        $packedBoxes = $this->doVolumePacking();

        //If we have multiple boxes, try and optimise/even-out weight distribution
        if ($packedBoxes->count() > 1 && $packedBoxes->count() < static::MAX_BOXES_TO_BALANCE_WEIGHT) {
            $redistributor = new WeightRedistributor($this->boxes);
            $redistributor->setLogger($this->logger);
            $packedBoxes = $redistributor->redistributeWeight($packedBoxes);
        }

        $this->logger->log(LogLevel::INFO, "packing completed, {$packedBoxes->count()} boxes");
        return $packedBoxes;
    }

    /**
     * Pack items into boxes using the principle of largest volume item first
     *
     * @throws ItemTooLargeException
     * @return PackedBoxList
     */
    public function doVolumePacking()
    {

        $packedBoxes = new PackedBoxList;

        //Keep going until everything packed
        while ($this->items->count()) {
            $boxesToEvaluate = clone $this->boxes;
            $packedBoxesIteration = new PackedBoxList;

            //Loop through boxes starting with smallest, see what happens
            while (!$boxesToEvaluate->isEmpty()) {
                $box = $boxesToEvaluate->extract();

                $volumePacker = new VolumePacker($box, clone $this->items);
                $volumePacker->setLogger($this->logger);
                $packedBox = $volumePacker->pack();
                if ($packedBox->getItems()->count()) {
                    $packedBoxesIteration->insert($packedBox);

                    //Have we found a single box that contains everything?
                    if ($packedBox->getItems()->count() === $this->items->count()) {
                        break;
                    }
                }
            }

            //Check iteration was productive
            if ($packedBoxesIteration->isEmpty()) {
                throw new ItemTooLargeException('Item ' . $this->items->top()->getDescription() . ' is too large to fit into any box', $this->items->top());
            }

            //Find best box of iteration, and remove packed items from unpacked list
            $bestBox = $packedBoxesIteration->top();
            $unPackedItems = $this->items->asArray();
            foreach (clone $bestBox->getItems() as $packedItem) {
                foreach ($unPackedItems as $unpackedKey => $unpackedItem) {
                    if ($packedItem === $unpackedItem) {
                        unset($unPackedItems[$unpackedKey]);
                        break;
                    }
                }
            }
            $unpackedItemList = new ItemList();
            foreach ($unPackedItems as $unpackedItem) {
                $unpackedItemList->insert($unpackedItem);
            }
            $this->items = $unpackedItemList;
            $packedBoxes->insert($bestBox);

        }

        return $packedBoxes;
    }
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible {

class Author {
    const Title = 'Drugoe';

    const ContactEmail = 'support@drugoe.de';

    const WebsiteUrl = 'https://drugoe.de/';

    const LicenseUrl = 'https://drugoe.de/kb/license';

    const PersonalAreaUrl = 'https://drugoe.de/personal';

    const LogoUrl = 'https://drugoe.de/logo/%s/%s/logo.png';

    const BugReportUrl = 'https://drugoe.de/bug/%s/%s/%s';

    public static $knowledgeBaseUrl = array(
        'uspslabel' => 'https://drugoe.de/kb/uspslabel'
    );
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible {

class Promise {
    /** @var string */
    private $id;
    /** @var array */
    private $data;

    /**
     * @param string $id
     * @param array $data
     */
    public function __construct($id, $data) {
        $this->id = $id;
        $this->data = $data;
    }

    /**
     * @param array $data
     * @return Promise
     */
    public static function createFromArray($data) {
        return new Promise($data['id'], $data['data']);
    }

    /**
     * @return array
     */
    public function toArray() {
        return array(
            'id' => $this->id,
            'data' => $this->data
        );
    }

    /**
     * @return string
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getData() {
        return $this->data;
    }

}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible {

class PromiseManager extends FileManager {
    const PromiseDir = 'smart-flexible/promise/';

    private static function getSafeId($id) {
        return preg_replace('/[^a-zA-Z0-9\-\_\.]/', '_', $id);
    }

    /**
     * @param string $id
     * @return null|string
     */
    private static function readRaw($id) {
        $path = DIR_CACHE . self::PromiseDir . self::getSafeId($id) . '.json';
        if (!file_exists($path)) {
            return null;
        }
        return file_get_contents($path);
    }

    /**
     * @param string $id
     * @return Promise|null
     */
    public static function load($id) {
        $data = self::readRaw($id);
        if ($data === null) {
            return null;
        }
        $promise = Promise::createFromArray(json_decode($data, true));
        if ($promise->getId() === $id) {
            return $promise;
        }
        return null;
    }

    /**
     * @param Promise $promise
     * @return int
     */
    public static function save($promise) {
        $old = self::load($promise->getId());
        if ($old) { // merge data if already exists
            $promise = new Promise($promise->getId(), array_merge(
                $old->getData(),
                $promise->getData()
            ));
        }
        $path = DIR_CACHE . self::PromiseDir;
        self::createFolder($path);
        return self::createFile(
            $path . self::getSafeId($promise->getId()) . '.json',
            json_encode($promise->toArray(), defined('JSON_PRETTY_PRINT') ? JSON_PRETTY_PRINT : 0)
        );
    }

}
}

namespace CanadapostSmartFlexibleRootNS\SmartFlexible {

abstract class FileManager {
    /**
     * Creates a directory
     * @param string $path
     * @return bool
     */
    protected static function createFolder($path) {
        if (!file_exists($path)) {
            return @mkdir($path, 0755, true);
        }
        return true;
    }

    /**
     * Creates a file
     * @param string $path
     * @param mixed $data
     * @return int bytes written
     */
    protected static function createFile($path, $data) {
        return @file_put_contents($path, $data);
    }
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\CoreFeatureChecker;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\CoreFeatureException;

class Debugger {
    const ShowNothingLogNothing = 'DEBUG_SHOW_NOTHING_LOG_NOTHING'; // 0
    const ShowNothingLogErrors = 'DEBUG_SHOW_NOTHING_LOG_ERRORS';
    const ShowNothingLogAll = 'DEBUG_SHOW_NOTHING_LOG_ALL'; // 1
    const ShowErrorsLogErrors = 'DEBUG_SHOW_ERRORS_LOG_ERRORS';
    const ShowErrorsLogAll = 'DEBUG_SHOW_ERROR_LOG_ALL'; // 2
    const Trace = 'DEBUG_TYPE_TRACE';
    const Error = 'DEBUG_TYPE_ERROR';
    public static $debugLevels = array(Debugger::ShowNothingLogNothing, Debugger::ShowNothingLogErrors,
        Debugger::ShowNothingLogAll, Debugger::ShowErrorsLogErrors, Debugger::ShowErrorsLogAll);

    /** @var Configuration */
    private $configuration;
    /** @var Object */
    private $log;

    /**
     * @param Configuration $configuration
     * @param Object $log
     * @throws CoreFeatureException
     */
    public function __construct($configuration, $log) {
        if (!$configuration instanceof Configuration) {
            throw new CoreFeatureException('Debug: incorrect configuration given');
        }
        if (!CoreFeatureChecker::hasMethod($log, 'write')) {
            throw new CoreFeatureException('Debug: incorrect logger given');
        }
        $this->configuration = $configuration;
        $this->log = $log;
    }

    /**
     * Writes to log depending on debug level
     * Argument 0 - debug type
     * Other - debug message data
     */
    public function debug() {
        $args = func_get_args();
        $debugType = array_shift($args);
        if ($this->configuration->get('debug') === Debugger::ShowNothingLogNothing) {
            return;
        }
        if (in_array(
            $this->configuration->get('debug'),
            array(Debugger::ShowNothingLogErrors, Debugger::ShowErrorsLogErrors),
            true
        )) { // log errors only
            if ($debugType !== Debugger::Error) {
                return;
            }
        }
        $result = array();
        foreach ($args as $arg) {
            if (is_array($arg) or is_object($arg) or in_array($arg, array(true, false, null), true)) {
                $result[] = var_export($arg, true);
            } else {
                $result[] = $arg;
            }
        }
        $this->log->write(implode(' ', $result));
    }
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\Ordered\OrderedBoxPacked;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Ordered\Shipment;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Ordered\PackagingList;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Origin\OriginPackage;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Packer\PackerBox;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Packer\PackerItem;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Packer\PackerItemConstrained;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Packer\PackerItemToPack;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Packer\PackerResult;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Packer\PackerResultBox;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker\Packer;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker\PackedBoxList;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker\ItemList;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker\PackedBox;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Packer\ValuableBox;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Address;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Number;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Time;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\CoreFeatureChecker;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\CoreFeatureException;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Packer\MultiPacker;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Packer\SimpleSameItemPacker;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Packer\RotatedPacker;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Packer\WeightBasedPacker;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Origin\OriginBox;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\CurrencyConverter;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\VersionChecker;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\ConfigurationException;

class BaseModel extends \Model {
    /** @var BaseModule */
    protected $module;
    /** @var BaseRatesProvider */
    protected $ratesProvider;
    /** @var BaseLabelsProvider */
    protected $labelsProvider;
    /** @var BaseValidationProvider */
    protected $validationProvider;
    /** @var Configuration */
    protected $configuration;
    /** @var Debugger */
    protected $debugger;
    /** @var CurrencyConverter */
    protected $converter;

    const Packer3dPacker = 'PACKER-3D-PACKER';
    const PackerIndividual = 'PACKER-INDIVIDUAL';
    const PackerWeightBased = 'PACKER-WEIGHT-BASED';
    const Packer3dSimplifyLimit = 250;
    const SortByPrice = 'SORT_BY_PRICE';
    const SortByTime = 'SORT_BY_TIME';
    const ImperialMeasures = 'IMPERIAL_MEASURES';
    const MetricMeasures = 'METRIC_MEASURES';
    const StatusEnabled = 'STATUS_ENABLED';
    const StatusDisabled = 'STATUS_DISABLED';
    const StatusOnlyForAdmin = 'STATUS_ONLY_FOR_ADMIN_REQUESTS';
    const AdminRequestFlagConst = 'SmartFlexible_isAdminRequest';
    public static $imperialMeasuresCountries = array('US', 'MM', 'LR');
    public static $dateFormats = array('m/d/Y', 'd/m/Y', 'd.m.Y', 'j M Y');
    public static $measureSystems = array(self::ImperialMeasures, self::MetricMeasures);

    /**
     * @param mixed $registry
     * @param BaseModule $module
     */
    public function __construct($registry, $module) {
        parent::__construct($registry);
        $this->module = $module;
        $this->ratesProvider = $module->getRatesProvider();
        $this->labelsProvider = $module->getLabelsProvider();
        $this->validationProvider = $module->getValidationProvider();
        $this->configuration = new Configuration($this->config, $module);
        $this->debugger = new Debugger($this->configuration, $this->log);
        $this->converter = new CurrencyConverter(Array($this->currency, 'convert'), $this->config);
    }

    /**
     * Returns customer currency code depending on engine version
     * @throws CoreFeatureException
     * @return string
     */
    protected function getCustomerCurrencyCode() {
        if (CoreFeatureChecker::hasProperty($this, 'currency') &&
            CoreFeatureChecker::hasMethod($this->currency, 'getCode')
        ) { // v1
            return $this->currency->getCode();
        } elseif (CoreFeatureChecker::hasProperty($this, 'session')) { // v2
            return $this->session->data['currency'];
        } else {
            throw new CoreFeatureException('Can not find customer currency object');
        }
    }

    /**
     * Sets options to Rates Provider from config by given array of keys
     * Keys will be prefixed
     * @param array $keys
     */
    public function setRatesProviderOptionsFromConfig($keys) {
        foreach ($keys as $key) {
            $this->ratesProvider->setOption($key, $this->configuration->get($key));
        }
    }

    /**
     * Sets options to Validation Provider from config by given array of keys
     * Keys will be prefixed
     * @param array $keys
     */
    public function setValidationProviderOptionsFromConfig($keys) {
        foreach ($keys as $key) {
            $this->validationProvider->setOption($key, $this->configuration->get($key));
        }
    }

    /**
     * Sets options to Labels Provider from config by given array of keys
     * Keys will be prefixed
     * @param array $keys
     */
    public function setLabelsProviderOptionsFromConfig($keys) {
        foreach ($keys as $key) {
            $this->labelsProvider->setOption($key, $this->configuration->get($key));
        }
    }

    /**
     * Create extension response
     * @param string $title
     * @param array $offers
     * @param string $error
     * @return array
     */
    public function createResponse($title, $offers, $error = null) {
        if (!is_array($offers)) {
            $offers = array();
        }
        if (in_array( // show empty response if debug level is ShowNothing
            $this->configuration->get('debug'),
            array(Debugger::ShowNothingLogErrors, Debugger::ShowNothingLogNothing, Debugger::ShowNothingLogAll),
            true
        )) {
            $error = null;
        }
        if (count($offers) === 0 && !$error) {
            $this->debugger->debug(Debugger::Trace, $this->module->getServiceName() . ' Smart & Flexible:',
                'There were no rates and errors to show. Please check module settings.');
            return array();
        }
        $offers = is_array($offers) ? $offers : array();
        return array(
            'code'       => $this->module->getExtensionName(),
            'title'      => $title,
            'quote'      => $offers,
            'sort_order' => $this->configuration->get('sort_order'),
            'error'      => (string)$error
        );
    }

    /**
     * Shorthand: Create extension response on error
     * @param string $text
     * @return array
     */
    public function createErrorResponse($text) {
        $this->debugger->debug(Debugger::Error, $this->module->getServiceName() . ' Smart & Flexible:', $text);
        return $this->createResponse($this->language->get('text_title'), array(), $text);
    }

    /**
     * @return bool
     */
    public function isModuleEnabledForCustomerRequests() {
        if (!$this->configuration->get('status_for_customers')) { // admin only
            if (defined(BaseModel::AdminRequestFlagConst)) {
                return true;
            }
            return false;
        }
        return true;
    }

    /**
     * Checks that we can ship to address
     * @param array $address
     * @return bool
     */
    public function isAddressShippable($address) {
        if ($this->configuration->get('all_geo_zones')) {
            return true;
        }
        if (is_array($this->configuration->get('geo_zones'))) {
            $allowedZones = array_keys(array_filter($this->configuration->get('geo_zones'), function ($v) {
                return (bool)$v;
            }));
            if (count($allowedZones)) {
                $query = $this->db->query('
                    SELECT * FROM ' . DB_PREFIX . 'zone_to_geo_zone
                    WHERE geo_zone_id IN (' . implode(', ', $allowedZones) . ')
                        AND country_id = \'' . (int)$address['country_id'] . '\'
                        AND (zone_id = \'' . (int)$address['zone_id'] . '\'
                        OR zone_id = \'0\')
                ');
                if ($query->num_rows) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Checks that customer group is allowed to use this extension
     * @return bool
     */
    public function isCustomerGroupAllowed() {
        if ($this->configuration->get('all_customer_groups')) {
            return true;
        }
        $customerGroupId = $this->customer->getGroupId() ?: $this->config->get('config_customer_group_id');
        if (is_array($this->configuration->get('customer_groups'))) {
            $allowedGroups = array_keys(array_filter($this->configuration->get('customer_groups'), function ($v) {
                return (bool)$v;
            }));
            return in_array((int)$customerGroupId, $allowedGroups, true);
        }
        return false;
    }

    /**
     * Checks that store is allowed to use this extension
     * @return bool
     */
    public function isStoreAllowed() {
        if ($this->configuration->get('all_stores')) {
            return true;
        }
        $storeId = $this->config->get('config_store_id');
        if (is_array($this->configuration->get('stores'))) {
            $allowedStores = array_keys(array_filter($this->configuration->get('stores'), function ($v) {
                return (bool)$v;
            }));
            return in_array((int)$storeId, $allowedStores, true);
        }
        return false;
    }


    /**
     * Returns an array of custom fixed packages to attach to Packer
     * @param array $packagesData
     * @param float $maxWeight
     * @return PackerBox[]
     */
    public function createCustomPackagesFixed($packagesData, $maxWeight) {
        $result = array();
        foreach ($packagesData as $data) {
            $data['type'] = $this->module->getCustomPackageFixedType();
            $data['title'] = 'Custom ' . OriginPackage::getCustomPackageTitle($data['type']);
            $data['code'] = $this->ratesProvider->getStandardPackageFixedDefaultCode();
            $data['dimensions_inside'] = array($data['length'], $data['width'], $data['height']);
            $data['dimensions_outside'] = $data['dimensions_inside'];
            if (isset($data['tare'])) {
                $data['tare'] = (float)$data['tare'] === 0.0 ? null : $data['tare'];
            }
            $data['density'] = OriginPackage::getCustomPackageDensity($data['type']);
            $package = OriginPackage::createFromArray($data);
            $result = array_merge($result, $package->generatePackerBoxes($maxWeight, null));
        }
        return $result;
    }

    /**
     * Returns an array of custom expandable packages to attach to Packer
     * @param array $packagesData
     * @param float $maxWeight
     * @param float|null $maxHeight
     * @return PackerBox[]
     */
    public function createCustomPackagesExpanded($packagesData, $maxWeight, $maxHeight) {
        $result = array();
        foreach ($packagesData as $data) {
            $data['type'] = $this->module->getCustomPackageExpandableType();
            $data['code'] = $this->ratesProvider->getStandardPackageExpandableDefaultCode();
            $data['title'] = 'Custom ' . OriginPackage::getCustomPackageTitle($data['type']);
            if (isset($data['tare'])) {
                $data['tare'] = (float)$data['tare'] === 0.0 ? null : $data['tare'];
            }
            $data['density'] = OriginPackage::getCustomPackageDensity($data['type']);
            $package = OriginPackage::createFromArray($data);
            $result = array_merge($result, $package->generatePackerBoxes($maxWeight, $maxHeight));
        }
        return $result;
    }

    /**
     * Returns cart total of shippable products only
     * @param array $products Native array
     * @return float total in system currency
     */
    public function calculateShippableTotal($products) {
        $shippableProducts = array_filter($products, function($product) {
            return isset($product['shipping']) ? (bool)$product['shipping'] : false;
        });
        return array_sum(array_map(function($product) {
            return isset($product['total']) ? $product['total'] : ($product['price'] * $product['quantity']);
        }, $shippableProducts));
    }

    /**
     * Returns an array of products to attach to Packer
     * Warning: 'weight' - is the total weight for 'quantity' number of same items
     * Warning: 'price' - is the price for one item
     * @since 19.10.2016 product dimensions can be not defined when Packer is Weight Based
     * @since 04.08.2017 creates constrained items when max insurance value specified
     * @param array $products
     * @param float|null $maxInsuranceValue used for constrained items production
     * @throws PackagingException
     * @return PackerItemToPack[]
     */
    public function createItemsToPack($products, $maxInsuranceValue) {
        $result = array();
        $constrainedItemLogic =
            function(PackerItem $self, ItemList $alreadyPackedItems, PackerBox $box) use ($maxInsuranceValue) {
                $total = array_reduce($alreadyPackedItems->asArray(), function($carry, PackerItem $item) {
                    return $carry + $item->getPrice();
                }, 0);
                if ($total + $self->getPrice() > $maxInsuranceValue) {
                    return false;
                }
                return true;
            };
        foreach ($products as $product) {
            if (!(bool)$product['shipping']) {
                continue;
            }
            if ($this->configuration->get('packer') === self::PackerWeightBased) {
                $productLength = null;
                $productWidth = null;
                $productHeight = null;
            } else {
                if (!(float)$product['length']) {
                    throw new PackagingException('Length is not defined for item ' . $product['name']);
                }
                if (!(float)$product['width']) {
                    throw new PackagingException('Width is not defined for item ' . $product['name']);
                }
                if (!(float)$product['height']) {
                    throw new PackagingException('Height is not defined for item ' . $product['name']);
                }

                $dimensions = array($product['length'], $product['width'], $product['height']);
                rsort($dimensions, SORT_NUMERIC);
                list($product['length'], $product['width'], $product['height']) = $dimensions;

                $productLength = $this->length->convert(
                    $product['length'], $product['length_class_id'], $this->configuration->get('inches_id')
                );
                $productWidth = $this->length->convert(
                    $product['width'], $product['length_class_id'], $this->configuration->get('inches_id')
                );
                $productHeight = $this->length->convert(
                    $product['height'], $product['length_class_id'], $this->configuration->get('inches_id')
                );

                $productLength += $this->configuration->get('dimension_adj_fix');
                $productWidth += $this->configuration->get('dimension_adj_fix');
                $productHeight += $this->configuration->get('dimension_adj_fix');
            }

            if (!(float)$product['weight']) {
                throw new PackagingException('Weight is not defined for item ' . $product['name']);
            }

            $productWeight = $this->weight->convert(
                    $product['weight'], $product['weight_class_id'], $this->configuration->get('pounds_id')
                ) / $product['quantity'];

            $productWeight += $this->configuration->get('weight_adj_fix') +
                $productWeight * $this->configuration->get('weight_adj_per') / 100;

            $productOptions = array_map(function($option) {
                return $option['name'] . ': ' . (isset($option['value']) ? $option['value'] : $option['option_value']);
            }, isset($product['option']) ? $product['option'] : array());

            $productPrice = $this->converter->fromSystem($product['price'], $this->module->getValueCurrencyCode(
                $this->configuration->getOriginCountryCode($this->model_localisation_country)
            ));
            $customNames = $this->configuration->get('product_names');
            $originCountries = $this->configuration->get('product_countries');
            $originZones = $this->configuration->get('product_zones');
            $hsCodes = $this->configuration->get('product_hs_codes');
            $shortName = isset($customNames[(int)$product['product_id']]) ?
                $customNames[(int)$product['product_id']] : null;
            $hsCode = isset($hsCodes[(int)$product['product_id']]) ? $hsCodes[(int)$product['product_id']] : null;
            $originCountryId = isset($originCountries[(int)$product['product_id']]) ?
                $originCountries[(int)$product['product_id']] : null;
            $originZoneId = isset($originZones[(int)$product['product_id']]) ?
                $originZones[(int)$product['product_id']] : null;

            $constructorOptions = array(
                'id' => $product['product_id'],
                'description' => $product['name'],
                'short_description' => $shortName,
                'hs_code' => $hsCode,
                'origin_country_id' => $originCountryId,
                'origin_zone_id' => $originZoneId,
                'options' => $productOptions,
                'length' => $productLength,
                'width' => $productWidth,
                'height' => $productHeight,
                'weight' => $productWeight,
                'price' => $productPrice
            );
            if ($maxInsuranceValue) {
                $constructorOptions['logic'] = $constrainedItemLogic;
            }
            $result[] = new PackerItemToPack(
                $maxInsuranceValue ?
                    new PackerItemConstrained($constructorOptions) : new PackerItem($constructorOptions),
                $product['quantity']
            );
        }
        return $result;
    }

    /**
     * Returns promo item to attach to Packer
     * Promo item depends on total cost
     * @param float $totalCost
     * @return PackerItemToPack[]
     */
    public function createPromoItemsToPack($totalCost) {
        $promos = array_filter($this->configuration->get('promo'), function($promo) use ($totalCost) {
            return $promo['min_cost'] <= $totalCost;
        });
        $promo = array_pop($promos);
        if ($promo) {
            $dimensions = array($promo['length'], $promo['width'], $promo['height']);
            rsort($dimensions, SORT_NUMERIC);
            list($promo['length'], $promo['width'], $promo['height']) = $dimensions;
            return array(
                new PackerItemToPack(new PackerItem(array(
                    'id' => null,
                    'description' => 'Promotional Items and Gifts',
                    'options' => array(),
                    'length' => $promo['length'],
                    'width' => $promo['width'],
                    'height' => $promo['height'],
                    'weight' => $promo['weight'],
                    'price' => 0
                )), 1)
            );
        }
        return array();
    }

    /**
     * Checks that item does not exceed service weight and insurance limits
     * @since 09.08.2017 accepts Max Insurance Value
     * @param PackerItem $item
     * @param float $maxWeight
     * @param float|null $maxInsuranceValue
     * @throws PackagingException
     * @return bool
     */
    public function checkItemExceedance($item, $maxWeight, $maxInsuranceValue) {
        if ($item->getWeight() > $maxWeight) {
            throw new PackagingException('Item ' . $item->getDescription() . ' exceeds the ' .
                'package weight limit of ' . $maxWeight . 'lb');
        }
        if ($maxInsuranceValue) {
            if ($item->getPrice() > $maxInsuranceValue) {
                throw new PackagingException('Item ' . $item->getDescription() . ' exceeds the ' .
                    'package insurance limit of ' . $maxInsuranceValue . ' ' . $this->module->getValueCurrencyCode(
                        $this->configuration->getOriginCountryCode($this->model_localisation_country)
                    ));
            }
        }
        return false;
    }

    /**
     * Use 3D Packer to pack items in available boxes
     * @since 16.11.2017 accepts products native array to prevent multiply DB queries
     * @param array $address
     * @param array $products
     * @param float $maxWeight
     * @param float|null $maxHeight
     * @param float|null $maxInsuranceValue
     * @throws PackagingException
     * @return PackedBoxList[]
     */
    public function pack3D($address, $products, $maxWeight, $maxHeight, $maxInsuranceValue) {
        $result = array();
        $errors = array();
        $packagingVariants = $this->ratesProvider->getPackagingVariants(
            $address,
            $this->createCustomPackagesFixed($this->configuration->get('custom_packages'), $maxWeight),
            $this->createCustomPackagesExpanded($this->configuration->get('custom_envelopes'), $maxWeight, $maxHeight)
        );
        foreach ($packagingVariants as $allBoxes) {
            $allItems = array_merge(
                $this->createItemsToPack($products, $maxInsuranceValue),
                $this->createPromoItemsToPack($this->cart->getSubTotal()) // before taxes
            );
            $packer = new MultiPacker(array(new SimpleSameItemPacker()));
            $totalQuantity = array_sum(array_map(function($itemToPack) {
                /** @var PackerItemToPack $itemToPack */
                return $itemToPack->getQuantity();
            }, $allItems));
            if ($totalQuantity < self::Packer3dSimplifyLimit) {
                $packer->addPackers(array(new Packer(), new RotatedPacker()));
            }
            $tareWeightInc = $this->configuration->get('box_weight_adj_fix');
            $allBoxes = array_map(function ($box) use ($tareWeightInc) {
                /** @var PackerBox $box */
                return $box->adjustTareWeight($tareWeightInc);
            }, $allBoxes);
            array_map(array($packer, 'addBox'), $allBoxes);
            /** @var PackerItemToPack $itemToPack */
            foreach ($allItems as $itemToPack) {
                $this->checkItemExceedance($itemToPack->getItem(), $maxWeight, $maxInsuranceValue);
                $packer->addItem($itemToPack->getItem(), $itemToPack->getQuantity());
            }
            try {
                $result[] = $packer->pack();
            } catch (\Exception $error) {
                $errors[] = $error;
            }
        }
        if ($result) {
            return $result;
        } else {
            $error = array_shift($errors);
            throw new PackagingException($error->getMessage(), $error->getCode(), $error);
        }
    }

    /**
     * Creates individual boxes for each item
     * Returns array with one set of packed boxes
     * @since 22.07.2016 considers tare (weight of carton)
     * @since 16.11.2017 accepts products native array to prevent multiply DB queries
     * @param array $products
     * @param float $maxWeight
     * @param float|null $maxInsuranceValue
     * @throws PackagingException
     * @throws ConfigurationException
     * @return PackedBoxList[]
     */
    public function packIndividual($products, $maxWeight, $maxInsuranceValue) {
        $packedBoxes = new PackedBoxList();
        $type = $this->module->getCustomPackageFixedType();
        /** @var PackerItemToPack $itemToPack */
        foreach ($this->createItemsToPack($products, $maxInsuranceValue) as $itemToPack) {
            $item = $itemToPack->getItem();
            $quantity = $itemToPack->getQuantity();
            $dimensions = array($item->getLength(), $item->getWidth(), $item->getHeight());
            $this->checkItemExceedance($item, $maxWeight, $maxInsuranceValue);
            for ($i = 0; $i < $quantity; $i++) {
                $originPackage = OriginPackage::createFromArray(array(
                    'id' => null,
                    'type' => $type,
                    'code' => $this->ratesProvider->getStandardPackageFixedDefaultCode(),
                    'title' => 'Individual Custom ' . OriginPackage::getCustomPackageTitle($type),
                    'image' => null,
                    'dimensions_outside' => $dimensions,
                    'dimensions_inside' => $dimensions,
                    'density' => $this->configuration->get('individual_tare') ?
                        OriginPackage::getCustomPackageDensity($type) : null,
                    'tare' => $this->configuration->get('individual_tare') ? null : 0
                ));
                /** @var PackerBox $box */
                $box = current($originPackage->generatePackerBoxes($maxWeight, null));
                $box = $box->adjustTareWeight($this->configuration->get('box_weight_adj_fix'));
                $itemsList = new ItemList();
                $itemsList->insert($item);
                $packedBoxes->insert(new PackedBox(
                    $box, $itemsList, 0, 0, 0, 0,
                    $item->getWidth(), $item->getLength(), $item->getDepth(), null
                ));
            }
        }
        return array($packedBoxes);
    }

    /**
     * Packs items by service weight limit only
     * This method use zero tare weight but it can be adjusted
     * @since 16.11.2017 accepts products native array to prevent multiply DB queries
     * @param array $products
     * @param float $maxWeight
     * @param float|null $maxInsuranceValue
     * @throws PackagingException
     * @return PackedBoxList[]
     */
    public function packWeightBased($products, $maxWeight, $maxInsuranceValue) {
        $packer = new WeightBasedPacker();
        $originBox = new OriginBox(array(
            'code' => $this->ratesProvider->getStandardPackageFixedDefaultCode(),
            'title' => 'Weight-based Custom Box',
            'tare' => 0,
            'max_weight' => $this->configuration->get('weight_based_limit') ?: null
        ));
        /** @var PackerBox $box */
        $box = current($originBox->generatePackerBoxes($maxWeight, null));
        $box = $box->adjustTareWeight($this->configuration->get('box_weight_adj_fix'));
        $packer->addBox($box);
        foreach ($this->createItemsToPack($products, $maxInsuranceValue) as $itemToPack) {
            $item = $itemToPack->getItem();
            $quantity = $itemToPack->getQuantity();
            $this->checkItemExceedance($item, $maxWeight, $maxInsuranceValue);
            $packer->addItem($item, $quantity);
        }
        try {
            return array($packer->pack());
        } catch (\Exception $error) {
            throw new PackagingException($error->getMessage(), $error->getCode(), $error);
        }
    }

    /**
     * Packer switching method
     * @since 16.11.2017 accepts products native array to prevent multiply DB queries
     * @param array $address
     * @param array $products
     * @param float $maxWeight
     * @param float|null $maxHeight
     * @param float|null $maxInsuranceValue
     * @throws PackagingException
     * @return PackedBox[][]
     */
    public function pack($address, $products, $maxWeight, $maxHeight, $maxInsuranceValue) {
        switch ($this->configuration->get('packer')) {
            case self::Packer3dPacker:
                $variants = $this->pack3D($address, $products, $maxWeight, $maxHeight, $maxInsuranceValue);
                break;
            case self::PackerIndividual:
                $variants = $this->packIndividual($products, $maxWeight, $maxInsuranceValue);
                break;
            case self::PackerWeightBased:
                $variants = $this->packWeightBased($products, $maxWeight, $maxInsuranceValue);
                break;
            default:
                throw new PackagingException('Packer unknown or not defined');
        }
        // convert variants content: PackedBoxList (Heap) to Array
        return array_map('iterator_to_array', $variants);
    }

    /**
     * Prepares array of packed boxes data for further API requests
     * Calculates total weight of all boxes
     * @param PackedBox[] $packedBoxes
     * @return PackerResult
     */
    public function getPackerResult($packedBoxes) {
        $result = array();
        /** @var PackedBox $packedBox */
        foreach ($packedBoxes as $packedBox) {
            /**
             * @var PackerBox $boxType
             */
            $boxType = $packedBox->getBox();
            $itemsInTheBox = $packedBox->getItems()->asArray();
            $result[] = new PackerResultBox(
                $boxType, $packedBox->getWeight(), $itemsInTheBox, $packedBox->getPackagingMap()
            );
        }
        $this->debugger->debug(Debugger::Trace, 'Boxes:', $result);
        return new PackerResult($result);
    }

    /**
     * Checks that Rate delivery date is avoided by settings (saturday or sunday)
     * @param Rate $rate
     * @return bool
     */
    public function isRateDeliveryDateAvoided($rate) {
        if ($rate->isParticularDate()) {
            $weekday = intval(date('w', $rate->getTimeFrom()));
            if (($weekday === 6) && ($this->configuration->get('avoid_delivery_saturdays'))) {
                return true;
            }
            if (($weekday === 0) && ($this->configuration->get('avoid_delivery_sundays'))) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param Rate[] $rates
     * @param array $address
     * @param PackerResult[] $packerResults
     * @return Rate[] Updated rates
     */
    public function avoidDeliveryOnWeekends($rates, $address, $packerResults) {
        $_this = $this;
        for ($iteration = 1; $iteration <= 2; $iteration++) {
            $restrictedRates = array_filter($rates, function ($rate) use ($_this) {
                return $_this->isRateDeliveryDateAvoided($rate);
            });
            if (count($restrictedRates)) {
                $shippingTime = $this->getShippingTime($this->getTime(), $iteration);
                $restrictedRatesIds = array_map(function ($rate) {
                    /** @var Rate $rate */
                    return $rate->getId();
                }, $restrictedRates);
                try {
                    $newRates = $this->ratesProvider->queryRates($address, $packerResults, $shippingTime);
                    foreach ($newRates as $newRate) {
                        foreach ($rates as $k => $oldRate) {
                            if ($newRate->getId() === $oldRate->getId()) {
                                if (in_array($oldRate->getId(), $restrictedRatesIds, true)) {
                                    if (!$this->isRateDeliveryDateAvoided($newRate)) {
                                        $rates[$k] = $newRate;
                                    }
                                }
                            }
                        }
                    }
                } catch (\Exception $e) {
                    break; // limit reached
                }
            } else {
                break;
            }
        }
        return $rates;
    }

    /**
     * Sorts Rates
     * @param Rate[] $rates
     * @return Rate[] Sorted $rates
     */
    public function sortRates($rates) {
        if ($this->configuration->get('sort_options') === self::SortByPrice) {
            uasort($rates, function($a, $b) {
                /**
                 * @var Rate $a
                 * @var Rate $b
                 */
                return $a->getCost() <= $b->getCost() ? -1 : 1;
            });
        } elseif ($this->configuration->get('sort_options') === self::SortByTime) {
            uasort($rates, function($a, $b) {
                /**
                 * @var Rate $a
                 * @var Rate $b
                 */
                if ($a->getTimeFrom() < $b->getTimeFrom()) {
                    if ($a->getTimeFrom() === null) {
                        return 1;
                    }
                    return -1;
                } elseif ($a->getTimeFrom() === $b->getTimeFrom()) {
                    return $a->getTimeTo() <= $b->getTimeTo() ? -1 : 1;
                } else {
                    if ($b->getTimeFrom() === null) {
                        return -1;
                    }
                    return 1;
                }
            });
        }
        return $rates;
    }

    /**
     * For replacement in mock
     * @return int
     */
    public function getTime() {
        return time();
    }

    /**
     * @param int $now
     * @param int $moreProcessingDays
     * @return int
     */
    public function getShippingTime($now, $moreProcessingDays = 0) {
        return Time::addBusinessDays(array(
            'date' => Time::addDayIfTimeHasPassed(
                $now, $this->configuration->get('cutoff') === BaseRatesProvider::CutoffDisabled ? null :
                    $this->configuration->get('cutoff')
            ),
            'days' => intval($this->configuration->get('processing_days')) + $moreProcessingDays,
            'holidays' => $this->configuration->get('processing_holidays'),
            'skip_saturday' => $this->configuration->get('processing_saturdays'),
            'skip_sunday' => $this->configuration->get('processing_sundays')
        ));
    }

    /**
     * Returns the total weight of packed boxes (may be variable)
     * @param PackerResult[] $packerResults
     * @return string
     */
    public function getTotalWeight($packerResults) {
        $weights = array_map(function($packerResult) {
            /** @var PackerResult $packerResult */
            return $packerResult->getTotalWeight();
        }, $packerResults);
        $min = $this->weight->format(
            $this->weight->convert(
                min($weights),
                $this->configuration->get('pounds_id'),
                $this->config->get('config_weight_class_id')
            ), $this->config->get('config_weight_class_id')
        );
        $max = $this->weight->format(
            $this->weight->convert(
                max($weights),
                $this->configuration->get('pounds_id'),
                $this->config->get('config_weight_class_id')
            ), $this->config->get('config_weight_class_id')
        );
        return $min === $max ? $max : ($min . '—' . $max);
    }

    /**
     * Returns the total number of packed boxes (may be variable)
     * @param PackerResult[] $packerResults
     * @return string
     */
    public function getBoxesCount($packerResults) {
        $boxesCount = array_map(function($packerResult) {
            /** @var PackerResult $packerResult */
            return count($packerResult->getBoxes());
        }, $packerResults);
        $min = min($boxesCount);
        $max = max($boxesCount);
        return $min === $max ? (string)$max : ($min . '—' . $max);
    }

    /**
     * Changes adjustment settings using adjustment rules
     * @since 25.05.2017 prepares runtime configuration option for grouping rates
     * @param float $costInSystemCurrency
     * @param Rate $rate
     * @param float $totalInSystemCurrency
     */
    public function applyAdjustmentRules($costInSystemCurrency, $rate, $totalInSystemCurrency) {
        $grouping = $this->configuration->get('_grouping');
        if (!is_array($grouping)) {
            $grouping = array();
        }
        foreach ($this->configuration->get('adjustment_rules') as $rule) {
            if (($ruleCondition = $rule['condition']) && is_array($rule['actions'])) {
                if ($ruleCondition['conditions'] && is_array($ruleCondition['conditions'])) {
                    $areSatisfied = true;
                    foreach ($ruleCondition['conditions'] as $condition) {
                        $isSatisfied = true;
                        if ($condition['type'] === 'comparison') {
                            switch ($condition['argument']) {
                                case 'rate':
                                    $argument = $costInSystemCurrency;
                                    break;
                                case 'total':
                                    $argument = $totalInSystemCurrency;
                                    break;
                                default:
                                    $argument = null;
                            }
                            if ($argument) {
                                switch ($condition['operator']) {
                                    case 'gt':
                                        $isSatisfied = $argument > $condition['value'];
                                        break;
                                    case 'gte':
                                        $isSatisfied = $argument >= $condition['value'];
                                        break;
                                    case 'eq':
                                        $isSatisfied = (float)$argument === (float)$condition['value'];
                                        break;
                                    case 'lte':
                                        $isSatisfied = $argument <= $condition['value'];
                                        break;
                                    case 'lt':
                                        $isSatisfied = $argument < $condition['value'];
                                        break;
                                    default:
                                        $isSatisfied = false;
                                }
                            } else {
                                $isSatisfied = false;
                            }
                        } elseif ($condition['type'] === 'services') {
                            $isSatisfied = in_array($rate->getMethodFullCode(), $condition['services'], true) ||
                                in_array('', $condition['services'], true); // or any service
                        }
                        if ($ruleCondition['type'] === 'logical') {
                            if ($ruleCondition['operator'] === 'and') {
                                $areSatisfied = $areSatisfied && $isSatisfied;
                            } elseif ($ruleCondition['operator'] === 'or') {
                                $areSatisfied = $areSatisfied || $isSatisfied;
                            }
                        }
                    }
                    if ($areSatisfied) {
                        foreach ($rule['actions'] as $action) {
                            switch ($action['type']) {
                                case 'rateFixed':
                                    $this->configuration->set('rate_adj_fix', $action['value']);
                                    break;
                                case 'ratePercent':
                                    $this->configuration->set('rate_adj_per', $action['value']);
                                    break;
                            }
                        }
                        if (isset($rule['grouping'])) {
                            if (!isset($grouping[$rule['grouping']['title']])) {
                                $grouping[$rule['grouping']['title']] = array();
                            }
                            array_push($grouping[$rule['grouping']['title']], $rate->getId());
                        }
                    }
                }
            }
        }
        $this->configuration->set('_grouping', $grouping);
    }

    /**
     * Performs grouping rates using runtime configuration
     * @since 24.11.2017 uses cost before adjustment in system currency to detect cheapest rate
     * @param Rate[] $rates
     * @return Rate[]
     */
    public function applyGroupingRates($rates) {
        $protectedIds = array();
        uasort($rates, function($a, $b) { // issue #318: presort rates using cost before adj in system currency
            /**
             * @var Rate $a
             * @var Rate $b
             */
            return $a->getCostBeforeAdjustmentInSystemCurrency() <= $b->getCostBeforeAdjustmentInSystemCurrency() ?
                -1 : 1;
        });
        if (is_array($this->configuration->get('_grouping'))) {
            foreach ($this->configuration->get('_grouping') as $title => $rateIds) {
                /** @var Rate|null $minRate */
                $minRate = null;
                foreach ($rates as $k => $rate) {
                    if (in_array($rate->getId(), $rateIds, true) && !in_array($rate->getId(), $protectedIds, true)) {
                        if (!$minRate) {
                            $minRate = $rate;
                        } elseif ($rate->getCost() < $minRate->getCost()) {
                            $minRate = $rate;
                        }
                        unset($rates[$k]);
                    }
                }
                if ($minRate) {
                    $minRate->setTitle($title)->setIsTitleEncoded(false); // predefined in grouping rule
                    array_push($protectedIds, $minRate->getId());
                    array_push($rates, $minRate);
                }
            }
        }
        return $rates;
    }

    /**
     * Entry point method for rates
     * @since 22.11.2016 sets insurance option to rates provider
     * @param array $address
     * @return array
     */
    public function getQuote($address) {
        $this->language->load(
            (VersionChecker::get()->isVersion3() ? 'extension/' : '') .
            'shipping/' . $this->module->getExtensionName()
        );
        try {
            $address = Address::fixAddress($address, $this->session, $this->customer);
            $address = $this->module->fixAddress(
                $address, $this->model_localisation_country, $this->model_localisation_zone
            );
        } catch (\Exception $error) {
            return $this->createErrorResponse($error->getMessage());
        }

        if (!$this->isModuleEnabledForCustomerRequests()) {
            return $this->createErrorResponse('This shipping method is not enabled for customers');
        }
        if (!$this->isAddressShippable($address)) {
            return $this->createErrorResponse('Delivery to your Geo Zone is not enabled');
        }
        if (!$this->isCustomerGroupAllowed()) {
            return $this->createErrorResponse('This shipping method is not enabled for your group');
        }
        if (!$this->isStoreAllowed()) {
            return $this->createErrorResponse('This shipping method is not enabled for this store');
        }

        $maxWeight = $this->ratesProvider->getPackageMaxWeight($address);
        if (Number::floatsAreEqual($maxWeight, 0)) {
            return $this->createErrorResponse('Delivery to this address is not available (API)');
        }
        $maxHeight = $this->ratesProvider->getPackageExpandableMaxHeight();
        $products = $this->cart->getProducts(); // issue #315, prevent multiple DB queries
        $shippableTotal = $this->calculateShippableTotal($products);
        $shouldInsuranceBeIncluded = ValuableBox::shouldInsuranceBeIncluded( // using shippable total in system currency
            $shippableTotal, null, $this->configuration, null, null
        );
        $maxInsuranceValue = $shouldInsuranceBeIncluded ?
            $this->ratesProvider->getPackageMaxInsuranceValue($address) : null;
        $shippingTime = $this->getShippingTime($this->getTime());
        $fixedAdjustmentInitial = $this->configuration->get('rate_adj_fix');
        $percentAdjustmentInitial = $this->configuration->get('rate_adj_per');

        try {
            // get packaging variants
            $packedBoxesVariants = $this->pack($address, $products, $maxWeight, $maxHeight, $maxInsuranceValue);
            $packerResults = array_map(Array($this, 'getPackerResult'), $packedBoxesVariants);

            // validation request
            if ($this->validationProvider) {
                $this->validationProvider->setDebugger(Array($this->debugger, 'debug'));
                $address['is_residential'] = $this->validationProvider->isAddressResidential($address);
                $this->debugger->debug(Debugger::Trace, 'Residential address:', $address['is_residential']);
            }

            // set up rates provider
            /** @var PackerResult $firstPackerResult */
            $this->ratesProvider->setOption('insurance', $shouldInsuranceBeIncluded);
            $this->ratesProvider->setOption('value_currency_code', $this->module->getValueCurrencyCode(
                $this->configuration->getOriginCountryCode($this->model_localisation_country)
            ));
            $this->ratesProvider->setDebugger(Array($this->debugger, 'debug'));

            // fetch rates
            $rates = $this->ratesProvider->queryRates($address, $packerResults, $shippingTime);

            // check avoid delivery on saturdays/sundays, combine with additional rates requests
            $rates = $this->avoidDeliveryOnWeekends($rates, $address, $packerResults);

            // calculate total weight and boxes count
            $totalWeight = $this->getTotalWeight($packerResults);
            $boxesCount = $this->getBoxesCount($packerResults);

            $quotes = array();
            foreach ($rates as $rate) { // apply adjustment rules and collect grouping
                $costInSystemCurrency = $this->converter->toSystem($rate->getCost(), $rate->getCurrencyCode());
                $rate->setCostBeforeAdjustmentInSystemCurrency($costInSystemCurrency); // issue #318
                $this->configuration->set('rate_adj_fix', $fixedAdjustmentInitial);
                $this->configuration->set('rate_adj_per', $percentAdjustmentInitial);
                $this->applyAdjustmentRules($costInSystemCurrency, $rate, $shippableTotal);
                $costInSystemCurrency = Number::adjust(
                    $costInSystemCurrency,
                    $this->configuration->get('rate_adj_fix'),
                    $this->configuration->get('rate_adj_per')
                );
                $costInSystemCurrency = max($costInSystemCurrency, $this->configuration->get('minimum_rate'));
                $costInSystemCurrency = round($costInSystemCurrency, 2, PHP_ROUND_HALF_UP);
                $rate->setCost($costInSystemCurrency)->setCurrencyCode($this->converter->getSystemCurrency());
            }

            // apply grouping
            $rates = $this->applyGroupingRates($rates);

            // sort rates
            $rates = $this->sortRates($rates);

            foreach ($rates as $rate) { // decode titles and build quotes
                $finalId = $rate->getIdForEcommerceSystem();
                if ($rate->getIsTitleEncoded()) {
                    $decodedTitle = $this->language->get('text_' . $rate->getTitle());
                    if ($decodedTitle && $decodedTitle !== 'text_' . $rate->getTitle()) {
                        $rate->setTitle($decodedTitle);
                    }
                }
                $titleClean = $rate->getTitle();
                if ($this->configuration->get('display_time')) {
                    $eta = null;
                    if ($rate->isParticularDate()) {
                        $eta = date($this->configuration->get('date_format'), $rate->getTimeFrom());
                    } elseif ($rate->isDateInternal()) {
                        $eta = $rate->getTimeFrom() . ' — ' . $rate->getTimeTo() . ' ' .
                            $this->language->get('text_business_days');
                    }
                    if ($eta) {
                        $rate->setTitle(
                            $rate->getTitle() . ' (' . $this->language->get('text_eta') . ' ' . $eta . ')'
                        );
                    }
                }
                $quotes[$finalId] = array(
                    'code' => $this->module->getExtensionName() . '.' . $finalId,
                    'title' => ($this->configuration->get('prefix') ?
                            $this->configuration->get('prefix') . ' ' : '') . $rate->getTitle(),
                    'cost' => $rate->getCost(),
                    'tax_class_id' => $this->configuration->get('tax_class_id'),
                    'text' => $this->currency->format( // (and convert) using user currency
                        $this->tax->calculate( // using system currency
                            $rate->getCost(),
                            $this->configuration->get('tax_class_id'),
                            $this->config->get('config_tax')
                        ), $this->getCustomerCurrencyCode()
                    ),
                    'count' => count($rate->getBoxes()),
                    'id' => $finalId,
                    'time_from' => $rate->getTimeFrom(),
                    'time_to' => $rate->getTimeTo(),
                    'boxes_info' => array_map(function($box) {
                        /** @var PackerResultBox $box */
                        return $box->toOrdered()->toArray();
                    }, $rate->getBoxes()),
                    'id_clean' => $rate->getId(),
                    'title_clean' => $titleClean
                );
            }
            $title = $this->language->get('text_title');
            if ($this->configuration->get('display_weight')) {
                $title .= ' (' . $this->language->get('text_weight') . ' ' .
                    $totalWeight . ', ' . $this->language->get('text_boxes') . ' ' . $boxesCount . ')';
            }
            $response = $this->createResponse($title, $quotes);
            $this->debugger->debug(Debugger::Trace, 'Quote:', $response);
        } catch (\Exception $error) {
            return $this->createErrorResponse($error->getMessage());
        }
        return $response;
    }

    /**
     * Entry point for packaging list, labels and tracking numbers
     * @param int $orderId
     * @param array $boxesInfo serialized OrderedBoxPacked[]. See BaseModel::getQuote()::boxes_info
     * @param array $address
     * @param string $methodCode See BaseModel::getQuote()::id_clean (original method code)
     * @param string $methodName See BaseModel::getQuote()::title_clean
     * @return bool
     */
    public function createPackagingList($orderId, $boxesInfo, $address, $methodCode, $methodName) {
        $this->debugger->debug(Debugger::Trace, 'Creating Packaging List for order #' . $orderId);
        $this->debugger->debug(Debugger::Trace, 'Got this boxes information: ', $boxesInfo);
        $this->debugger->debug(Debugger::Trace, 'Got method: ', $methodCode, $methodName);
        $this->debugger->debug(Debugger::Trace, 'Got address: ', $address);
        $shipments = array_map(function($box) {
            return new Shipment(array(
                'box' => OrderedBoxPacked::createFromArray($box),
                'tracking_number' => null,
                'documents' => array(),
                'error_message' => null
            ));
        }, $boxesInfo);
        $packagingList = new PackagingList(array(
            'order_id' => $orderId,
            'service_name' => $this->module->getServiceName(),
            'version' => $this->module->getVersion(),
            'method_code' => $methodCode,
            'address' => $address,
            'method_name' => $methodName,
            'shipments' => $shipments
        ));
        if (!PackagingListManager::save($packagingList)) {
            $this->debugger->debug(
                Debugger::Error, $this->module->getServiceName() . ' Smart & Flexible:',
                'Packaging list: Can not save file'
            );
            return false;
        }
        return true;
    }

    /**
     * @param int $orderId
     * @return null|PackagingList
     */
    public function requestLabels($orderId) {
        if ($this->configuration->get('label') !== BaseLabelsProvider::LabelAutomatically) {
            return null;
        }
        $this->debugger->debug(Debugger::Trace, 'Requesting labels for order #' . $orderId);
        try {
            return PackagingListManager::requestLabels(
                $orderId, null, $this->module, Array($this->debugger, 'debug'), $this->validationProvider,
                $this->labelsProvider, $this->configuration, $this->converter,
                Array('country' => $this->model_localisation_country, 'zone' => $this->model_localisation_zone)
            );
        } catch (\Exception $error) {
            $this->debugger->debug(
                Debugger::Error, $this->module->getServiceName() . ' Smart & Flexible:', $error->getMessage()
            );
            return null;
        }
    }

}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\VersionChecker;

class BaseAdminModel extends \Model {
    /** @var BaseModule */
    protected $module;
    /** @var BaseRatesProvider */
    protected $ratesProvider;
    /** @var BaseLabelsProvider */
    protected $labelsProvider;
    /** @var Configuration */
    protected $configuration;
    /** @var Debugger */
    protected $debugger;

    /**
     * @param mixed $registry
     * @param BaseModule $module
     */
    public function __construct($registry, $module) {
        parent::__construct($registry);
        $this->module = $module;
        $this->ratesProvider = $module->getRatesProvider();
        $this->labelsProvider = $module->getLabelsProvider();
        $this->configuration = new Configuration($this->config, $module);
        $this->debugger = new Debugger($this->configuration, $this->log);
    }

    /**
     * @return int|null
     */
    public function getProductNameMaxLength() {
        if (!$this->labelsProvider) {
            return null;
        }
        return $this->labelsProvider->getProductNameMaxLength();
    }

    /**
     * @param int $productId
     * @return string|null
     */
    public function getProductName($productId) {
        $setting = $this->configuration->get('product_names');
        return $productId ? (isset($setting[$productId]) ? $setting[$productId] : null) : null;
    }

    /**
     * @param int $productId
     * @param string $name
     * @return string|null
     */
    public function setProductName($productId, $name) {
        if (!$this->labelsProvider) {
            return null;
        }
        $productId = (int)$productId;
        $this->load->model('setting/setting');
        $settings = $this->model_setting_setting->getSetting(
            (VersionChecker::get()->isVersion3() ? 'shipping_' : '') . $this->module->getExtensionName()
        );
        $newName = $this->labelsProvider->shortenProductName($name);
        if (strlen($newName)) {
            $settings[$this->module->getPrefixedName('product_names')][$productId] = $newName;
        } else {
            unset($settings[$this->module->getPrefixedName('product_names')][$productId]);
        }
        $this->model_setting_setting->editSetting(
            (VersionChecker::get()->isVersion3() ? 'shipping_' : '') . $this->module->getExtensionName(),
            $settings
        );
        return $newName;
    }

    /**
     * @param int $productId
     * @return int|null
     */
    public function getProductCountry($productId) {
        $setting = $this->configuration->get('product_countries');
        return $productId ? (isset($setting[$productId]) ? $setting[$productId] : null) : null;
    }

    /**
     * @param int $productId
     * @return int|null
     */
    public function getProductZone($productId) {
        $setting = $this->configuration->get('product_zones');
        return $productId ? (isset($setting[$productId]) ? $setting[$productId] : null) : null;
    }

    /**
     * @param int $productId
     * @param int $countryId
     * @return int|null
     */
    public function setProductCountry($productId, $countryId) {
        if (!$this->labelsProvider) {
            return null;
        }
        $productId = (int)$productId;
        $this->load->model('setting/setting');
        $settings = $this->model_setting_setting->getSetting(
            (VersionChecker::get()->isVersion3() ? 'shipping_' : '') . $this->module->getExtensionName()
        );
        if ($countryId) {
            $settings[$this->module->getPrefixedName('product_countries')][$productId] = $countryId;
        } else {
            unset($settings[$this->module->getPrefixedName('product_countries')][$productId]);
        }
        $this->model_setting_setting->editSetting(
            (VersionChecker::get()->isVersion3() ? 'shipping_' : '') . $this->module->getExtensionName(),
            $settings
        );
        return $countryId;
    }

    /**
     * @param int $productId
     * @param int $zoneId
     * @return int|null
     */
    public function setProductZone($productId, $zoneId) {
        if (!$this->labelsProvider) {
            return null;
        }
        $productId = (int)$productId;
        $this->load->model('setting/setting');
        $settings = $this->model_setting_setting->getSetting(
            (VersionChecker::get()->isVersion3() ? 'shipping_' : '') . $this->module->getExtensionName()
        );
        if ($zoneId) {
            $settings[$this->module->getPrefixedName('product_zones')][$productId] = $zoneId;
        } else {
            unset($settings[$this->module->getPrefixedName('product_zones')][$productId]);
        }
        $this->model_setting_setting->editSetting(
            (VersionChecker::get()->isVersion3() ? 'shipping_' : '') . $this->module->getExtensionName(),
            $settings
        );
        return $zoneId;
    }

    /**
     * @param int $productId
     * @return string|null
     */
    public function getProductHsCode($productId) {
        $setting = $this->configuration->get('product_hs_codes');
        return $productId ? (isset($setting[$productId]) ? $setting[$productId] : null) : null;
    }

    /**
     * @param int $productId
     * @param string $hsCode
     * @return string|null
     */
    public function setProductHsCode($productId, $hsCode) {
        if (!$this->labelsProvider) {
            return null;
        }
        $productId = (int)$productId;
        $this->load->model('setting/setting');
        $settings = $this->model_setting_setting->getSetting(
            (VersionChecker::get()->isVersion3() ? 'shipping_' : '') . $this->module->getExtensionName()
        );
        if ($hsCode) {
            $settings[$this->module->getPrefixedName('product_hs_codes')][$productId] = $hsCode;
        } else {
            unset($settings[$this->module->getPrefixedName('product_hs_codes')][$productId]);
        }
        $this->model_setting_setting->editSetting(
            (VersionChecker::get()->isVersion3() ? 'shipping_' : '') . $this->module->getExtensionName(),
            $settings
        );
        return $hsCode;
    }

}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\Author;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\CoreFeatureChecker;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\CoreFeatureException;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Ordered\PackagingList;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Origin\OriginBox;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Origin\OriginEnvelope;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Origin\OriginPackage;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Ordered\Shipment;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Ordered\OrderedBoxPackedItem;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping\BaseModel;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Address;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Arrays;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Label;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Scene;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Square;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Weight;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\VersionChecker;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Locale;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\CurrencyConverter;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping\BaseController\Helper;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping\BaseView;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Ordered\AccompanyingDocument;

abstract class BaseController extends \Controller {
    /** @var Configuration */
    protected $configuration;
    /** @var BaseModule */
    protected $module;
    /** @var BaseRatesProvider */
    protected $ratesProvider;
    /** @var null|BaseLabelsProvider  */
    protected $labelsProvider;
    /** @var null|BaseValidationProvider */
    protected $validationProvider;
    /** @var Debugger */
    protected $debugger;
    /** @var CurrencyConverter */
    protected $converter;
    protected $error = array();
    protected $data = array();

    /**
     * @param mixed $registry
     * @param BaseModule $module
     */
    public function __construct($registry, $module) {
        parent::__construct($registry);
        $this->module = $module;
        $this->ratesProvider = $module->getRatesProvider();
        $this->labelsProvider = $module->getLabelsProvider();
        $this->validationProvider = $module->getValidationProvider();
        $this->configuration = new Configuration($this->config, $module);
        $this->debugger = new Debugger($this->configuration, $this->log);
        $this->converter = new CurrencyConverter(Array($this->currency, 'convert'), $this->config);
        $this->language->load(
            (VersionChecker::get()->isVersion3() ? 'extension/' : '') .
            'shipping/' . $this->module->getExtensionName()
        );
    }

    public function getCurrentRoute() {
        return $this->request->get['route'];
    }

    public function getParentRoute() {
        return implode('/', array_slice(explode('/', $this->getCurrentRoute()), 0, -1));
    }

    public function getTokenName() {
        return VersionChecker::get()->isVersion3() ? 'user_token' : 'token';
    }

    public function link($path, $args = '') {
        return $this->url->link(
            $path, $this->getTokenName() . '=' . $this->session->data[$this->getTokenName()] . '&' . $args,
            VersionChecker::get()->isVersion1() ? 'SSL' : true
        );
    }

    public function getFrontUrl() {
        return isset($this->request->server['HTTPS']) ?
            ($this->request->server['HTTPS'] ? HTTPS_CATALOG : HTTP_CATALOG) : HTTP_CATALOG;
    }

    public function isModEnabled() {
        return $this->module->getVqmod() ? $this->module->getVqmod()->isEnabled() : false;
    }

    public function isModInstalled() {
        return CoreFeatureChecker::isModificationInstalled($this->module->getExtensionName());
    }

    /**
     * Sets options to Labels Provider from config by given array of keys
     * Keys will be prefixed
     * @param array $keys
     */
    public function setLabelsProviderOptionsFromConfig($keys) {
        foreach ($keys as $key) {
            $this->labelsProvider->setOption($key, $this->configuration->get($key));
        }
    }

    /**
     * Sets options to Labels Provider from config by given array of keys
     * Keys will be prefixed
     * @param array $keys
     */
    public function setValidationProviderOptionsFromConfig($keys) {
        foreach ($keys as $key) {
            $this->validationProvider->setOption($key, $this->configuration->get($key));
        }
    }

    /**
     * Returns POST value
     * @param string $key
     * @return string|null
     */
    public function getPost($key) {
        return isset($this->request->post[$this->module->getPrefixedName($key)]) ?
            $this->request->post[$this->module->getPrefixedName($key)] : null;
    }

    /**
     * Sets POST value
     * @param string $key
     * @param mixed $value
     */
    public function setPost($key, $value) {
        $this->request->post[$this->module->getPrefixedName($key)] = $value;
    }

    /**
     * Returns 1 or 0 depending on boolean conversion of argument
     * @param mixed $value
     * @return int
     */
    public function toConfigBool($value) {
        return (bool)$value ? 1 : 0;
    }

    /**
     * Function to filter only those standard boxes, that should be shown for selection
     * @param OriginPackage $box
     * @return bool
     */
    public function filterStandardPackages($box) {
        $hiddenIds = $this->ratesProvider->getHiddenStandardPackagesIds();
        return !in_array($box->getId(), $hiddenIds, true);
    }

    /**
     * Returns render of zones select for defined country
     */
    public function zone() {
        if (!isset($this->request->get['country_id'])) {
            $this->respondWithError('Required: country_id', 400);
        }
        $this->load->model('localisation/zone');
        $this->data['zones'] = $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']);
        if (!count($this->data['zones'])) {
            $this->data['zones'][] = array('zone_id' => 0, 'name' => $this->language->get('text_none'));
        }
        $this->data[$this->module->getPrefixedName('zone_id')] = (int)$this->configuration->get('zone_id');
        $this->data['get_prefixed_name'] = Array($this->module, 'getPrefixedName');
        $this->data['get_extension_name'] = Array($this->module, 'getExtensionName');
        $this->data['has_feature'] = Array($this->module, 'hasFeature');
        $this->data['get_current_route'] = Array($this, 'getCurrentRoute');
        echo BaseView::replyZones($this->data);
    }

    /**
     * Returns render of billing zones select for defined country
     */
    public function billingZone() {
        if (!isset($this->request->get['country_id'])) {
            $this->respondWithError('Required: country_id', 400);
        }
        $this->load->model('localisation/zone');
        $this->data['zones'] = $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']);
        if (!count($this->data['zones'])) {
            $this->data['zones'][] = array('zone_id' => 0, 'name' => $this->language->get('text_none'));
        }
        $this->data[$this->module->getPrefixedName('billing_zone_id')] =
            (int)$this->configuration->get('billing_zone_id');
        $this->data['get_prefixed_name'] = Array($this->module, 'getPrefixedName');
        $this->data['get_extension_name'] = Array($this->module, 'getExtensionName');
        $this->data['has_feature'] = Array($this->module, 'hasFeature');
        $this->data['get_current_route'] = Array($this, 'getCurrentRoute');
        echo BaseView::replyBillingZones($this->data);
    }

    /**
     * Returns render of services select for defined country or user
     */
    public function services() {
        $result = array();
        if ($this->module->hasFeature(Features::MethodsDependOnShipperCountry)) {
            if (!isset($this->request->get['country_id'])) {
                $this->respondWithError('Required: country_id', 400);
            }
            $this->load->model('localisation/country');
            $countryData = $this->model_localisation_country->getCountry($this->request->get['country_id']);
            $countryCode = $countryData['iso_code_2'];
            $result = call_user_func(Array($this->ratesProvider, 'getMethodCodesByCountryCode'), $countryCode);
        } elseif ($this->module->hasFeature(Features::MethodsDependOnUserId)) {
            if (!isset($this->request->get['user_id'])) {
                if ($this->module->getIsDemoMode()) {
                    $this->request->get['user_id'] = $this->configuration->get('user_id');
                } else {
                    $this->respondWithError('Required: user_id', 400);
                }
            }
            try {
                $result = call_user_func(
                    Array($this->ratesProvider, 'getMethodCodesByUserId'),
                    $this->request->get['user_id']
                );
            } catch (\Exception $e) {
                $this->respondWithError($e->getMessage(), 400);
            }
        } else {
            $this->respondWithError('Feature is not supported', 400);
        }
        $requiredTranslations = array_merge(
            $this->ratesProvider->getAllShippingMethodGroups('text'),
            $this->ratesProvider->getAllShippingMethods('text'),
            array('text_select_all', 'text_unselect_all', 'text_refresh', 'text_services_by_user'));
        foreach ($requiredTranslations as $key) {
            $this->data[$key] = $this->language->get($key);
        }
        $this->data[$this->module->getPrefixedName('methods')] = $this->configuration->get('methods');
        $this->data['get_prefixed_name'] = Array($this->module, 'getPrefixedName');
        $this->data['get_extension_name'] = Array($this->module, 'getExtensionName');
        $this->data['has_feature'] = Array($this->module, 'hasFeature');
        $this->data['get_current_route'] = Array($this, 'getCurrentRoute');
        $this->data['method_codes_custom'] = $result;
        if ($this->module->hasFeature(Features::MethodsDependOnUserId)) {
            echo BaseView::replyServicesByUser($this->data);
        } else {
            echo BaseView::replyServices($this->data);
        }
    }

    /**
     * Shows the packaging list for order
     */
    public function packagingList() {
        $orderId = intval($this->request->get['order_id']);
        if (!$orderId) {
            $this->respondWithError('Order Id not specified', 400);
        }
        $packagingList = PackagingListManager::load($orderId);
        if (!$packagingList) {
            $this->respondWithError('Packing list not found or not compatible', 404);
        }
        $_this = $this;
        $locale = new Locale($this->configuration->get('measurement_system'));
        foreach ($packagingList->getShipments() as $shipmentId => $shipment) {
            $this->data['list'][] = array(
                'shipment' => $shipment,
                'locale' => $locale,
                'documents' => !$this->module->hasFeature(Features::ShippingLabel) ? null :
                    array_map(function($document, $documentId) use ($_this, $orderId, $shipmentId) {
                        /** @var AccompanyingDocument $document */
                        return array(
                            'title' => $document->getTypeDescription(),
                            'links' => array_map(function($page) use ($documentId, $_this, $orderId, $shipmentId) {
                                return $_this->link(
                                    $_this->getParentRoute() .
                                    (CoreFeatureChecker::isBrowserSupportsFramePrint() ?
                                        '/documentwrapper' : '/document'),
                                    'order_id=' . $orderId . '&box_id=' . $shipmentId .
                                    '&document_id=' . $documentId . '&page=' . $page
                                );
                            }, range(0, $document->getNumberOfPages() - 1))
                        );
                    }, $shipment->getDocuments(), array_keys($shipment->getDocuments())),
                'request_link' => ($this->module->hasFeature(Features::ShippingLabel) && !$shipment->hasLabel()) ?
                    $this->link(
                        $this->getParentRoute() . '/requestlabel',
                        'order_id=' . $orderId . '&box_id=' . $shipmentId
                    ) : null,
                'void_link' => ($this->module->hasFeature(Features::ShippingLabelVoid) && $shipment->hasLabel()) ?
                    $this->link(
                        $this->getParentRoute() . '/voidlabel', 'order_id=' . $orderId . '&box_id=' . $shipmentId
                    ) : null,
                'map_link' => $shipment->getBox()->getPackagingMap() ? $this->link(
                    $this->getParentRoute() . '/packingmap', 'order_id=' . $orderId . '&box_id=' . $shipmentId
                ) : null,
                'product_links' => array_map(function($item) use ($_this) {
                    /** @var OrderedBoxPackedItem $item */
                    if (!$item->getId()) {
                        return null;
                    }
                    return $item->link = $_this->link('catalog/product/edit', 'product_id=' . $item->getId());
                }, $shipment->getBox()->getProducts())
            );
        }
        $hasLabels = array_reduce($packagingList->getShipments(), function ($b, $shipment) {
            /** @var Shipment $shipment */
            return $b || $shipment->hasLabel();
        }, false);
        $this->data['all_labels_link'] = $this->module->hasFeature(Features::ShippingLabel) && !$hasLabels ?
            $this->link($this->getParentRoute() . '/requestlabel', 'order_id=' . $orderId . '&box_id=-1') : null;
        $this->data['order_id'] = $orderId;
        $this->data['service_name'] = $packagingList->getServiceName();
        $this->data['method_name'] = $packagingList->getMethodName();
        $this->data['get_prefixed_name'] = Array($this->module, 'getPrefixedName');
        $this->data['get_extension_name'] = Array($this->module, 'getExtensionName');
        $this->data['has_feature'] = Array($this->module, 'hasFeature');
        $this->data['get_current_route'] = Array($this, 'getCurrentRoute');
        $this->data['export'] = array(array(
            'title' => 'JSON',
            'link' => $this->link($this->getParentRoute() . '/exportorders', 'order_id=' . $orderId . '&format=json')
        ));
        if ($this->module->hasFeature(Features::ExportFormatClickShip)) {
            $this->data['export'][] = array(
                'title' => 'Click-n-ship address book',
                'link' => $this->link(
                    $this->getParentRoute() . '/exportorders', 'order_id=' . $orderId . '&format=clicknship'
                )
            );
        }
        if ($this->module->hasFeature(Features::ExportFormatFedexAddressBook)) {
            $this->data['export'][] = array(
                'title' => 'FedEx address book',
                'link' => $this->link(
                    $this->getParentRoute() . '/exportorders', 'order_id=' . $orderId . '&format=fedexaddressbook'
                )
            );
        }
        $this->data['version'] = $this->module->getVersion();
        echo BaseView::replyPackagingList($this->data);
    }

    public function exportOrders() {
        $orderId = intval($this->request->get['order_id']);
        if (!$orderId) {
            $this->respondWithError('Order Id not specified', 400);
        }
        $format = $this->request->get['format'];
        if (!$format) {
            $this->respondWithError('Format not specified', 400);
        }
        if (CoreFeatureChecker::hasAdminModel('sale/order')) { // v2 and v3
            $this->load->model('sale/order');
            $order = $this->model_sale_order->getOrder($orderId);
        } elseif (CoreFeatureChecker::hasAdminModel('account/order')) { // v1
            $this->load->model('account/order');
            $order = $this->model_account_order->getOrder($orderId);
        } else {
            throw new CoreFeatureException('Can not load order model');
        }
        if ($format === 'json') {
            $this->respondWithText(
                PackagingListManager::readRaw($orderId),
                'application/json',
                'order-' . $orderId . '.json'
            );
        } elseif ($format === 'clicknship') {
            $data = array(
                array(
                    'First Name' => $order['shipping_firstname'],
                    'MI' => '',
                    'Last Name' => $order['shipping_lastname'],
                    'Company' => $order['shipping_company'],
                    'Address 1' => $order['shipping_address_1'],
                    'Address 2' => $order['shipping_address_2'],
                    'Address 3' => '',
                    'City' => $order['shipping_city'],
                    'State/Province' => $order['shipping_zone'],
                    'ZIP/Postal Code' => $order['shipping_postcode'],
                    'Country' => $order['shipping_country'],
                    'Urbanization' => '',
                    'Phone Number' => $order['telephone'],
                    'Fax Number' => isset($order['fax']) ? $order['fax'] : '',
                    'E Mail' => $order['email'],
                    'Reference Number' => $order['order_id'],
                    'Nickname' => ''
                )
            );
            $output = Arrays::toCsv($data);
            $this->respondWithText($output, 'text/csv', 'order-' . $orderId . '.csv');
        } elseif ($format === 'fedexaddressbook') {
            $data = array(
                array(
                    'Nickname' => '',
                    'FullName' => $order['shipping_firstname'] . ' ' . $order['shipping_lastname'],
                    'FirstName' => $order['shipping_firstname'],
                    'LastName' => $order['shipping_lastname'],
                    'Title' => '',
                    'Company' => $order['shipping_company'],
                    'Department' => '',
                    'AddressOne' => $order['shipping_address_1'],
                    'AddressTwo' => $order['shipping_address_2'],
                    'City' => $order['shipping_city'],
                    'State' => $order['shipping_zone_code'],
                    'Zip' => $order['shipping_postcode'],
                    'PhoneNumber' => $order['telephone'],
                    'ExtensionNumber' => '',
                    'FAXNumber' => isset($order['fax']) ? $order['fax'] : '',
                    'PagerNumber' => '',
                    'MobilePhoneNumber' => '',
                    'CountryCode' => $order['shipping_iso_code_2'], // iso-2 code in sample
                    'EmailAddress' => $order['email'],
                    'VerifiedFlag' => 'Y',
                    'AcceptedFlag' => 'Y',
                    'ValidFlag' => 'Y',
                    'ResidentialFlag' => $order['shipping_company'] ? '' : 'R'
                )
            );
            $output = Arrays::toCsv($data);
            $this->respondWithText($output, 'text/csv', 'order-' . $orderId . '.csv');
        } else {
            $this->respondWithError('Unknown format ' . $format, 404);
        }
    }

    public function exportSettings() {
        if ($this->module->getIsDemoMode()) {
            $this->respondWithError('Can not export settings in demo mode', 403);
        }
        $this->load->model('setting/setting');
        $result = array(
            'extension' => $this->module->getExtensionName(),
            'version' => $this->module->getVersion(),
            'date' => date('d.m.Y h:i:s'),
            'vqmod_installed' => $this->isModInstalled(),
            'vqmod_enabled' => $this->isModEnabled(),
            'php' => phpversion(),
            'ecommerce_version' => VersionChecker::get()->getVersion(),
            'host' => gethostname(),
            'settings' => $this->model_setting_setting->getSetting(
                (VersionChecker::get()->isVersion3() ? 'shipping_' : '') . $this->module->getExtensionName()
            )
        );
        $this->respondWithText(
            json_encode($result, defined('JSON_PRETTY_PRINT') ? JSON_PRETTY_PRINT : 0),
            'application/json',
            $this->module->getExtensionName() . '.json'
        );
    }

    public function importSettings() {
        if (!isset($this->request->files['source'])) {
            $this->respondWithError('Required file: source', 400);
        }
        $data = @file_get_contents($this->request->files['source']['tmp_name']);
        if (!$data) {
            $this->respondWithError('Can not read file', 404);
        }
        $json = json_decode($data, true);
        if ($json === null) {
            $this->respondWithError('Invalid JSON file', 400);
        }
        if (!isset($json['extension'])) {
            $this->respondWithError('Can not find extension name in file', 404);
        }
        if ($json['extension'] !== $this->module->getExtensionName()) {
            $this->respondWithError('These settings are not compatible with this extension', 400);
        }
        if (!isset($json['settings'])) {
            $this->respondWithError('Can not find settings in file', 404);
        }
        if ($this->module->getIsDemoMode()) {
            $this->respondWithError('Can not change settings in demo mode', 403);
        }
        if (!$this->user->hasPermission('modify', $this->getParentRoute())) {
            $this->respondWithError('You do not have persmissions to change settings', 403);
        }
        $this->load->model('setting/setting');
        if (VersionChecker::get()->isVersion3()) { // compatibility to settings from oc before v3
            foreach ($json['settings'] as $k => $v) {
                if (strpos($k, 'shipping') !== 0) {
                    $json['settings']['shipping_' . $k] = $v;
                    unset($json['settings'][$k]);
                }
            }
        }
        $this->model_setting_setting->editSetting(
            (VersionChecker::get()->isVersion3() ? 'shipping_' : '') . $this->module->getExtensionName(),
            $json['settings']
        );
        $this->smartRedirect($this->getParentRoute());
    }


    /**
     * Requests label(s) for the order
     * @since 05.01.2017 responses with JSON: next[redirect,ajax],error,api...
     */
    public function requestLabel() {
        $orderId = intval($this->request->get['order_id']);
        $boxId = intval($this->request->get['box_id']);
        if (!$orderId) {
            $this->respondWithError('Can not find this order', 404);
        }
        if ($boxId === -1) { // all boxes
            $boxId = null;
        }
        if ($this->configuration->get('label') === BaseLabelsProvider::LabelDisabled) {
            $this->respondWithError('This feature is disabled in module settings');
        }
        $this->debugger->debug(Debugger::Trace, 'Requesting labels for order #' . $orderId . ' and ' .
            ($boxId === null ? 'all boxes' : 'box # ' . $boxId));
        try {
            $packagingList = PackagingListManager::requestLabels(
                $orderId, $boxId, $this->module, Array($this->debugger, 'debug'), $this->validationProvider,
                $this->labelsProvider, $this->configuration, $this->converter,
                Array('country' => $this->model_localisation_country, 'zone' => $this->model_localisation_zone)
            );
            $this->load->model('sale/order');
            $orderInfo = $this->model_sale_order->getOrder($orderId);
            $history = array(
                'order_status_id' => $orderInfo['order_status_id'],
                'notify' => 0,
                'comment' => 'Shipping labels have been requested:' . "\n" . $packagingList->getStatusText(),
                'override' => 0
            );
            if (CoreFeatureChecker::hasMethod($this->model_sale_order, 'addOrderHistory')) { // v1
                $this->model_sale_order->addOrderHistory($orderId, $history);
                $this->respondWithText(json_encode(array(
                    'next' => 'redirect',
                    'error' => null,
                )), 'application/json');
            } elseif (CoreFeatureChecker::hasAdminModel('user/api')) { // v2 or v3
                $this->load->model('user/api');
                $apiInfo = $this->model_user_api->getApi($this->config->get('config_api_id'));
                if (VersionChecker::get()->isVersion3()) {
                    $session = new \Session($this->config->get('session_engine'), $this->registry);
                    $session->start();
                    $this->model_user_api->deleteApiSessionBySessonId($session->getId());
                    $this->model_user_api->addApiSession(
                        $apiInfo['api_id'], $session->getId(), $this->request->server['REMOTE_ADDR']
                    );
                    $session->data['api_id'] = $apiInfo['api_id'];
                    $apiInfo['api_token'] = $session->getId();
                }
                $this->respondWithText(json_encode(array(
                    'next' => 'ajax',
                    'error' => null,
                    'entry' => $this->getFrontUrl() . 'index.php?',
                    'api_id' => $apiInfo['api_id'],
                    'api_key' => isset($apiInfo['key']) ? $apiInfo['key'] : null,
                    'api_username' => isset($apiInfo['username']) ? $apiInfo['username'] : null,
                    'api_password' => isset($apiInfo['password']) ? $apiInfo['password'] : null,
                    'api_ip' => $this->request->server['REMOTE_ADDR'],
                    'api_token' => isset($apiInfo['api_token']) ? $apiInfo['api_token'] : null, // v3
                    'order_id' => $orderId,
                    'history' => $history
                )), 'application/json');
            } else {
                $this->respondWithText(json_encode(array(
                    'next' => 'redirect',
                    'error' => 'Can not find model to write order history'
                )), 'application/json');
            }
        } catch (\Exception $error) {
            $this->debugger->debug(
                Debugger::Error, $this->module->getServiceName() . ' Smart & Flexible:', $error->getMessage()
            );
            $this->respondWithText(json_encode(array('error' => $error->getMessage())), 'application/json');
        }
    }

    public function documentWrapper() {
        $orderId = intval($this->request->get['order_id']);
        $boxId = intval($this->request->get['box_id']);
        $documentId = intval($this->request->get['document_id']);
        $page = intval($this->request->get['page']);
        if (!$orderId) {
            $this->respondWithError('Can not find this order', 404);
        }
        $shipments = PackagingListManager::load($orderId)->getShipments();
        if (!isset($shipments[$boxId])) {
            $this->respondWithError('Can not find this box in this order', 404);
        }
        $document = $shipments[$boxId]->getDocument($documentId);
        if (!$document) {
            $this->respondWithError('Can not find this document', 404);
        }
        $this->data['document_link'] = $this->link($this->getParentRoute() . '/document',
            'order_id=' . $orderId . '&box_id=' . $boxId . '&document_id=' . $documentId . '&page=' . $page);
        $this->data['document_format'] = (
            $this->configuration->get('label_format') === BaseLabelsProvider::LabelFormat4x6 &&
            $document->isLabel()
        ) ? BaseLabelsProvider::DocumentFormatPNG : ($document->isLabel() ?
            $this->labelsProvider->getDocumentFormat() : BaseLabelsProvider::DocumentFormatPDF
        );
        $this->data['save_link'] = $this->data['document_link'] . '&save=1';
        $this->data['get_prefixed_name'] = Array($this->module, 'getPrefixedName');
        $this->data['get_extension_name'] = Array($this->module, 'getExtensionName');
        $this->data['has_feature'] = Array($this->module, 'hasFeature');
        $this->data['get_current_route'] = Array($this, 'getCurrentRoute');
        $this->data['version'] = $this->module->getVersion();
        echo BaseView::replyDocumentWrapper($this->data);
    }

    /**
     * Shows the label or some other document
     */
    public function document() {
        $orderId = intval($this->request->get['order_id']);
        $boxId = intval($this->request->get['box_id']);
        $documentId = intval($this->request->get['document_id']);
        $page = intval($this->request->get['page']);
        if (!$orderId) {
            $this->respondWithError('Can not find this order', 404);
        }
        $shipments = PackagingListManager::load($orderId)->getShipments();
        if (!isset($shipments[$boxId])) {
            $this->respondWithError('Can not find this box in this order', 404);
        }
        $document = $shipments[$boxId]->getDocument($documentId);
        if (!$document) {
            $this->respondWithError('Can not find this document', 404);
        }
        $data = base64_decode($document->getPage($page));
        $filename = isset($this->request->get['save']) ? strtolower($document->getType()) . '-' .
            $orderId . '-' . $boxId . '-'. $page . '.' : null;
        if (
            $this->configuration->get('label_format') === BaseLabelsProvider::LabelFormat4x6 &&
            $this->module->hasFeature(Features::ShippingLabel4x6Inches) &&
            $document->isLabel()
        ) { // requires 4x6 resizing
            if ($this->labelsProvider->getDocumentFormat() === BaseLabelsProvider::DocumentFormatPDF) {
                try {
                    if ($this->configuration->get('pdf_converter') === BaseLabelsProvider::PdfConverterImagick) {
                        $data = Label::make4x6FromPdfImagick($data, $this->labelsProvider->getLabelPosition());
                    } elseif ($this->configuration->get('pdf_converter') === BaseLabelsProvider::PdfConverterGmagick) {
                        $data = Label::make4x6FromPdfGmagick($data, $this->labelsProvider->getLabelPosition());
                    } else {
                        $this->respondWithError('Unknown PDF converter selected', 404);
                    }
                } catch (\Exception $e) {
                    $this->respondWithError('Can not convert label. ' . $e->getMessage(), 500);
                }
            } elseif ($this->labelsProvider->getDocumentFormat() === BaseLabelsProvider::DocumentFormatPNG) {
                try {
                    $data = Label::make4x6FromPng($data, $this->labelsProvider->getLabelPosition());
                } catch (\Exception $e) {
                    $this->respondWithError('Can not process label. ' . $e->getMessage(), 500);
                }
            } else {
                $this->respondWithError('Unknown labels format: ' . $this->labelsProvider->getDocumentFormat(), 404);
            }
            $this->respondWithText($data, 'image/png', $filename ? $filename . 'png' : null);
            return;
        }
        $this->respondWithText(
            $data, $document->isLabel() ? $this->labelsProvider->getDocumentFormat() :
                BaseLabelsProvider::DocumentFormatPDF,
            $filename ? $filename . $this->labelsProvider->getDocumentExtension() : null
        );
    }

    public function voidLabel() {
        if (!$this->module->hasFeature(Features::ShippingLabelVoid)) {
            $this->respondWithError('Feature is not supported', 404);
        }
        $orderId = intval($this->request->get['order_id']);
        $boxId = intval($this->request->get['box_id']);
        if (!$orderId) {
            $this->respondWithError('Required: order_id', 400);
        }
        $packagingList = PackagingListManager::load($orderId);
        $shipments = $packagingList->getShipments();
        if (!isset($shipments[$boxId])) {
            $this->respondWithError('Can not find this box in this order', 404);
        }
        $this->labelsProvider->setOption('email', $this->config->get('config_email'));
        $this->labelsProvider->setDebugger(Array($this->debugger, 'debug'));
        $shipments[$boxId] = $this->labelsProvider->voidLabel($orderId, $shipments[$boxId]);
        $packagingList = new PackagingList(array(
            'order_id' => $packagingList->getOrderId(),
            'service_name' => $packagingList->getServiceName(),
            'version' => $packagingList->getVersion(),
            'method_code' => $packagingList->getMethodCode(),
            'address' => $packagingList->getAddress(),
            'method_name' => $packagingList->getMethodName(),
            'shipments' => $shipments
        ));
        PackagingListManager::save($packagingList);
        $this->smartRedirect($this->getParentRoute() . '/packaginglist', 'order_id=' . $orderId);
    }

    public function packingMap() {
        $orderId = intval($this->request->get['order_id']);
        $boxId = intval($this->request->get['box_id']);
        if (!$orderId) {
            $this->respondWithError('Required: order_id', 400);
        }
        $packagingList = PackagingListManager::load($orderId);
        $shipments = $packagingList->getShipments();
        if (!isset($shipments[$boxId])) {
            $this->respondWithError('Can not find this box in this order', 404);
        }
        if (!$shipments[$boxId]->getBox()->getPackagingMap()) {
            $this->respondWithError('Can not find packing map for this box', 404);
        }
        $reservedSpace = $shipments[$boxId]->getBox()->getPackagingMap()->getReservedSpace();
        $this->data['get_prefixed_name'] = Array($this->module, 'getPrefixedName');
        $this->data['get_extension_name'] = Array($this->module, 'getExtensionName');
        $this->data['has_feature'] = Array($this->module, 'hasFeature');
        $this->data['get_current_route'] = Array($this, 'getCurrentRoute');
        $this->data['version'] = $this->module->getVersion();
        $this->data['scene'] = Scene::generate(
            $shipments[$boxId]->getBox()->getInnerLength(),
            $shipments[$boxId]->getBox()->getInnerWidth(),
            $shipments[$boxId]->getBox()->getInnerHeight(),
            $reservedSpace
        );
        echo BaseView::replyPackagingMap($this->data);
    }

    /**
     * Returns the javascript to insert tracking number into order comment
     */
    public function tracking() {
        if ($this->configuration->get('tracking') !== BaseLabelsProvider::TrackingSendShipped) {
            $this->respondWithError('Feature is not enabled', 404);
        }
        $orderId = intval($this->request->get['order_id']);
        if (!$orderId) {
            $this->respondWithError('Required: order_id', 400);
        }
        $packagingList = PackagingListManager::load($orderId);
        if (!$packagingList) {
            $this->respondWithError('Packaging list not found', 404);
        }
        $trackingNumbers = array_filter(array_map(function($box) {
            /** @var Shipment $box */
            return $box->getTrackingNumber();
        }, $packagingList->getShipments()));
        if (!$trackingNumbers) {
            $this->respondWithError('Tracking numbers not found', 404);
        }
        $result = 'Tracking numbers: ' . implode(', ', $trackingNumbers);
        $this->respondWithText($result);
    }

    /**
     * Returns the tare weight of custom package
     * @see OriginFixed
     */
    public function tareOfCustomPackageFixed() {
        if (!(
            isset($this->request->get['length']) &&
            isset($this->request->get['width']) &&
            isset($this->request->get['height']) &&
            isset($this->request->get['measurement_system'])
        )) {
            $this->respondWithError('Required: length, width, height, measurement_system', 400);
        }
        $locale = new Locale($this->request->get['measurement_system']);
        $dimensions = array(
            $locale->acceptLength($this->request->get['length']),
            $locale->acceptLength($this->request->get['width']),
            $locale->acceptLength($this->request->get['height'])
        );
        if (array_filter($dimensions, function ($dim) {
            return $dim <= 0;
        })) {
            $this->respondWithError('All dimensions must be greater than zero', 400);
        }
        try {
            $weight = OriginPackage::createFromArray(array(
                'type' => $this->module->getCustomPackageFixedType(),
                'title' => '',
                'dimensions_outside' => $dimensions,
                'dimensions_inside' => $dimensions,
                'density' => OriginPackage::getCustomPackageDensity($this->module->getCustomPackageFixedType())
            ))->getTareWeightWithLocale($locale);
            $this->respondWithText($weight);
        } catch (\Exception $e) {
            $this->respondWithError($e->getMessage(), 400);
        }
    }

    /**
     * Returns the tare weight of custom package
     * @see OriginExpandable
     */
    public function tareOfCustomPackageExpandable() {
        if (!(
            isset($this->request->get['length']) &&
            isset($this->request->get['width']) &&
            isset($this->request->get['measurement_system'])
        )) {
            $this->respondWithError('Required: length, width, measurement_system', 400);
        }
        $locale = new Locale($this->request->get['measurement_system']);
        $length = $locale->acceptLength($this->request->get['length']);
        $width = $locale->acceptLength($this->request->get['width']);
        if ($length <= 0 || $width <= 0) {
            $this->respondWithError('Length and width must be greater than zero', 400);
        }
        try {
            $weight = OriginPackage::createFromArray(array(
                'type' => $this->module->getCustomPackageExpandableType(),
                'title' => '',
                'length' => $length,
                'width' => $width,
                'density' => OriginPackage::getCustomPackageDensity($this->module->getCustomPackageExpandableType())
            ))->getTareWeightWithLocale($locale);
            $this->respondWithText($weight);
        } catch (\Exception $e) {
            $this->respondWithError($e->getMessage(), 400);
        }

    }

    /**
     * Controller entry method: data preparation and saving
     */
    public function index() {
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('setting/setting');

        $isReload = isset($this->request->post['measurement_system_changed']);
        if ($isReload) {
            $locale = new Locale($this->request->post['measurement_system_old']);
            $this->validateLocaleDepended($locale);
            $this->data['success'] = $this->language->get('text_measurement_system_changed');
        } else {
            if (($this->request->server['REQUEST_METHOD'] === 'POST')) {
                if ($this->validate()) {
                    $this->model_setting_setting->editSetting(
                        (VersionChecker::get()->isVersion3() ? 'shipping_' : '') . $this->module->getExtensionName(),
                        $this->request->post
                    );
                    $this->session->data['success'] = sprintf(
                        $this->language->get('text_success'),
                        $this->module->getServiceName()
                    );
                    if (VersionChecker::get()->isVersion23()) {
                        $this->smartRedirect('extension/extension', 'type=shipping');
                    } elseif (VersionChecker::get()->isVersion3()) {
                        $this->smartRedirect('marketplace/extension', 'type=shipping');
                    } else {
                        $this->smartRedirect('extension/shipping');
                    }
                } elseif (!isset($this->error['warning'])) {
                    $this->error['warning'] = $this->language->get('error_save');
                }
            }
        }

        // default translations
        $requiredTranslations = array_merge(
            $this->ratesProvider->getAllShippingMethodGroups('text'),
            $this->ratesProvider->getAllShippingMethods('text'),
            Helper::$requiredTranslations
        );

        foreach ($requiredTranslations as $key) {
            $this->data[$key] = $this->language->get($key);
        }

        // translations with parameter
        foreach (Helper::$translationsWithParameter as $key) {
            $this->data[$key] = sprintf($this->data[$key], $this->module->getServiceName());
        }
        $this->data['help_label_foreign_data'] = sprintf(
            $this->data['help_label_foreign_data'],
            $this->link('setting/store')
        );
        $this->data['license_info'] = sprintf($this->data['license_info'], $this->data['heading_title'],
            Author::WebsiteUrl, Author::WebsiteUrl, Author::LicenseUrl);
        if ($this->module->getExtensionName() === 'usps_smart_flexible') {
            $this->data['help_label'] = sprintf($this->data['help_label'], Author::$knowledgeBaseUrl['uspslabel']);
        }

        // foreign data
        $this->data[$this->module->getPrefixedName('sender_name')] = $this->config->get('config_owner');
        $this->data[$this->module->getPrefixedName('sender_company')] = $this->config->get('config_name');
        $this->data[$this->module->getPrefixedName('sender_telephone')] = $this->config->get('config_telephone');
        if ($this->module->hasFeature(Features::SenderTelephone10Digits)) {
            if (strlen(
                preg_replace('/[^\d]/', '', $this->data[$this->module->getPrefixedName('sender_telephone')])
            ) !== 10) {
                $this->error['sender_telephone'] = $this->language->get('error_sender_telephone_10_digits');
            }
        }
        if ($this->module->hasFeature(Features::SenderTelephoneNotLonger15Digits)) {
            if (strlen(
                preg_replace('/[^\d]/', '', $this->data[$this->module->getPrefixedName('sender_telephone')])
            ) > 15) {
                $this->error['sender_telephone'] = $this->language->get('error_sender_telephone_not_longer_15_digits');
            }
        }

        foreach (Helper::$requiredErrors as $baseViewKey => $fieldKey) {
            $this->data[$baseViewKey] = isset($this->error[$fieldKey]) ? $this->error[$fieldKey] : '';
        }

        // nav controls
        Helper::buildNavControls($this->data, Array($this, 'link'), $this->getCurrentRoute());

        // input
        foreach (Helper::$requiredInputs as $key) {
            $this->data[$this->module->getPrefixedName($key)] =
                isset($this->request->post[$this->module->getPrefixedName($key)]) ? // if post
                    $this->request->post[$this->module->getPrefixedName($key)] : // use post, else
                    $this->configuration->get($key); // use config (or default)
        }

        // models
        $this->load->model('localisation/weight_class');
        $this->load->model('localisation/length_class');
        $this->load->model('localisation/tax_class');
        $this->load->model('localisation/country');
        $this->load->model('localisation/geo_zone');
        $this->load->model('setting/store');

        // selectors
        $this->data['displayed_statuses'] = Helper::getDisplayedStatuses($this->data);
        $this->data[$this->module->getPrefixedName('displayed_status')] =
            $this->data[$this->module->getPrefixedName('status_for_customers')] ?
                ($this->data[$this->module->getPrefixedName('status')] ?
                    BaseModel::StatusEnabled : BaseModel::StatusDisabled
                ) : BaseModel::StatusOnlyForAdmin;
        $this->data['booleans'] = Helper::getBooleans($this->data);
        $this->data['modes'] = Helper::getModes($this->data);
        $this->data['machinables'] = Helper::getMachinables($this->data);
        $this->data['debug_levels'] = Helper::getDebugLevels($this->data);
        $this->data['sortings'] = Helper::getSortings($this->data);
        $this->data['packers'] = Helper::getPackers($this->data, $this->module);
        $this->data['dropoffs'] = Helper::getDropoffs($this->data);
        $this->data['pickups'] = Helper::getPickups($this->data);
        $this->data['commercial_rates'] = Helper::getCommercialRates($this->data);
        $this->data['labels'] = Helper::getLabels($this->data);
        $this->data['trackings'] = Helper::getTrackings($this->data);
        $this->data['date_formats'] = Helper::getDateFormats();
        $this->data['hours'] = Helper::getHours($this->data);
        $this->data['google_holidays'] = Helper::$googleHolidays;
        $this->data[$this->module->getPrefixedName('google_import')] = 'en.usa#holiday@group.v.calendar.google.com';
        $this->data['measurement_systems'] = Helper::$measurementSystems;
        $this->data['label_formats'] = Helper::getLabelFormats($this->data, $this->module, $this->labelsProvider);
        $this->data['label_format_pdf'] = $this->module->hasFeature(Features::ShippingLabel) ?
            $this->labelsProvider->getDocumentFormat() === BaseLabelsProvider::DocumentFormatPDF : null;
        $this->data['pdf_converters'] = Helper::getPdfConverters($this->data, $this->module, $this->labelsProvider);
        $this->data['boxes'] = Helper::getBoxes($this->ratesProvider, Array($this, 'filterStandardPackages'));
        $this->data['weight_classes'] = $this->model_localisation_weight_class->getWeightClasses();
        $this->data['length_classes'] = $this->model_localisation_length_class->getLengthClasses();
        $this->data['tax_classes'] = Helper::getTaxClasses($this->data, $this->model_localisation_tax_class);
        $this->data['countries'] = $this->model_localisation_country->getCountries();
        $this->data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
        $this->data['customer_groups'] = $this->getCustomerGroups();
        $this->data['stores'] = Helper::getStores($this->model_setting_store, $this->config);

        Helper::findPounds($this->data, $this->module);
        Helper::findInches($this->data, $this->module);

        $this->data['provider_currency'] = $this->module->getValueCurrencyCode(
            $this->configuration->getOriginCountryCode($this->model_localisation_country, $this->getPost('country_id'))
        );
        $this->data['provider_currency_found'] = $this->currency->has($this->data['provider_currency']);

        $systemCurrency = $this->config->get('config_currency');
        $this->data['system_currency'] = $systemCurrency;
        $this->data['currency_unit'] = $this->currency->getSymbolLeft($systemCurrency) ?:
            $this->currency->getSymbolRight($systemCurrency);

        $this->data['locale'] = new Locale($this->data[$this->module->getPrefixedName('measurement_system')]);
        $this->data['token'] = $this->session->data[$this->getTokenName()];
        $this->data['token_name'] = $this->getTokenName();
        $this->data['version'] = $this->module->getVersion();
        $this->data['contact_email'] = $this->module->getContactEmail();
        $this->data['is_demo_mode'] = $this->module->getIsDemoMode();
        $this->data['method_codes'] = $this->ratesProvider->getMethodCodes();
        $this->data['max_weight'] = $this->ratesProvider->getPackageMaxWeight(Address::inUnitedStates());
        $this->data['get_prefixed_name'] = Array($this->module, 'getPrefixedName');
        $this->data['get_extension_name'] = Array($this->module, 'getExtensionName');
        $this->data['get_current_route'] = Array($this, 'getCurrentRoute');
        $this->data['has_feature'] = Array($this->module, 'hasFeature');
        $this->data['vqmod_installed'] = class_exists('VQmod');
        $this->data['mod_file_enabled'] = $this->isModEnabled();
        $this->data['mod_file_installed'] = $this->isModInstalled();
        $this->data['mod_modifications_url'] = $this->link('extension/modification');
        $this->data['export_settings_link'] = $this->link($this->getCurrentRoute() . '/exportsettings');
        $this->data['import_settings_link'] = $this->link($this->getCurrentRoute() . '/importsettings');
        $this->data['webhook'] = $this->getFrontUrl() . 'index.php?route=' .
            (VersionChecker::get()->isVersion3() ? 'extension/' : '') . 'shipping/' . $this->module->getExtensionName();
        $this->data['license_cookie'] = $this->module->getPrefixedName('license_close');
        $this->data['custom_package_fixed_unit'] =
            OriginPackage::getCustomPackageTareUnitType($this->module->getCustomPackageFixedType());
        $this->data['custom_package_expandable_unit'] =
            OriginPackage::getCustomPackageTareUnitType($this->module->getCustomPackageExpandableType());
        $this->outputModuleSettings();
    }

    /**
     * Validates the input data before saving new settings
     * @return bool
     */
    protected function validate() {
        // required
        if ($this->module->getIsDemoMode()) {
            $this->error['warning'] = $this->language->get('error_demo');
            return false;
        }
        if (!$this->user->hasPermission('modify', $this->getCurrentRoute())) {
            $this->error['warning'] = sprintf(
                $this->language->get('error_permission'),
                $this->module->getServiceName()
            );
        }
        if (!in_array($this->getPost('measurement_system'), BaseModel::$measureSystems, true)) {
            $this->error['measurement_system'] = $this->language->get('error_measurement_system');
        }
        $locale = new Locale($this->getPost('measurement_system'));
        if (!$this->getPost('user_id')) {
            $this->error['user_id'] = $this->language->get('error_user_id');
        }
        if (!$this->getPost('postcode')) {
            $this->error['postcode'] = $this->language->get('error_postcode');
        }
        if (!$this->getPost('pounds_id')) {
            $this->error['pounds_id'] = $this->language->get('error_required');
        }
        $this->setPost('pounds_id', (int)$this->getPost('pounds_id'));
        if (!$this->getPost('inches_id')) {
            $this->error['inches_id'] = $this->language->get('error_required');
        }
        $this->setPost('inches_id', (int)$this->getPost('inches_id'));
        if (!$this->currency->has(
            $this->module->getValueCurrencyCode($this->configuration->getOriginCountryCode(
                $this->model_localisation_country, $this->getPost('country_id')
            ))
        )) {
            $this->error['provider_currency'] = $this->language->get('error_provider_currency');
        }

        // shipping methods codes (bool)
        $methods = $this->getPost('methods');
        if (is_array($methods)) {
            foreach ($methods as $fullKey => $b) {
                $methods[$fullKey] = $this->toConfigBool($b);
            }
        } else {
            $this->setPost('methods', array());
        }
        $this->setPost('methods', $methods);

        // status
        $this->setPost('status', $this->toConfigBool(in_array(
            $this->getPost('displayed_status'), array(BaseModel::StatusEnabled, BaseModel::StatusOnlyForAdmin), true
        )));
        $this->setPost('status_for_customers', $this->toConfigBool(
            $this->getPost('displayed_status') !== BaseModel::StatusOnlyForAdmin
        ));

        // boolean
        $this->setPost('display_time', $this->toConfigBool($this->getPost('display_time')));
        $this->setPost('display_weight', $this->toConfigBool($this->getPost('display_weight')));
        $this->setPost('processing_saturdays', $this->toConfigBool($this->getPost('processing_saturdays')));
        $this->setPost('processing_sundays', $this->toConfigBool($this->getPost('processing_sundays')));
        $this->setPost('all_geo_zones', $this->toConfigBool($this->getPost('all_geo_zones')));
        $this->setPost('all_customer_groups', $this->toConfigBool($this->getPost('all_customer_groups')));
        $this->setPost('all_stores', $this->toConfigBool($this->getPost('all_stores')));
        $this->setPost('avoid_delivery_saturdays', $this->toConfigBool($this->getPost('avoid_delivery_saturdays')));
        $this->setPost('avoid_delivery_sundays', $this->toConfigBool($this->getPost('avoid_delivery_sundays')));
        $this->setPost('individual_tare', $this->toConfigBool($this->getPost('individual_tare')));

        // date format
        if (!in_array($this->getPost('date_format'), BaseModel::$dateFormats, true)) {
            $this->setPost('date_format', BaseModel::$dateFormats[0]);
        }

        // integer
        $this->setPost('tax_class_id', (int)$this->getPost('tax_class_id'));
        $this->setPost('sort_order', (int)$this->getPost('sort_order'));

        // processing days
        $this->setPost('processing_days', min(
            max(0, (int)$this->getPost('processing_days')),
            $this->ratesProvider->getMaxProcessingDays()
        ));

        // cutoff
        $cutoff = $this->getPost('cutoff');
        if ($cutoff !== BaseRatesProvider::CutoffDisabled) {
            if (!preg_match('/\d{2}:\d{2}/', $cutoff)) {
                $cutoff = BaseRatesProvider::CutoffDisabled;
            }
        }
        $this->setPost('cutoff', $cutoff);

        // debug level
        if (!in_array($this->getPost('debug'), Debugger::$debugLevels, true)) {
            $this->setPost('debug', Debugger::ShowNothingLogErrors);
        }

        // float
        $this->setPost('minimum_rate', (float)$this->getPost('minimum_rate'));
        $this->setPost('weight_adj_per', (float)$this->getPost('weight_adj_per'));
        $this->setPost('rate_adj_fix', (float)$this->getPost('rate_adj_fix'));
        $this->setPost('rate_adj_per', (float)$this->getPost('rate_adj_per'));
        $this->setPost('insurance_from', (float)$this->getPost('insurance_from'));

        // holidays list
        $holidays = $this->getPost('processing_holidays');
        if (is_array($holidays)) {
            foreach ($holidays as $k => $holiday) {
                unset($holidays[$k]);
                if (is_array($holiday)) {
                    if (isset($holiday[0]) && isset($holiday[1])) {
                        $month = (int)$holiday[0];
                        $day = (int)$holiday[1];
                        if (in_array($month, range(1, 12), true) && in_array($day, range(1, 31), true)) {
                            $holidays[$k] = array($month, $day);
                        }
                    }
                }
            }
        } else {
            $holidays = array();
        }
        $this->setPost('processing_holidays', array_values($holidays));

        // geo zones
        $geoZones = $this->getPost('geo_zones');
        if (is_array($geoZones)) {
            foreach ($geoZones as $zone => $b) {
                unset($geoZones[$zone]);
                if ((int)$zone) {
                    $geoZones[(int)$zone] = (int)$b;
                }
            }
        } else {
            $geoZones = array();
        }
        $this->setPost('geo_zones', $geoZones);

        // customer groups
        $customerGroups = $this->getPost('customer_groups');
        if (is_array($customerGroups)) {
            foreach ($customerGroups as $group => $b) {
                unset($customerGroups[$group]);
                if ((int)$group) {
                    $customerGroups[(int)$group] = (int)$b;
                }
            }
        } else {
            $customerGroups = array();
        }
        $this->setPost('customer_groups', $customerGroups);

        // stores
        $stores = $this->getPost('stores');
        if (is_array($stores)) {
            foreach ($stores as $store => $b) {
                unset($stores[$store]);
                // can be zero for default store - no check
                $stores[(int)$store] = (int)$b;
            }
        } else {
            $stores = array();
        }
        $this->setPost('stores', $stores);

        // standard packages
        $standardPackages = $this->getPost('standard_packages');
        if (is_array($standardPackages)) {
            foreach ($standardPackages as $box => $b) {
                unset($standardPackages[$box]);
                if ((int)$box) {
                    $standardPackages[(int)$box] = (int)$b;
                }
            }
        } else {
            $this->setPost('standard_packages', array());
        }
        $this->setPost('standard_packages', $standardPackages);

        // adjustment rules
        $this->validateAdjustmentRules();

        // locale depended
        $this->validateLocaleDepended($locale);

        // features validation
        $this->validateFeatures();

        // custom product extended information
        $this->setPost('product_names', $this->configuration->get('product_names'));
        $this->setPost('product_countries', $this->configuration->get('product_countries'));
        $this->setPost('product_zones', $this->configuration->get('product_zones'));
        $this->setPost('product_hs_codes', $this->configuration->get('product_hs_codes'));

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    protected function validateAdjustmentRules() {
        $result = array();
        if (is_array($this->getPost('adjustment_rules'))) {
            foreach ($this->getPost('adjustment_rules') as $rule) {
                $rule['rate_operator'] = in_array($rule['rate_operator'], Helper::$compareOperators, true) ?
                    $rule['rate_operator'] : null;
                $rule['total_operator'] = in_array($rule['total_operator'], Helper::$compareOperators, true) ?
                    $rule['total_operator'] : null;
                $rule['rate'] = $rule['rate'] === '' ? null : (float)$rule['rate'];
                $rule['total'] = $rule['total'] === '' ? null : (float)$rule['total'];
                $rule['fixed'] = $rule['fixed'] === '' ? null : (float)$rule['fixed'];
                $rule['percent'] = $rule['percent'] === '' ? null : (float)$rule['percent'];
                $rule['group'] = $rule['group'] === '' ? null : $rule['group'];
                $services = array();
                if (is_array($rule['services'])) {
                    $services = array_map('strval', $rule['services']);
                }
                $conditions = array();
                $actions = array();
                $grouping = null;
                if ($rule['rate_operator'] !== null && $rule['rate'] !== null) {
                    $conditions[] = array(
                        'type' => 'comparison',
                        'argument' => 'rate',
                        'operator' => $rule['rate_operator'],
                        'value' => $rule['rate']
                    );
                }
                if ($rule['total_operator'] !== null && $rule['total'] !== null) {
                    $conditions[] = array(
                        'type' => 'comparison',
                        'argument' => 'total',
                        'operator' => $rule['total_operator'],
                        'value' => $rule['total']
                    );
                }
                if ($services) {
                    $conditions[] = array(
                        'type' => 'services',
                        'services' => $services
                    );
                }
                if ($rule['fixed'] !== null) {
                    $actions[] = array(
                        'type' => 'rateFixed',
                        'value' => $rule['fixed']
                    );
                }
                if ($rule['percent'] !== null) {
                    $actions[] = array(
                        'type' => 'ratePercent',
                        'value' => $rule['percent']
                    );
                }
                if ($rule['group'] !== null) {
                    $grouping = array(
                        'title' => $rule['group']
                    );
                }
                $result[] = array(
                    'condition' => array(
                        'type' => 'logical',
                        'operator' => 'and',
                        'conditions' => $conditions,
                    ),
                    'actions' => $actions,
                    'grouping' => $grouping
                );
            }
        }
        $this->setPost('adjustment_rules', $result);
    }

    /**
     * @param Locale $locale
     */
    protected function validateLocaleDepended($locale) {
        // float
        $this->setPost('box_weight_adj_fix', $locale->acceptLargeWeight($this->getPost('box_weight_adj_fix')));
        $this->setPost('weight_adj_fix', $locale->acceptLargeWeight($this->getPost('weight_adj_fix')));
        $this->setPost('dimension_adj_fix', $locale->acceptLength($this->getPost('dimension_adj_fix')));
        $this->setPost('weight_based_limit', $locale->acceptLargeWeight($this->getPost('weight_based_limit')));

        // weight based faked box
        if ($this->module->hasFeature(Features::WeightBasedFakedBox) &&
            $this->getPost('packer') === BaseModel::PackerWeightBased
        ) {
            $fakedBox = $this->getPost('weight_based_faked_box');
            $this->setPost('weight_based_faked_box', array(
                'length' => $locale->acceptLength($fakedBox['length']),
                'width' => $locale->acceptLength($fakedBox['width']),
                'height' => $locale->acceptLength($fakedBox['height'])
            ));
            foreach ($this->getPost('weight_based_faked_box') as $v) {
                if (!$v) {
                    $this->error['weight_based_faked_box'] = $this->language->get('error_weight_based_faked_box');
                }
            }
        }

        // custom packages
        $customPackages = $this->getPost('custom_packages');
        if (!is_array($customPackages)) {
            $customPackages = array();
        }
        foreach ($customPackages as $k => $package) {
            unset($customPackages[$k]['package_id']);
            $customPackages[$k]['length'] = isset($package['length']) ? $locale->acceptLength($package['length']) : 0;
            $customPackages[$k]['width'] = isset($package['width']) ? $locale->acceptLength($package['width']) : 0;
            $customPackages[$k]['height'] = isset($package['height']) ? $locale->acceptLength($package['height']) : 0;
            $customPackages[$k]['max_load'] = isset($package['max_load']) ?
                $locale->acceptLargeWeight($package['max_load']) : 0;
            $customPackages[$k]['tare'] = isset($package['tare']) ? $locale->acceptSmallWeight($package['tare']) : 0;
            if (!($customPackages[$k]['length'] && $customPackages[$k]['width'] && $customPackages[$k]['height'])) {
                unset($customPackages[$k]);
            }
        }
        $this->setPost('custom_packages', $customPackages);

        // custom envelopes
        $customEnvelopes = $this->getPost('custom_envelopes');
        if (!is_array($customEnvelopes)) {
            $customEnvelopes = array();
        }
        foreach ($customEnvelopes as $k => $envelope) {
            unset($customEnvelopes[$k]['envelope_id']);
            $customEnvelopes[$k]['length'] = isset($envelope['length']) ?
                $locale->acceptLength($envelope['length']) : 0;
            $customEnvelopes[$k]['width'] = isset($envelope['width']) ? $locale->acceptLength($envelope['width']) : 0;
            $customEnvelopes[$k]['max_height'] = isset($envelope['max_height']) ?
                $locale->acceptLength($envelope['max_height']) : 0;
            $customEnvelopes[$k]['max_load'] = isset($envelope['max_load']) ?
                $locale->acceptLargeWeight($envelope['max_load']) : 0;
            $customEnvelopes[$k]['tare'] = isset($envelope['tare']) ? $locale->acceptSmallWeight($envelope['tare']) : 0;
            if (!($customEnvelopes[$k]['length'] && $customEnvelopes[$k]['width'])) {
                unset($customEnvelopes[$k]);
            }
        }
        $this->setPost('custom_envelopes', $customEnvelopes);

        // promo
        $promos = $this->getPost('promo');
        if (!is_array($promos)) {
            $promos = array();
        }
        foreach ($promos as $k => $promo) {
            unset($promos[$k]['promo_id']);
            $promos[$k]['length'] = isset($promo['length']) ? $locale->acceptLength($promo['length']) : 0;
            $promos[$k]['width'] = isset($promo['width']) ? $locale->acceptLength($promo['width']) : 0;
            $promos[$k]['height'] = isset($promo['height']) ? $locale->acceptLength($promo['height']) : 0;
            $promos[$k]['weight'] = isset($promo['weight']) ? $locale->acceptLargeWeight($promo['weight']) : 0;
            $promos[$k]['min_cost'] = isset($promo['min_cost']) ? (float)$promo['min_cost'] : 0;
            if (!($promos[$k]['length'] && $promos[$k]['width'] && $promos[$k]['height'] &&
                $promos[$k]['weight'] && $promos[$k]['min_cost'])
            ) {
                unset($promos[$k]);
            }
        }
        usort($promos, function($a, $b) {
            if ($a['min_cost'] < $b['min_cost']) {
                return -1;
            }
            return 1;
        });
        $this->setPost('promo', $promos);
    }

    protected function validateFeatures() {
        if ($this->module->hasFeature(Features::ShippingLabel)) {
            $this->setPost('label', in_array($this->getPost('label'), BaseLabelsProvider::$labelOptions, true) ?
                $this->getPost('label') : BaseLabelsProvider::LabelDisabled);
        }

        if ($this->module->hasFeature(Features::ShipperAddress) ||
            (
                $this->module->hasFeature(Features::ShippingLabel) &&
                $this->isModEnabled() &&
                in_array($this->getPost('label'), BaseLabelsProvider::$labelOptionsEnabled, true)
            )
        ) {
            if (!$this->getPost('country_id')) {
                $this->error['country_id'] = $this->language->get('error_country_id');
            }
            $this->load->model('localisation/country');
            $shipperCountryInfo = $this->model_localisation_country->getCountry($this->getPost('country_id'));
            if (in_array($shipperCountryInfo['iso_code_2'], $this->module->getCountriesRequiredZone(), true)) {
                if (!$this->getPost('zone_id')) {
                    $this->error['zone_id'] = $this->language->get('error_zone_id');
                }
            }
            if (!$this->getPost('city')) {
                $this->error['city'] = $this->language->get('error_city');
            }
            if (!$this->getPost('address')) {
                $this->error['address'] = $this->language->get('error_address');
            }
            $this->setPost('country_id', (int)$this->getPost('country_id'));
            $this->setPost('zone_id', (int)$this->getPost('zone_id'));
            $this->setPost('residential', $this->toConfigBool($this->getPost('residential')));
        }

        if ($this->module->hasFeature(Features::ShippingLabelRequiresCustomerNumber) &&
            $this->module->hasFeature(Features::AuthCustomerNumber) &&
            $this->isModEnabled() &&
            $this->getPost('label') !== BaseLabelsProvider::LabelDisabled &&
            !$this->getPost('customer_number')
        ) {
            $this->error['customer_number'] = $this->language->get('error_customer_number');
        }

        if ($this->module->hasFeature(Features::RequiresCustomerNumber) && !$this->getPost('customer_number')) {
            $this->error['customer_number'] = $this->language->get('error_customer_number');
        }

        if ($this->module->hasFeature(Features::Insurance)) {
            $this->setPost('insurance', $this->toConfigBool($this->getPost('insurance')));
        }

        if ($this->module->hasFeature(Features::AuthKey)) {
            if (!$this->getPost('key')) {
                $this->error['key'] = $this->language->get('error_key');
            }
        }

        if ($this->module->hasFeature(Features::AuthPassword)) {
            if (!$this->getPost('password')) {
                $this->error['password'] = $this->language->get('error_password');
            }
        }

        if ($this->module->hasFeature(Features::AuthMeter)) {
            if (!$this->getPost('meter')) {
                $this->error['meter'] = $this->language->get('error_meter');
            }
        }

        if ($this->module->hasFeature(Features::Dropoff)) {
            if (!$this->getPost('dropoff')) {
                $this->error['dropoff'] = $this->language->get('error_dropoff');
            }
        }

        if ($this->module->hasFeature(Features::Pickup)) {
            if (!$this->getPost('pickup')) {
                $this->error['pickup'] = $this->language->get('error_pickup');
            }
        }

        if ($this->module->hasFeature(Features::AccountRates)) {
            $this->setPost('account_rates', $this->toConfigBool($this->getPost('account_rates')));
        }

        if ($this->module->hasFeature(Features::ProductionMode)) {
            $this->setPost('production', $this->toConfigBool($this->getPost('production')));
        }

        if ($this->module->hasFeature(Features::CommercialRates)) {
            if (!$this->getPost('commercial_rates')) {
                $this->error['commercial_rates'] = $this->language->get('error_required');
            }
        }

        if ($this->module->hasFeature(Features::OriginZip5Digits)) {
            $postCodeFiltered = substr(preg_replace('/[^\d]/', '', $this->getPost('postcode')), 0, 5);
            if (strlen($postCodeFiltered) < 5) {
                $this->error['postcode'] = $this->language->get('error_postcode');
            }
            $this->setPost('postcode', $postCodeFiltered);
        }

        if ($this->module->hasFeature(Features::OriginZip6Alphanumeric)) {
            $postCodeFiltered = substr(preg_replace('/[^A-Z0-9]/', '', strtoupper($this->getPost('postcode'))), 0, 6);
            if (strlen($postCodeFiltered) < 6) {
                $this->error['postcode'] = $this->language->get('error_postcode');
            }
            $this->setPost('postcode', $postCodeFiltered);
        }

        if ($this->module->hasFeature(Features::BillingAddress) && !$this->getPost('billing_same')) {
            if (!$this->getPost('billing_country_id')) {
                $this->error['billing_country_id'] = $this->language->get('error_billing_country_id');
            }
            if (!$this->getPost('billing_zone_id')) {
                $this->error['billing_zone_id'] = $this->language->get('error_billing_zone_id');
            }
            if (!$this->getPost('billing_city')) {
                $this->error['billing_city'] = $this->language->get('error_billing_city');
            }
            if (!$this->getPost('billing_postcode')) {
                $this->error['billing_postcode'] = $this->language->get('error_billing_postcode');
            }
            if (!$this->getPost('billing_address')) {
                $this->error['billing_address'] = $this->language->get('error_billing_address');
            }
            $this->setPost('billing_country_id', (int)$this->getPost('billing_country_id'));
            $this->setPost('billing_zone_id', (int)$this->getPost('billing_zone_id'));
        }
    }

    /**
     * Performs module settings View render
     * @throws CoreFeatureException
     */
    protected function outputModuleSettings() {
        if (property_exists($this, 'template') &&
            property_exists($this, 'children') &&
            CoreFeatureChecker::hasMethod($this, 'render')
        ) { // v1
            $this->template = 'common/header.tpl'; // first render other template to prefetch children
            $this->children = array(
                'common/header',
                'common/footer',
            );
            $this->render();
        } elseif (CoreFeatureChecker::hasProperty($this, 'load')) { // v2 or v3
            $this->data['header'] = $this->load->controller('common/header');
            $this->data['column_left'] = $this->load->controller('common/column_left');
            $this->data['footer'] = $this->load->controller('common/footer');
        } else {
            throw new CoreFeatureException('Can not find render method');
        }
        $this->response->setOutput(BaseView::render($this->data));
    }

    /**
     * @param string $text
     * @param string $contentType
     * @param null|string $downloadFilename Force to download with this filename
     */
    protected function respondWithText($text, $contentType = 'text/plain', $downloadFilename = null) {
        header('Content-Type: ' . $contentType);
        if ($downloadFilename) {
            header('Content-Disposition: attachment; filename=' . $downloadFilename);
            header('Pragma: no-cache');
        }
        echo $text;
        exit;
    }

    /**
     * @param string $errorMessage
     * @param int $code HTTP Error Code
     */
    protected function respondWithError($errorMessage = '', $code = 404) {
        switch ($code) {
            // @codingStandardsIgnoreStart
            case 100: $text = 'Continue'; break;
            case 101: $text = 'Switching Protocols'; break;
            case 200: $text = 'OK'; break;
            case 201: $text = 'Created'; break;
            case 202: $text = 'Accepted'; break;
            case 203: $text = 'Non-Authoritative Information'; break;
            case 204: $text = 'No Content'; break;
            case 205: $text = 'Reset Content'; break;
            case 206: $text = 'Partial Content'; break;
            case 300: $text = 'Multiple Choices'; break;
            case 301: $text = 'Moved Permanently'; break;
            case 302: $text = 'Moved Temporarily'; break;
            case 303: $text = 'See Other'; break;
            case 304: $text = 'Not Modified'; break;
            case 305: $text = 'Use Proxy'; break;
            case 400: $text = 'Bad Request'; break;
            case 401: $text = 'Unauthorized'; break;
            case 402: $text = 'Payment Required'; break;
            case 403: $text = 'Forbidden'; break;
            case 404: $text = 'Not Found'; break;
            case 405: $text = 'Method Not Allowed'; break;
            case 406: $text = 'Not Acceptable'; break;
            case 407: $text = 'Proxy Authentication Required'; break;
            case 408: $text = 'Request Time-out'; break;
            case 409: $text = 'Conflict'; break;
            case 410: $text = 'Gone'; break;
            case 411: $text = 'Length Required'; break;
            case 412: $text = 'Precondition Failed'; break;
            case 413: $text = 'Request Entity Too Large'; break;
            case 414: $text = 'Request-URI Too Large'; break;
            case 415: $text = 'Unsupported Media Type'; break;
            case 500: $text = 'Internal Server Error'; break;
            case 501: $text = 'Not Implemented'; break;
            case 502: $text = 'Bad Gateway'; break;
            case 503: $text = 'Service Unavailable'; break;
            case 504: $text = 'Gateway Time-out'; break;
            case 505: $text = 'HTTP Version not supported'; break;
            // @codingStandardsIgnoreEnd
            default:
                $code = 500;
                $text = 'Internal Server Error';
        }
        $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
        header($protocol . ' ' . $code . ' ' . $text);
        $this->respondWithText($errorMessage);
    }

    /**
     * Performs redirect to menu of shipping extensions depending on engine version
     * @param string $path
     * @param string $args
     * @throws CoreFeatureException
     */
    protected function smartRedirect($path, $args = '') {
        $args = $args ? '&' . $args : '';
        if (CoreFeatureChecker::hasMethod($this, 'redirect')) { // v1
            $this->redirect($this->link($path, $args));
        } elseif (CoreFeatureChecker::hasProperty($this, 'response')) { // v2
            $this->response->redirect($this->link($path, $args));
        } else {
            throw new CoreFeatureException('Can not find redirect method');
        }
    }

    /**
     * Returns customer groups depending on engine version
     * @return array
     * @throws CoreFeatureException
     */
    protected function getCustomerGroups() {
        if (CoreFeatureChecker::hasAdminModel('customer/customer_group')) { // v2
            $this->load->model('customer/customer_group');
            return $this->model_customer_customer_group->getCustomerGroups();
        } elseif (CoreFeatureChecker::hasAdminModel('sale/customer_group')) { // v1
            $this->load->model('sale/customer_group');
            return $this->model_sale_customer_group->getCustomerGroups();
        } else {
            throw new CoreFeatureException('Can not load customer group model');
        }
    }
}
}

namespace CanadapostSmartFlexibleRootNS\SmartFlexible {

class CoreFeatureChecker {
    /**
     * Checks that model file exists
     * @param string $modelName
     * @return bool
     * @throws CoreFeatureException
     */
    public static function hasAdminModel($modelName) {
        if (!defined('DIR_APPLICATION')) {
            throw new CoreFeatureException('Constant DIR_APPLICATION not defined');
        }
        if (!defined('DIR_CATALOG')) {
            throw new CoreFeatureException('Cannot use hasAdminModel from catalog');
        }
        $modelsDir = DIR_APPLICATION . 'model';
        return file_exists($modelsDir . '/' . $modelName . '.php');
    }

    /**
     * Checks if module is installed to the modification system
     * @param string $modificationName
     * @throws CoreFeatureException
     * @return bool
     */
    public static function isModificationInstalled($modificationName) {
        if (!defined('DIR_SYSTEM')) {
            throw new CoreFeatureException('Constant DIR_SYSTEM not defined');
        }
        if (!defined('DIR_MODIFICATION')) {
            return false;
        }
        return file_exists(DIR_SYSTEM . $modificationName . '.ocmod.xml');
    }

    /**
     * Checks ability to open and convert PDF files
     * @return bool
     */
    public static function isPdfConverterInstalled() {
        return (self::isImageMagickInstalled() || self::isGraphicsMagickInstalled()) && self::isGhostscriptInstalled();
    }

    /**
     * Checks that ImageMagick is installed
     * @return bool
     */
    public static function isImageMagickInstalled() {
        if (class_exists('Imagick') && class_exists('ImagickPixel')) {
            return true;
        }
        return false;
    }

    /**
     * Checks that PHP function is not disabled
     * @param string $func
     * @return bool
     */
    public static function isFunctionEnabled($func) {
        $disabledFunctions = array_map('trim', explode(',', ini_get('disable_functions')));
        return is_callable($func) && !in_array($func, $disabledFunctions, true);
    }

    /**
     * Checks that GraphicsMagick is installed
     * @return bool
     */
    public static function isGraphicsMagickInstalled() {
        if (self::isFunctionEnabled('shell_exec')) {
            if (shell_exec('which gm')) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks that Ghostscript is installed
     * @return bool
     */
    public static function isGhostscriptInstalled() {
        if (self::isFunctionEnabled('shell_exec')) {
            if (shell_exec('which gs')) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks that GD installed
     * @return bool
     */
    public static function isGdInstalled() {
        return function_exists('imagecreatefromstring');
    }

    /**
     * Checks that running under MijoShop
     * @return bool
     */
    public static function isMijoShop() {
        return class_exists('MijoShop');
    }

    /**
     * Checks that running under MiwoShop
     * @return bool
     */
    public static function isMiwoShop() {
        return class_exists('MiwoShop');
    }

    /**
     * Checks that property exists
     * @param object $object
     * @param string $propertyName
     * @return bool
     */
    public static function hasProperty($object, $propertyName) {
        if (property_exists($object, $propertyName)) {
            return true;
        }
        if (method_exists($object, '__get')) {
            return $object->__get($propertyName) !== null;
        }
        return false;
    }

    /**
     * Checks that method exists
     * @param object $object
     * @param string $methodName
     * @return bool
     */
    public static function hasMethod($object, $methodName) {
        return method_exists($object, $methodName);
    }

    /**
     * @return bool
     */
    public static function isBrowserSupportsFramePrint() {
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox') !== false) {
            return false;
        }
        return true;
    }

}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\ConfigurationException;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\CoreFeatureChecker;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\CoreFeatureException;

class Configuration {
    /** @var Object */
    private $config;
    /** @var BaseModule */
    private $module;
    /** @var array */
    private $defaultValues = array();

    /**
     * @param Object $config
     * @param BaseModule $module
     * @throws ConfigurationException
     * @throws CoreFeatureException
     */
    public function __construct($config, $module) {
        if (!CoreFeatureChecker::hasMethod($config, 'get')) {
            throw new CoreFeatureException('Can not find get method in config object');
        }
        if (!CoreFeatureChecker::hasMethod($config, 'set')) {
            throw new CoreFeatureException('Can not find set method in config object');
        }
        if (!($module instanceof BaseModule)) {
            throw new ConfigurationException('Incorrect module given');
        }
        $this->config = $config;
        $this->module = $module;
        $this->defaultValues = $this->module->getDefaultSettings();
        $this->applyBackwardsCompatibilityChanges();
    }

    /**
     * Proceeds compatibility operations
     */
    protected function applyBackwardsCompatibilityChanges() {
        $this->set('methods', $this->getShippingMethodsFromConfig());
        $this->set('processing_holidays', $this->getHolidaysFromConfig());
        $this->set('debug', $this->getDebugFromConfig());
        $this->set('label', $this->getLabelFromConfig());
    }

    /**
     * Returns configuration value
     * Use default module settings if not defined
     * @since 15.11.2017 use default settings when default is array and empty string defined
     * @param string $key
     * @return mixed
     */
    public function get($key) {
        $storedValue = $this->config->get($this->module->getPrefixedName($key));
        if ($storedValue === null) {
            if (isset($this->defaultValues[$key])) {
                return $this->defaultValues[$key];
            }
        }
        // fix empty string value when default setting is array
        if ($storedValue === '' && isset($this->defaultValues[$key]) && is_array($this->defaultValues[$key])) {
            return $this->defaultValues[$key];
        }
        return $storedValue;
    }

    /**
     * Set new configuration value
     * @param string $key
     * @param mixed $value
     */
    public function set($key, $value) {
        $this->config->set($this->module->getPrefixedName($key), $value);
    }

    /**
     * @param object $modelLocalisationCountry
     * @param null|int $overrideId Used in controller when new configuration values come
     * @return string|null
     */
    public function getOriginCountryCode($modelLocalisationCountry, $overrideId = null) {
        $countryId = $overrideId ?: $this->get('country_id');
        if ($countryId) {
            $originCountryInfo = $modelLocalisationCountry->getCountry($countryId);
            return isset($originCountryInfo['iso_code_2']) ? $originCountryInfo['iso_code_2'] : null;
        }
        return null;
    }

    /**
     * Returns configuration value of 'methods'
     * Has a backward compatibility to build array from separately stored methods
     * @return array
     */
    protected function getShippingMethodsFromConfig() {
        $storedArray = $this->get('methods');
        if (is_array($storedArray)) {
            return $storedArray;
        }
        $result = array();
        foreach ($this->module->getRatesProvider()->getAllShippingMethods() as $fullKey) {
            $result[$fullKey] = $this->get($fullKey);
        }
        return $result;
    }

    /**
     * Returns configuration value of 'processing_holidays'
     * Has a backward compatibility to build array from lines of text
     * @return array
     */
    protected function getHolidaysFromConfig() {
        $storedArray = $this->get('processing_holidays');
        if (is_array($storedArray)) {
            return $storedArray;
        }
        if (trim($storedArray) === '') {
            return array();
        }
        return array_map(function($mmdd) {
            return array_map('intval', explode('/', $mmdd));
        }, explode("\n", $storedArray));
    }

    /**
     * Returns configuration value of 'debug'
     * Has a backward compatibility from integer values
     * @return string
     */
    protected function getDebugFromConfig() {
        $storedConst = $this->get('debug');
        if (in_array($storedConst, Debugger::$debugLevels, true)) {
            return $storedConst;
        }
        $storedInteger = (int)$storedConst;
        switch ($storedInteger) {
            case 0:
                return Debugger::ShowNothingLogNothing;
            case 1:
                return Debugger::ShowNothingLogAll;
            case 2:
                return Debugger::ShowErrorsLogAll;
            default:
                return isset($this->defaultValues['debug']) ? $this->defaultValues['debug'] :
                    Debugger::ShowNothingLogErrors;
        }
    }

    /**
     * Returns configuration value for 'label'
     * Has a backward compatibility from integer values
     * @return string
     */
    protected function getLabelFromConfig() {
        $storedConst = $this->get('label');
        if (in_array($storedConst, BaseLabelsProvider::$labelOptions, true)) {
            return $storedConst;
        }
        $storedInteger = (int)$storedConst;
        switch ($storedInteger) {
            case 0:
                return BaseLabelsProvider::LabelDisabled;
            case 1:
                return BaseLabelsProvider::LabelAutomatically;
            default:
                return isset($this->defaultValues['label']) ? $this->defaultValues['label'] :
                    BaseLabelsProvider::LabelDisabled;
        }
    }
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping\BaseController {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\CoreFeatureChecker;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Origin\OriginPackage;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping\BaseRatesProvider;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping\BaseModel;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping\Debugger;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping\BaseModule;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping\Features;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping\BaseLabelsProvider;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\VersionChecker;

class Helper {
    public static $compareOperators = array('gt', 'gte', 'eq', 'lte', 'lt');

    public static $requiredTranslations = array(
        'heading_title', 'text_enabled', 'text_disabled', 'text_all_zones', 'text_none', 'text_yes',
        'text_no', 'text_select_all', 'text_unselect_all', 'entry_min_box_weight', 'entry_max_box_weight',
        'entry_minimum_rate', 'help_minimum_rate', 'entry_weight_adj_fix', 'help_weight_adj_fix',
        'entry_weight_adj_per', 'help_weight_adj_per', 'entry_dimension_adj_fix', 'help_dimension_adj_fix',
        'entry_rate_adj_fix', 'help_rate_adj_fix', 'entry_rate_adj_per', 'help_rate_adj_per', 'entry_user_id',
        'help_user_id', 'entry_postcode', 'entry_machinable', 'entry_insurance', 'entry_hubid', 'help_hubid',
        'help_machinable', 'entry_custom_packages', 'help_custom_packages', 'entry_display_time',
        'help_display_time', 'entry_display_weight', 'help_display_weight', 'entry_pounds', 'help_pounds',
        'entry_inches', 'help_inches', 'entry_tax', 'entry_geo_zones', 'help_geo_zones', 'entry_status',
        'entry_sort_order', 'help_sort_order', 'entry_debug', 'help_debug', 'entry_processing_days',
        'help_processing_days', 'entry_prefix', 'help_prefix', 'entry_sort_options', 'help_sort_options',
        'text_no_sorting', 'text_sort_by_price', 'text_sort_by_time', 'entry_processing_saturdays',
        'entry_processing_sundays', 'entry_processing_holidays', 'help_processing_holidays',
        'tab_general', 'tab_package', 'tab_shipping', 'tab_services', 'tab_adjustments', 'entry_version',
        'text_all_geo_zones', 'text_add_package', 'text_remove_package', 'entry_length', 'entry_width',
        'entry_height', 'entry_max_load', 'entry_promo', 'help_promo', 'entry_min_cost', 'text_add_promo',
        'text_remove_promo', 'entry_weight', 'entry_standard_packages', 'help_standard_packages',
        'text_debug_show_nothing_log_nothing', 'text_debug_show_nothing_log_errors', 'entry_insurance_from',
        'text_debug_show_nothing_log_all', 'text_debug_show_errors_log_errors', 'text_packer_weight_based',
        'text_debug_show_errors_log_all', 'entry_boxes_checked', 'button_save', 'button_cancel',
        'entry_boxes_unchecked', 'entry_services', 'help_services', 'entry_packer', 'help_packer',
        'demo_disabled', 'demo_title', 'demo_info', 'entry_customer_groups', 'help_customer_groups',
        'text_all_customer_groups', 'entry_stores', 'help_stores', 'text_all_stores', 'entry_account_rates',
        'entry_country_id', 'entry_zone_id', 'help_zone_id', 'entry_city', 'entry_address', 'entry_tare',
        'entry_residential', 'help_residential', 'entry_key', 'entry_password', 'entry_meter',
        'text_regular_pickup', 'text_business_service_center', 'text_drop_box', 'text_request_courier',
        'text_station', 'entry_dropoff', 'entry_production', 'entry_pickup', 'help_production',
        'entry_commercial_rates', 'text_retail_rates', 'text_commercial_rates', 'text_commercial_plus_rates',
        'text_production_mode', 'text_developer_mode', 'text_home', 'text_shipping', 'text_automatic',
        'text_packer_3d_packer', 'text_packer_individual', 'text_daily_pickup', 'text_customer_counter',
        'text_one_time_pickup', 'text_letter_center', 'text_air_service_center', 'entry_box_weight_adj_fix',
        'help_box_weight_adj_fix', 'entry_date_format', 'entry_label', 'entry_weight_based_limit',
        'help_label', 'entry_tracking', 'help_tracking', 'text_tracking_not_send', 'tab_labels',
        'text_tracking_send_immediately', 'text_tracking_send_shipped', 'entry_mod', 'help_postcode_dub',
        'entry_contact', 'entry_cutoff', 'entry_cutoff_help', 'help_cutoff', 'entry_max_height',
        'entry_custom_envelopes', 'help_custom_envelopes', 'text_add_envelope', 'entry_avoid_delivery',
        'help_avoid_delivery', 'entry_avoid_delivery_saturdays', 'entry_avoid_delivery_sundays',
        'entry_maintenance', 'text_export_settings', 'text_import_settings', 'text_add_holiday',
        'entry_measurement_system', 'help_measurement_system', 'entry_label_format', 'help_label_format',
        'text_original_label', 'text_4x6_label', 'entry_sender_name', 'entry_sender_company',
        'entry_sender_telephone', 'help_label_foreign_data', 'help_insurance', 'entry_customer_number',
        'help_customer_number', 'entry_contract_id', 'help_contract_id', 'entry_promo_code', 'help_promo_code',
        'entry_provider_currency', 'help_provider_currency', 'entry_individual_tare', 'help_individual_tare',
        'help_weight_based_limit', 'text_bugreport', 'text_labels_disabled', 'text_labels_automatically',
        'text_labels_manually', 'entry_webhook', 'help_webhook', 'entry_pdf_converter', 'help_pdf_converter',
        'text_pdf_converter_imagick', 'text_pdf_converter_gmagick', 'license_title', 'license_info', 'license_buy',
        'license_close', 'entry_adjustment_rules', 'help_adjustment_rules', 'text_add_adjustment_rule',
        'entry_weight_based_faked_box', 'help_weight_based_faked_box', 'entry_billing_same', 'help_billing_same',
        'entry_billing_country_id', 'entry_billing_zone_id', 'entry_billing_city', 'entry_billing_postcode',
        'entry_billing_address', 'text_only_for_admin', 'help_insurance_from');

    public static $translationsWithParameter = array('help_packer', 'help_custom_packages', 'help_standard_packages');

    // errors: index in BaseView => field key assigned in array of errors (warning is a top level error)
    public static $requiredErrors = array('error_warning' => 'warning', 'error_user_id' => 'user_id',
        'error_postcode' => 'postcode', 'error_pounds_id' => 'pounds_id', 'error_inches_id' => 'inches_id',
        'error_country_id' => 'country_id', 'error_zone_id' => 'zone_id', 'error_city' => 'city',
        'error_address' => 'address', 'error_key' => 'key', 'error_password' => 'password',
        'error_meter' => 'meter', 'error_dropoff' => 'dropoff', 'error_commercial_rates' => 'commercial_rates',
        'error_measurement_system' => 'measurement_system', 'error_sender_telephone' => 'sender_telephone',
        'error_provider_currency' => 'provider_currency', 'error_customer_number' => 'customer_number',
        'error_adjustment_rules' => 'adjustment_rules', 'error_weight_based_faked_box' => 'weight_based_faked_box',
        'error_billing_country_id' => 'billing_country_id', 'error_billing_zone_id' => 'billing_zone_id',
        'error_billing_city' => 'billing_city', 'error_billing_postcode' => 'billing_postcode',
        'error_billing_address' => 'billing_address');

    public static $requiredInputs = array('user_id', 'postcode', 'machinable', 'display_time', 'display_weight',
        'pounds_id', 'hubid', 'label', 'tax_class_id', 'inches_id', 'geo_zones', 'debug', 'status', 'sort_order',
        'minimum_rate', 'tracking', 'weight_adj_fix', 'weight_adj_per', 'dimension_adj_fix', 'rate_adj_fix',
        'rate_adj_per', 'cutoff', 'processing_days', 'prefix', 'sort_options', 'processing_saturdays',
        'processing_sundays', 'processing_holidays', 'all_geo_zones', 'custom_packages', 'promo', 'standard_packages',
        'packer', 'all_customer_groups', 'customer_groups', 'all_stores', 'stores', 'country_id', 'zone_id', 'city',
        'address', 'residential', 'insurance', 'key', 'password', 'meter', 'dropoff', 'account_rates',
        'production', 'commercial_rates', 'methods', 'box_weight_adj_fix', 'date_format', 'measurement_system',
        'custom_envelopes', 'avoid_delivery_saturdays', 'avoid_delivery_sundays', 'label_format',
        'customer_number', 'contract_id', 'promo_code', 'insurance_from', 'individual_tare', 'status_for_customers',
        'weight_based_limit', 'pdf_converter', 'adjustment_rules', 'weight_based_faked_box', 'billing_same',
        'billing_country_id', 'billing_zone_id', 'billing_city', 'billing_postcode', 'billing_address');

    public static $googleHolidays = array(
        array(
            'text' => 'Australian Holidays',
            'value' => 'en.australian#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Austrian Holidays',
            'value' => 'en.austrian#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Brazilian Holidays',
            'value' => 'en.brazilian#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Canadian Holidays',
            'value' => 'en.canadian#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'China Holidays',
            'value' => 'en.china#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Christian Holidays',
            'value' => 'en.christian#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Danish Holidays',
            'value' => 'en.danish#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Dutch Holidays',
            'value' => 'en.dutch#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Finnish Holidays',
            'value' => 'en.finnish#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'French Holidays',
            'value' => 'en.french#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'German Holidays',
            'value' => 'en.german#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Greek Holidays',
            'value' => 'en.greek#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Hong Kong (C) Holidays',
            'value' => 'en.hong_kong_c#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Hong Kong Holidays',
            'value' => 'en.hong_kong#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Indian Holidays',
            'value' => 'en.indian#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Indonesian Holidays',
            'value' => 'en.indonesian#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Iranian Holidays',
            'value' => 'en.iranian#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Irish Holidays',
            'value' => 'en.irish#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Islamic Holidays',
            'value' => 'en.islamic#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Italian Holidays',
            'value' => 'en.italian#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Japanese Holidays',
            'value' => 'en.japanese#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Jewish Holidays',
            'value' => 'en.jewish#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Malaysian Holidays',
            'value' => 'en.malaysia#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Mexican Holidays',
            'value' => 'en.mexican#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'New Zealand Holidays',
            'value' => 'en.new_zealand#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Norwegian Holidays',
            'value' => 'en.norwegian#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Philippines Holidays',
            'value' => 'en.philippines#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Polish Holidays',
            'value' => 'en.polish#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Portuguese Holidays',
            'value' => 'en.portuguese#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Russian Holidays',
            'value' => 'en.russian#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Singapore Holidays',
            'value' => 'en.singapore#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'South Africa Holidays',
            'value' => 'en.sa#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'South Korean Holidays',
            'value' => 'en.south_korea#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Spain Holidays',
            'value' => 'en.spain#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Swedish Holidays',
            'value' => 'en.swedish#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Taiwan Holidays',
            'value' => 'en.taiwan#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Thai Holidays',
            'value' => 'en.thai#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'UK Holidays',
            'value' => 'en.uk#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'US Holidays',
            'value' => 'en.usa#holiday@group.v.calendar.google.com'
        ),
        array(
            'text' => 'Vietnamese Holidays',
            'value' => 'en.vietnamese#holiday@group.v.calendar.google.com'
        )
    );

    public static $measurementSystems = array(
        array(
            'text' => 'Imperial (inches and pounds)',
            'value' => BaseModel::ImperialMeasures
        ),
        array(
            'text' => 'Metric (centimeters and kilograms)',
            'value' => BaseModel::MetricMeasures
        )
    );

    public static function getDisplayedStatuses($data) {
        $result = array(
            array(
                'text' => $data['text_enabled'],
                'value' => BaseModel::StatusEnabled
            ),
            array(
                'text' => $data['text_disabled'],
                'value' => BaseModel::StatusDisabled
            )
        );
        if (CoreFeatureChecker::hasAdminModel('user/api')) { // v2
            $result[] = array(
                'text' => $data['text_only_for_admin'],
                'value' => BaseModel::StatusOnlyForAdmin
            );
        }
        return $result;
    }

    public static function getBooleans($data) {
        return array(
            array(
                'text' => $data['text_yes'],
                'value' => 1
            ),
            array(
                'text' => $data['text_no'],
                'value' => 0
            )
        );
    }

    public static function getModes($data) {
        return array(
            array(
                'text' => $data['text_production_mode'],
                'value' => 1
            ),
            array(
                'text' => $data['text_developer_mode'],
                'value' => 0
            )
        );
    }

    public static function getMachinables($data) {
        return array(
            array(
                'text' => $data['text_yes'],
                'value' => BaseRatesProvider::MachinableTrue
            ),
            array(
                'text' => $data['text_no'],
                'value' => BaseRatesProvider::MachinableFalse
            ),
            array(
                'text' => $data['text_automatic'],
                'value' => BaseRatesProvider::MachinableAutomatic
            )
        );
    }

    public static function getDebugLevels($data) {
        return array(
            array(
                'text' => $data['text_debug_show_nothing_log_nothing'],
                'value' => Debugger::ShowNothingLogNothing
            ),
            array(
                'text' => $data['text_debug_show_nothing_log_errors'],
                'value' => Debugger::ShowNothingLogErrors
            ),
            array(
                'text' => $data['text_debug_show_nothing_log_all'],
                'value' => Debugger::ShowNothingLogAll
            ),
            array(
                'text' => $data['text_debug_show_errors_log_errors'],
                'value' => Debugger::ShowErrorsLogErrors
            ),
            array(
                'text' => $data['text_debug_show_errors_log_all'],
                'value' => Debugger::ShowErrorsLogAll
            )
        );
    }

    public static function getSortings($data) {
        return array(
            array(
                'text'  => $data['text_no_sorting'],
                'value' => ''
            ),
            array(
                'text'  => $data['text_sort_by_price'],
                'value' => BaseModel::SortByPrice
            ),
            array(
                'text'  => $data['text_sort_by_time'],
                'value' => BaseModel::SortByTime
            )
        );
    }

    /**
     * @param array $data
     * @param BaseModule $module
     * @return array
     */
    public static function getPackers($data, $module) {
        $result = array(
            array(
                'text' => $data['text_packer_3d_packer'],
                'value' => BaseModel::Packer3dPacker
            ),
            array(
                'text' => $data['text_packer_individual'],
                'value' => BaseModel::PackerIndividual
            )
        );
        if ($module->hasFeature(Features::PackerWeightBased)) {
            array_push($result, array(
                'text' => $data['text_packer_weight_based'],
                'value' => BaseModel::PackerWeightBased
            ));
        }
        return $result;
    }

    public static function getDropoffs($data) {
        return array(
            array(
                'text' => $data['text_regular_pickup'],
                'value' => BaseRatesProvider::DropoffRegularPickup
            ),
            array(
                'text' => $data['text_business_service_center'],
                'value' => BaseRatesProvider::DropoffBusinessServiceCenter
            ),
            array(
                'text' => $data['text_drop_box'],
                'value' => BaseRatesProvider::DropoffDropBox
            ),
            array(
                'text' => $data['text_request_courier'],
                'value' => BaseRatesProvider::DropoffRequestCourier
            ),
            array(
                'text' => $data['text_station'],
                'value' => BaseRatesProvider::DropoffStation
            )
        );
    }

    public static function getPickups($data) {
        return array(
            array(
                'text' => $data['text_daily_pickup'],
                'value' => BaseRatesProvider::PickupDailyPickup
            ),
            array(
                'text' => $data['text_customer_counter'],
                'value' => BaseRatesProvider::PickupCustomerCounter
            ),
            array(
                'text' => $data['text_one_time_pickup'],
                'value' => BaseRatesProvider::PickupOneTimePickup
            ),
            array(
                'text' => $data['text_letter_center'],
                'value' => BaseRatesProvider::PickupLetterCenter
            ),
            array(
                'text' => $data['text_air_service_center'],
                'value' => BaseRatesProvider::PickupAirServiceCenter
            )
        );
    }

    public static function getCommercialRates($data) {
        return array(
            array(
                'text' => $data['text_retail_rates'],
                'value' => BaseRatesProvider::RetailRates
            ),
            array(
                'text' => $data['text_commercial_rates'],
                'value' => BaseRatesProvider::CommercialRates
            ),
            array(
                'text' => $data['text_commercial_plus_rates'],
                'value' => BaseRatesProvider::CommercialPlusRates
            )
        );
    }

    public static function getLabels($data) {
        return array(
            array(
                'text' => $data['text_labels_disabled'],
                'value' => BaseLabelsProvider::LabelDisabled
            ),
            array(
                'text' => $data['text_labels_automatically'],
                'value' => BaseLabelsProvider::LabelAutomatically
            ),
            array(
                'text' => $data['text_labels_manually'],
                'value' => BaseLabelsProvider::LabelManually
            )
        );
    }

    public static function getTrackings($data) {
        return array(
            array(
                'text' => $data['text_tracking_not_send'],
                'value' => BaseLabelsProvider::TrackingNotSend
            ),
            array(
                'text' => $data['text_tracking_send_immediately'],
                'value' => BaseLabelsProvider::TrackingSendImmediately
            ),
            array(
                'text' => $data['text_tracking_send_shipped'],
                'value' => BaseLabelsProvider::TrackingSendShipped
            )
        );
    }

    public static function getDateFormats() {
        $result = array();
        foreach (BaseModel::$dateFormats as $dateFormat) {
            $result[] = array(
                'text' => date($dateFormat, time()),
                'value' => $dateFormat
            );
        }
        return $result;
    }

    public static function getHours($data) {
        return array_merge(
            array(
                array(
                    'text' => $data['text_none'],
                    'value' => BaseRatesProvider::CutoffDisabled
                )
            ),
            array_map(function($value){
                $value = str_pad($value, 2, '0', STR_PAD_LEFT) . ':00';
                return array(
                    'text' => $value,
                    'value' => $value
                );
            }, range(0, 23))
        );
    }

    /**
     * @param array $data
     * @param BaseModule $module
     * @param BaseLabelsProvider $labelsProvider
     * @return array
     */
    public static function getLabelFormats($data, $module, $labelsProvider) {
        $result = array(
            array(
                'text' => $data['text_original_label'],
                'value' => BaseLabelsProvider::LabelFormatOriginal
            )
        );
        if ($module->hasFeature(Features::ShippingLabel4x6Inches)) {
            if (($labelsProvider->getDocumentFormat() === BaseLabelsProvider::DocumentFormatPDF &&
                    CoreFeatureChecker::isPdfConverterInstalled()
                ) || ($labelsProvider->getDocumentFormat() === BaseLabelsProvider::DocumentFormatPNG)
            ) {
                array_push($result, array(
                    'text' => $data['text_4x6_label'],
                    'value' => BaseLabelsProvider::LabelFormat4x6
                ));
            }
        }
        return $result;
    }

    /**
     * @param array $data
     * @param BaseModule $module
     * @param BaseLabelsProvider $labelsProvider
     * @return array
     */
    public static function getPdfConverters($data, $module, $labelsProvider) {
        $result = array();
        if ($module->hasFeature(Features::ShippingLabel4x6Inches)) {
            if ($labelsProvider->getDocumentFormat() === BaseLabelsProvider::DocumentFormatPDF) {
                if (CoreFeatureChecker::isImageMagickInstalled()) {
                    array_push($result, array(
                        'text' => $data['text_pdf_converter_imagick'],
                        'value' => BaseLabelsProvider::PdfConverterImagick
                    ));
                }
                if (CoreFeatureChecker::isGraphicsMagickInstalled()) {
                    array_push($result, array(
                        'text' => $data['text_pdf_converter_gmagick'],
                        'value' => BaseLabelsProvider::PdfConverterGmagick
                    ));
                }
            }
        }
        return $result;
    }

    /**
     * @param BaseRatesProvider $ratesProvider
     * @param callable $filterFunction
     * @return array
     */
    public static function getBoxes($ratesProvider, $filterFunction) {
        $result = array_filter(
            $ratesProvider->getStandardPackages(), $filterFunction
        );
        usort($result, function($a, $b) {
            /** @var OriginPackage $a */
            /** @var OriginPackage $b */
            if ($a->getTitle() < $b->getTitle()) {
                return -1;
            }
            return 1;
        });
        return $result;
    }

    /**
     * @param object $modelStore
     * @param object $config
     * @return array
     */
    public static function getStores($modelStore, $config) {
        $result = array(
            array(
                'store_id' => 0,
                'name' => $config->get('config_name')
            )
        );
        $result = array_merge($result, $modelStore->getStores());
        return $result;
    }

    public static function getTaxClasses($data, $modelTaxClass) {
        return array_merge(array(array(
            'tax_class_id' => 0,
            'title' => $data['text_none']
        )), $modelTaxClass->getTaxClasses());
    }

    /**
     * @param array $data
     * @param BaseModule $module
     */
    public static function findPounds(&$data, $module) {
        $data['pounds_found'] = false;
        $searchPounds = array_filter($data['weight_classes'], function ($weightClass) {
            if (
                stripos($weightClass['title'], 'pound') !== false &&
                stripos($weightClass['unit'], 'lb') !== false
            ) {
                return true;
            }
            return false;
        });
        if (count($searchPounds) === 1) {
            $data['pounds_found'] = true;
            $pounds = array_pop($searchPounds);
            $data[$module->getPrefixedName('pounds_id')] = $pounds['weight_class_id'];
        }
    }

    /**
     * @param array $data
     * @param BaseModule $module
     */
    public static function findInches(&$data, $module) {
        $data['inches_found'] = false;
        $searchInches = array_filter($data['length_classes'], function ($lengthClass) {
            if (
                stripos($lengthClass['title'], 'inch') !== false &&
                stripos($lengthClass['unit'], 'in') !== false
            ) {
                return true;
            }
            return false;
        });
        if (count($searchInches) === 1) {
            $data['inches_found'] = true;
            $inches = array_pop($searchInches);
            $data[$module->getPrefixedName('inches_id')] = $inches['length_class_id'];
        }
    }

    public static function buildNavControls(&$data, $linkFunction, $currentRoute) {
        if (VersionChecker::get()->isVersion23()) {
            $backLink = call_user_func($linkFunction, 'extension/extension', 'type=shipping');
        } elseif (VersionChecker::get()->isVersion3()) {
            $backLink = call_user_func($linkFunction, 'marketplace/extension', 'type=shipping');
        } else {
            $backLink = call_user_func($linkFunction, 'extension/shipping');
        }
        $data['breadcrumbs'] = array(
            array(
                'text'      => $data['text_home'],
                'href'      => call_user_func($linkFunction, VersionChecker::get()->isVersion1() ?
                    'common/home' : 'common/dashboard'),
                'separator' => false
            ),
            array(
                'text'      => $data['text_shipping'],
                'href'      => $backLink,
                'separator' => ' :: '
            ),
            array(
                'text'      => $data['heading_title'],
                'href'      => call_user_func($linkFunction, $currentRoute),
                'separator' => ' :: '
            )
        );
        $data['action'] = call_user_func($linkFunction, $currentRoute);
        $data['cancel'] = $backLink;
    }
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\Controls\Tab;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Controls\Select;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Controls\Input;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\ViewLoader;

class BaseView {
    public static function render($data) {
        $viewBuilder = ViewLoader::getViewBuilder($data);

        /**
         * Shipper Address is an array of rendered controls
         * 0 - Controls to show before Origin Postcode (default control)
         * 1 - Controls to show after Origin Postcode
         * 2 - Optional controls (Residential)
         * @var string[] $shipperAddressHtml
         */
        $shipperAddressHtml[0] = $viewBuilder->renderSetting('entry_country_id', '', 'error_country_id', true,
            $viewBuilder->renderSelect(Select::create()
                ->setNameKey('country_id')
                ->setSourceKey('countries')
                ->setValueField('country_id')
                ->setCaptionField('name')));
        $shipperAddressHtml[0] .= $viewBuilder->renderSetting('entry_zone_id', 'help_zone_id', 'error_zone_id', true,
            $viewBuilder->renderSelect(Select::create()
                ->setNameKey('zone_id')));
        $shipperAddressHtml[0] .= $viewBuilder->renderSetting('entry_city', '', 'error_city', true,
            $viewBuilder->renderInput(Input::create()
                ->setNameKey('city')));
        $shipperAddressHtml[1] = $viewBuilder->renderSetting('entry_address', '', 'error_address', true,
            $viewBuilder->renderTextarea(Input::create()
                ->setNameKey('address')));
        $shipperAddressHtml[2] = $viewBuilder->renderSetting('entry_residential', 'help_residential', '', true,
            $viewBuilder->renderOptions(Select::create()
                ->setNameKey('residential')
                ->setSourceKey('booleans')));

        /**
         * VQMOD and Modification Status
         * @var string $modInfoHtml
         */
        if ($data['mod_file_enabled']) {
            $modStatusHtml = 'Modification is installed. Module is fully operational.';
        } else {
            if ($data['mod_file_installed']) {
                $modStatusHtml = (
                    'Modification is installed, but not activated.
                    Please follow to <a href="'.$data['mod_modifications_url'].'">Modifications</a>
                    and press Refresh (on the top right).'
                );
            } else {
                if ($data['vqmod_installed']) {
                    $modStatusHtml = (
                        'vQmod is installed but modification was not loaded.
                        Make sure you copied <b>vqmod</b> directory to your website.
                        Please contact us at
                            <a href="mailto:' . $data['contact_email'] . '">' . $data['contact_email'] . '</a>
                        if you cannot fix this problem yourself.'
                    );
                } else {
                    $modStatusHtml = (
                        'vQmod is not installed. <a href="https://github.com/vqmod/vqmod/releases">Download
                        the latest release</a> and
                        <a href="https://drugoe.de/kb/ocmod-vqmod">
                            follow this manual
                        </a>
                        to install it on top of your E-Commerce Shop setup.'
                    );
                }

            }
        }

        /**
         * Tab General
         */
        $tabGeneralHtml = $viewBuilder->renderSetting('entry_mod', '', '', false,
            $viewBuilder->renderStatic($modStatusHtml)
        );
        $tabGeneralHtml .= $viewBuilder->renderSetting('entry_status', '', '', false,
            $viewBuilder->renderOptions(Select::create()
                ->setNameKey('displayed_status')
                ->setSourceKey('displayed_statuses')));
        $tabGeneralHtml .= $viewBuilder->renderMeasurementSystem();
        $tabGeneralHtml .= $viewBuilder->renderSetting('entry_user_id', 'help_user_id', 'error_user_id', true,
            $data['is_demo_mode'] ?
                $viewBuilder->renderStatic($data['demo_disabled']) :
                $viewBuilder->renderInput(Input::create()
                    ->setNameKey('user_id')));
        if ($viewBuilder->hasFeature(Features::AuthKey)) {
            $tabGeneralHtml .= $viewBuilder->renderSetting('entry_key', '', 'error_key', true,
                $data['is_demo_mode'] ?
                    $viewBuilder->renderStatic($data['demo_disabled']) :
                    $viewBuilder->renderInput(Input::create()
                        ->setNameKey('key')));
        }
        if ($viewBuilder->hasFeature(Features::AuthPassword)) {
            $tabGeneralHtml .= $viewBuilder->renderSetting('entry_password', '', 'error_password', true,
                $data['is_demo_mode'] ?
                    $viewBuilder->renderStatic($data['demo_disabled']) :
                    $viewBuilder->renderInput(Input::create()
                        ->setNameKey('password')));
        }
        if ($viewBuilder->hasFeature(Features::AuthMeter)) {
            $tabGeneralHtml .= $viewBuilder->renderSetting('entry_meter', '', 'error_meter', true,
                $data['is_demo_mode'] ?
                    $viewBuilder->renderStatic($data['demo_disabled']) :
                    $viewBuilder->renderInput(Input::create()
                        ->setNameKey('meter')));
        }
        if ($viewBuilder->hasFeature(Features::AuthCustomerNumber)) {
            $tabGeneralHtml .= $viewBuilder->renderSetting('entry_customer_number', 'help_customer_number',
                'error_customer_number', $viewBuilder->hasFeature(Features::RequiresCustomerNumber),
                $data['is_demo_mode'] ? $viewBuilder->renderStatic($data['demo_disabled']) :
                    $viewBuilder->renderInput(Input::create()
                        ->setNameKey('customer_number')));
        }
        if ($viewBuilder->hasFeature(Features::AuthContractId)) {
            $tabGeneralHtml .= $viewBuilder->renderSetting('entry_contract_id', 'help_contract_id', '', false,
                $data['is_demo_mode'] ?
                    $viewBuilder->renderStatic($data['demo_disabled']) :
                    $viewBuilder->renderInput(Input::create()
                        ->setNameKey('contract_id')));
        }
        if ($viewBuilder->hasFeature(Features::Webhook)) {
            $tabGeneralHtml .= $viewBuilder->renderSetting('entry_webhook', 'help_webhook', '', false,
                $viewBuilder->renderWebhook());
        }
        if ($viewBuilder->hasFeature(Features::PromoCode)) {
            $tabGeneralHtml .= $viewBuilder->renderSetting('entry_promo_code', 'help_promo_code', '', false,
                $viewBuilder->renderInput(Input::create()
                    ->setNameKey('promo_code')));
        }
        if ($viewBuilder->hasFeature(Features::ProductionMode)) {
            $tabGeneralHtml .= $viewBuilder->renderSetting('entry_production', 'help_production', '', true,
                $viewBuilder->renderSelect(Select::create()
                    ->setNameKey('production')
                    ->setSourceKey('modes')));
        }
        if ($viewBuilder->hasFeature(Features::HubId)) {
            $tabGeneralHtml .= $viewBuilder->renderSetting('entry_hubid', 'help_hubid', '', false,
                $viewBuilder->renderInput(Input::create()
                    ->setNameKey('hubid')));
        }
        if ($viewBuilder->hasFeature(Features::ShipperAddress)) {
            $tabGeneralHtml .= $shipperAddressHtml[0]; // before origin postcode
        }
        $tabGeneralHtml .= $viewBuilder->renderSetting('entry_postcode', '', 'error_postcode', true,
            $viewBuilder->renderInput(Input::create()
                ->setNameKey('postcode')));
        if ($viewBuilder->hasFeature(Features::ShipperAddress)) { // after origin postcode + optional
            $tabGeneralHtml .= $shipperAddressHtml[1] . $shipperAddressHtml[2];
        }
        if ($viewBuilder->hasFeature(Features::BillingAddress)) {
            $tabGeneralHtml .= $viewBuilder->renderSetting('entry_billing_same', 'help_billing_same', '', false,
                $viewBuilder->renderOptions(Select::create()
                    ->setNameKey('billing_same')
                    ->setSourceKey('booleans')));
            $tabGeneralHtml .= $viewBuilder->renderSetting('entry_billing_country_id', '', 'error_billing_country_id',
                true, $viewBuilder->renderSelect(Select::create()
                    ->setNameKey('billing_country_id')
                    ->setSourceKey('countries')
                    ->setValueField('country_id')
                    ->setCaptionField('name')));
            $tabGeneralHtml .= $viewBuilder->renderSetting('entry_billing_zone_id', '', 'error_billing_zone_id', true,
                $viewBuilder->renderSelect(Select::create()
                    ->setNameKey('billing_zone_id')));
            $tabGeneralHtml .= $viewBuilder->renderSetting('entry_billing_city', '', 'error_billing_city', true,
                $viewBuilder->renderInput(Input::create()
                    ->setNameKey('billing_city')));
            $tabGeneralHtml .= $viewBuilder->renderSetting('entry_billing_postcode', '', 'error_billing_postcode', true,
                $viewBuilder->renderInput(Input::create()
                    ->setNameKey('billing_postcode')));
            $tabGeneralHtml .= $viewBuilder->renderSetting('entry_billing_address', '', 'error_billing_address', true,
                $viewBuilder->renderTextarea(Input::create()
                    ->setNameKey('billing_address')));
        }
        $tabGeneralHtml .= $viewBuilder->renderSetting('entry_sort_order', 'help_sort_order', '', false,
            $viewBuilder->renderInput(Input::create()
                ->setNameKey('sort_order')));
        if ($data['pounds_found']) {
            $tabGeneralHtml .= $viewBuilder->renderHidden(Input::create()
                ->setNameKey('pounds_id'));
        } else {
            $tabGeneralHtml .= $viewBuilder->renderSetting('entry_pounds', 'help_pounds', 'error_pounds_id', true,
                $viewBuilder->renderSelect(Select::create()
                    ->setNameKey('pounds_id')
                    ->setSourceKey('weight_classes')
                    ->setValueField('weight_class_id')
                    ->setCaptionField('title')));
        }
        if ($data['inches_found']) {
            $tabGeneralHtml .= $viewBuilder->renderHidden(Input::create()
                ->setNameKey('inches_id'));
        } else {
            $tabGeneralHtml .= $viewBuilder->renderSetting('entry_inches', 'help_inches', 'error_inches_id', true,
                $viewBuilder->renderSelect(Select::create()
                    ->setNameKey('inches_id')
                    ->setSourceKey('length_classes')
                    ->setValueField('length_class_id')
                    ->setCaptionField('title')));
        }
        if (!$data['provider_currency_found']) {
            $tabGeneralHtml .= $viewBuilder->renderSetting('entry_provider_currency', 'help_provider_currency',
                'error_provider_currency', true, $viewBuilder->renderStatic('Currency with ISO code <code>' .
                $data['provider_currency'] . '</code> is required for this extension to convert prices'));
        }
        $tabGeneralHtml .= $viewBuilder->renderSetting('entry_debug', 'help_debug', '', false,
            $viewBuilder->renderSelect(Select::create()
                ->setNameKey('debug')
                ->setSourceKey('debug_levels')));
        $tabGeneralHtml .= $viewBuilder->renderSetting('entry_version', '', '', false,
            $viewBuilder->renderStatic($data['version']));
        $tabGeneralHtml .= $viewBuilder->renderSetting('entry_contact', '', '', false, $viewBuilder->renderContact());
        $tabGeneralHtml .= $viewBuilder->renderSetting('entry_maintenance', '', '', false,
            $viewBuilder->renderMaintenance());
        $tabGeneral = Tab::create()
            ->setId('tab-ext-general')
            ->setTitleKey('tab_general')
            ->setIsDefault(true)
            ->setContent($tabGeneralHtml);

        /**
         * Tab Package
         */
        $tabPackageHtml = '';
        if ($viewBuilder->hasFeature(Features::Machinable)) {
            $tabPackageHtml = $viewBuilder->renderSetting('entry_machinable', 'help_machinable', '', false,
                $viewBuilder->renderOptions(Select::create()
                    ->setNameKey('machinable')
                    ->setSourceKey('machinables')));
        }
        $tabPackageHtml .= $viewBuilder->renderSetting('entry_packer', 'help_packer', '', false,
            $viewBuilder->renderSelect(Select::create()
                ->setNameKey('packer')
                ->setSourceKey('packers')));
        $tabPackageHtml .= $viewBuilder->renderSetting('entry_individual_tare', 'help_individual_tare', '', false,
            $viewBuilder->renderOptions(Select::create()
                ->setNameKey('individual_tare')
                ->setSourceKey('booleans')));
        if ($viewBuilder->hasFeature(Features::WeightBasedFakedBox)) {
            $tabPackageHtml .= $viewBuilder->renderSetting('entry_weight_based_faked_box',
                'help_weight_based_faked_box', 'error_weight_based_faked_box', true,
                $viewBuilder->renderWeightBasedFakedBox());
        }
        $tabPackageHtml .= $viewBuilder->renderSetting('entry_weight_based_limit', 'help_weight_based_limit', '', false,
            $viewBuilder->renderInput(Input::create()
                ->setNameKey('weight_based_limit')
                ->setCssClass('small-number')
                ->setPrerender(Array($viewBuilder, 'prerenderLargeWeightWithEmpty'))
                ->setAddons(null, $viewBuilder->getLocale()->renderLargeWeightUnit())
            ));
        if ($viewBuilder->hasFeature(Features::StandardPackages)) {
            $tabPackageHtml .= $viewBuilder->renderSetting(
                'entry_standard_packages', 'help_standard_packages', '', false, $viewBuilder->renderStandardPackages()
            );
        }
        if ($viewBuilder->hasFeature(Features::CustomPackages)) {
            $tabPackageHtml .= $viewBuilder->renderSetting('entry_custom_packages', 'help_custom_packages', '', false,
                $viewBuilder->renderCustomPackagesFixed());
            $tabPackageHtml .= $viewBuilder->renderSetting('entry_custom_envelopes', 'help_custom_envelopes', '', false,
                $viewBuilder->renderCustomPackagesExpandable());
        }
        $tabPackageHtml .= $viewBuilder->renderSetting(
            'entry_promo', 'help_promo', '', false, $viewBuilder->renderPromo()
        );
        $tabPackage = Tab::create()
            ->setId('tab-ext-package')
            ->setTitleKey('tab_package')
            ->setContent($tabPackageHtml);

        /**
         * Tab Shipping
         */
        $tabShippingHtml = $viewBuilder->renderSetting(
            'entry_processing_days', 'help_processing_days', '', false, $viewBuilder->renderProcessing()
        );
        $tabShippingHtml .= $viewBuilder->renderSetting(
            'entry_processing_holidays', 'help_processing_holidays', '', false, $viewBuilder->renderHolidays()
        );
        $tabShippingHtml .= $viewBuilder->renderSetting(
            'entry_cutoff', 'help_cutoff', '', false, $viewBuilder->renderCutoff()
        );
        $tabShippingHtml .= $viewBuilder->renderSetting(
            'entry_avoid_delivery', 'help_avoid_delivery', '', false, $viewBuilder->renderAvoidDelivery()
        );
        if ($viewBuilder->hasFeature(Features::AccountRates)) {
            $tabShippingHtml .= $viewBuilder->renderSetting('entry_account_rates', '', '', false,
                $viewBuilder->renderOptions(Select::create()
                    ->setNameKey('account_rates')
                    ->setSourceKey('booleans')));
        }
        if ($viewBuilder->hasFeature(Features::CommercialRates)) {
            $tabShippingHtml .= $viewBuilder->renderSetting('entry_commercial_rates', '', 'error_commercial_rates',
                true, $viewBuilder->renderSelect(Select::create()
                    ->setNameKey('commercial_rates')
                    ->setSourceKey('commercial_rates')));
        }
        if ($viewBuilder->hasFeature(Features::Insurance)) {
            $tabShippingHtml .= $viewBuilder->renderSetting('entry_insurance', 'help_insurance', '', false,
                $viewBuilder->renderInsurance());
        }
        if ($viewBuilder->hasFeature(Features::Dropoff)) {
            $tabShippingHtml .= $viewBuilder->renderSetting('entry_dropoff', '', 'error_dropoff', true,
                $viewBuilder->renderSelect(Select::create()
                    ->setNameKey('dropoff')
                    ->setSourceKey('dropoffs')));
        }
        if ($viewBuilder->hasFeature(Features::Pickup)) {
            $tabShippingHtml .= $viewBuilder->renderSetting('entry_pickup', '', 'error_pickup', true,
                $viewBuilder->renderSelect(Select::create()
                    ->setNameKey('pickup')
                    ->setSourceKey('pickups')));
        }
        $tabShippingHtml .= $viewBuilder->renderSetting('entry_display_time', 'help_display_time', '', false,
            $viewBuilder->renderOptions(Select::create()
                    ->setNameKey('display_time')
                    ->setSourceKey('booleans')));
        $tabShippingHtml .= $viewBuilder->renderSetting('entry_date_format', '', '', false,
            $viewBuilder->renderSelect(
                Select::create()
                    ->setNameKey('date_format')
                    ->setSourceKey('date_formats')));
        $tabShippingHtml .= $viewBuilder->renderSetting('entry_display_weight', 'help_display_weight', '', false,
            $viewBuilder->renderOptions(Select::create()
                    ->setNameKey('display_weight')
                    ->setSourceKey('booleans')));
        $tabShippingHtml .= $viewBuilder->renderSetting('entry_tax', '', '', false,
            $viewBuilder->renderSelect(
                Select::create()
                    ->setNameKey('tax_class_id')
                    ->setSourceKey('tax_classes')
                    ->setValueField('tax_class_id')
                    ->setCaptionField('title')));
        $tabShippingHtml .= $viewBuilder->renderSetting('entry_geo_zones', 'help_geo_zones', '', false,
            $viewBuilder->renderGeoZones());
        $tabShippingHtml .= $viewBuilder->renderSetting('entry_customer_groups', 'help_customer_groups', '', false,
            $viewBuilder->renderCustomerGroups());
        $tabShippingHtml .= $viewBuilder->renderSetting('entry_stores', 'help_stores', '', false,
            $viewBuilder->renderStores());
        $tabShippingHtml .= $viewBuilder->renderSetting('entry_prefix', 'help_prefix', '', false,
            $viewBuilder->renderInput(Input::create()
                ->setNameKey('prefix')));
        $tabShippingHtml .= $viewBuilder->renderSetting('entry_sort_options', 'help_sort_options', '', false,
            $viewBuilder->renderSelect(Select::create()
                ->setNameKey('sort_options')
                ->setSourceKey('sortings')));
        $tabShipping = Tab::create()
            ->setId('tab-ext-shipping')
            ->setTitleKey('tab_shipping')
            ->setContent($tabShippingHtml);

        /**
         * Tab Labels
         */
        if ($viewBuilder->hasFeature(Features::ShippingLabel)) {
            if ($data['mod_file_enabled']) {
                $tabLabelsHtml = $viewBuilder->renderSetting('entry_label', 'help_label', '', false,
                    $viewBuilder->renderOptions(Select::create()
                        ->setNameKey('label')
                        ->setSourceKey('labels')));
                $tabLabelsHtml .= $viewBuilder->renderLabelFormatAndConverter();
                $tabLabelsHtml .= $viewBuilder->renderSetting('entry_tracking', 'help_tracking', '', false,
                    $viewBuilder->renderSelect(Select::create()
                        ->setNameKey('tracking')
                        ->setSourceKey('trackings')));
                if (!$viewBuilder->hasFeature(Features::ShipperAddress)) {
                    $tabLabelsHtml .= $shipperAddressHtml[0]; // before origin postcode
                    $tabLabelsHtml .= $viewBuilder->renderSetting('entry_postcode', 'help_postcode_dub', '', true,
                        $viewBuilder->renderInput(Input::create()
                            ->setNameKey('postcode_dub')
                            ->setIsDisabled(true)));
                    $tabLabelsHtml .= $shipperAddressHtml[1]; // after origin postcode
                }
                $tabLabelsHtml .= $viewBuilder->renderSetting('entry_sender_name', 'help_label_foreign_data', '', true,
                    $viewBuilder->renderInput(Input::create()
                        ->setNameKey('sender_name')
                        ->setIsDisabled(true)));
                $tabLabelsHtml .= $viewBuilder->renderSetting('entry_sender_company', 'help_label_foreign_data', '',
                    true, $viewBuilder->renderInput(Input::create()
                        ->setNameKey('sender_company')
                        ->setIsDisabled(true)));
                $tabLabelsHtml .= $viewBuilder->renderSetting('entry_sender_telephone', 'help_label_foreign_data',
                    'error_sender_telephone', true, $viewBuilder->renderInput(Input::create()
                        ->setNameKey('sender_telephone')
                        ->setIsDisabled(true)));
            } else {
                $tabLabelsHtml = $viewBuilder->renderSetting('entry_mod', '', '', false,
                    $viewBuilder->renderStatic(
                        'Modification file should be enabled to use this
                        feature. Follow instructions on the General Tab.'
                    )
                );
            }
            $tabLabels = Tab::create()
                ->setId('tab-ext-labels')
                ->setTitleKey('tab_labels')
                ->setContent($tabLabelsHtml);
        } else {
            $tabLabels = null;
        }

        /**
         * Tab Services
         */
        $tabServicesHtml = $viewBuilder->renderSetting(
            'entry_services', 'help_services', '', false, $viewBuilder->renderServices()
        );
        $tabServices = Tab::create()
            ->setId('tab-ext-services')
            ->setTitleKey('tab_services')
            ->setContent($tabServicesHtml);

        /**
         * Tab Adjustment
         */
        $tabAdjustmentsHtml = $viewBuilder->renderSetting('entry_box_weight_adj_fix', 'help_box_weight_adj_fix', '',
            false, $viewBuilder->renderInput(Input::create()
                ->setNameKey('box_weight_adj_fix')
                ->setCssClass('small-number')
                ->setAddons(null, $viewBuilder->getLocale()->renderLargeWeightUnit())
                ->setPrerender(Array($viewBuilder->getLocale(), 'renderLargeWeight'))));
        $tabAdjustmentsHtml .= $viewBuilder->renderSetting('entry_weight_adj_fix', 'help_weight_adj_fix', '', false,
            $viewBuilder->renderInput(Input::create()
                ->setNameKey('weight_adj_fix')
                ->setCssClass('small-number')
                ->setAddons(null, $viewBuilder->getLocale()->renderLargeWeightUnit())
                ->setPrerender(Array($viewBuilder->getLocale(), 'renderLargeWeight'))));
        $tabAdjustmentsHtml .= $viewBuilder->renderSetting('entry_weight_adj_per', 'help_weight_adj_per', '', false,
            $viewBuilder->renderInput(Input::create()
                ->setNameKey('weight_adj_per')
                ->setCssClass('small-number')
                ->setAddons(null, '%')));
        $tabAdjustmentsHtml .= $viewBuilder->renderSetting(
            'entry_dimension_adj_fix', 'help_dimension_adj_fix', '', false,
            $viewBuilder->renderInput(Input::create()
                ->setNameKey('dimension_adj_fix')
                ->setCssClass('small-number')
                ->setAddons(null, $viewBuilder->getLocale()->renderLengthUnit())
                ->setPrerender(Array($viewBuilder->getLocale(), 'renderLength'))));
        $tabAdjustmentsHtml .= $viewBuilder->renderSetting('entry_rate_adj_fix', 'help_rate_adj_fix', '', false,
            $viewBuilder->renderInput(Input::create()
                ->setNameKey('rate_adj_fix')
                ->setCssClass('small-number')
                ->setAddons(null, $data['currency_unit'])));
        $tabAdjustmentsHtml .= $viewBuilder->renderSetting('entry_rate_adj_per', 'help_rate_adj_per', '', false,
            $viewBuilder->renderInput(Input::create()
                ->setNameKey('rate_adj_per')
                ->setCssClass('small-number')
                ->setAddons(null, '%')));
        $tabAdjustmentsHtml .= $viewBuilder->renderSetting('entry_minimum_rate', 'help_minimum_rate', '', false,
            $viewBuilder->renderInput(Input::create()
                ->setNameKey('minimum_rate')
                ->setCssClass('small-number')
                ->setAddons(null, $data['currency_unit'])));
        $tabAdjustmentsHtml .= $viewBuilder->renderSetting('entry_adjustment_rules', 'help_adjustment_rules', '',
            false, $viewBuilder->renderAdjustmentRules());
        $tabAdjustments = Tab::create()
            ->setId('tab-ext-adjustments')
            ->setTitleKey('tab_adjustments')
            ->setContent($tabAdjustmentsHtml);

        /**
         * Assembly
         */
        $tabs = $viewBuilder->renderTabs(array(
            $tabGeneral, $tabPackage, $tabShipping, $tabLabels, $tabServices, $tabAdjustments
        ));
        $form = $viewBuilder->renderForm($tabs);
        $container = $viewBuilder->renderContainer($form);
        return $viewBuilder->finallyRenderModuleSettings($container);
    }

    public static function replyZones($data) {
        $viewBuilder = ViewLoader::getViewBuilder($data);
        return $viewBuilder->renderSelect(Select::create()
            ->setNameKey('zone_id')
            ->setSourceKey('zones')
            ->setValueField('zone_id')
            ->setCaptionField('name'));
    }

    public static function replyBillingZones($data) {
        $viewBuilder = ViewLoader::getViewBuilder($data);
        return $viewBuilder->renderSelect(Select::create()
            ->setNameKey('billing_zone_id')
            ->setSourceKey('zones')
            ->setValueField('zone_id')
            ->setCaptionField('name'));
    }

    public static function replyServices($data) {
        $viewBuilder = ViewLoader::getViewBuilder($data);
        return $viewBuilder->renderServices();
    }

    public static function replyServicesByUser($data) {
        $viewBuilder = ViewLoader::getViewBuilder($data);
        return $viewBuilder->renderServicesByUser();
    }

    public static function replyPackagingList($data) {
        $viewBuilder = ViewLoader::getViewBuilder($data);
        $viewBuilder->setIsStandaloneTemplate(true);
        $packagingList = $viewBuilder->renderPackagingList($data);
        return $viewBuilder->finallyRenderStandalonePage($packagingList);
    }

    public static function replyDocumentWrapper($data) {
        $viewBuilder = ViewLoader::getViewBuilder($data);
        $viewBuilder->setIsStandaloneTemplate(true);
        $wrapper = $viewBuilder->renderDocumentWrapper($data);
        return $viewBuilder->finallyRenderStandalonePage($wrapper);
    }

    public static function replyPackagingMap($data) {
        $viewBuilder = ViewLoader::getViewBuilder($data);
        $viewBuilder->setIsStandaloneTemplate(true);
        return $viewBuilder->finallyRenderPackagingMap($data);
    }

}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\ConfigurationException;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\FileManager;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Ordered\PackagingList;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Ordered\Shipment;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Arrays;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Address;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Packer\ValuableBox;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\CurrencyConverter;

class PackagingListManager extends FileManager {

    const ListsDir = 'smart-flexible/packaging-lists/';

    /**
     * @param PackagingList $packagingList
     * @return int bytes written
     */
    public static function save($packagingList) {
        $path = DIR_CACHE . self::ListsDir;
        self::createFolder($path);
        $json = json_encode($packagingList->toArray(), defined('JSON_PRETTY_PRINT') ? JSON_PRETTY_PRINT : 0);
        return self::createFile($path . $packagingList->getOrderId() . '.json', $json);
    }

    /**
     * @param $orderId
     * @return null|string
     */
    public static function readRaw($orderId) {
        $path = DIR_CACHE . self::ListsDir;
        $file = $path . $orderId . '.json';
        if (!file_exists($file)) {
            return null;
        }
        return file_get_contents($file);
    }

    /**
     * @param $orderId
     * @return PackagingList|null
     */
    public static function load($orderId) {
        $data = self::readRaw($orderId);
        if ($data === null) {
            return null;
        }
        try {
            $data = json_decode($data, true);
            if (Arrays::isAssoc($data)) {
                return PackagingList::createFromArray($data);
            } else {
                // backward compatibility - only shipments stored
                return new PackagingList(array(
                    'order_id' => $orderId,
                    'service_name' => null,
                    'version' => null,
                    'method_code' => null,
                    'address' => array(),
                    'method_name' => null,
                    'shipments' => array_map(function ($shipment) {
                        return Shipment::createFromArray($shipment);
                    }, $data)
                ));
            }
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @param int $orderId
     * @param int|null $shipmentId Use null to request all labels
     * @param BaseModule $module
     * @param callable $debugger
     * @param null|BaseValidationProvider $validationProvider
     * @param BaseLabelsProvider $labelsProvider
     * @param Configuration $configuration
     * @param CurrencyConverter $converter
     * @param object[] $models Array of models with indexes: country, zone
     * @throws ConfigurationException
     * @since 31.10.2017 accepts debugger to forward it to providers
     * @return null|PackagingList
     */
    public static function requestLabels(
        $orderId, $shipmentId, $module, $debugger, $validationProvider,
        $labelsProvider, $configuration, $converter, $models
    ) {
        if (!($module->hasFeature(Features::ShippingLabel) && $labelsProvider)) {
            throw new ConfigurationException('Feature is not enabled');
        }
        $packagingList = self::load($orderId);
        if (!$packagingList) {
            throw new ConfigurationException('Can not read packaging list');
        }
        $address = Address::fixAddress($packagingList->getAddress());
        $address = $module->fixAddress($address, $models['country'], $models['zone']);

        // validation request
        if ($validationProvider) {
            $validationProvider->setDebugger($debugger);
            $address['is_residential'] = $validationProvider->isAddressResidential($address);
        }

        $boxes = array_map(function($shipment) {
            /** @var Shipment $shipment */
            return $shipment->getBox();
        }, $packagingList->getShipments());
        $labelsProvider->setDebugger($debugger);
        $labelsProvider->setOption('insurance', ValuableBox::shouldInsuranceBeIncluded(
            null, $boxes, $configuration, $converter, $module->getValueCurrencyCode(
                $configuration->getOriginCountryCode($models['country'])
            )
        ));
        $labelsProvider->setOption('value_currency_code', $module->getValueCurrencyCode(
            $configuration->getOriginCountryCode($models['country'])
        ));
        $labelsProvider->setOption('get_country_code', function ($countryId) use ($models) {
            if (!$countryId) {
                return null;
            }
            $countryData = $models['country']->getCountry($countryId);
            return $countryData['iso_code_2'];
        });
        $labelsProvider->setOption('get_zone_code', function ($zoneId) use ($models) {
            if (!$zoneId) {
                return null;
            }
            $zoneData = $models['zone']->getZone($zoneId);
            return $zoneData['code'];
        });

        $shipments = array();
        foreach ($packagingList->getShipments() as $k => $shipment) {
            if (!$shipment->hasLabel()) {
                if ($shipmentId !== null) {
                    if ($shipmentId === $k) {
                        $shipments[$k] = $shipment;
                    }
                } else {
                    $shipments[$k] = $shipment;
                }
            }
        }

        $newShipments = $labelsProvider->queryLabelsAndTracking(
            $orderId, $shipments, $address, $packagingList->getMethodCode()
        );
        $shipments = $newShipments + $packagingList->getShipments();
        ksort($shipments);

        $packagingList = new PackagingList(array(
            'order_id' => $orderId,
            'service_name' => $packagingList->getServiceName(),
            'version' => $packagingList->getVersion(),
            'method_code' => $packagingList->getMethodCode(),
            'address' => $address,
            'method_name' => $packagingList->getMethodName(),
            'shipments' => $shipments
        ));
        PackagingListManager::save($packagingList);
        return $packagingList;
    }

}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible {

class ViewLoader {
    /**
     * Required $data fields to be Callable
     * @var array
     */
    private static $requiredCallables = array(
        'get_prefixed_name',
        'get_extension_name',
        'has_feature',
        'get_current_route'
    );

    /**
     * Returns ViewBuilder depending on engine version
     * @param array $data
     * @throws \Exception
     * @return ViewBuilderV1|ViewBuilderV2|ViewBuilderV3
     */
    public static function getViewBuilder($data) {
        foreach (self::$requiredCallables as $field) {
            if (!isset($data[$field])) {
                throw new \Exception('Field ' . $field . ' is not defined for ViewLoader');
            }
            if (!is_callable($data[$field])) {
                throw new \Exception('Field ' . $field . ' is not callable (ViewLoader)');
            }
        }
        $versionChecker = VersionChecker::get();
        if ($versionChecker->isVersion1()) {
            return new ViewBuilderV1($data);
        } elseif ($versionChecker->isVersion2()) {
            return new ViewBuilderV2($data);
        } elseif ($versionChecker->isVersion3()) {
            return new ViewBuilderV3($data);
        } else {
            throw new \Exception('Can not load proper view builder (ViewLoader)');
        }
    }
}
}

namespace CanadapostSmartFlexibleRootNS\SmartFlexible {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\Origin\OriginPackage;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping\BaseModel;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping\Features;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Ordered\Shipment;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping\BaseLabelsProvider;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Ordered\OrderedBoxPackedItem;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Weight;

abstract class ViewBuilder {
    protected $data;
    /** @var bool Force to return absolute path (for MijoShop and MiwoShop only) */
    protected $isStandaloneTemplate = false;

    /**
     * @param array $data
     */
    public function __construct($data) {
        $this->data = $data;
    }

    /**
     * @param bool $b
     */
    public function setIsStandaloneTemplate($b) {
        $this->isStandaloneTemplate = (bool)$b;
    }

    /**
     * @return Locale
     */
    public function getLocale() {
        return $this->data['locale'];
    }

    /**
     * Imported from Module
     * @return string
     */
    protected function getExtensionName() {
        return call_user_func($this->data['get_extension_name']);
    }

    protected function getOcFolder() {
        return chr(111) . chr(112) . chr(101) . chr(110) . chr(99) . chr(97) . chr(114) . chr(116);
    }

    /**
     * @return string
     */
    protected function getViewPath() {
        if (CoreFeatureChecker::isMijoShop() && $this->isStandaloneTemplate) {
            return HTTP_SERVER_TEMP . 'components/com_mijoshop/' . $this->getOcFolder() . '/admin/view';
        }
        if (CoreFeatureChecker::isMiwoShop()) {
            if ($this->isStandaloneTemplate) {
                return HTTP_SERVER_TEMP . 'wp-content/plugins/miwoshop/site/' . $this->getOcFolder() . '/admin/view';
            }
            return 'admin/view';
        }
        return 'view';
    }

    /**
     * @return string
     * @since 02.05.2017 public again for PHP 5.3 compatibility, called in closure
     */
    public function getImagesPath() {
        return $this->getViewPath() . '/image/' . $this->getExtensionName();
    }

    /**
     * @return string
     */
    protected function getJavascriptPath() {
        return $this->getViewPath() . '/javascript/smart-flexible';
    }

    /**
     * @return string
     */
    protected function getStylesPath() {
        if ((CoreFeatureChecker::isMijoShop() || CoreFeatureChecker::isMiwoShop()) && !$this->isStandaloneTemplate) {
            // magic: they does not properly convert style links
            return 'admin/view/stylesheet/smart-flexible';
        }
        return $this->getViewPath() . '/stylesheet/smart-flexible';
    }

    /**
     * Imported from Module
     * @param string $key
     * @return string
     */
    protected function getPrefixedName($key) {
        return call_user_func($this->data['get_prefixed_name'], $key);
    }

    /**
     * @param string $feature
     * @return bool
     */
    public function hasFeature($feature) {
        return call_user_func($this->data['has_feature'], $feature);
    }

    /**
     * @return array
     */
    protected function getMethodCodesCustom() {
        if (isset($this->data['method_codes_custom'])) {
            if (is_array($this->data['method_codes_custom'])) {
                return $this->data['method_codes_custom'];
            }
        }
        return array();
    }

    /**
     * @return string
     */
    protected function getCurrentRoute() {
        return call_user_func($this->data['get_current_route']);
    }

    /**
     * @return string
     */
    protected function getEntryPoint() {
        if (CoreFeatureChecker::isMiwoShop()) {
            return 'admin.php?page=miwoshop&option=com_miwoshop';
        }
        return 'index.php?';
    }

    /**
     * Returns value of $data element by key
     * @param $key
     * @return mixed
     */
    protected function getData($key) {
        return isset($this->data[$key]) ? $this->data[$key] : '';
    }

    /**
     * Returns value of $data element by prefixed key
     * See $this->getPrefixedName()
     * @since 20.04.2017 accepts array indexes
     * @param $key
     * @return mixed
     */
    protected function getValue($key) {
        if (preg_match('/(.+)\[(.+)\]/', $key, $matches)) {
            $array = $this->getData($this->getPrefixedName($matches[1]));
            return isset($array[$matches[2]]) ? $array[$matches[2]] : '';
        }
        return $this->getData($this->getPrefixedName($key));
    }

    abstract protected function getTableCssClass($isForServices = false);
    abstract protected function getCenterCssClass();
    abstract protected function getHelpCssClass();
    abstract protected function getHeader();

    /**
     * Returns global footer
     * @return string
     */
    protected function getFooter() {
        return $this->getData('footer');
    }

    /**
     * @return string
     */
    protected function getjQuery() {
        return '<script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>';
    }

    protected function getThreeJs() {
        return '<script src="https://cdnjs.cloudflare.com/ajax/libs/three.js/85/three.min.js"></script>';
    }

    protected function getLicenseLogo() {
        return sprintf('url(' . Author::LogoUrl . ')', $this->getExtensionName(), $this->getData('version'));
    }

    abstract protected function getInitMethod();

    /**
     * Returns view scripts
     * @return string
     */
    protected function getScripts() {
        $scripts = array();
        $scripts[] = sprintf('<script src="%s/%s.js?%s"></script>',
            $this->getJavascriptPath(), $this->getExtensionName(), $this->getData('version'));
        $methodsDependOnShipperCountry = $this->hasFeature(Features::ShipperAddress) &&
            $this->hasFeature(Features::MethodsDependOnShipperCountry);
        $methodsDependOnUserId = $this->hasFeature(Features::MethodsDependOnUserId);
        $scripts[] = sprintf('<script>$(function() {' .
            'objSmartFlexible.init({imagesPath: "%s", currentRoute: "%s", token: "%s", entryPoint: "%s", ' .
            'individualTare: "%s", weightBasedLimit: "%s", standardPackages: "%s", customPackages: "%s", ' .
            'customEnvelopes: "%s", promo: "%s", processingHolidays: "%s", insurance: "%s", insuranceFrom: "%s", ' .
            'packer: "%s", label: "%s", labelDisabled: "%s", postcode: "%s", countryId: "%s", zoneId: "%s", ' .
            'postcodeDub: "%s", measurementSystem: "%s", center: "%s", packer3dPacker: "%s", packerIndividual: "%s", ' .
            'packerWeightBased: "%s", textRemovePackage: "%s", textRemovePromo: "%s", ' .
            'methodsDependOnShipperCountry: "%s", labelFormat: "%s", tracking: "%s", city: "%s", address: "%s", ' .
            'senderName: "%s", senderCompany: "%s", senderTelephone: "%s", userId: "%s", ' .
            'methodsDependOnUserId: "%s", labelOriginal: "%s", pdfConverter: "%s", labelManually: "%s", ' .
            'trackingImmediately: "%s", licenseCookie: "%s", adjustmentRules: "%s", weightBasedFakedBox: "%s", ' .
            'billingSame: "%s", billingCountryId: "%s", billingZoneId: "%s", billingCity: "%s", ' .
            'billingPostcode: "%s", billingAddress: "%s", tokenName: "%s"}); objSmartFlexible.%s(); });</script>',
            $this->getImagesPath(), $this->getCurrentRoute(), $this->getData('token'), $this->getEntryPoint(),
            $this->getPrefixedName('individual_tare'), $this->getPrefixedName('weight_based_limit'),
            $this->getPrefixedName('standard_packages'), $this->getPrefixedName('custom_packages'),
            $this->getPrefixedName('custom_envelopes'), $this->getPrefixedName('promo'),
            $this->getPrefixedName('processing_holidays'), $this->getPrefixedName('insurance'),
            $this->getPrefixedName('insurance_from'), $this->getPrefixedName('packer'),
            $this->getPrefixedName('label'), BaseLabelsProvider::LabelDisabled, $this->getPrefixedName('postcode'),
            $this->getPrefixedName('country_id'), $this->getPrefixedName('zone_id'),
            $this->getPrefixedName('postcode_dub'), $this->getPrefixedName('measurement_system'),
            $this->getCenterCssClass(), BaseModel::Packer3dPacker, BaseModel::PackerIndividual,
            BaseModel::PackerWeightBased, $this->getData('text_remove_package'), $this->getData('text_remove_promo'),
            $methodsDependOnShipperCountry, $this->getPrefixedName('label_format'), $this->getPrefixedName('tracking'),
            $this->getPrefixedName('city'), $this->getPrefixedName('address'), $this->getPrefixedName('sender_name'),
            $this->getPrefixedName('sender_company'), $this->getPrefixedName('sender_telephone'),
            $this->getPrefixedName('user_id'), $methodsDependOnUserId, BaseLabelsProvider::LabelFormatOriginal,
            $this->getPrefixedName('pdf_converter'), BaseLabelsProvider::LabelManually,
            BaseLabelsProvider::TrackingSendImmediately, $this->getData('license_cookie'),
            $this->getPrefixedName('adjustment_rules'), $this->getPrefixedName('weight_based_faked_box'),
            $this->getPrefixedName('billing_same'), $this->getPrefixedName('billing_country_id'),
            $this->getPrefixedName('billing_zone_id'), $this->getPrefixedName('billing_city'),
            $this->getPrefixedName('billing_postcode'), $this->getPrefixedName('billing_address'),
            $this->getData('token_name'), $this->getInitMethod());
        return implode("\n", $scripts);
    }

    /**
     * Returns view styles
     * @return string
     */
    protected function getStyles() {
        return sprintf('<link href="%s/%s.css?%s" rel="stylesheet">',
            $this->getStylesPath(), $this->getExtensionName(), $this->getData('version'));
    }

    abstract protected function renderBreadcrumbs();
    abstract protected function renderHeadingNotice();
    abstract protected function renderSubHeader();
    abstract protected function renderError($key);
    abstract public function renderContainer($content);
    abstract protected function renderTab(Controls\Tab $tab);
    abstract public function renderTabs($tabs = array());
    abstract public function renderSetting($titleKey, $helpKey, $errorKey, $isRequired, $content);
    abstract protected function renderDemo();
    abstract protected function renderLicense();

    public function renderStatic($content) {
        return sprintf('<p class="form-control-static">%s</p>', $content);
    }

    public function renderSelect(Controls\Select $select) {
        $value = $this->getValue($select->getNameKey());
        $result = sprintf('<select class="form-control %s" name="%s">',
            $select->getCssClass(), $this->getPrefixedName($select->getNameKey()));
        $source = $this->getData($select->getSourceKey());
        if (is_array($source)) {
            foreach ($source as $item) {
                $result .= sprintf('<option value="%s"%s>%s</option>',
                    $item[$select->getValueField()],
                    ((string)$value === (string)$item[$select->getValueField()] ? ' selected="selected"' : ''),
                    $item[$select->getCaptionField()]);
            }
        }
        $result .= '</select>';
        return $result;
    }

    public function renderOptions(Controls\Select $select) {
        $value = $this->getValue($select->getNameKey());
        $result = '<div class="btn-group" data-toggle="buttons">';
        $source = $this->getData($select->getSourceKey());
        if (is_array($source)) {
            foreach ($source as $item) {
                $isActive = (string)$value === (string)$item[$select->getValueField()];
                $result .= sprintf('<label class="btn btn-default%s">' .
                    '<input type="radio" name="%s" value="%s" autocomplete="off"%s> %s</label>',
                    $isActive ? ' active' : '',
                    $this->getPrefixedName($select->getNameKey()),
                    $item[$select->getValueField()],
                    $isActive ? ' checked' : '',
                    $item[$select->getCaptionField()]);
            }
        }
        $result .= '</div>';
        return $result;
    }

    abstract protected function isInputAddonRequiresSpacing();

    public function renderInput(Controls\Input $input) {
        $value = $this->getValue($input->getNameKey());
        if ($input->getPrerender()) {
            $value = call_user_func($input->getPrerender(), $value);
        }
        $addons = $input->getAddons();
        if ($addons['left'] || $addons['right']) {
            $result = sprintf('<div class="input-group %s">', $input->getCssClass()) .
                    ($addons['left'] ?
                        sprintf('<span class="input-group-addon">%s%s</span>',
                            $addons['left'], $this->isInputAddonRequiresSpacing() ? ' ' : ''
                        ) : ''
                    ) .
                    sprintf('<input class="form-control"%s name="%s" value="%s" />',
                        $input->getIsDisabled() ? ' disabled="disabled"' : '',
                        $this->getPrefixedName($input->getNameKey()),
                        $value) .
                    ($addons['right'] ?
                        sprintf('<span class="input-group-addon">%s%s</span>',
                            $this->isInputAddonRequiresSpacing() ? ' ' : '', $addons['right']
                        ) : ''
                    ) .
              '</div>';
        } else {
            $result = sprintf('<input class="form-control %s"%s name="%s" value="%s" />',
                $input->getCssClass(),
                $input->getIsDisabled() ? ' disabled="disabled"' : '',
                $this->getPrefixedName($input->getNameKey()),
                $value);
        }
        return $result;
    }

    public function renderHidden(Controls\Input $input) {
        $value = $this->getValue($input->getNameKey());
        if ($input->getPrerender()) {
            $value = call_user_func($input->getPrerender(), $value);
        }
        return sprintf('<input name="%s" type="hidden" value="%s" />',
            $this->getPrefixedName($input->getNameKey()), $value);
    }

    public function renderCheckbox(Controls\Input $input) {
        if ($input->getIsChecked() === null) {
            $input->setIsChecked($this->getValue($input->getNameKey()));
        }
        return sprintf('<div class="checkbox"><label><input name="%s" type="hidden" value="0" />' .
            '<input name="%s"%s type="checkbox" value="1"%s />%s</label></div>',
            $this->getPrefixedName($input->getNameKey()),
            $this->getPrefixedName($input->getNameKey()),
            $input->getIsDisabled() ? ' disabled="disabled"' : '',
            $input->getIsChecked() ? ' checked="checked"' : '',
            $input->getCaption());
    }

    public function renderTextarea(Controls\Input $input) {
        $value = $this->getValue($input->getNameKey());
        if ($input->getPrerender()) {
            $value = call_user_func($input->getPrerender(), $value);
        }
        return sprintf('<textarea class="form-control %s" name="%s">%s</textarea>',
            $input->getCssClass(),
            $this->getPrefixedName($input->getNameKey()), $value);
    }

    public function renderMeasurementSystem() {
        $result = $this->renderSetting(
            'entry_measurement_system', 'help_measurement_system', 'error_measurement_system', true,
            $this->renderSelect(Controls\Select::create()
                ->setNameKey('measurement_system')
                ->setSourceKey('measurement_systems')
                ->setCaptionField('text')
                ->setValueField('value')
            ) .
            $this->renderStatic('System currency is <strong>' . $this->getData('system_currency') .
                '</strong>, hereinafter is marked with <strong>' . $this->getData('currency_unit') . '</strong>')
        );
        return $result;
    }

    public function renderStandardPackages() {
        $scripts = array();
        $enabledBoxes = $this->getValue('standard_packages');
        foreach ($this->data['boxes'] as $box) {
            /** @var OriginPackage $box */
            $boxEnabled = isset($enabledBoxes[$box->getId()]) ? $enabledBoxes[$box->getId()] : false;
            $scripts[] = sprintf('objSmartFlexible.addStandardBox(%d, "%s", "%s", "%s", "%s", %d);',
                $box->getId(), $box->getTitle(), $box->getImage(), $box->getSizeDescription($this->getLocale()),
                $box->getWeightDescription($this->getLocale()), (int)$boxEnabled);
        }
        $result = sprintf('<div id="standard-boxes-checked"><h4>%s</h4></div>' .
            '<div id="standard-boxes-unchecked"><h4>%s</h4></div>' .
            '<script>$(function() {%s});</script>',
            $this->getData('entry_boxes_checked'),
            $this->getData('entry_boxes_unchecked'),
            implode("\n", $scripts));
        return $result;
    }

    public function prerenderLargeWeightWithEmpty($v) {
        if ($v) {
            return $this->getLocale()->renderLargeWeight($v);
        }
        return '';
    }

    public function prerenderLengthWithEmpty($v) {
        if ($v) {
            return $this->getLocale()->renderLength($v);
        }
        return '';
    }

    public function renderWeightBasedFakedBox() {
        return sprintf('<div class="weight-based-faked-box__holder">%s%s%s</div>',
            $this->renderInput(
                Controls\Input::create()
                    ->setNameKey('weight_based_faked_box[length]')
                    ->setCssClass('weight-based-faked-box__dimension')
                    ->setAddons(null, '&#215;')
                    ->setPrerender(Array($this, 'prerenderLengthWithEmpty'))),
            $this->renderInput(
                Controls\Input::create()
                    ->setNameKey('weight_based_faked_box[width]')
                    ->setCssClass('weight-based-faked-box__dimension')
                    ->setAddons(null, '&#215;')
                    ->setPrerender(Array($this, 'prerenderLengthWithEmpty'))),
            $this->renderInput(
                Controls\Input::create()
                    ->setNameKey('weight_based_faked_box[height]')
                    ->setCssClass('weight-based-faked-box__dimension')
                    ->setAddons(null, $this->getLocale()->renderLengthUnit())
                    ->setPrerender(Array($this, 'prerenderLengthWithEmpty')))
            );
    }

    public function renderCustomPackagesFixed() {
        $scripts = array();
        foreach ($this->getValue('custom_packages') as $package) {
            $scripts[] = sprintf('objSmartFlexible.addCustomPackage("%s", "%s", "%s", "%s", "%s");',
                isset($package['length']) ? $this->getLocale()->renderLength($package['length']) : '',
                isset($package['width']) ? $this->getLocale()->renderLength($package['width']) : '',
                isset($package['height']) ? $this->getLocale()->renderLength($package['height']) : '',
                isset($package['max_load']) ? $this->getLocale()->renderLargeWeight($package['max_load']) : '',
                isset($package['tare']) ? $this->getLocale()->renderSmallWeight($package['tare']) : '');
        }
        $result = sprintf('<p><a href="#" ' .
                'onclick=\'objSmartFlexible.addCustomPackage("", "", "", "", ""); return false;\'>%s</a></p>',
                $this->getData('text_add_package')) .
            sprintf('<table class="%s" style="width: initial;"><thead><tr>' .
                str_repeat('<td class="%s">%s, <em>%s</em></td>', 5) .
                '<td>&nbsp;</td></tr></thead><tbody id="custom_packages_holder"></tbody></table>' .
                '<script>$(function() {%s});</script>',
                $this->getTableCssClass(),
                $this->getCenterCssClass(),
                $this->getData('entry_length'),
                $this->getLocale()->renderLengthUnit(),
                $this->getCenterCssClass(),
                $this->getData('entry_width'),
                $this->getLocale()->renderLengthUnit(),
                $this->getCenterCssClass(),
                $this->getData('entry_height'),
                $this->getLocale()->renderLengthUnit(),
                $this->getCenterCssClass(),
                $this->getData('entry_max_load'),
                $this->getLocale()->renderLargeWeightUnit(),
                $this->getCenterCssClass(),
                $this->getData('entry_tare'),
                $this->getData('custom_package_fixed_unit') === Weight::UnitTypeSmall ?
                    $this->getLocale()->renderSmallWeightUnit() : $this->getLocale()->renderLargeWeightUnit(),
                implode("\n", $scripts));
        return $result;
    }

    public function renderCustomPackagesExpandable() {
        $scripts = array();
        foreach ($this->getValue('custom_envelopes') as $envelope) {
            $scripts[] = sprintf('objSmartFlexible.addCustomEnvelope("%s", "%s", "%s", "%s", "%s");',
                isset($envelope['length']) ? $this->getLocale()->renderLength($envelope['length']) : '',
                isset($envelope['width']) ? $this->getLocale()->renderLength($envelope['width']) : '',
                isset($envelope['max_height']) ? $this->getLocale()->renderLength($envelope['max_height']) : '',
                isset($envelope['max_load']) ? $this->getLocale()->renderLargeWeight($envelope['max_load']) : '',
                isset($envelope['tare']) ? $this->getLocale()->renderSmallWeight($envelope['tare']) : '');
        }
        $result = sprintf('<p><a href="#" ' .
                'onclick=\'objSmartFlexible.addCustomEnvelope("", "", "", "", ""); return false;\'>%s</a></p>',
                $this->getData('text_add_envelope')) .
            sprintf('<table class="%s" style="width: initial;"><thead><tr>' .
                str_repeat('<td class="%s">%s, <em>%s</em></td>', 5) .
                '<td>&nbsp;</td></tr></thead><tbody id="custom_envelopes_holder"></tbody></table>' .
                '<script>$(function() {%s});</script>',
                $this->getTableCssClass(),
                $this->getCenterCssClass(),
                $this->getData('entry_length'),
                $this->getLocale()->renderLengthUnit(),
                $this->getCenterCssClass(),
                $this->getData('entry_width'),
                $this->getLocale()->renderLengthUnit(),
                $this->getCenterCssClass(),
                $this->getData('entry_max_height'),
                $this->getLocale()->renderLengthUnit(),
                $this->getCenterCssClass(),
                $this->getData('entry_max_load'),
                $this->getLocale()->renderLargeWeightUnit(),
                $this->getCenterCssClass(),
                $this->getData('entry_tare'),
                $this->getData('custom_package_expandable_unit') === Weight::UnitTypeSmall ?
                    $this->getLocale()->renderSmallWeightUnit() : $this->getLocale()->renderLargeWeightUnit(),
                implode("\n", $scripts));

        return $result;
    }

    public function renderPromo() {
        $scripts = array();
        foreach ($this->getValue('promo') as $promo) {
            $scripts[] = sprintf('objSmartFlexible.addPromo("%s", "%s", "%s", "%s", "%s");',
                isset($promo['min_cost']) ? $promo['min_cost'] : '',
                isset($promo['length']) ? $this->getLocale()->renderLength($promo['length']) : '',
                isset($promo['width']) ? $this->getLocale()->renderLength($promo['width']) : '',
                isset($promo['height']) ? $this->getLocale()->renderLength($promo['height']) : '',
                isset($promo['weight']) ? $this->getLocale()->renderLargeWeight($promo['weight']) : '');
        }
        $result = sprintf('<p><a href="#" ' .
                'onclick=\'objSmartFlexible.addPromo("", "", "", "", ""); return false;\'>%s</a></p>',
                $this->getData('text_add_promo')) .
            sprintf('<table class="%s" style="width: initial;"><thead><tr>' .
                str_repeat('<td class="%s">%s, <em>%s</em></td>', 5) .
                '<td>&nbsp;</td></tr></thead><tbody id="promo_holder"></tbody></table>' .
                '<script>$(function() {%s});</script>',
                $this->getTableCssClass(),
                $this->getCenterCssClass(),
                $this->getData('entry_min_cost'),
                $this->getData('currency_unit'),
                $this->getCenterCssClass(),
                $this->getData('entry_length'),
                $this->getLocale()->renderLengthUnit(),
                $this->getCenterCssClass(),
                $this->getData('entry_width'),
                $this->getLocale()->renderLengthUnit(),
                $this->getCenterCssClass(),
                $this->getData('entry_height'),
                $this->getLocale()->renderLengthUnit(),
                $this->getCenterCssClass(),
                $this->getData('entry_weight'),
                $this->getLocale()->renderLargeWeightUnit(),
                implode("\n", $scripts));
        return $result;
    }

    public function renderAdjustmentRules() {
        $scripts = array();
        if (is_array($this->getValue('adjustment_rules'))) {
            foreach ($this->getValue('adjustment_rules') as $rule) {
                $scripts[] = sprintf('objSmartFlexible.addAdjustmentRule(' . json_encode($rule) . ');');
            }
        }
        $result = sprintf('<a href="#" class="btn btn-primary adjustment-rule__add" ' .
            'onclick="objSmartFlexible.addAdjustmentRule({}); return false">%s</a><script>$(function() {%s});</script>',
            $this->getData('text_add_adjustment_rule'), implode("\n", $scripts));
        return $result;
    }

    public function renderProcessing() {
        $result = $this->renderInput(Controls\Input::create()
            ->setNameKey('processing_days')
            ->setCssClass('small-number'));
        $result .= '<div>&nbsp;</div>';
        $result .= $this->renderCheckbox(Controls\Input::create()
            ->setNameKey('processing_saturdays')
            ->setCaption($this->getData('entry_processing_saturdays')));
        $result .= $this->renderCheckbox(Controls\Input::create()
            ->setNameKey('processing_sundays')
            ->setCaption($this->getData('entry_processing_sundays')));
        return $result;
    }

    public function renderHolidays() {
        $googleHolidays = $this->renderSelect(Controls\Select::create()
            ->setNameKey('google_import')
            ->setSourceKey('google_holidays')
            ->setCssClass('google-holidays')
            ->setCaptionField('text')
            ->setValueField('value'));
        $result = sprintf('<p>%s &nbsp; <a href="#" onclick="objSmartFlexible.importHolidays(); return false;">' .
            'Import</a> &middot; <a href="#" onclick="objSmartFlexible.clearHolidays(); return false">Clear</a>' .
            '</p><div id="holidays__holder">',
            $googleHolidays);
        for ($m = 1; $m <= 12; $m++) {
            $month = date('F', mktime(0, 0, 0, $m, 1));
            $result .= sprintf('<div id="holidays_%s" class="holidays__month"><div>%s</div>' .
                '<div class="holidays__list"><div class="holidays_add">%s</div></div></div>',
                $m, $month, $this->getData('text_add_holiday'));
        }
        $scripts = array();
        foreach ($this->getValue('processing_holidays') as $holiday) {
            $scripts[] = sprintf('objSmartFlexible.addHoliday("%s", "%s");', $holiday[0], $holiday[1]);
        }
        $result .= sprintf('</div><script>%s</script>', implode("\n", $scripts));

        return $result;
    }

    public function renderAvoidDelivery() {
        $result = $this->renderCheckbox(Controls\Input::create()
            ->setNameKey('avoid_delivery_saturdays')
            ->setCaption($this->getData('entry_avoid_delivery_saturdays')));
        $result .= $this->renderCheckbox(Controls\Input::create()
            ->setNameKey('avoid_delivery_sundays')
            ->setCaption($this->getData('entry_avoid_delivery_sundays')));
        return $result;
    }

    protected function getTime() {
        return time();
    }

    public function renderCutoff() {
        $result = $this->renderSelect(Controls\Select::create()
            ->setNameKey('cutoff')
            ->setSourceKey('hours'));
        $result .= sprintf('<div class="%s">Server time: %s</div><div class="%s">%s</div>',
            $this->getHelpCssClass(),
            date('H:i', $this->getTime()),
            $this->getHelpCssClass(),
            $this->getData('entry_cutoff_help'));
        return $result;
    }

    public function renderInsurance() {
        $result = '<div class="insurance__holder">';
        $result .= $this->renderOptions(Controls\Select::create()
            ->setNameKey('insurance')
            ->setSourceKey('booleans'));
        $result .= $this->renderInput(Controls\Input::create()
            ->setNameKey('insurance_from')
            ->setCssClass('insurance__from')
            ->setAddons($this->getData('entry_insurance_from'), $this->getData('currency_unit')));
        $result .= '</div>' . sprintf('<div class="%s" id="insurance_from_help">%s</div>',
            $this->getHelpCssClass(), $this->getData('help_insurance_from'));
        return $result;
    }

    public function renderGeoZones() {
        $result = $this->renderCheckbox(Controls\Input::create()
            ->setNameKey('all_geo_zones')
            ->setCaption($this->getData('text_all_geo_zones')));
        $result .= '<div>&nbsp;</div>';
        $geoZones = $this->getValue('geo_zones');
        foreach ($this->getData('geo_zones') as $geoZone) {
            $zoneEnabled = false;
            if (isset($geoZones[$geoZone['geo_zone_id']])) {
                $zoneEnabled = $geoZones[$geoZone['geo_zone_id']];
            }
            $result .= $this->renderCheckbox(Controls\Input::create()
                ->setNameKey('geo_zones[' . $geoZone['geo_zone_id'] . ']')
                ->setCaption($geoZone['name'])
                ->setIsChecked($zoneEnabled));
        }
        return $result;
    }

    public function renderCustomerGroups() {
        $result = $this->renderCheckbox(Controls\Input::create()
            ->setNameKey('all_customer_groups')
            ->setCaption($this->getData('text_all_customer_groups')));
        $result .= '<div>&nbsp;</div>';
        $customerGroups = $this->getValue('customer_groups');
        foreach ($this->getData('customer_groups') as $customerGroup) {
            $groupEnabled = false;
            if (isset($customerGroups[$customerGroup['customer_group_id']])) {
                $groupEnabled = $customerGroups[$customerGroup['customer_group_id']];
            }
            $result .= $this->renderCheckbox(Controls\Input::create()
                ->setNameKey('customer_groups[' . $customerGroup['customer_group_id']. ']')
                ->setCaption($customerGroup['name'])
                ->setIsChecked($groupEnabled));
        }
        return $result;
    }

    public function renderStores() {
        $result = $this->renderCheckbox(Controls\Input::create()
            ->setNameKey('all_stores')
            ->setCaption($this->getData('text_all_stores')));
        $result .= '<div>&nbsp;</div>';
        $stores = $this->getValue('stores');
        foreach ($this->getData('stores') as $store) {
            $storeEnabled = false;
            if (isset($stores[$store['store_id']])) {
                $storeEnabled = $stores[$store['store_id']];
            }
            $result .= $this->renderCheckbox(Controls\Input::create()
                ->setNameKey('stores[' . $store['store_id'] . ']')
                ->setCaption($store['name'])
                ->setIsChecked($storeEnabled));
        }
        return $result;
    }

    private function renderGlobalServiceButtons($isWithRefresh = false) {
        $refresh = sprintf(' / <a href="#" onclick="objSmartFlexible.fetchServicesByUser(); return false;">%s</a>',
            $this->getData('text_refresh'));
        return sprintf('<div><a href="#" onclick=\'$(this).parents("td").find(":checkbox").prop("checked", true); ' .
            'return false;\'>%s</a> / ' .
            '<a href="#" onclick=\'$(this).parents("td").find(":checkbox").prop("checked", false); ' .
            'return false;\'>%s</a>%s</div>',
            $this->getData('text_select_all'), $this->getData('text_unselect_all'), $isWithRefresh ? $refresh : '');
    }

    public function renderServices() {
        $result = sprintf('<table class="%s services"><tr>', $this->getTableCssClass(true));
        if ($this->hasFeature(Features::ShipperAddress) && $this->hasFeature(Features::MethodsDependOnShipperCountry)) {
            $methodCodes = $this->getMethodCodesCustom();
        } else {
            $methodCodes = $this->data['method_codes'];
        }
        $methods = $this->getValue('methods');
        foreach (array_keys($methodCodes) as $service) {
            $checkboxes = array();
            foreach ($methodCodes[$service] as $group => $codes) {
                $checkboxes[] = sprintf('<div class="services__group">%s</div>', $this->getData('text_' . $group));
                foreach ($codes as $code) {
                    $fullKey = $service . '_' . $code;
                    $methodEnabled = false;
                    if (isset($methods[$fullKey])) {
                        $methodEnabled = $methods[$fullKey];
                    }
                    $checkboxes[] = $this->renderCheckbox(Controls\Input::create()
                        ->setNameKey('methods[' . $fullKey . ']')
                        ->setCaption($this->getData('text_' . $fullKey))
                        ->setIsChecked($methodEnabled));
                }
            }
            $result .= sprintf('<td><div class="services__service">%s</div>%s%s</td>',
                $this->getData('text_' . $service),
                $this->renderGlobalServiceButtons(),
                implode("\n", $checkboxes));
        }
        $result .= '</tr></table>';
        return $result;
    }

    public function renderServicesByUser() {
        $result = '';
        $methods = $this->getValue('methods');
        foreach ($this->getMethodCodesCustom() as $group => $codes) {
            $result .= sprintf('<div class="services__group">%s</div>', $group);
            foreach ($codes as $code => $title) {
                $methodEnabled = false;
                if (isset($methods[$code])) {
                    $methodEnabled = $methods[$code];
                }
                $result .= $this->renderCheckbox(Controls\Input::create()
                    ->setNameKey('methods[' . $code . ']')
                    ->setCaption($title)
                    ->setIsChecked($methodEnabled));
            }
        }
        return sprintf('<table class="%s services"><tr><td>' .
            '<div class="services__service">%s</div>%s%s</td></tr></table>',
            $this->getTableCssClass(true), $this->getData('text_services_by_user'),
            $this->renderGlobalServiceButtons(true), $result);
    }

    public function renderWebhook() {
        return $this->renderStatic(sprintf('<input class="form-control" disabled="disabled" value="%s">',
            $this->getData('webhook')));
    }

    public function renderLabelFormatAndConverter() {
        $result = '';
        if ($this->hasFeature(Features::ShippingLabel4x6Inches)) {
            $result .= $this->renderSetting('entry_label_format', 'help_label_format', '', false,
                $this->renderSelect(Controls\Select::create()
                    ->setNameKey('label_format')
                    ->setSourceKey('label_formats')));
            if ($this->getData('label_format_pdf')) {
                $result .= $this->renderSetting('entry_pdf_converter', 'help_pdf_converter', '',
                    false, $this->renderSelect(Controls\Select::create()
                        ->setNameKey('pdf_converter')
                        ->setSourceKey('pdf_converters')));
            }
        }
        return $result;
    }

    public function renderContact() {
        return $this->renderStatic(sprintf('<a href="mailto:%s">%s</a> &middot; ' .
            '<a href="' . Author::BugReportUrl . '">%s</a>',
            $this->getData('contact_email'),
            $this->getData('contact_email'),
            $this->getExtensionName(),
            $this->getData('version'),
            'OpenCart ' . VersionChecker::get()->getVersion(),
            $this->getData('text_bugreport')));
    }

    public function renderMaintenance() {
        return $this->renderStatic(sprintf('<a href="%s">%s</a> &middot; <a id="import-settings">%s</a>',
            $this->getData('export_settings_link'),
            $this->getData('text_export_settings'),
            $this->getData('text_import_settings')));
    }

    public function renderForm($content) {
        return sprintf('<form id="upload" action="%s" method="POST" enctype="multipart/form-data">
                <input name="source" type="file" style="display: none">
                <div id="upload__cancel">&times;</div>
                <div id="upload__title">%s</div>
                <div class="upload__notice">
                    Are you sure you want to replace your settings with ones from this file?
                </div>
                <div class="upload__notice text-danger">
                    Warning: This operation may cause malfunction of the extension. This operation can not be undone.
                </div>
                <div id="upload__submit">
                    <input type="submit" value="Upload">
                </div>
            </form>
            <form action="%s" method="post" enctype="multipart/form-data" id="smart_flexible" class="form-horizontal">
            %s</form>',
            $this->getData('import_settings_link'),
            $this->getData('text_import_settings'),
            $this->getData('action'),
            $content);
    }

    abstract protected function getContentGlobalCssClass();

    public function finallyRenderModuleSettings($content) {
        $cssClasses = array($this->getContentGlobalCssClass());
        return sprintf('%s<div id="content" class="%s">%s</div>%s',
            $this->getHeader(),
            implode(' ', $cssClasses),
            $this->renderSubHeader() . $content,
            $this->getFooter());
    }

    public function finallyRenderStandalonePage($content) {
        return sprintf('<html><head>%s</head><body>%s</body></html>',
            $this->getStyles() . $this->getjQuery() . $this->getScripts(), $content);
    }

    public function finallyRenderPackagingMap($data) {
        return <<<HTML
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Packing Map Visualisation</title>
		<meta charset="utf-8">
		<meta name="generator" content="Three.js Editor">
		<meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
		{$this->getThreeJs()}{$this->getjQuery()}{$this->getScripts()}{$this->getStyles()}
	</head>
	<body ontouchstart="" class="packaging-map">
	    <div id="description"></div>
		<script>
            var app = objSmartFlexible.getPackagingMapApplication();
            var player = new app.Player();
            if (player.load({$data['scene']})) {
                player.setSize(window.innerWidth, window.innerHeight);
                player.play();
                $('body').append(player.dom);
                $(window).on('resize', function() {
                    player.setSize(window.innerWidth, window.innerHeight);
                });
            }
        </script>
	</body>
</html>
HTML;
    }

    public function renderPackagingList($data) {
        $_this = $this;
        $export = '';
        $method = '';
        $allLabelsLink = '';
        $content = '';
        if ($data['export']) {
            $export = sprintf('<div class="packaging-list__export">%s</div>',
                implode("\n", array_map(function($export) use ($_this) {
                    return sprintf('<li><img src="%s/share.svg"><a href="%s">%s</a></li>',
                        $_this->getImagesPath(), $export['link'], $export['title']);
                }, $data['export'])));
        }
        if ($data['method_name'] && $data['service_name']) {
            $method .= sprintf('<h2 class="packaging-list__method">Shipping method: %s (%s)</h2>',
                $data['method_name'], $data['service_name']);
        }
        if ($data['all_labels_link']) {
            $allLabelsLink = sprintf('<button class="packaging-list__all-labels" ' .
                'onclick="objSmartFlexible.requestLabels(\'%s\', this);">Request all labels</button>',
                $data['all_labels_link']);
        }
        foreach ($data['list'] as $box) {
            /** @var Shipment $shipment */
            $shipment = $box['shipment'];
            /** @var Locale $locale */
            $locale = $box['locale'];
            $dimensions = $shipment->getBox()->getOriginPackage()->getSizeDescription($box['locale']);
            $image = $shipment->getBox()->getOriginPackage()->getImage();
            $mapButton = $box['map_link'] ? sprintf(
                '<button class="packaging-list__box-map" onclick="window.location = \'%s\';">Packing Map</button>',
                $box['map_link']) : '';
            $tracking = '';
            $error = '';
            $buttons = '';
            if ($shipment->getTrackingNumber()) {
                $tracking = sprintf('<input readonly="readonly" class="packaging-list__box-tracking" value="%s" />',
                    $shipment->getTrackingNumber());
            }
            $products = implode("\n", array_map(function($item, $itemId) use ($box, $locale) {
                /** @var OrderedBoxPackedItem $item */
                return sprintf('<li>%s &#215; %s <span>%s</span>%s</li>',
                    $item->getQuantity(),
                    $box['product_links'][$itemId] ?
                        sprintf('<a href="%s">%s</a>',
                            $box['product_links'][$itemId],
                            $item->getDescription())
                        : $item->getDescription(),
                    $locale->renderLargeWeight($item->getWeight(), true),
                    implode("\n", array_map(function($option) {
                        return sprintf('<br /><span>%s</span>', $option);
                    }, $item->getOptions()))
                );
            }, $shipment->getBox()->getProducts(), array_keys($shipment->getBox()->getProducts())));
            if ($shipment->getErrorMessage()) {
                $error = '<pre>' . $shipment->getErrorMessage() . '</pre>';
            }
            if ($box['request_link'] || $box['void_link'] || $box['documents']) {
                $requestButton = '';
                $documentButtons = '';
                $voidButton = '';
                if ($box['request_link']) {
                    $requestButton = sprintf('<button class="packaging-list__box-document" onclick="%s">%s</button>',
                        sprintf('objSmartFlexible.requestLabels(\'%s\', this);', $box['request_link']),
                        'Request Label');
                }
                if ($box['documents']) {
                    foreach ($box['documents'] as $document) {
                        if (count($document['links']) === 1) {
                            $documentButtons .= sprintf(
                                '<button class="packaging-list__box-document" onclick="%s">%s</button>',
                                sprintf('window.location = \'%s\';', $document['links'][0]),
                                $document['title']
                            );
                        } else {
                            $documentButtons .= '<div class="packaging-list__box-document-title">' .
                                '<div>' . $document['title'] . ' Pages</div>';
                            foreach ($document['links'] as $k => $link) {
                                $documentButtons .= sprintf(
                                    '<button class="packaging-list__box-document _page" onclick="%s">%s</button>',
                                    sprintf('window.location = \'%s\';', $link),
                                    $k + 1
                                );
                            }
                            $documentButtons .= '</div>';
                        }
                    }
                }
                if ($box['void_link']) {
                    $voidButton = sprintf(
                        '<button class="packaging-list__box-void" onclick="window.location = \'%s\';">' .
                        'Void Label</button>', $box['void_link']);
                }
                $buttons = sprintf('<div class="packaging-list__box-controls-holder">%s%s%s%s</div>',
                    $requestButton, $documentButtons, $voidButton, $mapButton);
            }
            $content .= sprintf('<div class="packaging-list__box">' .
                '<div class="packaging-list__box-image-holder">%s</div>' .
                '<div class="packaging-list__info-holder"><h3 class="packaging-list__box-heading">' .
                '<span class="packaging-list__box-title">%s</span>&#32;' .
                '<span class="packaging-list__box-dimensions-and-weight">%s%s</span></h3>' .
                '%s<ul class="packaging-list__box-content">%s</ul>%s</div>%s</div>',
                $image ? sprintf('<img src="%s/%s">', $this->getImagesPath(), $image) : '',
                $shipment->getBox()->getOriginPackage()->getTitle(),
                $dimensions ? $dimensions . ', ' : '',
                $locale->renderLargeWeight($shipment->getBox()->getWeight(), true),
                $tracking, $products, $error, $buttons);
        }
        return sprintf('<div class="packaging-list__wrapper">%s' .
            '<h1 class="packaging-list__title">Packing List for Order &num;%s</h1>%s%s%s</div>',
            $export, $data['order_id'], $method, $allLabelsLink, $content);
    }

    public function renderDocumentWrapper($data) {
        $save = sprintf('<button onclick="window.location = \'%s\'">Save</button>', $data['save_link']);
        if ($data['document_format'] === BaseLabelsProvider::DocumentFormatPDF) {
            return sprintf('<div class="document-wrapper"><div class="document-wrapper__controls">' .
                '<button onclick="%s">Print</button>%s</div>' .
                '<iframe id="document" class="document-wrapper__document" src="%s"></iframe></div>',
                'objSmartFlexible.printFrame(\'document\', \'' . $data['document_link'] . '\');',
                $save, $data['document_link']);
        } else {
            return sprintf('<div class="document-wrapper__controls _absolute">' .
                '<button onclick="%s">Print</button>%s</div>' .
                '<div id="document" class="document-wrapper__document _absolute"><img src="%s"></div>' .
                '<script>$(function(){objSmartFlexible.fixDocumentHeight();});</script>',
                'window.print();', $save, $data['document_link']);
        }
    }
}
}

namespace CanadapostSmartFlexibleRootNS\SmartFlexible {

class VersionChecker {
    /** @var VersionChecker */
    private static $instance;
    /** @var string */
    private $version;
    /** @var int[] */
    private $supportedMajorVersions = array(1, 2, 3);
    /** @var int */
    private $major;
    /** @var int */
    private $minor;
    /** @var int */
    private $patch;

    /**
     * @throws \Exception
     */
    private function __construct() {
        $this->version = VERSION;
        list($this->major, $this->minor, $this->patch) = array_map('intval', explode('.', $this->version));
        if (!in_array($this->major, $this->supportedMajorVersions, true)) {
            throw new \Exception('Version ' . $this->version . ' is not supported');
        }
    }

    /**
     * @return VersionChecker
     */
    public static function get() {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @return string
     */
    public function getVersion() {
        return $this->version;
    }

    /**
     * Returns version major bit
     * @return int
     */
    private function getMajor() {
        return $this->major;
    }

    /**
     * Returns version minor bit
     * @return int
     */
    private function getMinor() {
        return $this->minor;
    }

    /**
     * Checks that Version 1 running
     * @return bool
     */
    public function isVersion1() {
        return $this->getMajor() === 1;
    }

    /**
     * Checks that Version 2 running
     * @return bool
     */
    public function isVersion2() {
        return $this->getMajor() === 2;
    }

    /**
     * Checks that Version 2.3 running
     * @return bool
     */
    public function isVersion23() {
        return $this->getMajor() === 2 && $this->getMinor() === 3;
    }

    /**
     * Checks that Version 3 running
     * @return bool
     */
    public function isVersion3() {
        return $this->getMajor() === 3;
    }
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Controls {

class Select {
    private $nameKey;
    private $sourceKey;
    private $valueField = 'value';
    private $captionField = 'text';
    private $cssClass;

    /**
     * @return Select
     */
    public static function create() {
        return new self();
    }

    private function __construct() {
    }

    /**
     * @return mixed
     */
    public function getCaptionField() {
        return $this->captionField;
    }

    /**
     * @param mixed $captionField
     * @return $this
     */
    public function setCaptionField($captionField) {
        $this->captionField = $captionField;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSourceKey() {
        return $this->sourceKey;
    }

    /**
     * @param mixed $sourceKey
     * @return $this
     */
    public function setSourceKey($sourceKey) {
        $this->sourceKey = $sourceKey;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNameKey() {
        return $this->nameKey;
    }

    /**
     * @param mixed $nameKey
     * @return $this
     */
    public function setNameKey($nameKey) {
        $this->nameKey = $nameKey;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValueField() {
        return $this->valueField;
    }

    /**
     * @param mixed $valueField
     * @return $this
     */
    public function setValueField($valueField) {
        $this->valueField = $valueField;
        return $this;
    }

    /**
     * @return string
     */
    public function getCssClass() {
        return $this->cssClass;
    }

    /**
     * @param string $cssClass
     * @return $this
     */
    public function setCssClass($cssClass) {
        $this->cssClass = $cssClass;
        return $this;
    }
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Controls {

class Input {
    private $nameKey;
    private $isChecked = null;
    private $isDisabled = null;
    private $caption;
    private $cssClass;
    private $addons = array('left' => null, 'right' => null);
    /** @var callable */
    private $prerender;

    /**
     * @return Input
     */
    public static function create() {
        return new self();
    }

    private function __construct() {
    }

    /**
     * @return bool|null
     */
    public function getIsChecked() {
        return $this->isChecked;
    }

    /**
     * @param bool $isChecked
     * @return $this
     */
    public function setIsChecked($isChecked) {
        $this->isChecked = (bool)$isChecked;
        return $this;
    }

    /**
     * @return string
     */
    public function getNameKey() {
        return $this->nameKey;
    }

    /**
     * @param string $nameKey
     * @return $this
     */
    public function setNameKey($nameKey) {
        $this->nameKey = $nameKey;
        return $this;
    }

    /**
     * @return string
     */
    public function getCaption() {
        return $this->caption;
    }

    /**
     * @param string $caption
     * @return $this
     */
    public function setCaption($caption) {
        $this->caption = $caption;
        return $this;
    }

    /**
     * @return string
     */
    public function getCssClass() {
        return $this->cssClass;
    }

    /**
     * @param string $cssClass
     * @return $this
     */
    public function setCssClass($cssClass) {
        $this->cssClass = $cssClass;
        return $this;
    }

    /**
     * @return array
     */
    public function getAddons() {
        return $this->addons;
    }

    /**
     * @param string $left
     * @param string $right
     * @return $this
     */
    public function setAddons($left, $right) {
        $this->addons = array('left' => $left, 'right' => $right);
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsDisabled() {
        return $this->isDisabled;
    }

    /**
     * @param bool $isDisabled
     * @return $this
     */
    public function setIsDisabled($isDisabled) {
        $this->isDisabled = (bool)$isDisabled;
        return $this;
    }

    /**
     * @return callable
     */
    public function getPrerender() {
        return $this->prerender;
    }

    /**
     * @param callable $prerender
     * @return $this
     */
    public function setPrerender($prerender) {
        $this->prerender = $prerender;
        return $this;
    }


}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Controls {

class Tab {
    private $id;
    private $isDefault;
    private $content;
    private $titleKey;

    /**
     * @return Tab
     */
    public static function create() {
        return new self();
    }

    private function __construct() {
    }

    /**
     * @return mixed
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * @param mixed $content
     * @return $this
     */
    public function setContent($content) {
        $this->content = $content;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return $this
     */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsDefault() {
        return $this->isDefault;
    }

    /**
     * @param mixed $isDefault
     * @return $this
     */
    public function setIsDefault($isDefault) {
        $this->isDefault = $isDefault;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitleKey() {
        return $this->titleKey;
    }

    /**
     * @param mixed $titleKey
     * @return $this
     */
    public function setTitleKey($titleKey) {
        $this->titleKey = $titleKey;
        return $this;
    }
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping\BaseModel;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Length;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Weight;

class Locale {
    /** @var bool */
    private $isMetric;

    /**
     * @param string $measurementSystem
     */
    public function __construct($measurementSystem) {
        $this->isMetric = $measurementSystem === BaseModel::MetricMeasures;
    }

    /**
     * Returns inches length value for view in inches or centimeters
     * @param float $in
     * @param bool $isWithUnit
     * @return string Inches or centimeters
     */
    public function renderLength($in, $isWithUnit = false) {
        if ($this->isMetric) {
            return round(Length::convertInchesToCentimeters($in), 2) .
                ($isWithUnit ? ' ' . $this->renderLengthUnitForNumber() : '');
        }
        return round($in, 2) . ($isWithUnit ? $this->renderLengthUnitForNumber() : '');
    }

    /**
     * Returns inches length dimensions for view in inches or centimeters
     * @param float[] $dimensions
     * @return string
     */
    public function renderDimensions($dimensions) {
        $_this = $this;
        if (!array_filter($dimensions)) {
            return '';
        }
        $rounded = array_map(function($dimension) use ($_this) {
            return $_this->renderLength($dimension);
        }, $dimensions);
        return implode(' &#215; ', $rounded) . ($this->isMetric ? ' ' : '') . $this->renderLengthUnitForNumber();
    }

    /**
     * Import length value (inches or centimeters) into inches
     * @param float $value
     * @return float
     */
    public function acceptLength($value) {
        $value = (float)$value;
        if ($this->isMetric) {
            return Length::convertCentimetersToInches($value);
        }
        return $value;
    }

    /**
     * Returns length unit string (inches or centimeters)
     * @return string
     */
    public function renderLengthUnitForNumber() {
        if ($this->isMetric) {
            return Length::UnitCentimeter;
        }
        return Length::UnitInchShort;
    }

    /**
     * Returns length unit string (inches or centimeters)
     * @return string
     */
    public function renderLengthUnit() {
        if ($this->isMetric) {
            return Length::UnitCentimeter;
        }
        return Length::UnitInchLong;
    }

    /**
     * Returns ounces value for view in ounces or grams
     * @param float $oz
     * @param bool $isWithUnit
     * @return string Ounces or grams
     */
    public function renderSmallWeight($oz, $isWithUnit = false) {
        if ($this->isMetric) {
            return round(Weight::convertOuncesToGrams($oz)) .
                ($isWithUnit ? ' ' . $this->renderSmallWeightUnit() : '');
        }
        return round($oz, 2) . ($isWithUnit ? ' ' . $this->renderSmallWeightUnit() : '');
    }

    /**
     * Import small weight value (ounces or grams) into ounces
     * @param float $value
     * @return float
     */
    public function acceptSmallWeight($value) {
        $value = (float)$value;
        if ($this->isMetric) {
            return Weight::convertGramsToOunces($value);
        }
        return $value;
    }

    /**
     * Returns small weight unit value string (ounces or grams)
     * @return string
     */
    public function renderSmallWeightUnit() {
        if ($this->isMetric) {
            return Weight::UnitGram;
        }
        return Weight::UnitOunce;
    }

    /**
     * Returns pounds value for view value in pounds or kilograms
     * @param float $lb
     * @param bool $isWithUnit
     * @return string
     */
    public function renderLargeWeight($lb, $isWithUnit = false) {
        if ($this->isMetric) {
            return round(Weight::convertPoundsToKilograms($lb), 2) .
                ($isWithUnit ? ' ' . $this->renderLargeWeightUnit() : '');
        }
        return round($lb, 2) . ($isWithUnit ? ' ' . $this->renderLargeWeightUnit() : '');
    }

    /**
     * Import large weight value (pounds or kilograms) into pounds
     * @param float $value
     * @return float
     */
    public function acceptLargeWeight($value) {
        $value = (float)$value;
        if ($this->isMetric) {
            return Weight::convertKilogramsToPounds($value);
        }
        return $value;
    }

    /**
     * Returns large weight unit string (pounds or kilograms)
     * @return string
     */
    public function renderLargeWeightUnit() {
        if ($this->isMetric) {
            return Weight::UnitKilogram;
        }
        return Weight::UnitPound;
    }
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible {

class ViewBuilderV1 extends ViewBuilder {
    protected function renderBreadcrumbs() {
        $crumbs = array_map(function($breadcrumb) {
            return sprintf('%s<a href="%s">%s</a>', $breadcrumb['separator'], $breadcrumb['href'], $breadcrumb['text']);
        }, $this->getData('breadcrumbs'));
        return sprintf('<div class="breadcrumb">%s</div>', implode("\n", $crumbs));
    }

    protected function renderHeadingNotice() {
        if ($this->getData('error_warning')) {
            return sprintf('<div class="warning">%s</div>', $this->getData('error_warning'));
        } elseif ($this->getData('success')) {
            return sprintf('<div class="success">%s</div>', $this->getData('success'));
        }
        return '';
    }

    protected function renderDemo() {
        if ($this->data['is_demo_mode']) {
            return sprintf('<div class="attention"><div><strong>%s</strong></div><div>%s</div></div>',
                $this->getData('demo_title'), $this->getData('demo_info'));
        }
        return '';
    }

    protected function renderLicense() {
        if (!isset($_COOKIE[$this->getData('license_cookie')]) && !$this->data['is_demo_mode']) {
            return sprintf('<div id="license" class="attention" style="background-image: %s;"><div>' .
                '<strong>%s</strong></div><div>%s</div><div class="license__controls">' .
                '<a class="button" href="%s">%s</a>' .
                '<a class="button" onclick="objSmartFlexible.licenseClose();">%s</a></div></div>',
                $this->getLicenseLogo(), $this->getData('license_title'), $this->getData('license_info'),
                Author::PersonalAreaUrl, $this->getData('license_buy'), $this->getData('license_close'));
        }
        return '';
    }

    protected function renderSubHeader() {
        return $this->renderBreadcrumbs() .
            $this->renderHeadingNotice() .
            $this->renderDemo() .
            $this->renderLicense() .
            $this->getScripts() .
            $this->getStyles();
    }

    public function renderContainer($content) {
        return sprintf('<div class="box"><div class="heading">' .
            '<h1><img src="view/image/shipping.png" alt="" /> %s</h1>' .
            '<div class="buttons">' .
            '<a onclick="$(\'#smart_flexible\').submit();" class="button">%s</a>' .
            '<a href="%s" class="button">%s</a>' .
            '</div></div><div class="content">%s</div></div>',
            $this->getData('heading_title'),
            $this->getData('button_save'),
            $this->getData('cancel'),
            $this->getData('button_cancel'),
            $content
        );
    }

    protected function renderTab(Controls\Tab $tab) {
        return sprintf('<div id="%s" class="tab-pane"><table class="form">%s</table></div>',
            $tab->getId(), $tab->getContent());
    }

    public function renderTabs($tabs = array()) {
        $tabsHeaders = array();
        $tabsContent = array();
        foreach ($tabs as $tab) {
            if ($tab) {
                /** @var Controls\Tab $tab */
                $tabsHeaders[] = sprintf('<a href="#%s">%s</a>', $tab->getId(), $this->getData($tab->getTitleKey()));
                $tabsContent[] = $this->renderTab($tab);
            }
        }
        return sprintf('<div id="tabs" class="htabs">%s</div>%s',
            implode("\n", $tabsHeaders), implode("\n", $tabsContent));
    }

    public function renderSetting($titleKey, $helpKey, $errorKey, $isRequired, $content) {
        $requiredHtml = $isRequired ? '<span class="required">*</span> ' : '';
        return sprintf('<tr><td valign="top"><p>%s%s</p><div class="%s">%s</div></td><td>%s%s</td></tr>',
            $requiredHtml,
            $this->getData($titleKey),
            $this->getHelpCssClass(),
            $this->getData($helpKey),
            $content,
            $this->renderError($errorKey));
    }

    protected function renderError($key) {
        if ($this->getData($key)) {
            return sprintf('<span class="error">%s</span>', $this->getData($key));
        }
        return '';
    }

    /**
     * @param bool $isForServices
     * @return string
     */
    protected function getTableCssClass($isForServices = false) {
        return $isForServices ? 'form' : 'list';
    }

    /**
     * @return string
     */
    protected function getCenterCssClass() {
        return 'center';
    }

    /**
     * @return string
     */
    protected function getHelpCssClass() {
        return 'help';
    }

    /**
     * Returns global header
     * @return string
     */
    protected function getHeader() {
        return $this->getData('header');
    }

    /**
     * @return string
     */
    protected function getInitMethod() {
        return 'initForVersion1';
    }

    /**
     * @return bool
     */
    protected function isInputAddonRequiresSpacing() {
        return true;
    }

    /**
     * @return string
     */
    protected function getContentGlobalCssClass() {
        return '_v1';
    }
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible {

class ViewBuilderV2 extends ViewBuilder {
    protected function renderBreadcrumbs() {
        $crumbs = array_map(function($breadcrumb) {
            return sprintf('<li><a href="%s">%s</a></li>', $breadcrumb['href'], $breadcrumb['text']);
        }, $this->getData('breadcrumbs'));
        return sprintf('<ul class="breadcrumb">%s</ul>', implode("\n", $crumbs));
    }

    protected function renderHeadingNotice() {
        if ($this->getData('error_warning')) {
            return sprintf('<div class="alert alert-danger" role="alert">%s</div>', $this->getData('error_warning'));
        } elseif ($this->getData('success')) {
            return sprintf('<div class="alert alert-success" role="alert">%s</div>', $this->getData('success'));
        }
        return '';
    }

    protected function renderDemo() {
        if ($this->data['is_demo_mode']) {
            return sprintf('<div class="row"><div class="col-xs-6 col-xs-offset-3"><div class="panel panel-warning">' .
                '<div class="panel-heading"><strong>%s</strong></div><div class="panel-body">%s</div>' .
                '</div></div></div>',
                $this->getData('demo_title'), $this->getData('demo_info'));
        }
        return '';
    }

    protected function renderLicense() {
        if (!isset($_COOKIE[$this->getData('license_cookie')]) && !$this->data['is_demo_mode']) {
            return sprintf('<div id="license" class="row"><div class="col-xs-6 col-xs-offset-3">' .
                '<div class="panel panel-info"><div class="panel-heading" ' .
                'style="background-image: %s;"><strong>%s</strong></div>' .
                '<div class="panel-body"><div>%s</div><div class="license__controls">' .
                '<a class="btn btn-primary" href="%s">%s</a>' .
                '<a class="btn btn-default" onclick="objSmartFlexible.licenseClose();">%s</a></div></div>' .
                '</div></div></div>',
                $this->getLicenseLogo(), $this->getData('license_title'), $this->getData('license_info'),
                Author::PersonalAreaUrl, $this->getData('license_buy'), $this->getData('license_close'));
        }
        return '';
    }

    protected function renderSubHeader() {
        return sprintf('<div class="page-header"><div class="container-fluid"><div class="pull-right pad-bottom-sm">' .
            '<a onclick="$(\'#smart_flexible\').submit();" class="btn btn-primary">' .
            '<i class="fa fa-floppy-o pad-right-sm"></i> %s</a>' .
            '<a href="%s" class="btn btn-default"><i class="fa fa-reply pad-right-sm"></i> %s</a> ' .
            '</div><h1 class="panel-title">%s</h1>%s</div></div>%s',
            $this->getData('button_save'),
            $this->getData('cancel'),
            $this->getData('button_cancel'),
            $this->getData('heading_title'),
            $this->renderBreadcrumbs(),
            $this->renderHeadingNotice() .
            $this->renderDemo() .
            $this->renderLicense() .
            $this->getScripts() .
            $this->getStyles());
    }

    public function renderContainer($content) {
        return sprintf('<div class="container-fluid">%s</div>', $content);
    }

    protected function renderTab(Controls\Tab $tab) {
        $defaultClass = $tab->getIsDefault() ? ' active' : '';
        return sprintf('<div id="%s" class="tab-pane%s">%s</div>', $tab->getId(), $defaultClass, $tab->getContent());
    }

    public function renderTabs($tabs = array()) {
        $tabsHeaders = array();
        $tabsContent = array();
        foreach ($tabs as $tab) {
            if ($tab) {
                /** @var Controls\Tab $tab */
                $tabsHeaders[] = sprintf('<li role="presentation"%s>' .
                    '<a href="#%s" role="tab" data-toggle="tab">%s</a></li>',
                    $tab->getIsDefault() ? ' class="active"' : '',
                    $tab->getId(),
                    $this->getData($tab->getTitleKey()));
                $tabsContent[] = $this->renderTab($tab);
            }
        }
        return sprintf('<ul class="nav nav-tabs" role="tablist">%s</ul><div class="tab-content">%s</div>',
            implode("\n", $tabsHeaders), implode("\n", $tabsContent));
    }

    public function renderSetting($titleKey, $helpKey, $errorKey, $isRequired, $content) {
        $requiredClass = $isRequired ? ' text-warning' : '';
        return sprintf('<div class="form-group"><div class="col-sm-4"><label class="control-label%s">%s</label>' .
            '<div class="%s">%s</div></div><div class="col-sm-8">%s%s</div></div>',
            $requiredClass,
            $this->getData($titleKey),
            $this->getHelpCssClass(),
            $this->getData($helpKey),
            $content,
            $this->renderError($errorKey));
    }

    protected function renderError($key) {
        if ($this->getData($key)) {
            return sprintf('<p class="text-danger">%s</p>', $this->getData($key));
        }
        return '';
    }

    /**
     * @param bool $isForServices
     * @return string
     */
    protected function getTableCssClass($isForServices = false) {
        return 'table';
    }

    /**
     * @return string
     */
    protected function getCenterCssClass() {
        return 'text-center';
    }

    /**
     * @return string
     */
    protected function getHelpCssClass() {
        return 'help-block';
    }

    /**
     * Returns global header
     * @return string
     */
    protected function getHeader() {
        return $this->getData('header') . $this->getData('column_left');
    }

    /**
     * @return string
     */
    protected function getInitMethod() {
        return 'initForVersion2';
    }

    /**
     * @return bool
     */
    protected function isInputAddonRequiresSpacing() {
        return false;
    }

    /**
     * @return string
     */
    protected function getContentGlobalCssClass() {
        return '_v2';
    }

}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible {

class ViewBuilderV3 extends ViewBuilderV2 {
    // same

    /**
     * @return string
     */
    protected function getInitMethod() {
        return 'initForVersion3';
    }

    /**
     * @return string
     */
    protected function getContentGlobalCssClass() {
        return '_v3';
    }
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Origin {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\ConfigurationException;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Packer\PackerBox;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Locale;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Weight;

abstract class OriginPackage {
    const TYPE_BOX = 'box';
    const TYPE_ENVELOPE = 'envelope';
    const TYPE_PALLET = 'pallet';
    const TYPE_CRATE = 'crate';

    /**
     * Id in provider. Null defines a custom box
     * @var int|null
     */
    protected $id;
    /**
     * Shipping service (foreign) code for API requests.
     * @var string|null
     */
    protected $code;
    /** @var string */
    protected $title;
    /** @var string */
    protected $image;
    /** @var float */
    protected $tareWeight;
    /** @var bool */
    protected $isTareCalculated = false;
    /** @var float|null */
    protected $maxWeight;

    /**
     * @return int|null
     */
    final public function getId() {
        return $this->id;
    }

    /**
     * @return null|string
     */
    final public function getCode() {
        return $this->code;
    }

    /**
     * @return float
     */
    final public function getTareWeight() {
        return $this->tareWeight;
    }

    /**
     * @return string
     */
    final public function getImage() {
        return $this->image;
    }

    /**
     * @return string
     */
    final public function getTitle() {
        return $this->title;
    }

    /**
     * @return float|null
     */
    final public function getMaxWeight() {
        return $this->maxWeight;
    }


    /** @return string */
    abstract public function getType();

    /**
     * @param Locale $locale
     * @return string
     */
    abstract public function getSizeDescription($locale);

    /**
     * @param Locale $locale
     * @return string
     */
    final public function getWeightDescription($locale) {
        return ($this->isTareCalculated ? '&asymp;' : '') . $this->getTareWeightWithLocale($locale, true) .
        ($this->getMaxWeight() ? (', ' . $locale->renderLargeWeight($this->getMaxWeight(), true) . ' max') : '');
    }


    /**
     * This method MUST be defined in final class
     * @throws ConfigurationException
     * @return string
     */
    protected static function getTareUnitType() {
        throw new ConfigurationException('Can not be called from abstract class');
    }

    /**
     * This method MUST be defined in final class
     * @throws ConfigurationException
     */
    protected static function getDefaultDensity() {
        throw new ConfigurationException('Can not be called from abstract class');
    }

    /**
     * @param Locale $locale
     * @param bool $isWithUnit
     * @throws ConfigurationException
     * @return string
     */
    final public function getTareWeightWithLocale($locale, $isWithUnit = false) {
        switch ($this->getTareUnitType()) {
            case Weight::UnitTypeLarge:
                return $locale->renderLargeWeight($this->getTareWeight(), $isWithUnit);
            case Weight::UnitTypeSmall:
                return $locale->renderSmallWeight(Weight::convertPoundsToOunces($this->getTareWeight()), $isWithUnit);
            default:
                throw new ConfigurationException('Unknown tare weight unit type ' . $this->getTareUnitType());
        }
    }

    /**
     * @param float $maxWeight
     * @param float $maxHeight for Pallets
     * @return PackerBox[]
     */
    abstract public function generatePackerBoxes($maxWeight, $maxHeight);

    /** @return array */
    abstract public function toArray();

    /**
     * @param array $data
     * @throws ConfigurationException
     * @return OriginPackage
     */
    final public static function createFromArray($data) {
        switch ($data['type']) {
            case self::TYPE_BOX:
                return new OriginBox($data);
            case self::TYPE_ENVELOPE:
                return new OriginEnvelope($data);
            case self::TYPE_PALLET:
                return new OriginPallet($data);
            case self::TYPE_CRATE:
                return new OriginCrate($data);
            default:
                throw new ConfigurationException('Unknown Origin Package type: ' . $data['type']);
        }
    }

    /**
     * @param string $type
     * @return float
     * @throws ConfigurationException
     */
    final public static function getCustomPackageDensity($type) {
        switch ($type) {
            case self::TYPE_BOX:
                return OriginBox::getDefaultDensity();
            case self::TYPE_ENVELOPE:
                return OriginEnvelope::getDefaultDensity();
            case self::TYPE_PALLET:
                return OriginPallet::getDefaultDensity();
            case self::TYPE_CRATE:
                return OriginCrate::getDefaultDensity();
            default:
                throw new ConfigurationException('Unknown Origin Package type: '. $type);
        }
    }

    /**
     * @param string $type
     * @return string
     * @throws ConfigurationException
     */
    final public static function getCustomPackageTareUnitType($type) {
        switch ($type) {
            case self::TYPE_BOX:
                return OriginBox::getTareUnitType();
            case self::TYPE_ENVELOPE:
                return OriginEnvelope::getTareUnitType();
            case self::TYPE_PALLET:
                return OriginPallet::getTareUnitType();
            case self::TYPE_CRATE:
                return OriginCrate::getTareUnitType();
            default:
                throw new ConfigurationException('Unknown Origin Package type: '. $type);
        }
    }

    final public static function getCustomPackageTitle($type) {
        return ucfirst($type);
    }


}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Origin {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Weight;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Length;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Locale;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Packer\PackerBox;

abstract class OriginFixed extends OriginPackage {
    /** @var float[]|null[] */
    protected $dimensionsOutside;
    /** @var float[]|null[] */
    protected $dimensionsInside;

    final public static function createFromMetric($data) {
        $class = get_called_class();
        if (isset($data['tare'])) {
            $data['tare'] = call_user_func(Array($class, 'getTareUnitType')) === Weight::UnitTypeSmall ?
                Weight::convertGramsToOunces($data['tare']) : Weight::convertKilogramsToPounds($data['tare']);
        }
        if (isset($data['max_load'])) {
            $data['max_load'] = Weight::convertKilogramsToPounds($data['max_load']);
        }
        if (isset($data['max_weight'])) {
            $data['max_weight'] = Weight::convertKilogramsToPounds($data['max_weight']);
        }
        $cmToIn = function($v) {
            return Length::convertCentimetersToInches($v);
        };
        $data['dimensions_outside'] = array_map($cmToIn, $data['dimensions_outside']);
        $data['dimensions_inside'] = array_map($cmToIn, $data['dimensions_inside']);
        return new $class($data);
    }

    /**
     * @param Locale $locale
     * @return string
     */
    final public function getSizeDescription($locale) {
        return $locale->renderDimensions($this->dimensionsOutside);
    }

    /**
     * @return array
     */
    final public function toArray() {
        return array(
            'type' => $this->getType(),
            'id' => $this->id,
            'code' => $this->code,
            'title' => $this->title,
            'image' => $this->image,
            'tare' => $this->getTareUnitType() === Weight::UnitTypeSmall ?
                Weight::convertPoundsToOunces($this->tareWeight) : $this->tareWeight,
            'max_weight' => $this->maxWeight,
            'dimensions_outside' => $this->dimensionsOutside,
            'dimensions_inside' => $this->dimensionsInside
        );
    }

}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Origin {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Weight;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Length;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Locale;

abstract class OriginExpandable extends OriginPackage {
    /** @var float */
    protected $length;
    /** @var float */
    protected $width;
    /** @var float|null */
    protected $maxHeight;

    final public static function createFromMetric($data) {
        $class = get_called_class();
        if (isset($data['tare'])) {
            $data['tare'] = call_user_func(Array($class, 'getTareUnitType')) === Weight::UnitTypeSmall ?
                Weight::convertGramsToOunces($data['tare']) : Weight::convertKilogramsToPounds($data['tare']);
        }
        if (isset($data['max_load'])) {
            $data['max_load'] = Weight::convertKilogramsToPounds($data['max_load']);
        }
        if (isset($data['max_weight'])) {
            $data['max_weight'] = Weight::convertKilogramsToPounds($data['max_weight']);
        }
        $data['length'] = Length::convertCentimetersToInches($data['length']);
        $data['width'] = Length::convertCentimetersToInches($data['width']);
        if (isset($data['max_height'])) {
            $data['max_height'] = Length::convertCentimetersToInches($data['max_height']);
        }
        return new $class($data);
    }

    /**
     * @param Locale $locale
     * @return string
     */
    final public function getSizeDescription($locale) {
        return $locale->renderDimensions(array($this->length, $this->width)) .
        ($this->maxHeight ? ' (x ' . $locale->renderLength($this->maxHeight, true) . ' max)' : '');
    }

    final public function toArray() {
        return array(
            'type' => $this->getType(),
            'id' => $this->id,
            'code' => $this->code,
            'title' => $this->title,
            'image' => $this->image,
            'tare' => $this->getTareUnitType() === Weight::UnitTypeSmall ?
                Weight::convertPoundsToOunces($this->tareWeight) : $this->tareWeight,
            'max_weight' => $this->maxWeight,
            'length' => $this->length,
            'width' => $this->width,
            'max_height' => $this->maxHeight
        );
    }


}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Origin {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\ConfigurationException;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Weight;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Square;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Packer\PackerBox;

class OriginBox extends OriginFixed {
    const CustomBoxImage = 'custom-box.png';

    /**
     * tare - predefined weight of the box (oz)
     * density - Weight material density constant
     * max_load - load limit for custom boxes (lb)
     * max_weight - brutto weight limit (lb)
     * dimensions_outside, dimensions_inside - (in)
     * @since 18.10.2016 tare can be zero and dimensions can be null
     * @param array $data
     * @throws ConfigurationException
     */
    public function __construct($data) {
        if (!isset($data['tare'])) {
            if (!isset($data['density'])) {
                throw new ConfigurationException('Tare or Density must be specified for OriginBox');
            }
            $this->isTareCalculated = true;
        }
        $data['max_load'] = isset($data['max_load']) ? $data['max_load'] : 0;
        $this->dimensionsOutside = isset($data['dimensions_outside']) ? $data['dimensions_outside'] :
            array(null, null, null);
        $this->dimensionsInside = isset($data['dimensions_inside']) ? $data['dimensions_inside'] :
            array(null, null, null);
        $this->id = isset($data['id']) ? $data['id'] : null;
        $this->code = isset($data['code']) ? $data['code'] : null;
        $this->title = $data['title'];
        $this->image = isset($data['image']) ? $data['image'] : self::CustomBoxImage;
        $this->tareWeight = isset($data['tare']) ? Weight::convertOuncesToPounds($data['tare']) :
            Weight::ofMaterial(Square::ofBoxSurface($this->dimensionsOutside), $data['density']);
        $this->maxWeight = isset($data['max_weight']) ? $data['max_weight'] :
            ($data['max_load'] ? $data['max_load'] + $this->tareWeight : null);
    }

    public function getType() {
        return self::TYPE_BOX;
    }

    public static function getTareUnitType() {
        return Weight::UnitTypeSmall;
    }

    public static function getDefaultDensity() {
        return Weight::SoftCartonDensity;
    }

    /**
     * @param float $maxWeight
     * @param float $maxHeight not used
     * @return PackerBox[]
     */
    final public function generatePackerBoxes($maxWeight, $maxHeight) {
        return array(new PackerBox(array(
            'tare' => $this->tareWeight,
            'max_weight' => $this->maxWeight ? min($this->maxWeight, $maxWeight) : $maxWeight,
            'dimensions_outside' => $this->dimensionsOutside,
            'dimensions_inside' => $this->dimensionsInside,
            'origin' => $this
        )));
    }

}
}

namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Utils {

class Length {
    const CentimetersInInch = 2.54;
    const MillimetersInCentimeter = 10;
    const UnitInchShort = '&quot;';
    const UnitInchLong = 'in';
    const UnitCentimeter = 'cm';

    /**
     * Converts centimeters to inches
     * @param float $cm
     * @return float
     */
    public static function convertCentimetersToInches($cm) {
        return $cm / self::CentimetersInInch;
    }

    /**
     * Converts inches to centimeters
     * @param float $in
     * @return float
     */
    public static function convertInchesToCentimeters($in) {
        return $in * self::CentimetersInInch;
    }

    /**
     * Converts centimeters to millimeters
     * @param float $cm
     * @return float
     */
    public static function convertCentimetersToMillimeters($cm) {
        return $cm * self::MillimetersInCentimeter;
    }
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Origin {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\ConfigurationException;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Packer\PackerBox;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Weight;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Square;

class OriginEnvelope extends OriginExpandable {
    const CustomEnvelopeImage = 'custom-envelope.png';

    /**
     * Length and Width dimensions being reduced depending on Height dimension grows and using this value
     * X(z) = Xo - 1.95 * Z
     */
    const DimensionsRatio = 1.95;

    /** Used to calculate number of steps (must be less than 1) */
    const StepsRatio = 0.5;
    const MinSteps = 2;
    const MinGrow = 0.05;

    /**
     * tare - predefined weight of the envelope (oz)
     * density - Weight material density constant
     * max_load - load limit for custom envelopes (lb)
     * max_weight - brutto weight limit (lb)
     * max_height - expandable height limit (in)
     * length, width - (in)
     * @since 18.10.2016 tare can be zero
     * @param $data
     * @throws ConfigurationException
     */
    public function __construct($data) {
        if (!isset($data['tare'])) {
            if (!isset($data['density'])) {
                throw new ConfigurationException('Tare or Density must be specified for OriginEnvelope');
            }
            $this->isTareCalculated = true;
        }
        $data['max_load'] = isset($data['max_load']) ? $data['max_load'] : 0;
        $data['max_height'] = isset($data['max_height']) ? $data['max_height'] : 0;
        $this->length = $data['length'];
        $this->width = $data['width'];
        $this->id = isset($data['id']) ? $data['id'] : null;
        $this->code = isset($data['code']) ? $data['code'] : null;
        $this->title = $data['title'];
        $this->image = isset($data['image']) ? $data['image'] : self::CustomEnvelopeImage;
        $this->tareWeight = isset($data['tare']) ? Weight::convertOuncesToPounds($data['tare']) :
            Weight::ofMaterial(Square::ofFlatSurface($this->length, $this->width), $data['density']);
        $this->maxWeight = isset($data['max_weight']) ? $data['max_weight'] :
            ($data['max_load'] ? $data['max_load'] + $this->tareWeight : null);
        $this->maxHeight = $data['max_height'] ?: null;
    }

    public function getType() {
        return self::TYPE_ENVELOPE;
    }

    public static function getTareUnitType() {
        return Weight::UnitTypeSmall;
    }

    public static function getDefaultDensity() {
        return Weight::PaperDensity;
    }

    /**
     * @param float $maxWeight
     * @param float $maxHeight not used
     * @return PackerBox[]
     */
    public function generatePackerBoxes($maxWeight, $maxHeight) {
        $result = array();
        $minDimension = min($this->length, $this->width);

        // less than predefined limit or width
        $heightLimit = $this->maxHeight ?: $minDimension;
        $steps = max(round($heightLimit / self::StepsRatio), self::MinSteps);
        $stepGrow = max($heightLimit / $steps, self::MinGrow);
        for ($height = $stepGrow; $height <= $heightLimit; $height += $stepGrow) {
            $dimensionReducer = self::DimensionsRatio * $height;
            if ($height >= $minDimension - $dimensionReducer) {
                // less than calculated width
                break;
            }
            $dimensions = array(
                $this->length - $dimensionReducer,
                $this->width - $dimensionReducer,
                $height
            );
            $result[] = new PackerBox(array(
                'tare' => $this->tareWeight,
                'max_weight' => ($this->maxWeight ? min($this->maxWeight, $maxWeight) : $maxWeight),
                'dimensions_outside' => $dimensions,
                'dimensions_inside' => $dimensions,
                'origin' => $this
            ));
        }
        return $result;
    }

}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Origin {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\ConfigurationException;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Packer\PackerBox;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Weight;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Square;

class OriginPallet extends OriginExpandable {
    const CustomPalletImage = 'custom-pallet.png';

    /** Used to calculate number of steps */
    const StepsRatio = 1.0;
    const MinSteps = 2;
    const MinGrow = 0.25;
    /** Height of wood */
    const PalletHeight = 5.67;

    /**
     * tare - predefined weight of the pallet (LB!)
     * density - Weight material density constant
     * max_load - load limit for custom pallet (lb)
     * max_weight - brutto weight limit (lb)
     * max_height - expandable height limit (in)
     * length, width, height - (in)
     * @since 18.10.2016 tare can be zero
     * @param $data
     * @throws ConfigurationException
     */
    public function __construct($data) {
        if (!isset($data['tare'])) {
            if (!isset($data['density'])) {
                throw new ConfigurationException('Tare or Density must be specified for OriginPallet');
            }
            $this->isTareCalculated = true;
        }
        $data['max_load'] = isset($data['max_load']) ? $data['max_load'] : 0;
        $this->length = $data['length'];
        $this->width = $data['width'];
        $this->maxHeight = isset($data['max_height']) ? $data['max_height'] : null;
        $this->id = isset($data['id']) ? $data['id'] : null;
        $this->code = isset($data['code']) ? $data['code'] : null;
        $this->title = $data['title'];
        $this->image = isset($data['image']) ? $data['image'] : self::CustomPalletImage;
        $this->tareWeight = isset($data['tare']) ? $data['tare'] :
            Weight::ofMaterial(Square::ofFlatSurface($this->length, $this->width), $data['density']);
        $this->maxWeight = isset($data['max_weight']) ? $data['max_weight'] :
            ($data['max_load'] ? $data['max_load'] + $this->tareWeight : null);
    }

    public function getType() {
        return self::TYPE_PALLET;
    }

    public static function getTareUnitType() {
        return Weight::UnitTypeLarge;
    }

    public static function getDefaultDensity() {
        return Weight::WoodDensity;
    }

    /**
     * @param float $maxWeight
     * @param float $maxHeight
     * @return PackerBox[]
     */
    public function generatePackerBoxes($maxWeight, $maxHeight) {
        $result = array();
        $finalMaxHeight = $this->maxHeight ? min($this->maxHeight, $maxHeight) : $maxHeight;
        $steps = max(round($finalMaxHeight / self::StepsRatio), self::MinSteps);
        $stepGrow = max($finalMaxHeight / $steps, self::MinGrow);
        for ($height = $stepGrow; $height <= $finalMaxHeight; $height += $stepGrow) {
            if ($height < self::PalletHeight) {
                continue;
            }
            $dimensionsOutside = array($this->length, $this->width, $height);
            $dimensionsInside = array($this->length, $this->width, $height - self::PalletHeight);
            $result[] = new PackerBox(array(
                'tare' => $this->tareWeight,
                'max_weight' => ($this->maxWeight ? min($this->maxWeight, $maxWeight) : $maxWeight),
                'dimensions_outside' => $dimensionsOutside,
                'dimensions_inside' => $dimensionsInside,
                'dimensions_sort' => false,
                'origin' => $this
            ));
        }
        return $result;
    }

}
}

namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Origin {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\ConfigurationException;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Weight;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Square;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Packer\PackerBox;

class OriginCrate extends OriginFixed {
    const CustomCrateImage = 'custom-crate.png';
    const CrateSideThickness = 0.25;
    /** Height of wood in the bottom */
    const CratePalletHeight = 5.67;

    /**
     * tare - predefined weight of the crate (LB!)
     * density - Weight material density constant
     * max_load - load limit for custom crate (lb)
     * max_weight - brutto weight limit (lb)
     * dimensions_outside, dimensions_inside - (in)
     * @since 18.10.2016 tare can be zero and dimensions can be null
     * @param array $data
     * @throws ConfigurationException
     */
    public function __construct($data) {
        if (!isset($data['tare'])) {
            if (!isset($data['density'])) {
                throw new ConfigurationException('Tare or Density must be specified for OriginCrate');
            }
            $this->isTareCalculated = true;
        }
        $data['max_load'] = isset($data['max_load']) ? $data['max_load'] : 0;
        $this->dimensionsInside = $data['dimensions_inside'];
        $this->dimensionsOutside = isset($data['id']) ? $data['dimensions_outside'] : array( // when custom
            $data['dimensions_outside'][0] + 2 * self::CrateSideThickness,
            $data['dimensions_outside'][1] + 2 * self::CrateSideThickness,
            $data['dimensions_outside'][2] + self::CratePalletHeight + self::CrateSideThickness
        );
        $this->id = isset($data['id']) ? $data['id'] : null;
        $this->code = isset($data['code']) ? $data['code'] : null;
        $this->title = $data['title'];
        $this->image = isset($data['image']) ? $data['image'] : self::CustomCrateImage;
        $this->tareWeight = isset($data['tare']) ? $data['tare'] :
            Weight::ofMaterial(Square::ofBoxSurface($this->dimensionsOutside), $data['density']);
        $this->maxWeight = isset($data['max_weight']) ? $data['max_weight'] :
            ($data['max_load'] ? $data['max_load'] + $this->tareWeight : null);
    }

    public function getType() {
        return self::TYPE_CRATE;
    }

    public static function getTareUnitType() {
        return Weight::UnitTypeLarge;
    }

    public static function getDefaultDensity() {
        return Weight::WoodDensity;
    }

    /**
     * @param float $maxWeight
     * @param float $maxHeight not used
     * @return PackerBox[]
     */
    public function generatePackerBoxes($maxWeight, $maxHeight) {
        return array(new PackerBox(array(
            'tare' => $this->tareWeight,
            'max_weight' => $this->maxWeight ? min($this->maxWeight, $maxWeight) : $maxWeight,
            'dimensions_outside' => $this->dimensionsOutside,
            'dimensions_inside' => $this->dimensionsInside,
            'dimensions_sort' => false,
            'origin' => $this
        )));
    }

}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Packer {

interface PackerInterface {
    public function addBox($box);
    public function addItem($item, $quantity);
    public function pack();
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Packer {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping\Configuration;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\ConfigurationException;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\CurrencyConverter;

abstract class ValuableBox {
    /**
     * @return float|null
     */
    abstract public function getOuterHeight();

    /**
     * @return float|null
     */
    abstract public function getInnerHeight();

    /**
     * @return float|null
     */
    abstract public function getOuterLength();

    /**
     * @return float|null
     */
    abstract public function getInnerLength();

    /**
     * @return float
     */
    abstract public function getValue();

    /**
     * @return float
     */
    abstract public function getWeight();

    /**
     * @return float|null
     */
    abstract public function getOuterWidth();

    /**
     * @return float|null
     */
    abstract public function getInnerWidth();

    /**
     * @return float|null
     */
    abstract public function getOuterVolume();

    /**
     * @return string
     */
    abstract public function getCode();

    /**
     * Returns that insurance is enabled only when total boxes value is above the limit
     * @since 03.08.2017 accepts $total in system currency
     * @param float|null $total in system currency, Null forces to use $boxes
     * @param ValuableBox[]|null $boxes
     * @param Configuration $configuration
     * @param CurrencyConverter|null $converter, not required when using total
     * @param string|null $boxCurrency Boxes value currency, not required when using total
     * @return bool
     */
    public static function shouldInsuranceBeIncluded($total, $boxes, $configuration, $converter, $boxCurrency) {
        if (!(bool)$configuration->get('insurance')) {
            return false;
        }
        $min = (float)$configuration->get('insurance_from');
        if ($min > 0) {
            if ($total) {
                return (float)$total >= $min; // in system currency
            } elseif (is_array($boxes)) {
                try {
                    $total = array_sum(array_map(function ($box) {
                        /** @var ValuableBox $box */
                        if (!method_exists($box, 'getValue')) {
                            throw new ConfigurationException('invalid argument - boxes');
                        }
                        return $box->getValue();
                    }, $boxes));
                } catch (\Exception $e) {
                    return true;
                }
                return $converter->toSystem($total, $boxCurrency) >= $min; // back to system currency
            }
        }
        return true;
    }
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Packer {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker\PackedBoxList;

class MultiPacker {
    /** @var PackerInterface[] */
    protected $packers;

    /**
     * @param PackerInterface[] $packers
     */
    public function __construct($packers) {
        $this->packers = $packers;
    }

    /**
     * @param PackerInterface[] $packers
     */
    public function addPackers($packers) {
        $this->packers = array_merge($this->packers, $packers);
    }

    /**
     * @param PackerItem $item
     * @param int $quantity
     */
    public function addItem($item, $quantity) {
        foreach ($this->packers as $packer) {
            $packer->addItem($item, $quantity);
        }
    }

    /**
     * @param PackerBox $box
     */
    public function addBox($box) {
        foreach ($this->packers as $packer) {
            $packer->addBox($box);
        }
    }

    /**
     * @throws \Exception
     * @return PackedBoxList
     */
    public function pack() {
        $results = array();
        $errorsMessages = array();
        foreach ($this->packers as $packer) {
            try {
                $packedResult = $packer->pack();
                $results[] = $packedResult;
            } catch (\Exception $e) {
                $errorsMessages[] = get_class($packer) . ': ' . $e->getMessage();
            }
        }

        /**
         * @param PackedBoxList $res
         * @return float
         */
        $calculateTotalVolume = function($res) {
            $res = clone $res;
            $volume = 0;
            foreach ($res as $packedBox) {
                /** @var PackerBox $boxType */
                $boxType = $packedBox->getBox();
                $volume += $boxType->getInnerLength() * $boxType->getInnerWidth() * $boxType->getInnerDepth();
            }
            return $volume;
        };

        $bestResult = null;
        $bestCount = null;
        $bestVolume = null;
        /** @var PackedBoxList $result */
        foreach ($results as $result) {
            $calculatedVolume = $calculateTotalVolume($result);
            if (
                $bestCount === null ||
                $result->count() < $bestCount ||
                ($result->count() === $bestCount && $calculatedVolume < $bestVolume)
            ) {
                $bestCount = $result->count();
                $bestVolume = $calculatedVolume;
                $bestResult = $result;
            }
        }

        if ($bestResult) {
            return $bestResult;
        } else {
            throw new \Exception(implode(".\n", $errorsMessages));
        }
    }
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Packer {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker\PackedBoxList;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker\PackedBox;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker\ItemList;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\PackagingMap\Map;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\PackagingMap\Reserved;

class SimplePacked {
    const AveragedItemText = 'Averaged item';
    /** @var int */
    private $quantity;
    /** @var Reserved[] */
    private $reserved;

    public function __construct($quantity, $reserved) {
        $this->quantity = $quantity;
        $this->reserved = $reserved;
    }

    /**
     * @return int
     */
    public function getQuantity() {
        return $this->quantity;
    }

    /**
     * @return Reserved[]
     */
    public function getReserved() {
        return $this->reserved;
    }
}

class SimpleSameItemPacker implements PackerInterface {
    /** @var PackerBox[] */
    protected $boxes;
    /** @var PackerItem[] */
    protected $items;

    /**
     * @param PackerBox $box
     */
    public function addBox($box) {
        $this->boxes[] = $box;
    }

    /**
     * @param PackerItem $item
     * @param int $quantity
     */
    public function addItem($item, $quantity) {
        for ($i = 0; $i < $quantity; $i++) {
            $this->items[] = $item;
        }
    }

    /**
     * @since 18.10.2016 this packing method uses box tare weight
     * @throws \Exception
     * @return PackedBoxList
     */
    public function pack() {
        if (count($this->items) === 0) {
            throw new \Exception('Items are not specified');
        }

        if (count($this->boxes) === 0) {
            throw new \Exception('Boxes are not specified');
        }

        $itemsToPack = $this->items;
        $itemLength = 0;
        $itemWidth = 0;
        $itemDepth = 0;
        $itemWeight = 0;
        foreach ($this->items as $item) {
            /** @var PackerItem $item */
            $itemLength = max($itemLength, $item->getLength());
            $itemWidth = max($itemWidth, $item->getWidth());
            $itemDepth = max($itemDepth, $item->getDepth());
            $itemWeight = max($itemWeight, $item->getWeight());
        }

        $itemQuantity = count($itemsToPack);

        $boxResults = array();
        /** @var PackerBox $box */
        foreach ($this->boxes as $box) {
            $packed = $this->packIntoBox(array($itemLength, $itemWidth, $itemDepth), $box, $itemQuantity);
            $boxItemQuantity = min(
                $packed->getQuantity(),
                (int)(($box->getMaxWeight() - $box->getEmptyWeight()) / $itemWeight)
            );
            if ($boxItemQuantity > 0) {
                $boxInnerVolume = $box->getInnerVolume();
                if (
                    empty($boxResults[$boxItemQuantity]) ||
                    $boxResults[$boxItemQuantity]['boxVolume'] > $boxInnerVolume
                ) {
                    $boxResults[$boxItemQuantity] = array(
                        'box' => $box,
                        'boxVolume' => $boxInnerVolume,
                        'boxItemQuantity' => $boxItemQuantity,
                        'reserved' => array_slice($packed->getReserved(), 0, $boxItemQuantity)
                    );
                }
            }
        }

        if (count($boxResults) > 0) {
            ksort($boxResults);

            $result = new PackedBoxList();

            $remainingItemCount = $itemQuantity;
            while ($remainingItemCount > 0) {
                $matchingBox = null;
                foreach ($boxResults as $boxResult) {
                    if ($boxResult['boxItemQuantity'] >= $remainingItemCount) {
                        $matchingBox = $boxResult;
                        break;
                    }
                }

                if (!$matchingBox) {
                    $matchingBox = end($boxResults);
                }

                /** @var PackerBox $matchingBoxObj */
                $matchingBoxObj = $matchingBox['box'];

                $items = new ItemList();
                $itemCountToPut = min($remainingItemCount, $matchingBox['boxItemQuantity']);
                $matchingBox['reserved'] = array_slice($matchingBox['reserved'], 0, $itemCountToPut);
                while ($itemCountToPut > 0) {
                    /** @var PackerItem|PackerItemConstrained $item */
                    $item = current($itemsToPack);
                    if ($item instanceof PackerItemConstrained) {
                        if (!$item->canBePackedInBox($items, $matchingBoxObj)) {
                            break;
                        }
                    }
                    array_shift($itemsToPack);
                    $items->insert($item);
                    $remainingItemCount--;
                    $itemCountToPut--;
                }

                $result->insert(
                    new PackedBox(
                        $matchingBoxObj,
                        $items,
                        0, 0, 0,
                        $matchingBoxObj->getMaxWeight() - $matchingBoxObj->getEmptyWeight() -
                        $items->count() * $itemWeight,
                        $matchingBoxObj->getInnerWidth(),
                        $matchingBoxObj->getInnerLength(),
                        $matchingBoxObj->getInnerDepth(),
                        new Map(get_class($this), $matchingBox['reserved'])
                    )
                );
            }

            return $result;
        }

        throw new \Exception('Cannot fit items to any box');
    }

    /**
     * @param float[] $itemDimensions
     * @param PackerBox $box
     * @param int $mappingLimit max quantity to put into box packaging map
     * @return SimplePacked
     */
    private function packIntoBox($itemDimensions, $box, $mappingLimit) {
        return $this->packIntoRect(
            $this->permuteDimensions($itemDimensions),
            array($box->getInnerLength(), $box->getInnerWidth(), $box->getInnerDepth()),
            $mappingLimit
        );
    }

    /**
     * @param float[][] $itemPositions
     * @param float[] $rect
     * @param int $mappingLimit max quantity to put into packaging map
     * @param float[] $rectOrigin
     * @return SimplePacked
     */
    private function packIntoRect($itemPositions, $rect, $mappingLimit, $rectOrigin = array(0, 0, 0)) {
        list($boxLength, $boxWidth, $boxDepth) = $rect;
        list($offsetLength, $offsetWidth, $offsetDepth) = $rectOrigin;
        $quantities = array();
        $reserved = array();
        foreach ($itemPositions as $itemPos) {
            list($itemLength, $itemWidth, $itemDepth) = $itemPos;
            $lengthRowCount = (int)($boxLength / $itemLength);
            $depthRowCount = (int)($boxDepth / $itemDepth);
            $widthRowCount = (int)($boxWidth / $itemWidth);

            if ($lengthRowCount === 0 || $widthRowCount === 0 || $depthRowCount === 0) {
                continue;
            }

            $remainingLength = $boxLength - $itemLength * $lengthRowCount;
            $remainingWidth = $boxWidth - $itemWidth * $widthRowCount;
            $remainingDepth = $boxDepth - $itemDepth * $depthRowCount;

            for ($d = 0; $d < $depthRowCount; $d++) {
                for ($l = 0; $l < $lengthRowCount; $l++) {
                    for ($w = 0; $w < $widthRowCount; $w++) {
                        $reserved[] = new Reserved(
                            $offsetLength + $itemLength * $l,
                            $offsetWidth + $itemWidth * $w,
                            $offsetDepth + $itemDepth * $d,
                            $itemLength, $itemWidth, $itemDepth,
                            SimplePacked::AveragedItemText
                        );
                        if (count($reserved) >= $mappingLimit) { // prevent memory leak
                            break 3;
                        }
                    }
                }
            }

            $remainingSpaceRects = array(
                array($remainingLength, $boxWidth, $boxDepth),
                array($boxLength, $remainingWidth, $boxDepth),
                array($boxLength, $boxWidth, $remainingDepth)
            );

            $remainingOrigins = array(
                array($offsetLength + $itemLength * $lengthRowCount, $offsetWidth, $offsetDepth),
                array($offsetLength, $offsetWidth + $itemWidth * $widthRowCount, $offsetDepth),
                array($offsetLength, $offsetWidth, $offsetDepth + $itemDepth * $depthRowCount)
            );

            $mainQuantity = $lengthRowCount * $widthRowCount * $depthRowCount;
            foreach ($remainingSpaceRects as $k => $remainingRect) {
                $subPack = $this->packIntoRect(
                    $itemPositions, $remainingRect,
                    $mappingLimit - $mainQuantity, $remainingOrigins[$k]
                );
                $quantities[] = $mainQuantity + $subPack->getQuantity();
                $reserved = array_merge($reserved, $subPack->getReserved());
            }

        }

        $maxPacked = array_reduce($quantities, 'max', 0);
        return new SimplePacked($maxPacked, array_slice($reserved, 0, $maxPacked));
    }

    /**
     * @param float[] $items
     * @param float[] $perms
     * @param float[][] $ret
     * @return float[][]
     */
    private function permuteDimensions($items, $perms = array(), &$ret = array()) {
        if (empty($items)) {
            $ret[] = $perms;
        } else {
            for ($i = count($items) - 1; $i >= 0; --$i) {
                $newItems = $items;
                $newPermutations = $perms;
                list($foo) = array_splice($newItems, $i, 1);
                array_unshift($newPermutations, $foo);
                $this->permuteDimensions($newItems, $newPermutations, $ret);
            }
        }
        return $ret;
    }
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Packer {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker\PackedBoxList;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker\PackedBox;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker\ItemList;

class WeightBasedPacker implements PackerInterface {
    /** @var PackerBox[] */
    protected $boxes;
    /** @var PackerItem[] */
    protected $items;

    /**
     * @param PackerBox $box
     */
    public function addBox($box) {
        $this->boxes[] = $box;
    }

    /**
     * @param PackerItem $item
     * @param int $quantity
     */
    public function addItem($item, $quantity) {
        for ($i = 0; $i < $quantity; $i++) {
            $this->items[] = $item;
        }
    }

    /**
     * @throws \Exception
     * @return PackedBoxList
     */
    public function pack() {
        if (count($this->items) === 0) {
            throw new \Exception('Items are not specified');
        }

        if (count($this->boxes) === 0) {
            throw new \Exception('Boxes are not specified');
        }

        $itemsToPack = $this->items;
        usort($this->boxes, function ($a, $b) {
            /** @var $a PackerBox */
            /** @var $b PackerBox */
            if ($a->getMaxWeight() < $b->getMaxWeight()) {
                return -1;
            } elseif ($a->getMaxWeight() > $b->getMaxWeight()) {
                return 1;
            }
            return 0;
        });

        $result = new PackedBoxList();

        do {
            $totalRemainingWeight = array_sum(array_map(function ($item) {
                /** @var $item PackerItem */
                return $item->getWeight();
            }, $itemsToPack));

            /** @var $matchingBox PackerBox */
            $matchingBox = current(array_filter($this->boxes, function ($box) use ($totalRemainingWeight) {
                /** @var $box PackerBox */
                if ($box->getMaxWeight() >= $totalRemainingWeight) {
                    return true;
                }
                return false;
            }));

            if (!$matchingBox) {
                $matchingBox = end($this->boxes);
                reset($this->boxes);
            }

            $boxRemainingWeight = $matchingBox->getMaxWeight() - $matchingBox->getEmptyWeight();
            $packed = new ItemList();
            foreach ($itemsToPack as $k => $item) {
                if ($item->getWeight() <= $boxRemainingWeight) {
                    $packed->insert($item);
                    $boxRemainingWeight -= $item->getWeight();
                    unset($itemsToPack[$k]);
                }
            }

            if (count($packed)) {
                $result->insert(new PackedBox(
                    $matchingBox,
                    $packed,
                    0, 0, 0, $boxRemainingWeight,
                    $matchingBox->getInnerWidth(), $matchingBox->getInnerLength(), $matchingBox->getInnerDepth(),
                    null
                ));
            } else {
                throw new \Exception('Can not fit items in weight-based boxes');
            }

            $theRest = count($itemsToPack);
        } while ($theRest > 0);

        return $result;
    }

}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Packer {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker\ConstrainedItem;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker\Packer;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker\Item;

class RotatedPacker extends Packer {
    /**
     * Add rotated item (replace length and width)
     * @since 07.08.2017 takes into account constrained items
     * @param Item $item actually: PackerItem
     * @param int $qty
     */
    public function addItem(Item $item, $qty = 1) {
        /** @var PackerItem|PackerItemConstrained $item */
        $constructorOptions = array(
            'id' => $item->getId(),
            'description' => $item->getDescription(),
            'options' => $item->getOptions(),
            'length' => $item->getWidth(),
            'width' => $item->getLength(),
            'height' => $item->getHeight(),
            'weight' => $item->getWeight(),
            'price' => $item->getPrice()
        );
        if ($item instanceof ConstrainedItem) {
            $constructorOptions['logic'] = $item->getLogic();
            $rotatedItem = new PackerItemConstrained($constructorOptions);
        } else {
            $rotatedItem = new PackerItem($constructorOptions);
        }
        parent::addItem($rotatedItem, $qty);
    }
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Packer {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\Origin\OriginPackage;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Number;

class PackerBox implements \CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker\Box {
    /** Logic-fix constant. Helps to fix floating point failure to completely fill box */
    const MaxBoxWeightCorrection = 0.000000001;

    /**
     * Can be changed by adjustTareWeight()
     * @var float
     */
    private $tareWeight;
    /**
     * Not equals OriginPackage::$maxWeight
     * @var float
     */
    private $maxWeight;
    /** @var float[]|null[] */
    private $dimensionsOutside;
    /** @var float[]|null[] */
    private $dimensionsInside;
    /** @var OriginPackage */
    private $originPackage;

    public function __construct($data) {
        if (!isset($data['dimensions_sort'])) {
            $data['dimensions_sort'] = true;
        }
        if ($data['dimensions_sort']) { // specific option for OriginPallet and OriginCrate
            rsort($data['dimensions_outside'], SORT_NUMERIC);
            rsort($data['dimensions_inside'], SORT_NUMERIC);
        }
        $this->tareWeight = $data['tare'];
        $this->maxWeight = $data['max_weight'] + self::MaxBoxWeightCorrection;
        $this->dimensionsOutside = $data['dimensions_outside'];
        $this->dimensionsInside = $data['dimensions_inside'];
        $this->originPackage = $data['origin'];
    }

    /**
     * Increases box tare weight. Actually returns the new box object with increased tare weight
     * @param float $incTareWeight
     * @return PackerBox
     */
    public function adjustTareWeight($incTareWeight) {
        if (Number::floatsAreEqual($incTareWeight, 0)) {
            return $this;
        }
        $newBox = clone($this);
        $newBox->tareWeight += $incTareWeight;
        return $newBox;
    }

    public function getReference() {
        // for packer use
        return $this->originPackage->getTitle();
    }

    public function getEmptyWeight() {
        return $this->tareWeight;
    }

    public function getMaxWeight() {
        return $this->maxWeight;
    }

    public function getOuterLength() {
        return $this->dimensionsOutside[0];
    }

    public function getOuterWidth() {
        return $this->dimensionsOutside[1];
    }

    public function getOuterDepth() {
        return $this->dimensionsOutside[2];
    }

    public function getInnerLength() {
        return $this->dimensionsInside[0];
    }

    public function getInnerWidth() {
        return $this->dimensionsInside[1];
    }

    public function getInnerDepth() {
        return $this->dimensionsInside[2];
    }

    public function getInnerVolume() {
        return array_product($this->dimensionsInside);
    }

    public function getDimensionsOutside() {
        return $this->dimensionsOutside;
    }

    /**
     * @return OriginPackage
     */
    public function getOriginPackage() {
        return $this->originPackage;
    }

}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Packer {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker\Item;

class PackerItem implements Item {
    /**
     * E-Commerce system product id
     * @var int|null
     */
    private $id;
    /** @var string */
    private $description;
    /** @var null|string */
    private $shortDescription;
    /** @var null|int Country of Origin */
    private $originCountryId;
    /** @var null|int Zone of Origin */
    private $originZoneId;
    /** @var null|string Harmonised System code */
    private $hsCode;
    /** @var string[] */
    private $options;
    /** @var float|null */
    private $width;
    /** @var float|null */
    private $length;
    /** @var float|null */
    private $height;
    /** @var float */
    private $weight;
    /**
     * In providers currency
     * @var float
     */
    private $price;
    /** @var bool */
    private $rotatable = true;

    public function __construct($data) {
        $this->id = $data['id'];
        $this->description = $data['description'];
        $this->shortDescription = isset($data['short_description']) ? $data['short_description'] : null;
        $this->originCountryId = isset($data['origin_country_id']) ? $data['origin_country_id'] : null;
        $this->originZoneId = isset($data['origin_zone_id']) ? $data['origin_zone_id'] : null;
        $this->hsCode = isset($data['hs_code']) ? $data['hs_code'] : null;
        $this->options = is_array($data['options']) ? $data['options'] : array();
        $this->length = $data['length'];
        $this->width = $data['width'];
        $this->height = $data['height'];
        $this->weight = $data['weight'];
        $this->price = $data['price'];
        $this->rotatable = isset($data['$rotatable']) ? (bool)$data['$rotatable'] : true;
    }

    public function getId() {
        return $this->id;
    }

    /**
     * Checks that Item is equals to another (by id and options)
     * @param PackerItem $otherItem
     * @return bool
     */
    public function isEqualTo($otherItem) {
        $thisHash = $this->getId() . serialize($this->getOptions());
        $otherHash = $otherItem->getId() . serialize($otherItem->getOptions());
        return $thisHash === $otherHash;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getShortDescription() {
        return $this->shortDescription;
    }

    public function getOriginCountryId() {
        return $this->originCountryId;
    }

    public function getOriginZoneId() {
        return $this->originZoneId;
    }

    public function getHsCode() {
        return $this->hsCode;
    }

    public function getOptions() {
        return $this->options;
    }

    public function getWidth() {
        return $this->width;
    }

    public function getLength() {
        return $this->length;
    }

    public function getDepth() {
        return $this->height;
    }

    public function getHeight() {
        return $this->height;
    }

    public function getWeight() {
        return $this->weight;
    }

    public function getVolume() {
        return $this->width * $this->length * $this->height;
    }

    /**
     * In providers currency
     * @return float
     */
    public function getPrice() {
        return $this->price;
    }

    public function getKeepFlat() {
        return !$this->rotatable;
    }

    public function isRotatable() {
        return $this->rotatable;
    }

    public static function getSample() {
        return new self(array(
            'id' => null,
            'description' => 'test',
            'options' => array(),
            'length' => 10,
            'width' => 20,
            'height' => 30,
            'weight' => 5,
            'price' => 100
        ));
    }
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Packer {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\ConfigurationException;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker\Box;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker\ConstrainedItem;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\DVDoug\BoxPacker\ItemList;

class PackerItemConstrained extends PackerItem implements ConstrainedItem {
    /** @var callable with args: this item, packed items, box. Returns bool */
    private $logic;

    public function __construct($data) {
        parent::__construct($data);
        if (!isset($data['logic'])) {
            throw new ConfigurationException('Constrained item logic missing');
        }
        if (!is_callable($data['logic'])) {
            throw new ConfigurationException('Constrained item logic is not callable');
        }
        $this->logic = $data['logic'];
    }

    /**
     * @param ItemList $alreadyPackedItems
     * @param Box $box
     * @return bool
     */
    public function canBePackedInBox(ItemList $alreadyPackedItems, Box $box) {
        return (bool)call_user_func($this->logic, $this, $alreadyPackedItems, $box);
    }

    /**
     * @return callable
     */
    public function getLogic() {
        return $this->logic;
    }

}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Packer {

class PackerItemToPack {
    /** @var PackerItem */
    private $item;
    /** @var int */
    private $quantity;

    /**
     * @param PackerItem $item
     * @param int $quantity
     */
    public function __construct($item, $quantity) {
        $this->item = $item;
        $this->quantity = $quantity;
    }

    /**
     * @return PackerItem
     */
    public function getItem() {
        return $this->item;
    }

    /**
     * @return int
     */
    public function getQuantity() {
        return $this->quantity;
    }


}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Packer {

class PackerResult {
    /** @var PackerResultBox[] */
    private $boxes;

    /**
     * @param PackerResultBox[] $boxes
     */
    public function __construct($boxes) {
        $this->boxes = $boxes;
    }

    /**
     * @return PackerResultBox[]
     */
    public function getBoxes() {
        return $this->boxes;
    }

    /**
     * @return float
     */
    public function getTotalWeight() {
        return array_sum(array_map(function($box) {
            /** @var PackerResultBox $box */
            return $box->getWeight();
        }, $this->boxes));
    }

    /**
     * Returns total value of all boxes
     * @return float In providers currency
     */
    public function getTotalValue() {
        return array_sum(array_map(function($box) {
            /** @var PackerResultBox $box */
            return $box->getValue();
        }, $this->boxes));
    }

}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Packer {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\Ordered\OrderedBoxPacked;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Ordered\OrderedBoxPackedItem;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\PackagingMap\Map;

class PackerResultBox extends ValuableBox {
    /** @var PackerBox */
    private $box;
    /** @var float */
    private $weight;
    /** @var PackerItem[] */
    private $products;
    /** @var Map|null */
    private $packagingMap;

    /**
     * @param PackerBox $box
     * @param float $weight
     * @param PackerItem[] $products
     * @param Map|null $packagingMap
     */
    public function __construct($box, $weight, $products, $packagingMap) {
        $this->box = $box;
        $this->weight = $weight;
        $this->products = $products;
        $this->packagingMap = $packagingMap;
    }

    /**
     * @return int|null
     */
    public function getId() {
        return $this->box->getOriginPackage()->getId();
    }

    /**
     * @return string
     */
    public function getCode() {
        return $this->box->getOriginPackage()->getCode();
    }

    /**
     * @return string
     */
    public function getType() {
        return $this->box->getOriginPackage()->getType();
    }

    /**
     * @return float|null
     */
    public function getOuterHeight() {
        return $this->box->getOuterDepth();
    }

    /**
     * @return float|null
     */
    public function getInnerHeight() {
        return $this->box->getInnerDepth();
    }

    /**
     * @return float|null
     */
    public function getOuterLength() {
        return $this->box->getOuterLength();
    }

    /**
     * @return float|null
     */
    public function getInnerLength() {
        return $this->box->getInnerLength();
    }

    /**
     * @return int
     */
    public function getProductCount() {
        return count($this->products);
    }

    /**
     * @return PackerItem[]
     */
    public function getProducts() {
        return $this->products;
    }

    /**
     * Returns value of all items in the box
     * @return float In providers currency
     */
    public function getValue() {
        return array_sum(array_map(function($item) {
            /** @var PackerItem $item */
            return $item->getPrice();
        }, $this->products));
    }

    /**
     * @return float
     */
    public function getWeight() {
        return $this->weight;
    }

    /**
     * @return float|null
     */
    public function getOuterWidth() {
        return $this->box->getOuterWidth();
    }

    /**
     * @return float|null
     */
    public function getInnerWidth() {
        return $this->box->getInnerWidth();
    }

    /**
     * @return float
     */
    public function getOuterVolume() {
        return $this->getOuterLength() * $this->getOuterWidth() * $this->getOuterHeight();
    }

    public function getPackagingMap() {
        return $this->packagingMap;
    }

    /**
     * @return OrderedBoxPacked
     */
    public function toOrdered() {
        $groups = array();
        foreach ($this->getProducts() as $item) {
            $groupFound = false;
            foreach ($groups as $k => $group) {
                if ($item->isEqualTo($group['item'])) {
                    $groups[$k]['quantity']++;
                    $groupFound = true;
                    break;
                }
            }
            if (!$groupFound) {
                $groups[] = array('item' => $item, 'quantity' => 1);
            }
        }
        $products = array();
        foreach ($groups as $group) {
            /** @var PackerItem $item */
            $item = $group['item'];
            $products[] = new OrderedBoxPackedItem(array(
                'id' => $item->getId(),
                'description' => $item->getDescription(),
                'short_description' => $item->getShortDescription(),
                'origin_country_id' => $item->getOriginCountryId(),
                'origin_zone_id' => $item->getOriginZoneId(),
                'hs_code' => $item->getHsCode(),
                'weight' => $item->getWeight(),
                'price' => $item->getPrice(),
                'options' => $item->getOptions(),
                'quantity' => $group['quantity']
            ));
        }
        return new OrderedBoxPacked(array(
            'weight' => $this->getWeight(),
            'value' => $this->getValue(),
            'outer_length' => $this->getOuterLength(),
            'outer_width' => $this->getOuterWidth(),
            'outer_height' => $this->getOuterHeight(),
            'inner_length' => $this->getInnerLength(),
            'inner_width' => $this->getInnerWidth(),
            'inner_height' => $this->getInnerHeight(),
            'products' => $products,
            'origin' => $this->box->getOriginPackage(),
            'packaging_map' => $this->getPackagingMap()
        ));
    }

}
}

namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Ordered {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\ConfigurationException;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Origin\OriginPackage;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Packer\ValuableBox;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\PackagingMap\Map;

class OrderedBoxPacked extends ValuableBox {
    /** @var float */
    private $weight;
    /** @var float */
    private $value;
    /** @var float|null */
    private $outerLength;
    /** @var float|null */
    private $innerLength;
    /** @var float|null */
    private $outerWidth;
    /** @var float|null */
    private $innerWidth;
    /** @var float|null */
    private $outerHeight;
    /** @var float|null */
    private $innerHeight;
    /** @var OrderedBoxPackedItem[] */
    private $products;
    /** @var OriginPackage */
    private $originPackage;
    /** @var Map|null */
    private $packagingMap;

    /**
     * @param array $data
     */
    public function __construct($data) {
        $this->weight = $data['weight'];
        $this->value = $data['value'];
        $this->outerLength = $data['outer_length'];
        $this->outerWidth = $data['outer_width'];
        $this->outerHeight = $data['outer_height'];
        $this->innerLength = $data['inner_length'];
        $this->innerWidth = $data['inner_width'];
        $this->innerHeight = $data['inner_height'];
        $this->products = $data['products'];
        $this->originPackage = $data['origin'];
        $this->packagingMap = isset($data['packaging_map']) ? $data['packaging_map'] : null;
    }

    /**
     * @since 18.10.2016 dimensions can be null when weight-based packed
     * @param array $data
     * @return OrderedBoxPacked
     * @throws ConfigurationException
     */
    public static function createFromArray($data) {
        // backward compatibility for dimensions
        if (!array_key_exists('outer_length', $data)) {
            $data['outer_length'] = isset($data['length']) ? $data['length'] : null;
        }
        if (!array_key_exists('inner_length', $data)) {
            $data['inner_length'] = isset($data['length']) ? $data['length'] : null;
        }
        if (!array_key_exists('outer_width', $data)) {
            $data['outer_width'] = isset($data['width']) ? $data['width'] : null;
        }
        if (!array_key_exists('inner_width', $data)) {
            $data['inner_width'] = isset($data['width']) ? $data['width'] : null;
        }
        if (!array_key_exists('outer_height', $data)) {
            $data['outer_height'] = isset($data['height']) ? $data['height'] : null;
        }
        if (!array_key_exists('inner_height', $data)) {
            $data['inner_height'] = isset($data['height']) ? $data['height'] : null;
        }

        $data['products'] = array_map(function($product) {
            return new OrderedBoxPackedItem($product);
        }, $data['products']);
        if (!isset($data['origin'])) {
            throw new ConfigurationException('Origin Box is not specified for Ordered Box Packed');
        }
        $data['origin'] = OriginPackage::createFromArray($data['origin']);
        $data['packaging_map'] = isset($data['packaging_map']) ? Map::createFromArray($data['packaging_map']) : null;
        return new OrderedBoxPacked($data);
    }

    /**
     * @return array
     */
    public function toArray() {
        return array(
            'weight' => $this->weight,
            'value' => $this->value,
            'outer_length' => $this->outerLength,
            'outer_width' => $this->outerWidth,
            'outer_height' => $this->outerHeight,
            'inner_length' => $this->innerLength,
            'inner_width' => $this->innerWidth,
            'inner_height' => $this->innerHeight,
            'products' => array_map(function($item) {
                /** @var OrderedBoxPackedItem $item */
                return $item->toArray();
            }, $this->products),
            'origin' => $this->originPackage->toArray(),
            'packaging_map' => $this->packagingMap ? $this->packagingMap->toArray() : null
        );
    }

    /**
     * @return float|null
     */
    public function getOuterHeight() {
        return $this->outerHeight;
    }

    /**
     * @return float|null
     */
    public function getInnerHeight() {
        return $this->innerHeight;
    }

    /**
     * @return float|null
     */
    public function getOuterLength() {
        return $this->outerLength;
    }

    /**
     * @return float|null
     */
    public function getInnerLength() {
        return $this->innerLength;
    }

    /**
     * @return OrderedBoxPackedItem[]
     */
    public function getProducts() {
        return $this->products;
    }

    /**
     * @return float
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * @return float
     */
    public function getWeight() {
        return $this->weight;
    }

    /**
     * @return float|null
     */
    public function getOuterWidth() {
        return $this->outerWidth;
    }

    /**
     * @return float|null
     */
    public function getInnerWidth() {
        return $this->innerWidth;
    }

    /**
     * @return OriginPackage
     */
    public function getOriginPackage() {
        return $this->originPackage;
    }

    /**
     * @return float
     */
    public function getOuterVolume() {
        return $this->getOuterLength() * $this->getOuterWidth() * $this->getOuterHeight();
    }

    /**
     * @return string
     */
    public function getCode() {
        return $this->getOriginPackage()->getCode();
    }

    /**
     * @return null|Map
     */
    public function getPackagingMap() {
        return $this->packagingMap;
    }
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Ordered {

class OrderedBoxPackedItem {
    /** @var int */
    private $id;
    /** @var string */
    private $description;
    /** @var string|null */
    private $shortDescription;
    /** @var int|null */
    private $originCountryId;
    /** @var int|null */
    private $originZoneId;
    /** @var string|null */
    private $hsCode;
    /** @var float */
    private $weight;
    /** @var float */
    private $price;
    /** @var int */
    private $quantity = 1;
    /** @var string[] */
    private $options = array();

    /**
     * @param array $data
     */
    public function __construct($data) {
        $this->id = $data['id'];
        $this->description = $data['description'];
        $this->shortDescription = isset($data['short_description']) ? $data['short_description'] : null;
        $this->originCountryId = isset($data['origin_country_id']) ? $data['origin_country_id'] : null;
        $this->originZoneId = isset($data['origin_zone_id']) ? $data['origin_zone_id'] : null;
        $this->hsCode = isset($data['hs_code']) ? $data['hs_code'] : null;
        $this->weight = $data['weight'];
        $this->price = $data['price'];
        $this->options = $data['options'];
        $this->quantity = $data['quantity'];

    }

    /**
     * @return array
     */
    public function toArray() {
        return array(
            'id' => $this->id,
            'description' => $this->description,
            'short_description' => $this->shortDescription,
            'origin_country_id' => $this->originCountryId,
            'origin_zone_id' => $this->originZoneId,
            'hs_code' => $this->hsCode,
            'weight' => $this->weight,
            'price' => $this->price,
            'options' => $this->options,
            'quantity' => $this->quantity
        );
    }

    /**
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @return null|string
     */
    public function getShortDescription() {
        return $this->shortDescription;
    }

    /**
     * @return int|null
     */
    public function getOriginCountryId() {
        return $this->originCountryId;
    }

    /**
     * @return int|null
     */
    public function getOriginZoneId() {
        return $this->originZoneId;
    }

    /**
     * @return null|string
     */
    public function getHsCode() {
        return $this->hsCode;
    }

    /**
     * @param null|int $numberOfDigits
     * @return string
     */
    public function getHsCodeAsNumeric($numberOfDigits = null) {
        $digits = preg_replace('/[^\d]/', '', $this->getHsCode());
        if ($numberOfDigits) {
            return substr($digits, 0, $numberOfDigits);
        }
        return $digits;
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return string[]
     */
    public function getOptions() {
        return $this->options;
    }

    /**
     * @return float
     */
    public function getPrice() {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getQuantity() {
        return $this->quantity;
    }

    /**
     * @return float
     */
    public function getWeight() {
        return $this->weight;
    }

}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Ordered {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\MultiRequest;

class AccompanyingDocument {
    const TypeLabel = 'LABEL';
    const TypeInvoice = 'INVOICE';

    /** @var string */
    protected $type;
    /** @var string[] base64 encoded binary data */
    protected $pages = array();
    /** @var string[] urls of pages */
    protected $urls = array();

    /**
     * @param string $type
     * @param string[] $pages
     */
    public function __construct($type, $pages) {
        $this->type = $type;
        $this->pages = is_array($pages) ? $pages : array();
    }

    /**
     * @return int
     */
    public function getNumberOfPages() {
        return count($this->pages);
    }

    /**
     * @param int $pageNumber
     * @return string
     */
    public function getPage($pageNumber) {
        return isset($this->pages[$pageNumber]) ? $this->pages[$pageNumber] : null;
    }

    /**
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getTypeDescription() {
        switch ($this->type) {
            case self::TypeLabel:
                return 'Shipping Label';
            case self::TypeInvoice:
                return 'Invoice';
        }
        return null;
    }

    /**
     * @return bool
     */
    public function isLabel() {
        return $this->type === self::TypeLabel;
    }

    /**
     * @return array
     */
    public function toArray() {
        return array(
            'type' => $this->type,
            'pages' => $this->pages
        );
    }

    /**
     * @param array $data
     * @return AccompanyingDocument
     */
    public static function createFromArray($data) {
        return new self($data['type'], $data['pages']);
    }

    /**
     * @param string $type
     * @param string[] $urls
     * @return AccompanyingDocument
     */
    public static function createBlankForFurtherRequest($type, $urls) {
        $document = new static($type, array()); // use static for mock test
        $document->urls = $urls;
        return $document;
    }

    /**
     * Uses MultiRequest to download documents
     * @param AccompanyingDocument[] $allDocuments associated to the list of shipments
     * @param callable $setupFunction with args: $curl and $url
     * @return AccompanyingDocument[] updated
     */
    public static function performMultiRequest($allDocuments, $setupFunction) {
        $multiRequest = new MultiRequest();
        foreach ($allDocuments as $k => $shipmentDocuments) {
            foreach ($shipmentDocuments as $e => $document) {
                foreach ($document->urls as $i => $url) {
                    $curl = curl_init($url);
                    call_user_func($setupFunction, $curl, $url);
                    $multiRequest->addHandler($k . ':' . $e . ':' . $i, $curl);
                }
            }
        }
        $fetched = $multiRequest->execute();
        foreach ($allDocuments as $k => $shipmentDocuments) {
            foreach ($shipmentDocuments as $e => $document) {
                foreach ($document->urls as $i => $url) {
                    $allDocuments[$k][$e]->pages[$i] = base64_encode($fetched[$k . ':' . $e . ':' . $i]);
                    $allDocuments[$k][$e]->urls = array(); // for tests
                }
            }
        }
        return $allDocuments;
    }
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Ordered {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\ConfigurationException;

class Shipment {
    /** @var OrderedBoxPacked */
    private $box;
    /** @var string|null */
    private $trackingNumber = null;
    /** @var string|null */
    private $transactionNumber = null;
    /** @var string|null */
    private $errorMessage = null;
    /** @var AccompanyingDocument[] */
    private $documents = array();

    /**
     * @param array $data
     */
    public function __construct($data) {
        $this->box = $data['box'];
        $this->trackingNumber = $data['tracking_number'];
        // backward compatibility, required for UPS voidLabel()
        $this->transactionNumber = isset($data['transaction_number']) ? $data['transaction_number'] : null;
        $this->errorMessage = $data['error_message'];
        // backward compatibility, replaced label field
        $this->documents = isset($data['documents']) ? (is_array($data['documents']) ? $data['documents'] : array()) :
            array();
    }

    /**
     * @param array $data
     * @return Shipment
     * @throws ConfigurationException
     */
    public static function createFromArray($data) {
        $data['box'] = OrderedBoxPacked::createFromArray($data['box']);
        // backward compatibility of labels
        if (!isset($data['documents'])) {
            $data['documents'] = array();
        }
        $data['documents'] = array_map(function($array) {
            return AccompanyingDocument::createFromArray($array);
        }, $data['documents']);
        if (isset($data['label']) && $data['label']) {
            $data['documents'][] = new AccompanyingDocument(AccompanyingDocument::TypeLabel, array($data['label']));
        }
        return new Shipment($data);
    }

    /**
     * @return array
     */
    public function toArray() {
        return array(
            'box' => $this->box->toArray(),
            'tracking_number' => $this->trackingNumber,
            'transaction_number' => $this->transactionNumber,
            'error_message' => $this->errorMessage,
            'documents' => array_map(function($document) {
                /** @var AccompanyingDocument $document */
                return $document->toArray();
            }, $this->documents)
        );
    }

    /**
     * @return OrderedBoxPacked
     */
    public function getBox() {
        return $this->box;
    }

    /**
     * @return null|string
     */
    public function getErrorMessage() {
        return $this->errorMessage;
    }

    /**
     * @return null|string
     */
    public function getTrackingNumber() {
        return $this->trackingNumber;
    }

    /**
     * @return null|string
     */
    public function getTransactionNumber() {
        return $this->transactionNumber;
    }

    /**
     * @return AccompanyingDocument[]
     */
    public function getDocuments() {
        return $this->documents;
    }

    /**
     * @param int $documentId
     * @return null|AccompanyingDocument
     */
    public function getDocument($documentId) {
        return isset($this->documents[$documentId]) ? $this->documents[$documentId] : null;
    }

    /**
     * @return bool
     */
    public function hasLabel() {
        foreach ($this->documents as $document) {
            if ($document->isLabel()) {
                return true;
            }
        }
        return false;
    }
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Ordered {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\ConfigurationException;

class PackagingList {
    /** @var int */
    private $orderId;
    /** @var string */
    private $serviceName;
    /** @var string */
    private $version;
    /** @var string */
    private $methodCode;
    /** @var array */
    private $address;
    /** @var string */
    private $methodName;
    /** @var Shipment[] */
    private $shipments;

    /**
     * @param array $data
     */
    public function __construct($data) {
        $this->orderId = $data['order_id'];
        $this->serviceName = $data['service_name'];
        $this->version = $data['version'];
        // backward compatibility, required since label request was moved to separate method
        $this->methodCode = isset($data['method_code']) ? $data['method_code'] : null;
        $this->address = isset($data['address']) ? $data['address'] : array();
        $this->methodName = $data['method_name'];
        $this->shipments = $data['shipments'];
    }

    /**
     * @param array $data
     * @return PackagingList
     * @throws ConfigurationException
     */
    public static function createFromArray($data) {
        $data['shipments'] = array_map(function($shipment) {
            return Shipment::createFromArray($shipment);
        }, $data['shipments']);
        return new PackagingList($data);
    }

    /**
     * @return array
     */
    public function toArray() {
        return array(
            'order_id' => $this->orderId,
            'service_name' => $this->serviceName,
            'version' => $this->version,
            'method_code' => $this->methodCode,
            'address' => $this->address,
            'method_name' => $this->methodName,
            'shipments' => array_map(function($shipment) {
                /** @var Shipment $shipment */
                return $shipment->toArray();
            }, $this->shipments)
        );
    }

    /**
     * @return string
     */
    public function getMethodCode() {
        return $this->methodCode;
    }

    /**
     * @return array
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getMethodName() {
        return $this->methodName;
    }

    /**
     * @return int
     */
    public function getOrderId() {
        return $this->orderId;
    }

    /**
     * @return string
     */
    public function getServiceName() {
        return $this->serviceName;
    }

    /**
     * @return Shipment[]
     */
    public function getShipments() {
        return $this->shipments;
    }

    /**
     * @return string
     */
    public function getVersion() {
        return $this->version;
    }

    /**
     * @return string
     */
    public function getStatusText() {
        return implode("\n", array_map(function ($shipment, $k) {
            /** @var Shipment $shipment */
            return 'Box ' . ($k + 1) . ': ' . ($shipment->hasLabel() ?
                'label purchased' : ($shipment->getErrorMessage() ? 'error' : 'waiting')) .
            ($shipment->getTrackingNumber() ? ', tracking number ' .
                $shipment->getTrackingNumber() : '');
        }, $this->getShipments(), array_keys($this->getShipments())));
    }
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible {

class ConfigurationException extends \Exception {
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping {

class PackagingException extends \Exception {
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible {

class ApiException extends \Exception {
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible {

class CoreFeatureException extends \Exception {
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Utils {

class ImperialWeight {
    private $pounds;
    private $ounces;

    /**
     * @param float $pounds
     * @param float $ounces
     */
    public function __construct($pounds, $ounces) {
        $this->pounds = $pounds;
        $this->ounces = $ounces;
    }

    /**
     * @return float
     */
    public function getOunces() {
        return $this->ounces;
    }

    /**
     * @return float
     */
    public function getPounds() {
        return $this->pounds;
    }

}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Utils {

class Time {
    /** Day length is seconds */
    const DayLength = 86400;

    /**
     * @param int $date
     * @param string|null $time 'HH:MM'
     * @return int
     */
    public static function addDayIfTimeHasPassed($date, $time) {
        if ($time !== null) {
            if ($date >= strtotime($time . date(' d.m.Y', $date))) {
                return $date + self::DayLength;
            }
        }
        return $date;
    }

    /**
     * Add business days to date
     * @param array $data Options:
     *      date Day to start (int)
     *      days Number of business days (int)
     *      limit Maximum quantity of days to add (int|null)
     *      holidays Holidays to skip in array(M,D) format (array)
     *      skip_saturday Do not count Saturdays (bool|int)
     *      skip_sunday Do not count Sundays (bool|int)
     * @return int New date
     */
    public static function addBusinessDays($data) {
        $dateFinish = $data['date'] + $data['days'] * self::DayLength;
        $periodStart = $data['date'];
        do {
            $daysOff = self::countDaysOff(
                $periodStart, $dateFinish, $data['holidays'],
                $data['skip_saturday'], $data['skip_sunday']
            );
            if ($daysOff > 0) {
                $periodStart = $dateFinish + self::DayLength;
                $dateFinish += $daysOff * self::DayLength;
            }
        } while ($daysOff > 0);
        if (isset($data['limit'])) {
            if ((($dateFinish - $data['date']) / self::DayLength) > $data['limit']) {
                $dateFinish = $data['date'] + $data['limit'] * self::DayLength;
            }
        }
        return $dateFinish;
    }

    /**
     * Returns quantity of non-business days in period
     * @param int $dateStart
     * @param int $dateFinish
     * @param array $holidays Holidays to include in array(M,D) format
     * @param bool|int $skipSat Do not count Saturdays
     * @param bool|int $skipSun Do not count Sundays
     * @return int
     */
    public static function countDaysOff($dateStart, $dateFinish, $holidays, $skipSat, $skipSun) {
        $counter = 0;
        for ($day = $dateStart; $day <= $dateFinish; $day += self::DayLength) {
            $weekDay = intval(date('w', $day));
            $dayAsArray = array(intval(date('n', $day)), intval(date('j', $day)));
            if ($weekDay === 6) {
                if (intval($skipSat) === 0) {
                    $counter++;
                    continue;
                }
            }
            if ($weekDay === 0) {
                if (intval($skipSun) === 0) {
                    $counter++;
                    continue;
                }
            }
            if (in_array($dayAsArray, $holidays, true)) {
                $counter++;
                continue;
            }
        }
        return $counter;
    }

}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Utils {

class Weight {
    /** Average Density of materials (Pounds per square Inch) */
    const HardCartonDensity = 0.00130;
    const SoftCartonDensity = 0.00092;
    const PaperDensity = 0.00022;
    const PlasticDensity = 0.00012;
    const WoodDensity = 0.012;

    /** convertion constants */
    const OuncesInGram = 0.035274;
    const PoundsInKilogram = 2.20462;
    const OuncesInPound = 16;
    const GramsInKilogram = 1000;

    /** unit constants */
    const UnitOunce = 'oz';
    const UnitPound = 'lb';
    const UnitGram = 'g';
    const UnitKilogram = 'Kg';

    /** unit type constants */
    const UnitTypeSmall = 'SMALL';
    const UnitTypeLarge = 'LARGE';

    /**
     * Divide weight to pounds and ounces
     * @param float $weight
     * @return ImperialWeight
     */
    public static function calculateImperialWeight($weight) {
        $pounds = floor($weight);
        $ounces = round(16 * ($weight - $pounds), 2); // max 5 digits
        return new ImperialWeight($pounds, $ounces);
    }

    /**
     * Returns material weight using square and density
     * @param float $square square inch
     * @param float $density One of density constants
     * @return float lb
     */
    public static function ofMaterial($square, $density) {
        return $square * $density;
    }

    /**
     * Converts pounds to ounces
     * @param float $lb lbs
     * @return float oz
     */
    public static function convertPoundsToOunces($lb) {
        return $lb * self::OuncesInPound;
    }

    /**
     * Convert ounces to pounds
     * @param float $oz
     * @return float Weight in pounds
     */
    public static function convertOuncesToPounds($oz) {
        return $oz / self::OuncesInPound;
    }

    /**
     * Convert grams to kilograms
     * @param float $g
     * @return float Weight in kilograms
     */
    public static function convertGramsToKilograms($g) {
        return $g / self::GramsInKilogram;
    }

    /**
     * Convert kilograms to grams
     * @param float $kg
     * @return float Weight in grams
     */
    public static function convertKilogramsToGrams($kg) {
        return $kg * self::GramsInKilogram;
    }

    /**
     * Converts grams to ounces
     * @param float $g
     * @return float
     */
    public static function convertGramsToOunces($g) {
        return $g * self::OuncesInGram;
    }

    /**
     * Converts ounces to grams
     * @param float $oz
     * @return float
     */
    public static function convertOuncesToGrams($oz) {
        return $oz / self::OuncesInGram;
    }

    /**
     * Converts kilograms to pounds
     * @param float $kg
     * @return float
     */
    public static function convertKilogramsToPounds($kg) {
        return $kg * self::PoundsInKilogram;
    }

    /**
     * Converts pounds to kilograms
     * @param float $lb
     * @return float
     */
    public static function convertPoundsToKilograms($lb) {
        return $lb / self::PoundsInKilogram;
    }

}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Utils {

class Number {
    const Epsilon = 0.00000001;

    /**
     * @param float $a
     * @param float $b
     * @return bool
     */
    public static function floatsAreEqual($a, $b) {
        return abs($a - $b) < self::Epsilon;
    }

    /**
     * Returns adjusted number by fix and percent
     * @param float $number
     * @param float $adjustFix
     * @param float $adjustPercent
     * @return float
     */
    public static function adjust($number, $adjustFix, $adjustPercent) {
        return $adjustFix + ($number * ((100.00 + $adjustPercent) / 100.00));
    }
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Utils {

class Square {
    /**
     * Returns square of box surface
     * @param float[3] $dimensions
     * @return float square inch
     */
    public static function ofBoxSurface($dimensions) {
        return 2 * (
            $dimensions[0] * $dimensions[1] +
            $dimensions[0] * $dimensions[2] +
            $dimensions[1] * $dimensions[2]
        );
    }

    /**
     * Returns square of flat object surface (two sides actually)
     * @param float $length
     * @param float $width
     * @return float
     */
    public static function ofFlatSurface($length, $width) {
        return 2 * $length * $width;
    }
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Utils {

class Arrays {
    private static $spaces = '    ';

    private static function padding($offset) {
        return str_repeat(self::$spaces, $offset);
    }

    /**
     * Returns array of elements of child arrays
     * @param array $arr
     * @param bool $isPreserveNumericKeys
     * @return array
     */
    public static function flatten($arr, $isPreserveNumericKeys) {
        if ($isPreserveNumericKeys) {
            $function = function ($carry, $subarr) {
                return $subarr + $carry;
            };
        } else {
            $function = 'array_merge';
        }
        return array_reduce($arr, $function, array());
    }

    /**
     * Checks that array does not have numeric keys
     * @param $arr
     * @return bool
     */
    public static function isAssoc($arr) {
        $numericKeysCount = count(array_filter(array_keys($arr), function($key) {
            return is_numeric($key);
        }));
        return !(bool)$numericKeysCount;
    }

    /**
     * Save 2-dimensional array to CSV string
     * @param array $arr
     * @return string
     */
    public static function toCsv($arr) {
        $csv = fopen('php://temp/maxmemory:'. (5 * 1024 * 1024), 'r+');
        foreach ($arr as $k => $row) {
            if ($k === 0) {
                fputcsv($csv, array_keys($row));
            }
            fputcsv($csv, array_values($row));
        }
        rewind($csv);
        $result = stream_get_contents($csv);
        fclose($csv);
        return $result;
    }

    /**
     * Returns more reusable dump of array
     * @param array $arr
     * @param int $offset
     * @return string
     */
    public static function dump($arr, $offset = 0) {
        $result = '';
        if (is_array($arr)) {
            $result .= "array(\n";
            foreach ($arr as $k => $entry) {
                $result .= self::padding($offset + 1) . (is_numeric($k) ? $k : '\'' . $k . '\'') . ' => ' .
                    self::dump($entry, $offset + 1);
            }
            $result .= self::padding($offset) . "),\n";
        } elseif (is_object($arr)) {
            $result .= self::padding($offset) . var_export($arr, true) . ",\n";
        } else {
            $result .= (is_int($arr) || is_float($arr) ? $arr :
                    (is_bool($arr) ? ($arr ? 'true' : 'false') : ('\'' . $arr . '\''))) . ",\n";
        }
        if ($offset === 0) {
            echo '<pre>' . $result . '</pre>';
        } else {
            return $result;
        }
    }

}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Utils {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\ConfigurationException;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\CoreFeatureChecker;

class Address {
    const COUNTRY_US = 'US';
    const COUNTRY_AU = 'AU';
    const COUNTRY_CA = 'CA';
    const COUNTRY_NL = 'NL';
    private static $armedForcesZones = array('AA', 'AC', 'AE', 'AF', 'AM', 'AP');
    private static $armedForcesCities = array('APO', 'FPO', 'DPO');
    private static $requiredFields = array('country_id', 'zone_id', 'zone_code', 'city', 'address_1',
        'address_2', 'firstname', 'lastname', 'company', 'telephone', 'email');

    /**
     * Get faked address in United States
     * @return array
     */
    public static function inUnitedStates() {
        return array(
            'iso_code_2' => self::COUNTRY_US,
            'postcode' => '94040'
        );
    }

    public static function inAustralia() {
        return array(
            'iso_code_2' => self::COUNTRY_AU,
            'postcode' => '2000'
        );
    }

    public static function inCanada() {
        return array(
            'iso_code_2' => self::COUNTRY_CA,
            'postcode' => 'M4A1R2'
        );
    }

    /**
     * Checks that address is in US
     * @param array $address
     * @return bool
     */
    public static function isUnitedStates($address) {
        if (isset($address['iso_code_2'])) {
            return $address['iso_code_2'] === self::COUNTRY_US;
        }
        return false;
    }

    /**
     * Checks that address is in Australia
     * @param array $address
     * @return bool
     */
    public static function isAustralia($address) {
        if (isset($address['iso_code_2'])) {
            return $address['iso_code_2'] === self::COUNTRY_AU;
        }
        return false;
    }

    /**
     * Checks that address is in Canada
     * @param array $address
     * @return bool
     */
    public static function isCanada($address) {
        if (isset($address['iso_code_2'])) {
            return $address['iso_code_2'] === self::COUNTRY_CA;
        }
        return false;
    }

    /**
     * Checks that address is US APO/FPO/DPO
     * @param array $address
     * @return bool
     */
    public static function isArmedForces($address) {
        if (self::isUnitedStates($address)) {
            if (isset($address['zone_code']) && isset($address['city'])) {
                if (in_array($address['zone_code'], self::$armedForcesZones, true)) {
                    if (in_array(trim(strtoupper($address['city'])), self::$armedForcesCities, true)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Checks that all required address fields are set
     * @since 21.01.2017 tries to fetch telephone and email from session and customer objects (for EasyPost)
     * @param array $address
     * @param object $session
     * @param object $customer
     * @throws ConfigurationException
     * @return array
     */
    public static function fixAddress($address, $session = null, $customer = null) {
        if (!array_key_exists('iso_code_2', $address)) {
            throw new ConfigurationException('Country field must be set');
        }
        if (!array_key_exists('postcode', $address)) {
            throw new ConfigurationException('Postcode field must be set');
        }
        foreach (self::$requiredFields as $fld) {
            if (!array_key_exists($fld, $address)) {
                $address[$fld] = null;
            }
        }
        if ($address['telephone'] === null && $session) {
            if (CoreFeatureChecker::hasProperty($session, 'data')) {
                if (is_array($session->data)) {
                    if (isset($session->data['guest'])) {
                        if (isset($session->data['guest']['telephone'])) {
                            $address['telephone'] = $session->data['guest']['telephone'];
                        }
                    }
                }
            }
        }
        if ($address['telephone'] === null && $customer) {
            if (CoreFeatureChecker::hasMethod($customer, 'getTelephone')) {
                $address['telephone'] = $customer->getTelephone();
            }
        }
        if ($address['email'] === null && $session) {
            if (CoreFeatureChecker::hasProperty($session, 'data')) {
                if (is_array($session->data)) {
                    if (isset($session->data['guest'])) {
                        if (isset($session->data['guest']['email'])) {
                            $address['email'] = $session->data['guest']['email'];
                        }
                    }
                }
            }
        }
        if ($address['email'] === null && $customer) {
            if (CoreFeatureChecker::hasMethod($customer, 'getEmail')) {
                $address['email'] = $customer->getEmail();
            }
        }
        $address['postcode'] = self::fixPostcode($address['iso_code_2'], $address['postcode']);
        return $address;
    }

    public static function fixPostcode($countryCode, $postcode) {
        $postcode = trim($postcode);
        switch ($countryCode) {
            case self::COUNTRY_NL:
                $postcode = preg_replace('/\s+/', '', $postcode);
                if (strlen($postcode) === 6) {
                    return substr($postcode, 0, 4) . ' ' . substr($postcode, -2, 2);
                }
                return $postcode;
                break;
            default:
                return $postcode;
        }
    }

}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Utils {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\ApiException;

if (!function_exists('json_last_error_msg')) {
    function json_last_error_msg() { // @codingStandardsIgnoreLine
        static $ERRORS = array(
            JSON_ERROR_NONE => 'No error',
            JSON_ERROR_DEPTH => 'Maximum stack depth exceeded',
            JSON_ERROR_STATE_MISMATCH => 'State mismatch (invalid or malformed JSON)',
            JSON_ERROR_CTRL_CHAR => 'Control character error, possibly incorrectly encoded',
            JSON_ERROR_SYNTAX => 'Syntax error',
            JSON_ERROR_UTF8 => 'Malformed UTF-8 characters, possibly incorrectly encoded'
        );
        $error = json_last_error();
        return isset($ERRORS[$error]) ? $ERRORS[$error] : 'Unknown error';
    }
}

class Response {
    const ERROR_EMPTY = 'Error fetching data from API';
    const ERROR_INVALID = 'Invalid API response';

    /**
     * @param string $response
     * @throws ApiException
     * @return \DOMDocument
     */
    public static function fromXml($response) {
        if (!$response) {
            throw new ApiException(self::ERROR_EMPTY);
        }
        $dom = new \DOMDocument('1.0', 'UTF-8');
        try {
            $dom->loadXml($response);
        } catch (\Exception $error) {
            throw new ApiException(self::ERROR_INVALID);
        }
        return $dom;
    }

    /**
     * @param string $response
     * @throws ApiException
     * @return array|null
     */
    public static function fromJson($response) {
        if (!$response) {
            throw new ApiException(self::ERROR_EMPTY);
        }
        $json = json_decode($response, true);
        if (!$json) {
            throw new ApiException(self::ERROR_INVALID . ': ' . json_last_error_msg());
        }
        return $json;
    }


}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Utils {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\ConfigurationException;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\CoreFeatureChecker;

class Shell {

    /**
     * Argument 0 - command
     * Other - arguments
     * @return string Output
     * @throws ConfigurationException
     */
    public static function exec() {
        if (!CoreFeatureChecker::isFunctionEnabled('exec')) {
            throw new ConfigurationException('exec() function is not enabled');
        }
        $args = func_get_args();
        $cmd = array_shift($args);
        $args = array_map('escapeshellarg', $args);
        $finalCmd = $cmd . ' ' . implode(' ', $args);
        exec($finalCmd . ' 2>&1', $output, $err);
        $output = implode("\n", $output);
        if ($err) {
            throw new ConfigurationException('Error while executing command' . "\n" . $finalCmd . "\n" . $output);
        }
        return $output;
    }
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Utils {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\ConfigurationException;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\CoreFeatureChecker;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Shell;

class Label {
    /**
     * @param string $data
     * @param array $position
     * @throws ConfigurationException
     * @return string
     */
    public static function make4x6FromPdfImagick($data, $position) {
        if (!CoreFeatureChecker::isImageMagickInstalled()) {
            throw new ConfigurationException('ImageMagick is not installed on your server.');
        }
        if (!CoreFeatureChecker::isGhostscriptInstalled()) {
            throw new ConfigurationException('Ghostscript is not installed on your server.');
        }
        $imagick = new \Imagick();
        $resolution = $position['resolution'];
        $imagick->setResolution($resolution, $resolution);
        if ($imagick->readImageBlob($data)) {
            $imagick->setIteratorIndex(0);
            $size = $imagick->getImageGeometry();
            $imagick->cropImage(
                $size['width'], $position['height'] * $resolution,
                $position['left'] * $resolution, $position['top'] * $resolution
            );
            $imagick->setImagePage(0, 0, 0, 0);
            $imagick->trimImage(0.3);
            $imagick->setImagePage(0, 0, 0, 0);
            $imagick->rotateImage(new \ImagickPixel('#FFF'), $position['rotate']);
            $size = $imagick->getImageGeometry();
            if (($size['width'] < 4 * $resolution) && ($size['height'] < 6 * $resolution)) {
                $imagick->resizeImage(4 * $resolution, 6 * $resolution, \Imagick::FILTER_POINT, 0, true);
            } else {
                $imagick->cropImage(
                    4 * $resolution, 6 * $resolution,
                    max(0, ($size['width'] - 4 * $resolution) / 2),
                    max(0, ($size['height'] - 6 * $resolution) / 2)
                );
            }
            $imagick->setImageFormat('png');
            $data = $imagick->getImageBlob();
            $imagick->clear();
            unset($imagick);
            return $data;
        }
        throw new ConfigurationException('Can not read label data');
    }

    /**
     * @param $data
     * @param $position
     * @return string
     * @throws ConfigurationException
     */
    public static function make4x6FromPdfGmagick($data, $position) {
        if (!CoreFeatureChecker::isGraphicsMagickInstalled()) {
            throw new ConfigurationException('GraphicsMagick is not installed on your server.');
        }
        if (!CoreFeatureChecker::isGhostscriptInstalled()) {
            throw new ConfigurationException('Ghostscript is not installed on your server.');
        }
        $resolution = $position['resolution'];
        $pdfFile = tempnam(sys_get_temp_dir(), 'label-');
        $pngFile = $pdfFile . '.png';
        $pdfHandler = fopen($pdfFile, 'w');
        fwrite($pdfHandler, $data);
        fclose($pdfHandler);
        Shell::exec('gm', 'convert', '-density', $resolution . 'x' . $resolution, $pdfFile . '[0]', $pngFile);
        unlink($pdfFile);
        $size = self::gmagickGetPngSize($pngFile);
        Shell::exec('gm', 'convert', $pngFile,
            '-crop', $size['width'] . 'x' . $position['height'] * $resolution . '+' . $position['left'] * $resolution .
            '+' . $position['top'] * $resolution,
            '-fuzz', '3%', '-trim',
            '-rotate', $position['rotate'], $pngFile
        );
        $size = self::gmagickGetPngSize($pngFile);
        if (($size['width'] < 4 * $resolution) && ($size['height'] < 6 * $resolution)) {
            Shell::exec('gm', 'convert', $pngFile,
                '-resize', 4 * $resolution . 'x' . 6 * $resolution, $pngFile);
        } else {
            Shell::exec('gm', 'convert', $pngFile,
                '-crop', 4 * $resolution . 'x' . 6 * $resolution . '+' .
                max(0, ($size['width'] - 4 * $resolution) / 2) . '+' . max(0, ($size['height'] - 6 * $resolution) / 2),
                $pngFile
            );
        }
        $result = file_get_contents($pngFile);
        unlink($pngFile);
        return $result;
    }

    /**
     * @param string $data
     * @param array $position
     * @return string
     * @throws ConfigurationException
     */
    public static function make4x6FromPng($data, $position) {
        if (!CoreFeatureChecker::isGdInstalled()) {
            throw new ConfigurationException('GD is not installed on your server.');
        }
        if ($image = imagecreatefromstring($data)) {
            $resolution = $position['resolution'];
            $width = $position['rotate'] ? round(6 * $position['height'] * $resolution / 4) :
                round(4 * $position['height'] * $resolution / 6);
            $image = self::imageCrop(
                $image, $position['left'] * $resolution, $position['top'] * $resolution,
                $width, round($position['height'] * $resolution)
            );
            $image = imagerotate($image, -$position['rotate'], imagecolorallocate($image, 255, 255, 255));
            return self::imagePng($image);
        }
        throw new ConfigurationException('Can not read label data');
    }

    /**
     * @param resource $image
     * @param int $x
     * @param int $y
     * @param int $width
     * @param int $height
     * @return resource
     */
    private static function imageCrop($image, $x, $y, $width, $height) {
        $newImage = imagecreatetruecolor($width, $height);
        imagecopyresampled($newImage, $image, 0, 0, $x, $y, $width, $height, $width, $height);
        imagedestroy($image);
        return $newImage;
    }

    /**
     * @param resource $image
     * @return string
     */
    private static function imagePng(&$image) {
        ob_start();
        imagepng($image);
        $data = ob_get_contents();
        ob_end_clean();
        imagedestroy($image);
        return $data;
    }

    private static function gmagickGetPngSize($filename) {
        $output = Shell::exec('gm', 'identify', $filename);
        if (!preg_match('/\w+\.png PNG (\d+)x(\d+).*/', $output, $matches)) {
            throw new ConfigurationException('Can not parse PNG size: ' . $output);
        }
        return array(
            'width' => $matches[1],
            'height' => $matches[2]
        );
    }

}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Utils {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\PackagingMap\Reserved;

class Scene {
    const ZFightOffset = 0.1;
    const CameraDistance = 1.2;
    const ColorMin = 100;
    const ColorMax = 200;
    const BackgroundColor = 11184810;
    private static $zeroMatrix = array(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);

    /**
     * @return int
     */
    private static function getRandomColor() {
        return array_product(array(
            mt_rand(self::ColorMin, self::ColorMax),
            mt_rand(self::ColorMin, self::ColorMax),
            mt_rand(self::ColorMin, self::ColorMax)
        ));
    }

    /**
     * @param float $boxLength
     * @param float $boxWidth
     * @param float $boxHeight
     * @param Reserved $reserved
     * @return array Matrix
     */
    private static function calculateRelativePosition($boxLength, $boxWidth, $boxHeight, $reserved) {
        return array(
            1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0,
            $reserved->getOffsetLength() - $boxLength / 2 + $reserved->getLength() / 2,
            $reserved->getOffsetHeight() - $boxHeight / 2 + $reserved->getHeight() / 2,
            $reserved->getOffsetWidth() - $boxWidth / 2 + $reserved->getWidth() / 2,
            1
        );
    }

    /**
     * @param int $id
     * @param float $x
     * @param float $y
     * @param float $z
     * @return array Structure
     */
    private static function generateSpotlight($id, $x, $y, $z) {
        return array(
            'uuid' => 'Light' . $id,
            'type' => 'PointLight',
            'name' => 'PointLight ' . $id,
            'matrix' => array(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, $x, $y, $z, 1),
            'color' => 16777215,
            'intensity' => 1,
            'distance' => 0,
            'decay' => 1,
            'shadow' => array(
                'camera' => array(
                    'uuid' => 'LightDirection' . $id,
                    'type' => 'PerspectiveCamera',
                    'fov' => 90,
                    'zoom' => 1,
                    'near' => 0.5,
                    'far' => 500,
                    'focus' => 10,
                    'aspect' => 1,
                    'filmGauge' => 35,
                    'filmOffset' => 0
                )
            )
        );
    }

    /**
     * @param float $boxLength
     * @param float $boxWidth
     * @param float $boxHeight
     * @param Reserved[] $reservedSpace
     * @return string JSON
     */
    public static function generate($boxLength, $boxWidth, $boxHeight, $reservedSpace) {
        $spotlightPosition = max($boxLength, $boxWidth, $boxHeight);
        $cameraPosition = max($boxLength, $boxWidth, $boxHeight) * self::CameraDistance;
        $zFightLength = $boxLength + self::ZFightOffset;
        $zFightWidth = $boxWidth + self::ZFightOffset;
        $zFightHeight = $boxHeight + self::ZFightOffset;
        $geometries = array(
            array(
                'uuid' => 'BoxGeometry',
                'type' => 'BoxBufferGeometry',
                'width' => $zFightLength,
                'height' => $zFightHeight,
                'depth' => $zFightWidth,
                'widthSegments' => 0,
                'heightSegments' => 0,
                'depthSegments' => 0
            )
        );
        foreach ($reservedSpace as $k => $reserved) {
            /** @var Reserved $reserved */
            $geometries[] = array(
                'uuid' => 'ProductGeometry' . $k,
                'type' => 'BoxBufferGeometry',
                'width' => $reserved->getLength(),
                'height' => $reserved->getHeight(),
                'depth' => $reserved->getWidth(),
                'widthSegments' => 0,
                'heightSegments' => 0,
                'depthSegments' => 0
            );
        }
        $materials = array(
            array(
                'uuid' => 'BoxMaterial',
                'type' => 'MeshPhongMaterial',
                'color' => 9591811,
                'emissive' => 0,
                'specular' => 1118481,
                'shininess' => 30,
                'opacity' => 0.5,
                'transparent' => true,
                'depthFunc' => 3,
                'depthTest' => true,
                'depthWrite' => true,
                'skinning' => false,
                'morphTargets' => false,
                'dithering' => false
            )
        );
        foreach ($reservedSpace as $k => $reserved) {
            $materials[] = array(
                'uuid' => 'ProductMaterial' . $k,
                'type' => 'MeshPhongMaterial',
                'color' => self::getRandomColor(),
                'emissive' => 0,
                'specular' => 1118481,
                'shininess' => 30,
                'depthFunc' => 3,
                'depthTest' => true,
                'depthWrite' => true,
                'skinning' => false,
                'morphTargets' => false,
                'dithering' => false
            );
        }
        $products = array();
        foreach ($reservedSpace as $k => $reserved) {
            $products[] = array(
                'uuid' => 'Product' . $k,
                'type' => 'Mesh',
                'name' => 'Product',
                'matrix' => self::calculateRelativePosition($boxLength, $boxWidth, $boxHeight, $reserved),
                'geometry' => 'ProductGeometry' . $k,
                'material' => 'ProductMaterial' . $k,
                'userData' => array(
                    'description' => $reserved->getDescription(),
                )
            );
        }
        $result = array(
            'metadata' => array('type' => 'App'),
            'project' => array(
                'gammaInput' => false,
                'gammaOutput' => false,
                'shadows' => true,
                'vr' => false
            ),
            'camera' => array(
                'metadata' => array(
                    'version' => 4.5,
                    'type' => 'Object',
                    'generator' => 'Object3D.toJSON'
                ),
                'object' => array(
                    'uuid' => 'InitialCamera',
                    'type' => 'PerspectiveCamera',
                    'name' => 'Camera',
                    'matrix' => array(0.167786, 0, -0.985823, 0, -0.476808, 0.875253, -0.081152, 0, 0.862845, 0.483665,
                        0.146855, 0, $cameraPosition, $cameraPosition, $cameraPosition, 1),
                    'fov' => 50,
                    'zoom' => 1,
                    'near' => 0.1,
                    'far' => 10000,
                    'focus' => 10,
                    'aspect' => 1.616482,
                    'filmGauge' => 35,
                    'filmOffset' => 0
                )
            ),
            'scene' => array(
                'metadata' => array(
                    'version' => 4.5,
                    'type' => 'Object',
                    'generator' => 'Object3D.toJSON'
                ),
                'geometries' => $geometries,
                'materials' => $materials,
                'object' => array(
                    'uuid' => 'Scene',
                    'type' => 'Scene',
                    'name' => 'Scene',
                    'matrix' => self::$zeroMatrix,
                    'children' => array(
                        array(
                            'uuid' => 'Box',
                            'type' => 'Mesh',
                            'name' => 'Box',
                            'matrix' => self::$zeroMatrix,
                            'geometry' => 'BoxGeometry',
                            'material' => 'BoxMaterial',
                            'children' => $products
                        ),
                        self::generateSpotlight(3, -$spotlightPosition, $spotlightPosition, $spotlightPosition),
                        self::generateSpotlight(4, $spotlightPosition, $spotlightPosition, $spotlightPosition),
                        self::generateSpotlight(5, $spotlightPosition, $spotlightPosition, -$spotlightPosition),
                        self::generateSpotlight(6, -$spotlightPosition, $spotlightPosition, -$spotlightPosition)
                    ),
                    'background' => self::BackgroundColor
                )
            )
        );
        return json_encode($result);
    }
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Utils {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\CoreFeatureException;

class CurrencyConverter {
    /** @var callable */
    private $converter;
    /** @var object */
    private $config;

    /**
     * @param callable $converter
     * @param object $config
     * @throws CoreFeatureException
     */
    public function __construct($converter, $config) {
        if (!is_callable($converter)) {
            throw new CoreFeatureException('Invalid converter');
        }
        $this->converter = $converter;
        if (!method_exists($config, 'get')) {
            throw new CoreFeatureException('Invalid config');
        }
        $this->config = $config;
    }

    /**
     * @return string
     */
    public function getSystemCurrency() {
        return $this->config->get('config_currency');
    }

    /**
     * @param float $value
     * @param string $fromCurrency
     * @return float
     */
    public function toSystem($value, $fromCurrency) {
        return call_user_func($this->converter, $value, $fromCurrency, $this->getSystemCurrency());
    }

    /**
     * @param float $value
     * @param string $toCurrency
     * @return float
     */
    public function fromSystem($value, $toCurrency) {
        return call_user_func($this->converter, $value, $this->getSystemCurrency(), $toCurrency);
    }
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping\BaseModel;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping\BaseModule;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping\Features;

class CanadapostModule extends BaseModule {

    /**
     * @return CanadapostRatesProvider
     */
    public function getRatesProvider() {
        return new CanadapostRatesProvider();
    }

    /**
     * @return CanadapostLabelsProvider
     */
    public function getLabelsProvider() {
        return new CanadapostLabelsProvider();
    }

    /**
     * @return null
     */
    public function getValidationProvider() {
        return null;
    }

    /**
     * @return null
     */
    public function getVqmod() {
        return \CanadapostSmartFlexibleVqmod::get();
    }

    /** Extension Name */
    const Name = 'canadapost_smart_flexible';

    /**
     * @return string
     */
    public function getExtensionName() {
        return self::Name;
    }

    /**
     * @return string
     */
    public function getServiceName() {
        return 'Canada Post';
    }

    /**
     * Enabled features array
     * @var array
     */
    private static $features = array(
        Features::AuthPassword,
        Features::AuthCustomerNumber,
        Features::AuthContractId,
        Features::StandardPackages,
        Features::CustomPackages,
        Features::ProductionMode,
        Features::Insurance,
        Features::PackerWeightBased,
        Features::OriginZip6Alphanumeric,
        Features::PromoCode,
        Features::ShippingLabel,
        Features::ShippingLabel4x6Inches,
        Features::ShippingLabelRequiresCustomerNumber,
        Features::ShippingLabelVoid
    );

    /**
     * Metric system is default for Canada Post module
     * @return array
     */
    public function getDefaultSettings() {
        $baseSettings = parent::getDefaultSettings();
        $baseSettings['measurement_system'] = BaseModel::MetricMeasures;
        return $baseSettings;
    }

    /**
     * @param string $feature
     * @return bool
     */
    public function hasFeature($feature) {
        return in_array($feature, self::$features, true);
    }

    /**
     * Countries ISO-2 codes, that required zone to be set
     * @var string[]
     */
    private static $countriesRequiredZone = array('CA');

    /**
     * @return string[]
     */
    public function getCountriesRequiredZone() {
        return self::$countriesRequiredZone;
    }

    /**
     * This method replaces default currency with CAD
     * @param string $originCountryCode
     * @return string
     */
    public function getValueCurrencyCode($originCountryCode) {
        return 'CAD';
    }

}
}

namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\Origin\OriginPackage;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping\BaseModel;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\ConfigurationException;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Author;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\VersionChecker;

abstract class BaseModule {
    /** Extension Version Number */
    const Version = '5.4.6';

    /**
     * @return string
     */
    public function getVersion() {
        return self::Version;
    }

    /**
     * @return string
     */
    public function getContactEmail() {
        return Author::ContactEmail;
    }

    /**
     * @return BaseRatesProvider
     */
    abstract public function getRatesProvider();

    /**
     * @return BaseLabelsProvider|null
     */
    abstract public function getLabelsProvider();

    /**
     * @return BaseValidationProvider|null
     */
    abstract public function getValidationProvider();

    /**
     * @return BaseVqmod|null
     */
    abstract public function getVqmod();

    /**
     * Switches extension to demo mode
     * Hides User ID setting in Admin View
     * Prevents settings update
     */
    const isDemoMode = false;

    /**
     * @return bool
     */
    public function getIsDemoMode() {
        return self::isDemoMode;
    }

    /**
     * Returns default settings
     * Used in BaseController and BaseModel::getConfig()
     * @return array
     */
    public function getDefaultSettings() {
        return array(
            'status' => 1,
            'status_for_customers' => 1,
            'minimum_rate' => 1.00,
            'box_weight_adj_fix' => 0.00,
            'weight_adj_fix' => 0.00,
            'weight_adj_per' => 0.00,
            'dimension_adj_fix' => 0.00,
            'rate_adj_fix' => 0.00,
            'rate_adj_per' => 0.00,
            'processing_days' => 0,
            'cutoff' => BaseRatesProvider::CutoffDisabled,
            'processing_saturdays' => 1,
            'processing_sundays' => 1,
            'processing_holidays' => array(),
            'geo_zones' => array(),
            'all_geo_zones' => 1,
            'custom_packages' => array(),
            'custom_envelopes' => array(),
            'promo' => array(),
            'standard_packages' => array(),
            'debug' => Debugger::ShowNothingLogErrors,
            'packer' => BaseModel::PackerIndividual,
            'machinable' => BaseRatesProvider::MachinableFalse,
            'display_time' => 1,
            'display_weight' => 1,
            'all_customer_groups' => 1,
            'customer_groups' => array(),
            'all_stores' => 1,
            'stores' => array(),
            'insurance' => 0,
            'dropoff' => BaseRatesProvider::DropoffRegularPickup,
            'account_rates' => 0,
            'production' => 1,
            'sort_options' => BaseModel::SortByPrice,
            'date_format' => BaseModel::$dateFormats[0],
            'label' => BaseLabelsProvider::LabelDisabled,
            'label_format' => BaseLabelsProvider::LabelFormatOriginal,
            'pdf_converter' => BaseLabelsProvider::PdfConverterImagick,
            'tracking' => BaseLabelsProvider::TrackingSendShipped,
            'avoid_delivery_saturdays' => 0,
            'avoid_delivery_sundays' => 0,
            'measurement_system' => BaseModel::ImperialMeasures,
            'residential' => 0,
            'individual_tare' => 1,
            'weight_based_limit' => 0,
            'product_names' => array(),
            'product_countries' => array(),
            'product_zones' => array(),
            'product_hs_codes' => array(),
            'adjustment_rules' => array(),
            'weight_based_faked_box' => array(),
            'billing_same' => true
        );
    }

    /** @return string */
    abstract public function getExtensionName();

    /** @return string */
    abstract public function getServiceName();

    /**
     * Returns argument prefixed with extension name
     * @param string $key
     * @return string
     */
    public function getPrefixedName($key) {
        return (VersionChecker::get()->isVersion3() ? 'shipping_' : '') . $this->getExtensionName() . '_' . $key;
    }

    /**
     * @param string $feature
     * @return bool
     */
    abstract public function hasFeature($feature);

    /**
     * @return string[]
     */
    abstract public function getCountriesRequiredZone();

    /**
     * Returns currency code used to specify parcel value
     * This method can be replaced in final module
     * @since 13.10.2017 accepts origin country ISO-2 code to detect currency code (required for FedEx India)
     * @param string $originCountryCode
     * @return string
     */
    public function getValueCurrencyCode($originCountryCode) {
        return 'USD';
    }

    /**
     * Fix address (module specific)
     * @param array $address
     * @param object $modelCountry
     * @param object $modelZone
     * @return array
     * @throws ConfigurationException
     */
    public function fixAddress($address, $modelCountry, $modelZone) {
        return $address;
    }

    /**
     * @return string
     */
    public function getCustomPackageFixedType() {
        return OriginPackage::TYPE_BOX;
    }

    /**
     * @return string
     */
    public function getCustomPackageExpandableType() {
        return OriginPackage::TYPE_ENVELOPE;
    }
}
}

namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping {

class Features {
    const Machinable = 'FEATURE_MACHINABLE'; // usps
    const StandardPackages = 'FEATURE_STANDARD_PACKAGES'; // except easypost
    const CustomPackages = 'FEATURE_CUSTOM_PACKAGES';
    const ShipperAddress = 'FEATURE_SHIPPER_ADDRESS';
    const BillingAddress = 'FEATURE_BILLING_ADDRESS'; // fedex freight
    const Insurance = 'FEATURE_INSURANCE';
    const AuthKey = 'FEATURE_AUTHORIZATION_KEY';
    const AuthPassword = 'FEATURE_AUTHORIZATION_PASSWORD';
    const AuthMeter = 'FEATURE_AUTHORIZATION_METER';
    const AuthCustomerNumber = 'FEATURE_AUTHORIZATION_CUSTOMER_NUMBER'; // canada, ups
    const RequiresCustomerNumber = 'FEATURE_REQUIRES_CUSTOMER_NUMBER'; // fedex freight
    const AuthContractId = 'FEATURE_AUTHORIZATION_CONTRACT_ID'; // canada
    const HubId = 'FEATURE_HUB_ID'; // fedex
    const Dropoff = 'FEATURE_DROPOFF'; // fedex
    const AccountRates = 'FEATURE_ACCOUNT_RATES';
    const CommercialRates = 'FEATURE_COMMERCIAL_RATES'; // usps
    const ProductionMode = 'FEATURE_PRODUCTION_MODE';
    const Pickup = 'FEATURE_PICKUP'; // ups
    const MethodsDependOnShipperCountry = 'FEATURE_METHODS_DEPEND_ON_SHIPPER_COUNTRY'; // ups
    const MethodsDependOnUserId = 'FEATURE_METHODS_DEPEND_ON_USER_ID'; // easypost
    const OriginZip5Digits = 'FEATURE_ORIGIN_ZIP_5_DIGITS'; // usps
    const OriginZip6Alphanumeric = 'FEATURE_ORIGIN_ZIP_6_ALPHANUMERIC'; // canada
    const SenderTelephone10Digits = 'FEATURE_SENDER_TELEPHONE_10_DIGITS'; // usps
    const SenderTelephoneNotLonger15Digits = 'FEATURE_SENDER_TELEPHONE_NOT_LONGER_15_DIGITS'; // ups
    const ShippingLabel = 'FEATURE_SHIPPING_LABEL'; // usps, ups
    const ShippingLabel4x6Inches = 'FEATURE_SHIPPING_LABEL_4X6_INCHES'; // usps, ups
    const ShippingLabelVoid = 'FEATURE_SHIPPING_LABEL_VOID'; // ups
    const ShippingLabelRequiresCustomerNumber = 'FEATURE_SHIPPING_LABEL_REQUIRES_CUSTOMER_NUMBER'; // ups, canada
    const ExportFormatClickShip = 'FEATURE_EXPORT_FORMAT_CLICK_N_SHIP'; // usps
    const ExportFormatFedexAddressBook = 'FEATURE_EXPORT_FORMAT_FEDEX_ADDRESS_BOOK'; // fedex
    const PackerWeightBased = 'FEATURE_PACKER_WEIGHT_BASED'; // ups, auspost, canadapost, fedex, usps
    const WeightBasedFakedBox = 'FEATURE_WEIGHT_BASED_FAKED_BOX'; // usps
    const PromoCode = 'FEATURE_PROMO_CODE'; // canada
    const Webhook = 'FEATURE_WEBHOOK'; // easypost
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping {

abstract class BaseValidationProvider {
    /** @var array */
    protected $options = array();
    /** @var callable */
    protected $debugger = null;

    /**
     * @param string $key
     * @param mixed $value
     */
    public function setOption($key, $value) {
        $this->options[$key] = $value;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getOption($key) {
        return isset($this->options[$key]) ? $this->options[$key] : null;
    }

    /**
     * @param callable $debugger
     */
    public function setDebugger($debugger) {
        if (is_callable($debugger)) {
            $this->debugger = $debugger;
        }
    }

    public function debug() {
        if (!empty($this->debugger)) {
            call_user_func_array($this->debugger, func_get_args());
        }
    }

    /**
     * @param array $address
     * @return bool|null
     */
    abstract public function isAddressResidential($address);

}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\ConfigurationException;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Ordered\Shipment;

abstract class BaseLabelsProvider {
    /**
     * Provider options
     * Examples: user_id, postcode, production, label, address, city
     * @var array
     */
    protected $options = array();

    /** @var callable */
    protected $debugger = null;

    /** Common feature value constants */
    const LabelDisabled = 'LABEL_DISABLED';
    const LabelAutomatically = 'LABEL_AUTOMATICALLY';
    const LabelManually = 'LABEL_MANUALLY';
    public static $labelOptions = array(self::LabelDisabled, self::LabelAutomatically, self::LabelManually);
    public static $labelOptionsEnabled = array(self::LabelAutomatically, self::LabelManually);
    const TrackingNotSend = 'TRACKING-NOT-SEND';
    const TrackingSendImmediately = 'TRACKING-SEND-IMMIDIATELY';
    const TrackingSendShipped = 'TRACKING-SEND-SHIPPED';
    const LabelFormatOriginal = 'LABEL_ORIGINAL';
    const LabelFormat4x6 = 'LABEL_4x6';
    const DocumentFormatPDF = 'application/pdf';
    const DocumentFormatPNG = 'image/png';
    const PdfConverterImagick = 'PDF_CONVERTER_IMAGICK';
    const PdfConverterGmagick = 'PDF_CONVERTER_GMAGICK';

    /**
     * @param string $key
     * @param mixed $value
     */
    public function setOption($key, $value) {
        $this->options[$key] = $value;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getOption($key) {
        return isset($this->options[$key]) ? $this->options[$key] : null;
    }

    /**
     * @param callable $debugger
     */
    public function setDebugger($debugger) {
        if (is_callable($debugger)) {
            $this->debugger = $debugger;
        }
    }

    public function debug() {
        if (!empty($this->debugger)) {
            call_user_func_array($this->debugger, func_get_args());
        }
    }

    /**
     * Returns mime type of original document
     * this method can be replaced
     * @return string
     */
    public function getDocumentFormat() {
        return self::DocumentFormatPDF;
    }

    /**
     * @return string
     * @throws ConfigurationException
     */
    public function getDocumentExtension() {
        switch ($this->getDocumentFormat()) {
            case self::DocumentFormatPDF:
                return 'pdf';
            case self::DocumentFormatPNG:
                return 'png';
            default:
                throw new ConfigurationException('Can not convert mime type to extension');
        }
    }

    /**
     * @return array [top,left,height,rotate,resolution]
     */
    abstract public function getLabelPosition();

    /**
     * Returns length of product name to write on shipping label
     * this method MUST be replaced
     * @return int
     */
    public function getProductNameMaxLength() {
        return 255;
    }

    /**
     * @param string $name
     * @throws ConfigurationException
     * @since 09.02.2017 uses iconv/mbstring from utf8
     * @return string
     */
    public function shortenProductName($name) {
        $substrFunc = null;
        if (extension_loaded('mbstring')) {
            $substrFunc = function($string, $offset, $length) {
                return mb_substr($string, $offset, $length);
            };
        } elseif (function_exists('iconv')) {
            $substrFunc = function($string, $offset, $length) {
                return iconv_substr($string, $offset, $length, 'UTF-8');
            };
        } else {
            throw new ConfigurationException('iconv or mbstring required to be installed');
        }
        return (string)$substrFunc(trim((string)$name), 0, $this->getProductNameMaxLength());
    }

    /**
     * @param int $orderId
     * @param Shipment[] $shipments
     * @param array $address
     * @param string $methodCode
     * @return Shipment[]
     */
    abstract public function queryLabelsAndTracking($orderId, $shipments, $address, $methodCode);

    /**
     * @param int $orderId
     * @param Shipment $shipment
     * @return Shipment
     */
    abstract public function voidLabel($orderId, $shipment);
}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\Origin\OriginBox;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Origin\OriginEnvelope;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Origin\OriginPackage;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Packer\PackerResult;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Packer\PackerResultBox;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping\Debugger;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping\BaseRatesProvider;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping\Rate;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Length;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Address;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Weight;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Response;

class CanadapostRatesProvider extends BaseRatesProvider {
    const ApiDeveloperHost = 'https://ct.soa-gw.canadapost.ca/rs/ship/price';
    const ApiProductionHost = 'https://soa-gw.canadapost.ca/rs/ship/price';
    const MaxProcessingDays = 14;
    const MaxBoxWeight = 66; // https://www.canadapost.ca/tools/pg/prices/SBParcels-e.pdf
    const GeneralMethodGroup = 'general';
    const CurrencyCode = 'CAD';
    const MaxPackageInsuranceValue = 1000;

    /**
     * @return int
     */
    public function getMaxProcessingDays() {
        return self::MaxProcessingDays;
    }

    /**
     * @param array $address
     * @return float
     */
    public function getPackageMaxWeight($address) {
        return self::MaxBoxWeight;
    }

    /**
     * @param array $address
     * @return float in CAD
     */
    public function getPackageMaxInsuranceValue($address) {
        return self::MaxPackageInsuranceValue;
    }

    private static $methodCodes = array(
        'domestic' => array(
            self::GeneralMethodGroup => array('DOM.RP', 'DOM.EP', 'DOM.PC', 'DOM.DT', 'DOM.LIB'),
            'xpress' => array('DOM.XP', 'DOM.XP.CERT')
        ),
        'usa' => array(
            self::GeneralMethodGroup => array('USA.EP', 'USA.SP.AIR', 'USA.XP'),
            'priority' => array('USA.PW.ENV', 'USA.PW.PAK', 'USA.PW.PARCEL'),
            'tracked' => array('USA.TP', 'USA.TP.LVM')
        ),
        'international' => array(
            self::GeneralMethodGroup => array('INT.XP', 'INT.TP'),
            'parcel' => array('INT.IP.AIR', 'INT.IP.SURF'),
            'priority' => array('INT.PW.ENV', 'INT.PW.PAK', 'INT.PW.PARCEL'),
            'smallpacket' => array('INT.SP.AIR', 'INT.SP.SURF')
        )
    );

    /**
     * @return array
     */
    public static function getMethodCodes() {
        return self::$methodCodes;
    }

    /**
     * @return OriginPackage[]
     */
    public function getStandardPackages() {
        return array(
            OriginBox::createFromMetric(array(
                'id' => 1,
                'title' => 'Mailing Box XS',
                'image' => 'mailing-box.jpg',
                'dimensions_outside' => array(14, 14, 14),
                'dimensions_inside' => array(14, 14, 14),
                'density' => Weight::SoftCartonDensity
            )),
            OriginBox::createFromMetric(array(
                'id' => 2,
                'title' => 'Mailing Box S',
                'image' => 'mailing-box.jpg',
                'dimensions_outside' => array(28.6, 22.9, 6.4),
                'dimensions_inside' => array(28.6, 22.9, 6.4),
                'density' => Weight::SoftCartonDensity
            )),
            OriginBox::createFromMetric(array(
                'id' => 3,
                'title' => 'Mailing Box M',
                'image' => 'mailing-box.jpg',
                'dimensions_outside' => array(31.1, 23.5, 13.3),
                'dimensions_inside' => array(31.1, 23.5, 13.3),
                'density' => Weight::SoftCartonDensity
            )),
            OriginBox::createFromMetric(array(
                'id' => 4,
                'title' => 'Mailing Box L',
                'image' => 'mailing-box.jpg',
                'dimensions_outside' => array(38.1, 30.5, 9.5),
                'dimensions_inside' => array(38.1, 30.5, 9.5),
                'density' => Weight::SoftCartonDensity
            )),
            OriginBox::createFromMetric(array(
                'id' => 5,
                'title' => 'Mailing Box XL',
                'image' => 'mailing-box.jpg',
                'dimensions_outside' => array(40, 30.5, 21.6),
                'dimensions_inside' => array(40, 30.5, 21.6),
                'density' => Weight::SoftCartonDensity
            )),
            new OriginBox(array(
                'id' => 6,
                'title' => 'Moving Box 3.1 ft³',
                'image' => 'moving-box-31.jpg',
                'dimensions_outside' => array(18, 18, 16),
                'dimensions_inside' => array(18, 18, 16),
                'density' => Weight::SoftCartonDensity
            )),
            new OriginBox(array(
                'id' => 7,
                'title' => 'Moving Box 2 ft³',
                'image' => 'moving-box-2.jpg',
                'dimensions_outside' => array(18, 15, 12.5),
                'dimensions_inside' => array(18, 15, 12.5),
                'density' => Weight::SoftCartonDensity
            )),
            new OriginBox(array(
                'id' => 8,
                'title' => 'Moving Box 1.5 ft³',
                'image' => 'moving-box-15.jpg',
                'dimensions_outside' => array(16, 13, 13),
                'dimensions_inside' => array(16, 13, 13),
                'density' => Weight::SoftCartonDensity
            )),
            new OriginEnvelope(array(
                'id' => 9,
                'title' => 'Bubble Mailer XS',
                'image' => 'bubble-mailer-0.jpg',
                'length' => 6,
                'width' => 9,
                'density' => Weight::PlasticDensity
            )),
            new OriginEnvelope(array(
                'id' => 10,
                'title' => 'Bubble Mailer ML',
                'image' => 'bubble-mailer-4.jpg',
                'length' => 9.5,
                'width' => 13,
                'density' => Weight::PlasticDensity
            )),
            new OriginEnvelope(array(
                'id' => 11,
                'title' => 'Bubble Mailer L',
                'image' => 'bubble-mailer-5.jpg',
                'length' => 10.5,
                'width' => 16,
                'density' => Weight::PlasticDensity
            )),
            new OriginEnvelope(array(
                'id' => 12,
                'title' => 'Bubble Mailer XL',
                'image' => 'bubble-mailer-7.jpg',
                'length' => 14.5,
                'width' => 19,
                'density' => Weight::PlasticDensity
            )),
            new OriginEnvelope(array(
                'id' => 13,
                'title' => 'Bubble Mailer CD',
                'image' => 'bubble-mailer-cd.jpg',
                'length' => 6.5,
                'width' => 7.5,
                'density' => Weight::PlasticDensity
            )),
            new OriginEnvelope(array(
                'id' => 14,
                'title' => 'Bubble Mailer M',
                'image' => 'bubble-mailer-2.jpg',
                'length' => 8.5,
                'width' => 11,
                'density' => Weight::PlasticDensity
            )),
            new OriginEnvelope(array(
                'id' => 15,
                'title' => 'Kraft Envelope 6x9',
                'image' => 'kraft-69.jpg',
                'length' => 5.75,
                'width' => 9.5,
                'density' => Weight::PaperDensity
            )),
            new OriginEnvelope(array(
                'id' => 16,
                'title' => 'Kraft Envelope 10x13',
                'image' => 'kraft-1013.jpg',
                'length' => 10,
                'width' => 13,
                'density' => Weight::PaperDensity
            )),
            new OriginEnvelope(array(
                'id' => 17,
                'title' => 'Photo Mailer 6x8',
                'image' => 'photo-68.jpg',
                'length' => 6,
                'width' => 8,
                'density' => Weight::SoftCartonDensity
            )),
            new OriginEnvelope(array(
                'id' => 17,
                'title' => 'Photo Mailer 9x12',
                'image' => 'photo-912.jpg',
                'length' => 9,
                'width' => 12,
                'density' => Weight::SoftCartonDensity
            )),
        );
    }

    /**
     * @param string[] $requests
     * @return string[]
     */
    public function queryApi($requests) {
        $multiRequest = new MultiRequest();
        foreach ($requests as $k => $request) {
            $curl = curl_init($this->getOption('production') ? self::ApiProductionHost : self::ApiDeveloperHost);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($curl, CURLOPT_CAINFO, __DIR__ . '/cacert.pem');
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_USERPWD, $this->getOption('user_id') . ':' . $this->getOption('password'));
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/vnd.cpc.ship.rate-v3+xml',
                    'Accept: application/vnd.cpc.ship.rate-v3+xml')
            );
            $multiRequest->addHandler($k, $curl);
            $this->debug(Debugger::Trace, 'CANADA POST DATA SENT:', $request);
        }
        return $multiRequest->execute();
    }

    /**
     * Prepares API Request strings
     * @param array $address
     * @param PackerResultBox[] $boxes
     * @param int $shippingTime
     * @return string[]
     */
    public function prepareApiRequests($address, $boxes, $shippingTime) {
        $requests = array();
        foreach ($boxes as $box) {
            $customerNumber = $this->getOption('customer_number') ?
                '<customer-number>' . $this->getOption('customer_number') . '</customer-number>' : '';
            $contractId = $this->getOption('contract_id') ?
                '<contract-id>' . $this->getOption('contract_id') . '</contract-id>' : '';
            $promoCode = $this->getOption('promo_code') ?
                '<promo-code>' . $this->getOption('promo_code') . '</promo-code>' : '';
            $quoteType = $customerNumber ? 'commercial' : 'counter';
            $shippingDate = date('Y-m-d', $shippingTime);
            if (!$this->getOption('insurance')) {
                $insurance = '';
            } else {
                $insuranceValue = round($box->getValue(), 2);
                $insurance = <<<XML
                    <options>
                        <option>
                            <option-code>COV</option-code>
                            <option-amount>{$insuranceValue}</option-amount>
                        </option>
                    </options>
XML;
            }
            $weight = round(Weight::convertPoundsToKilograms($box->getWeight()), 3);
            if ($box->getOuterLength() && $box->getOuterWidth() && $box->getOuterHeight()) {
                $length = round(Length::convertInchesToCentimeters($box->getOuterLength()), 1);
                $width = round(Length::convertInchesToCentimeters($box->getOuterWidth()), 1);
                $height = round(Length::convertInchesToCentimeters($box->getOuterHeight()), 1);
                $dimensions = <<<XML
                    <dimensions>
                        <length>{$length}</length>
                        <width>{$width}</width>
                        <height>{$height}</height>
                    </dimensions>
XML;
            } else {
                $dimensions = '';
            }
            if (Address::isCanada($address)) {
                $postcode = strtoupper(preg_replace('/[^A-Z0-9]/i', '', $address['postcode']));
                $destination = <<<XML
                    <domestic>
                        <postal-code>{$postcode}</postal-code>
                    </domestic>
XML;
            } elseif (Address::isUnitedStates($address)) {
                $postcode = preg_replace('/[^\d\-]/', '', $address['postcode']);
                if (strlen($postcode) !== 10) {
                    $postcode = substr($postcode, 0, 5);
                }
                $destination = <<<XML
                    <united-states>
                        <zip-code>{$postcode}</zip-code>
                    </united-states>
XML;
            } else {
                $destination = <<<XML
                    <international>
                        <country-code>{$address['iso_code_2']}</country-code>
                    </international>
XML;
            }
            $requests[] = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
                <mailing-scenario xmlns="http://www.canadapost.ca/ws/ship/rate-v3">
                    {$customerNumber}
                    {$contractId}
                    {$promoCode}
                    <quote-type>{$quoteType}</quote-type>
                    <expected-mailing-date>{$shippingDate}</expected-mailing-date>
                    {$insurance}
                    <parcel-characteristics>
                        <weight>{$weight}</weight>
                        {$dimensions}
                    </parcel-characteristics>
                    <origin-postal-code>{$this->getOption('postcode')}</origin-postal-code>
                    <destination>
                        {$destination}
                    </destination>
                </mailing-scenario>
XML;
        }
        return $requests;
    }

    /**
     * @param string $response
     * @param array $address
     * @param PackerResultBox[] $boxes
     * @return Rate[]
     * @throws ApiException
     */
    public function parseApiResponse($response, $address, $boxes) {
        $this->debug(Debugger::Trace, 'CANADA POST DATA RECV:', $response);
        $dom = Response::fromXml($response);
        /** @var \DomElement $messagesNode */
        $messagesNode = $dom->getElementsByTagName('messages')->item(0);
        if ($messagesNode) {
            $errorMessages = array_map(function($messageNode) {
                /** @var \DomElement $messageNode */
                return $messageNode->getElementsByTagName('description')->item(0)->nodeValue;
            }, iterator_to_array($messagesNode->getElementsByTagName('message')));
            throw new ApiException(implode('; ', $errorMessages));
        }
        $result = array();
        foreach ($dom->getElementsByTagName('price-quote') as $priceNode) {
            /** @var \DomElement $priceNode */
            $methodCode = $priceNode->getElementsByTagName('service-code')->item(0)->nodeValue;
            $methodFullCode = (Address::isCanada($address) ? 'domestic_' :
                (Address::isUnitedStates($address) ? 'usa_' : 'international_')) . $methodCode;
            if ($this->isMethodEnabled($methodFullCode)) {
                $methodName = $priceNode->getElementsByTagName('service-name')->item(0)->nodeValue;
                $cost = $priceNode->getElementsByTagName('due')->item(0)->nodeValue;
                $expectedNode = $priceNode->getElementsByTagName('expected-delivery-date')->item(0);
                $expectedTime = $expectedNode ? strtotime($expectedNode->nodeValue) : null;
                $result[] = Rate::create()
                    ->setId($methodCode)
                    ->setMethodFullCode($methodFullCode)
                    ->setTitle($methodName)
                    ->setCost($cost)
                    ->setCurrencyCode('CAD')
                    ->setTimeFrom($expectedTime)
                    ->setTimeTo($expectedTime)
                    ->setBoxes($boxes); // placed here for consistency
            }
        }
        return $result;
    }

    /**
     * @param array $address
     * @param PackerResult[] $packerResults
     * @param int $shippingTime
     * @return Rate[]
     */
    public function queryRates($address, $packerResults, $shippingTime) {
        /** @var PackerResult $packerResult */
        $packerResult = current($packerResults);
        $boxes = $packerResult->getBoxes();
        $requests = $this->prepareApiRequests($address, $boxes, $shippingTime);
        $responses = $this->queryApi($requests);
        $rates = array();
        foreach ($responses as $response) {
            $rates = array_merge($rates, $this->parseApiResponse($response, $address, $boxes));
        }
        return $this->combineRates($rates, $boxes);
    }

}
}

namespace CanadapostSmartFlexibleRootNS\SmartFlexible {

class MultiRequest {
    /** @var resource */
    private $multiHandler;
    /** @var resource[] */
    private $handlers = array();
    /** @var string[] */
    private $results = array();

    public function __construct() {
        $this->multiHandler = curl_multi_init();
    }

    /**
     * @param int|string $id
     * @param resource $handler
     */
    public function addHandler($id, $handler) {
        $this->handlers[$id] = $handler;
        curl_multi_add_handle($this->multiHandler, $handler);
    }

    /**
     * Returns an array of results preserving ids of requests
     * @return string[]
     */
    public function execute() {
        $running = null;
        do {
            curl_multi_exec($this->multiHandler, $running);
            curl_multi_select($this->multiHandler);
        } while ($running);
        foreach ($this->handlers as $id => $handler) {
            $this->results[$id] = curl_multi_getcontent($handler);
            curl_multi_remove_handle($this->multiHandler, $handler);
        };
        curl_multi_close($this->multiHandler);
        return $this->results;
    }

}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\Packer\PackerBox;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Origin\OriginPackage;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Packer\PackerResult;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Packer\PackerResultBox;

abstract class BaseRatesProvider {
    /**
     * Provider options
     * Examples: minimum_rate, tax_class_id, user_id, postcode, processing_days, display_time, ...
     * @var array
     */
    protected $options = array();

    /** @var callable */
    protected $debugger = null;

    /**
     * @param string $key
     * @param mixed $value
     */
    public function setOption($key, $value) {
        $this->options[$key] = $value;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getOption($key) {
        return isset($this->options[$key]) ? $this->options[$key] : null;
    }

    /**
     * Returns a value of 'methods' option element
     * @param string $fullKey
     * @return bool
     */
    public function isMethodEnabled($fullKey) {
        $methods = $this->getOption('methods');
        return isset($methods[$fullKey]) ? (bool)$methods[$fullKey] : false;
    }

    /**
     * @param callable $debugger
     */
    public function setDebugger($debugger) {
        if (is_callable($debugger)) {
            $this->debugger = $debugger;
        }
    }

    public function debug() {
        if (!empty($this->debugger)) {
            call_user_func_array($this->debugger, func_get_args());
        }
    }

    const MachinableTrue = 'MACHINABLE_TRUE';
    const MachinableFalse = 'MACHINABLE_FALSE';
    const MachinableAutomatic = 'MACHINABLE_AUTOMATIC';
    const DropoffBusinessServiceCenter = 'BUSINESS_SERVICE_CENTER';
    const DropoffDropBox = 'DROP_BOX';
    const DropoffRegularPickup = 'REGULAR_PICKUP';
    const DropoffRequestCourier = 'REQUEST_COURIER';
    const DropoffStation = 'STATION';
    const PickupDailyPickup = '01';
    const PickupCustomerCounter = '03';
    const PickupOneTimePickup = '06';
    const PickupLetterCenter = '19';
    const PickupAirServiceCenter = '20';
    const RetailRates = 'RETAIL_RATES';
    const CommercialRates = 'COMMERCIAL_RATES';
    const CommercialPlusRates = 'COMMERCIAL_PLUS_RATES';
    const CutoffDisabled = '';

    /**
     * @return int
     */
    abstract public function getMaxProcessingDays();

    /**
     * @param array $address
     * @return float
     * @since 26.08.2017 can return 0 (USPS) when delivery to this country is not available
     */
    abstract public function getPackageMaxWeight($address);

    /**
     * This method can be replaced in final provider
     * @param array $address
     * @return null|float in currency from Module::getValueCurrencyCode()
     */
    public function getPackageMaxInsuranceValue($address) {
        return null;
    }

    /**
     * Used for pallets
     * This method can be replaced in final provider
     * @return float|null
     */
    public function getPackageExpandableMaxHeight() {
        return null;
    }

    /**
     * This method can be replaced in final provider
     * @return null|string
     */
    public function getStandardPackageFixedDefaultCode() {
        return null;
    }

    /**
     * This method can be replaced in final provider
     * @return null|string
     */
    public function getStandardPackageExpandableDefaultCode() {
        return null;
    }

    /**
     * @return OriginPackage[]
     */
    abstract public function getStandardPackages();

    /**
     * Returns a structure of method codes
     * This method MUST be replaced in final provider
     * @return array
     */
    public static function getMethodCodes() {
        return array();
    }

    /**
     * Returns an array of hidden standard packages IDs
     * This method can be replaced in final provider
     * @return int[]
     */
    public function getHiddenStandardPackagesIds() {
        return array();
    }

    /**
     * Returns an array of standard boxes to attach to Packer
     * @param OriginPackage[] $boxes
     * @param array $address
     * @return PackerBox[]
     */
    public function createStandardPackages($boxes, $address) {
        $result = array();
        $maxWeight = $this->getPackageMaxWeight($address);
        $maxHeight = $this->getPackageExpandableMaxHeight();
        foreach ($boxes as $box) {
            $result = array_merge($result, $box->generatePackerBoxes($maxWeight, $maxHeight));
        }
        return $result;
    }

    /**
     * Returns variants of packaging
     * By default: 1) all enabled standard and custom packages
     * This method can be replaced in final provider
     * @param array $address
     * @param PackerBox[] $customPackagesFixed
     * @param PackerBox[] $customPackagesExpandable
     * @return PackerBox[][] variants of boxes sets
     */
    public function getPackagingVariants($address, $customPackagesFixed, $customPackagesExpandable) {
        $variants = array();
        /**
         * Filter standard packages
         * @param PackerBox[] $standardPackages
         * @param array $enabledPackages config array: [id => bool]
         * @return PackerBox[]
         */
        $filterBoxesFunc = function($standardPackages, $enabledPackages) {
            // array_values drops keys
            return array_values(array_filter($standardPackages,
                function($box) use ($enabledPackages) {
                    /** @var PackerBox $box */
                    if (isset($enabledPackages[$box->getOriginPackage()->getId()])) {
                        return (bool)$enabledPackages[$box->getOriginPackage()->getId()];
                    }
                    return false;
                }
            ));
        };
        $variants[] = array_merge(
            $filterBoxesFunc(
                $this->createStandardPackages($this->getStandardPackages(), $address),
                $this->getOption('standard_packages')
            ),
            $customPackagesFixed,
            $customPackagesExpandable
        );
        return $variants;
    }

    /**
     * Returns a list of shipping method groups keys prefixed with argument
     * @param bool|string $prefix Use 'text' for translations
     * @return string[]
     */
    public function getAllShippingMethodGroups($prefix) {
        $result = array();
        foreach ($this->getMethodCodes() as $service => $groups) {
            $result[] = ($prefix ? $prefix . '_' : '') . $service;
            foreach ($groups as $group => $codes) {
                $result[] = ($prefix ? $prefix . '_' : '') . $group;
            }
        }
        return $result;
    }

    /**
     * Returns a list of shipping method keys prefixed with argument
     * @param bool|string $prefix
     * @return string[]
     */
    public function getAllShippingMethods($prefix = false) {
        $result = array();
        foreach ($this->getMethodCodes() as $service => $groups) {
            foreach ($groups as $codes) {
                foreach ($codes as $code) {
                    $result[] = ($prefix ? $prefix . '_' : '') . $service . '_' . $code;
                }
            }
        }
        return $result;
    }

    /**
     * @param array $address
     * @param PackerResult[] $packerResults
     * @param int $shippingTime
     * @return Rate[]
     */
    abstract public function queryRates($address, $packerResults, $shippingTime);

    /**
     * Combines data of Rates using ID
     * @param Rate[] $rates
     * @param PackerResultBox[] $boxes
     * @return Rate[]
     */
    public function combineRates($rates, $boxes) {
        /** @var Rate[] $result */
        $result = array();
        $crc = array();
        foreach ($rates as $rate) {
            /** @var Rate $rate */
            if (isset($result[$rate->getId()])) {
                $result[$rate->getId()]->setCost($result[$rate->getId()]->getCost() + $rate->getCost());
                $crc[$rate->getId()]++;
            } else {
                $result[$rate->getId()] = $rate;
                $crc[$rate->getId()] = 1;
            }
        }
        $validCrc = array_filter($crc, function ($count) use ($boxes) {
            return $count === count($boxes);
        });
        return array_intersect_key($result, $validCrc);
    }

}
}

namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\Packer\PackerResultBox;

class Rate {
    /**
     * This id is unique in rate response.
     * It contain method code (not unique for USPS) or rate unique id (for EasyPost)
     * @var string
     */
    private $id;
    /**
     * This code is used for adjustment rules based on services.
     * This code is not unique for EasyPost rates and represents Carrier account
     * This code is the same as id for UPS
     * @since 09.04.2017
     * @var string
     */
    private $methodFullCode;
    /** @var string */
    private $title;
    /**
     * Is title provided as code and should be decoded
     * @var bool
     */
    private $isTitleEncoded;
    /** @var float */
    private $costBeforeAdjustmentInSystemCurrency;
    /** @var float */
    private $cost;
    /** @var string */
    private $currencyCode;
    /**
     * Date or number of days to deliver
     * @var int|null
     */
    private $timeFrom;
    /** @var int|null */
    private $timeTo;
    /** @var PackerResultBox[] */
    private $boxes;

    public static function create() {
        return new self();
    }

    private function __construct() {
    }

    /**
     * @return float
     */
    public function getCost() {
        return $this->cost;
    }

    /**
     * @param float $cost
     * @return $this
     */
    public function setCost($cost) {
        $this->cost = (float)$cost;
        return $this;
    }

    /**
     * @return float
     */
    public function getCostBeforeAdjustmentInSystemCurrency() {
        return $this->costBeforeAdjustmentInSystemCurrency;
    }

    /**
     * @param float $cost in system currency
     * @return $this
     */
    public function setCostBeforeAdjustmentInSystemCurrency($cost) {
        $this->costBeforeAdjustmentInSystemCurrency = (float)$cost;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyCode() {
        return $this->currencyCode;
    }

    /**
     * @param string $currencyCode
     * @return $this
     */
    public function setCurrencyCode($currencyCode) {
        $this->currencyCode = $currencyCode;
        return $this;
    }

    /**
     * Returns original Rate ID defined by Rates Provider
     * @return string
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Returns method full code
     * @return string
     */
    public function getMethodFullCode() {
        return $this->methodFullCode;
    }

    /**
     * Returns ID prepared for E-Commerce system (prefixed with id and without dots)
     * @return string
     */
    public function getIdForEcommerceSystem() {
        return 'id' . str_replace('.', '_', $this->id);
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setMethodFullCode($code) {
        $this->methodFullCode = $code;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getTimeFrom() {
        return $this->timeFrom;
    }

    /**
     * @param int|null $timeFrom
     * @return $this
     */
    public function setTimeFrom($timeFrom) {
        $this->timeFrom = $timeFrom;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getTimeTo() {
        return $this->timeTo;
    }

    /**
     * @param int|null $timeTo
     * @return $this
     */
    public function setTimeTo($timeTo) {
        $this->timeTo = $timeTo;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsTitleEncoded() {
        return $this->isTitleEncoded;
    }

    /**
     * @param boolean $isTitleEncoded
     * @return $this
     */
    public function setIsTitleEncoded($isTitleEncoded) {
        $this->isTitleEncoded = $isTitleEncoded;
        return $this;
    }

    /**
     * @param PackerResultBox[] $boxes
     * @return $this
     */
    public function setBoxes($boxes) {
        $this->boxes = $boxes;
        return $this;
    }

    /**
     * @return PackerResultBox[]
     */
    public function getBoxes() {
        return $this->boxes;
    }

    /**
     * @return bool
     */
    public function isParticularDate() {
        return ($this->getTimeFrom() === $this->getTimeTo()) && ($this->getTimeFrom() !== null);
    }

    /**
     * @return bool
     */
    public function isDateInternal() {
        return ($this->getTimeFrom() !== $this->getTimeTo()) && ($this->getTimeFrom() !== null);
    }

}
}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\Ordered\AccompanyingDocument;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping\BaseLabelsProvider;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping\Debugger;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Ordered\Shipment;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Ordered\OrderedBoxPackedItem;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Response;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Weight;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Utils\Length;

class CanadapostLabelsProvider extends BaseLabelsProvider {
    /** Api urls with places for customer numbers */
    const ContractShipmentDeveloperApi = 'https://ct.soa-gw.canadapost.ca/rs/%s/%s/shipment';
    const ContractShipmentProductionApi = 'https://soa-gw.canadapost.ca/rs/%s/%s/shipment';
    const NonContractShipmentDeveloperApi = 'https://ct.soa-gw.canadapost.ca/rs/%s/ncshipment';
    const NonContractShipmentProductionApi = 'https://soa-gw.canadapost.ca/rs/%s/ncshipment';
    const ServiceDeveloperApi = 'https://ct.soa-gw.canadapost.ca/rs/ship/service/%s?country=%s';
    const ServiceProductionApi = 'https://soa-gw.canadapost.ca/rs/ship/service/%s?country=%s';
    const IncreaseLimitUrl = 'https://www.canadapost.ca/cpotools/apps/drc/increaseLimits?execution=e8s1';

    /** @var bool Use contract shipment */
    private $isContractShipment = true;

    /** @var string[] One of these options used in label request */
    private static $nonDeliveryOptions = array('RASE', 'RTS', 'ABAN');

    /**
     * @return array [top,left,height,rotate,resolution]
     */
    public function getLabelPosition() {
        return array(
            'top' => 0 / 300,
            'left' => 0 / 300,
            'height' => 1800 / 300,
            'rotate' => 0,
            'resolution' => 300
        );
    }

    /**
     * @return int
     */
    public function getProductNameMaxLength() {
        return 45;
    }

    public function setContractShipment() {
        $this->isContractShipment = $this->getOption('contract_id');
    }

    /**
     * Returns the code of non-delivery option for requested shipping method
     * Or returns null when non-delivery options are not available for this method
     * @param string $response
     * @return string|null
     */
    public function parseServiceNonDeliveryOptionsResponse($response) {
        $this->debug(Debugger::Trace, 'CANADA POST SERVICE INFO RESPONSE:', $response);
        try {
            $dom = Response::fromXml($response);
            $options = $dom->getElementsByTagName('option-code');
        } catch (\Exception $e) {
            return null;
        }
        for ($i = 0; $i < $options->length; $i++) {
            if (in_array($options->item($i)->nodeValue, self::$nonDeliveryOptions, true)) {
                return $options->item($i)->nodeValue;
            }
        }
        return null;
    }

    /**
     * Generates requests body to create shipping labels
     * @param Shipment[] $shipments
     * @param array $address
     * @param string $methodCode
     * @param string|null $nonDeliveryOption
     * @internal param int $orderId
     * @return string[]
     */
    public function prepareShipmentRequests($shipments, $address, $methodCode, $nonDeliveryOption) {
        $requests = array();
        $nonDelivery = '';
        $_this = $this;
        if ($nonDeliveryOption) {
            $nonDelivery = <<<XML
            <option>
                <option-code>{$nonDeliveryOption}</option-code>
            </option>
XML;
        }
        foreach ($shipments as $k => $shipment) {
            $box = $shipment->getBox();
            $weight = round(Weight::convertPoundsToKilograms($box->getWeight()), 3);
            $dimensions = '';
            if ($box->getOuterLength() && $box->getOuterWidth() && $box->getOuterHeight()) {
                $length = round(Length::convertInchesToCentimeters($box->getOuterLength()), 1);
                $width = round(Length::convertInchesToCentimeters($box->getOuterWidth()), 1);
                $height = round(Length::convertInchesToCentimeters($box->getOuterHeight()), 1);
                $dimensions = <<<XML

            <dimensions>
                <length>{$length}</length>
                <width>{$width}</width>
                <height>{$height}</height>
            </dimensions>
XML;
            }
            $insuranceValue = 0;
            $sku = implode("\r\n", array_map(function ($product) use (&$insuranceValue, $_this) {
                /** @var OrderedBoxPackedItem $product */
                $weight = round(Weight::convertPoundsToKilograms($product->getWeight()), 3);
                $price = round($product->getPrice(), 2);
                $insuranceValue += $price * $product->getQuantity();
                $description = $_this->shortenProductName(
                    $product->getShortDescription() ?: $product->getDescription()
                );
                $hsCode = '';
                if ($product->getHsCode()) {
                    $hsCode = '<hs-tariff-code>' . $product->getHsCode() .'</hs-tariff-code>';
                }
                $originCountry = '';
                $originZone = '';
                if ($product->getOriginCountryId() && is_callable($_this->getOption('get_country_code'))) {
                    $originCountry = '<country-of-origin>' .
                        call_user_func($_this->getOption('get_country_code'), $product->getOriginCountryId()) .
                        '</country-of-origin>';
                    if ($product->getOriginZoneId() && is_callable($_this->getOption('get_zone_code'))) {
                        $originZone = '<province-of-origin>' .
                            call_user_func($_this->getOption('get_zone_code'), $product->getOriginZoneId()) .
                            '</province-of-origin>';
                    }
                }
                return <<<XML

                <item>
                    <customs-number-of-units>{$product->getQuantity()}</customs-number-of-units>
                    <customs-unit-of-measure>PCE</customs-unit-of-measure>
                    <customs-description>{$description}</customs-description>
                    {$hsCode}
                    <unit-weight>{$weight}</unit-weight>
                    <customs-value-per-unit>{$price}</customs-value-per-unit>
                    {$originCountry}
                    {$originZone}
                </item>
XML;
            }, $box->getProducts()));
            $insurance = '';
            if ($this->getOption('insurance')) {
                // issue #195: CRC required
                // $insuranceValue = round($box->getValue(), 2);
                $insuranceValue = round($insuranceValue, 2);
                $insurance = <<<XML

            <option>
                <option-code>COV</option-code>
                <option-amount>{$insuranceValue}</option-amount>
            </option>

XML;
            }
            $options = '';
            if ($nonDeliveryOption || $insurance) {
                $options = '<options>' . $nonDelivery . $insurance . '        </options>';
            }
            $stateTo = '';
            if ($address['zone_code']) {
                $stateTo = '<prov-state>' . $address['zone_code'] . '</prov-state>';
            }

            if ($this->isContractShipment) {
                $labelFormat = '4x6';
                if (preg_match('/^(INT|USA)\.PW\..+$/si', $methodCode)) { // all priority worldwide methods
                    $labelFormat = '8.5x11';
                }
                $requests[$k] = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<shipment xmlns="http://www.canadapost.ca/ws/shipment-v8">
    <transmit-shipment>true</transmit-shipment>
    <requested-shipping-point>{$this->getOption('postcode')}</requested-shipping-point>
    <delivery-spec>
        <service-code>{$methodCode}</service-code>
        <sender>
            <name>{$this->getOption('owner')}</name>
            <company>{$this->getOption('store')}</company>
            <contact-phone>{$this->getOption('telephone')}</contact-phone>
            <address-details>
                <address-line-1>{$this->getOption('shipper_address')}</address-line-1>
                <city>{$this->getOption('shipper_city')}</city>
                <prov-state>{$this->getOption('shipper_state')}</prov-state>
                <country-code>{$this->getOption('shipper_country')}</country-code>
                <postal-zip-code>{$this->getOption('postcode')}</postal-zip-code>
            </address-details>
        </sender>
        <destination>
            <name>{$address['firstname']} {$address['lastname']}</name>
            <company>{$address['company']}</company>
            <client-voice-number>{$address['telephone']}</client-voice-number>
            <address-details>
                <address-line-1>{$address['address_1']}</address-line-1>
                <address-line-2>{$address['address_2']}</address-line-2>
                <city>{$address['city']}</city>
                {$stateTo}
                <country-code>{$address['iso_code_2']}</country-code>
                <postal-zip-code>{$address['postcode']}</postal-zip-code>
            </address-details>
        </destination>
        {$options}
        <parcel-characteristics>
            <weight>{$weight}</weight>{$dimensions}
            <mailing-tube>false</mailing-tube>
        </parcel-characteristics>
        <print-preferences>
            <output-format>{$labelFormat}</output-format>
        </print-preferences>
        <preferences>
            <show-packing-instructions>false</show-packing-instructions>
            <show-postage-rate>true</show-postage-rate>
            <show-insured-value>true</show-insured-value>
        </preferences>
        <customs>
            <currency>{$this->getOption('value_currency_code')}</currency>
            <reason-for-export>SOG</reason-for-export>
            <sku-list>{$sku}
            </sku-list>
        </customs>
        <settlement-info>
            <contract-id>{$this->getOption('contract_id')}</contract-id>
            <intended-method-of-payment>Account</intended-method-of-payment>
        </settlement-info>
    </delivery-spec>
</shipment>
XML;
            } else { // only from Canada
                $requests[$k] = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<non-contract-shipment xmlns="http://www.canadapost.ca/ws/ncshipment-v4">
    <delivery-spec>
        <service-code>{$methodCode}</service-code>
        <sender>
            <name>{$this->getOption('owner')}</name>
            <company>{$this->getOption('store')}</company>
            <contact-phone>{$this->getOption('telephone')}</contact-phone>
            <address-details>
                <address-line-1>{$this->getOption('shipper_address')}</address-line-1>
                <city>{$this->getOption('shipper_city')}</city>
                <prov-state>{$this->getOption('shipper_state')}</prov-state>
                <postal-zip-code>{$this->getOption('postcode')}</postal-zip-code>
            </address-details>
        </sender>
        <destination>
            <name>{$address['firstname']} {$address['lastname']}</name>
            <company>{$address['company']}</company>
            <client-voice-number>{$address['telephone']}</client-voice-number>
            <address-details>
                <address-line-1>{$address['address_1']}</address-line-1>
                <address-line-2>{$address['address_2']}</address-line-2>
                <city>{$address['city']}</city>
                {$stateTo}
                <country-code>{$address['iso_code_2']}</country-code>
                <postal-zip-code>{$address['postcode']}</postal-zip-code>
            </address-details>
        </destination>
        {$options}
        <parcel-characteristics>
            <weight>{$weight}</weight>{$dimensions}
        </parcel-characteristics>
        <preferences>
            <show-packing-instructions>false</show-packing-instructions>
            <show-postage-rate>true</show-postage-rate>
            <show-insured-value>true</show-insured-value>
        </preferences>
        <customs>
            <currency>{$this->getOption('value_currency_code')}</currency>
            <reason-for-export>SOG</reason-for-export>
            <sku-list>{$sku}
            </sku-list>
        </customs>
    </delivery-spec>
</non-contract-shipment>
XML;

            }
        }
        return $requests;
    }

    /**
     * Analyze result of shipping label creation
     * @param Shipment $shipment
     * @param string $response
     * @return Shipment
     */
    public function parseShipmentResponse($shipment, $response) {
        $info = null;
        $messages = null;
        $errorMessage = null;
        $tracking = null;
        $selfUrl = null;
        $voidUrl = null;
        $documents = array();
        $labelUrls = array();
        $invoiceUrls = array();
        $this->debug(Debugger::Trace, 'CANADA POST LABEL DATA RECV:', $response);
        try {
            $dom = Response::fromXml($response);
            /** @var \DomElement $info */
            $info = $dom->getElementsByTagName('shipment-info')->item(0);
            if (!$info) {
                $info = $dom->getElementsByTagName('non-contract-shipment-info')->item(0);
            }
            /** @var \DomElement $messages */
            $messages = $dom->getElementsByTagName('messages')->item(0);
        } catch (\Exception $e) {
            $errorMessage = $e->getMessage();
        }
        $increaseLimitUrl = self::IncreaseLimitUrl;
        if ($messages) {
            $errorMessage = implode('; ', array_map(function($messageNode) use ($increaseLimitUrl) {
                /** @var \DomElement $messageNode */
                $message = $messageNode->getElementsByTagName('description')->item(0)->nodeValue;
                if ($message === 'Rejected by SLM Monitor') {
                    $message .= '<br><em>This error means that Canada Post API request limit was exceeded (too many ' .
                        'shipping rate requests per minute). To increase request quota, please contact Canada Post ' .
                        '<a href="' . $increaseLimitUrl . '">here</a></em>';
                }
                return $message;
            }, iterator_to_array($messages->getElementsByTagName('message'))));
        }
        if ($info) {
            $tracking = $info->getElementsByTagName('tracking-pin')->item(0);
            $links = $info->getElementsByTagName('link');
            for ($i = 0; $i < $links->length; $i++) {
                if ($links->item($i)->getAttribute('rel') === 'label') {
                    $labelUrls[] = $links->item($i)->getAttribute('href');
                }
                if ($links->item($i)->getAttribute('rel') === 'commercialInvoice') {
                    $invoiceUrls[] = $links->item($i)->getAttribute('href');
                }
                if ($links->item($i)->getAttribute('rel') === 'refund') {
                    $voidUrl = $links->item($i)->getAttribute('href');
                }
                if ($links->item($i)->getAttribute('rel') === 'self') {
                    $selfUrl = $links->item($i)->getAttribute('href');
                }
            }
            if (count($labelUrls)) {
                $documents[] =
                    AccompanyingDocument::createBlankForFurtherRequest(AccompanyingDocument::TypeLabel, $labelUrls);
            }
            if (count($invoiceUrls)) {
                $documents[] =
                    AccompanyingDocument::createBlankForFurtherRequest(AccompanyingDocument::TypeInvoice, $invoiceUrls);
            }
        }
        if (!$voidUrl) {
            $voidUrl = $selfUrl ? ($selfUrl . '/refund') : null;
        }
        return new Shipment(array(
            'box' => $shipment->getBox(),
            'transaction_number' => $voidUrl,
            'tracking_number' => $tracking ? $tracking->nodeValue : null, // may be no tracking number
            'documents' => $documents, // will be fetched on the next step by proceedMultiRequest()
            'error_message' => $errorMessage
        ));
    }

    /**
     * Generates request body to void label
     * @return string
     */
    public function prepareVoidRequest() {
        if ($this->isContractShipment) {
            return <<<XML
<?xml version="1.0" encoding="utf-8"?>
<shipment-refund-request xmlns="http://www.canadapost.ca/ws/shipment-v8">
<email>{$this->getOption('email')}</email>
</shipment-refund-request>
XML;
        } else {
            return <<<XML
<?xml version="1.0" encoding="utf-8"?>
<non-contract-shipment-refund-request xmlns="http://www.canadapost.ca/ws/ncshipment-v4">
<email>{$this->getOption('email')}</email>
</non-contract-shipment-refund-request>
XML;
        }
    }

    /**
     * Analyze result of voiding label
     * @param string $response
     * @throws ApiException
     * @return bool
     */
    public function parseVoidResponse($response) {
        $this->debug(Debugger::Trace, 'CANADA POST LABEL VOID RESPONSE:', $response);
        $errorMessage = null;
        $dom = Response::fromXml($response);
        /** @var \DomElement $ticket */
        $ticket = $dom->getElementsByTagName('service-ticket-id')->item(0);
        if ($ticket) {
            return true;
        }
        /** @var \DomElement $messages */
        $messages = $dom->getElementsByTagName('messages')->item(0);
        if ($messages) {
            $errorMessage = implode('; ', array_map(function($messageNode) {
                /** @var \DomElement $messageNode */
                return $messageNode->getElementsByTagName('description')->item(0)->nodeValue;
            }, iterator_to_array($messages->getElementsByTagName('message'))));
            throw new ApiException($errorMessage);
        }
        return false;
    }

    /**
     * Sends request to get service information
     * @param string $methodCode
     * @param array $address
     * @return string
     */
    public function queryServiceInformation($methodCode, $address) {
        $url = sprintf(
            $this->getOption('production') ? self::ServiceProductionApi : self::ServiceDeveloperApi,
            $methodCode, $address['iso_code_2']
        );
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curl, CURLOPT_CAINFO, __DIR__ . '/cacert.pem');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, $this->getOption('user_id') . ':' . $this->getOption('password'));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Accept: application/vnd.cpc.ship.rate-v3+xml'
        ));
        $this->debug(Debugger::Trace, 'CANADA POST SERVICE INFO REQUEST:', $url);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    /**
     * Sends requests to create shipping labels
     * @param string[] $requests
     * @return string[]
     */
    public function queryShipmentApi($requests) {
        $multiRequest = new MultiRequest();
        if ($this->getOption('production')) {
            if ($this->isContractShipment) {
                $url = sprintf(
                    self::ContractShipmentProductionApi,
                    $this->getOption('customer_number'),
                    $this->getOption('customer_number')
                );
            } else {
                $url = sprintf(self::NonContractShipmentProductionApi, $this->getOption('customer_number'));
            }
        } else {
            if ($this->isContractShipment) {
                $url = sprintf(
                    self::ContractShipmentDeveloperApi,
                    $this->getOption('customer_number'),
                    $this->getOption('customer_number')
                );
            } else {
                $url = sprintf(self::NonContractShipmentDeveloperApi, $this->getOption('customer_number'));
            }
        }
        if ($this->isContractShipment) {
            $contentType = 'application/vnd.cpc.shipment-v8+xml';
        } else {
            $contentType = 'application/vnd.cpc.ncshipment-v4+xml';
        }
        $this->debug(Debugger::Trace, 'CANADA POST LABEL API USED:', $url);
        foreach ($requests as $k => $request) {
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($curl, CURLOPT_CAINFO, __DIR__ . '/cacert.pem');
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_USERPWD, $this->getOption('user_id') . ':' . $this->getOption('password'));
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: ' . $contentType,
                'Accept: ' . $contentType
            ));
            $multiRequest->addHandler($k, $curl);
            $this->debug(Debugger::Trace, 'CANADA POST LABEL DATA SENT:', $request);
        }
        return $multiRequest->execute();
    }

    /**
     * Sends requests to get document binary data
     * @param AccompanyingDocument[] $allDocuments with urls assigned to shipments
     * @return AccompanyingDocument[] with fetched data
     */
    public function queryDocumentData($allDocuments) {
        $_this = $this;
        $setupFunction = function($curl, $url) use ($_this) {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($curl, CURLOPT_CAINFO, __DIR__ . '/cacert.pem');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_USERPWD, $_this->getOption('user_id') . ':' . $_this->getOption('password'));
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Accept: application/pdf'
            ));
            $_this->debug(Debugger::Trace, 'CANADA POST LABEL URL USED:', $url);
        };
        return AccompanyingDocument::performMultiRequest($allDocuments, $setupFunction);
    }

    /**
     * Sends request to void API
     * @param string $url
     * @param string $request
     * @return string
     */
    public function queryVoidApi($url, $request) {
        $curl = curl_init($url);
        if ($this->isContractShipment) {
            $contentType = 'application/vnd.cpc.shipment-v8+xml';
        } else {
            $contentType = 'application/vnd.cpc.ncshipment-v4+xml';
        }
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curl, CURLOPT_CAINFO, __DIR__ . '/cacert.pem');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, $this->getOption('user_id') . ':' . $this->getOption('password'));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: ' . $contentType,
            'Accept: ' . $contentType
        ));
        $this->debug(Debugger::Trace, 'CANADA POST LABEL VOID REQUEST:', $url, $request);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    /**
     * Entry point to request labels and tracking numbers
     * @param int $orderId
     * @param Shipment[] $shipments
     * @param array $address
     * @param string $methodCode
     * @return Shipment[] updated shipments with numbers, labels and messages
     */
    public function queryLabelsAndTracking($orderId, $shipments, $address, $methodCode) {
        $this->setContractShipment();
        $response = $this->queryServiceInformation($methodCode, $address);
        $nonDeliveryOption = $this->parseServiceNonDeliveryOptionsResponse($response);
        $requests = $this->prepareShipmentRequests($shipments, $address, $methodCode, $nonDeliveryOption);
        $responses = $this->queryShipmentApi($requests);
        foreach ($shipments as $k => $shipment) {
            $shipments[$k] = $this->parseShipmentResponse($shipment, $responses[$k]);
        }
        $allDocuments = array_map(function($shipment) {
            /** @var Shipment $shipment */
            return count($shipment->getDocuments()) ? $shipment->getDocuments() : null;
        }, $shipments);
        $allDocuments = $this->queryDocumentData(array_filter($allDocuments));
        foreach ($shipments as $k => $shipment) {
            $shipments[$k] = new Shipment(array(
                'box' => $shipment->getBox(),
                'transaction_number' => $shipment->getTransactionNumber(),
                'tracking_number' => $shipment->getTrackingNumber(),
                'documents' => isset($allDocuments[$k]) ? $allDocuments[$k] : array(),
                'error_message' => $shipment->getErrorMessage()
            ));
        }
        return $shipments;
    }

    /**
     * Entry point to void label
     * @param int $orderId
     * @param Shipment $shipment
     * @return Shipment updated shipment with message
     */
    public function voidLabel($orderId, $shipment) {
        $this->setContractShipment();
        if ($shipment->getTransactionNumber()) {
            $request = $this->prepareVoidRequest();
            $response = $this->queryVoidApi($shipment->getTransactionNumber(), $request);
            try {
                $result = $this->parseVoidResponse($response);
            } catch (\Exception $e) {
                return new Shipment(array(
                    'box' => $shipment->getBox(),
                    'transaction_number' => $shipment->getTransactionNumber(),
                    'tracking_number' => $shipment->getTrackingNumber(),
                    'documents' => $shipment->getDocuments(),
                    'error_message' => 'Void failed: ' . $e->getMessage()
                ));
            }
            return new Shipment(array(
                'box' => $shipment->getBox(),
                'transaction_number' => $result ? null : $shipment->getTransactionNumber(),
                'tracking_number' => $result ? null : $shipment->getTrackingNumber(),
                'documents' => $result ? array() : $shipment->getDocuments(),
                'error_message' => $result ? 'Label has been voided' : 'Failed to void label'
            ));
        } else {
            return new Shipment(array(
                'box' => $shipment->getBox(),
                'transaction_number' => $shipment->getTransactionNumber(),
                'tracking_number' => $shipment->getTrackingNumber(),
                'documents' => $shipment->getDocuments(),
                'error_message' => 'This label can not be voided (no voiding url stored)'
            ));
        }
    }
}
}
namespace {


use \CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping\BaseVqmod;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\CanadapostModule;

class CanadapostSmartFlexibleVqmod extends BaseVqmod {
    public function __construct() {
        parent::__construct(new CanadapostModule());
    }
}

}


namespace CanadapostSmartFlexibleRootNS\SmartFlexible\Shipping {

use \CanadapostSmartFlexibleRootNS\SmartFlexible\Controls\Select;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\CoreFeatureChecker;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\VersionChecker;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Ordered\Shipment;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\Ordered\PackagingList;
use \CanadapostSmartFlexibleRootNS\SmartFlexible\ViewLoader;

abstract class BaseVqmod {
    /** @var BaseVqmod[] */
    protected static $instances = array();
    /** @var BaseModule */
    protected $module;

    /**
     * @return BaseVqmod
     */
    public static function get() {
        $className = get_called_class();
        if (empty(static::$instances[$className])) {
            static::$instances[$className] = new $className();
        }
        return static::$instances[$className];
    }

    protected function __construct($module) {
        $this->module = $module;
    }

    /**
     * Loads an admin or front Smart & Flexible Model
     * @param object $controllerOrModel
     * @param string|null $extName from BaseModule::getExtensionName
     * @return BaseAdminModel|BaseModel
     */
    protected function getSmartFlexibleModel($controllerOrModel, $extName = null) {
        $smartFlexibleModel = 'model_' . (VersionChecker::get()->isVersion3() ? 'extension_' : '') .
            'shipping_' . ($extName ? $extName : $this->module->getExtensionName());
        if (!CoreFeatureChecker::hasProperty($controllerOrModel, $smartFlexibleModel)) {
            $controllerOrModel->load->model(
                (VersionChecker::get()->isVersion3() ? 'extension/' : '') .
                'shipping/' . ($extName ? $extName : $this->module->getExtensionName())
            );
        }
        return $controllerOrModel->$smartFlexibleModel;
    }

    /** @var bool */
    protected $isEnabled = false;

    public function isEnabled() {
        return $this->isEnabled;
    }

    public function setEnabled() {
        $this->isEnabled = true;
    }

    public function addPackagingListButtonLink($orderInfo, &$data, $controller) {
        $extName = $this->module->getExtensionName();
        $route = (VersionChecker::get()->isVersion23() || VersionChecker::get()->isVersion3() ? 'extension/' : '') .
            'shipping/' . $extName;
        if (preg_match('/^' . $extName. '\./', $orderInfo['shipping_code'])) {
            $data[$this->module->getPrefixedName('packaging_list')] = $controller->url->link(
                $route . '/packaginglist',
                (VersionChecker::get()->isVersion3() ? 'user_token' : 'token') . '=' .
                $controller->session->data[VersionChecker::get()->isVersion3() ? 'user_token' : 'token'] .
                '&order_id=' . (int)$controller->request->get['order_id'], true
            );
            $data[$this->module->getPrefixedName('tracking')] = $controller->url->link(
                $route . '/tracking',
                (VersionChecker::get()->isVersion3() ? 'user_token' : 'token') . '=' .
                $controller->session->data[VersionChecker::get()->isVersion3() ? 'user_token' : 'token'] .
                '&order_id=' . (int)$controller->request->get['order_id'], true
            );
        }
    }


    public function addPackagingListButton($packagingListLink, $trackingLink) {
        $trackingLink = html_entity_decode($trackingLink);
        $script = <<<JS
            $(function() {
                $('#input-order-status, select[name="order_status_id"]').on('change', function() {
                    if ($(this).find('option:selected').text() == 'Shipped') {
                        var handlerSuccess = function(text) {
                            $('#input-comment, textarea[name="comment"]').val(text);
                        };
                        $.ajax({
                            url: "{$trackingLink}",
                            success: handlerSuccess,
                            dataType: 'text'
                        });
                    }
                });
            });
JS;
        if (VersionChecker::get()->isVersion2() || VersionChecker::get()->isVersion3()) {
            $result = '<a href="' . $packagingListLink . '" target="_blank" data-toggle="tooltip" ' .
                'title="Packing list and shipping labels" class="btn btn-success"><i class="fa fa-tag"></i></a>' .
                '<script>' . $script . '</script>';
            return $result;
        } elseif (VersionChecker::get()->isVersion1()) {
            return '<a href="' . $packagingListLink . '" target="_blank" class="button">' .
                'Packing list and shipping labels</i></a><script>' . $script . '</script>';
        }
    }

    /**
     * @param object $controller
     * @param array $shippingAddress
     * @param array $orderData
     */
    public function createPackagingList($controller, $shippingAddress, $orderData) {
        $extName = $this->module->getExtensionName();
        if (
            isset($controller->session->data['shipping_method']) &&
            is_array($controller->session->data['shipping_method']) &&
            isset($controller->session->data['shipping_method']['code']) &&
            (strpos($controller->session->data['shipping_method']['code'], '.') !== false)
        ) {
            list($service, $id) = explode('.', $controller->session->data['shipping_method']['code']);
            if ($service === $extName) {
                if ($shippingAddress) {
                    $controller->session->data['shipping_address'] = $shippingAddress;
                }
                $controller->session->data['shipping_address']['email'] = $orderData['email'];
                $controller->session->data['shipping_address']['telephone'] = $orderData['telephone'];
                $offer = $controller->session->data['shipping_methods'][$extName]['quote'][$id];
                $this->getSmartFlexibleModel($controller, $extName)->createPackagingList(
                    $controller->session->data['order_id'],
                    $offer['boxes_info'],
                    $controller->session->data['shipping_address'],
                    $offer['id_clean'],
                    $offer['title_clean']
                );
            }
        }
    }

    public function defineAdminRequest() {
        if (!defined(BaseModel::AdminRequestFlagConst)) {
            define(BaseModel::AdminRequestFlagConst, true);
        }
    }

    /**
     * @param mixed $model
     * @param int $orderId
     * @param array $orderInfo
     * @param int $orderStatusId
     * @return string[]|null
     */
    public function getTrackingNumbers($model, $orderId, $orderInfo, $orderStatusId) {
        $extName = $this->module->getExtensionName();
        if (preg_match('/^' . $extName . '\./', $orderInfo['shipping_code'])) {
            /** @var PackagingList $packagingList */
            $packagingList = $this->getSmartFlexibleModel($model, $extName)->requestLabels($orderId);
            if ($packagingList) {
                $comment = 'Shipping labels have been requested:' . "\n" . $packagingList->getStatusText();
                if (CoreFeatureChecker::hasMethod($model, 'update')) { // v1
                    $model->update($orderId, $orderStatusId, $comment, false);
                } elseif (CoreFeatureChecker::hasMethod($model, 'addOrderHistory')) { // v2
                    $model->addOrderHistory($orderId, $orderStatusId, $comment, false);
                }
            }
            if ($model->config->get($this->module->getPrefixedName('tracking')) ===
                BaseLabelsProvider::TrackingSendImmediately
            ) {
                if ($packagingList) {
                    return array_filter(array_map(function ($shipment) {
                        /** @var Shipment $shipment */
                        return $shipment->getTrackingNumber();
                    }, $packagingList->getShipments()));
                }
            }
        }
        return null;
    }

    public function addTrackingNumbersForHtmlMail($trackingNumbers, &$data) {
        if ($trackingNumbers) {
            $data[$this->module->getPrefixedName('tracking_numbers')] = $trackingNumbers;
        }
    }


    public function addTrackingNumbersForTextMail($trackingNumbers, &$text) {
        if ($trackingNumbers) {
            $text .= 'Tracking numbers: ' . implode(', ', $trackingNumbers) . "\n\n";
        }
    }

    public function addTrackingNumbersInHtmlTemplate($trackingNumbers) {
        if (is_array($trackingNumbers)) {
            $result = '<table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; ' .
                    'border-left: 1px solid #DDDDDD; margin-bottom: 20px;">' .
                    '<thead>' .
                        '<tr>' .
                            '<td style="font-size: 12px; border-right: 1px solid #DDDDDD; ' .
                                'border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; ' .
                                'text-align: left; padding: 7px; color: #222222;">' .
                                'Tracking numbers' .
                            '</td>' .
                        '</tr>' .
                    '</thead>' .
                    '<tbody>';
            foreach ($trackingNumbers as $trackingNumber) {
                $result .=
                    '<tr>' .
                        '<td style="font-size: 12px; border-right: 1px solid #DDDDDD; ' .
                            'border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">' .
                            $trackingNumber .
                        '</td>' .
                    '</tr>';
            }
            $result .=
                    '</tbody>' .
                '</table>';
            return $result;
        }
    }

    /**
     * @param object $controller
     * @return int|null
     */
    public function getProductNameMaxLength($controller) {
        return $this->getSmartFlexibleModel($controller)->getProductNameMaxLength();
    }

    /**
     * @param object $controller
     * @since 03.05.2017 does not require productId as an argument
     * @return string|null
     */
    public function getProductName($controller) {
        if (isset($controller->request->post[$this->module->getPrefixedName('product_name')])) {
            return $controller->request->post[$this->module->getPrefixedName('product_name')];
        } elseif (isset($controller->request->get['product_id'])) {
            return $this->getSmartFlexibleModel($controller)->getProductName($controller->request->get['product_id']);
        }
        return null;
    }

    /**
     * @param object $controller
     * @return string|null
     */
    public function getProductHsCode($controller) {
        if (isset($controller->request->post[$this->module->getPrefixedName('product_hs_code')])) {
            return $controller->request->post[$this->module->getPrefixedName('product_hs_code')];
        } elseif (isset($controller->request->get['product_id'])) {
            return $this->getSmartFlexibleModel($controller)->getProductHsCode($controller->request->get['product_id']);
        }
        return null;
    }

    /**
     * @param object $controller
     * @return string html select
     */
    public function getProductCountries($controller) {
        $controller->load->model('localisation/country');
        $viewBuilder = ViewLoader::getViewBuilder(array(
            'get_prefixed_name' => Array($this->module, 'getPrefixedName'),
            'get_extension_name' => Array($this->module, 'getExtensionName'),
            'has_feature' => Array($this->module, 'hasFeature'),
            'get_current_route' => function() use ($controller) {
                return $controller->request->get['route'];
            },
            'countries' => array_merge(array(
                array(
                    'country_id' => null,
                    'name' => '—'
                )
            ), $controller->model_localisation_country->getCountries()),
            $this->module->getPrefixedName('product_country') =>
                isset($controller->request->post[$this->module->getPrefixedName('product_country')]) ?
                $controller->request->post[$this->module->getPrefixedName('product_country')] : (
                    isset($controller->request->get['product_id']) ?
                        $this->getSmartFlexibleModel($controller)->getProductCountry(
                            $controller->request->get['product_id']
                        ) : null
                )
        ));
        return $viewBuilder->renderSelect(Select::create()
            ->setNameKey('product_country')
            ->setCaptionField('name')
            ->setValueField('country_id')
            ->setSourceKey('countries'));
    }

    /**
     * @param object $controller
     * @return string html select
     */
    public function getProductZones($controller) {
        $controller->load->model('localisation/zone');
        $viewBuilder = ViewLoader::getViewBuilder(array(
            'get_prefixed_name' => Array($this->module, 'getPrefixedName'),
            'get_extension_name' => Array($this->module, 'getExtensionName'),
            'has_feature' => Array($this->module, 'hasFeature'),
            'get_current_route' => function() use ($controller) {
                return $controller->request->get['route'];
            },
            'zones' => array_merge(array(
                array(
                    'zone_id' => null,
                    'name' => '—'
                )
            ), $controller->model_localisation_zone->getZonesByCountryId(
                isset($controller->request->post[$this->module->getPrefixedName('product_country')]) ?
                    $controller->request->post[$this->module->getPrefixedName('product_country')] : (
                isset($controller->request->get['product_id']) ?
                    $this->getSmartFlexibleModel($controller)->getProductCountry(
                        $controller->request->get['product_id']
                    ) : null
                )
            )),
            $this->module->getPrefixedName('product_zone') =>
                isset($controller->request->post[$this->module->getPrefixedName('product_zone')]) ?
                    $controller->request->post[$this->module->getPrefixedName('product_zone')] : (
                isset($controller->request->get['product_id']) ?
                    $this->getSmartFlexibleModel($controller)->getProductZone(
                        $controller->request->get['product_id']
                    ) : null
                )
        ));
        return $viewBuilder->renderSelect(Select::create()
            ->setNameKey('product_zone')
            ->setCaptionField('name')
            ->setValueField('zone_id')
            ->setSourceKey('zones'));
    }

    /**
     * @param object $controller
     * @param int $productId
     * @param string $name
     * @return string|null
     */
    public function setProductName($controller, $productId, $name) {
        return $this->getSmartFlexibleModel($controller)->setProductName($productId, $name);
    }

    /**
     * @param object $controller
     * @param int $productId
     * @param int $countryId
     * @return int|null
     */
    public function setProductCountry($controller, $productId, $countryId) {
        return $this->getSmartFlexibleModel($controller)->setProductCountry($productId, $countryId);
    }

    /**
     * @param object $controller
     * @param int $productId
     * @param int $zoneId
     * @return int|null
     */
    public function setProductZone($controller, $productId, $zoneId) {
        return $this->getSmartFlexibleModel($controller)->setProductZone($productId, $zoneId);
    }

    /**
     * @param object $controller
     * @param int $productId
     * @param string $hsCode
     * @return string|null
     */
    public function setProductHsCode($controller, $productId, $hsCode) {
        return $this->getSmartFlexibleModel($controller)->setProductHsCode($productId, $hsCode);
    }
}
}